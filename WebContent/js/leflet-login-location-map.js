/*
	Created By: Anik
	Date:		20 Apr 2022
 */
var fillColorEMIS;
var fillColorHover= '#00FF00';
var map = L.map('map-container').setView([23.251314, 90.851784], 8);
var map_layer;
var layerGroup;
var markerGroup;
var animateMap = [
	{
		"name":"Bangladesh",
		"zilla":0
	},
	{
		"name":"Brahaman",
		"zilla":12
	},
	{
		"name":"Chandpur",
		"zilla":13
	},
	{
		"name":"Faridpur",
		"zilla":12
	}

];
var anim_pos=0;
var marker;
var map_parameters ={
	zilla : 0,
	upazila : 0,
	union : 0,
	methodType:0
};

var markerIconFacility = new L.icon({
	iconUrl: "images/image/facility_pointer.png",
	iconSize: [32, 32],
	iconAnchor: [10, 41],
});

var markerIconSatelite = new L.icon({
	iconUrl: "images/image/satelite_pointer.png",
	iconSize: [32, 32],
	iconAnchor: [10, 41],
});

//Bread crumbs
var breadCrumb = [];
//Primary bread crumbs element
var bc = {
	name: "Bangladesh",
	zillaid: 0,
	upazilaid: 0
};
breadCrumb.push(bc);

/*Legend specific*/
var legend = L.control({ position: "topright" });


function getMap(param){
	$.ajax(
		"mapdata",
		{
			data:{
				zilla:param.zilla,
				upazila: param.upazila,
				union:param.union,
				methodType:param.methodType
			},
			type : "POST",
			success: function (data){
				generateMap(data, param.zilla);
				//console.log(data);
			},
			error:function(e){
				alert(e);
			}
		}
	);
}

//this function will generate map and add feature, event (click, hover, etc) and style to the map
function generateMap(geoJsonData, zillaid){
	// initialize the map
	//Move to global declaration
	// map = L.map('map-container').setView([23.251314, 90.851784], 8);

	//Disable Double click zoom
	map.doubleClickZoom.disable();
	// load a tile layer
	//Open street map image as map background

	if($( window ).width()>800)
		map_image_url = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	else
		map_image_url = '';

	L.tileLayer(map_image_url,
		{
			attribution: 'Bangladesh',
			maxZoom: 27,
			minZoom: 1
		}).addTo(map);

	//Initialize Layer group
	//layerGroup = new L.LayerGroup();
	//layerGroup.addTo(map);

	mapJS = JSON.parse(geoJsonData);
	map_layer = L.geoJson(mapJS,{
		onEachFeature: onEachFeature,
		pointToLayer: function(geoJsonPoint, latlng) {
			//return L.marker(latlng, {
			//icon:markerIcon
			//});
		},
		style: polystyle
	}).addTo(map);


	//Legend
	legend.onAdd = function(map) {
		var div = L.DomUtil.create("div", "legend");
		div.innerHTML += '<i style="background: '+fillColorEMIS+'"></i><span>eMIS Area</span><br>';
		div.innerHTML += '<i style="background: #7a7a7a"></i><span>Other</span><br>';

		//div.innerHTML += '<i class="icon" style="background-image: url(https://d30y9cdsu7xlg0.cloudfront.net/png/194515-200.png);background-repeat: no-repeat;"></i><span>Grænse</span><br>';
		return div;
	};

	legend.addTo(map);

	if(zillaid==0)
		load_marker(0,0,0)


	//addMarker(map,L);


	map.fitBounds(map_layer.getBounds());

	//This function will call if it is implemented.
	if(typeof event_mapChange === "function")
		event_mapChange();
}

function addToBreadCrumb(feature){

	bc = {
		name: feature.properties.name,
		zillaid: feature.properties.zillaid,
		upazilaid: feature.properties.upazilaid
	};

	breadCrumb.push(bc);

	//console.log(breadCrumb);
	generateBreadCrumbs();


}

function generateBreadCrumbs(){

	$("#bread-crumb").empty();

	$.each( breadCrumb, function( index, obj ) {

		$("#bread-crumb").append("<li data-zillaid="+obj.zillaid+" data-upazilaid="+obj.upazilaid+">"+obj.name+"</li>");
		//console.log( obj );
	});
}


//THis function will fire in each click on Map
function onEachFeature(feature, layer) {


	fillColorEMIS= feature.properties.color;



	//bind click
	if(feature.properties.is_mamoni ==1){
		layer.on("dblclick",function(e){
			//load map in dbl click
			if (map != null) {
				//map.remove();
				//map_layer.remove();

				remove_previous_map_layer();
				map_parameters.zilla= feature.properties.zillaid;
				map_parameters.upazila= feature.properties.upazilaid;
				//map_parameters.union = feature.properties.unionid;
				//alert(feature.properties.upazilaid);
				//console.log(feature.properties)

				addToBreadCrumb(feature);
				getMap(map_parameters);
				//addToAnimateMap(feature,layer);
			}

			if(typeof event_double_click === "function")
				event_double_click(feature,layer);
		});

		layer.on('click', function (e) {
			//This function will call if it is implemented.
			if(typeof event_click === "function")
			{
				event_click(feature,layer);
			}

			this.setStyle({
				'fillColor': fillColorHover
			});

		});
	}

	//bind Mouse Hover
	if(feature.properties.is_mamoni ==1){
		layer.on('mouseover', function (e) {

			if(typeof event_mouseover === "function")
				event_mouseover(feature,layer);
		});
	}


	//Bind Mouse hover is not Mamoni
	if(feature.properties.is_mamoni !=1){
		layer.on('mouseover', function (e) {
			if (typeof event_mouseover_outside_mamoni === "function")
				event_mouseover_outside_mamoni(feature, layer);
		});
	}



	if(feature.properties.is_mamoni ==1){
		layer.on('mouseover', function () {
			//if(feature.properties.is_mamoni == 1){
			this.setStyle({
				'fillColor': fillColorHover
			});
			//}
		});
	}

	layer.on('mouseout', function () {
		this.setStyle({
			'fillColor': feature.properties.color
		});
	});

	//Adding label to Map
	if(feature.properties.is_mamoni ==1){
		layer.bindTooltip(
			feature.properties.name,
			{
				permanent:true,
				direction:'center',
				className: 'areaLabel'
			}
		);
	}

	//adding Marker on Map. Uncomment following line to get marker
	//addMarker(feature,layer);



}

//This function will add color and style to map
function polystyle(feature) {
	return {
		fillColor: feature.properties.color,
		weight: 2,
		opacity: 1,
		color: 'white',  //Outline color
		fillOpacity: 0.8
	};
}

//Add value to Map Animation list
function addToAnimateMap(feature,layer){

	anim = {
		"name":feature.properties.name,
		"zilla":feature.properties.zillaid
	};
	animateMap.push(anim);
}

function remove_previous_map_layer(){
	// console.log(markerGroup.hasLayer(marker));
	if(map.hasLayer(map_layer)){
		map.removeLayer(map_layer);
	}

}


$(document).ready(function(){
	getMap(map_parameters);

	$("#bread-crumb").on("click","li",function(){
		zillaid=$(this).data('zillaid');
		upazilaid=$(this).data('upazilaid');
		unionid = $(this).data('unionid');

		if (map != null) {

			remove_previous_map_layer();
			map_parameters.zilla= zillaid;
			map_parameters.upazila= upazilaid;
			//map_parameters.union = unionid;
			//console.log(map_parameters);
			getMap(map_parameters);

			//for(i=$(this).index(); breadCrumb.lenght -1; i++)
			breadCrumb.length = $(this).index()+1;
			generateBreadCrumbs();
		}

		if(typeof event_breadCrumb_click === "function")
			event_breadCrumb_click(zillaid,upazilaid,0);

	});

	//Generate bread crumbs first time
	generateBreadCrumbs();

});



var map_animation_controller = function(){

	if(anim_pos>=animateMap.length )
		anim_pos = 0;

	map_parameters.zilla= animateMap[anim_pos].zilla;
	map_parameters.upazila= 0;

	if (map != null) {
		map.remove();
		getMap(map_parameters);
	}
	//console.log(anim_pos);

	anim_pos = anim_pos +1;
	generateBreadCrumbs();

};

function preloader(status) {
	if(status == 0)
		$("#pre-loader").addClass("hide");
	else
		$("#pre-loader").removeClass("hide");
}