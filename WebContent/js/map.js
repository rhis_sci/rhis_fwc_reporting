/**
 * Created by masum.billah on 10/18/2017.
 */

//$("body").disableSelection();
$(document).ajaxStart(function () {
    NProgress.start();
    setTimeout(function (){NProgress.done();},10000);
}).ajaxStop(function () {
    NProgress.done();
    NProgress.remove();
});
function numberWithCommas(x) {
    return (Math.ceil(x)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

// Return today's date and time
var currentTime = new Date();

// returns the month (from 0 to 11)
var month = monthNames[currentTime.getMonth()];

// returns the day of the month (from 1 to 31)
var day = currentTime.getDate();

// returns the year (four digits)
var year = currentTime.getFullYear();

var geoInfo = new Object();
geoInfo.zilla = 36;
geoInfo.upazila = 0;
geoInfo.type = "1";
$.ajax({
    'type': "POST",
    'url': 'dashboardData',
    'data': {"geoInfo":JSON.stringify(geoInfo)},
    'dataType': "json",
    'success': function (data) {
        console.log("Full Data Information");
        console.log(data);
    }
});

var upazila_mapinfo = (function () {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': 'json/Upazila_MapInfo.json',
        'dataType': "json",
        'success': function (data) {
            json = data;
        }
    });
    return json;
})();

console.log(upazila_mapinfo);

var population_data = (function () {
    var json = null;
    var geoInfo = new Object();
    geoInfo.zilla = 36;
    geoInfo.upazila = 0;
    geoInfo.type = "0";
    $.ajax({
        'type': "POST",
        'async': false,
        'global': false,
        'url': 'mapInfo',
        'data': {"geoInfo":JSON.stringify(geoInfo)},
        'dataType': "json",
        'success': function (data) {
            json = data;
        }
    });
    return json;
})();
console.log("population_data");
console.log(population_data);

var zilla_list_info = $("#zilla_list").text().split(",");
console.log(zilla_list_info);

var union_data = "";
var upazila_data = "";
var zilla_data = "";

var date_value = new Date();

function load_dis(){
    $(".map_heading").text("Map of Bangladesh");
    hideTooltip();
    $(".zilla_legend").show();
    $(".union_legend").hide();
    //Map dimensions (in pixels)
    var width = 426,
        height = 600;

    //Map projection
    var projection = d3.geo.mercator()
        .scale(123262.83313561068)
        .center([90.56198764383417,23.348253082500126]) //projection center
        .translate([width/2,height/2]) //translate to center the map in view

    //Generate paths based on projection
    var path = d3.geo.path()
        .projection(projection);

    //Create an SVG
    var svg = d3.select("map").append("svg")
        .attr("width", width)
        .attr("height", height);

    //Group for the map features
    var features = svg.append("g")
        .attr("class","features");


    d3.json("json/District_Map.geojson",function(error,geodata) {
        if (error) return console.log(error); //unknown error, check the console

        $('#loading_div').html("<img src='image/loading.gif' />");
        $('#loading_div').show();
        hideGif();
        NProgress.start();
        NProgress.set(0.4);
        setTimeout(function () {
            //var zilla_data = (function () {
            var json = null;
            var geoInfo = new Object();
            geoInfo.zilla = 36;
            geoInfo.upazila = 0;
            geoInfo.type = "1";
            geoInfo.methodType = "1";
            $.ajax({
                type: "POST",
                async: true,
                global: false,
                url: 'mapInfo',
                data: {"geoInfo":JSON.stringify(geoInfo)},
                dataType: "json",
                success: function (data) {
                    zilla_data = data;
                    console.log("zilla_data");
                    console.log(zilla_data);
                },
                complete: function() {
                    $('#loading_div').html("");
                    $('#loading_div').hide();
                    NProgress.done();
                    NProgress.remove();
                },

            });
            //return json;
            //})();
            //return json;
        }, 1000);

        /*zilla_data = (function () {
         var json = null;
         var geoInfo = new Object();
         geoInfo.zilla = 36;
         geoInfo.upazila = 0;
         geoInfo.type = "1";
         $.ajax({
         'type': "POST",
         'async': false,
         'global': false,
         'url': 'mapInfo',
         'data': {"geoInfo":JSON.stringify(geoInfo)},
         'dataType': "json",
         'success': function (data) {
         json = data;
         }
         });
         return json;
         })();*/

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

            var tapped=false
            //Create a path for each map feature in the data
            features.selectAll("path")
                .data(geodata.features)
                .enter()
                .append("path")
                .attr("d",path)
                .on("mouseover",showTooltip)
                .on("click",showFixedTooltip)
                .style("fill", function(d){
                    if(d.properties.DIVID==10)
                        var color="#f39dc2";
                    else if(d.properties.DIVID==20)
                        var color="#c56340";
                    else if(d.properties.DIVID==30)
                        var color="#fcdba8";
                    else if(d.properties.DIVID==40)
                        var color="#81c242";
                    else if(d.properties.DIVID==50)
                        var color="#a085b2";
                    else if(d.properties.DIVID==55)
                        var color="#e1cf3d";
                    else if(d.properties.DIVID==60)
                        var color="#fef58e";
                    else if(d.properties.DIVID==99)
                        var color="#00daff";

                    //if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93) var color="#00ff14";
                    //else if(d.properties.OBJECTID==93) var color="#00ff14";

                    return color;
                })
                //.on("mousemove",moveTooltip)
                .on("mousemove",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#d38fad";
                        else if(d.properties.DIVID==20)
                            var color="#b56346";
                        else if(d.properties.DIVID==30)
                            var color="#d8bf97";
                        else if(d.properties.DIVID==40)
                            var color="#78a84a";
                        else if(d.properties.DIVID==50)
                            var color="#856e93";
                        else if(d.properties.DIVID==55)
                            var color="#d6cf7e";
                        else if(d.properties.DIVID==60)
                            var color="#c1b449";
                        else if(d.properties.DIVID==99)
                            var color="#16b3ce";

                        //if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93) var color="#6d6726";

                        return color;
                    });
                })
                .on("mouseout",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#f39dc2";
                        else if(d.properties.DIVID==20)
                            var color="#c56340";
                        else if(d.properties.DIVID==30)
                            var color="#fcdba8";
                        else if(d.properties.DIVID==40)
                            var color="#81c242";
                        else if(d.properties.DIVID==50)
                            var color="#a085b2";
                        else if(d.properties.DIVID==55)
                            var color="#e1cf3d";
                        else if(d.properties.DIVID==60)
                            var color="#fef58e";
                        else if(d.properties.DIVID==99)
                            var color="#00daff";

                        //if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93) var color="#00ff14";

                        return color;
                    });
                    hideTooltip();
                })
                .on("touchstart", function(d){
                    if(!tapped){
                        tapped=setTimeout(function(){
                            tapped=null
                        },300); //wait 300ms
                    } else {
                        clearTimeout(tapped);
                        tapped=null
                        clicked(d);
                    }
                });
        }
        else{
            //Create a path for each map feature in the data
            features.selectAll("path")
                .data(geodata.features)
                .enter()
                .append("path")
                .attr("d",path)
                .on("mouseover",showTooltip)
                .on("click",showFixedTooltip)
                .style("fill", function(d){
                    if(d.properties.DIVID==10)
                        var color="#f39dc2";
                    else if(d.properties.DIVID==20)
                        var color="#c56340";
                    else if(d.properties.DIVID==30)
                        var color="#fcdba8";
                    else if(d.properties.DIVID==40)
                        var color="#81c242";
                    else if(d.properties.DIVID==50)
                        var color="#a085b2";
                    else if(d.properties.DIVID==55)
                        var color="#e1cf3d";
                    else if(d.properties.DIVID==60)
                        var color="#fef58e";
                    else if(d.properties.DIVID==99)
                        var color="#00daff";

                    //if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93) var color="#00ff14";
                    //else if(d.properties.OBJECTID==93) var color="#00ff14";

                    return color;
                })
                //.on("mousemove",moveTooltip)
                .on("mousemove",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#d38fad";
                        else if(d.properties.DIVID==20)
                            var color="#b56346";
                        else if(d.properties.DIVID==30)
                            var color="#d8bf97";
                        else if(d.properties.DIVID==40)
                            var color="#78a84a";
                        else if(d.properties.DIVID==50)
                            var color="#856e93";
                        else if(d.properties.DIVID==55)
                            var color="#d6cf7e";
                        else if(d.properties.DIVID==60)
                            var color="#c1b449";
                        else if(d.properties.DIVID==99)
                            var color="#16b3ce";

                        //if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93) var color="#6d6726";

                        return color;
                    });
                })
                .on("mouseout",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#f39dc2";
                        else if(d.properties.DIVID==20)
                            var color="#c56340";
                        else if(d.properties.DIVID==30)
                            var color="#fcdba8";
                        else if(d.properties.DIVID==40)
                            var color="#81c242";
                        else if(d.properties.DIVID==50)
                            var color="#a085b2";
                        else if(d.properties.DIVID==55)
                            var color="#e1cf3d";
                        else if(d.properties.DIVID==60)
                            var color="#fef58e";
                        else if(d.properties.DIVID==99)
                            var color="#00daff";

                        //if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93) var color="#00ff14";

                        return color;
                    });
                    hideTooltip();
                })
                .on("dblclick",clicked);
        }


        features.selectAll("text")
            .data(geodata.features)
            .enter()
            .append("svg:text")
            .text(function(d){
                if(d.properties.OBJECTID=="6" || d.properties.OBJECTID=="15" || d.properties.OBJECTID=="26" || d.properties.OBJECTID=="47" || d.properties.OBJECTID=="61" || d.properties.OBJECTID=="81" || d.properties.OBJECTID=="85" || d.properties.OBJECTID=="91")
                    return d.properties.DISTNAME;
            })
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            })
            .attr("text-anchor","middle")
            .attr('font-size','15pt')
            .attr('font-weight','bold');

        features.selectAll(".mark")
            .data(geodata.features)
            .enter()
            .append("image")
            .attr('class','mark')
            .attr('width', 20)
            .attr('height', 20)
            .attr("xlink:href",function(d){
                //if(d.properties.OBJECTID=="12" || d.properties.OBJECTID=="13" || d.properties.OBJECTID=="29" || d.properties.OBJECTID=="30" || d.properties.OBJECTID=="36" || d.properties.OBJECTID=="42" || d.properties.OBJECTID=="50" || d.properties.OBJECTID=="51" || d.properties.OBJECTID=="56" || d.properties.OBJECTID=="75" || d.properties.OBJECTID=="93")
                if(zilla_list_info.includes(d.properties.OBJECTID))
                    return 'image/Implemented_Area.gif';
            })
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            });
        //.attr("transform", function(d) {return "translate(" + x + "," + y + ")";});

    });
}

function load_upz(get_dis_name, dist_id){
    $(".map_heading").text("Upazila Map of "+get_dis_name+" District");
    hideTooltip();
    $(".zilla_legend").hide();
    $(".union_legend").show();
    $(".prev_dis").show();
    $(".prev_upz").hide();

    $('#loading_div').html("<img src='image/loading.gif' />");
    $('#loading_div').show();
    hideGif();

    setTimeout(function () {
        //upazila_data = (function () {
        //var json = null;
        var geoInfo = new Object();
        geoInfo.zilla = dist_id;
        geoInfo.upazila = 0;
        geoInfo.type = "2";
        geoInfo.methodType = "1";
        $.ajax({
            'type': "POST",
            'async': true,
            'global': false,
            'url': 'mapInfo',
            'data': {"geoInfo":JSON.stringify(geoInfo)},
            'dataType': "json",
            success: function (data) {
                upazila_data = data;
                console.log(upazila_data);
                $('#loading_div').html("");
                $('#loading_div').hide();
            }
        });
        //return json;
        //})();
    }, 100);
    //Map dimensions (in pixels)
    var width = 426,
        height = 600;

    var scale;
    var latitude;
    var longitude;

    $.each(upazila_mapinfo[dist_id], function(i, item) {
        //alert(upazila_data[key][i]);
        //info = info + i + " : " + upazila_data[key][i] + "<br>";
        if(i=="scale")
            scale = upazila_mapinfo[dist_id][i];
        else if(i=="latitude")
            latitude = upazila_mapinfo[dist_id][i];
        else if(i=="longitude")
            longitude = upazila_mapinfo[dist_id][i];
    });

    //Map projection
    var projection = d3.geo.mercator()
        .scale(scale)
        .center([latitude,longitude]) //projection center
        .translate([width/2,height/2]) //translate to center the map in view

    //Generate paths based on projection
    var path = d3.geo.path()
        .projection(projection);

    //Create an SVG
    var svg = d3.select("map").append("svg")
        .attr("width", width)
        .attr("height", height);

    //Group for the map features
    var features = svg.append("g")
        .attr("class","features");


    d3.json("json/"+get_dis_name+"/"+get_dis_name+"_Upazila.geojson",function(error,geodata) {
        if (error) return console.log(error); //unknown error, check the console

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            var tapped=false
            //Create a path for each map feature in the data
            features.selectAll("path")
                .data(geodata.features)
                .enter()
                .append("path")
                .attr("d",path)
                .on("mouseover",showTooltip)
                .style("fill", "#dadbc0")
                //.on("mousemove",moveTooltip)
                .on("mousemove", function(){d3.select(this).style("fill", "#99875e")})
                .on("mouseout",function(){d3.select(this).style("fill", "#dadbc0"); hideTooltip();})
                .on("touchstart", function(d){
                    if(upazila_data[d.properties.MEAN_DISTC+"_"+d.properties.MEAN_UZCOD].hasOwnProperty("Upazila Name")) {
                        if (!tapped) {
                            tapped = setTimeout(function () {
                                tapped = null
                            }, 300); //wait 300ms
                        } else {
                            clearTimeout(tapped);
                            tapped = null
                            clicked_upz(d);
                        }
                    }
                });
        }
        else{
            //Create a path for each map feature in the data
            features.selectAll("path")
                .data(geodata.features)
                .enter()
                .append("path")
                .attr("d",path)
                .on("mouseover",showTooltip)
                .style("fill", "#dadbc0")
                //.on("mousemove",moveTooltip)
                .on("mousemove", function(){d3.select(this).style("fill", "#99875e")})
                .on("mouseout",function(){d3.select(this).style("fill", "#dadbc0"); hideTooltip();})
                .on("dblclick", function (d) {console.log(upazila_data);console.log(d.properties.MEAN_DISTC+"_"+d.properties.MEAN_UZCOD);
                    if(upazila_data.hasOwnProperty(d.properties.MEAN_DISTC+"_"+d.properties.MEAN_UZCOD))
                        clicked_upz(d);
                });
        }

        features.selectAll("text")
            .data(geodata.features)
            .enter()
            .append("svg:text")
            .text(function(d){
                return d.properties.UPAZILA;
            })
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            })
            .attr("text-anchor","middle")
            .attr('font-size','10pt')
            .attr('font-weight','bold')
            .on("mouseover",showTooltip)
            .on("mouseout",hideTooltip);

        /*var marks = [{long: 91.32249, lat: 24.64563},{long: 91.28054, lat: 24.56505},{long: 91.19942, lat: 24.50716}];

         features.selectAll(".mark")
         .data(marks)
         .enter()
         .append("image")
         .attr('class','mark')
         .attr('width', 7)
         .attr('height', 10)
         .attr("xlink:href",'https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/24x24/DrawingPin1_Blue.png')
         .attr("transform", function(d) {return "translate(" + projection([d.long,d.lat]) + ")";});*/

        //var marks = upazila_data["Facility_info"];
        var facility_info = (function () {
            var json = null;
            var geoInfo = new Object();
            geoInfo.zilla = dist_id;
            geoInfo.upazila = 0;
            geoInfo.type = "4";
            $.ajax({
                'type': "POST",
                'async': false,
                'global': false,
                'url': 'mapInfo',
                'data': {"geoInfo":JSON.stringify(geoInfo)},
                'dataType': "json",
                'success': function (data) {
                    json = data;
                }
            });
            return json;
        })();
        var marks = facility_info["Facility_info"];
        console.log(marks);

        features.selectAll(".mark")
            .data(marks)
            .enter()
            .append("image")
            .attr('class','mark')
            .attr('width', 20)
            .attr('height', 20)
            .attr("xlink:href",function(d) {
                if(d.facility_category=="A")
                    var image = 'image/FWC_CAT_A.png';
                else if(d.facility_category=="B")
                    var image = 'image/FWC_CAT_B.png';
                else if(d.facility_category=="C")
                    var image = 'image/FWC_CAT_C.png';
                else{
                    if(d.facility_type == 'UHC')
                        var image = 'image/UHC.png';
                    else
                        var image = '';
                }
                return image;
            })
            //.attr("xlink:href",'https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/24x24/DrawingPin1_Blue.png')
            .attr("transform", function(d) {return "translate(" + projection([d.lon,d.lat]) + ")";})
            .on("mouseover",showTooltipFacility)
            .on("mouseout",hideTooltipFacility);

    });
}

function load_union(get_upazila_name, zillaid, upazilaid){
    $(".map_heading").text("Union Map of " + get_upazila_name + " Upazila");
    hideTooltip();
    $(".zilla_legend").hide();
    $(".union_legend").show();
    $(".prev_dis").show();
    $(".prev_upz").show();

    $('#loading_div').html("<img src='image/loading.gif' />");
    $('#loading_div').show();
    hideGif();

    setTimeout(function () {
        //union_data = (function () {
        //var json = null;
        var geoInfo = new Object();
        geoInfo.zilla = zillaid;
        geoInfo.upazila = upazilaid;
        geoInfo.type = "3";
        geoInfo.methodType = "1";
        $.ajax({
            'type': "POST",
            'async': true,
            'global': false,
            'url': 'mapInfo',
            'data': {"geoInfo":JSON.stringify(geoInfo)},
            'dataType': "json",
            'success': function (data) {
                union_data = data;
                console.log(union_data);
                $('#loading_div').html("");
                $('#loading_div').hide();
            }
        });
        //return json;
        //})();
    }, 100);
    //Map dimensions (in pixels)
    var width = 600,
        height = 600;

    var scale;
    var latitude;
    var longitude;

    $.each(upazila_mapinfo[zillaid+"_"+upazilaid], function(i, item) {
        //alert(upazila_data[key][i]);
        //info = info + i + " : " + upazila_data[key][i] + "<br>";
        if(i=="scale")
            scale = upazila_mapinfo[zillaid+"_"+upazilaid][i];
        else if(i=="latitude")
            latitude = upazila_mapinfo[zillaid+"_"+upazilaid][i];
        else if(i=="longitude")
            longitude = upazila_mapinfo[zillaid+"_"+upazilaid][i];
    });

    //alert(scale + "_" + latitude + "_" + longitude);

    //Map projection
    var projection = d3.geo.mercator()
        .scale(scale)
        .center([latitude,longitude]) //projection center
        .translate([width/2,height/2]) //translate to center the map in view

    //Generate paths based on projection
    var path = d3.geo.path()
        .projection(projection);

    //Create an SVG
    var svg = d3.select("map").append("svg")
        .attr("width", width)
        .attr("height", height);

    //Group for the map features
    var features = svg.append("g")
        .attr("class","features");


    d3.json("json/"+dist_name+"/"+get_upazila_name+".geojson",function(error,geodata) {
        if (error) return console.log(error); //unknown error, check the console

        //Create a path for each map feature in the data
        features.selectAll("path")
            .data(geodata.features)
            .enter()
            .append("path")
            .attr("d",path)
            .on("mouseover",showTooltip)
            .style("fill", "#dadbc0")
            //.style("fill", function(d){if(d.properties.Category=="A") var color="#4ac63f"; else if(d.properties.Category=="B") var color="#fcf823" ; else if(d.properties.Category=="C") var color="#ff6868" ; else var color="white"; return color;})
            //.on("mousemove",moveTooltip)
            //.on("mousemove", function(){d3.select(this).style("fill", function(d){if(d.properties.Category=="A") var color="#62ef77"; else if(d.properties.Category=="B") var color="#ccc93d" ; else if(d.properties.Category=="C") var color="#c64f4f" ; else var color="#cad1cb"; return color;})})
            //.on("mouseout",function(){d3.select(this).style("fill", function(d){if(d.properties.Category=="A") var color="#4ac63f"; else if(d.properties.Category=="B") var color="#fcf823" ; else if(d.properties.Category=="C") var color="#ff6868" ; else var color="white"; return color;}); hideTooltip();})
            .on("mousemove", function(){d3.select(this).style("fill", "#99875e")})
            .on("mouseout",function(){d3.select(this).style("fill", "#dadbc0"); hideTooltip();});

        features.selectAll("text")
            .data(geodata.features)
            .enter()
            .append("svg:text")
            .text(function(d){
                console.log(d.properties.UNINAME);
                return d.properties.UNINAME;
            })
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            })
            .attr("text-anchor","middle")
            .attr('font-size','10pt')
            .attr('font-weight','bold')
            .on("mouseover",showTooltip)
            .on("mouseout",hideTooltip);

        //var marks = union_data["Facility_info"];

        var facility_info = (function () {
            var json = null;
            var geoInfo = new Object();
            geoInfo.zilla = dist_id;
            geoInfo.upazila = upazilaid;
            geoInfo.type = "4";
            $.ajax({
                'type': "POST",
                'async': false,
                'global': false,
                'url': 'mapInfo',
                'data': {"geoInfo":JSON.stringify(geoInfo)},
                'dataType': "json",
                'success': function (data) {
                    json = data;
                }
            });
            return json;
        })();
        var marks = facility_info["Facility_info"];

        features.selectAll(".mark")
            .data(marks)
            .enter()
            .append("image")
            .attr('class','mark')
            .attr('width', 20)
            .attr('height', 20)
            .attr("xlink:href",function(d) {
                if(d.facility_category=="A")
                    var image = 'image/FWC_CAT_A.png';
                else if(d.facility_category=="B")
                    var image = 'image/FWC_CAT_B.png';
                else if(d.facility_category=="C")
                    var image = 'image/FWC_CAT_C.png';
                else{
                    if(d.facility_type == 'UHC')
                        var image = 'image/UHC.png';
                    else
                        var image = '';
                }
                return image;
            })
            .attr("transform", function(d) {return "translate(" + projection([d.lon,d.lat]) + ")";})
            .on("mouseover",showTooltipFacility)
            .on("mouseout",hideTooltipFacility);

    });
}

var type_name = "dis";
var dist_id;
var dist_name;
var upz_id;
var upz_name;
load_dis();

//Create a tooltip, hidden at the start
var tooltip = d3.select("map").append("div").attr("class","tooltip");

// Add optional onClick events for features here
// d.properties contains the attributes (e.g. d.properties.name, d.properties.population)
function clicked(d) {
    if(d.properties.OBJECTID==12 || d.properties.OBJECTID==13 || d.properties.OBJECTID==29 || d.properties.OBJECTID==30 || d.properties.OBJECTID==36 || d.properties.OBJECTID==42 || d.properties.OBJECTID==50 || d.properties.OBJECTID==51 || d.properties.OBJECTID==56 || d.properties.OBJECTID==75 || d.properties.OBJECTID==93){
        $("svg").remove();
        type_name = "upz";
        dist_id = d.properties.OBJECTID;
        dist_name = d.properties.DISTNAME;
        load_upz(d.properties.DISTNAME, d.properties.OBJECTID);
    }
}

function clicked_upz(d) {
    if(dist_id==12 || dist_id==13 || dist_id==29 || dist_id==30 || dist_id==36 || dist_id==42 || dist_id==50 || dist_id==51 || dist_id==56 || dist_id==75 || dist_id==93){
        $("svg").remove();
        type_name = "union";
        upz_id = d.properties.MEAN_UZCOD;
        upz_name = d.properties.UPAZILA;
        load_union(d.properties.UPAZILA, d.properties.MEAN_DISTC, d.properties.MEAN_UZCOD);
    }
}

function get_data_zilla(key,zilla_name){
    var info = "";
    // if(key=="12" || key=="13" || key=="29" || key=="30" || key=="36" || key=="42" || key=="50" || key=="51" || key=="56" || key=="75" || key=="93"){
    if(zilla_list_info.includes(key)){
        //info = info + "Service Statistics of "+month+", "+year+"<br /><br />";

        //info = info + "District Name : " + zilla_name + "<br>";

        //info = info + "<div class='col-md-12'><div class='small-box bg-yellow'><div class='inner'><h4>" + numberWithCommas(population_data[key]["Total"]) + "</h4><p>Total Population</p></div><div class='icon'><i class='fa fa-users'></i></div></div></div></div>"
        //info = info + "Total Population : " + numberWithCommas(population_data[key]["Total"]) + "<br>";
        //info = info + "Male : " + numberWithCommas(population_data[key]["Male"]) + "<br>";
        //info = info + "Female : " + numberWithCommas(population_data[key]["Female"]) + "<br>";
        //info = info + "<div class='row'><div class='col-md-2'><img width='50' src='image/couple.png'></div><div class='col-md-10'><div class='clearfix'><span class='pull-left'>ELCO</span><small class='pull-right badge bg-blue'><b>" + numberWithCommas(Math.round(population_data[key]["Total"]*(21/100))) + "</b></small></div><div class='progress progress-xs progress-striped active'><div class='progress-bar progress-bar-primary' style='width: 100%'></div></div></div></div>";
        //info = info + "ELCO : " + numberWithCommas(Math.round(population_data[key]["Total"]*(21/100))) + "<br><br>";

        //info = info + "<div class='row'><div class='col-md-12'><div class='alert alert-info' style='text-align:center;'>Service Statistics of <strong>"+month+", "+year+"</strong></div></div></div>";

        //info = info + "Delivery : " + ((zilla_data[key]["Delivery"]!="")?zilla_data[key]["Delivery"]:"0") + "<br>";
        //info = info + "<div class='row'><div class='col-md-4' id='anc_chart'></div><div class='col-md-4' id='pnc_chart'></div><div class='col-md-4' id='pncn_chart'></div></div>";
        info = info + "<div class='row'><div class='col-md-12'><div class='info-box info-box3 bg-green'><span class='info-box-icon_button' style='height:55px;font-size:40px;line-height:55px;margin-bottom:0px;'><i class='fa fa-location-arrow'></i></span><div class='info-box-content_button'><span class='info-box-text'><strong style='font-size:16px;'>" + zilla_name + "</strong></span><div class='progress' style='margin-top:0px;margin-bottom:0px;'><div class='progress-bar' style='width: 100%'></div></div><span class='progress-description'># of Upazila "+zilla_data[key]["Upazila_count"]+", # of Unions "+zilla_data[key]["Union_count"]+"</span></div></div></div>";
        if(population_data[key].hasOwnProperty("Total"))
            info = info + "<div class='col-md-12' style='margin-top: -15px;'><div class='info-box info-box3 bg-yellow'><span class='info-box-icon_button' style='height:82px;font-size:45px;line-height:82px;margin-bottom:0px;'><i class='fa fa-users'></i></span><div class='info-box-content_button'><span class='info-box-text' style='font-size:16px;'>Total Population<strong class='pull-right' style='font-size:16px;'>" + numberWithCommas(population_data[key]["Total"]) + "</strong></span><div class='progress' style='margin-top:0px;margin-bottom:0px;'><div class='progress-bar' style='width: 100%'></div></div><span class='progress-description' style='font-size:16px;'><img width='25' src='image/male.png'>  "+numberWithCommas(population_data[key]["Male"])+" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class='pull-right'><img width='25' src='image/couple.png'>  " + numberWithCommas(Math.round(population_data[key]["Total"]*(26.7/100))) + "</strong> <br><img width='25' src='image/female.png'>  "+numberWithCommas(population_data[key]["Female"])+"</span></div></div></div>";
        else
            info = info + "<div class='col-md-12' style='margin-top: -15px;'><div class='info-box info-box3 bg-yellow'><span class='info-box-icon_button' style='height:82px;font-size:45px;line-height:82px;margin-bottom:0px;'><i class='fa fa-users'></i></span><div class='info-box-content_button'><span class='info-box-text' style='font-size:16px;'>Total Population<strong class='pull-right' style='font-size:16px;'>" + numberWithCommas(population_data[key]["Total"]) + "</strong></span><div class='progress' style='margin-top:0px;margin-bottom:0px;'><div class='progress-bar' style='width: 100%'></div></div><span class='progress-description' style='font-size:16px;'><img width='25' src='image/male.png'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class='pull-right'><img width='25' src='image/couple.png'></strong> <br><img width='25' src='image/female.png'></span></div></div></div>";
        info = info + "<div class='col-md-12' style='margin-top: -15px;'><div class='info-box info-box3 bg-aqua'><span class='info-box-icon_button' style='height:55px;font-size:40px;line-height:55px;margin-bottom:0px;'><i class='fa fa-bar-chart'></i></span><div class='info-box-content_button'><span class='info-box-text' style='text-align: center; margin-top: 2.5%;'><strong style='font-size:16px;'>Service Statistics of "+month+", "+year+"</strong></span></div></div></div></div>";
        info = info + "<div class='row'><div class='col-md-2'><img width='50' src='image/delivery_newborn.png'></div><div class='col-md-10'><div class='clearfix'><span class='pull-left'>Delivery</span><small class='pull-right badge bg-blue'><b>" + ((zilla_data[key]["Delivery"]!="")?zilla_data[key]["Delivery"]:"0") + "</b></small></div><div class='progress progress-xs progress-striped active'><div class='progress-bar progress-bar-primary' style='width: 100%'></div></div></div></div>";
        info = info + "<div class='row'><div class='col-md-12'><table class='table table-bordered'><tr class='active'><th></th><th>Visit1</th><th>Visit2</th><th>Visit3</th><th>Visit4</th></tr><tr class='info'>"
        info = info + "<td><b>" + "ANC" + " :</b></td><td>" + ((zilla_data[key]["ANC1"]!="")?zilla_data[key]["ANC1"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["ANC2"]!="")?zilla_data[key]["ANC2"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["ANC3"]!="")?zilla_data[key]["ANC3"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["ANC4"]!="")?zilla_data[key]["ANC4"]:"0") + "</td></tr>";
        info = info + "<tr class='success'><td><b>" + "PNC(M)" + " :</b></td><td>" + ((zilla_data[key]["PNC1-M"]!="")?zilla_data[key]["PNC1-M"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["PNC2-M"]!="")?zilla_data[key]["PNC2-M"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["PNC3-M"]!="")?zilla_data[key]["PNC3-M"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["PNC4-M"]!="")?zilla_data[key]["PNC4-M"]:"0") + "</td></tr>";
        info = info + "<tr class='warning'><td><b>" + "PNC(N)" + " :</b></td><td>" + ((zilla_data[key]["PNC1-N"]!="")?zilla_data[key]["PNC1-N"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["PNC2-N"]!="")?zilla_data[key]["PNC2-N"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["PNC3-N"]!="")?zilla_data[key]["PNC3-N"]:"0") + "</td>";
        info = info + "<td>" + ((zilla_data[key]["PNC4-N"]!="")?zilla_data[key]["PNC4-N"]:"0") + "</td></tr></table></div></div>";

    }
    else
        info = info + zilla_name;

    return info;
}

function get_data_upz(key, get_upz_name){
    // console.log(key+"-"+get_upz_name);
    var info = "";
    //var upazila_name = "";
    //$.each(upazila_data[key], function(i, item) {
    //alert(upazila_data[key][i]);
    //info = info + i + " : " + upazila_data[key][i] + "<br>";
    if(dist_id==12 || dist_id==13 || dist_id==29 || dist_id==30 || dist_id==36 || dist_id==42 || dist_id==50 || dist_id==51 || dist_id==56 || dist_id==75 || dist_id==93){
        if(upazila_data[key].hasOwnProperty("Upazila Name")) {
            info = info + "<div class='row'><div class='col-md-12'><div class='info-box info-box3 bg-green'><span class='info-box-icon_button' style='height:55px;font-size:40px;line-height:55px;margin-bottom:0px;'><i class='fa fa-location-arrow'></i></span><div class='info-box-content_button'><span class='info-box-text'><strong style='font-size:16px;'>" + upazila_data[key]["Upazila Name"] + "</strong></span><div class='progress' style='margin-top:0px;margin-bottom:0px;'><div class='progress-bar' style='width: 100%'></div></div><span class='progress-description'># of Unions " + upazila_data["Upazila_info"][key] + "</span></div></div></div>"
            info = info + "<div class='col-md-12' style='margin-top: -15px;'><div class='info-box info-box3 bg-yellow'><span class='info-box-icon_button' style='height:82px;font-size:45px;line-height:82px;margin-bottom:0px;'><i class='fa fa-users'></i></span><div class='info-box-content_button'><span class='info-box-text' style='font-size:16px;'>Total Population<strong class='pull-right' style='font-size:16px;'>" + numberWithCommas(population_data[key]["Total"]) + "</strong></span><div class='progress' style='margin-top:0px;margin-bottom:0px;'><div class='progress-bar' style='width: 100%'></div></div><span class='progress-description' style='font-size:16px;'><img width='25' src='image/male.png'>  " + numberWithCommas(population_data[key]["Male"]) + " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class='pull-right'><img width='25' src='image/couple.png'>  " + numberWithCommas(Math.round(population_data[key]["Total"] * (26.7 / 100))) + "</strong> <br><img width='25' src='image/female.png'>  " + numberWithCommas(population_data[key]["Female"]) + "</span></div></div></div>"
            info = info + "<div class='col-md-12' style='margin-top: -15px;'><div class='info-box info-box3 bg-aqua'><span class='info-box-icon_button' style='height:55px;font-size:40px;line-height:55px;margin-bottom:0px;'><i class='fa fa-bar-chart'></i></span><div class='info-box-content_button'><span class='info-box-text' style='text-align: center; margin-top: 2.5%;'><strong style='font-size:16px;'>Service Statistics of " + month + ", " + year + "</strong></span></div></div></div></div>"
            info = info + "<div class='row'><div class='col-md-2'><img width='50' src='image/delivery_newborn.png'></div><div class='col-md-10'><div class='clearfix'><span class='pull-left'>Delivery</span><small class='pull-right badge bg-blue'><b>" + ((upazila_data[key]["Delivery"] != "") ? upazila_data[key]["Delivery"] : "0") + "</b></small></div><div class='progress progress-xs progress-striped active'><div class='progress-bar progress-bar-primary' style='width: 100%'></div></div></div></div>";
            info = info + "<div class='row'><div class='col-md-12'><table class='table table-bordered'><tr class='active'><th></th><th>Visit1</th><th>Visit2</th><th>Visit3</th><th>Visit4</th></tr><tr class='info'>"
            info = info + "<td><b>" + "ANC" + " :</b></td><td>" + ((upazila_data[key]["ANC1"] != "") ? upazila_data[key]["ANC1"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["ANC2"] != "") ? upazila_data[key]["ANC2"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["ANC3"] != "") ? upazila_data[key]["ANC3"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["ANC4"] != "") ? upazila_data[key]["ANC4"] : "0") + "</td></tr>";
            info = info + "<tr class='success'><td><b>" + "PNC(M)" + " :</b></td><td>" + ((upazila_data[key]["PNC1-M"] != "") ? upazila_data[key]["PNC1-M"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["PNC2-M"] != "") ? upazila_data[key]["PNC2-M"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["PNC3-M"] != "") ? upazila_data[key]["PNC3-M"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["PNC4-M"] != "") ? upazila_data[key]["PNC4-M"] : "0") + "</td></tr>";
            info = info + "<tr class='warning'><td><b>" + "PNC(N)" + " :</b></td><td>" + ((upazila_data[key]["PNC1-N"] != "") ? upazila_data[key]["PNC1-N"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["PNC2-N"] != "") ? upazila_data[key]["PNC2-N"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["PNC3-N"] != "") ? upazila_data[key]["PNC3-N"] : "0") + "</td>";
            info = info + "<td>" + ((upazila_data[key]["PNC4-N"] != "") ? upazila_data[key]["PNC4-N"] : "0") + "</td></tr></table></div></div>";
        }
        else
            info = info + "eMIS System is not implemented yet.";
    }
    else
        info = info + get_upz_name;

    return info;
}

function get_data_union(key, get_union_name, get_facility_name, get_facility_type){
    // console.log(key+" "+get_union_name+" "+get_union_name+get_facility_name+" "+get_union_name+get_facility_type);
    var info = "";
    //var union_info = "";

    info = info + "<div class='row'><div class='col-md-12'><div class='info-box info-box3 bg-green'><span class='info-box-icon_button' style='height:55px;font-size:40px;line-height:55px;margin-bottom:0px;'><i class='fa fa-location-arrow'></i></span><div class='info-box-content_button'><span class='info-box-text'><strong style='font-size:16px;'>" + get_union_name + "</strong></span><div class='progress' style='margin-top:0px;margin-bottom:0px;'><div class='progress-bar' style='width: 100%'></div></div><span class='progress-description'>"+get_facility_name+" ("+get_facility_type+")</span></div></div></div>"

    if(union_data.hasOwnProperty(key)){
        info = info + "<div class='col-md-12' style='margin-top: -15px;'><div class='info-box info-box3 bg-yellow'><span class='info-box-icon_button' style='height:82px;font-size:45px;line-height:82px;margin-bottom:0px;'><i class='fa fa-users'></i></span><div class='info-box-content_button'><span class='info-box-text' style='font-size:16px;'>Total Population<strong class='pull-right' style='font-size:16px;'>" + numberWithCommas(population_data[key]["Total"]) + "</strong></span><div class='progress' style='margin-top:0px;margin-bottom:0px;'><div class='progress-bar' style='width: 100%'></div></div><span class='progress-description' style='font-size:16px;'><img width='25' src='image/male.png'>  "+numberWithCommas(population_data[key]["Male"])+" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class='pull-right'><img width='25' src='image/couple.png'>  " + numberWithCommas(Math.round(population_data[key]["Total"]*(26.7/100))) + "</strong> <br><img width='25' src='image/female.png'>  "+numberWithCommas(population_data[key]["Female"])+"</span></div></div></div>"
        info = info + "<div class='col-md-12' style='margin-top: -15px;'><div class='info-box info-box3 bg-aqua'><span class='info-box-icon_button' style='height:55px;font-size:40px;line-height:55px;margin-bottom:0px;'><i class='fa fa-bar-chart'></i></span><div class='info-box-content_button'><span class='info-box-text' style='text-align: center; margin-top: 2.5%;'><strong style='font-size:16px;'>Service Statistics of "+month+", "+year+"</strong></span></div></div></div></div>"
        info = info + "<div class='row'><div class='col-md-2'><img width='50' src='image/delivery_newborn.png'></div><div class='col-md-10'><div class='clearfix'><span class='pull-left'>Delivery</span><small class='pull-right badge bg-blue'><b>" + ((union_data[key]["Delivery"]!="")?union_data[key]["Delivery"]:"0") + "</b></small></div><div class='progress progress-xs progress-striped active'><div class='progress-bar progress-bar-primary' style='width: 100%'></div></div></div></div>";
        info = info + "<div class='row'><div class='col-md-12'><table class='table table-bordered'><tr class='active'><th></th><th>Visit1</th><th>Visit2</th><th>Visit3</th><th>Visit4</th></tr><tr class='info'>"
        info = info + "<td><b>" + "ANC" + " :</b></td><td>" + ((union_data[key]["ANC1"]!="")?union_data[key]["ANC1"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["ANC2"]!="")?union_data[key]["ANC2"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["ANC3"]!="")?union_data[key]["ANC3"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["ANC4"]!="")?union_data[key]["ANC4"]:"0") + "</td></tr>";
        info = info + "<tr class='success'><td><b>" + "PNC(M)" + " :</b></td><td>" + ((union_data[key]["PNC1-M"]!="")?union_data[key]["PNC1-M"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["PNC2-M"]!="")?union_data[key]["PNC2-M"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["PNC3-M"]!="")?union_data[key]["PNC3-M"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["PNC4-M"]!="")?union_data[key]["PNC4-M"]:"0") + "</td></tr>";
        info = info + "<tr class='warning'><td><b>" + "PNC(N)" + " :</b></td><td>" + ((union_data[key]["PNC1-N"]!="")?union_data[key]["PNC1-N"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["PNC2-N"]!="")?union_data[key]["PNC2-N"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["PNC3-N"]!="")?union_data[key]["PNC3-N"]:"0") + "</td>";
        info = info + "<td>" + ((union_data[key]["PNC4-N"]!="")?union_data[key]["PNC4-N"]:"0") + "</td></tr></table></div></div>";
    }


    /*info = info + "Service Statistics of "+month+", "+year+"<br /><br />";
     info = info + "<b>Union Name :</b> " + get_union_name + "<br>";
     info = info + "<b>Facility Name :</b> " + get_facility_name + "<br>";
     info = info + "<b>Facility Category :</b> " + get_facility_type + "<br>";
     info = info + "Total Population : " + numberWithCommas(population_data[key]["Total"]) + "<br>";
     info = info + "Male : " + numberWithCommas(population_data[key]["Male"]) + "<br>";
     info = info + "Female : " + numberWithCommas(population_data[key]["Female"]) + "<br>";
     info = info + "ELCO : " + numberWithCommas(Math.round(population_data[key]["Total"]*(21/100))) + "<br><br>";
     if(union_data.hasOwnProperty(key)){
     info = info + "Delivery : " + ((union_data[key]["Delivery"]!="")?union_data[key]["Delivery"]:"0") + "<br>";
     info = info + "<table class='table'><tr><th></th><th>Visit1</th><th>Visit2</th><th>Visit3</th><th>Visit4</th></tr><tr>"
     info = info + "<td><b>" + "ANC" + " :</b></td><td>" + ((union_data[key]["ANC1"]!="")?union_data[key]["ANC1"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["ANC2"]!="")?union_data[key]["ANC2"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["ANC3"]!="")?union_data[key]["ANC3"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["ANC4"]!="")?union_data[key]["ANC4"]:"0") + "</td></tr>";
     info = info + "<tr><td><b>" + "PNC(M)" + " :</b></td><td>" + ((union_data[key]["PNC1-M"]!="")?union_data[key]["PNC1-M"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["PNC2-M"]!="")?union_data[key]["PNC2-M"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["PNC3-M"]!="")?union_data[key]["PNC3-M"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["PNC4-M"]!="")?union_data[key]["PNC4-M"]:"0") + "</td></tr>";
     info = info + "<tr><td><b>" + "PNC(N)" + " :</b></td><td>" + ((union_data[key]["PNC1-N"]!="")?union_data[key]["PNC1-N"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["PNC2-N"]!="")?union_data[key]["PNC2-N"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["PNC3-N"]!="")?union_data[key]["PNC3-N"]:"0") + "</td>";
     info = info + "<td>" + ((union_data[key]["PNC4-N"]!="")?union_data[key]["PNC4-N"]:"0") + "</td></tr></table>";

     }*/

    return info;
}


//Position of the tooltip relative to the cursor
var tooltipOffset = {x: 5, y: -25};

//Create a tooltip, hidden at the start
function showTooltip(d) {
    //moveTooltip();
    //alert(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD));
    if(type_name=="dis"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(d.properties.OBJECTID,d.properties.DISTNAME));

    }
    else if(type_name=="upz"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD, d.properties.UPAZILA));
    }
    else{
        //alert(d.properties.Dis_Code + "_" + d.properties.Upa_Code + "_" + d.properties.Union_Code);
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_union(d.properties.Dis_Code + "_" + d.properties.Upa_Code.replace(/^0+/, '') + "_" + d.properties.Union_Code.replace(/^0+/, ''), d.properties.UNINAME, ((d.properties.Facility_N!=null)?d.properties.Facility_N:""), ((d.properties.Category!=null)?d.properties.Category:"")));
    }

    //.text(d.properties.MEAN_DISTC + " , " +d.properties.MEAN_UZCOD);

    $(".chart_view").show();
}

//Create a tooltip, hidden at the start
function showFixedTooltip(d) {
    //moveTooltip();
    //alert(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD));
    if(type_name=="dis"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(d.properties.OBJECTID,d.properties.DISTNAME));

    }
    else if(type_name=="upz"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD, d.properties.UPAZILA));
    }
    else{
        //alert(d.properties.Dis_Code + "_" + d.properties.Upa_Code + "_" + d.properties.Union_Code);
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_union(d.properties.Dis_Code + "_" + d.properties.Upa_Code.replace(/^0+/, '') + "_" + d.properties.Union_Code.replace(/^0+/, ''), d.properties.Union_Name, ((d.properties.Facility_N!=null)?d.properties.Facility_N:""), ((d.properties.Category!=null)?d.properties.Category:"")));
    }

    //.text(d.properties.MEAN_DISTC + " , " +d.properties.MEAN_UZCOD);

    $(".chart_view").show();
}

//Move the tooltip to track the mouse
function moveTooltip() {
    $(".tooltip_view").style("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .style("left",(d3.event.pageX+tooltipOffset.x)+"px");

    $(".chart_view").show();
}

//Create a tooltip, hidden at the start
function hideTooltip(d) {
    $(".tooltip_view").hide();//style("display","none");
    if(type_name=="upz"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(dist_id, dist_name));
    }
    else if(type_name=="union"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_upz(dist_id + "_" + upz_id,upz_name));
    }
    $(".chart_view").hide();
}

function moveTooltip() {
    $(".tooltip_view").style("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .style("left",(d3.event.pageX+tooltipOffset.x)+"px");

    $(".chart_view").show();
}

function showTooltipFacility(d) {
    moveTooltipFacility();
    $(".mouse_hover").show()
        .text(d.Facility_Name);
}

function moveTooltipFacility() {
    $(".mouse_hover").css("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .css("left",(d3.event.pageX+tooltipOffset.x)+"px");
}

function hideTooltipFacility() {
    $(".mouse_hover").hide();
}

function prev_dis() {
    $("svg").remove();
    load_dis();
    type_name = "dis";
    $(".prev_dis").hide();
    $(".prev_upz").hide();
}

function prev_upz() {
    $("svg").remove();
    load_upz(dist_name, dist_id);
    type_name = "upz";
    $(".prev_dis").show();
    $(".prev_upz").hide();
}
