$(document).ready(function(){
	$(document).ajaxStart(function () {
        NProgress.start();
        setTimeout(function (){NProgress.done();},10000);

    }).ajaxStop(function () {
		NProgress.done();
		// NProgress.remove();
    });

	$(".ui-helper-hidden-accessible").remove();
	var currentyear = (new Date).getFullYear();

	var f = $("<div id='bottom' class='div_footer'><footer>Copyright \u00A9"+currentyear+" RHIS (Version 1.0)</footer></div>");
	$("body").append(f);

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$("body").addClass("sidebar-open");
		$(document).on("click",'.menu-list', function(){
			$( ".sidebar-toggle" ).trigger( "click" );
		});
	}

    function ReadCookie() {
        var allcookies = document.cookie;
        // document.write ("All Cookies : " + allcookies );

        // Get all the cookies pairs in an array
        cookiearray = allcookies.split(';');

        // Now take key value pair out of this array
        var name = "";
        var value = "";
        var c = 0;
        for(var i=0; i<cookiearray.length; i++) {
            name = cookiearray[i].split('=')[0];
            value = cookiearray[i].split('=')[1];

            name = name.trim();
            // value = value.trim();

            // console.log(value);
            // console.log("username");
            if(name=="username" && value!="") {
                $("#providerID").val(value);
                c = 1;
            }
            if(name=="userpass") {
                $("#providerPass").val(value);
            }
        }

        if(c==1) {
            loginCheck();
        }
    }


    ReadCookie();

//	$(document).ajaxStart(function () {
//		$.ajax({
//	           type: "POST",
//	           url:"checkSession",
//	           timeout:300000,
//	           success: function (response) {
//	        	   console.log(response);
//	        	   if(response=='false'){
//	        		   location.reload();   
//	           	   }
//	        	   else{
//	        		   load_home();
//	        	   }
//	           },
//	           complete: function() {
//	        	   preventMultiCall = 0;
//	           },
//	           error: function(xhr, status, error) {
//					alert(xhr.status);
//	        	   	alert(status);
//					//alert(error);
//	           }
//			});	
//	});

	var preventMultiCall = 0;

	$(document).on("keypress", '#providerPass',function (event){
		if(event.keyCode == 13){  //13 means 'enter' key
			if(preventMultiCall==0){
				preventMultiCall = 1;
				preventMultiCall = loginCheck();
			}
		}
	});



	//***Change Password: Coded By Evana: 30/01/2019 *****

    $(document).on("click",'#change_password', function(){


        if(preventMultiCall==0){
            //preventMultiCall = 1;
            preventMultiCall = loadChangePassword();
            //preventMultiCall = 0;

        }


    });

    function loadChangePassword()
    {
        //alert("Change My Password again!");

        $.ajax({
            type: "POST",
            url:"ChangePassword",
            timeout:300000,
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        //return preventMultiCall;

        //return false;
    }
    /**** Change Password: End Here*****/

	/*****
	 * Change Provider Information
	 * @Author:  Evana
	 * Date: 19/02/2019
	 * **********/

    function delete_cookie( name ) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    $(document).on("click",'#logout', function(){


        delete_cookie("username");
        delete_cookie("userpass");

        location.reload();
    });

	$(document).on("click",'#ProvEditForm', function(){

		//alert("Password Change");

        event.preventDefault();
        //event.stopPropagation();
        //e.stopImmediatePropagation();

		if($("#editProvName").val() == "")
		{

			$('#provname_validate').fadeIn(2000, function () {
				//First animation complete
				$('#provname_validate').html('<span style="font-size:15px;color:darkred;">The field is required</span>');

			})
				.delay(2000) //Wait 4 seconds
				.fadeOut(2000, function () {

					$('#provname_validate').html('');
				});

			$('#editProvName').focus();
			$("#editProvName").css("background-color", "#E8F5FA");
			//callDialog("Please give the provider name.");
			//$('#modal_box').dialog("close");
			return false;
		}
		else if($("#editProvMob").val() == "")
		{
			$('#mobile_validate').fadeIn(2000, function () {
				//First animation complete
				$('#mobile_validate').html('<span style="font-size:15px;color:darkred;">The field is required</span>');

			})
				.delay(2000) //Wait 4 seconds
				.fadeOut(2000, function () {

					$('#mobile_validate').html('');
				});

			$('#editProvMob').focus();
			$("#editProvMob").css("background-color", "#E8F5FA");
			//callDialog("Please give the provider mobile.");
			//$('#modal_box').dialog("close");
			return false;
		}
		else if($("#editProvPass").val() == "")
		{
			$('#pass_validate').fadeIn(2000, function () {
				//First animation complete
				$('#pass_validate').html('<span style="font-size:15px;color:darkred;">The field is required</span>');

			})
				.delay(2000) //Wait 4 seconds
				.fadeOut(2000, function () {

					$('#pass_validate').html('');
				});

			$('#editProvPass').focus();
			$("#editProvPass").css("background-color", "#E8F5FA");
			//callDialog("Please give the provider mobile.");
			//$('#modal_box').dialog("close");
			return false;
		}
		else
		{
			changeProviderInfo();
			return false;
		}
	});


	function changeProviderInfo()
	{
        //e.preventDefault();
		loadGif();

		var forminfo = new Object();

        forminfo.provname = $('#editProvName').val();
		forminfo.mobileno = $('#editProvMob').val();
		forminfo.provpass = $('#editProvPass').val();
		forminfo.providerid = $('#providercode').val();
		forminfo.zillaid = $('#zillaid').val();
		forminfo.upazilaid = $('#upazilaid').val();
		forminfo.providertype = $('#providertype').val();
        forminfo.active = $('#active').prop("checked")?"1":"";
        forminfo.inactive = $('#inactive').prop("checked")?"2":"";
        forminfo.active_date = $('#active_date').val();
        forminfo.approval_upazilas = $('#approval_upazilas').val();
        forminfo.start_date = $('#start_date').val();

		var url = "provinfoupdated";

		$.ajax({
            cache:false,
			type: "POST",
			url:url,
			timeout:300000, //60 seconds timeout
			data:{"forminfo":JSON.stringify(forminfo)},
			success: function (response) {

				if (response == "1") {

                    $('#modal_box').dialog("close");
                    $('#modal_box').dialog("destroy");

                    //alert("Profile Successfully updated");
                    //callDialog("<b style='color:green'>Profile Successfully updated</b>");
                    //$('#profile_update_info').html("<b style='color:green'>Profile Succesfully Updated.</b>");


                    $("#reportDataTable").load(location.href + " #reportDataTable");

                    getReport1Result();
                    //document.getElementById("ProvEntryForm").click();
                    //forminfo=null;

                    /*
                    forminfo.provname = "";
                    forminfo.mobileno = "";
                    forminfo.provpass = "";
                    forminfo.providerid = "";
                    forminfo.zillaid = "";
                    forminfo.upazilaid = "";
                    forminfo.providertype = "";
                    */


                    //$('#editProvName').val("");
                    //$('#editProvMob').val("");
                    //$('#editProvMob').val('');
                    //$('#providercode').val('');
                    //$('#zillaid').val('');
                    //$('#upazilaid').val('');
                    //$('#providertype').val('');

                    $(".ui-dialog-content").dialog().dialog("destroy");
                    $(".ui-dialog-content").dialog().dialog("close");
                    //$(".ui-dialog-content").dialog().dialog("destroy");
                    //event.preventDefault();

					callDialog("<b style='color:green'>Profile Successfully updated</b>");
                    return false;
                    //callProfileDialog("<b style='color:green'>Profile Succesfully Updated.</b>");

                    //$(this).window("close");
                   // callProfileDialog("<b style='color:green'>Profile Succesfully Updated.</b>");
                    //e.stopPropagation();
                }
				else
                {
                    callDialog("<b style='color:red'>Sorry! Profile can not be Updated.</b>");
                }

                //$('#modal_box').dialog("close");
			},
			complete: function() {
				preventMultiCall = 0;

			},
			error: function(xhr, status, error) {
                checkInternetConnection(xhr)
			}
		});
		//alert(preventMultiC);
		return preventMultiCall;

	}

	/**  provider Information update end here.. **/


	/*****
	 * Change Facility Name
	 * @Author:  Evana
	 * Date: 18/09/2019
	 * **********/

	$(document).on("click",'#facEditForm', function(){

		//alert("Password Change");

		event.preventDefault();
		//event.stopPropagation();
		//e.stopImmediatePropagation();

		if($("#editfacName").val() == "")
		{
			//$('#name_validate').html("Please give the facility name.");

			//callDialog("Please give the facility name");

			$('#name_validate').fadeIn(3000, function () {
				//First animation complete
				$('#name_validate').html('<span style="font-size:15px;color:darkred;">The field is required</span>');

			})
				.delay(3000) //Wait 4 seconds
				.fadeOut(3000, function () {

					$('#name_validate').html('');
				});

			$('#editfacName').focus();
			$("#editfacName").css("background-color", "#E8F5FA");

			//return false;

		}
		else
		{
			changeFacilityInfo();
			return false;
		}
	});


	function changeFacilityInfo()
	{
		//e.preventDefault();
		loadGif();

		var forminfo = new Object();

		forminfo.facilityid = '';
		forminfo.zillaid = '';
		forminfo.upazilaid = '';
		forminfo.editfacName = '';

		// alert(forminfo.provname);
		// alert(forminfo.mobileno);
		// alert(forminfo.provpass);

		forminfo.facilityid = $('#facilityid').val();
		forminfo.zillaid = $('#zillaid').val();
		forminfo.upazilaid = $('#upazilaid').val();
		//forminfo.editfacName = $('#editfacName').val();

		var check = toUnicode($('#editfacName').val());

		//forminfo.UnionFacilityName = check;
		/* @Author: Evana, @Date: 23/06/2019 */

		check = JSON.parse('"' + check + '"');
		forminfo.editfacName = check;
		var url = "facinfoupdated";

		$.ajax({
			cache:false,
			type: "POST",
			url:url,
			timeout:60000, //60 seconds timeout
			data:{"forminfo":JSON.stringify(forminfo)},
			success: function (response) {

				if (response == "1") {

					$('#modal_box').dialog("destroy");
					$('#modal_box').dialog("close");



					$("#reportDataTable").load(location.href + " #reportDataTable");

					getReport1Result();

					$(".ui-dialog-content").dialog().dialog("destroy");
					$(".ui-dialog-content").dialog().dialog("close");
					//$(".ui-dialog-content").dialog().dialog("close");

					event.preventDefault();

					callDialog("<b style='color:green'>Information Successfully Updated</b>");
					return false;

				}
				else
				{
					callDialog("<b style='color:red'>Sorry! Profile can not be updated.</b>");
				}

			},
			complete: function() {
				preventMultiCall = 0;

			},
			error: function(xhr, status, error) {
                checkInternetConnection(xhr)
			}
		});
		//alert(preventMultiC);
		return preventMultiCall;

	}

	/**  Facility Information update end here.. **/

    $(document).on("click",'#login', function(){
		if(preventMultiCall==0){
			preventMultiCall = 1;
			preventMultiCall = loginCheck();
		}
	});

	//login check
	function loginCheck(){

		//var preventMultiC = 1;
		var loginObj = new Object();
		loginObj.uid = $('#providerID').val();
		loginObj.upass = $('#providerPass').val();
        if (document.getElementById('rememberme').checked)
            loginObj.rememberme = 1;
        else
            loginObj.rememberme = 0;
		loginObj.client = "1"; //web

		$.ajax({
           type: "POST",
           url:"login",
           timeout:300000, //60 seconds timeout
           data:{"loginInfo":JSON.stringify(loginObj)},
           success: function (response) {
        	   if(response=='false'){
             		   $("#error_msg").text("Sorry! Wrong information provided.");
           	   }
        	   else{
        		   $("#remove_div").remove();
        		   $( "#wrapper" ).html(response);

        		   var min_height = $( window ).height()-100;
        		   $(".content-wrapper").css('min-height',min_height);

        			var copyright = "Copyright &copy; "+currentyear;
        			$("#copyrigth").html(copyright);

        			//if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) )
        			//{
        				$( ".sidebar-toggle" ).trigger( "click" );
        				$( ".sidebar-toggle" ).trigger( "click" );
        			//}

        	   }

        	   //load_home();
			   	load_new_dashboard();

           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               // alert(xhr.status);
               // alert(status);

               if (xhr.readyState == 0) {
                   $('#error_msg').dialog({
                       modal: true,
                       title: "Message",
                       width: 420,
                       open: function () {
                           $(this).html("Network is unavailable");
                       },
                       buttons: {
                           Ok: function () {
                               $(this).dialog("close");
                           }
                       }
                   });
               } else {
               	alert("Network available.Check other Issue!!")
			   }
           }
		});
		//alert(preventMultiC);

		return preventMultiCall;
	}

	$(document).on("click",'.home', function(){
		load_home();
	});

	function load_home()
	{
		$.ajax({
           type: "POST",
           url:"home",
           timeout:100000,
           success: function (response) {
        	   $(".remove_div").remove();
    		   $( "#append_report" ).html(response);
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
			   checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

    $(document).on("click",'.loginTracking', function(){
        $.ajax({
            type: "POST",
            url:"loadTrackingMap",
            timeout:300000,
            success: function (response) {
                $(".remove_div").remove();
                $("#head_title").html('Provider<span> Login Tracking</span>');
                $( "#append_report" ).html(response);
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    });

    $(document).on("click",'.dashboard', function(){
        load_dashboard();
    });

    function load_dashboard() {
        var dashboardObj = new Object();
        dashboardObj.providerId = ($("#ProviderID").text()).trim();
        $.ajax({
            type: "POST",
            url:"dashboard",
            timeout:300000,
            data:{"dashboardInfo":JSON.stringify(dashboardObj)},
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }

    $(document).on("click",'.emis_overview', function(){
        load_emis_overview();
    });

    function load_emis_overview() {
        // var dashboardObj = new Object();
        // dashboardObj.providerId = ($("#ProviderID").text()).trim();
        $.ajax({
            type: "POST",
            url:"emisOverview",
            timeout:300000,
            //data:{"dashboardInfo":JSON.stringify(dashboardObj)},
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }

	$(document).on("click",'.entry1', function(){
		userEntryForm("1");
	});

    /*Coded at: 04/04/2019:
    * @author: Evana:
    * User Entry Form
    * */
	$(document).on("click",'.entry11', function(){
		//alert("This is general user entry");
		userEntryForm("11");
	});

    /**
	 * Manage Provider (Anjum)
     */
    $(document).on("click",'.entry12', function(){
        userEntryForm("12");
    });

	$(document).on("click",'.entry2', function(){
		userEntryForm("2");
	});

	$(document).on("click",'.entry3', function(){
		userEntryForm("3");
	});

	$(document).on("click",'.entry4', function(){
		userEntryForm("4");
	});

	$(document).on("click",'.entry5', function(){
		userEntryForm("5");
	});

	$(document).on("click",'.entry6', function(){
		userEntryForm("6");
	});

    $(document).on("click",'.entry7', function(){
        userEntryForm("7");
    });

    $(document).on("click",'.entry8', function(){
        userEntryForm("8");
    });

    $(document).on("click",'.entry9', function(){
        userEntryForm("9");
    });

    $(document).on("click",'.entry10', function(){
        userEntryForm("10");
    });

    $(document).on("click",'.entry26', function(){
        userEntryForm("26");
    });

	$(document).on("click",'.entry27', function(){
		userEntryForm("27");
	});
    $(document).on("click",'.entry28', function(){
        userEntryForm("28");
    });
	$(document).on("click",'.add_csba', function(){
		userEntryForm("addcsba");
	});

	$(document).on("click",'.db_rep', function(){
		NewDbEntryForm();
	});

	$(document).on("click",'.sym1', function(){
		SymmetricDSConfForm("1");
	});

	$(document).on("click",'.sym2', function(){
		SymmetricDSConfForm("2");
	});

	$(document).on("click",'.flist', function(){
		FacilityListForm("1");
	});

	$(document).on("click",'.report1', function(){
		viewReport("1");
	});

	$(document).on("click",'.report2', function(){
		viewReport("2");
	});

	$(document).on("click",'.report3', function(){
		viewReport("3");
	});

	$(document).on("click",'.report4', function(){
		viewReport("4");
	});

	$(document).on("click",'.report5', function(){
		viewReport("5");
	});

	$(document).on("click",'.report6', function(){
		viewReport("6");
	});

	$(document).on("click",'.report7', function(){
		viewReport("7");
	});

	$(document).on("click",'.report8', function(){
		viewReport("8");
	});

	$(document).on("click",'.report9', function(){
		viewReport("9");
	});

	$(document).on("click",'.report10', function(){
		viewReport("10");
	});

	$(document).on("click",'.report11', function(){
		viewReport("11");
	});

	$(document).on("click",'.report12', function(){
		viewReport("12");
	});

	$(document).on("click",'.report13', function(){
		viewReport("13");
	});

	$(document).on("click",'.report14', function(){
		viewReport("14");
	});

    $(document).on("click",'.report18', function(){
        viewReport("18");
    });
    $(document).on("click",'.report16', function(){
        viewReport("16");
    });
    $(document).on("click",'.report17', function(){
        viewReport("17");
    });
    $(document).on("click",'.report19', function(){
        viewReport("19");
    });
    $(document).on("click",'.report20', function(){
        viewReport("20");
    });
    $(document).on("click",'.report21', function(){
        viewReport("21");
    });
    $(document).on("click",'.report22', function(){
        viewReport("22");
    });
    $(document).on("click",'.report23', function(){
        viewReport("23");
    });
	$(document).on("click",'.report24', function(){
		//alert("hello");
       // document.location.href = "NEWREPORT";
    });
    $(document).on("click",'.report25', function(){
        viewReport("25");
    });
    $(document).on("click",'.report26', function(){
        viewReport("26");
    });
    $(document).on("click",'.report27', function(){
        viewReport("27");
    });
    $(document).on("click",'.reportQed', function(){
        viewReport("28");
    });
    $(document).on("click",'.reportFCC', function(){
        viewReport("29");
    });
    $(document).on("click",'.reportFCCTrend', function(){
        viewReport("30");
    });
	$(document).on("click",'.report31', function(){
		//client information
		viewReport("31");
	});

	$(document).on("click",'.report15', function(){
		loadGif();

		$.ajax({
	           type: "POST",
	           url:"loadTrckingMap",
	           timeout:300000,
	           success: function (response) {
	        	   $(".remove_div").remove();
	        	   $("#head_title").html('Provider<span> Login Tracking</span>');
	    		   $( "#append_report" ).html(response);

	    		   var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
	    		   var bottom = $el.position().top + $el.outerHeight(true);
	    		   $(".main-sidebar").css('height',bottom+"px");
	           },
	           complete: function() {
	        	   preventMultiCall = 0;
	           },
	           error: function(xhr, status, error) {
                   checkInternetConnection(xhr)
	           }
			});
			//alert(preventMultiC);
			return preventMultiCall;


	});

	$(document).on("click",'.approval1', function(){
		viewApprovalReport("1");
	});

    $(document).on("click",'.approval3', function(){
        viewApprovalReport("3");
    });
	
	function userEntryForm(type)
	{
		loadGif();

		var typeObj = new Object();
		typeObj.utype = ($("#acc_ty").val()).trim();
		typeObj.type = type;

		if(type=="5" || type=="27" || type=="28")
			var url = "facilityAdd";
		else if(type=="7" || type=="10")
            var url = "providerTransfer";
		else
			var url = "userEntry";


		$.ajax({
           type: "POST",
           url:url,
           timeout:300000, //60 seconds timeout
           data:{"typeInfo":JSON.stringify(typeObj)},
           success: function (response) {
        	   $(".remove_div").remove();
        	   $("#head_title").html('Provider<span> Entry Form</span>');
    		   $( "#append_report" ).html(response);
    		   $("#formType").val(type);

    		   if(type==2)
    		   {
    			   $( "#Pname" ).hide();
    			   $( "#Pmob" ).hide();
    			   $( "#Ppass" ).hide();
    			   $( "#FacName" ).hide();
    			   $( "#aav_provider_type" ).hide();
                   $( "#db_upload_download" ).hide();
    		   }
    		   else if(type==3 || type==12)
    		   {
    			   $( "#Pname" ).hide();
    			   $( "#Pmob" ).hide();
    			   $( "#Ppass" ).hide();
    			   $( "#FacName" ).hide();
    			   $( "#provider_type" ).hide();
    			   $( "#provider_code" ).hide();
    			   $( "#change_type" ).show();
    			   $( "#aav_provider_type" ).show();
    			   if(!(type == 12)) {
                       $("#version_name").show();
                   }
                   $( "#db_upload_download" ).hide();
    		   }
    		   else if(type==4)
    		   {
    			   $( "#Pname" ).hide();
    			   $( "#Pmob" ).hide();
    			   $( "#Ppass" ).hide();
    			   $( "#FacName" ).hide();
    			   $( "#provider_type" ).hide();
    			   $( "#provider_code" ).hide();
    			   $( "#change_type" ).hide();
    			   $( "#version_name" ).hide();
    			   $( ".retired_date" ).show();
    			   $( "#unionName" ).hide();
    			   $( "#aav_provider_type" ).hide();
                   $( "#db_upload_download" ).hide();
                   $( "#retired_type" ).show();
                   $( "#provider_id" ).show();
    		   }
    		   else if(type==5) /*Facility Add*/
    		   {
    			   $( "#Pname" ).hide();
    			   $( "#Pmob" ).hide();
    			   $( "#Ppass" ).hide();
    			   $( "#FacName" ).hide();
    			   $( "#provider_type" ).hide();
    			   $( "#provider_code" ).hide();
    			   $( "#change_type" ).hide();
    			   $( "#version_name" ).hide();
    			   $( ".retired_date" ).hide();
    			   $( "#unionName" ).show();
    			   $( "#facility_type" ).show();
    			   $( "#aav_provider_type" ).hide();
                   $( "#db_upload_download" ).hide();
    		   }
			   else if(type==27 || type==28)  /*Facility List*/
			   {
				   $( "#Pname" ).hide();
				   $( "#Pmob" ).hide();
				   $( "#Ppass" ).hide();
				   $( "#FacName" ).hide();
				   $( "#provider_type" ).hide();
				   $( "#provider_code" ).hide();
				   $( "#change_type" ).hide();
				   $( "#version_name" ).hide();
				   $( ".retired_date" ).hide();
				   $( "#unionName" ).hide();
				   $( "#facility_type" ).show();
				   $( "#aav_provider_type" ).hide();
				   $( "#db_upload_download" ).hide();
			   }
    		   else if(type==6)
    		   {
    			   $( "#Pname" ).hide();
    			   $( "#Pmob" ).hide();
    			   $( "#Ppass" ).hide();
    			   $( "#FacName" ).hide();
    			   $( "#provider_type" ).hide();
    			   $( "#provider_code" ).hide();
    			   $( "#change_type" ).hide();
    			   $( "#version_name" ).hide();
    			   $( ".retired_date" ).hide();
    			   $( "#unionName" ).hide();
    			   $( "#upazilaName" ).hide();
    			   $( "#provider_type" ).show();
    			   $( "#aav_provider_type" ).hide();
                   $( "#db_upload_download" ).hide();
    		   }
    		   else if(type==26)
               {
                   $( "#Pname" ).hide();
                   $( "#Pmob" ).hide();
                   $( "#Ppass" ).hide();
                   $( "#FacName" ).hide();
                   $( "#provider_type" ).hide();
                   $( "#provider_code" ).hide();
                   $( "#change_type" ).hide();
                   $( "#version_name" ).hide();
                   $( ".retired_date" ).hide();
                   $( "#unionName" ).hide();
                   $( "#upazilaName" ).hide();
                   $( "#provider_type" ).show();
                   $( "#aav_provider_type" ).hide();
                   $( "#db_upload_download" ).hide();
               }
               else if(type==8)
               {
                   $( "#Pname" ).hide();
                   $( "#Pmob" ).hide();
                   $( "#Ppass" ).hide();
                   $( "#FacName" ).hide();
                   $( "#provider_type" ).hide();
                   $( "#provider_code" ).hide();
                   $( "#change_type" ).hide();
                   $( "#version_name" ).hide();
                   $( ".retired_date" ).hide();
                   $( ".retired_date" ).hide();
                   $( "#unionName" ).show();
                   $( "#upazilaName" ).show();
                   $( "#provider_type" ).hide();
                   $( "#aav_provider_type" ).hide();
                   $( "#db_upload_download" ).hide();
               }
               else if(type==9)
			   {
				   $( "#Pname" ).hide();
				   $( "#Pmob" ).hide();
				   $( "#Ppass" ).hide();
				   $( "#FacName" ).hide();
				   $( "#provider_type" ).hide();
				   $( "#provider_code" ).hide();
				   $( "#change_type" ).show();
				   $( "#aav_provider_type" ).show();
				   $( "#version_name" ).hide();
				   $( "#db_upload_download" ).show();
			   }
			   else if(type==11)      /* General User Entry Form */
			   {
			   		//alert("This is general user entry");
				   $( "#Pname" ).show();
				   $( "#Pmob" ).show();
				   $( "#Ppass" ).show();
				   $( "#FacName" ).show();
				   $( "#provider_facility_type" ).hide();
				   $( "#aav_provider_type" ).hide();
				   $( "#db_upload_download" ).hide();
			   }
    		   else
    		   {
    			   $( "#Pname" ).show();
    			   $( "#Pmob" ).show();
    			   $( "#Ppass" ).show();
    			   $( "#FacName" ).show();
    			   $("#version_name").show();
    			   $( "#provider_facility_type" ).show();
    			   $( "#aav_provider_type" ).hide();
                   $( "#db_upload_download" ).hide();
    		   }

    		   switch(type) {
	    		   case "1":
	    			   $("#head_title").html('Add <span>Provider</span>');
	    			   $("#box_title").html('<strong>Add Provider </strong>');
	    		        break;
	    		   case "2":
	    			   $("#head_title").html('Assign <span> Health ID</span>');
	    			   $("#box_title").html('<strong>Assign Health ID </strong>');
	    		        break;
	    		   case "3":
	    			   $("#head_title").html('Update <span> Application</span>');
	    			   $("#box_title").html('<strong>Update Application </strong>');
	    		        break;
	    		   case "4":
	    			   $("#head_title").html('Update <span> Provider Status</span>');
	    			   $("#box_title").html('<strong>Update Provider Status</strong>');
	    		        break;
	    		   case "5":
	    			   $("#head_title").html('Add <span>Facility</span>');
	    			   $("#box_title").html('<strong>Add Facility </strong>');
	    		        break;
	    		   case "6":
	    			   $("#head_title").html('View <span> Provider</span>');
	    			   $("#box_title").html('<strong>View Provider </strong>Info');
	    			   //$("#table_title").html('<strong>View Provider List</strong>');
	    		        break;
                   case "7":
                       $("#head_title").html('Transfer <span> Provider</span>');
                       $("#box_title").html('<strong>Transfer Provider</strong>');
                       //$("#table_title").html('<strong>Transfer Provider List</strong>');
                       break;
                   case "8":
                       $("#head_title").html('Update HH Block & Unit <span> By Village</span>');
                       $("#box_title").html('<strong>Update HH Block & Unit By Village</strong>');
                      // $("#table_title").html('<strong>Update HH Block & Unit By Village</strong>');
                       $("#ProvEntryForm").val("Load Village");
                       break;
                   case "9":
                       $("#head_title").html('Manage <span> Offline Database</span>');
                       $("#box_title").html('<strong>Manage Offline Database</strong>');
                       break;
				   case "11":
					   $("#head_title").html('Add <span> User</span>');
					   $("#box_title").html('<strong>Add User </strong>');
					   break;
                   case "26":
                       $("#head_title").html('View <span> User Info</span>');
                       $("#box_title").html('<strong>View User Info </strong>');
                       //$("#table_title").html('<strong>View User Details</strong>');
                       break;
				   case "27":
					   $("#head_title").html('View <span> Facility List</span>');
					   $("#box_title").html('<strong>View Facility List </strong>');
					   //$("#table_title").html('<strong>View User Details</strong>');
					   break;

                   case "28":
                       $("#head_title").html('View Facility List <span> with Provider</span>');
                       $("#box_title").html('<strong>View Facility List with Provider</strong>');
                       //$("#table_title").html('<strong>View User Details</strong>');
                       break;

			   }

    		   var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
    		   var bottom = $el.position().top + $el.outerHeight(true);
    		   $(".main-sidebar").css('height',bottom+"px");
    		   //alert(bottom);

			   loadDivision("#RdivName");
    		   //loadZilla("#RzillaName");

           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	/***** Coded By Evana: 30/01/2019**********/
    $(document).on("click",'#ChangePasswordForm', function(){

    	//alert("Password Change");

        if($("#CurrPass").val() == "")
		{
			$('#CurrPass').focus();
			$("#CurrPass").css("background-color", "#E8F5FA");
			callDialog("Please give the current password.");
			//return false;
		}
        else if($("#NewPass").val() == "")
        {

			$('#NewPass').focus();
			$("#NewPass").css("background-color", "#E8F5FA");
			callDialog("Please give the new password.");

        }
        else if($("#ConPass").val() == "")
        {
			$('#ConPass').focus();
			$("#ConPass").css("background-color", "#E8F5FA");
			callDialog("Please give the confirm password.");

        }
		else
        {
        	 var newpassd = $("#NewPass").val();
			 var confrpass = $("#ConPass").val();

			if(newpassd === confrpass)
			{
				changePasswordInfo();
			}
			else
			{
				callDialog("Your New Password and Confirm Password are not same.");

			}

        }
    });

    function changePasswordInfo()
    {
        loadGif()

        var forminfo = new Object();
        forminfo.currentpass = $('#CurrPass').val();
        forminfo.newpass = $('#NewPass').val();
        forminfo.oldpass = $('#ConPass').val();
        forminfo.uid = $('#uid').val();

        //alert(forminfo.uid);

        var url = "UpdatePassword";

        $.ajax({
            type: "POST",
            url:url,
            timeout:300000, //60 seconds timeout
            data:{"forminfo":JSON.stringify(forminfo)},
            success: function (response) {

            if (response == "1") {

				$('#CurrPass').val('');
				$('#NewPass').val('');
				$('#ConPass').val('');

				$("#CurrPass").css("background-color", "white");
				$("#NewPass").css("background-color", "white");
				$("#ConPass").css("background-color", "white");
                callDialog("<b style='color:green'>Password Successfully Changed.</b>");

            }
            else
				{
					$("#CurrPass").css("background-color", "white");
					$("#NewPass").css("background-color", "white");
					$("#ConPass").css("background-color", "white");
					callDialog("<b style='color:red'>Sorry! Password can not be changed.</b>");
				}


            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;


    }

	$(document).on("click", '#ProvEntryForm' ,function (){
		
		
		if($('#change_type').is(':visible') && $("#changeType").val() == "")
			callDialog("Please give the version given type.");
		else if($('#facility_type').is(':visible') && $("#facilityType").val() == "")
			callDialog("Please give the facility type.");
        else if($('#retired_type').is(':visible') && $("#retiredType").val() == "")
            callDialog("Please give the status type.");
        else if($('#leave_type').is(':visible') && $("#leaveType").val() == "")
            callDialog("Please give the leave type.");
        else if($("#divID").val() == "")
            callDialog("Please give the current division name.");
		else if($("#distID").val() == "")
			callDialog("Please give the current district name.");
        else if($('#upzID').is(':visible') && $("#RupaZilaName").val() == "")
            callDialog("Please give the current upazila name.");
        else if($('#unID').is(':visible') && $("#RunionName").val() == "")
            callDialog("Please give the current union name.");
		else if($("#RdivName").val() == "")
			callDialog("Please give the division name.");
		else if($("#RzillaName").val() == "")
			callDialog("Please give the district name.");
		//else if($('#upazilaName').is(':visible') && $("#RupaZilaName").val() == "" && $("#providerType").val() != "22" && $("#formType").val() != "27" && $("#facilityType").val() != "6_MCWC")
		else if($('#upazilaName').is(':visible') && $("#RupaZilaName").val() == "" && $("#providerType").val() != "22" && $("#formType").val() != "27" && $("#formType").val() != "28") /* Coded Changed: 05/09/2019, @by : Evana*/
			callDialog("Please give the upazila name.");
		else if($('#unionName').is(':visible') && $("#RunionName").val() == "" && $("#facilityType").val() != "6_MCWC" && $("#facilityType").val() != "7_DH")
		{
			callDialog("Please give the union name.");
			//return false;
		}
		//callDialog("Please give the union name.");
		//else if($('#unionName').is(':visible') && $("#RunionName").val() == "")
		//callDialog("Please give the union name.");
		else if($('#provider_type').is(':visible') && $("#providerType").val() == "")
			callDialog("Please give the provider type.");
		else if($('#Pname').is(':visible') && $("#ProvName").val() == "")
			callDialog("Please give the provider name.");
		else if($('#Pmob').is(':visible') && $("#ProvMob").val() == "")
			callDialog("Please give the provider mobile no.");
		else if($('#provider_code').is(':visible') && $("#ProvCode").val() == "")
			callDialog("Please give the provider code.");
		else if($('#Ppass').is(':visible') && $("#ProvPass").val() == "")
			callDialog("Please give the provider password.");
		else if($('#FacName').is(':visible') && $("#facilityName").val() == "")
			callDialog("Please give the facility name.");
		else if($('#version_name').is(':visible') && $("#version").val() == "" && $('#formType').val() != 1)
		{
			callDialog("Please give the version name.");
		}
		else if($('#retired_date').is(':visible') && $("#end_date").val() == "")
            callDialog("Please give the date.");
        else if($('#TransferDate').is(':visible') && $("#transfer_date").val() == "")
            callDialog("Please give the transfer date.");
        else if($('#db_upload_download').is(':visible') && $("#upload_download_status").val() == "")
            callDialog("Please give the database status change type.");

		else if($("#formType").val() == "7"){
            var confirm_msg = "<b>Current Location Info:</b><br>";
			confirm_msg = confirm_msg + "<b>Division: </b>"+$('#divID option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>District: </b>"+$('#distID option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>Upazila: </b>"+$('#upzID option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>Union: </b>"+$('#unID option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>Provider Name: </b>"+$('#providerID option:selected').text()+ "<br><br>";
            confirm_msg = confirm_msg + "<b>Transfer Info:</b><br>";
			confirm_msg = confirm_msg + "<b>Divisin: </b>"+$('#RdivName option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>District: </b>"+$('#RzillaName option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>Upazila: </b>"+$('#RupaZilaName option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>Union: </b>"+$('#RunionName option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>Facility Type: </b>"+$('#providerFacility option:selected').text()+ "<br>";
            confirm_msg = confirm_msg + "<b>Facility Name: </b>"+$('#facilityName option:selected').text()+" ("+$('#facilityName').val()+")"+ "<br>";
            confirm_msg = confirm_msg + "<b>Transfer Date: </b>"+$('#transfer_date').val()+ "<br><br>";
            confirm_msg = confirm_msg + " <br>Do you want to submit?";


            $("#model_box").dialog({
                modal: true,
                title: "Message",
                width: 420,
                open: function () {
                    $(this).html(confirm_msg);
                },
                buttons : {
                    "Yes" : function() {
                        $(this).dialog('close');
                        if($("#formType").val() == "6" || $("#formType").val() == "26")
                            getReport1Result();
                        else
                            saveProviderInfo();
                    },
                    "No" : function() {
                        $(this).dialog("close");
                        return false;
                    }
                }
            });


        }
		else{
			var confirm_msg = "<b>Division: </b>"+$('#RdivName option:selected').text()+ "<br>" + "<b>District: </b>"+$('#RzillaName option:selected').text()+ "<br>";

            if($('#retired_type').is(':visible'))
                var confirm_msg = confirm_msg + "<b>Status Type: </b>"+$('#retiredType option:selected').text()+ "<br>";

            if($('#leave_type').is(':visible'))
                var confirm_msg = confirm_msg + "<b>Leave Type: </b>"+$('#leaveType option:selected').text()+ "<br>";

			if($('#upazilaName').is(':visible'))
				var confirm_msg = confirm_msg + "<b>Upazila: </b>"+$('#RupaZilaName option:selected').text()+ "<br>";

			//if(($("#changeType").val() == "" || $("#changeType").val() == "3" || $("#changeType").val() == "4") && $("#formType").val() != "4")
			if($('#unionName').is(':visible'))
				var confirm_msg = confirm_msg + "<b>Union: </b>"+$('#RunionName option:selected').text()+ "<br>";

			if($('#facility_type').is(':visible'))
				var confirm_msg = confirm_msg + "<b>Facility Type: </b>"+$('#facilityType option:selected').text()+ "<br>";

            if($('#db_upload_download').is(':visible'))
                var confirm_msg = confirm_msg + "<b>Database Change Type: </b>"+$('#upload_download_status option:selected').text()+ "<br>";
			
			if($("#formType").val() == "1" || $("#formType").val() == "2" || ($("#formType").val() == "3" && $("#changeType").val() == "4")){
				var confirm_msg = confirm_msg + "<b>Provider Type: </b>"+$('#providerType option:selected').text()+ "<br>"+
								"<b>Provider Code: </b>"+$('#ProvCode').val()+ "<br>";
			}
			else if($("#formType").val() == "6" || $("#formType").val() == "26")
				var confirm_msg = confirm_msg + "<b>Provider Type: </b>"+$('#providerType option:selected').text()+ "<br>";

            if ($('#provider_id').is(':visible'))
                var confirm_msg = confirm_msg + "<b>Provider: </b>"+$('#providerID option:selected').text()+ "<br>";


            if($("#formType").val() == "3"){
				var confirm_msg = confirm_msg + "<b>Version Given By: </b>"+$('#changeType option:selected').text()+ "<br>"+
								"<b>Provider Type: </b>"+$('#aav_providerType option:selected').text()+ "<br>"+
								"<b>Version Name: </b>"+$('#version').val()+ "<br>";
			}

			if($("#formType").val() == "4"){
				var confirm_msg = confirm_msg + "<b>Date: </b>"+$('#end_date').val()+ "<br>";
			}

			if($("#formType").val() == "1"){
				var confirm_msg = confirm_msg + "<b>Provider Name: </b>"+$('#ProvName').val()+ "<br>"+
							"<b>MoProviderbile No.: </b>"+$('#ProvMob').val()+ "<br>"+
							"<b>Provider Password: </b>"+$('#ProvPass').val()+ "<br>"+
							"<b>Facility Name: </b>"+$('#facilityName option:selected').text()+" ("+$('#facilityName').val()+")"+ "<br>";
			}

            if($('#leaveCause').is(':visible'))
                var confirm_msg = confirm_msg + "<b>Leave Comment: </b>"+$('#leaveComment').val()+ "<br>";

			var confirm_msg = confirm_msg + " <br>Do you want to submit?";


            /*
			$("#model_box").dialog({
				modal: true,
			    title: "Message",
			    width: 420,
			    open: function () {
				    $(this).html(confirm_msg);
			   },
			   buttons : {
			        "Yes" : function() {
			        	$(this).dialog('close');
			        	if($("#formType").val() == "6" || $("#formType").val() == "26")
			        		getReport1Result();
			        	else
			        		saveProviderInfo();
			        },
			        "No" : function() {
			          $(this).dialog("close");
			          return false;
			        }
			      }
		    });

			*/
			if($("#formType").val() == "6" || $("#formType").val() == "26" || $("#formType").val() == "27"|| $("#formType").val() == "28" || $("#formType").val() == "addcsba")
            {
            	getReport1Result();
            }
            else
            {
				$("#model_box").dialog({
					modal: true,
					title: "Message",
					width: 420,
					open: function () {
						$(this).html(confirm_msg);
					},
					buttons : {
						"Yes" : function() {
							$(this).dialog('close');
							saveProviderInfo();
						},
						"No" : function() {
							$(this).dialog("close");
							return false;
						}
					}
				});

            	//saveProviderInfo();
            }
            return false;
		}
	});

    /**
	 * User Entry Information Submit
	 *
	 * */

	$(document).on("click", '#UserEntryForm' ,function (){

		//var providerType = $('#providerType').val();
		//alert($('#providerType').val());
		if($('#change_type').is(':visible') && $("#changeType").val() == "")
			callDialog("Please give the version given type.");
		else if($('#facility_type').is(':visible') && $("#facilityType").val() == "")
			callDialog("Please give the facility type.");
		//else if(providerType == 999)

		else if(($("#distID").val() == "")&&($('#providerType').val() != 999))
			callDialog("Please give the current district name.");
		else if($('#upzID').is(':visible') && $("#RupaZilaName").val() == "")
			callDialog("Please give the current upazila name.");
		else if($('#unID').is(':visible') && $("#RunionName").val() == "")
			callDialog("Please give the current union name.");
		else if(($("#RdivName").val() == "")&&($('#providerType').val() != 999))
			callDialog("Please give the division name.");
		else if(($("#RzillaName").val() == "")&&($('#providerType').val() != 999))
			callDialog("Please give the district name.");
		else if($('#upazilaName').is(':visible') && $("#RupaZilaName").val() == "" && $("#providerType").val() != "22" && $("#formType").val() != "27")
			callDialog("Please give the upazila name.");
		else if($('#unionName').is(':visible') && $("#RunionName").val() == "")
			callDialog("Please give the union name.");
		else if($('#provider_type').is(':visible') && $("#providerType").val() == "")
			callDialog("Please give the provider type.");
		else if($('#Pname').is(':visible') && $("#ProvName").val() == "")
			callDialog("Please give the provider name.");
		else if($('#Pmob').is(':visible') && $("#ProvMob").val() == "")
			callDialog("Please give the provider mobile no.");
		else if($('#provider_code').is(':visible') && $("#ProvCode").val() == "")
			callDialog("Please give the provider code.");
		else if($('#Ppass').is(':visible') && $("#ProvPass").val() == "")
			callDialog("Please give the provider password.");
		else if($('#FacName').is(':visible') && $("#facilityName").val() == "")
			callDialog("Please give the facility name.");
		else if($('#version_name').is(':visible') && $("#version").val() == "")
			callDialog("Please give the version name.");
		else if($('#retired_date').is(':visible') && $("#end_date").val() == "")
			callDialog("Please give the retired date.");
		else if($('#TransferDate').is(':visible') && $("#transfer_date").val() == "")
			callDialog("Please give the transfer date.");
		else if($('#db_upload_download').is(':visible') && $("#upload_download_status").val() == "")
			callDialog("Please give the database status change type.");
		else if($("#formType").val() == "7"){
			var confirm_msg = "<b>Current Location Info:</b><br>";
			confirm_msg = confirm_msg + "<b>District: </b>"+$('#distID option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>Upazila: </b>"+$('#upzID option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>Union: </b>"+$('#unID option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>Provider Name: </b>"+$('#providerID option:selected').text()+ "<br><br>";
			confirm_msg = confirm_msg + "<b>Transfer Info:</b><br>";
			confirm_msg = confirm_msg + "<b>Division: </b>"+$('#RdivName option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>District: </b>"+$('#RzillaName option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>Upazila: </b>"+$('#RupaZilaName option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>Union: </b>"+$('#RunionName option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>Facility Type: </b>"+$('#providerFacility option:selected').text()+ "<br>";
			confirm_msg = confirm_msg + "<b>Facility Name: </b>"+$('#facilityName option:selected').text()+" ("+$('#facilityName').val()+")"+ "<br>";
			confirm_msg = confirm_msg + "<b>Transfer Date: </b>"+$('#transfer_date').val()+ "<br><br>";
			confirm_msg = confirm_msg + " <br>Do you want to submit?";

			$("#model_box").dialog({
				modal: true,
				title: "Message",
				width: 420,
				open: function () {
					$(this).html(confirm_msg);
				},
				buttons : {
					"Yes" : function() {
						$(this).dialog('close');
						if($("#formType").val() == "6" || $("#formType").val() == "26" || $("#formType").val() == "27")
							getReport1Result();
						else
							saveProviderInfo();
					},
					"No" : function() {
						$(this).dialog("close");
						return false;
					}
				}
			});
		}
		else{
			var confirm_msg = "<b>District: </b>"+$('#RzillaName option:selected').text()+ "<br>";

			if($('#upazilaName').is(':visible'))
				var confirm_msg = confirm_msg + "<b>Upazila: </b>"+$('#RupaZilaName option:selected').text()+ "<br>";

			//if(($("#changeType").val() == "" || $("#changeType").val() == "3" || $("#changeType").val() == "4") && $("#formType").val() != "4")
			if($('#unionName').is(':visible'))
				var confirm_msg = confirm_msg + "<b>Union: </b>"+$('#RunionName option:selected').text()+ "<br>";

			if($('#facility_type').is(':visible'))
				var confirm_msg = confirm_msg + "<b>Facility Type: </b>"+$('#facilityType option:selected').text()+ "<br>";

			if($('#db_upload_download').is(':visible'))
				var confirm_msg = confirm_msg + "<b>Database Change Type: </b>"+$('#upload_download_status option:selected').text()+ "<br>";

			if($("#formType").val() == "1" || $("#formType").val() == "2" || ($("#formType").val() == "3" && $("#changeType").val() == "4")){
				var confirm_msg = confirm_msg + "<b>Provider Type: </b>"+$('#providerType option:selected').text()+ "<br>"+
					"<b>Provider Code: </b>"+$('#ProvCode').val()+ "<br>";
			}
			else if($("#formType").val() == "6" || $("#formType").val() == "26")
				var confirm_msg = confirm_msg + "<b>Provider Type: </b>"+$('#providerType option:selected').text()+ "<br>";

			if ($('#provider_id').is(':visible'))
                var confirm_msg = confirm_msg + "<b>Provider: </b>"+$('#providerID option:selected').text()+ "<br>";

            if($("#formType").val() == "3"){
				var confirm_msg = confirm_msg + "<b>Version Given By: </b>"+$('#changeType option:selected').text()+ "<br>"+
					"<b>Provider Type: </b>"+$('#aav_providerType option:selected').text()+ "<br>"+
					"<b>Version Name: </b>"+$('#version').val()+ "<br>";
			}

			if($("#formType").val() == "4"){
				var confirm_msg = confirm_msg + "<b>Retired Date: </b>"+$('#end_date').val()+ "<br>";
			}

			if($("#formType").val() == "1"){
				var confirm_msg = confirm_msg + "<b>Provider Name: </b>"+$('#ProvName').val()+ "<br>"+
					"<b>Provider Mobile No.: </b>"+$('#ProvMob').val()+ "<br>"+
					"<b>Provider Password: </b>"+$('#ProvPass').val()+ "<br>"+
					"<b>Facility Name: </b>"+$('#facilityName option:selected').text()+" ("+$('#facilityName').val()+")"+ "<br>";
			}

			if($("#formType").val() == "11"){
				var confirm_msg = confirm_msg + "<b>Provider Name: </b>"+$('#ProvName').val()+ "<br>"+
					"<b>Provider Mobile No.: </b>"+$('#ProvMob').val()+ "<br>"+
					"<b>Provider Password: </b>"+$('#ProvPass').val()+ "<br>";
					//"<b>Facility Name: </b>"+$('#facilityName option:selected').text()+" ("+$('#facilityName').val()+")"+ "<br>";
			}

			var confirm_msg = confirm_msg + " <br>Do you want to submit?";

			if($("#formType").val() == "6" || $("#formType").val() == "26" || $("#formType").val() == "27")
			{ getReport1Result(); }
			else
			{

				//saveProviderInfo();

				$("#model_box").dialog({
					modal: true,
					title: "Message",
					width: 420,
					open: function () {
						$(this).html(confirm_msg);
					},
					buttons : {
						"Yes" : function() {
							$(this).dialog('close');
							saveProviderInfo();
						},
						"No" : function() {
							$(this).dialog("close");
							return false;
						}
					}
				});

			}
			return false;
		}
	});

	/*Unicode Checking function*/

	function containsNonLatinCodepoints(s) {
		return /[^\u0000-\u00ff]/.test(s);
	}

	var regex = /[^\u0000-\u00ff]/; // Small performance gain from pre-compiling the regex

	function containsDoubleByte(str) {
		if (!str.length) return false;
		if (str.charCodeAt(0) > 255) return true;
		return regex.test(str);
	}

	function hasUnicode (str) {

		var i = 0;
		var str = str.replace(/\s/g, "");
		var len = str.length;

		//alert(len);
		//alert(str);

		var j = 0;
		for (i = 0; i < len; i++) {

			if (str.charCodeAt(i) > 127)
			{
				j = j + 1;
			}

		}
		//alert(j);
		if(j < len)
		{
			//alert("Please write Unicode!");
			return false;
		}
		else
		return true;
	}

	/**** String to Unicode Convertion  *****/

	function toUnicode(theString) {
		var unicodeString = '';
		for (var i=0; i < theString.length; i++) {
			var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
			while (theUnicode.length < 4) {
				theUnicode = '0' + theUnicode;
			}
			theUnicode = '\\u' + theUnicode;
			unicodeString += theUnicode;
		}
		return unicodeString;
	}


	function saveProviderInfo ()
	{
		loadGif();

		var provtype = $('#providerType').val();
		//alert("Provider Type=" +provtype);

		//var preventMultiC = 1;
		var forminfo = new Object();
		forminfo.changetype = $('#changeType').val();
		if(provtype == 999)
		{
			forminfo.divid = $('#RdivName').val();
            forminfo.zillaid = $('#RzillaName').val();
			forminfo.zillaname = "Dhaka";
			forminfo.upazilaid = 99;
			forminfo.unionid = 99;
			forminfo.unionname = "Dhaka";
		}
		else
		{
			forminfo.divid = $('#RdivName').val();
			forminfo.zillaid = $('#RzillaName').val();
			forminfo.zillaname = $('#RzillaName option:selected').text().split(" - ")[1].toLowerCase();
			forminfo.upazilaid = $('#RupaZilaName').val();
			forminfo.unionid = $('#RunionName').val();
			forminfo.unionname = $('#RunionName option:selected').text().split(" - ")[1];

		}
		//alert($('#version').val());
        forminfo.retiredType = $('#retiredType').val();
        forminfo.leaveType = $('#leaveType').val();
        forminfo.leaveComment = $('#leaveComment').val();
		forminfo.providertype = $('#providerType').val();
		forminfo.providercode = $('#ProvCode').val();
		forminfo.providerpass = $('#ProvPass').val();
		forminfo.providername = $('#ProvName').val();
		forminfo.providermobile = $('#ProvMob').val();
		forminfo.facilityid = $('#facilityName').val();
		forminfo.facilityname = $('#facilityName option:selected').text();
		forminfo.facilitytype = $('#facilityType').val();
		forminfo.aav_providerType = $('#aav_providerType').val();
		forminfo.version = $('#version').val();
		forminfo.retireddate = $('#end_date').val();
		forminfo.type = $('#formType').val();
		forminfo.upload_download_status = $("#upload_download_status").val();
		forminfo.providerID = $('#providerID').val();


		if($('#formType').val() == "7" || $('#formType').val() == "10"){
            forminfo.curr_zillaid = $('#distID').val();
            forminfo.curr_upazilaid = $('#upzID').val();
            forminfo.curr_unionid = $('#unID').val();
            forminfo.transfer_date = $('#transfer_date').val();
		}

		//alert(reportObj.report1_union);

        //Add value for new 3 updated info (notice,sql,helpline) for 'Manage provider'-Anjum
        if($('#formType').val() == "12"){
            forminfo.notice = $('#notice_txt_input').val();
            forminfo.sql = $('#sql_txt_input').val();
            forminfo.helpline = $('#helpline_txt_input').val();

        }

        if($('#formType').val() == "5")
		{

			//alert(check);

			//var english = /^[A-Za-z0-9]*$/;

			if($('#UnionFacilityName').val() == "")
			{
				callDialog("Facility Name Can Not Be Empty!");
				return false;
			}
			else
			{

				//forminfo.UnionFacilityName = $('#UnionFacilityName').val(); /* @Author: Evana, @Date: 23/06/2019 */

				var check = toUnicode($('#UnionFacilityName').val());

				//forminfo.UnionFacilityName = check;
				/* @Author: Evana, @Date: 23/06/2019 */

				check = JSON.parse('"' + check + '"');

				forminfo.UnionFacilityName = check;
				//alert("Decoded Text= "+check);
			}

			var url = "saveFacilityInfo";
		}

		else if($('#formType').val() == "8")
			var url = "loadVill";
		else
			var url = "saveProviderInfo";

		$.ajax({
           type: "POST",
           url:url,
           timeout:300000, //60 seconds timeout
           data:{"forminfo":JSON.stringify(forminfo)},
           success: function (response) {
			   if($('#formType').val() == "8")
			   {
                   $(".box-default").addClass("collapsed-box");
                   $(".search_form").hide();
                   // $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

                   $("#reportTable").remove();
                   $("#table_row").show();
                   $("#table_append" ).html(response);

                   var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                   var bottom = $el.position().top + $el.outerHeight(true);
                   $(".main-sidebar").css('height',bottom+"px");
                   //alert(bottom);

                   $('#table_row')[0].scrollIntoView( true );
			   }else if($('#formType').val() == "1"){
                   if (response == "1") {
                       callDialog("<b style='color:green'>Data Entry successfully.</b>");
                       userEntryForm($('#formType').val());
                   }
                   else
                       callDialog("<b style='color:red'>Data could not insert(Provider Id Already Exist or Something Else).</b>");
			   }else if($('#formType').val() == "10"){  // Add Additional facility
				   if (response == "1") {
					   callDialog("<b style='color:green'>Data Entry successfully.</b>");
					   userEntryForm($('#formType').val());
				   }
				   else
					   callDialog("<b style='color:red'>Data could not insert(This provider already assigned into this facility).</b>");
			   }else if($('#formType').val() == "11"){
                   if (response == "1") {
                       callDialog("<b style='color:green'>Data Entry successfully.</b>");
                       userEntryForm($('#formType').val());
                   }
                   else
                       callDialog("<b style='color:red'>Data could not insert(User Id Already Exist or Something Else).</b>");
               }
			   else {
                   if (response == "1") {
                       callDialog("<b style='color:green'>Data Entry successfully.</b>");
                       userEntryForm($('#formType').val());
                   }
                   else
                       callDialog("<b style='color:red'>Data could not insert.</b>");
               }
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}



	function NewDbEntryForm()
	{
		loadGif();

//		var reportObj = new Object();
//		reportObj.type = type;

		$.ajax({
           type: "POST",
           url:"NewDBEntry",
           timeout:300000, //60 seconds timeout
//           data:{"reportInfo":JSON.stringify(reportObj)},
           success: function (response) {
        	   $(".remove_div").remove();
        	   $("#head_title").html('New DB<span> Entry Form</span>');
    		   $( "#append_report" ).html(response);

    		   var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
    		   var bottom = $el.position().top + $el.outerHeight(true);
    		   $(".main-sidebar").css('height',bottom+"px");
    		   //alert(bottom);

			   loadDivision("#RdivName");
    		   //loadZilla("#RzillaName");
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	$(document).on("click", '#NewDBEntryForm' ,function (){
		if($("#RzillaName").val() == "")
			callDialog("Please give the district name.");
		else if($("#RupaZilaName").val() == "")
			callDialog("Please give the upazila name.");
		else if($("#DBUrl").val() != "" && ($("#DBUserName").val() == "" || $("#DBUserPass").val() == ""))
		{
			if($("#DBUserName").val() == "")
				callDialog("Please give the DB username.");

			else if($("#DBUserPass").val() == "")
				callDialog("Please give the DB password.");
		}
		else{
			var confirm_msg = "<b>Division: </b>"+$('#RdivName option:selected').text()+ "<br>"+
							"<b>District: </b>"+$('#RupaZilaName option:selected').text()+ "<br>"+
							"<b>Upazila: </b>"+$('#RupaZilaName option:selected').text()+ "<br>"+
							"<b>DB URL: </b>"+$('#DBUrl').val()+ "<br>"+
							"<b>DB User Name: </b>"+$('#DBUserName').val()+ "<br>"+
							"<b>DB User Password: </b>"+$('#DBUserPass').val()+ "<br>"+
							"<br> <br>Do you want to submit?";
			$("#model_box").dialog({
				modal: true,
			    title: "Message",
			    width: 420,
			    open: function () {
				    $(this).html(confirm_msg);
			   },
			   buttons : {
			        "Yes" : function() {
			        	$(this).dialog('close');
			        	createNewDB();
			        },
			        "No" : function() {
			          $(this).dialog("close");
			          return false;
			        }
			      }
		    });
		}
	});

	function createNewDB()
	{
		loadGif();

		//var preventMultiC = 1;
		var forminfo = new Object();
		forminfo.zilla = $('#RzillaName').val();
		forminfo.zillaname = $('#RzillaName option:selected').text().split(" - ")[1].toLowerCase();
		forminfo.upazila = $('#RupaZilaName').val();
		forminfo.dburl = $('#DBUrl').val();
		forminfo.dbusername = $('#DBUserName').val();
		forminfo.dbuserpass = $('#DBUserPass').val();

		//alert(reportObj.report1_union);

		$.ajax({
           type: "POST",
           url:"CreateDB",
           timeout:300000, //60 seconds timeout
           data:{"forminfo":JSON.stringify(forminfo)},
           success: function (response) {
        	   if(response=="1")
        	   {
	        	   callDialog("<b style='color:green'>Database create successfully.</b>");
	        	   NewDbEntryForm();
        	   }
        	   else
        		   callDialog("<b style='color:red'>Database could not create.</b>");
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	function SymmetricDSConfForm(type)
	{
		loadGif();

//		var reportObj = new Object();
//		reportObj.type = type;

		$.ajax({
           type: "POST",
           url:"SymmetricDSConfForm",
           timeout:300000, //60 seconds timeout
//           data:{"reportInfo":JSON.stringify(reportObj)},
           success: function (response) {
        	   $(".remove_div").remove();

    		   $( "#append_report" ).html(response);

        	   switch(type) {
	    		   case "1":
	    			   $("#head_title").html('Create Symmetric Channel Onetime');
	    			   $("#box_title").html('Create Symmetric Channel Onetime');
	    			   $(".TableName").hide();
	    			   $(".syncType").hide();
	    		        break;
	    		   case "2":
	    			   $("#head_title").html('Create Symmetric Channel');
	    			   $("#box_title").html('Create Symmetric Channel');
	    		        break;
			   }

				$("#formType").val(type);

				var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
				var bottom = $el.position().top + $el.outerHeight(true);
				$(".main-sidebar").css('height',bottom+"px");
				//alert(bottom);
				loadDivision("#RdivName");
				//loadZilla("#RzillaName");
			},
			complete: function() {
				preventMultiCall = 0;
			},
			error: function(xhr, status, error) {
                checkInternetConnection(xhr)
			}
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	/**
	 * @Author: evana
	 * @date: 12/06/2019
	 * @Function: Facility List display
	 * **/

	function FacilityListForm(type)
	{

        loadGif();


//		var reportObj = new Object();
//		reportObj.type = type;

		$.ajax({
			type: "POST",
			url:"FacilityList",
			timeout:300000, //60 seconds timeout
//           data:{"reportInfo":JSON.stringify(reportObj)},
			success: function (response) {
				$(".remove_div").remove();

				$( "#append_report" ).html(response);

				switch(type) {
					case "1":
						$("#head_title").html('View Facility List');
						$("#box_title").html('View Facility List');
						$(".TableName").hide();
						$(".syncType").hide();
						break;

				}

				$("#formType").val(type);

				var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime

				var bottom = $el.position().top + $el.outerHeight(true);
				$(".main-sidebar").css('height',bottom+"px");
				//alert(bottom);
				loadDivision("#RdivName");

			},
			complete: function() {
				preventMultiCall = 0;
			},
			error: function(xhr, status, error) {
                checkInternetConnection(xhr)
			}
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	$(document).on("change", '.upazilaDD', function(){
		var geoInfo = new Object();
		geoInfo.zilla = $('#RzillaName').val();
		geoInfo.upazila = $('#RupaZilaName').val();

		$.ajax({
           type: "POST",
           url:"loadTablelist",
           timeout:300000, //60 seconds timeout
           data:{"geoInfo":JSON.stringify(geoInfo)},
           success: function (response) {
        	   $( "#sts" ).empty();
        	   $( "#sts" ).html(response);
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	});

	$(document).on("click", '#SymmetricDS' ,function (){
		if($("#RzillaName").val() == "")
			callDialog("Please give the district name.");
		else if($("#RupaZilaName").val() == "")
			callDialog("Please give the upazila name.");
		else if($(".tableType:checked").val()=="" && $("#formType").val() == "2")
			callDialog("Please give the table type.");
		else if($("#TableName").val() == "" && $("#formType").val() == "2" && $(".tableType:checked").val()=="3")
			callDialog("Please give the table name.");
		else if($("#syncType").val() == "" && $("#formType").val() == "2")
			callDialog("Please give the sync type.");
		else{
			var confirm_msg = "<b>District: </b>"+$('#RzillaName option:selected').text()+ "<br>"+
							"<b>Upazila: </b>"+$('#RupaZilaName option:selected').text()+ "<br>";
			if($("#formType").val() == "2")
			{
				if($(".tableType:checked").val()=="1")
					confirm_msg += "<b>Table Name: </b>"+$('#tbs').val()+ "<br>";
				else if($(".tableType:checked").val()=="2")
					confirm_msg += "<b>Table Name: </b>"+$('#tsns').val()+ "<br>";
				else
					confirm_msg += "<b>Table Name: </b>"+$('#TableName').val()+ "<br>";
				confirm_msg += "<b>Sync Type: </b>"+$('#syncType option:selected').text()+ "<br>";
			}

				confirm_msg += "<br> <br>Do you want to submit?";

			$("#model_box").dialog({
				modal: true,
			    title: "Message",
			    width: 420,
			    open: function () {
				    $(this).html(confirm_msg);
			   },
			   buttons : {
			        "Yes" : function() {
			        	$(this).dialog('close');
			        	createSymChannel();
			        },
			        "No" : function() {
			          $(this).dialog("close");
			          return false;
			        }
			      }
		    });
		}
	});

	function createSymChannel()
	{
		loadGif();

		//var preventMultiC = 1;
		var forminfo = new Object();
		forminfo.zilla = $('#RzillaName').val();
		forminfo.upazila = $('#RupaZilaName').val();

		if($(".tableType:checked").val()=="1")
			forminfo.tablename = $('#tbs').val();
		else if($(".tableType:checked").val()=="2")
			forminfo.tablename = $('#tsns').val();
		else
			forminfo.tablename = $('#TableName').val();

		forminfo.synctype = $('#syncType').val();
		forminfo.formtype = $('#formType').val();

		//alert(reportObj.report1_union);

		$.ajax({
           type: "POST",
           url:"createSymChannel",
           timeout:300000, //60 seconds timeout
           data:{"forminfo":JSON.stringify(forminfo)},
           success: function (response) {
        	   if(response=="1")
        	   {
	        	   callDialog("<b style='color:green'>Create Symmetric Channel.</b>");
	        	   SymmetricDSConfForm($('#formType').val());
        	   }
        	   else
        		   callDialog("<b style='color:red'>Symmetric Channel could not create.</b>");
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	//login check
	function viewReport(type){

		loadGif();

		var reportObj = new Object();
		reportObj.type = type;
		reportObj.utype = ($("#acc_ty").val()).trim();


		$.ajax({
           type: "POST",
           url:"reportView",
           timeout:300000, //60 seconds timeout
           data:{"reportInfo":JSON.stringify(reportObj)},
           success: function (response) {
        	   /*if(response=='false'){
             		   $("#error_msg").text("Sorry! Wrong information provided.");
           	   }
        	   else{
        		   $("#content").remove();
        		   $( "#wrap" ).after(response);

        		   var headText = $('#facName').text();
        		   var h = $("<header>" + headText + "</header>");
        		   $("#top").prepend(h);

        		   getInitialSetup();
        		   initializeSpecificField();

        	   }*/
        	   //alert(response);
        	   $(".remove_div").remove();
    		   $( "#append_report" ).html(response);

    		   if(type=="1")
			   {
    			   $("#method_type").show();
    			   $("#unionName").hide();
    			   $("#view_type").hide();
			   }
    		   else if(type=="3" || type=="10")
			   {
    			   $("#view_type").show();
    			   $("#unionName").hide();
    			   $("#method_type").show();

//    			   if(type=="3")
//    				   $("#method_type").show();
//    			   else
//    				   $("#method_type").hide();
			   }
    		   else if(type=="4")
			   {
    			   $("#unionName").show();
    			   $("#view_type").hide();
    			   $("#mis3Type").show();
    			   $("#facility_name").show();
    			   $("#facility_type").show();
//    			   $("#districtName").hide();
//    			   $("#upazilaName").hide();
//    			   $("#unionName").hide();
			   }
			   else if(type=="24")
			   {
				   $("#unionName").show();
				   $("#view_type").hide();
				   $("#mis3Type").show();
				   $("#facility_name").show();
				   $("#facility_type").show();
//    			   $("#districtName").hide();
//    			   $("#upazilaName").hide();
//    			   $("#unionName").hide();
			   }
    		   else if(type=="5" || type=="6" || type=="7" || type=="14" )//|| type=="27")
    		   {

//    			   $(".search_form").hide();
//    			   getReport1Result();
    			   $("#unionName").hide();
    			   $("#view_type").hide();
    			   $(".date_type").hide();
    		   }

			   else if(type=="8")
			   {
				   $("#unionName").hide();
				   $("#view_type").hide();
				   $(".date_type").show();
			   }
    		   else if(type=="9")
			   {
    			   $("#unionName").show();
    			   $("#view_type").hide();
    			   $(".date_type").show();
    			   $("#serType").show();
    			   $("#indType").show();
			   }
    		   else if(type=="11")
			   {
    			   $("#method_type").hide();
    			   $("#unionName").hide();
    			   $("#view_type").hide();
			   }
               else if(type=="12")
			   {
				   $("#divisionName").hide();
    			   $("#districtName").hide();
    			   $("#upazilaName").hide();
    			   $("#unionName").hide();
    			   $("#provider_type").show();
			   }
    		   else if(type=="13" || type=="23")
			   {
    			   $("#districtName").show();
    			   $("#upazilaName").show();
    			   $("#unionName").hide();
    			   $("#provider_type").show();
    			   $(".date_type").hide();
    			   if(type=="13")
                       $("#view_level").show();
			   }else if(type=="18"){
    			   $("#districtName").show();
    			   $("#upazilaName").show();
    			   $("#unionName").show();
    			   $("#provider_type").hide();
				   $("#facility_name").show();
    			   $("#submitReport1").val("Download");
			   }else if(type=="16"){
    			   $("#districtName").show();
    			   $("#upazilaName").show();
    			   $("#unionName").show();
				   $("#facility_name").show();
    			   $("#provider_type").hide();

               }else if(type=="17"){
    			   $("#districtName").show();
    			   $("#upazilaName").show();
    			   $("#unionName").show();
				   $("#facility_name").show();
    			   $("#provider_type").hide();

               }else if(type=="19"){
    			   $("#districtName").show();
    			   $("#upazilaName").show();
    			   $("#unionName").show();
				   $("#facility_name").show();
    			   $("#provider_type").hide();

               }else if(type=="20"){
    			   $("#districtName").show();
    			   $("#upazilaName").show();
    			   $("#unionName").show();
				   $("#facility_name").show();
    			   $("#provider_type").hide();
               }else if(type=="21"){
                   $("#districtName").show();
                   $("#upazilaName").show();
                   $("#unionName").show();
                   $("#provider_type").hide();
                   $("#facility_name").show();
                   $("#facility_type").show();
                   $("#submitReport1").val("Download");
               }else if(type=="22"){
                   $("#facility_name").hide();
                   $("#provider_type").hide();
                   $(".levelMonth").hide();
                   $(".levelDate").hide();
                   $("#view_level").show();
               }
               else if(type=="25")
               {
                   $("#unionName").show();
                   $(".date_type").hide();
               }
               else if(type=="26" || type=="27")
               {
                   $("#unionName").hide();
                   $(".levelYear").hide();
                   $(".levelDate").hide();
               }
               else if(type == "28" || type == "29"){
                   $("#view_type").show();
			   }
			   else if(type == "30"){
                   $(".date_type").hide();
                   $(".graph_view_type").show();
                   $("#facility_name").show();
			   }else if(type == "31"){
				   $(".date_type").hide();
				   $("#unionName").show();

			   } else {
    			   $("#unionName").hide();
    			   $("#view_type").hide();
			   }

    		   if($('.date_type').is(':visible')){
                   if(type=="22") {
                       $('#year_type').prop('checked', true);
                       $(".year_wise").show();
                   }
                   else{
                       $('#month_type').prop('checked', true);
                       $(".month_wise").show();
				   }
	           }
	           else if($('.graph_view_type').is(':visible')){
                   $('#daily').prop('checked', true);
                   $(".date_wise").show();
			   }
	           else{
	           	$('#month_type').prop('checked', false);
	           	$(".month_wise").hide();
	           }

    		   switch(type) {
	    		   case "1":
	    			   $("#head_title").html('<strong>Provider Wise Performance </strong>');
	    			   $("#box_title").html('<strong>Provider Wise Performance </strong>');
	    			   //$("#table_title").html('<strong>Provider Wise</strong> Report');
	    		        break;
	    		   case "2":
	    			   $("#head_title").html('<strong>MNC Report </strong><span> Aggregate</span>');
	    			   $("#box_title").html('<strong>MNC Report </strong>Aggregate');
	    			   $("#table_title").html('<strong>Aggregate</strong> MNC Report');
	    		        break;
	    		   case "3":
	    			   $("#head_title").html('<strong>Service Statistics</strong>');
	    			   $("#box_title").html('<strong>Service Statistics</strong>');
	    			   //$("#table_title").html('<strong>Service Statistics Report</strong>');
	    		        break;
	    		   case "4":
	    			   $("#head_title").html('<strong>MIS-3</strong>');
	    			   $("#box_title").html('<strong>MIS-3 Report </strong>');
	    			   //$("#table_title").html('<strong>MIS-3</strong>');
	    		        break;
	    		   case "5":
	    			   $("#head_title").html('<strong>Upazila Base Performance</strong>');
	    			   $("#box_title").html('<strong>Upazila Performance (Month Wise) </strong>');
	    			   $("#table_title").html('<strong>Upazila Performance (Month Wise)</strong>');
	    		        break;
	    		   case "6":
	    			   $("#head_title").html('<strong>View ANC Last 7 Days</strong>');
	    			   $("#box_title").html('<strong>View ANC Last 7 Days </strong>');
	    			   //$("#table_title").html('<strong>View ANC Last 7 Days</strong>');
	    			   $(".anc7d").html('ANC 7 Days');
	    		        break;
	    		   case "7":
	    			   $("#head_title").html('<strong>View Delivery Issues for Last 3 Months</strong>');
	    			   $("#box_title").html('<strong>View Delivery Issues for Last 3 Months </strong>');
	    			   //$("#table_title").html('<strong>View Delivery Issue for Last 3 Months</strong>');
	    			   $(".delivery3m").html('Delivery Issue 3 Months');
	    		        break;
	    		   case "8":
	    			   $("#head_title").html('<strong>MIS-4</strong>');
	    			   $("#box_title").html('<strong>MIS-4 Report </strong>');
	    			   $("#table_title").html('<strong>MIS-4</strong>');
	    		        break;
	    		   case "9":
	    			   $("#head_title").html('<strong>Upazila Performance By Selected Indicators</strong>');
	    			   $("#box_title").html('<strong>Upazila Performance By Selected Indicators</strong>');
	    			   //$("#table_title").html('<strong>Monitoring Services</strong>');
	    		        break;
	    		   case "10":
	    			   $("#head_title").html('<strong>Facility & Satellite Clinic Report</strong>');
	    			   $("#box_title").html('<strong>Facility & Satellite Clinic Report</strong>');
	    			   //$("#table_title").html('<strong>Maternal and Child Health</strong>');
	    		        break;
	    		   case "11":
	    			   $("#head_title").html('<strong>CSBA Performance</strong>');
	    			   $("#box_title").html('<strong>CSBA </strong>Performance');
	    		        break;
	    		   case "12":
	    			   $("#head_title").html('<strong>View Monitoring Tool Login Info</strong>');
	    			   $("#box_title").html('<strong>View Monitoring Tool Login Info</strong>');
	    		        break;
	    		   case "13":
	    			   $("#head_title").html('<strong>View Provider Login Info</strong>');
	    			   $("#box_title").html('<strong>View Provider Login Info</strong>');
	    			   $("#table_title").html('<strong>View Provider Login Info</strong>');
	    		        break;
	    		   case "14":
	    			   $("#head_title").html('<strong>View Synchronization Status (SymmetricDS)</strong>');
	    			   $("#box_title").html('<strong>View Synchronization Status (SymmetricDS)</strong>');
	    			   $("#table_title").html('<strong>View Synchronization Status (SymmetricDS)</strong>');
	    		        break;


				   case "18":
	    			   $("#head_title").html('<strong>মা ও নবজাতক সেবা রেজিস্টার</strong>');
	    			   $("#box_title").html('<strong>মা ও নবজাতক সেবা রেজিস্টার</strong>');
	    		        break;

	    		   case "16":
	    			   $("#head_title").html('<strong>গর্ভনিরোধক খাবার বড়ি, কনডম ও ইসিপি রেজিস্টার</strong>');
	    			   $("#box_title").html('<strong>গর্ভনিরোধক খাবার বড়ি, কনডম ও ইসিপি রেজিস্টার</strong>');
	    		        break;
	    		   case "17":
	    			   $("#head_title").html('<strong>সাধারন রোগী রেজিস্টার</strong>');
	    			   $("#box_title").html('<strong>সাধারন রোগী রেজিস্টার</strong>');
	    		        break;
	    		   case "19":
	    			   $("#head_title").html('<strong>ইঞ্জেক্টেবল রেজিস্টার</strong>');
	    			   $("#box_title").html('<strong>ইঞ্জেক্টেবল রেজিস্টার</strong>');
	    		        break;
	    		   case "20":
	    			   $("#head_title").html('<strong>আই ইউ ডি রেজিস্টার</strong>');
	    			   $("#box_title").html('<strong>আই ইউ ডি রেজিস্টার</strong>');
	    		        break;
                   case "21":
                       $("#head_title").html('<strong>ইমপ্ল্যান্ট রেজিস্টার </strong>');
                       $("#box_title").html('<strong>ইমপ্ল্যান্ট রেজিস্টার </strong>');
                       break;
                   case "22":
                       $("#head_title").html('<strong>Disease Trend</strong>');
                       $("#box_title").html('<strong>Disease Trend</strong>');
                       break;
                   case "23":
                       $("#head_title").html('<strong>View Registration Status</strong>');
                       $("#box_title").html('<strong>View Registration Status</strong>');
                       break;
				   case "25":
					   $("#head_title").html('<strong>Pregnant Women List</strong>');
					   $("#box_title").html('<strong>Pregnant Women List </strong>');
					   break;
                   case "26":
                       $("#head_title").html('<strong>MIS3 Submission Information</strong>');
                       $("#box_title").html('<strong>MIS3 Submission Information</strong>');
                       break;
                   case "27":
                       $("#head_title").html('<strong>Satelite Planning Information</strong>');
                       $("#box_title").html('<strong>Satelite Planning Information</strong>');
                       break;
                   case "28":
                       $("#head_title").html('<strong>QED Indicator</strong>');
                       $("#box_title").html('<strong>QED Indicator</strong>');
                       break;
                   case "29":
                       $("#head_title").html('<strong>Flu Like Symptom</strong>');
                       $("#box_title").html('<strong>Flu Like Symptom</strong>');
                       break;
                   case "30":
                       $("#head_title").html('<strong>Flu Like Symptom Trend</strong>');
                       $("#box_title").html('<strong>Flu Like Symptom Trend</strong>');
                       break;
				   case "31":
					   $("#head_title").html('<strong>Client Information</strong>');
					   $("#box_title").html('<strong>Client Information </strong>');
					   break;
    		   }


    		   var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
    		   var bottom = $el.position().top + $el.outerHeight(true);
    		   $(".main-sidebar").css('height',bottom+"px");
    		   //alert(bottom);
			   loadDivision("#RdivName");
    		   //loadZilla("#RzillaName");
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
				//alert(xhr.status);
        	   	//alert(status);
				//alert(error);
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	$(document).on("click", '#submitReport1' ,function (){
        if($('#reportType').val()=="4" && ($("#facilityName").val()=="" || $("#facilityName").val()==null)){
            callDialog("Please select facility name.");
		}else if($('#reportType').val()=="newborn"){
           getNewbornReport();
        }else if($('#reportType').val()=="hrinfo"){
            viewHRInformation(); //hrinfo
        }
        else if($('#reportType').val()=="storageinfo"){

            viewStorageInformation();
        }
        else if($('#reportType').val()=="synchronizationstatus"){

            if($('#TransferDate').is(':visible') && $("#end_date").val() == "") {
                callDialog("Please set a date.");
            }
            else {
                viewSynchronizationStatus();
            }
        }
        else
			getReport1Result();
	});


	function getReport1Result(){
		loadGif();

		//var preventMultiC = 1;
		var reportObj = new Object();
		reportObj.reportViewType = $('#reportViewType').val();
        reportObj.reportViewLevel = $('#reportViewLevel').val();
		reportObj.viewServiceBy = $('#viewServiceBy').val();
		reportObj.report1_division = $('#RdivName').val(); /*  @Author: evana: @Date: 11/06/2019 */
		reportObj.report1_zilla = $('#RzillaName').val();
		reportObj.report1_upazila = $('#RupaZilaName').val();
		reportObj.report1_union = $('#RunionName').val();
		reportObj.report1_month = $('#monthSelect').val();
        reportObj.report1_year = $('#yearSelect').val();
		reportObj.report1_start_date = $('#start_date').val();
		reportObj.report1_end_date = $('#end_date').val();
		reportObj.type = (($('#reportType').val()!="")?$('#reportType').val():"0");
		reportObj.formType = (($("#formType").val()!="")?$("#formType").val():"0");
		reportObj.serviceType = $('#serviceType').val();
		reportObj.indicatorType = $('#indicatorType').val();
		reportObj.methodType = $('#methodType').val();
		reportObj.sbaType = $('#sbaType').val();
		reportObj.deliveryPlace = $('#deliveryPlace').val();
		reportObj.providertype = $('#providerType').val();
		reportObj.mis3ViewType = $('input[name=mis3ViewType]:checked').val();
		reportObj.facilityId = $('#facilityName').val();
        reportObj.facilityName = $('#facilityName option:selected').text();
		reportObj.facilityType = $('#facilityType option:selected').text();
		if($('#reportType').val()!="4" || $('#reportType').val()!="21")
        	reportObj.utype = ($("#acc_ty").val()).trim();
		if($('#reportType').val()=="4") {
            reportObj.ucode = ($("#ProviderID").text()).trim();
            reportObj.mis3MonitoringTool = "1";
            // Coded By Evana: 03/01/2019
			reportObj.mis3NewMonitoringTool = "3";
        }

		if($('#reportType').val()=="25"){
			reportObj.fromAge = $('#fromAge :selected').text();
			reportObj.toAge = $('#toAge :selected').text();
		}

        if($('#reportType').val()=="3") {
            if($('#aggregate_report').is(':checked')) {
                reportObj.reportViewOption = "1";
            }else if($('#aggregate_report_monthwise').is(':checked')) {
                reportObj.reportViewOption = "2";
            }
        }

		if($('#formType').val() == "addcsba"){
			reportObj.communityActive = $('#community_active:checked').val();
			reportObj.versionNo = $('#version_no').val();


		}

		reportObj.providerId = "";
		reportObj.csba = "";
        reportObj.facilityid = $('#facilityName').val();
		if($('#month_type').is(':checked')) { reportObj.report1_dateType = "1"; }
		else if($('#date_type').is(':checked')) { reportObj.report1_dateType = "2"; }
		else if($('#year_type').is(':checked')) { reportObj.report1_dateType = "3"; }

        if($('#monthly').is(':checked')) { reportObj.report1_graphViewType = "1"; }
        else if($('#weekly').is(':checked')) { reportObj.report1_graphViewType = "2"; }
        else if($('#daily').is(':checked')) { reportObj.report1_graphViewType = "3"; }

		if($('#reportType').val()=="28")
            reportObj.methodType = "4";
		else if($('#reportType').val()=="29")
            reportObj.methodType = "5";

		//Show a error message for invalid form submit(Anjum)
		var numOfNonValueFields = $("span[id^=select2][title*=Select],input[type=text].form-control").filter(function(){
            /**
             * 2nd condition for new ui change of service statistics.
             * it got disabled element for element #RdivName
             */
            if($(this).is(":visible") && !($(this).parents().get(2).disabled == true) ){
                 //&& $(this).prop('class').match(/disabled/gi) === null
            	 // if($(this).parents("span[class*=disabled]").prop("disabled")==true){
            	 // 	return false;
				 // }else {
                     return ($(this).text().replace(/.*\bselect\b.*/gi, "") || $(this).val()).length < 1;
                 // }
            }
            return false;
        }).length;

		if(numOfNonValueFields>0){
            callDialog("Please fill out all the required fields")

		}else {
            $.ajax({
                type: "POST",
                url: "report1Result",
                timeout: 300000, //60 seconds timeout
                data: {"report1Info": JSON.stringify(reportObj)},
                success: function (response) {
                        $(".box-default").addClass("collapsed-box");
                        $(".search_form").hide();
                        // $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

                        $("#reportTable").remove();
                        $("#table_row").show();
                        if ($("#reportType").val() == '18') {
                            $("#table_append").html(response);
                        } else {
                            $("#table_append").html(response);
                        }

                        // console.log($("#graphResultSet").val());
                        // console.log($("#mapResultSet").val());

                        if ($("#reportType").val() != "1" && $("#reportType").val() != "3" && $("#reportType").val() != "11" && $("#reportType").val() != "22" && $("#reportType").val() != "30")
                            $("#graphTable").remove();

                        if ($("#reportType").val() == "1" || $("#reportType").val() == "3" || $("#reportType").val() == "11" || $("#reportType").val() == "22" || $("#reportType").val() == "30") {
                            // if($('#methodType').val() == "1" || $("#reportType" ).val() == "11")
                            //    getReportGraph(JSON.parse($("#graphResultSet").val()));
                            // else if($('#methodType').val() == "2")
                            //    getReportGraph(JSON.parse($("#graphResultSet").val()));
                            // else if($('#methodType').val() == "3")
                            //    getReportGraph(JSON.parse($("#graphResultSet").val()));
                            getReportGraph(JSON.parse($("#graphResultSet").val()), "chartContainer");
                        }

                        if ($("#reportType").val() > 0) {
                            var division_value = $('#RdivName option:selected').text().split(" - ")[1];
                            var zilla_value = $('#RzillaName option:selected').text().split(" - ")[1];
                            var upz_value = $('#RupaZilaName option:selected').text().split(" - ")[1];

                            console.log("upazila value=" + upz_value);

                            var union_value = $('#RunionName option:selected').text().split(" - ")[1];

                            var months_value = {
                                '01': 'Jan',
                                '02': 'Feb',
                                '03': 'Mar',
                                '04': 'Apr',
                                '05': 'May',
                                '06': 'Jun',
                                '07': 'Jul',
                                '08': 'Aug',
                                '09': 'Sep',
                                '10': 'Oct',
                                '11': 'Nov',
                                '12': 'Dec'
                            };

                            if ($("#reportType").val() != "25" && $("#reportType").val() != "5") { // check if its not pregnanat women list

                                var yearly = $('#yearSelect').val();

                                var month_value = months_value[$('#monthSelect').val().split("-")[1]];
                                var year_value = $('#monthSelect').val().split("-")[0];

                                var month_value_fs = months_value[$('#start_date').val().split("-")[1]];
                                var year_value_fs = $('#start_date').val().split("-")[0];
                                var day_value_fs = $('#start_date').val().split("-")[2];

                                var month_value_ls = months_value[$('#end_date').val().split("-")[1]];
                                var year_value_ls = $('#end_date').val().split("-")[0];
                                var day_value_ls = $('#end_date').val().split("-")[2];
                            }
                        }

                        if ($("#reportType").val() == "6") {
                            $("#head_title").html('View ANC Last 7 Days (' + $('#RupaZilaName option:selected').text() + ')');
                            $("#box_title").html('<strong>View ANC Last 7 Days (' + $('#RupaZilaName option:selected').text() + ') </strong>');
                            $("#table_title").html('<strong>View ANC Last 7 Days (' + $('#RupaZilaName option:selected').text() + ')</strong>');
                            $(".anc7d").html('ANC 7 Days (' + $('#RupaZilaName option:selected').text() + ')');
                        }
                        else if ($("#reportType").val() == "7") {
                            $("#head_title").html('View Delivery Issues for Last 3 Months (' + $('#RupaZilaName option:selected').text() + ')');
                            $("#box_title").html('<strong>View Delivery Issues for Last 3 Months (' + $('#RupaZilaName option:selected').text() + ')</strong>');
                            $("#table_title").html('<strong>View Delivery Issue for Last 3 Months (' + $('#RupaZilaName option:selected').text() + ')</strong>');
                            $(".delivery3m").html('Delivery Issue 3 Months <br> (' + $('#RupaZilaName option:selected').text() + ')');
                        }
                        else if ($("#reportType").val() == "5") {
                            $("#box_title").html('Upazila Performance(Month Wise)');
                            $("#table_title").html('');
                        }
                        else if ($("#reportType").val() == "1" || $("#reportType").val() == "9") {
                            if ($("#reportType").val() == "1") {
                                $("#box_title").html('<strong>Provider Performance</strong> Report');
                                $("#table_title").html('');
                            }

                            else {
                                $("#box_title").html('<strong>Upazila Performance By Selected Indicators</strong>');
                                $("#table_title").html('');
                            }


                            if ($('#year_type').is(':checked')) {
                                if ($('#RunionName').val() != null) {
                                    $("#box_title").append(" (" + union_value + ", " + upz_value + ", " + zilla_value + ") (Year of " + yearly + ")");
                                    //$("#table_title").append(" (" + union_value + ", " + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");

                                }
                                else if ($('#RupaZilaName').val() != null && $('#RunionName').val() == null) {
                                    $("#box_title").append(" (" + upz_value + ", " + zilla_value + ") (Year of " + yearly + ")");
                                    //$("#table_title").append(" (" + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                }

                                else {
                                    $("#box_title").append(" (" + zilla_value + ") (Year of " + yearly + ")");
                                    //$("#table_title").append(" (" + zilla_value + ") (" + month_value + ", " + year_value + ")");

                                }
                            }
                            else if ($('#month_type').is(':checked')) {
                                if ($('#RunionName').val() != null) {
                                    $("#box_title").append(" (" + union_value + ", " + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                    //$("#table_title").append(" (" + union_value + ", " + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");

                                }
                                else if ($('#RupaZilaName').val() != null && $('#RunionName').val() == null) {
                                    $("#box_title").append(" (" + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                    //$("#table_title").append(" (" + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                }

                                else {
                                    $("#box_title").append(" (" + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                    //$("#table_title").append(" (" + zilla_value + ") (" + month_value + ", " + year_value + ")");

                                }
                            }
                            else if ($('#date_type').is(':checked')) {
                                if ($('#RunionName').val() != null) {
                                    $("#box_title").append(" (" + union_value + ", " + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                                    //$("#table_title").append(" (" + union_value + ", " + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");

                                }

                                else if ($('#RupaZilaName').val() != null && $('#RunionName').val() == null) {
                                    $("#box_title").append(" (" + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                                    //$("#table_title").append(" (" + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");

                                }
                                else {
                                    $("#box_title").append(" (" + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                                    //$("#table_title").append(" (" + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");

                                }

                            }
                        }
                        else if ($("#reportType").val() == "3" || $("#reportType").val() == "10" || $("#reportType").val() == "11" || $("#reportType").val() == "28" || $("#reportType").val() == "29" || $("#reportType").val() == "30") {
                            var get_geo_val;

                            if ($("#reportType").val() == "3") {
                                $("#box_title").html('<strong>Service Statistics</strong>');
                                $("#table_title").html("");
                            }

                            else if ($("#reportType").val() == "10") {
                                $("#box_title").html('<strong>Facility & Satellite Clinic Report</strong>');
                                $("#table_title").html('');
                            }

                            else if ($("#reportType").val() == "11") {
                                $("#box_title").html('<strong>CSBA Performance</strong>');
                                $("#table_title").html('');
                            }
                            else if ($("#reportType").val() == "28") {
                                $("#box_title").html('<strong>QED Indicator</strong>');
                                $("#table_title").html('');
                            }
                            else if ($("#reportType").val() == "29") {
                                $("#box_title").html('<strong>Flu Like Symptom Report</strong>');
                                $("#table_title").html('');
                            }
                            else if ($("#reportType").val() == "30") {
                                $("#box_title").html('<strong>Flu Like Symptom Trend</strong>');
                                $("#table_title").html('');
                            }


                            if ($('#reportViewType').val() == "1")
                                get_geo_val = "District Wise";
                            else if ($('#RdivName option:selected').text() == "All")
                                get_geo_val = "All Districts";
                            else if ($('#reportViewType').val() == "2")
                                get_geo_val = zilla_value;
                            else
                                get_geo_val = zilla_value + ", " + upz_value;

                            if ($('#year_type').is(':checked')) {

                                $("#box_title").append(" (" + get_geo_val + ") (Year of " + yearly + ")");
                                //$("#table_title").append(" ("+get_geo_val+") ("+month_value+", "+year_value+")");
                            }
                            else if ($('#month_type').is(':checked')) {

                                $("#box_title").append(" (" + get_geo_val + ") (" + month_value + ", " + year_value + ")");
                                //$("#table_title").append(" ("+get_geo_val+") ("+month_value+", "+year_value+")");
                            }
                            else if ($('#date_type').is(':checked')) {
                                $("#box_title").append(" (" + get_geo_val + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                                //$("#table_title").append(" ("+get_geo_val+") ("+day_value_fs+" "+month_value_fs+", "+year_value_fs+" - "+day_value_ls+" "+month_value_ls+", "+year_value_ls+")");
                            }
                            else if ($('#monthly').is(':checked')) {
                                $("#box_title").append(" (" + get_geo_val + ") (Month wise report of year " + yearly + ")");
                            }
                            else if ($('#weekly').is(':checked')) {
                                $("#box_title").append(" (" + get_geo_val + ") (Week wise report of month " + month_value + ", " + year_value + ")");
                            }
                            else if ($('#daily').is(':checked')) {
                                $("#box_title").append(" (" + get_geo_val + ") (Daily report from " + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " to " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                            }
                        }
                        else if ($("#reportType").val() == "4") {
                            if ($('#year_type').is(':checked')) {
                                if ($('#RunionName').val() != null && $('#RunionName').val() != "") {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + union_value + ", " + upz_value + ", " + zilla_value + ") (Year of " + yearly + ")");
                                    $("#table_title").html("");
                                    //$("#table_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + union_value + ", " + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                }
                                else if ($('#RupaZilaName').val() != "" && $('#RupaZilaName').val() != null) {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + upz_value + ", " + zilla_value + ") (Year of " + yearly + ")");
                                    $("#table_title").html("");

                                }
                                else {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + zilla_value + ") (Year of " + yearly + ")");

                                    $("#table_title").html("");

                                }
                            }
                            else if ($('#month_type').is(':checked')) {
                                if ($('#RunionName').val() != null && $('#RunionName').val() != "") {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + union_value + ", " + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                    $("#table_title").html("");
                                    //$("#table_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + union_value + ", " + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                }
                                else if ($('#RupaZilaName').val() != "" && $('#RupaZilaName').val() != null) {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + upz_value + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");
                                    $("#table_title").html("");

                                }
                                else {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + zilla_value + ") (" + month_value + ", " + year_value + ")");

                                    $("#table_title").html("");

                                }
                            }
                            else if ($('#date_type').is(':checked')) {
                                if ($('#RunionName').val() != null && $('#RunionName').val() != "") {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + union_value + ", " + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                                    //$("#table_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + union_value + ", " + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");


                                }
                                else if ($('#RupaZilaName').val() != "" && $('#RupaZilaName').val() != null) {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                                    // $("#table_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + upz_value + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");


                                }
                                else {
                                    $("#box_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                                    // $("#table_title").html("<strong>MIS-3</strong> (" + $('#facilityType option:selected').text() + ", " + zilla_value + ") (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");


                                }
                            }
                        }
                        // For new MIS3Report
                        else if ($("#reportType").val() == "12") {
                            if ($('#year_type').is(':checked')) {
                                $("#table_title").html("");
                                $("#box_title").html("<strong>Monitoring Tool </strong>Login Information (Year of " + yearly + ")");

                            }
                            else if ($('#month_type').is(':checked')) {
                                $("#table_title").html("");
                                $("#box_title").html("<strong>Monitoring Tool </strong>Login Information (" + month_value + ", " + year_value + ")");

                            }
                            else if ($('#date_type').is(':checked')) {
                                $("#table_title").html("");
                                $("#box_title").html("<strong>Monitoring Tool </strong>Login Information (" + day_value_fs + " " + month_value_fs + ", " + year_value_fs + " - " + day_value_ls + " " + month_value_ls + ", " + year_value_ls + ")");
                            }

                        }
                        else if ($("#reportType").val() == "13" || $("#reportType").val() == "14" || $("#reportType").val() == "25") {
                            var table_title = "";
                            if ($("#reportType").val() == "13") {
                                //alert(upz_value);
                                //alert(union_value);

                                if (upz_value === undefined && union_value === undefined) {
                                    //alert(upz_value);
                                    $("#table_title").html("");
                                    $("#box_title").html("Provider Login Information (" + zilla_value + ")");
                                }
                                else if (union_value === undefined) {
                                    $("#table_title").html("");
                                    $("#box_title").html("Provider Login Information (" + zilla_value + ", " + upz_value + ")");
                                }
                                else {
                                    $("#table_title").html("");
                                    $("#box_title").html("Provider Login Information (" + zilla_value + ", " + upz_value + ", " + union_value + ")");
                                }

                            }

                            else if ($("#reportType").val() == "14") {
                                $("#table_title").html("");
                                $("#box_title").html("Synchronization Issue (" + zilla_value + ", " + upz_value + ")");
                            }
                            else {
                                $("#table_title").html("");
                                if(union_value==undefined) {
                                    $("#box_title").html("Pregnant Women List (" + zilla_value + ", " + upz_value +")");
                                }else{
                                    $("#box_title").html("Pregnant Women List (" + zilla_value + ", " + upz_value + ", " + union_value + ")");
                                }
                            }

                        }
                        else if ($("#reportType").val() == "28") {
                            var table_title = "";
                            // $("#box_title").html("QED Result ("+zilla_value+", "+upz_value+", "+union_value+")");
                            if ($("#reportType").val() == "28") {

                            }
                        }
                        else if ($("#reportType").val() == "26") {
                            if (upz_value === undefined)
                                $("#box_title").html("MIS3 Submission Information (" + zilla_value + ") (" + month_value + ", " + year_value + ")");
                            else
                                $("#box_title").html("MIS3 Submission Information (" + zilla_value + ", " + upz_value + ") (" + month_value + ", " + year_value + ")");
                        }

                        var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                        var bottom = $el.position().top + $el.outerHeight(true);
                        $(".main-sidebar").css('height', bottom + "px");
                        //alert(bottom);

                        $('#table_row')[0].scrollIntoView(true);

                        // $('table thead').css({"display":"block","position": "sticky","top":0});
                        // $('table thead tr').css({"background":"white"});
                        // $('table').css({"display":"block"});
                        // $('table').removeClass('nowrap');
                        // $('table').removeClass('dataTable');
                        // $('table').addClass('wrap');
                        // // $('table thead tr th:first-child').css({"width":"82px"});
                        // $('table thead tr th').css({"width":"100px","word-wrap": "break-word"});
                        // $('table tbody tr td').css({"width":"100px","word-wrap": "break-word"});
                        // $('table tfoot tr th').css({"width":"100px","word-wrap": "break-word"});
                        // $('table tfoot tr td').css({"width":"100px","word-wrap": "break-word"});
                        //
                        // // $('table tbody tr td:first-child').css({"width":"82px"});
                        // // $('table thead tr th').not(":first").css({"width":"82px"});
                        // // $('table tbody tr td').not(":first").css({"width":"82px"});
                        // $('table thead tr th').removeClass('sorting');
                        // $('table thead tr th').removeClass('sorting_asc');
                        // if(($('#reportType').val()=="3") && $('#methodType').val()=="1")
                        // 	load_corresponding_map(reportObj);
                },
                complete: function () {
                    preventMultiCall = 0;
                },
                error: function (xhr, status, error) {
                    //alert(xhr.status);
                    //alert(status);
                    //alert(error);
                    checkInternetConnection(xhr)
                }
            });
            //alert(preventMultiC);
        }

		return preventMultiCall;
	}

	function getNewbornReport() {
        loadGif();
        var reportObj = new Object();
        reportObj.type = 'newborn';
        //for newborn dashboard
        reportObj.report1_division = $('#RdivName').val();
        reportObj.report1_zilla = $('#RzillaName').val();
        reportObj.report1_upazila = $('#RupaZilaName').val();
        reportObj.report1_union = $('#RunionName').val();
        reportObj.report1_month = $('#monthSelect').val();
        reportObj.report1_year = $('#yearSelect').val();
        reportObj.report1_start_date = $('#start_date').val();
        reportObj.report1_end_date = $('#end_date').val();
        // reportObj.indicatorType = 'DAN' ;
        // reportObj.deliveryPlace = '2';
        reportObj.serviceType="";

        if($('#month_type').is(':checked')) { reportObj.report1_dateType = "1"; }
        else if($('#date_type').is(':checked')) { reportObj.report1_dateType = "2"; }
        else if($('#year_type').is(':checked')) { reportObj.report1_dateType = "3"; }

        $.ajax({
            type: "POST",
            url:"newbornDashboard",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {
                // $(".remove_div").remove();
                $(".box-default").addClass("collapsed-box");
                // $(".search_form").hide();
                $("#table_row").show();
                $("#table_append").html(response);
                // $( "#append_report" ).html(response);
                $("#head_title").html('<strong>Essential Newborn Care</strong>');
                $("#box_title").html('<strong>Essential Newborn Care</strong>');

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                //loadZilla("#RzillaName");

            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;

    }

	function viewApprovalReport(type)
	{
		loadGif();

		var utype = ($("#acc_ty").val()).trim();

		var infoApprovalObj = new Object();
		infoApprovalObj.utype = ($("#acc_ty").val()).trim();
		infoApprovalObj.supervisorId = ($("#ProviderID").text()).trim();
		infoApprovalObj.type = type;

		//console.log($("#ProviderID").text());

		$.ajax({
           type: "POST",
           url:"approval",
           timeout:300000, //60 seconds timeout
           data:{"infoApproval":JSON.stringify(infoApprovalObj)},
           success: function (response) {
        	   $(".remove_div").remove();
    		   $( "#append_report" ).html(response);
    		   $("#approvalType").val(type);

    		   if(type==1)
    		   {
    			   if(utype=="14"){
	    			   $( "#districtName" ).hide();
	    			   $( "#upazilaName" ).hide();
    			   }
    		   }
    		   if(type=="3")//satelite session planning
                   $("#satelite_box").show();

    		   switch(type) {
	    		   case "1":
	    			   $("#head_title").html('MIS3<span> Approval</span>');
	    			   $("#box_title").html('<strong>MIS3 </strong>Approval');
	    		        break;

                   case "3":
                       $("#head_title").html('Satelite Session Planning<span> Approval</span>');
                       $("#box_title").html('<strong>Satelite Session Planning </strong>Approval');
                       break;
			   }

    		   var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
    		   var bottom = $el.position().top + $el.outerHeight(true);
    		   $(".main-sidebar").css('height',bottom+"px");
    		   //alert(bottom);
			   loadDivision("#RdivName");
    		   //loadZilla("#RzillaName");
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
			   checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

	$(document).on("click", '#submitApproval' ,function (){
		getApprovalReport();
	});

	function getApprovalReport(){

		loadGif();

		//var preventMultiC = 1;
		var reportObj = new Object();
		reportObj.report1_zilla = $('#RzillaName').val();
		reportObj.utype = ($("#acc_ty").val()).trim();
		reportObj.facilityId = $('#facilityName').val();
		reportObj.supervisorId = ($("#ProviderID").text()).trim();
		reportObj.report1_month = $('#monthSelect').val();

		//console.log(reportObj);
		$.ajax({
           type: "POST",
           url:"submittedmis3",
           timeout:300000, //60 seconds timeout
           data:{"approvalInfo":JSON.stringify(reportObj)},
           success: function (response) {
        	   //alert(response);
        	   $(".box-default").addClass("collapsed-box");
        	   $(".search_form").hide();
        	   // $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

        	   $("#reportTable").remove();
        	   $("#table_row").show();
    		   $("#table_append" ).html(response);

        	   $("#exportOption").hide();

    		   var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
    		   var bottom = $el.position().top + $el.outerHeight(true);
    		   $(".main-sidebar").css('height',bottom+"px");
    		   //alert(bottom);

    		   $('#table_row')[0].scrollIntoView( true );
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

    $(document).on("click", '.synchronization_status' ,function (){

        viewSynchronizationStatusReport();

    });

    function viewSynchronizationStatusReport()
    {
        loadGif();
        var reportObj = new Object();
        reportObj.status = 'view';
        reportObj.type = 'synchronizationstatus';
        reportObj.delivery= $('#delivery_text').val();
        reportObj.gp= $('#gp_text').val();
        reportObj.anc= $('#anc_text').val();
        reportObj.pillcondom= $('#pillcondom_text').val();
        reportObj.iud= $('#iud_text').val();
        reportObj.pregwomen= $('#pregwomen_text').val();


       /* if($(".delivery:checked").val()=="1" && $(".gp:checked").val()=="2" && $(".anc:checked").val()=="3" && $(".pillcondom:checked").val()=="4" && $(".iud:checked").val()=="5" && $(".pregwomen:checked").val()=="6"){

        }*/

       /* var forminfo = new Object();
        forminfo.zilla = $('#RzillaName').val();
        forminfo.upazila = $('#RupaZilaName').val();
        forminfo.

        if($(".tableType:checked").val()=="1")
            forminfo.tablename = $('#tbs').val();
        else if($(".tableType:checked").val()=="2")
            forminfo.tablename = $('#tsns').val();
        else
            forminfo.tablename = $('#TableName').val();

        forminfo.synctype = $('#syncType').val();
        forminfo.formtype = $('#formType').val();*/




        /*  if($('#zilaName').is(':visible') && $("#RzillaName").val() == "Please Select District")
           callDialog("Please give the zilla name.");*/


        $.ajax({
            type: "POST",
            url:"SynchronizationStatus",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                loadDivision("#RdivName");
                /*if($('#RdivName option:selected').text() =="All") {
                    $('#RzillaName').prop("disabled", true);
                    $('#RupaZilaName').prop("disabled", true);
                }*/
                $("#unionName").hide();

                if($('#RupaZilaName option:selected').text() =="All")
                    $('#RunionName').prop("disabled", true);
                if($('#RdivName option:selected').text() =="All") {
                    $('#RzillaName').prop("disabled", true);
                    $('#RupaZilaName').prop("disabled", true);
                    $('#RunionName').prop("disabled", true);
                }

                // && $("#providerType").val() != "22" && $("#formType").val() != "27" && $("#formType").val() != "28"

            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }


    function viewSynchronizationStatus()
    {
        loadGif();
        var reportObj = new Object();
        reportObj.zilla = $('#RzillaName').val();
        reportObj.upazila = $('#RupaZilaName').val();
        reportObj.division = $('#RdivName').val();
        reportObj.delivery= $('#delivery_text').val();
        reportObj.gp= $('#gp_text').val();
        reportObj.anc= $('#anc_text').val();
        reportObj.pillcondom= $('#pillcondom_text').val();
        reportObj.iud= $('#iud_text').val();
        reportObj.pregwomen= $('#pregwomen_text').val();
        reportObj.end_date= $('#end_date').val();



        $("#unionName").hide();
        reportObj.type = 'synchronizationstatus';

       /* if($('#TransferDate').is(':visible') && $("#end_date").val() == "")
            callDialog("Please give the date.");*/

        $.ajax({
            type: "POST",
            url:"SynchronizationStatus",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {

                // $(".box-default").addClass("collapsed-box");
                // $(".search_form").hide();
                $("#table_row").show();
                $("#table_append").html(response);

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                loadDivision("#RdivName");



            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }

    $(document).on("click", '.storage_information' ,function (){

       /* if($('#zilaName').is(':visible') && $("#RzillaName").val() == "Please Select District")
            callDialog("Please give the zilla name.");*/
        viewStorageReport();

    });

    function viewStorageReport()
    {
        loadGif();
        var reportObj = new Object();
        reportObj.status = 'view';
        reportObj.type = 'storageinfo';


        /*  if($('#zilaName').is(':visible') && $("#RzillaName").val() == "Please Select District")
           callDialog("Please give the zilla name.");*/


        $.ajax({
            type: "POST",
            url:"StorageInformation",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                loadDivision("#RdivName");
                /*if($('#RdivName option:selected').text() =="All") {
                    $('#RzillaName').prop("disabled", true);
                    $('#RupaZilaName').prop("disabled", true);
                }*/
                $("#unionName").hide();

                if($('#RupaZilaName option:selected').text() =="All")
                    $('#RunionName').prop("disabled", true);
                if($('#RdivName option:selected').text() =="All") {
                    $('#RzillaName').prop("disabled", true);
                    $('#RupaZilaName').prop("disabled", true);
                    $('#RunionName').prop("disabled", true);
                }

                // && $("#providerType").val() != "22" && $("#formType").val() != "27" && $("#formType").val() != "28"

            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }

    function viewStorageInformation()
    {
        loadGif();
        var reportObj = new Object();
        reportObj.zilla = $('#RzillaName').val();
        reportObj.upazila = $('#RupaZilaName').val();
        reportObj.division = $('#RdivName').val();
        $("#unionName").hide();
        reportObj.type = 'storageinfo';

        /*if($('#zilaName').is(':visible') && $("#RzillaName").val() == "Please Select District")
            callDialog("Please give the zilla name.");*/

        $.ajax({
            type: "POST",
            url:"StorageInformation",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {

                // $(".box-default").addClass("collapsed-box");
                // $(".search_form").hide();
                $("#table_row").show();
                $("#table_append").html(response);

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                loadDivision("#RdivName");



            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }


    $(document).on("click", '.hr_information' ,function (){
        viewHRReport();

    });

    function viewHRInformation()
    {
        loadGif();
        var reportObj = new Object();
        reportObj.zilla = $('#RzillaName').val();
        reportObj.upazila = $('#RupaZilaName').val();
        reportObj.division = $('#RdivName').val();

        reportObj.type = 'hrinfo';

        $.ajax({
            type: "POST",
            url:"hrInformation",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {

                // $(".box-default").addClass("collapsed-box");
                // $(".search_form").hide();
                $("#table_row").show();
                $("#table_append").html(response);

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                loadDivision("#RdivName");
                //loadZilla("#RzillaName");
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }

    function viewHRReport()
    {
        loadGif();
        var reportObj = new Object();
        reportObj.status = 'view';
        reportObj.type = 'hrinfo';

        $.ajax({
            type: "POST",
            url:"hrInformation",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                loadDivision("#RdivName");
                if($('#RdivName option:selected').text() =="All") {
                    $('#RzillaName').prop("disabled", true);
                    $('#RupaZilaName').prop("disabled", true);
                }
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }



    $(document).on("click", '.newborn_report' ,function (){
        viewNewbornReport();

    });

    function viewNewbornReport()
    {
        loadGif();
        var reportObj = new Object();
        reportObj.type = 'newborn';

        $.ajax({
            type: "POST",
            url:"reportView",
            data:{"reportInfo":JSON.stringify(reportObj)},
            timeout:300000, //60 seconds timeout
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);



                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
                loadDivision("#RdivName");
                console.log("mm")

                if($('#RupaZilaName option:selected').text() =="All")
                    $('#RunionName').prop("disabled", true);
                if($('#RdivName option:selected').text() =="All") {
                    $('#RzillaName').prop("disabled", true);
                    $('#RupaZilaName').prop("disabled", true);
                    $('#RunionName').prop("disabled", true);
                }

                //loadZilla("#RzillaName");
            },
            complete: function() {

                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }


	$(document).on("click", '.client_details' ,function (){
		viewClientDetailsReport();

	});

	function viewClientDetailsReport(){
		loadGif();

		$.ajax({
			type: "POST",
			url:"clientInformation",
			timeout:300000, //60 seconds timeout
			success: function (response) {
				$(".remove_div").remove();
				$( "#append_report" ).html(response);

				var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
				var bottom = $el.position().top + $el.outerHeight(true);
				$(".main-sidebar").css('height',bottom+"px");
				//alert(bottom);
				loadDivision("#RdivName");
				//loadZilla("#RzillaName");
			},
			complete: function() {
				preventMultiCall = 0;
			},
			error: function(xhr, status, error) {
				checkInternetConnection(xhr)
			}
		});
		//alert(preventMultiC);
		return preventMultiCall;

	}

    $(document).on("click", '.facility_information' ,function (){
        viewFacilityInformation();
    });


    function viewFacilityInformation()
    {
        loadGif();

        $.ajax({
            type: "POST",
            url:"facilityInformation",
            timeout:300000, //60 seconds timeout
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);
				loadDivision("#RdivName");
                //loadZilla("#RzillaName");
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }

	function load_corresponding_map(obj) {
		load_dis(obj.report1_year,obj.report1_month,obj.report1_start_date,obj.report1_end_date);
		dist_id = obj.report1_zilla;

		//alert("Dist ID="+dist_id);

		//@Author: Evana, @Date: 03/07/2019
		// divData = JSON.parse($('#divisionSet').val());
		// var divis = Object.keys(divData);
		// var temp = [];
		// var division_id;
		// var index;
		//
		// for (index = 0; index < divis.length; ++index) {
		// 	temp = divis[index].split("-");
		// 	if(temp[0] == dist_id) {
		// 		division_id = temp[1];
		// 		//alert("division id= " + division_id);
		// 		index = divis.length ;
		// 		break;
		// 	}
		// 	temp = [];
		// }

		//alert(division_id);
        // if($("#reportViewType").val()!="1")
        // 	dist_name = zillaJS[0][dist_id]["nameEnglish"];
        // else
        //     dist_name = "";
        zillaJS = JSON.parse(division.replace(/\s+/g,""));

        dist_name = zillaJS[0][dist_id]["nameEnglish"];
		//dist_name = zillaJS[0][division_id]["Zilla"][dist_id]["nameEnglish"];
        dist_name = dist_name.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        if(obj.report1_upazila != ""){
            var geoInfo = new Object();
            geoInfo.zilla = dist_id;
            geoInfo.upazila = 0;
            geoInfo.type = "2";
			geoInfo.year = obj.report1_year;
            geoInfo.month = obj.report1_month;
            geoInfo.start = obj.report1_start_date;
            geoInfo.end = obj.report1_end_date;
            $.ajax({
                type: "POST",
                async: false,
                global: false,
                url: 'mapInfo',
                data: {"geoInfo":JSON.stringify(geoInfo)},
                dataType: "json",
                success: function (data) {
                    upazila_data = data;
                    $('#loading_div').html("");
                    $('#loading_div').hide();
                }
            });
            $("svg").remove();
            upz_id =  obj.report1_upazila;
            upz_name = zillaJS[0][division_id]["Zilla"][dist_id]["Upazila"][upz_id]["nameEnglishUpazila"];
            upz_name = upz_name.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
            type_name = "union";
            load_union(upz_name, dist_id, upz_id,obj.report1_year,obj.report1_month,obj.report1_start_date,obj.report1_end_date);
        }else{
            $("svg").remove();
            load_upz(dist_name, dist_id,obj.report1_year,obj.report1_month,obj.report1_start_date,obj.report1_end_date);
            type_name = "upz";
		}
    }

    $(document).on("click",'.downloadDB', function(){
        var downloadInfo = new Object();
        downloadInfo.dbName = "RHIS_"+(($('#RzillaName').val()<10)?"0"+$('#RzillaName').val():$('#RzillaName').val())+"_"+(($('#RupaZilaName').val()<10)?"0"+$('#RupaZilaName').val():$('#RupaZilaName').val());
        //alert(downloadInfo);
        $.ajax({
            type: "POST",
            url:"downloadDB",
            timeout:300000, //60 seconds timeout
            data:{"downloadInfo":JSON.stringify(downloadInfo)},
            dataType: "json",
            success: function (data) {
                console.log(data);
            }
        });
    });

    var population_data = (function () {
        var json = null;
        var geoInfo = new Object();
        geoInfo.zilla = 36;
        geoInfo.upazila = 0;
        geoInfo.type = "0";
        $.ajax({
            'type': "POST",
            'async': false,
            'global': false,
            'url': 'mapInfo',
            'data': {"geoInfo":JSON.stringify(geoInfo)},
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();

    function getReportGraph(response, div_id, graph_of, graph_title)
    {
        var dataPoints_preg_women = [];
        var title_name;
        var xaxis_title;
        var labelFont;
        var valueFormatString;
        var interval;
        var intervalType;
        var elco_data = [];
        var total_population = [];

        zillaJS = JSON.parse(division.replace(/\s+/g,""));


        if($("#reportType" ).val() == "1") {
            title_name = "Provider Wise Service Statistics";
            xaxis_title = "Provider Name";
            labelFont = 14;
        }
        else if($("#reportType" ).val() == "11")
        {
            title_name = "Provider Wise Service Statistics (CSBA)";
            xaxis_title = "Provider Name";
            labelFont = 14;
        }
        else if($("#reportType" ).val() == "22")
        {
            title_name = "Disease Statistics By Year Month";
            xaxis_title = "Year Month";
            labelFont = 14;
            valueFormatString = "MMM";
			interval = 1;
            intervalType = "month";
        }
        else if($("#reportType" ).val() == "30")
        {
            title_name = "Flu Like Symptom Trend";
            xaxis_title = "Monthly/Weekly/Daily";
            labelFont = 14;
            valueFormatString = "MMM";
            interval = 1;
            intervalType = "month";
        }
        else if($("#dashboardView" ).val() == "1")
        {
            title_name = graph_of + " " + graph_title + " Service Statistics";
            xaxis_title = "Location Name";
            labelFont = 14;
        }
        else if($("#reportType" ).val() == "3") {
        	if($("#reportViewType" ).val() == "1") {
                title_name = "District Wise Service Statistics";
                xaxis_title = "District Name";
            }
        	else if($("#reportViewType" ).val() == "2") {
                title_name = "Upazila Wise Service Statistics";
                xaxis_title = "Upazila Name";
            }
            else if($("#reportViewType" ).val() == "3") {
                title_name = "Union Wise Service Statistics";
                xaxis_title = "Union Name";
            }
            else if($("#reportViewType" ).val() == "4") {
                title_name = "Facility Wise Service Statistics";
                xaxis_title = "Facility Name";
            }

            var start_date = "";
            var end_date = "";

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();

            if($('#year_type').is(':checked')) {
                start_date = $('#yearSelect').val()+"-01-01";
                if($('#yearSelect').val()==yyyy)
                    end_date = $('#yearSelect').val()+"-"+mm+"-"+dd;
                else
                    end_date = $('#yearSelect').val()+"-12-31";
            }
            else if($('#month_type').is(':checked')){
                var month_year = ($('#monthSelect').val()).split("-");
                start_date = $('#monthSelect').val()+"-01";
                if(month_year[0]==yyyy && month_year[1]==mm)
                    end_date = $('#monthSelect').val()+"-"+dd;
                else
                    end_date = $('#monthSelect').val()+"-"+(new Date(month_year[0], month_year[1], 0).getDate());
            }
            else if($('#date_type').is(':checked')){
                start_date = $('#start_date').val();
                end_date = $('#end_date').val();
            }

            var num_days = Math.round((new Date(end_date)-new Date(start_date))/(1000*60*60*24));

            division_id = $('#RdivName').val();
            zilla_id = $('#RzillaName').val();
            upzila_id = $('#RupaZilaName').val();
            mapData = JSON.parse($('#mapResultSet').val());
			console.log(response);
            var index;
            var temp = [];

            $.each(response[0], function (response_data_key, response_data_value) {
                $.each(response_data_value, function (i) {
                    for (var key in mapData) {
                        if (mapData.hasOwnProperty(key)) {
                            temp = [];
                            if($("#reportViewType" ).val() == "1") {
                                divData = JSON.parse($('#divisionSet').val());
                                var divis = Object.keys(divData);
                                for (index = 0; index < divis.length; ++index) {
                                    temp = divis[index].split("-");

                                    if (temp[0] == key) {
                                        division_id = temp[1];
                                        index = divis.length;
                                        break;
                                    }
                                }
                            }

                            var check_name = "";
                            var item_name = ($("#reportViewType" ).val() == "4")?i.split("_")[2]:i;

                            if($("#reportViewType" ).val() == "1")
                            	check_name = zillaJS[0][temp[1]]["Zilla"][key]["nameEnglish"];
                            else if($("#reportViewType" ).val() == "2")
                                check_name = zillaJS[0][division_id]["Zilla"][zilla_id]['Upazila'][key.split('_')[1]]['nameEnglishUpazila'];
                            else if($("#reportViewType" ).val() == "3" || $("#reportViewType" ).val() == "4")
                                check_name = zillaJS[0][division_id]["Zilla"][zilla_id]['Upazila'][upzila_id]['Union'][key.split('_')[2]]['nameEnglishUnion'];

                            if (check_name == item_name.replace(/\s/g,'')) {
                                try {
                                	if($("#reportViewType" ).val() == "4") {
                                		var key_val = key.split("_");
                                        key = key_val.slice(0, key_val.length - 1).join("_");
                                    }

                                    if($('#methodType').val() == "1") {
                                        dataPoints_preg_women.push({
                                            label: ($("#reportViewType" ).val() == "4")?i.split("_")[0]:i,
                                            y: parseInt(Math.round((((population_data[key]['Total'] * (22.8 / 1000)) * 1.15) / 365) * num_days))
                                        });
                                    }
                                    else if($('#methodType').val() == "2"){
                                        elco_data.push({
                                            label: ($("#reportViewType" ).val() == "4")?i.split("_")[0]:i,
                                            y: parseInt(Math.round(((population_data[key]['Total'] * 0.224) / 365) * num_days))
                                        });
									}
                                    else if($('#methodType').val() == "3"){
                                        total_population.push({
                                            label: ($("#reportViewType" ).val() == "4")?i.split("_")[0]:i,
                                            y: parseInt(Math.round(((population_data[key]['Total']) / 365) * num_days))
                                        });
									}
                                } catch (err) {
                                    if($('#methodType').val() == "1")
                                    	dataPoints_preg_women.push({label: ($("#reportViewType" ).val() == "4")?i.split("_")[0]:i, y: parseInt(0)});
                                    else if($('#methodType').val() == "2")
                                        elco_data.push({label: ($("#reportViewType" ).val() == "4")?i.split("_")[0]:i, y: parseInt(0)});
                                    else if($('#methodType').val() == "3")
                                        total_population.push({label: ($("#reportViewType" ).val() == "4")?i.split("_")[0]:i, y: parseInt(0)});
                                }
                            }
                        }

                    }
                });
                return false;
            });
            labelFont = 14;
        }

        CanvasJS.addColorSet("colorShades",
            [//colorSet Array
                "#4661EE",
                "#EC5657",
                "#1BCDD1",
                "#8FAABB",
                "#2E8B57",
                "#B08BEB",
                "#3EA0DD",
                "#F5A52A",
                "#23BFAA",
                "#FAA586",
                "#EB8CC6",
                "#2F4F4F",
                "#008080",
                "#3CB371",
                "#90EE90"
            ]);

        var chart = new CanvasJS.Chart(div_id,
            {
                colorSet: "colorShades",

                title:{
                    text: title_name,
                    fontSize: 30
                },
                axisX:{
                    title: xaxis_title,
                    labelAngle: 135,
                    labelFontSize: labelFont,
                    valueFormatString: valueFormatString,
                    interval:interval,
                    intervalType: intervalType,
                    titleFontSize: 25,
                },
                axisY:{
                  title:"Number of Services",
				  labelFontSize: 20,
				  titleFontSize: 25,
                },
                axisY2: {
                    title: "Estimated Pregnant Women",
                    titleFontSize: 25,
                },
                toolTip: {
                    enabled: true,
                    shared: true  ,
                    contentFormatter: function(e){
                        //MACH
                        var str = "";
                        if($("#reportType" ).val() == "22" || $("#reportType" ).val() == "30")
                        	str = e.entries[0].dataPoint.label+"<br>";
                        else
                            str = e.entries[0].dataPoint.label+"<br>";
                        for (var i = 0; i < e.entries.length; i++){
                            if(e.entries[i].dataSeries.visible){
                                var temp = ("<span style='color: " + e.entries[i].dataSeries.color + ";'><strong>" + e.entries[i].dataSeries.name + '</strong></span>: '+ e.entries[i].dataPoint.y);
                                str = str.concat(temp, '<br>');
                            }
                        };
                        return (str);
                    }
                },
                animationEnabled: true,
                data: (function () {
                    var data = [];
                    $.each(response, function (l, list) {
						$.each(list, function(i, item) {
							if (i != "TOTAL") {
								var visible;
								var chartType;
								if (i == "Delivery" || ($('#methodType').val() != "1" && $("#reportViewType").val() != "11"))
									visible = true;
								else
									visible = false;

								if($("#reportType" ).val() == "22" || $("#reportType" ).val() == "30")
									chartType = "line";
								else
									chartType = "column";

								data.push({
									title: "column",
									type: chartType,
									name: i,
									showInLegend: "true",
									visible: visible,
									dataPoints: (function () {
										var element = [];
										if($("#reportType" ).val() == "22" || $("#reportType" ).val() == "30") {
											$.each(item, function (j) {
												element.push({
													label: item[j]['YearMonth'],
													y: parseInt(item[j]['Num'])
												});
												console.log(item[j]['YearMonth']);
											})
										}
										else{
											$.each(item, function (j) {
												element.push({
													label: ($("#reportViewType" ).val() == "4")?j.split("_")[0]:j,
													y: parseInt(item[j])
												});
											})
										}
										return element;
									})()

								})
							}
						})
                    })
                    return data;
                })(),
                legend:{
                    horizontalAlign: "center", // left, center ,right
                    verticalAlign: "top",  // top, center, bottom
                    cursor:"pointer",
                    itemclick: function(e) {
                        if (typeof (e.dataSeries.visible) ===  "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        }
                        else
                        {
                            e.dataSeries.visible = true;
                        }
                        chart.render();
                    }
                }
            });
        chart.render();
        if($("#reportType" ).val() == "3" && $('#methodType').val() == "1") {
        	chart.options.axisY2={title: "Estimated Pregnant Women",titleFontSize: 25};
            chart.options.data.push({
                    type: "spline",
                    name: "Number of Pregnant Women (EST)",
                    showInLegend: "true",
                    dataPoints: dataPoints_preg_women
                }
            );
            chart.render();
        }
        // if($("#reportType" ).val() == "3" && $('#methodType').val() == "2") {
        //     chart.options.axisY2={title: "Estimated Eligible Couple",titleFontSize: 25};
        //     chart.options.data.push({
        //             type: "spline",
        //             //axisYType: "secondary",
        //             toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}",
        //             name: "Number of Eligible Couple (EST)",
        //             showInLegend: "true",
        //             dataPoints: elco_data
        //         }
        //     );
        //     chart.render();
        // }
        // if($("#reportType" ).val() == "3" && $('#methodType').val() == "3") {
        //     chart.options.axisY2={title: "Total Population",titleFontSize: 25};
        //     chart.options.data.push({
        //             type: "spline",
        //             //axisYType: "secondary",
        //             toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}",
        //             name: "Total Population",
        //             showInLegend: "true",
        //             dataPoints: total_population
        //         }
        //     );
        //     chart.render();
        // }
        chart.render();
        $(".canvasjs-chart-credit").hide();
    }
    
    
    //Upate by shahed
    $(document).on("click",'.new-dash', function(){
		load_new_dashboard();
	});

	function load_new_dashboard()
	{
		$.ajax({
           type: "POST",
           url:"newdashboard",
           timeout:300000,
           success: function (response) {
        	   // $(".remove_div").remove();
    		   $( "#append_report" ).html(response);
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
               checkInternetConnection(xhr)
           }
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}


	$(document).on("click",'.mhealth-report', function(){

		load_mhalth($(this).data('id'));
	});

	function load_mhalth(id)
	{
		$.ajax({
			type: "POST",
			url:"mhealth",
			timeout:300000,
            data:{"uid":id},
			success: function (response) {
				$(".remove_div").remove();
				$( "#append_report" ).html(response);
			},
			complete: function() {
				preventMultiCall = 0;
			},
			error: function(xhr, status, error) {
                checkInternetConnection(xhr)
			}
		});
		//alert(preventMultiC);
		return preventMultiCall;
	}

    $(document).on("click",'.server-status', function(){
        load_developerTools("1");
    });

    $(document).on("click",'.dbdownload-info', function(){
        load_developerTools("3");
    });

    function load_developerTools(type)
    {
        var reportObj = new Object();
        reportObj.menuType = type;
        $.ajax({
            type: "POST",
            url:"tools",
            data:{"info":JSON.stringify(reportObj)},
            timeout:300000,
            success: function (response) {
                $(".remove_div").remove();
                $( "#append_report" ).html(response);
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }

});