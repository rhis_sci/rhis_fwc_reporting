var mnh_data;
var population = [];
var family_planning;
var general_patient;
var facility_assessment;
var emisCoverageJson={
    "03":[4,14,51,73,89,91,95],
    "04":[9,19,28,47,85],
    "12":[2,4,7,13,33,63,85,90,94],
    "13":[22,45,47,49,58,76,79,95],
    "15":[4,6,8,10,12,18,19,20,28,33,35,37,39,41,43,47,53,55,57,61,65,70,74,78,82,86],
    "19":[9,15,18,27,31,33,36,40,54,67,72,74,75,81,87,94],
    "22":[16,24,45,49,56,66,90,94],
    "27":[10,12,17,21,30,38,43,47,56,60,64,69,77],
    "29":[3,10,18,21,47,56,62,84,90],
    "30":[14,25,29,41,51,94],
    "32":[21,24,30,67,82,88,91],
    "35":[32,43,51,58,91],
    "36":[2,5,11,26,44,68,71,77],
    "42":[40,43,73,84],
    "44":[14,19,33,42,71,80],
    "46":[43,49,61,65,67,70,77,80],
    "49":[6,8,9,18,52,61,77,79,94],
    "50":[15,39,63,71,79,94],
    "51":[33,43,58,65,73],
    "52":[2,33,39,55,70],
    "54":[40,54,80,87],
    "56":[10,22,28,46,70,78,82],
    "58":[14,35,56,65,74,80,83],
    "69":[9,15,41,44,55,63,91],
    "73":[12,15,36,45,64,85],
    "75":[7,10,21,36,47,80,83,85,87],
    "77":[4,25,34,73,90],
    "84":[7,21,25,29,36,47,58,75,78,87],
    "85":[3,27,42,49,58,73,76,92],
    "90":[18,23,27,29,32,33,47,50,86,89,92],
    "91":[8,17,20,27,31,35,38,41,53,59,62,94],
    "93":[9,19,23,25,28,38,47,57,66,76,85,95],
    "94":[8,51,82,86,94]
};

//This function will return GO ID zilla_upazila_union
function getGoID(feature) {
    // Current GO ID zillaid_upazillaID_unionid
    zillaid=feature.properties.zillaid;
    upazilaid=feature.properties.upazilaid;
    unionid=feature.properties.unionid;

    goid="";

    if (zillaid!=0 && upazilaid ==0 && typeof unionid === 'undefined' )
        goid = zillaid;
    else if(zillaid!=0 && upazilaid !=0 && typeof unionid === 'undefined' )
        goid = zillaid + "_"+ upazilaid;
    else if(zillaid!=0 && upazilaid !=0 && unionid !=0 )
        goid = zillaid + "_"+ upazilaid+ "_"+ unionid;

    //console.log(zillaid);
   // console.log(upazilaid);
    //console.log(unionid);
    console.log ("current id : " + goid);

    return goid;
}

var geojsonFeature =
    [
        {
            "type": "Feature",
            "properties": {
                "name": "Coors Field",
                "amenity": "Baseball Stadium",
                "popupContent": "This is where the Rockies play!"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [91.52052,24.66503]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "name": "Coors Field",
                "amenity": "Baseball Stadium",
                "popupContent": "This is where the Rockies play!"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [91.1483442433061,24.0738398702368]
            }
        }

    ];

function event_click(feature,layer) {
    loadDataToWidget(feature,layer);
    //tapLayer(feature,layer);
}

function event_mouseover(feature,layer) {
    //loadDataToWidget(feature,layer);
}

function tapLayer(feature,layer) {
    id = getGoID(feature);
    layer.bindPopup('<div style="border-bottom: solid 1px #DDD; padding: 3px; font-weight: bold; margin-bottom: 15px;">'+
        feature.properties.name+'</div>' +
        '<div style="max-height: 500px; overflow: scroll; padding: 10px;">'+
        '</div>'
    );
}

//This function will load marker in map in upazila and union level in double click on map
function load_marker(zilla,upazila,union) {

    json = {"zilla":zilla,"upazila":upazila,"unions":union,"type":4,"processType":3};
    var Obj = new Object();

    Obj.zillaid = zilla==0?"All":zilla;
    Obj.upazilaid = upazila==0?"All":upazila;

    $.ajax({
            type : "POST",
            async: false,
            global: false,
            url : "http://119.148.6.215:8080/eMIS/mapplot",
            // url : "http://localhost:8081/mapplot",
            timeout:15000, //15 seconds timeout
            dataType : "json",
            data : {"info" : JSON.stringify(Obj)},
            success : function(server_response) {
                addMarker(server_response);
                preloader(0); //hide loading
            },
            error : function(xhr, status, error) {
                alert(status);
                alert(error);
            }
    });
}

function loadDataToWidget(feature,layer){
    goid = getGoID(feature);

    if(feature.properties.upazilaid == 0)
        propName = "District: " + "<span class=\"area-name\">"+feature.properties.name+"</span>";

    if(feature.properties.upazilaid != 0)
        propName = "Upazila: " + "<span class=\"area-name\">"+feature.properties.name+"</span>";

    //console.log(emisCoverage(feature.properties.zillaid));

    $("#area-name").html(propName);
}

function event_mouseover_outside_mamoni(feature,layer) {
    //alert('');

    //console.log('cl');
    //cleanMNH();
    //cleanPopulation();
    //fp_clean();
    //cleanGP();
}

//This function will remove previous chart and create new chart canvas
function clearChart(container) {
    $('#'+container).remove();
    $('#'+container+'-container').append('<canvas id="'+container+'"><canvas>');
}

//This function will fire in double click on any map layer
function event_double_click(feature, layer) {
    zillaid = feature.properties.zillaid;
    upazilaid = feature.properties.upazilaid;
    load_marker(zillaid,upazilaid,0);
}

//This function will call while clicking on breadcrumb text
function event_breadCrumb_click(zillaid, upazilaid,unionid) {
    removeMarker();
    load_marker(zillaid,upazilaid,unionid)


}

function removeMarker() {
    //Remove previous marker
    if(map.hasLayer(markerGroup))
        map.removeLayer(markerGroup);
}

//THis function will add marker in map
function addMarker(data){
    removeMarker();
    markerGroup = L.layerGroup().addTo(map);

    for (var key in data){
        for (var key1 in data[key]){
            if(key1 != "zillaid" && key1 != "upazilaid"){
                if(data[key][key1]["satellite_name"]==""){
                    iconurl = "image/facility_pointer.png";
                }
                else{
                    iconurl = "image/satelite_pointer.png";
                }

                var icon = L.icon({
                    iconUrl: iconurl,
                    //shadowUrl: 'leaf-shadow.png',

                    iconSize:     [20, 25], // size of the icon
                    shadowSize:   [23, 23], // size of the shadow
                    //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
                    //shadowAnchor: [4, 62],  // the same for the shadow
                    //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });

                marker = L.marker([parseFloat(data[key][key1]["latitude"]),parseFloat(data[key][key1]["longitude"])], {icon: icon});

                var provider_name = data[key][key1]["ProvName"].toUpperCase();
                var facility_name = data[key][key1]["FacilityName"].toUpperCase();
                var satelite_name = data[key][key1]["satellite_name"].toUpperCase();

                var pointerTitle = provider_name + " - " + data[key][key1]["provider_id"];
                if(data[key][key1]["satellite_name"]==""){
                    pointerTitle = pointerTitle + "\n" + facility_name;
                }
                else{
                    pointerTitle = pointerTitle + "\n" + satelite_name + " (Satelite Center)";
                }
                pointerTitle = pointerTitle + "\n" + getGeoName(data[key][key1]["zillaid"] + "," + data[key][key1]["upazilaid"]);

                var popup = marker.bindPopup(pointerTitle);

                markerGroup.addLayer(marker);
            }
        }
    }

    var overlay = {'Provider Info': markerGroup};
    L.layerGroup(markerGroup).addTo(map);

}