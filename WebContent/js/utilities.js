$(document).on("keyup", '#ProvMob', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

$(document).on("keyup", '#version', function() {
    $(this).val($(this).val().replace(/[^\d]/, ''));
});

var forgotPass="<h2>????????? ???? ??? ???? ????? ???????? ??????????? ???? ??????? ?????</h2>";
var changePass="<h2>Change Password</h2>";

var geo_data = (function () {
    var json = null;
    $.ajax({
        'type': "GET",
        'async': false,
        'global': false,
        'url': 'distUpzInfo',
        'dataType': "json",
        'success': function (data) {
            json = data;
        }
    });
    return json;
})();

$( document ).ready(function() {
    $("#zilla_list").text(geo_data["Zilla"]);
});

// geo_data = JSON.parse(geo_data);
// console.log(geo_data["Zilla"]);

function disableField(fieldId){

}

//Function for Alert Dialog
function callDialog(message){

	$('#model_box').dialog({
		modal: true,
		title: "Message",
		width: 420,
		open: function () {
			$(this).html(message);
		},
		buttons: {
			Ok: function () {
				$(this).dialog("close");
			}
		}
	});
}

function checkInternetConnection(xhr){
    if (xhr.readyState == 0) {
    	callDialog("Network is unavailable")
    }else{
    	callDialog("Network available.Check other Issue!!")
	}
}

// Dialogue for Profile Update
function callProfileDialog(message)
{
	$('#model_box').dialog({
		modal: true,
		title: "Message",
		width: 420,
		open: function () {
			$(this).html(message);
		},
		buttons: {
			Ok: function () {
				//$( "#wrapper" ).html(response);
				//$("#reportDataTable").load(location.href + " #reportDataTable");
				//$("#table_append").load(location.href + " #table_append");
				//$("#reportDataTable").load(" #reportDataTable");
				$("#model_box").dialog("close");

			}
		},
		close : function(){
			// functionality goes here
			alert("close here");
		}
	});
}

//loading gif for every ajax call
$(document).ajaxStart(function () {
	$('#loading_div').html("<img src='image/loading.gif' />");
	$('#loading_div').show();
    hideGif();
}).ajaxStop(function () {
	$('#loading_div').html("");
	$('#loading_div').hide();
});

//loading gif for every place from where its called
function loadGif(){
    $(document).ajaxStart(function () {
        $('#loading_div').html("<img src='image/loading.gif' />");
        $('#loading_div').show();
        hideGif();
    }).ajaxStop(function () {
        $('#loading_div').html("");
        $('#loading_div').hide();
    });


}
//hide the loading gif after 60000 when any error happens
function hideGif(){
    setTimeout(function () {
        	$('#loading_div').hide();
    },60000)
}

//load Division names in the dropdown from json
function loadDivision(name){
	//alert("division json loaded");
	zillaJS = JSON.parse(division.replace(/\s+/g,""));
	//alert(zillaJS);
	$(name).empty();

	var imp_zilla = geo_data

	var output = "";
	if($("#reportType").val()=="30")
        output = "<option value ='' selected>All</option>";
    else if($("#reportType").val()=="newborn" || $("#reportType").val()=="hrinfo" || $("#reportType").val()=="storageinfo" || $("#reportType").val()=="synchronizationstatus")
        output = "<option value ='0' selected>All</option>";
	else
        output = "<option value ='' selected>Please Select Division</option>";

	$.each(zillaJS[0], function(key, val) {
		output = output + "<option value = " + key + ">" + key + " - " + val["nameEnglish"] + "</option>";
	});


    $(name).append(output);
	return output;
}


//load zilla names in the dropdown from json
function loadZilla(name){
	//zillaJS = JSON.parse(zilla.replace(/\s+/g,""));
	zillaJS = JSON.parse(zilla.replace(/\s+/g,""));
	//alert(zillaJS.);
	$(name).empty();
	var output = "";

    if($("#reportType").val()=="newborn" || $("#reportType").val()=="storageinfo"|| $("#reportType").val()=="synchronizationstatus")
        output = output + "<option value = '0'>All</option>";
    else
        output = "<option value ='' selected>Please Select District</option>";


	$.each(zillaJS[0], function(key, val) {
		output = output + "<option value = " + key + ">" + key + " - " + val["nameEnglish"] + "</option>";
	});

	if($("#formType").val() == "7")
		output = output + "<option value = '99'>99 - Other</option>";



	$(name).append(output);
	return output;
}

//load Zilla
$(document).on("change", '#RdivName' ,function (){

	//disable district union and upazila
	//$('#RzillaName').attr('disabled','disabled');
	//$('#RupaZilaName').attr('disabled','disabled');
	//$('#RunionName').attr('disabled','disabled');
	
	//Empty options 
	// $('#RzillaName').find('option').remove().end();
	// $('#RupaZilaName').find('option').remove().end();
	// $('#RunionName').find('option').remove().end();

	//set zilla, upazila, union empty
    $('#RzillaName').empty();
    $('#RupaZilaName').empty();
    $('#RunionName').empty();
	
	if($("#RdivName").val() != "99")
	{	
		loadFilterZilla("#RdivName","#RzillaName");
		//Enable Zilla
		$('#RzillaName').removeAttr('disabled');
	}
	if($("#formType").val() == "1") //@evana , @date: 28/07/2019, Provider Creation
	{
		$("#facilityName").empty();
	}

});

//load upazila
$(document).on("change", '#RzillaName' ,function (){
	//disable upazila,union
    if($("#RzillaName").val()=="0"){
        $('#RunionName').prop("disabled", true);
        $('#RupaZilaName').prop("disabled", true);
	}else{
        // for service statistics it will disable all if we select upazilawise
    	if($("#reportType").val()=="3") {
            if (!($("#RupaZilaName").val() == null)) {

                $('#RunionName').prop("disabled", false);
                $('#RupaZilaName').prop("disabled", false);
            }
        }else{
            $('#RunionName').prop("disabled", false);
            $('#RupaZilaName').prop("disabled", false);

		}
	}



    //set upazila, union empty
    $('#RupaZilaName').empty();
    $('#RunionName').empty();

	if($("#formType").val() == "1") //@evana , @date: 28/07/2019, Provider Creation
	{
		$("#facilityName").empty();
	}

	if($("#RzillaName").val() != "99")
	//loadUpazila("#RzillaName","#RupaZilaName");
		loadUpazila("#RdivName","#RzillaName","#RupaZilaName");

	if($('#unionFacilityName').is(':visible') && ($('#menuType').val() == 5))
	{

		if(($('#facilityType').val()=="6_MCWC") || ($('#facilityType').val()=="7_DH")) {


			var zillaName = $("#RzillaName").val();
			//var upazilaName = $("#RupaZilaName").val();
			//var unionName = $("#RunionName").val();

			//alert(unionName);

			var zillaid = zillaName.split("_")[0];
			//var upazilaid = upazilaName.split("_")[0];
			//var unionid = unionName.split("_")[0];

			var forminfo = new Object();
			forminfo.zillaid = zillaid;
			//forminfo.upazilaid = upazilaid;
			//forminfo.unionid = unionid;
			forminfo.facilityType = $('#facilityType').val().split("_")[0];

			$.ajax({
				type: "POST",
				url: "getBanglaFacility",
				timeout: 60000, //60 seconds timeout
				data: {"forminfo": JSON.stringify(forminfo)},
				success: function (response) {
					console.log(response);
					facilityJS = JSON.parse(response);

					//alert(facilityJS[1]);

					var output = "";
					$.each(facilityJS, function (key, val) {
						output = output + val;
					});

					//alert(output);

					$("#UnionFacilityName").html(output);
					$("#UnionFacilityName").val(output);

					//$("#UnionFacilityName").empty();

					//$("#UnionFacilityName").append(output);
				},
				complete: function () {
					preventMultiCall = 0;
				},
				error: function (xhr, status, error) {
                    checkInternetConnection(xhr)
				}
			});
			return false;
		}
		else
			{
				return false
			}
	}
	//}
	else
		return false;

});


//load Union wise facilty name
$(document).on("change", '#RunionName' ,function (){


	if($("#formType").val() == "1") //@evana , @date: 28/07/2019, Provider Creation
	{
		$("#facilityName").empty();
		loadFacility();

	}

	if($('#unionFacilityName').is(':visible') && ($('#menuType').val() == 5))
	{


		var zillaName = $("#RzillaName").val();
		var upazilaName = $("#RupaZilaName").val();
		var unionName = $("#RunionName").val();

		//alert(unionName);

		var zillaid = zillaName.split("_")[0];
		var upazilaid = upazilaName.split("_")[0];
		var unionid = unionName.split("_")[0];

		var forminfo = new Object();
		forminfo.zillaid = zillaid;
		forminfo.upazilaid = upazilaid;
		forminfo.unionid = unionid;
		forminfo.facilityType = $('#facilityType').val().split("_")[0];

		$.ajax({
			type: "POST",
			url: "getBanglaFacility",
			timeout: 60000, //60 seconds timeout
			data: {"forminfo": JSON.stringify(forminfo)},
			success: function (response)
			{
				console.log(response);
				facilityJS = JSON.parse(response);

				//alert(facilityJS[1]);

				var output = "";
				$.each(facilityJS, function(key, val)
				{
					output = output + val ;
				});

				//alert(output);

				$("#UnionFacilityName").html(output);
				$("#UnionFacilityName").val(output);

				//$("#UnionFacilityName").empty();

				//$("#UnionFacilityName").append(output);
			},
			complete: function () {
				preventMultiCall = 0;
			},
			error: function (xhr, status, error) {
                checkInternetConnection(xhr)
			}
		});
		return false;
	}
	else
		return false;

	//Reset Union
	// $('#RupaZilaName').find('option').remove().end();
	// $('#RunionName').find('option').remove().end();
	
    if($("#RzillaName").val() != "99")
		//loadUpazila("#RzillaName","#RupaZilaName");
	loadUpazila("#RdivName","#RzillaName","#RupaZilaName");
  
    //Enable UpaZilla
	$('#RupaZilaName').removeAttr('disabled');

});

$(document).on("change", '#SzillaName' ,function (){
	loadUpazila("#SzillaName","#SupaZilaName");

});


function loadFilterZilla(nameDiv,nameZilla){

	//alert(nameZilla);
	fzillaJS = JSON.parse(division.replace(/\s+/g,""));

	var id = $(nameDiv).val();

	id = id.split("_")[0];
	//alert(id);
	//alert(zillaJS);
	$(nameZilla).empty();

    var imp_zilla = geo_data["Zilla"].split(",");

    var output = "";
	var option_value = "Please Select District";
    if($("#reportType").val()=="newborn")
        output = output + "<option value = '0'>All</option>";
    else
        output = "<option value ='' selected>"+option_value+"</option>";



	$.each(fzillaJS[0][id]["Zilla"], function(key, val) {
		if(imp_zilla.includes(key) || $("#formType").val() == "11" || $("#formType").val() == "26")
			output = output + "<option value = " + key + ">" + key + " - " + val["nameEnglish"] + "</option>";
	});

	if($("#formType").val() == "7")
		output = output + "<option value = '99'>99 - Other</option>";


	$(nameZilla).append(output);

    // off upazila field
    if($(nameZilla).val() =="0") {
        $('#RupaZilaName').prop("disabled", true);
        $('#RunionName').prop("disabled", true);
    }

	
}

function loadUpazila(nameDiv,nameZilla,nameUpz){
	//zillaJS = JSON.parse(zilla.replace(/\s+/g,""));

	upzillaJS = JSON.parse(division.replace(/\s+/g,""));

	var zillaid = $(nameZilla).val();
	zillaid = zillaid.split("_")[0];

	var divid = $(nameDiv).val();

	//alert(zillaid);

	//alert("Division ID="+divid);

	$(nameUpz).empty();

	var imp_upz = geo_data[zillaid].split(",");

	var option_value = "";

	// if($("#reportType").val()=="9" && ($("#indicatorType").val()=="HBP" || $("#indicatorType").val()=="LBW")) {
	//
	// 	option_value = "All";
	// 	$('#RunionName').append("<option value ='' selected>"+option_value+"</option>");
	// }
	// else
	if($("#reportType").val()=="mHealth"||$("#reportType").val()=="26" || $("#reportType").val()=="27" || $("#formType").val() == "27" || $("#formType").val() == "28" || $("#reportType").val()=="30" || ($("#reportType").val()=="28" && $("#reportViewType").val()=="4")|| $("#reportType").val()=="hrinfo" || $("#reportType").val()=="storageinfo" || $("#reportType").val()=="synchronizationstatus")
	{
		option_value = "All";
	} else
		option_value = "Please Select Upazila";

	var output = "<option value ='' selected>"+option_value+"</option>";
    // output = output + "<option value = ''>All</option>";

    //fix upazilla value "All" in upazila performance by selected indecator tyoe =9
    if($("#reportType").val()=="newborn"||$("#reportType").val()=="9")
        output = output + "<option value = '0' selected>All</option>";

	$.each(upzillaJS[0][divid]["Zilla"][zillaid]["Upazila"], function(key, val) {
		if(imp_upz.includes(key) || $("#formType").val() == "11")
			output = output + "<option value = " + key + ">" + key + " - " + val["nameEnglishUpazila"] + "</option>";
	});



	$(nameUpz).append(output);

    // off union field
    if($(nameUpz).val() =="0") {
        $('#RunionName').prop("disabled", true);
    }




}

//load union
$(document).on("change", '#RupaZilaName' ,function (){
    if($('#RunionName').prop("disabled", true));
        $('#RunionName').prop("disabled", false);


    if($("#RupaZilaName").val() == "0")
        $('#RunionName').prop("disabled", true);

        //set union empty
        $('#RunionName').empty();

        if ($("#formType").val() == "1") //@evana , @date: 28/07/2019, Provider Creation
        {
            $("#facilityName").empty();
            loadFacility();
        }
        //Reset Union
        // $('#RunionName').find('option').remove().end();

        //loadUnion("#RzillaName","#RupaZilaName","#RunionName");
        loadUnion("#RdivName", "#RzillaName", "#RupaZilaName", "#RunionName");


        if ($('#unionFacilityName').is(':visible') && ($('#menuType').val() == 5)) {

            var zillaName = $("#RzillaName").val();
            var upazilaName = $("#RupaZilaName").val();
            var unionName = $("#RunionName").val();

            //alert(unionName);

            var zillaid = zillaName.split("_")[0];
            var upazilaid = upazilaName.split("_")[0];
            var unionid = unionName.split("_")[0];

            var forminfo = new Object();
            forminfo.zillaid = zillaid;
            forminfo.upazilaid = upazilaid;
            forminfo.unionid = unionid;
            forminfo.facilityType = $('#facilityType').val().split("_")[0];

            if (forminfo.facilityType == "7" || forminfo.facilityType == "6") /* @Code at Date: 05/09/2019, @author: evana */
            {
                return false;
            }
            else {

                $.ajax({
                    type: "POST",
                    url: "getBanglaFacility",
                    timeout: 60000, //60 seconds timeout
                    data: {"forminfo": JSON.stringify(forminfo)},
                    success: function (response) {
                        //console.log(response);
                        facilityJS = JSON.parse(response);

                        //alert(facilityJS[1]);
                        console.log("facility Js= " + facilityJS);

                        var output = "";
                        $.each(facilityJS, function (key, val) {
                            output = output + val;
                        });

                        //alert(output);

                        $("#UnionFacilityName").html(output);
                        $("#UnionFacilityName").val(output);

                        //$("#UnionFacilityName").empty();

                        //$("#UnionFacilityName").append(output);
                    },
                    complete: function () {
                        preventMultiCall = 0;
                    },
                    error: function (xhr, status, error) {
                        checkInternetConnection(xhr)

                    }
                });

            }
            // $.ajax({
            // 	type: "POST",
            // 	url: "getBanglaFacility",
            // 	timeout: 60000, //60 seconds timeout
            // 	data: {"forminfo": JSON.stringify(forminfo)},
            // 	success: function (response)
            // 	{
            // 		//console.log(response);
            // 		facilityJS = JSON.parse(response);
            //
            // 		//alert(facilityJS[1]);
            // 		console.log("facility Js= "+facilityJS);
            //
            // 		var output = "";
            // 		$.each(facilityJS, function(key, val)
            // 		{
            // 			output = output + val ;
            // 		});
            //
            // 		//alert(output);
            //
            // 		$("#UnionFacilityName").html(output);
            // 		$("#UnionFacilityName").val(output);
            //
            // 		//$("#UnionFacilityName").empty();
            //
            // 		//$("#UnionFacilityName").append(output);
            // 	},
            // 	complete: function () {
            // 		preventMultiCall = 0;
            // 	},
            // 	error: function (xhr, status, error) {
            // 		alert(xhr.status);
            // 		alert(status);
            //
            // 	}
            // });
            return false;
        }
        else {
            return false;
        }

        //Enable Union
        $('#RunionName').removeAttr('disabled');

});

$(document).on("change", '#SupaZilaName' ,function (){
	loadUnion("#SzillaName","#SupaZilaName","#SunionName");
});

function loadUnion(nameDiv,nameZilla,nameUpz,nameUnion){
	zillaJS = JSON.parse(division.replace(/\s+/g,""));
	var idZilla = $(nameZilla).val();
	idZilla = idZilla.split("_")[0];
	var idUpz = $(nameUpz).val();
	var idDiv = $(nameDiv).val();

        $(nameUnion).empty();

        var option_value = "Please Select Union";

        if ($("#reportType").val() == "9" || $("#reportType").val() == "25" || $("#reportType").val() == "30")
            option_value = "All";
        else
            option_value = "Please Select Union";

        //var output = "<option value ='' selected>"+option_value+"</option>";
        var output = "<option value ='' selected>" + option_value + "</option>";

        if($("#reportType").val()=="newborn")
             output = output + "<option value = '0'>All</option>";

        $.each(zillaJS[0][idDiv]["Zilla"][idZilla]["Upazila"][idUpz]["Union"], function (key, val) {
            output = output + "<option value = " + key + ">" + key + " - " + val["nameEnglishUnion"] + "</option>";
        });

        $(nameUnion).append(output);
    }


$(document).on("click", '.next_button' ,function (){
	var button_val = $(".next_button").val();
	//alert(parseInt(button_val)+1);
	if(button_val<4)
	{
		$(".next_button").val(parseInt(button_val)+1);
		$(".pre_button").val(parseInt(button_val)-1);
		$(".table_hide").hide();
		$(".page_"+button_val).show();
		$(".showing_page").text("  "+button_val+" of 3  ");
		$(".pre_button").removeAttr("disabled");
		if((parseInt(button_val)+1)>3)
			$(".next_button").attr("disabled","disabled");

		$(".page_no").hide();
	}
});

$(document).on("click", '.pre_button' ,function (){
	var button_val = $(".pre_button").val();
	if(button_val>0)
	{
		$(".pre_button").val(parseInt(button_val)-1);
		$(".next_button").val(parseInt(button_val)+1);
		$(".table_hide").hide();
		$(".page_"+button_val).show();
		$(".showing_page").text("  "+button_val+" of 3  ");
		$(".next_button").removeAttr("disabled");
		if((parseInt(button_val)-1)<1)
		{
			$(".pre_button").attr("disabled","disabled");
			$(".page_no").show();
		}
	}
});

//For New MIS3 Form: Coded By Evana : 10/01/2019
$(document).on("click", '.next_button_new' ,function (){
	//alert("Next button clicked");
	var button_val = $(".next_button_new").val();
	//alert(parseInt(button_val)+1);
	//alert(button_val);

	if(button_val<6)
	{
		$(".next_button_new").val(parseInt(button_val)+1);
		$(".pre_button_new").val(parseInt(button_val)-1);
		$(".table_hide").hide();
		$(".page_"+button_val).show();
		//$(".table_header_"+button_val).show();
		//$("#mis_form_"+button_val).show();
		$(".showing_page").text("  "+button_val+" of 5  ");
		$(".pre_button_new").removeAttr("disabled");
		if((parseInt(button_val)+1)>5)
			$(".next_button_new").attr("disabled","disabled");

		$(".page_no").hide();
	}
});

$(document).on("click", '.pre_button_new' ,function (){
	var button_val = $(".pre_button_new").val();
	//alert(button_val);
	if(button_val>0)
	{
		$(".pre_button_new").val(parseInt(button_val)-1);
		$(".next_button_new").val(parseInt(button_val)+1);
		$(".table_hide").hide();
		$(".page_"+button_val).show();
		$(".table_header_"+button_val).show();

		$(".showing_page").text("  "+button_val+" of 5 ");
		$(".next_button_new").removeAttr("disabled");

		if((parseInt(button_val)-1)<1)
		{
			$(".pre_button_new").attr("disabled","disabled");
			$(".page_no").show();
		}
	}
});


$(document).on("click", '.dhis2_export' ,function (){
	var dhis2_month = $(".dhis2_month").val();
	var dhis2_year = $(".dhis2_year").val();
	var dhis2_facilityid = $("#facilityName").val();
	var dhis2_zilla = $('#RzillaName').val();
	var dhis2_upazila = "99";
	if($('#RupaZilaName').val()!=null && $('#RupaZilaName').val()!="")
		upazila = $('#RupaZilaName').val();
	var dhis2_union = "99";
	if($('#RunionName').val()!=null && $('#RunionName').val()!="")
		dhis2_union = $('#RunionName').val();
	$.ajax({
		type: "GET",
		url:"http://mchdrhis.icddrb.org:8089/dhisweb/mis3rpt?dataset=mFICoNMCDG4&facilityid="+dhis2_facilityid+"&zilla="+dhis2_zilla+"&upz="+dhis2_upazila+"&un="+dhis2_union+"&year="+dhis2_year+"&month="+dhis2_month,
		timeout:60000, //60 seconds timeout
		dataType: "json",
		success: function (data) {
			//var response = JSON.parse(data);
			console.log(data['dhis_response']['status']);
			if(data['dhis_response']['status']=="SUCCESS")
				callDialog("<b style='color:green'>Export data to dhis2 successfully</b>");
			else
				callDialog("<b style='color:green'>!!</b>");

		}
	});
});


