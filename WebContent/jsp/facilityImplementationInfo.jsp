<%@ page import="org.json.JSONObject" %>
<%@ page import="java.util.Iterator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title" id="box_title">Number of Facility Using eMIS</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body search_form table-responsive">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered nowrap reportExport" id="reportDataTable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <td>District</td>
                                <td>DH</td>
                                <td>MCWC</td>
                                <td>UHC</td>
                                <td>UH&FWC</td>
                                <td>USC</td>
                                <td>RD</td>
                                <td>Sadar Clinic</td>
                                <td>10/20-bed Hospital</td>
                                <td>Others</td>
                                <td>Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                JSONObject facilityInfo = new JSONObject(request.getAttribute("facilityInfo").toString());
                                Iterator<String> keys = facilityInfo.keys();
                                int dh,mcwc,uhc,uhfwc,usc,rd,sc,tentwentybedhospital,total10and20,others,rowTotal,inTotal;
                                dh=mcwc=uhc=uhfwc=usc=rd=sc=tentwentybedhospital=others=rowTotal=inTotal=0;
                                while (keys.hasNext()){
                                    rowTotal = 0;
                                    String key = keys.next();
                                    if (facilityInfo.get(key) instanceof JSONObject && !(facilityInfo.get(key).toString().equals("{}"))) {
                            %>
                            <tr>
                                <td><%= ((JSONObject) facilityInfo.get(key)).get("Name") %></td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("DH")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("DH"));
                                            dh += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("DH").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("DH").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("DH").toString());
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("MCWC")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("MCWC"));
                                            mcwc += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("MCWC").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("MCWC").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("MCWC").toString());
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("UHC")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("UHC"));
                                            uhc += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("UHC").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("UHC").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("UHC").toString());
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("UH&FWC")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("UH&FWC"));
                                            uhfwc += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("UH&FWC").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("UH&FWC").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("UH&FWC").toString());
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("USC")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("USC"));
                                            usc += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("USC").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("USC").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("USC").toString());
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("RD")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("RD"));
                                            rd += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("RD").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("RD").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("RD").toString());
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("Sadar Clinic")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("Sadar Clinic"));
                                            sc += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("Sadar Clinic").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("Sadar Clinic").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("Sadar Clinic").toString());
                                        }
                                    %>
                                </td>

                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("10-bed Hospital")||((JSONObject) facilityInfo.get(key)).has("20-bed Hospital") ){
                                            int tenbed = 0;
                                            int twenbed = 0;

                                            if(((JSONObject) facilityInfo.get(key)).has("20-bed Hospital")){
                                                twenbed = Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("20-bed Hospital").toString());
                                            }else if (((JSONObject) facilityInfo.get(key)).has("10-bed Hospital")){
                                                tenbed = Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("10-bed Hospital").toString());
                                            }
                                            total10and20 = tenbed+twenbed;
                                            out.print(total10and20);
                                            tentwentybedhospital += total10and20;
                                            rowTotal += total10and20;
                                            inTotal += total10and20;
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(((JSONObject) facilityInfo.get(key)).has("Others")){
                                            out.print(((JSONObject) facilityInfo.get(key)).get("Others"));
                                            others += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("Others").toString());
                                            rowTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("Others").toString());
                                            inTotal += Integer.parseInt(((JSONObject) facilityInfo.get(key)).get("Others").toString());
                                        }
                                    %>
                                </td>
                                <td><%= rowTotal %></td>
                            </tr>
                            <% } } %>
                            <tr>
                                <td>Total</td>
                                <td><%= dh %></td>
                                <td><%= mcwc %></td>
                                <td><%= uhc %></td>
                                <td><%= uhfwc %></td>
                                <td><%= usc %></td>
                                <td><%= rd %></td>
                                <td><%= sc %></td>
                                <td><%= tentwentybedhospital %></td>
                                <td><%= others %></td>
                                <td><%= inTotal %></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
</div>

<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>
<script type="text/javaScript">
    $('.clickable').click(function () {
        $('.clickable').css({"background-color": "rgb(255, 255, 255)"});
        $(this).css({"background-color": "rgb(0, 172, 214)"});
        $('.clickable > span').removeClass("bg-aqua").addClass("bg-gray");
        $(this).find("span:first").removeClass("bg-gray").addClass("bg-aqua");
        $('.clickable span img').each(function(index, element) {
            var url = $(element).prop("src").replace("png","jpg");
            $(element).attr("src",url);
        });
        url = $(this).find("span img").prop("src").replace("jpg","png");
        $(this).find("span img").attr("src",url);
        openTab($(this).attr("data"));
    });
    function openTab(tabName) {
        var tabcontent;
        tabcontent = document.getElementsByClassName("tab");
        for (var i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        document.getElementById(tabName).style.display = "block";
    }
    function doExport(selector, params) {
        var options = {
            //ignoreRow: [1,11,12,-2],
            //ignoreColumn: [0,-1],
            //pdfmake: {enabled: true},
            tableName: 'Facility Information',
            worksheetName: 'Facility Information'
        };

        $.extend(true, options, params);

        $(selector).tableExport(options);
    }

    function DoOnCellHtmlData(cell, row, col, data) {
        var result = "";
        if (data != "") {
            var html = $.parseHTML( data );

            $.each( html, function() {
                if ( typeof $(this).html() === 'undefined' )
                    result += $(this).text();
                else if ( $(this).is("input") )
                    result += $('#'+$(this).attr('id')).val();
                else if ( $(this).is("select") )
                    result += $('#'+$(this).attr('id')+" option:selected").text();
                else if ( $(this).hasClass('no_export') !== true )
                    result += $(this).html();
            });
        }
        return result;
    }

    function DoOnMsoNumberFormat(cell, row, col) {
        var result = "";
        if (row > 0 && col == 0)
            result = "\\@";
        return result;
    }

</script>



<script>
    $(document).ready(function() {
        //$('#reportDataTable').DataTable();


        $(document).ready(function() {
            var filterFunc = function (sData) {
                return sData.replace(/\n/g," ").replace( /<.*?>/g, "" );
            };
            var table = $('#reportDataTable').dataTable({
                "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
                "processing": true,
                "paging": true,
                "lengthMenu": [ 10, 25, 50, 75, 100 ],
                "language": {
                    "info": "Total _TOTAL_",
                    "infoEmpty": "Total _TOTAL_",
                    "zeroRecords": "No records",
                    "lengthMenu": "Show _MENU_ entries",
                    "searchIcon":"",
                    "paginate": {
                        "first": "First",
                        "last": "Last",
                        "next": "Next",
                        "previous": "Prev"
                    }
                },
                "initComplete": function(oSettings, json) {
                    $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                    $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                    $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");

                },
                "buttons": ['copy',{
                    extend: 'excelHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'csvHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    title:"Service Statistics",
                    pageSize: 'LEGAL'
                },{
                    extend: 'print',
                    text: 'Print',
                    autoPrint: true
                }
                ]
            });
        });
        var table = $('#reportDataTableFilter').dataTable({
            "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
            "processing": true,
            "paging": true,
            "lengthMenu": [ 10, 25, 50, 75, 100 ],
            "language": {
                "info": "Total _TOTAL_",
                "infoEmpty": "Total _TOTAL_",
                "zeroRecords": "No records",
                "lengthMenu": "Show _MENU_ entries",
                "searchIcon":"",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Prev"
                }
            },
            "initComplete": function(oSettings, json) {
                $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );

            },
            "buttons": ['copy',{
                extend: 'excelHtml5',
                title:"Facility Information"
            },{
                extend: 'csvHtml5',
                title:"Facility Information"
            },{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title:"Facility Information"
            }, {
                extend: 'print',
                text: 'Print',
                autoPrint: true
            }
            ]
        } );
    });




</script>



