<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 3/2/2021
  Time: 5:01 PM
  To change this template use File | Settings | File Templates.
--%>


<style>
    .input-daterange input, #monthSelect, #yearSelect {
        background: url('image/calendar.png') no-repeat;
        background-size: 30px 30px;
        padding-left: 30px;
        text-align: center;
        background-color: #ffffff !important;
    }
</style>


<% String menutype = request.getAttribute("type").toString();%>


<div class="row">
    <div class="col-md-12">
        <div class="form-group col-md-12 date_type">
            <label>Select Date Type</label><br/>
            <% if (!(menutype.equals("4"))) { %>
            <span class="levelYear"><input type="radio" id="year_type" name="date_type"
                                           value="3" class="flat-red">&nbsp;&nbsp;&nbsp;<span id="year_wise_type">Year Wise</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <%}%>
            <span class="levelMonth"><input type="radio" id="month_type" name="date_type"
                                            value="1" class="flat-red">&nbsp;&nbsp;&nbsp;<span id="month_wise_type">Month Wise</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <% if (!(menutype.equals("26")) && !(menutype.equals("22")) ) { %>
            <span class="levelDate"><input type="radio" id="date_type" name="date_type"
                                           value="2"
                                           class="flat-red">&nbsp;&nbsp;&nbsp;<span id="date_wise_type">Date Wise</span></span>
            <%}%>
        </div>
        <!--need for flulike(trend)-->
        <div class="form-group col-md-12 graph_view_type" style="display: none;">
            <label>Select Trend View Type</label><br/>
            <span class="levelMonthly"><input type="radio" id="monthly" name="date_type" value="1" class="flat-red">&nbsp;&nbsp;&nbsp;Monthly&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span class="levelWeekly"><input type="radio" id="weekly" name="date_type" value="2" class="flat-red">&nbsp;&nbsp;&nbsp;Weekly&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span class="levelDaily"><input type="radio" id="daily" name="date_type" value="3" class="flat-red">&nbsp;&nbsp;&nbsp;Daily</span>
        </div>

            <% if (menutype.equals("3")) { %>
        <div class="form-group date col-md-5 year_wise" style="display: none">
                <%} else { %>
            <div class="form-group date col-md-2 year_wise" style="display: none">
                <%}%>
                <label class="control-label">Year</label>
                <input type="text" id="yearSelect" class="form-control" readonly>
            </div>
                <% if (menutype.equals("3")) { %>
            <div class="form-group date col-md-6 month_wise" style="display: none">
                <%} else { %>
                <div class="form-group date col-md-2 month_wise" style="display: none">
                    <%}%>
                    <label class="control-label">Month</label>
                    <input type="text" id="monthSelect" class="form-control" readonly>
                </div>
                <% if (menutype.equals("3")) { %>
                <div class="form-group col-md-10 date_wise " style="display: none">
                    <%} else { %>
                    <div class="form-group col-md-4 date_wise " style="display: none">
                        <%}%>
                        <label class="control-label" for="start_date">Date Range</label>
                        <div class="input-group input-daterange">
                            <input type="text" id="start_date" class="form-control" readonly>
                            <div class="input-group-addon">to</div>
                            <input type="text" id="end_date" class="form-control" readonly>
                        </div>
                    </div>
                    <% if (!(menutype.equals("3"))) { %>
                    <!--submit button-->
                    <%@include file="/jsp/util/submitButton.jsp" %>
                    <!--/submit button-->
                    <%}%>


                    <%--<div class="form-group col-md-4">--%>
                    <%--<label class="control-label"></label>--%>
                    <%--<div class="input-group input-button">--%>
                    <%--<input type="hidden" id="reportType"--%>
                    <%--value="<% out.print(request.getAttribute("type")); %>">--%>
                    <%--<div>--%>
                    <%--<button type="button" id="submitReport1" class="btn btn-primary">--%>
                    <%--<i class="fa fa-send"></i> Submit--%>
                    <%--</button>--%>
                    <%--</div>--%>
                    <%--</div>--%>
                    <%--</div>--%>
                </div>
            </div>


            <!-- bootstrap datepicker -->
            <link rel="stylesheet" href="library/datepicker/datepicker.min.css">
            <!-- bootstrap datepicker -->
            <script src="library/datepicker/bootstrap-datepicker.min.js"></script>


            <script>
                $(document).ready(function () {

                    var menu = "<%out.print(menutype.trim());%>";
                    if (menu === "3") {
                        $('#year_wise_type').text("Yearly");
                        $('#month_wise_type').text("Monthly");
                        $('#date_wise_type').text("Date Range");
                    }


                    if($('.date_type').is(':visible')){

                            $('#month_type').prop('checked', true);
                            $(".month_wise").show();

                    }

                    //disease trend month section is removed
                    if(menu == "22"){
                        $(".month_wise").hide();
                    }

                });
                $('#year_type, #monthly').on('change', function () {
                    $(".year_wise").show();
                    $('#end_date').datepicker('setDate', null);
                    $('#start_date').datepicker('setDate', null);
                    $('#monthSelect').datepicker('setDate', null);
                    $('#yearSelect').datepicker('setDate', new Date().getFullYear().toString());
                    $(".date_wise").hide();
                    $(".month_wise").hide();
                });

                $('#month_type, #weekly').on('change', function () {
                    $(".month_wise").show();
                    $('#end_date').datepicker('setDate', null);
                    $('#start_date').datepicker('setDate', null);
                    $('#yearSelect').datepicker("setDate", null);
                    $('#monthSelect').datepicker('setDate', new Date().getFullYear() + '-' + (new Date().getMonth() + 1).toString());
                    $(".date_wise").hide();
                    $(".year_wise").hide();
                });

                $('#date_type, #daily').on('change', function () {
                    $(".date_wise").show();
                    $('#end_date').datepicker("setDate", new Date());
                    $('#monthSelect').datepicker("setDate", null);
                    $('#yearSelect').datepicker("setDate", null);
                    $(".month_wise").hide();
                    $(".year_wise").hide();
                });

                $('#yearSelect').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    viewMode: "years",
                    minViewMode: "years",
                    format: 'yyyy'
                }).datepicker('setDate', new Date().getFullYear().toString());

                $('#monthSelect').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    minViewMode: 1,
                    format: 'yyyy-mm'
                }).datepicker('setDate', new Date().getFullYear() + '-' + (new Date().getMonth() + 1).toString());

                $('#start_date').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });
                $('#end_date').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd',
                });
            </script>

