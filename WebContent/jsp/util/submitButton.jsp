<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 3/7/2021
  Time: 11:57 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="form-group col-md-2">
    <label class="control-label"></label>
    <div class="input-group input-button">
        <input type="hidden" id="reportType" value="<% out.print(request.getAttribute("type")); %>">
        <div>
            <button type="button" id="submitReport1" class="btn btn-primary">
                <i class="fa fa-send"></i> Submit
            </button>
        </div>
    </div>
</div>
