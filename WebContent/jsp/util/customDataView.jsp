<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 10/10/2021
  Time: 1:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@include file="../newborn/newbornResult.jsp" %>--%>

<!-- Tab views -->
<div class="w3-container">
    <div class="w3-row">
        <div class="" id="presentationType" style="display: block;">
            <div style="text-align: center;">

                <div class="col-md-6">
                    <div class="info-box bg-white clickable" style="background-color: rgb(0, 172, 214);"
                         data="graph_view">
                            <span class="info-box-icon bg-aqua" id="graphImgBackground">
                                <img id="graphImg" src="image/graph.png" alt="">
                            </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><h4></h4></span><br>
                            <span class="info-box-number">Graph View</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="info-box bg-white clickable" style="background-color: rgb(255, 255, 255);"
                         data="table_view">
                            <span class="bg-gray info-box-icon" id="tableImgBackground">
                                <img src="image/table.jpg" alt="">
                            </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><h4></h4></span><br>
                            <span class="info-box-number">Table View</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Details views -->
    <div id="graph_view" class="w3-container tab">
        <div id="graph_container" style="width:100%" class="col-xs-12"></div>
    </div>

    <div id="table_view" class="w3-container tab" style="display:none">
        <div class="col-md-12" id="table_container" style="margin-top: 2%;">


        </div>
    </div>
</div>


<!-- JS handlers -->
<script>
    let state = {};
    $('.clickable').click(function () {
        $('.clickable').css({"background-color": "rgb(255, 255, 255)"});
        $(this).css({"background-color": "rgb(0, 172, 214)"});
        $('.clickable > span').removeClass("bg-aqua").addClass("bg-gray");
        $(this).find("span:first").removeClass("bg-gray").addClass("bg-aqua");
        $('.clickable span img').each(function (index, element) {
            var url = $(element).prop("src").replace("png", "jpg");
            $(element).attr("src", url);
        });
        url = $(this).find("span img").prop("src").replace("jpg", "png");
        $(this).find("span img").attr("src", url);

        //open tab
        var tabcontent = document.getElementsByClassName("tab");
        for (var i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        let tabName = $(this).attr("data");
        document.getElementById(tabName).style.display = "block";
        // execute the function once
        if (state[tabName] === undefined) {
            window["exec_" + tabName]();
            state[tabName] = true;
        }
    });
</script>