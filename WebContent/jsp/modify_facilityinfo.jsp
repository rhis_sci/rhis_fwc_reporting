<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.json.JSONObject" %>

		<style>
			input[type="number"]::-webkit-outer-spin-button,
			input[type="number"]::-webkit-inner-spin-button {
			    -webkit-appearance: none;
			    margin: 0;
			}
			input[type="number"] {
			    -moz-appearance: textfield;
			}
		</style>
		<%
       
        String jsondata = request.getAttribute("jsonString").toString();
        JSONObject jsonObj = new JSONObject(jsondata);
       
	   %>
        
       <div class="row" id="facility_div">
		<div class="col-md-12">
		<div id="profile_update_info"></div>
			 <div class="form-group col-md-8" id="Pname">
			 
				<label>Facility Name</label>
				<input type="hidden" name="facilityid" id="facilityid" value="<%= jsonObj.getString("facilityid") %>" />
				<input type="hidden" name="zillaid" id="zillaid" value="<%= jsonObj.getString("zillaid") %>" />
				<input type="hidden" name="upazilaid" id="upazilaid" value="<%= jsonObj.getString("upazilaid") %>" />
				<input type="text" class="form-control" id="editfacName" value="<%= jsonObj.getString("facility_name") %>" required>
				<div id="name_validate" align="left"></div>
	           </div>
			
			   
			<div class="box-footer col-md-12">
				<input type="hidden" id="formType" value="">
				<input type="button" id="facEditForm" class="btn btn-primary" value="Update Information">
			</div>
		</div>
	   </div>