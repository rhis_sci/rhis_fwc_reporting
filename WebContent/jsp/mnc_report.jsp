<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.sci.rhis.util.EnglishtoBangla" %>
<%@ page import="org.sci.rhis.db.DBSchemaInfo" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page pageEncoding="UTF-8" %>
<script src="js/lib/ddtf.js"></script>
<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>

<style>
	.mnc-table-left th,.mnc-table-right th{
		border: 1px solid black!important;
		text-align: center!important;
		vertical-align: middle!important;
		padding: 2px;
	}
	.mnc-table-left td,.mnc-table-right td{
		vertical-align: top!important;
		padding-left: 2px;
	}
	.nowrap{
		white-space: nowrap!important;
	}
	.inner_table td{
		border: 1px solid black!important;
		text-align: left!important;
		vertical-align: middle!important;
		padding-left: 2px;
	}
	.pad-bot-15 div{
		padding-bottom: 10px;
	}
	.pad-bot-5 div{
		padding-bottom: 5px;
	}
	.bold-italic{
		font-weight: bold;
		font-style: italic;
		padding-left: 4px;
		font-size: 110%;
	}
</style>
<%

%>
<div class="tile-body nopadding" id="reportTable">
	<div class="row">
		<div class="col-md-12">
			<div class="dropdown" style="float: right; margin-right: 2%;">
				<button class="btn btn-primary" type="button" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i> Print</button>

<%--				<button class="btn btn-primary" type="button" id="btnPrint" onclick="PrintElem('#printDiv')"><i class="fa fa-print" aria-hidden="true"></i> Print</button>--%>
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
					<span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

	<div class="row">
		<div class="col-md-12">
			<div id="printDiv" style="margin-top: 2%;">
				<style>
					/*#mnc-table-left th,#mnc-table-right th{*/
						/*border: 1px solid black!important;*/
						/*text-align: center!important;*/
						/*vertical-align: middle!important;*/
						/*padding: 2px;*/
					/*}*/
					.mnc-table-left td,.mnc-table-right td{
						vertical-align: top!important;
					}
					.mnc-table-left{
						min-height: 8.5in;
					}
					.pad-bot-15 div{
						padding-bottom: 10px;
					}
					table { /* Or specify a table class */
						max-height: 100%!important;
						overflow: hidden!important;
						page-break-after: always!important;
					}
					/*.pad-bot-5 div{*/
						/*padding-bottom: 5px;*/
					/*}*/
					/*.bold-italic{*/
						/*font-weight: bold;*/
						/*font-style: italic;*/
						/*padding-left: 4px;*/
						/*font-size: 110%;*/
					/*}*/
				</style>
				<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'>
					<tr>
						<%--<td width="20%">--%>
							<%--<div style='border: 1px solid black;text-align: center;'>দুটি সন্তানের বেশি নয়<br>একটি হলে ভাল হয়।</div>--%>
						<%--</td>--%>
						<%--<td width="50%" style='text-align: center;'>--%>
							<%--<img width='60' src='image/dgfp_logo.png'></br>--%>
							<%--<h1>মা ও নবজাতক সেবা রেজিস্টার</h1>--%>
							<%--গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>পরিবার পরিকল্পনা অধিদপ্তর--%>
						<%--</td>--%>
						<%--<td width="30%" style='text-align: right;' class='page_no'></td>--%>
					</tr>
				</table>
				</br>
				<%
					String data = request.getAttribute("Result").toString();
					JSONArray json = new JSONArray(data);
					for (int i = 0; i < json.length(); i=i+2) {
						JSONObject singleRow = new JSONObject();
						JSONObject singleRow2 = new JSONObject();
						try {
						    singleRow = json.getJSONObject(i);
							if(i+1 >= json.length()){
								Iterator keys = singleRow.keys();
								while(keys.hasNext()){
									String key = (String) keys.next();
									singleRow2.put(key,"");
								}
							}else{
								singleRow2 = json.getJSONObject(i + 1);
							}

							singleRow.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_age").toString()).replaceAll("\\byears\\b|\\byear\\b", "বছর").replaceAll("\\bmons\\b|\\bmon\\b", "মাস").replaceAll("\\bdays\\b|\\bday\\b", "দিন"));
							singleRow2.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_age").toString()).replaceAll("\\byears\\b|\\byear\\b", "বছর").replaceAll("\\bmons\\b|\\bmon\\b", "মাস").replaceAll("\\bdays\\b|\\bday\\b", "দিন"));

							singleRow.put("client_last_child_age",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_last_child_age").toString()));
							singleRow2.put("client_last_child_age",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_last_child_age").toString()));

							singleRow.put("client_lmp",EnglishtoBangla.getEngToBanDate(singleRow.get("client_lmp").toString()));
							singleRow2.put("client_lmp",EnglishtoBangla.getEngToBanDate(singleRow2.get("client_lmp").toString()));

							singleRow.put("client_edd",EnglishtoBangla.getEngToBanDate(singleRow.get("client_edd").toString()));
							singleRow2.put("client_edd",EnglishtoBangla.getEngToBanDate(singleRow2.get("client_edd").toString()));

							singleRow.put("client_para",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_para").toString()));
							singleRow2.put("client_para",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_para").toString()));

							singleRow.put("client_gravida",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_gravida").toString()));
							singleRow2.put("client_gravida",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_gravida").toString()));

							singleRow.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_son").toString()));
							singleRow2.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_son").toString()));

							singleRow.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_dau").toString()));
							singleRow2.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_dau").toString()));

							singleRow.put("total_child",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("total_child").toString()));
							singleRow2.put("total_child",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("total_child").toString()));

							singleRow.put("client_height",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_height").toString()));
							singleRow2.put("client_height",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_height").toString()));

							singleRow.put("anc1_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("anc1_service_date").toString()));
							singleRow.put("anc1_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc1_bp").toString()));
							singleRow.put("anc1_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc1_weight").toString()));
							singleRow.put("anc1_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc1_uterus_height").toString()));
							singleRow.put("anc1_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc1_fetus_heart_rate").toString()));
							singleRow.put("anc1_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc1_hemoglobin").toString()));
							singleRow.put("anc1_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc1_complication_sign").toString()));

							singleRow.put("anc2_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("anc2_service_date").toString()));
							singleRow.put("anc2_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc2_bp").toString()));
							singleRow.put("anc2_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc2_weight").toString()));
							singleRow.put("anc2_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc2_uterus_height").toString()));
							singleRow.put("anc2_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc2_fetus_heart_rate").toString()));
							singleRow.put("anc2_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc2_hemoglobin").toString()));
							singleRow.put("anc2_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc2_complication_sign").toString()));

							singleRow.put("anc3_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("anc3_service_date").toString()));
							singleRow.put("anc3_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc3_bp").toString()));
							singleRow.put("anc3_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc3_weight").toString()));
							singleRow.put("anc3_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc3_uterus_height").toString()));
							singleRow.put("anc3_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc3_fetus_heart_rate").toString()));
							singleRow.put("anc3_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc3_hemoglobin").toString()));
							singleRow.put("anc3_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc3_complication_sign").toString()));

							singleRow.put("anc4_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("anc4_service_date").toString()));
							singleRow.put("anc4_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc4_bp").toString()));
							singleRow.put("anc4_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc4_weight").toString()));
							singleRow.put("anc4_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc4_uterus_height").toString()));
							singleRow.put("anc4_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc4_fetus_heart_rate").toString()));
							singleRow.put("anc4_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc4_hemoglobin").toString()));
							singleRow.put("anc4_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("anc4_complication_sign").toString()));


							singleRow2.put("anc1_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("anc1_service_date").toString()));
							singleRow2.put("anc1_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc1_bp").toString()));
							singleRow2.put("anc1_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc1_weight").toString()));
							singleRow2.put("anc1_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc1_uterus_height").toString()));
							singleRow2.put("anc1_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc1_fetus_heart_rate").toString()));
							singleRow2.put("anc1_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc1_hemoglobin").toString()));
							singleRow2.put("anc1_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc1_complication_sign").toString()));

							singleRow2.put("anc2_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("anc2_service_date").toString()));
							singleRow2.put("anc2_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc2_bp").toString()));
							singleRow2.put("anc2_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc2_weight").toString()));
							singleRow2.put("anc2_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc2_uterus_height").toString()));
							singleRow2.put("anc2_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc2_fetus_heart_rate").toString()));
							singleRow2.put("anc2_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc2_hemoglobin").toString()));
							singleRow2.put("anc2_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc2_complication_sign").toString()));

							singleRow2.put("anc3_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("anc3_service_date").toString()));
							singleRow2.put("anc3_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc3_bp").toString()));
							singleRow2.put("anc3_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc3_weight").toString()));
							singleRow2.put("anc3_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc3_uterus_height").toString()));
							singleRow2.put("anc3_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc3_fetus_heart_rate").toString()));
							singleRow2.put("anc3_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc3_hemoglobin").toString()));
							singleRow2.put("anc3_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc3_complication_sign").toString()));

							singleRow2.put("anc4_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("anc4_service_date").toString()));
							singleRow2.put("anc4_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc4_bp").toString()));
							singleRow2.put("anc4_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc4_weight").toString()));
							singleRow2.put("anc4_uterus_height",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc4_uterus_height").toString()));
							singleRow2.put("anc4_fetus_heart_rate",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc4_fetus_heart_rate").toString()));
							singleRow2.put("anc4_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc4_hemoglobin").toString()));
							singleRow2.put("anc4_complication_sign",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("anc4_complication_sign").toString()));


							singleRow.put("admission_date",EnglishtoBangla.getEngToBanDate(singleRow.get("admission_date").toString()));
							singleRow2.put("admission_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("admission_date").toString()));

							singleRow.put("delivery_date",EnglishtoBangla.getEngToBanDate(singleRow.get("delivery_date").toString()));
							singleRow2.put("delivery_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("delivery_date").toString()));

							singleRow.put("outcome_time",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("outcome_time").toString()));
							singleRow2.put("outcome_time",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("outcome_time").toString()));

                            singleRow.put("live_birth",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("live_birth").toString()));
                            singleRow.put("still_birth",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("still_birth").toString()));
                            singleRow.put("still_birth_fresh",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("still_birth_fresh").toString()));
                            singleRow.put("still_birth_macerated",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("still_birth_macerated").toString()));

                            singleRow2.put("live_birth",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("live_birth").toString()));
                            singleRow2.put("still_birth",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("still_birth").toString()));
                            singleRow2.put("still_birth_fresh",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("still_birth_fresh").toString()));
                            singleRow2.put("still_birth_macerated",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("still_birth_macerated").toString()));

                            singleRow.put("nBoy",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("nBoy").toString()));
                            singleRow.put("nGirl",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("nGirl").toString()));

                            singleRow2.put("nBoy",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("nBoy").toString()));
                            singleRow2.put("nGirl",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("nGirl").toString()));


                            singleRow.put("birthWeight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("birthWeight").toString()));
                            singleRow2.put("birthWeight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("birthWeight").toString()));


                            singleRow.put("pncm1_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncm1_service_date").toString()));
                            singleRow.put("pncm1_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm1_bp").toString()));
                            singleRow.put("pncm1_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm1_temp").toString()));
                            singleRow.put("pncm1_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm1_hemoglobin").toString()));

                            singleRow.put("pncm2_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncm2_service_date").toString()));
                            singleRow.put("pncm2_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm2_bp").toString()));
                            singleRow.put("pncm2_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm2_temp").toString()));
                            singleRow.put("pncm2_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm2_hemoglobin").toString()));

                            singleRow.put("pncm3_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncm3_service_date").toString()));
                            singleRow.put("pncm3_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm3_bp").toString()));
                            singleRow.put("pncm3_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm3_temp").toString()));
                            singleRow.put("pncm3_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm3_hemoglobin").toString()));

                            singleRow.put("pncm4_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncm4_service_date").toString()));
                            singleRow.put("pncm4_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm4_bp").toString()));
                            singleRow.put("pncm4_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm4_temp").toString()));
                            singleRow.put("pncm4_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncm4_hemoglobin").toString()));


                            singleRow2.put("pncm1_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncm1_service_date").toString()));
                            singleRow2.put("pncm1_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm1_bp").toString()));
                            singleRow2.put("pncm1_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm1_temp").toString()));
                            singleRow2.put("pncm1_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm1_hemoglobin").toString()));

                            singleRow2.put("pncm2_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncm2_service_date").toString()));
                            singleRow2.put("pncm2_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm2_bp").toString()));
                            singleRow2.put("pncm2_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm2_temp").toString()));
                            singleRow2.put("pncm2_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm2_hemoglobin").toString()));

                            singleRow2.put("pncm3_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncm3_service_date").toString()));
                            singleRow2.put("pncm3_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm3_bp").toString()));
                            singleRow2.put("pncm3_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm3_temp").toString()));
                            singleRow2.put("pncm3_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm3_hemoglobin").toString()));

                            singleRow2.put("pncm4_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncm4_service_date").toString()));
                            singleRow2.put("pncm4_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm4_bp").toString()));
                            singleRow2.put("pncm4_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm4_temp").toString()));
                            singleRow2.put("pncm4_hemoglobin",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncm4_hemoglobin").toString()));
//--------------------pncc
                            singleRow.put("pncc1_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncc1_service_date").toString()));
                            singleRow.put("pncc1_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc1_temp").toString()));
                            singleRow.put("pncc1_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc1_weight").toString()));
                            singleRow.put("pncc1_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc1_breathing_per_minute").toString()));

                            singleRow.put("pncc2_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncc2_service_date").toString()));
                            singleRow.put("pncc2_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc2_temp").toString()));
                            singleRow.put("pncc2_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc2_weight").toString()));
                            singleRow.put("pncc2_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc2_breathing_per_minute").toString()));

                            singleRow.put("pncc3_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncc3_service_date").toString()));
                            singleRow.put("pncc3_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc3_temp").toString()));
                            singleRow.put("pncc3_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc3_weight").toString()));
                            singleRow.put("pncc3_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc3_breathing_per_minute").toString()));

                            singleRow.put("pncc4_service_date",EnglishtoBangla.getEngToBanDate(singleRow.get("pncc4_service_date").toString()));
                            singleRow.put("pncc4_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc4_temp").toString()));
                            singleRow.put("pncc4_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc4_weight").toString()));
                            singleRow.put("pncc4_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pncc4_breathing_per_minute").toString()));


                            singleRow2.put("pncc1_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncc1_service_date").toString()));
                            singleRow2.put("pncc1_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc1_temp").toString()));
                            singleRow2.put("pncc1_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc1_weight").toString()));
                            singleRow2.put("pncc1_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc1_breathing_per_minute").toString()));

                            singleRow2.put("pncc2_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncc2_service_date").toString()));
                            singleRow2.put("pncc2_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc2_temp").toString()));
                            singleRow2.put("pncc2_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc2_weight").toString()));
                            singleRow2.put("pncc2_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc2_breathing_per_minute").toString()));

                            singleRow2.put("pncc3_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncc3_service_date").toString()));
                            singleRow2.put("pncc3_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc3_temp").toString()));
                            singleRow2.put("pncc3_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc3_weight").toString()));
                            singleRow2.put("pncc3_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc3_breathing_per_minute").toString()));

                            singleRow2.put("pncc4_service_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("pncc4_service_date").toString()));
                            singleRow2.put("pncc4_temp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc4_temp").toString()));
                            singleRow2.put("pncc4_weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc4_weight").toString()));
                            singleRow2.put("pncc4_breathing_per_minute",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pncc4_breathing_per_minute").toString()));


                            singleRow.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("serial").toString()));
                            singleRow2.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("serial").toString()));

                            singleRow.put("serial_date",EnglishtoBangla.getEngToBanDate(singleRow.get("serial_date").toString()));
                            singleRow2.put("serial_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("serial_date").toString()));



						}catch (Exception e){
						    e.printStackTrace();
						}
				%>
				<h1 align="middle">মা ও নবজাতকের সেবা রেজিস্টার</h1>
				<table   class="mnc-table-left"  border="1" cellspacing="0" style="font-size:11px; min-height: 8.5in;table-layout:fixed;">
					<tbody >
					<tr>
						<th rowspan="3" >রেজিঃ নম্বর</br> (বাৎসরিক </br>ক্রমিক/সন) </br>ও</br> তারিখ</th>
						<th  colspan="2" class="nowrap"><b>সেবা গ্রহীতার সাধারণ তথ্য</b></th>
						<th  colspan="8" ><b>গর্ভকালীন সেবা</b></th>
						<th colspan="3" ><b>প্রসাবকালীন সেবা</b></th>
					</tr>
					<tr>
						<th rowspan="2" class="nowrap">সেবা গ্রহীতার পরিচিতি</th>
						<th rowspan="2" class="nowrap">গর্ভবতী মহিলার তথ্য</th>
						<th rowspan="2" nowrap>বর্তমান সেবার তথ্য</th>
						<th rowspan="2" nowrap >পরিদর্শন ১</th>
						<th rowspan="2" nowrap >পরিদর্শন ২</th>
						<th rowspan="2" nowrap >পরিদর্শন ৩</th>
						<th rowspan="2" nowrap >পরিদর্শন ৪</th>
						<th rowspan="2" >পরিদর্শন ৪ এর অধিক</td>
						<th rowspan="2" class="nowrap">অসুবিধা ও রোগ</td>
						<th rowspan="2" class="nowrap">চিকিৎসা ও পরামর্শ</td>

						<th rowspan="2" class="nowrap">প্রসব সংক্রান্ত তথ্য</th>
						<th rowspan="2" class="nowrap">প্রসব সংক্রান্ত জটিলতা</th>
						<th rowspan="2" class="nowrap">চিকিৎসা ও পরামর্শ</th>
					</tr>
					<tr>

					</tr>
					<tr>
						<th class="nowrap">১</th>
						<th class="nowrap">২</th>
						<th class="nowrap">৩</th>
						<th class="nowrap">৪</th>
						<th class="nowrap">৫</th>
						<th >৬</th>
						<th >৭</th>
						<th class="nowrap">৮</th>
						<th class="nowrap">৯</th>
						<th class="nowrap">১০</th>
						<th class="nowrap">১১</th>
						<th class="nowrap">১২</th>
						<th >১৩</th>
						<th >১৪</th>
					</tr>
						<%--Frist Client--%>
					<tr>
						<td rowspan="14" class="nowrap">
							<div class="row">
								<div class="col-md-12">
									<span class="bold-italic" style="text-align: center"><%
                                        if(singleRow.get("serial") !=""){
                                            out.print(singleRow.get("serial"));
                                            out.print('/');
                                            String date = singleRow.get("serial_date").toString();
                                            out.println(date.substring(date.length() - 4));
                                        }
                                    %>
                                    </span>
                                </br>
                                    <span class="bold-italic"><% out.println(singleRow.get("serial_date"));%></span>
								</div>
							</div>
						</td>
						<td nowrap rowspan="14" class="pad-bot-15">
							<div class="row">
								<div class="col-md-12">
									নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_name"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_age"));%> </span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									স্বামীর নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_husband"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									গ্রাম/মহল্লা: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_village"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									ইউনিট/ওয়ার্ড নম্বর: &nbsp;
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									বাড়ি/জিআর/হোল্ডিং নম্বর: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_house_holding"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									দম্পতি নম্বর : &nbsp;
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									মোবাইল নম্বর : &nbsp;<span class="bold-italic">0<% out.println(singleRow.get("client_mobile"));%></span>
								</div>
							</div>
						</td>
						<td nowrap rowspan="14" class="pad-bot-5">
							<div class="row">
								<div class="col-md-12">
									<b>শেষ মাসিকের তারিখ: &nbsp;</b><span class="bold-italic"><% out.println(singleRow.get("client_lmp"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>প্রসবের সম্ভাব্য তারিখ: &nbsp;</b><span class="bold-italic"><% out.println(singleRow.get("client_edd"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>প্যারা: &nbsp;</b><span class="bold-italic"><% out.println(singleRow.get("client_para"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>গ্রাভিডা: &nbsp;</b><span class="bold-italic"> <% out.println(singleRow.get("client_gravida"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>জীবিত সন্তান সংখ্যা: &nbsp;</b> <span class="bold-italic"> <% out.println(singleRow.get("total_child")); %></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									ছেলে: &nbsp; <span class="bold-italic"> <% out.println(singleRow.get("client_son"));%></span>  মেয়ে:<span class="bold-italic"> <% out.println(singleRow.get("client_dau"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>শেষ সন্তানের বয়স: &nbsp;</b><span class="bold-italic"> <% out.println(singleRow.get("client_last_child_age"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>উচ্চতা : &nbsp;</b><span class="bold-italic"> <% out.println(singleRow.get("client_height"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>রক্তের গ্রুপ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow.get("client_blood_group"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
                                    <b>টিটি টিকা:</b> (টিক দিন)</br>
                                    ১<input type="checkbox" disabled <%out.println(singleRow.get("is_tt1"));%>>
                                    ২<input type="checkbox" disabled <%out.println(singleRow.get("is_tt2"));%>>
                                    ৩<input type="checkbox" disabled <%out.println(singleRow.get("is_tt3"));%>>
                                    ৪<input type="checkbox" disabled <%out.println(singleRow.get("is_tt4"));%>>
                                    ৫<input type="checkbox" disabled <%out.println(singleRow.get("is_tt5"));%>></div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<b>পূর্বের ইতিহাস ঝুঁকিপূর্ণ কি না?</b>
								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<%
										String riskHistoryNote = singleRow.get("client_risk_his_note").toString();
										String [] risk ={"","রক্তপাত (প্রসবপূর্ব অথবা প্রসবোত্তর)","বিলম্বিত প্রসব","বাধাগ্রস্থ প্রসব","গর্ভফুল না পড়া","মৃত সন্তান প্রসব করা","৪৮ ঘন্টার মধ্যে নবজাতক মরে যাওয়া","পা বা সমস্ত শরীর বেশি রকম ফোলা (ইডিমা)","একল্যাম্পসিয়া বা খিঁচুনিসহ বার বার অজ্ঞান হওয়া","সিজারিয়ান অপারেশন বা যন্ত্রের মাধ্যমে (ফরসেপ/ভেকুয়াম) প্রসব"};
										String riskStr = "";
										for(int k=0;k<riskHistoryNote.split(",").length;k++){
											if(riskHistoryNote.length()>0) {
												try{
													riskStr +=  risk[Integer.parseInt(riskHistoryNote.split(",")[k])]+",</br>";
												}catch (Exception e){

												}
											}
										}
										if(riskHistoryNote.equals("7,8") || riskHistoryNote.equals("8,7") || riskHistoryNote.equals("")){
									%>
									<input type="checkbox"  disabled> হ্যাঁ &nbsp;
									<input type="checkbox" checked disabled> না
									<%
									}else{
									%>
									<input type="checkbox" checked disabled> হ্যাঁ &nbsp;
									<input type="checkbox"  disabled> না
									<%}%>

								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									হ্যাঁ হলে উল্লেখ </br>করুন</br>
									<span> <% out.println(riskStr);%></span>
								</div>
							</div>

						</td>

						<td nowrap>সেবাগ্রহনের তারিখ</td>
						<td nowrap >
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_service_date"));%></span>
							</div>
						</td>
						<td nowrap >
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_service_date"));%></span>
							</div>
						</td>
						<td nowrap >
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_service_date"));%></span>
							</div>
						</td>
						<td nowrap >
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_service_date"));%></span>
							</div>
						</td>

						<td>&nbsp;</td>

						<td rowspan="14" nowrap >
							<div nowrap class="col-md-12" >
								পরিদর্শন ১:
								<span class="bold-italic">
										</br>অসুবিধাঃ <%
									String [] anc_sym = {"","ব্যথা সহ বা ব্যথা ছাড়া যে কোন ধরনের প্রসব-পূর্ব রক্তস্রাব","খিচুনি হওয়া৷", "হাত, পা, মুখ ফুলে যাওয়া", "প্রচন্ড মাথা ব্যথা ও চোখে ঝাপসা দেখা", "খিঁচুনি ও অজ্ঞান হয়ে যাওয়া", "বিলম্বিত ও বাধাপ্রাপ্ত প্রসব বেদনা ১২ ঘণ্টার বেশী", "মাথা ছাড়া অন্য কোন উপস্থাপিত অঙ্গ থাকলে", "রক্তস্রাব", "শরীরে পানি আসা", "তিন দিনের বেশি জ্বর", "দুর্গন্ধযুক্ত স্রাব"};
									String symptom_str = "";
									String [] anc1_symptom = singleRow.get("anc1_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc1_symptom.length;sym++){
											if(anc1_symptom[sym].length()>0){
												symptom_str += "</br>"+anc_sym[Integer.parseInt(anc1_symptom[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(symptom_str);
								%>
									</br>রোগঃ <% out.println(singleRow.get("anc1_disease"));%>
									</span>
							</div>
							<div class="col-md-12 nowrap" >
								পরিদর্শন ২:
								<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
									String [] anc2_symptom = singleRow.get("anc2_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc2_symptom.length;sym++){
											if(anc2_symptom[sym].length()>0){
												symptom_str +=  "</br>"+anc_sym[Integer.parseInt(anc2_symptom[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(symptom_str);%>
									</br>রোগঃ <% out.println(singleRow.get("anc2_disease"));%>
									</span>
							</div>
							<div class="col-md-12 nowrap" >
								পরিদর্শন ৩:
								<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
									String [] anc3_symptom = singleRow.get("anc3_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc3_symptom.length;sym++){
											if(anc3_symptom[sym].length()>0){
												symptom_str += "</br>"+ anc_sym[Integer.parseInt(anc3_symptom[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(symptom_str);%>
									</br>রোগঃ <% out.println(singleRow.get("anc3_disease"));%>
									</span>
							</div>
							<div class="col-md-12 nowrap" >
								পরিদর্শন ৪:
								<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
									String [] anc4_symptom = singleRow.get("anc4_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc4_symptom.length;sym++){
											if(anc4_symptom[sym].length()>0){
												symptom_str += "</br>"+ anc_sym[Integer.parseInt(anc4_symptom[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(symptom_str);%>
									</br>রোগঃ <% out.println(singleRow.get("anc4_disease"));%>
									</span>
								</br>পরিদর্শন (৪ এর অধিক):
								<br> পুনঃ পরিদর্শনঃ
							</div>
						</td>
						<td rowspan="14" nowrap="">
							<div class="col-md-12" >
								<b>পরিদর্শন ১:</b>
								<span class="bold-italic">
										</br>চিকিৎসাঃ <%
									String [] anc_treatment = {"", "INJ. MgSo4","CAP. AMOXYCILLIN 500 MG",
											"TAB. ALBENDAZOLE 400 MG",
											"TAB. DIAZEPAM 5 MG",
											"TAB. METRONIDAZOL 400 MG",
											"TAB. PARACETAMOL 500 MG",
											"TAB. VITAMIN B COMPLEX",
											"TAB. ANTACID (650 MG)",
											"TAB. DROTAVERIN HYDROCHLORIDE (40 MG)",
											"TAB. PANTOPRAZOLE (20 MG)",
											"TAB. CHLORPHENIRAMINE MALEATE 4 MG)",
											"TAB. SALBUTAMOL (4 MG)",
											"TAB. CALCIUM CARBONATE (500 MG)",
											"TAB. COTRIMOXAZOLE (120 MG) DISPERSIBLE",
											"TAB. COTRIMOXAZOLE (960 MG)",
											"POWDER FOR SUSPENSION AMOXYCILLIN (PAED) 15 ML",
											"COTRIMOXAZOLE SUSPENSION (120 MG/5ML) 60ML",
											"PARACETAMOL SUSPENSION (120 MG./ 5 ML.) 60 ML.",
											"OINT. BENZOIC ACID (6%) AND SALICYLIC ACID (3%) - 1 KG JAR",
											"POWDER FOR SUSPENSION AMOXYCILLIN (100ML)",
											"DISPENSING ENVELOP(14 CM. X 9 CM.) SELF-LOCKING POLYBAG",
											"CAP. DOXICYCLINE 100 MG",
											"TAB. IRON & FOLIC ACID",
											"TAB. FOLIC ACID (5mg)"};
									String [] anc_advice = {"গর্ভবতী মাকে কমপক্ষে ৪ বার গর্ভকালীন সময়ে ভিজিটে আসুন",
											"অন্য সময়ের চাইতে বেশী পরিমাণে (কম পক্ষে ১ মুঠ চালের ভাত ও ১ চামচ ডাল বেশী) খান",
											"বেশী পরিমাণে পানি খান (কম পক্ষে ৮ গ্লাস)",
											"রক্তস্বল্পতা প্রতিরোধ করার জন্য মাকে আয়রন সমৃদ্ধ খাবার যেমন সবুজ শাকসবজি, ডাল, ডিম খেতে হবে এবং এগুলোর সাথে আয়রন এবং ফলিক এসিড ট্যাবলেট খেতে খান",
											"যদি পা ফুলে যায় অতিরিক্ত (আলগা) লবণ খাওয়া বাদ দিতে হবে এবং পর্যাপ্ত বিশ্রাম নিন",
											"দুপুরের খাবার পর কমপক্ষে ২ ঘন্টা বিশ্রাম ও রাতে ৮ ঘন্টা ঘুমান (বিশেষ করে বাম কাত হয়ে)",
											"স্বাভাবিক সাংসারিক হালকা কাজকর্মে উত্‍সাহ কিন্তু ভারী কাজে নিষেধ",
											"প্রথম তিন মাস ও শেষ তিনমাস দীর্ঘ ভ্রমণ এড়িয়ে চলুন",
											"টাইট (আঁটসাঁট) পোশাক পরিহার এবং ঢিলা ও আরামদায়ক পোশাক পড়ুন",
											"(গর্ভপাতের আশংকা থাকলে) প্রথম তিনমাস এবং শেষের দুইমাস স্বামী সহবাস থেকে বিরত থাকুন",
											"টিটি টিকা দিন",
											"বারবার অল্প করে খান , তৈলাক্ত ও মসলা জাতীয় খাবার এবং ধূমপানে বিরত থাকুন",
											"তিন বেলা ভারী খাবারের পরিবর্তে অল্প অল্প করে বারেবারে খান",
											"(কোষ্ঠকাঠিন্য হলে) প্রচুর পরিমাণ পানি, শাকসবজি, হালকা গরম দুধ এবং ইসুপগুলের ভুষি খান",
											"(পায়ে পানি আসলে) পা সামান্য উঁচু রাখা"};
									String treatment_str = "";
									String advice_str = "দেওয়া হয়েছে";
									String [] anc1_treatment = singleRow.get("anc1_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc1_treatment.length;sym++){
											if(anc1_treatment[sym].length()>0){
												treatment_str += "</br>"+ anc_treatment[Integer.parseInt(anc1_treatment[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(treatment_str.toLowerCase());
								%>
									</br>পরামর্শঃ <%
									out.println(advice_str);%>
									</span>
							</div>
							<div class="col-md-12 nowrap" >
								<b> পরিদর্শন ২: </b>
								<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
									String [] anc2_treatment = singleRow.get("anc2_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc2_treatment.length;sym++){
											if(anc2_treatment[sym].length()>0){
												treatment_str += "</br>"+ anc_treatment[Integer.parseInt(anc2_treatment[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(treatment_str.toLowerCase());%>
									</br>পরামর্শঃ <%
									out.println(advice_str);%>
									</span>
							</div>
							<div class="col-md-12 nowrap" >
								<b> পরিদর্শন ৩:</b>
								<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
									String [] anc3_treatment = singleRow.get("anc3_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc3_treatment.length;sym++){
											if(anc3_treatment[sym].length()>0){
												treatment_str +=  "</br>"+ anc_treatment[Integer.parseInt(anc3_treatment[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(treatment_str.toLowerCase());%>
									</br>পরামর্শঃ <%
									out.println(advice_str);%>
									</span>
							</div>
							<div class="col-md-12 nowrap" >
								<b>পরিদর্শন ৪:</b>
								<span class="bold-italic">
										</br>চিকিৎসাঃ <%
									treatment_str = "";
									String [] anc4_treatment = singleRow.get("anc4_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
									try{
										for(int sym=0;sym<anc4_treatment.length;sym++){
											if(anc4_treatment[sym].length()>0){
												treatment_str += "</br>"+ anc_treatment[Integer.parseInt(anc4_treatment[sym])];
											}
										}
									}catch (Exception e){
									}
									out.println(treatment_str.toLowerCase());%>
									</br>পরামর্শঃ <%
									advice_str = "";
									out.println(advice_str);%>
									</span>
								</br><b>পরিদর্শন (৪ এর অধিক):</b>
								<br> পুনঃ পরিদর্শনঃ
								<br>রেফার(     )
								<br>কেন্দ্রের নাম ও কারনঃ
								<br>রেফার(     )
								<br>কেন্দ্রের নাম ও কারনঃ
							</div>

						</td>
						<td nowrap rowspan="14">
							<div class="col-md-12" >
								<b>প্রসবের স্থান: &nbsp;</b> <span class="bold-italic"><%
								if(singleRow.get("delivery_place").toString().equals("0")){
									out.println("বাড়ি");
								}else if(!singleRow.get("delivery_place").toString().equals("")){
									out.println("সেবাকেন্দ্রে");
								}
							%></span>
							</div>
							<div class="col-md-12" >
									<span class="bold-italic"><%
										if(singleRow.get("delivery_place").toString().equals("1")){
											out.println("মেডিকেল কলেজ হাসপাতাল");
										}else if(singleRow.get("delivery_place").toString().equals("2")){
											out.println("জেলা সদর হাসপাতাল / সরকারি হাসপাতাল");
										}else if(singleRow.get("delivery_place").toString().equals("3")){
											out.println("মা ও শিশু কল্যাণ কেন্দ্র");
										}else if(singleRow.get("delivery_place").toString().equals("4")){
											out.println("উপজেলা স্বাস্থ্য কমপ্লেক্স");
										}else if(singleRow.get("delivery_place").toString().equals("5")){
											out.println("ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র");
										}else if(singleRow.get("delivery_place").toString().equals("6")){
											out.println("কমিউনিটি ক্লিনিক");
										}else if(singleRow.get("delivery_place").toString().equals("7")){
											out.println("এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র");
										}else if(singleRow.get("delivery_place").toString().equals("8")){
											out.println("প্রাইভেট ক্লিনিক / হাসপাতাল");
										}else if(singleRow.get("delivery_place").toString().equals("9")){
											out.println("অন্যান্য");
										}
									%></span>
							</div>
							<div class="col-md-6" >
								<b>ভর্তির তারিখ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow.get("admission_date"));%></span>
							</div>
							<div class="col-md-6" >
								<b>ওয়ার্ড/শয্যা: &nbsp;</b>
							</div>
							<div class="col-md-8" >
								<b>প্রসবের তারিখ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow.get("delivery_date"));%></span>
							</div>
							<div class="col-md-4" >
								<b>সময়: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow.get("outcome_time"));%></span>
							</div>
							<div class="col-md-12" >
								<b>প্রসবের ধরণ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow.get("delivery_type"));%></span>
							</div>
							<div class="col-md-12" >
								<b>প্রসবের ফলাফল: &nbsp;</b> জীবিত জন্ম:
								<span class="bold-italic"> <% out.println(singleRow.get("live_birth"));%>
								</span>
							</div>
							<div class="col-md-12" >
								মৃত জন্ম: &nbsp;<% out.println(singleRow.get("still_birth"));%>
								ফ্রেশ: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("still_birth_fresh"));%></span>  ম্যাসারেটেড: <span class="bold-italic"><% out.println(singleRow.get("still_birth_macerated"));%></span>
							</div>
							<div class="col-md-12" >
								<b>লিঙ্গ: &nbsp;</b> <span class="bold-italic"> ছেলে:<% out.println(singleRow.get("nBoy"));%> মেয়ে:<% out.println(singleRow.get("nGirl"));%></span>
							</div>
							<div class="col-md-12" >
								<b>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনায় (AMTSL) যা</br>
									অনুসরণ করা হয়েছে -</b>
								</br>প্রসবের ১ মিনিটের মধ্যে অক্সিটোসিন প্রয়োগ:<span class="bold-italic"> <% out.println(singleRow.get("apply_oxytocin"));%></span>
								</br>গর্ভফুল প্রসবের জন্য নাভিরজ্জুতে নিয়ন্ত্রিত টান<span class="bold-italic"> <% out.println(singleRow.get("applyTraction"));%></span>
								</br>জরায়ু ম্যাসাজ<span class="bold-italic"> <% out.println(singleRow.get("uterusMassage"));%></span>
							</div>
							<div class="col-md-12" >
								অক্সিটোসিন না থাকলে মিসোপ্রোস্টল খাওয়ানো হয়েছে: &nbsp;
								<span class="bold-italic"><% out.println(singleRow.get("misoprostol"));%></span>
							</div>
							<div class="col-md-12" >
								<b>প্রসব সম্পাদনকারীর নাম,পদবী</b> </br>
								<span class="bold-italic">
									<% out.println(singleRow.get("attendant_name"));%>,
									<% out.println(singleRow.get("attendant_designation"));%>
								</span>
							</div>
						</td>
						<td nowrap rowspan="14">
							<br><input type="checkbox" disabled <%out.println(singleRow.get("excessBloodLoss"));%>> অতিরিক্ত রক্তক্ষরণ &nbsp;
							<br><input type="checkbox" disabled <%out.println(singleRow.get("lateDelivery"));%>> বিলম্বিত প্রসব &nbsp;&nbsp;&nbsp;
							<br><input type="checkbox" disabled <%out.println(singleRow.get("blockedDelivery"));%>> বাধাগ্রস্থ প্রসব &nbsp;&nbsp;&nbsp;
							<br><input type="checkbox" disabled <%out.println(singleRow.get("placenta"));%>> গর্ভফুল না পড়া &nbsp;&nbsp;&nbsp;
							<br><input type="checkbox" disabled <%out.println(singleRow.get("headache"));%>> প্রচন্ড মাথা ব্যাথা &nbsp;&nbsp;&nbsp;
							<br><input type="checkbox" disabled <%out.println(singleRow.get("blurryVision"));%>> চোখে ঝাপসা</br> দেখা
							<br><input type="checkbox" disabled <%out.println(singleRow.get("otherBodyPart"));%>> মাথা ছাড়া অন্য </br>কোন অঙ্গ বের</br> হওয়া &nbsp;&nbsp;&nbsp;
							<br><input type="checkbox" disabled <%out.println(singleRow.get("convulsion"));%>> খিচুঁনি &nbsp;&nbsp;&nbsp;
							<br><input type="checkbox" disabled <%out.println(singleRow.get("others"));%>> অন্যান্য
						</td>
						<td rowspan="14">
							<b>ইপিসিওট্মি করা হয়েছে কি না?</b>
							</br><input type="checkbox" disabled <%out.println(singleRow.get("episiotomy"));%>> হ্যাঁ
							</br><input type="checkbox" disabled <%out.println(singleRow.get("episiotomy"));%>> না
							</br> <b>রেফার</b>(কেন্দ্রের নাম ও কারন)
						</td>
					</tr>
					<tr>
						<td class="nowrap">রক্তচাপ</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_bp"));%></span>
							</div>
						</td>
						<td nowrap >
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_bp"));%></span>
							</div>
						</td>
						<td nowrap >
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_bp"));%></span>
							</div>
						</td>
						<td nowrap >
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_bp"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td nowrap >ওজন (কেজি)</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_weight"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_weight"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_weight"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_weight"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td >ইডিমা</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_edema"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_edema"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_edema"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_edema"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td nowrap>জরায়ুর উচ্চতা (সপ্তাহ)</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_uterus_height"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_uterus_height"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_uterus_height"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_uterus_height"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>

					</tr>
					<tr>
						<td nowrap>ফিটাসের হৃদপিণ্ডের গতি (প্রতি মিনিটে)</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_fetus_heart_rate"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_fetus_heart_rate"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_fetus_heart_rate"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_fetus_heart_rate"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td nowrap>ফিটাল প্রেজেনটেশন</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_fetal_presentation"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_fetal_presentation"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_fetal_presentation"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_fetal_presentation"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td nowrap>রক্তস্বল্পতা</td>
						<td nowrap>
							<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc1_anemia"));
											%>
										</span>
							</div>
						</td>
						<td>
							<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc2_anemia"));
											%>
										</span>
							</div>
						</td>
						<td>
							<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc3_anemia"));
											%>
										</span>
							</div>
						</td>
						<td>
							<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc3_anemia"));
											%>
										</span>
							</div>
						</td>
						<td>&nbsp;</td>

					</tr>
					<tr>
						<td nowrap>হিমোগ্লোবিন</td>
						<td nowrap>
							<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc1_hemoglobin"));
											%>
										</span>
							</div>
						</td>
						<td><div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc2_hemoglobin"));
											%>
										</span>
						</div></td>
						<td><div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc3_hemoglobin"));
											%>
										</span>
						</div></td>
						<td><div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("anc4_hemoglobin"));
											%>
										</span>
						</div></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td nowrap>জন্ডিস</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_jaundice"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_jaundice"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_jaundice"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_jaundice"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td nowrap>প্রস্রাব পরিক্ষা সুগার</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_urine_sugar"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_urine_sugar"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_urine_sugar"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_urine_sugar"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>


					</tr>
					<tr>
						<td nowrap>প্রস্রাব পরিক্ষা এলবুমিন</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc1_urine_albumin"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("anc2_urine_albumin"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc3_urine_albumin"));%></span>
							</div>
						</td>
						<td nowrap>
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("anc4_urine_albumin"));%></span>
							</div>
						</td>
						<td>&nbsp;</td>


					</tr>
					<tr>
						<td nowrap>বিপদ চিহ্ন / জটিলতা</td>

						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>

					</tr>
					<tr>
						<td nowrap>পুনঃ পরিদর্শন </td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
		<%--2nd Client--%>
						<tr>
							<td rowspan="14" class="nowrap">
								<div class="row">
                                    <div class="col-md-12">
									<span class="bold-italic" style="text-align: center"><%
                                        if(singleRow2.get("serial") !=""){
                                            out.print(singleRow2.get("serial"));
                                            out.print('/');
                                            String date = singleRow2.get("serial_date").toString();
                                            out.println(date.substring(date.length() - 4));
                                        }
                                    %>
                                    </span>
                                        </br>
                                        <span class="bold-italic"><% out.println(singleRow2.get("serial_date"));%></span>
                                    </div>
								</div>
							</td>
							<td nowrap rowspan="14" class="pad-bot-15">
								<div class="row">
									<div class="col-md-12">
										নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_age"));%> </span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামীর নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_husband"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম/মহল্লা: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_village"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বাড়ি/জিআর/হোল্ডিং নম্বর: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_house_holding"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										দম্পতি নম্বর : &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										মোবাইল নম্বর : &nbsp;<span class="bold-italic">0<% out.println(singleRow2.get("client_mobile"));%></span>
									</div>
								</div>
							</td>
							<td nowrap rowspan="14" class="pad-bot-5">
								<div class="row">
									<div class="col-md-12">
										<b>শেষ মাসিকের তারিখ: &nbsp;</b></br><span class="bold-italic"><% out.println(singleRow2.get("client_lmp"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>প্রসবের সম্ভাব্য তারিখ: &nbsp;</b></br><span class="bold-italic"><% out.println(singleRow2.get("client_edd"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>প্যারা: &nbsp;</b><span class="bold-italic"><% out.println(singleRow2.get("client_para"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>গ্রাভিডা: &nbsp;</b><span class="bold-italic"> <% out.println(singleRow2.get("client_gravida"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>জীবিত সন্তান সংখ্যা: &nbsp;</b> <span class="bold-italic"> <% out.println(singleRow2.get("total_child")); %></span>

									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ছেলে: &nbsp; <span class="bold-italic"> <% out.println(singleRow2.get("client_son"));%></span>  মেয়ে:<span class="bold-italic"> <% out.println(singleRow.get("client_dau"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>শেষ সন্তানের বয়স: &nbsp;</b></br><span class="bold-italic"> <% out.println(singleRow2.get("client_last_child_age"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>উচ্চতা : &nbsp;</b><span class="bold-italic"> <% out.println(singleRow2.get("client_height"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>রক্তের গ্রুপ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow2.get("client_blood_group"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>টিটি টিকা:</b> (টিক দিন)</br>
                                        ১<input type="checkbox" disabled <%out.println(singleRow2.get("is_tt1"));%>>
                                        ২<input type="checkbox" disabled <%out.println(singleRow2.get("is_tt2"));%>>
                                        ৩<input type="checkbox" disabled <%out.println(singleRow2.get("is_tt3"));%>>
                                        ৪<input type="checkbox" disabled <%out.println(singleRow2.get("is_tt4"));%>>
                                        ৫<input type="checkbox" disabled <%out.println(singleRow2.get("is_tt5"));%>>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<b>পূর্বের ইতিহাস</br> ঝুঁকিপূর্ণ কি না?</b>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-12">
										<%
											 riskHistoryNote = singleRow2.get("client_risk_his_note").toString();
											riskStr = "";
											for(int k=0;k<riskHistoryNote.split(",").length;k++){
												if(riskHistoryNote.length()>0) {
													try{
														riskStr +=  risk[Integer.parseInt(riskHistoryNote.split(",")[k])]+",</br>";
													}catch (Exception e){

													}
												}
											}
											if(riskHistoryNote.equals("7,8") || riskHistoryNote.equals("8,7") || riskHistoryNote.equals("")){
										%>
										<input type="checkbox"  disabled> হ্যাঁ &nbsp;
										<input type="checkbox" checked disabled> না
										<%
										}else{
										%>
										<input type="checkbox" checked disabled> হ্যাঁ &nbsp;
										<input type="checkbox"  disabled> না
										<%}%>

									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										হ্যাঁ হলে উল্লেখ </br>করুন</br>
										<span> <% out.println(riskStr);%></span>
									</div>
								</div>

							</td>

							<td nowrap>সেবাগ্রহনের তারিখ</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_service_date"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_service_date"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_service_date"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_service_date"));%></span>
								</div>
							</td>

							<td>&nbsp;</td>

							<td rowspan="14" nowrap >
								<div nowrap class="col-md-12" >
									পরিদর্শন ১:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%
										symptom_str = "";
										 anc1_symptom = singleRow2.get("anc1_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc1_symptom.length;sym++){
												if(anc1_symptom[sym].length()>0){
													symptom_str += "</br>"+anc_sym[Integer.parseInt(anc1_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);
									%>
										</br>রোগঃ <% out.println(singleRow2.get("anc1_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ২:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										anc2_symptom = singleRow2.get("anc2_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc2_symptom.length;sym++){
												if(anc2_symptom[sym].length()>0){
													symptom_str +=  "</br>"+anc_sym[Integer.parseInt(anc2_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow2.get("anc2_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ৩:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										anc3_symptom = singleRow2.get("anc3_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc3_symptom.length;sym++){
												if(anc3_symptom[sym].length()>0){
													symptom_str += "</br>"+ anc_sym[Integer.parseInt(anc3_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow2.get("anc3_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ৪:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										anc4_symptom = singleRow2.get("anc4_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc4_symptom.length;sym++){
												if(anc4_symptom[sym].length()>0){
													symptom_str += "</br>"+ anc_sym[Integer.parseInt(anc4_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow2.get("anc4_disease"));%>
									</span>
									</br>পরিদর্শন (৪ এর অধিক):
									<br> পুনঃ পরিদর্শনঃ
								</div>
							</td>
							<td rowspan="14" nowrap="">
								<div class="col-md-12" >
									<b>পরিদর্শন ১:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%

										treatment_str = "";
										advice_str = "দেওয়া হয়েছে";
										anc1_treatment = singleRow2.get("anc1_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc1_treatment.length;sym++){
												if(anc1_treatment[sym].length()>0){
													treatment_str += "</br>"+ anc_treatment[Integer.parseInt(anc1_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());
									%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b> পরিদর্শন ২: </b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
										anc2_treatment = singleRow2.get("anc2_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc2_treatment.length;sym++){
												if(anc2_treatment[sym].length()>0){
													treatment_str += "</br>"+ anc_treatment[Integer.parseInt(anc2_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b> পরিদর্শন ৩:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
										anc3_treatment = singleRow2.get("anc3_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc3_treatment.length;sym++){
												if(anc3_treatment[sym].length()>0){
													treatment_str +=  "</br>"+ anc_treatment[Integer.parseInt(anc3_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ৪:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%
										treatment_str = "";
										anc4_treatment = singleRow2.get("anc4_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<anc4_treatment.length;sym++){
												if(anc4_treatment[sym].length()>0){
													treatment_str += "</br>"+ anc_treatment[Integer.parseInt(anc4_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());%>
										</br>পরামর্শঃ <%
										advice_str = "";
										out.println(advice_str);%>
									</span>
									</br><b>পরিদর্শন (৪ এর অধিক):</b>
									<br> পুনঃ পরিদর্শনঃ
									<br>রেফার(     )
									<br>কেন্দ্রের নাম ও কারনঃ
									<br>রেফার(     )
									<br>কেন্দ্রের নাম ও কারনঃ
								</div>

							</td>
							<td nowrap rowspan="14">
								<div class="col-md-12" >
									<b>প্রসবের স্থান: &nbsp;</b> <span class="bold-italic"><%
									if(singleRow2.get("delivery_place").toString().equals("0")){
										out.println("বাড়ি");
									}else if(!singleRow2.get("delivery_place").toString().equals("")){
										out.println("সেবাকেন্দ্রে");
									}
								%></span>
								</div>
								<div class="col-md-12" >
									<span class="bold-italic"><%
										if(singleRow2.get("delivery_place").toString().equals("1")){
											out.println("মেডিকেল কলেজ হাসপাতাল");
										}else if(singleRow2.get("delivery_place").toString().equals("2")){
											out.println("জেলা সদর হাসপাতাল / সরকারি হাসপাতাল");
										}else if(singleRow2.get("delivery_place").toString().equals("3")){
											out.println("মা ও শিশু কল্যাণ কেন্দ্র");
										}else if(singleRow2.get("delivery_place").toString().equals("4")){
											out.println("উপজেলা স্বাস্থ্য কমপ্লেক্স");
										}else if(singleRow2.get("delivery_place").toString().equals("5")){
											out.println("ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র");
										}else if(singleRow2.get("delivery_place").toString().equals("6")){
											out.println("কমিউনিটি ক্লিনিক");
										}else if(singleRow2.get("delivery_place").toString().equals("7")){
											out.println("এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র");
										}else if(singleRow2.get("delivery_place").toString().equals("8")){
											out.println("প্রাইভেট ক্লিনিক / হাসপাতাল");
										}else if(singleRow2.get("delivery_place").toString().equals("9")){
											out.println("অন্যান্য");
										}
									%></span>
								</div>
								<div class="col-md-6" >
									<b>ভর্তির তারিখ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow2.get("admission_date"));%></span>
								</div>
								<div class="col-md-6" >
									<b>ওয়ার্ড/শয্যা: &nbsp;</b>
								</div>
								<div class="col-md-8" >
									<b>প্রসবের তারিখ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow2.get("delivery_date"));%></span>
								</div>
								<div class="col-md-4" >
									<b>সময়: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow2.get("outcome_time"));%></span>
								</div>
								<div class="col-md-12" >
									<b>প্রসবের ধরণ: &nbsp;</b> <span class="bold-italic"><% out.println(singleRow2.get("delivery_type"));%></span>
								</div>
								<div class="col-md-12" >
									<b>প্রসবের ফলাফল: &nbsp;</b> জীবিত জন্ম:
									<span class="bold-italic"> <% out.println(singleRow2.get("live_birth"));%>
								</span>
								</div>
								<div class="col-md-12" >
									মৃত জন্ম: &nbsp;<% out.println(singleRow2.get("still_birth"));%>
									ফ্রেশ: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("still_birth_fresh"));%></span>  ম্যাসারেটেড: <span class="bold-italic"><% out.println(singleRow.get("still_birth_macerated"));%></span>
								</div>
								<div class="col-md-12" >
									<b>লিঙ্গ: &nbsp;</b> <span class="bold-italic"> ছেলে:<% out.println(singleRow2.get("nBoy"));%> মেয়ে:<% out.println(singleRow.get("nGirl"));%></span>
								</div>
								<div class="col-md-12" >
									<b>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনায় (AMTSL) যা</br>
										অনুসরণ করা হয়েছে -</b>
									</br>প্রসবের ১ মিনিটের মধ্যে অক্সিটোসিন প্রয়োগ:<span class="bold-italic"> <% out.println(singleRow2.get("apply_oxytocin"));%></span>
									</br>গর্ভফুল প্রসবের জন্য নাভিরজ্জুতে নিয়ন্ত্রিত টান<span class="bold-italic"> <% out.println(singleRow2.get("applyTraction"));%></span>
									</br>জরায়ু ম্যাসাজ<span class="bold-italic"> <% out.println(singleRow2.get("uterusMassage"));%></span>
								</div>
								<div class="col-md-12" >
									অক্সিটোসিন না থাকলে মিসোপ্রোস্টল খাওয়ানো হয়েছে: &nbsp;
									<span class="bold-italic"><% out.println(singleRow2.get("misoprostol"));%></span>
								</div>
								<div class="col-md-12" >
									<b>প্রসব সম্পাদনকারীর নাম,পদবী</b> </br>
									<span class="bold-italic">
									<% out.println(singleRow2.get("attendant_name"));%>,
									<% out.println(singleRow2.get("attendant_designation"));%>
								</span>
								</div>
							</td>
							<td nowrap rowspan="14">
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("excessBloodLoss"));%>> অতিরিক্ত রক্তক্ষরণ &nbsp;
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("lateDelivery"));%>> বিলম্বিত প্রসব &nbsp;&nbsp;&nbsp;
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("blockedDelivery"));%>> বাধাগ্রস্থ প্রসব &nbsp;&nbsp;&nbsp;
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("placenta"));%>> গর্ভফুল না পড়া &nbsp;&nbsp;&nbsp;
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("headache"));%>> প্রচন্ড মাথা ব্যাথা &nbsp;&nbsp;&nbsp;
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("blurryVision"));%>> চোখে ঝাপসা</br> দেখা
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("otherBodyPart"));%>> মাথা ছাড়া অন্য </br>কোন অঙ্গ বের</br> হওয়া &nbsp;&nbsp;&nbsp;
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("convulsion"));%>> খিচুঁনি &nbsp;&nbsp;&nbsp;
								<br><input type="checkbox" disabled <%out.println(singleRow2.get("others"));%>> অন্যান্য
							</td>
							<td rowspan="14">
								<b>ইপিসিওট্মি করা হয়েছে কি না?</b>
								</br><input type="checkbox" disabled <%out.println(singleRow2.get("episiotomy"));%>> হ্যাঁ
								</br><input type="checkbox" disabled <%out.println(singleRow2.get("episiotomy"));%>> না
								</br> <b>রেফার</b>(কেন্দ্রের নাম ও কারন)
							</td>

						</tr>
						<tr>
							<td class="nowrap">রক্তচাপ</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_bp"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_bp"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_bp"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_bp"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td nowrap >ওজন (কেজি)</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_weight"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_weight"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_weight"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_weight"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td >ইডিমা</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_edema"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_edema"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_edema"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_edema"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td nowrap>জরায়ুর উচ্চতা (সপ্তাহ)</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_uterus_height"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_uterus_height"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_uterus_height"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_uterus_height"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>

						</tr>
						<tr>
							<td nowrap>ফিটাসের হৃদপিণ্ডের গতি (প্রতি মিনিটে)</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_fetus_heart_rate"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_fetus_heart_rate"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_fetus_heart_rate"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_fetus_heart_rate"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td nowrap>ফিটাল প্রেজেনটেশন</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_fetal_presentation"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_fetal_presentation"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_fetal_presentation"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_fetal_presentation"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td nowrap>রক্তস্বল্পতা</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc1_anemia"));
											%>
										</span>
								</div>
							</td>
							<td>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc2_anemia"));
											%>
										</span>
								</div>
							</td>
							<td>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc3_anemia"));
											%>
										</span>
								</div>
							</td>
							<td>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc3_anemia"));
											%>
										</span>
								</div>
							</td>
							<td>&nbsp;</td>

						</tr>
						<tr>
							<td nowrap>হিমোগ্লোবিন</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc1_hemoglobin"));
											%>
										</span>
								</div>
							</td>
							<td><div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc2_hemoglobin"));
											%>
										</span>
							</div></td>
							<td><div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc3_hemoglobin"));
											%>
										</span>
							</div></td>
							<td><div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("anc4_hemoglobin"));
											%>
										</span>
							</div></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td nowrap>জন্ডিস</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_jaundice"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_jaundice"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_jaundice"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_jaundice"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td nowrap>প্রস্রাব পরিক্ষা সুগার</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_urine_sugar"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_urine_sugar"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_urine_sugar"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_urine_sugar"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>


						</tr>
						<tr>
							<td nowrap>প্রস্রাব পরিক্ষা এলবুমিন</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc1_urine_albumin"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow2.get("anc2_urine_albumin"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc3_urine_albumin"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow2.get("anc4_urine_albumin"));%></span>
								</div>
							</td>
							<td>&nbsp;</td>


						</tr>
						<tr>
							<td nowrap>বিপদ চিহ্ন / জটিলতা</td>

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>
						<tr>
							<td nowrap>পুনঃ পরিদর্শন </td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>


					</tbody>
				</table>
				<h1 align="middle">মা ও নবজাতকের সেবা রেজিস্টার</h1>
				<table  class="mnc-table-right"  border="1" cellspacing="0" style="font-size:11px;">
					<thead>
						<tr>
							<th rowspan="3" nowrap><b>নবজাতক সংক্রান্ত তথ্য</b></th>
							<th colspan="14" nowrap><b>প্রসাবোত্তর সেবা</b></th>
							<th rowspan="3" nowrap><b>নবজাতকের মৃত্যু</b></th>
							<th rowspan="3" nowrap><b>মাতৃ মৃত্যু</b></th>
							<th rowspan="3" nowrap><b>মন্তব্য</b></th>
						</tr>
						<tr>
							<th colspan="7" ><b>মা</b></th>
							<th colspan="7" ><b>নবজাতক</b></th>
						</tr>
						<tr>
							<th nowrap>বর্তমান সেবার তথ্য</th>
							<th nowrap>পরিদর্শন ১</th>
							<th nowrap>পরিদর্শন ২</th>
							<th nowrap>পরিদর্শন ৩</th>
							<th nowrap>পরিদর্শন ৪</th>
							<th >অসুবিধা ও রোগ</th>
							<th >চিকিৎসা ও পরামর্শ</th>
							<th nowrap>বর্তমান সেবার তথ্য</th>
							<th nowrap>পরিদর্শন ১</th>
							<th nowrap>পরিদর্শন ২</th>
							<th nowrap>পরিদর্শন ৩</th>
							<th nowrap>পরিদর্শন ৪</th>
							<th >অসুবিধা ও রোগ</th>
							<th >চিকিৎসা ও পরামর্শ</th>
						</tr>
						<tr>
							<th >১৫</th>
							<th >১৬</th>
							<th >১৭</th>
							<th nowrap>১৮</th>
							<th nowrap>১৯</th>
							<th nowrap>২০</th>
							<th nowrap>২১</th>
							<th nowrap>২২</th>
							<th >২৩</th>
							<th >২৪</th>
							<th nowrap>২৫</th>
							<th nowrap>২৬</th>
							<th nowrap>২৭</th>
							<th nowrap>২৮</th>
							<th nowrap>২৯</td>
							<th >৩০</td>
							<th >৩১</td>
							<th >৩২</td>
						</tr>
					</thead>
					<tbody>
						<%--1st client--%>
						<tr>
							<td nowrap rowspan="10" class="pad-bot-15">
								<div class="col-md-12" ><b>জন্ম ওজন: &nbsp;<span class="bold-italic"><%out.println(singleRow.get("birthWeight"));%>(কেজি)</span></b></div>
								<div class="col-md-12" ><b>অপরিণত জন্ম(৩৭ সপ্তাহ পূর্ণ হওয়ার আগে): &nbsp;</b><span class="bold-italic"><%out.println(singleRow.get("immatureBirth"));%></span></div>
								<div class="col-md-12" ><b>১মিনিটের মধ্যে মোছানো হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow.get("dryingAfterBirth"));%></span></div>
								<div class="col-md-12" ><b>নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো</br> হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow.get("skinTouch"));%></span></div>
								<div class="col-md-12" ><b>নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার</br> করা হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow.get("chlorehexidin"));%></span></div>
								<div class="col-md-12" ><b>জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow.get("breastFeed"));%></span></div>
								<div class="col-md-12" ><b>রিসাসসিটেশন : &nbsp;</b><span class="bold-italic"><%out.println(singleRow.get("resassitation"));%></span></div>
								<div class="col-md-12" >স্টিমুলেশন : &nbsp;<span class="bold-italic"><%out.println(singleRow.get("stimulation"));%></span></div>
								<div class="col-md-12" >ব্যাগ ও মাস্ক ব্যাবহার: &nbsp;<span class="bold-italic"><%out.println(singleRow.get("bagNMask"));%></span></div>
								<div class="col-md-12" ><b>রেফারঃ</b></br>
                                    <%
                                        if(singleRow.get("nb_referCenterName").toString().equals("1")){
                                            out.println("<b>কেন্দ্রের নাম :</b> মেডিকেল কলেজ হাসপাতাল </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("2")){
                                            out.println("<b>কেন্দ্রের নাম :</b> জেলা সদর হাসপাতাল / সরকারি হাসপাতাল </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("3")){
                                            out.println("<b>কেন্দ্রের নাম :</b> মা ও শিশু কল্যাণ কেন্দ্র </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("4")){
                                            out.println("<b>কেন্দ্রের নাম :</b> উপজেলা স্বাস্থ্য কমপ্লেক্স </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("5")){
                                            out.println("<b>কেন্দ্রের নাম :</b> ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("6")){
                                            out.println("<b>কেন্দ্রের নাম :</b> কমিউনিটি ক্লিনিক </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("7")){
                                            out.println("<b>কেন্দ্রের নাম :</b> এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("8")){
                                            out.println("<b>কেন্দ্রের নাম :</b> প্রাইভেট ক্লিনিক / হাসপাতাল </br><b>কারন :</b> বিস্তারিত");
                                        }else if(singleRow.get("nb_referCenterName").toString().equals("9")){
                                            out.println("<b>কেন্দ্রের নাম :</b> অন্যান্য </br><b>কারন :</b> বিস্তারিত");
                                        }
                                    %>
                                </div>
							</td>
							<td  nowrap>সেবাগ্রহনের তারিখ</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm1_service_date"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm2_service_date"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm3_service_date"));%></span>
								</div>
							</td>
							<td nowrap >
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm4_service_date"));%></span>
								</div>
							</td>
							<td nowrap rowspan="10" >
								<div class="col-md-12 " >
									পরিদর্শন ১:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%
										String [] pncm_sym = {"","প্রসবের পরে অত্যধিক রক্তস্রাব (৫ মিনিটে প্যাড ভিজে যাওয়া)",
												"খিচুনি হওয়া৷", "উচ্চ রক্তচাপ", "জ্বর,তল পেটে ব্যথা,দুগন্ধযুক্ত স্রাব",
												"বুকে ব্যথা ও শ্বাসকস্ট", "যোনিপথে প্রসাব ও পায়খানা আসা",
												"যোনিপথে কিছু নেমে আসা"};
										symptom_str = "";
										String [] pncm1_symptom = singleRow.get("pncm1_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm1_symptom.length;sym++){
												if(pncm1_symptom[sym].length()>0){
													symptom_str += "</br>"+pncm_sym[Integer.parseInt(pncm1_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);
									%>
										</br>রোগঃ <% out.println(singleRow.get("pncm1_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ২:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										String [] pncm2_symptom = singleRow.get("pncm2_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm2_symptom.length;sym++){
												if(pncm2_symptom[sym].length()>0){
													symptom_str +=  "</br>"+pncm_sym[Integer.parseInt(pncm2_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow.get("pncm2_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ৩:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										String [] pncm3_symptom = singleRow.get("pncm3_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm3_symptom.length;sym++){
												if(pncm3_symptom[sym].length()>0){
													symptom_str += "</br>"+ pncm_sym[Integer.parseInt(pncm3_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow.get("pncm3_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ৪:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										String [] pncm4_symptom = singleRow.get("pncm4_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm4_symptom.length;sym++){
												if(pncm4_symptom[sym].length()>0){
													symptom_str += "</br>"+ pncm_sym[Integer.parseInt(pncm4_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow.get("pncm4_disease"));%>
									</span>
								</div>
							</td>
							<td rowspan="10" nowrap>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ১:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%
										String [] pncm_treatment = {"",
												"INJ. MgSo4","CAP. AMOXYCILLIN 500 MG",
												"TAB. ALBENDAZOLE 400 MG",
												"TAB. DIAZEPAM 5 MG",
												"TAB. METRONIDAZOL 400 MG",
												"TAB. PARACETAMOL 500 MG",
												"TAB. VITAMIN B COMPLEX",
												"TAB. ANTACID (650 MG)",
												"TAB. DROTAVERIN HYDROCHLORIDE (40 MG)",
												"TAB. PANTOPRAZOLE (20 MG)",
												"TAB. CHLORPHENIRAMINE MALEATE 4 MG)",
												"TAB. SALBUTAMOL (4 MG)",
												"TAB. CALCIUM CARBONATE (500 MG)",
												"TAB. COTRIMOXAZOLE (120 MG) DISPERSIBLE",
												"TAB. COTRIMOXAZOLE (960 MG)",
												"POWDER FOR SUSPENSION AMOXYCILLIN (PAED) 15 ML",
												"COTRIMOXAZOLE SUSPENSION (120 MG/5ML) 60ML",
												"PARACETAMOL SUSPENSION (120 MG./ 5 ML.) 60 ML.",
												"OINT. BENZOIC ACID (6%) AND SALICYLIC ACID (3%) - 1 KG JAR",
												"POWDER FOR SUSPENSION AMOXYCILLIN (100ML)",
												"DISPENSING ENVELOP(14 CM. X 9 CM.) SELF-LOCKING POLYBAG",
												"CAP. DOXICYCLINE 100 MG",
												"TAB. IRON & FOLIC ACID",
												"TAB. FOLIC ACID (5mg)"};

										String [] pncm_advice = {"",
												"অন্য সময়ের চাইতে বেশী পরিমাণে (কম পক্ষে ১ মুঠ চালের ভাত ও ১ চামচ ডাল বেশী) খান",
												"রক্তস্বল্পতা প্রতিরোধ করার জন্য মাকে আয়রন সমৃদ্ধ খাবার যেমন সবুজ শাকসবজি, ডাল, ডিম খেতে হবে এবং এগুলোর সাথে আয়রন এবং ফলিক এসিড ট্যাবলেট খেতে খান",
												"যদি পা ফুলে যায় অতিরিক্ত (আলগা) লবণ খাওয়া বাদ দিতে হবে এবং পর্যাপ্ত বিশ্রাম নিন",
												"৩ মাস আয়রন এবং ১ মাস ফলিক এসিড খান",
												"যদি পা ফুলে যায় অতিরিক্ত (আলগা) লবণ খাওয়া বাদ দিতে হবে এবং পর্যাপ্ত বিশ্রাম নিন",
												"প্রসবের ২ সপ্তাহের মধ্যে ভিটামিন-এ ক্যাপসুল ২,০০,০০০ আই.ইউ",
												"প্রসব ঝরা বন্ধ করার জন্য পেরিনিয়াম ও পেটের পেশীর ব্যায়াম করুন",
												"প্রসব ও পায়খানার পর পেরিনিয়াম ভালভাবে পরিষ্কার করুন",
												"পেরিনিয়ামের ক্ষত শুকনা ও পরিষ্কার রাখুন",
												"পেরিনিয়ামের প্যাড/কাপড় দিনে কমপক্ষে চার বার পরিবর্তন করুন ৷ যদি প্রসবোত্তর স্রাব বেশী হয়, তখন বেশী বার পরিবর্তন করুন ৷ যদি কাপড় ব্যবহার করে তা ভালমতো ধুয়ে অথবা গরম পানিতে ধুয়ে রোদ্রে শুকিয়ে ব্যবহার করুন",
												"প্রতিদিন গোসল করুন",
												"প্রসবের পর ৬ সপ্তাহ পর্যন্ত স্বামী সহবাস করবেন না"
										};
										treatment_str = "";
										advice_str = "";
										String [] pncm1_treatment = singleRow.get("pncm1_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm1_treatment.length;sym++){
												if(pncm1_treatment[sym].length()>0){
													treatment_str += "</br>"+ pncm_treatment[Integer.parseInt(pncm1_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());
									%>
										</br>পরামর্শঃ <%
										advice_str = "দেওয়া হয়েছে";
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ২:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
										String [] pncm2_treatment = singleRow.get("pncm2_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm2_treatment.length;sym++){
												if(pncm2_treatment[sym].length()>0){
													treatment_str += "</br>"+ pncm_treatment[Integer.parseInt(pncm2_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ৩:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
										String [] pncm3_treatment = singleRow.get("pncm3_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm3_treatment.length;sym++){
												if(pncm3_treatment[sym].length()>0){
													treatment_str +=  "</br>"+ pncm_treatment[Integer.parseInt(pncm3_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ৪:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%
										treatment_str = "";
										String [] pncm4_treatment = singleRow.get("pncm4_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncm4_treatment.length;sym++){
												if(pncm4_treatment[sym].length()>0){
													treatment_str += "</br>"+ pncm_treatment[Integer.parseInt(pncm4_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str.toLowerCase());%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
									<br>রেফার(     )
									<br>কেন্দ্রের নাম ও কারনঃ
									<br>রেফার(     )
									<br>কেন্দ্রের নাম ও কারনঃ
								</div>
							</td>
							<td nowrap>সেবাগ্রহনের তারিখ</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc1_service_date"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc2_service_date"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc3_service_date"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc4_service_date"));%></span>
								</div>
							</td>
							<td rowspan="10" nowrap >
								<div class="col-md-12 nowrap" >
									পরিদর্শন ১:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%
										String [] pncc_sym = {"","খিচুনি হওয়া৷",
												"জন্মের ২৪ ঘন্টার মধ্যে ও দ্বিতীয় সপ্তাহের পরে জন্ডিস হওয়া এবং যে কোন সময়ে হাতের তালু,পায়ের পাতায় জন্ডিস আসা",
												"দীঘ সময় ধরে শুয়ে থাকা বা জেগে না উঠা", "শ্বাস-প্রশ্বাসে (শ্বাস মিনিটে ৩০ এর কম বা ৬০ এর বেশী)",
												"জ্বর", "পুজ নি:সরণ সহ চোখ লাল হওয়া ও ফুলে যাওয়া",
												"নাভীরজ্জর চারিদিকে লাল হয়ে যাওয়া ও নি:সরণ হওয়া",
												"মায়ের দুধ কম টানা বা মোটেই না চোষা","নেতিয়ে পরা বা কম নড়াচড়া","ত্বকে ১০ টির বেশি পুঁজবটি","পেট ফোলা"};
										symptom_str = "";
										String [] pncc1_symptom = singleRow.get("pncc1_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc1_symptom.length;sym++){
												if(pncc1_symptom[sym].length()>0){
													symptom_str += "</br>"+pncc_sym[Integer.parseInt(pncc1_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);
									%>
										</br>রোগঃ <% out.println(singleRow.get("pncc1_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ২:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										String [] pncc2_symptom = singleRow.get("pncc2_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc2_symptom.length;sym++){
												if(pncc2_symptom[sym].length()>0){
													symptom_str +=  "</br>"+pncc_sym[Integer.parseInt(pncc2_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow.get("pncc2_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ৩:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										String [] pncc3_symptom = singleRow.get("pncc3_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc3_symptom.length;sym++){
												if(pncc3_symptom[sym].length()>0){
													symptom_str += "</br>"+ pncc_sym[Integer.parseInt(pncc3_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow.get("pncc3_disease"));%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									পরিদর্শন ৪:
									<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
										String [] pncc4_symptom = singleRow.get("pncc4_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc4_symptom.length;sym++){
												if(pncc4_symptom[sym].length()>0){
													symptom_str += "</br>"+ pncc_sym[Integer.parseInt(pncc4_symptom[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(symptom_str);%>
										</br>রোগঃ <% out.println(singleRow.get("pncc4_disease"));%>
									</span>
								</div>
							</td>
							<td rowspan="10" nowrap>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ১:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%
										String [] pncc_treatment = {"",
												"INJ. MgSo4","CAP. AMOXYCILLIN 500 MG",
												"TAB. ALBENDAZOLE 400 MG",
												"TAB. DIAZEPAM 5 MG",
												"TAB. METRONIDAZOL 400 MG",
												"TAB. PARACETAMOL 500 MG",
												"TAB. VITAMIN B COMPLEX",
												"TAB. ANTACID (650 MG)",
												"TAB. DROTAVERIN HYDROCHLORIDE (40 MG)",
												"TAB. PANTOPRAZOLE (20 MG)",
												"TAB. CHLORPHENIRAMINE MALEATE 4 MG)",
												"TAB. SALBUTAMOL (4 MG)",
												"TAB. CALCIUM CARBONATE (500 MG)",
												"TAB. COTRIMOXAZOLE (120 MG) DISPERSIBLE",
												"TAB. COTRIMOXAZOLE (960 MG)",
												"POWDER FOR SUSPENSION AMOXYCILLIN (PAED) 15 ML",
												"COTRIMOXAZOLE SUSPENSION (120 MG/5ML) 60ML",
												"PARACETAMOL SUSPENSION (120 MG./ 5 ML.) 60 ML.",
												"OINT. BENZOIC ACID (6%) AND SALICYLIC ACID (3%) - 1 KG JAR",
												"POWDER FOR SUSPENSION AMOXYCILLIN (100ML)",
												"DISPENSING ENVELOP(14 CM. X 9 CM.) SELF-LOCKING POLYBAG",
												"CAP. DOXICYCLINE 100 MG",
												"TAB. IRON & FOLIC ACID",
												"TAB. FOLIC ACID (5mg)"};

										String [] pncc_advice = {"",
												"নবজাত শিশুকে কখনই অনাবৃত রাখবেন না৷",
												"শিশু জন্মের সাথে সাথে বা ৭২ ঘণ্টার মধ্যে গোসল করাবেন না৷",
												"প্রথম ২৪ ঘনটায় শিশুর শরীর কোন প্রকার তৈল মেখে পরিষ্কার করবেন না৷",
												"কখনই ভারনিক্স (Vernix)ওঠাবেন না৷",
												"জন্মের পর পরই শাল দুধ খাওয়ান",
												"জন্মের ১ ঘণ্টার মধ্যে বুকের দুধ খাননো শুরু করুন",
												"শিশুর চাহিদা অনুযায়ী শুধুমাত্র বুকের দুধ খাওয়ান",
												"নবজাতককে শুকনো ও উষ্ণ রাখুন",
												"কাঁটা নাভিতে ৭.১% CHX ব্যবহার করুন ও পরবর্তীতে শুকনো রাখুন",
												"নবজাতককে ধরার পূর্বে হাত পরিষ্কার করুন",
												"জন্মের পর পর যত তাড়াতাড়ি সম্ভব বিসিজি এবং পোলিও টিকা দিন",
												"ইপিআই-এর আওতায় টিকার সময়সূচী অনুযায়ী শিশুকে রোগ প্রতিরোধের উদ্দেশ্যে টিকা দিন",
												"নবজাতককে বুকের দুধ খানবার পূর্বে পরিষ্কার করে নিন"
										};
										treatment_str = "";
										advice_str = "";
										String [] pncc1_treatment = singleRow.get("pncc1_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc1_treatment.length;sym++){
												if(pncc1_treatment[sym].length()>0){
													treatment_str += "</br>"+ pncc_treatment[Integer.parseInt(pncc1_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str);
									%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12" nowrap >
									<b>পরিদর্শন ২:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
										String [] pncc2_treatment = singleRow.get("pncc2_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc2_treatment.length;sym++){
												if(pncc2_treatment[sym].length()>0){
													treatment_str += "</br>"+ pncc_treatment[Integer.parseInt(pncc2_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str);%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ৩:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
										String [] pncc3_treatment = singleRow.get("pncc3_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc3_treatment.length;sym++){
												if(pncc3_treatment[sym].length()>0){
													treatment_str +=  "</br>"+ pncc_treatment[Integer.parseInt(pncc3_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str);%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
								</div>
								<div class="col-md-12 nowrap" >
									<b>পরিদর্শন ৪:</b>
									<span class="bold-italic">
										</br>চিকিৎসাঃ <%
										treatment_str = "";
										String [] pncc4_treatment = singleRow.get("pncc4_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
										try{
											for(int sym=0;sym<pncc4_treatment.length;sym++){
												if(pncc4_treatment[sym].length()>0){
													treatment_str += "</br>"+ pncc_treatment[Integer.parseInt(pncc4_treatment[sym])];
												}
											}
										}catch (Exception e){
										}
										out.println(treatment_str);%>
										</br>পরামর্শঃ <%
										out.println(advice_str);%>
									</span>
									<br>রেফার(     )
									<br>কেন্দ্রের নাম ও কারনঃ
									<br>রেফার(     )
									<br>কেন্দ্রের নাম ও কারনঃ
								</div>
							</td>
							<td rowspan="10" nowrap>
								<%
									String dt = "";
									String coz="";
									String place="";
									if(!singleRow.get("deathDT_child").toString().equals("")) {
										dt = singleRow.get("deathDT_child").toString();
										coz = singleRow.get("causeOfDeath_child").toString();
										place = singleRow.get("placeOfDeath_child").toString();
									}
								%>
								<div class="col-md-12" >
									তারিখঃ </br><span class="bold-italic"><% out.println(dt);%></span>
								</div>
								<div class="col-md-12" >
									স্থান: </br><span class="bold-italic"><% out.println(coz);%></span>
								</div>
								<div class="col-md-12" >
									কারন: </br><span class="bold-italic"><% out.println(place);%></span>
								</div>
							</td>
							<td rowspan="10" nowrap>
								<%
									dt = "";
									coz="";
									place="";
									if(singleRow.get("deathOfPregWomen").toString().equals("1")){
										dt = singleRow.get("deathDT").toString();
										coz = singleRow.get("causeOfDeath").toString();
										place = singleRow.get("placeOfDeath").toString();
									}
								%>
								<div class="col-md-12" >
									তারিখঃ </br><span class="bold-italic"><% out.println(dt);%></span>
								</div>
								<div class="col-md-12" >
									স্থান:</br> <span class="bold-italic"><% out.println(coz);%></span>
								</div>
								<div class="col-md-12" >
									কারন:</br> <span class="bold-italic"><% out.println(place);%></span>
								</div>
							</td>
							<td rowspan="10" class="nowrap">

							</td>
						</tr>
						<tr>
							<td>তাপমাত্রা</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm1_temp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm2_temp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm3_temp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm4_temp"));%></span>
								</div>
							</td>
							<td>তাপমাত্রা</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc1_temp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc2_temp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc3_temp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc4_temp"));%></span>
								</div>
							</td>
						</tr>
						<tr>
							<td>রক্তচাপ </td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm1_bp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm2_bp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm3_bp"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm4_bp"));%></span>
								</div>
							</td>
							<td nowrap>ওজন (কেজি)</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc1_weight"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc2_weight"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc3_weight"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc4_weight"));%></span>
								</div>
							</td>
						</tr>
						<tr>
							<td>রক্তস্বল্পতা</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow.get("pncm1_anemia"));%>
										</span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow.get("pncm2_anemia"));%>
										</span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow.get("pncm3_anemia"));%>
										</span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow.get("pncm4_anemia"));%>
										</span>
								</div>
							</td>
							<td nowrap>শ্বাস (মিনিটে কতবার)</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc1_breathing_per_minute"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc2_breathing_per_minute"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc3_breathing_per_minute"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc4_breathing_per_minute"));%></span>
								</div>
							</td>
						</tr>
						<tr>
							<td>হিমোগ্লোবিন</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("pncm1_hemoglobin"));
											%>
										</span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("pncm2_hemoglobin"));
											%>
										</span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("pncm3_hemoglobin"));
											%>
										</span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow.get("pncm4_hemoglobin"));
											%>
										</span>
								</div>
							</td>
							<td rowspan="5" nowrap>
								</br> <b> বিপদ  চিহ্নঃ (যদি</br> থাকে ডানে নম্বর উল্লেখ</br> করুন)</b>
								</br> ১. বুকের দুধ টানতে না</br> পারা নেতিয়ে পড়া
								</br> ২. খিছুনি ৩. জ্বর বা</br> শরীর ঠাণ্ডা হওয়া
								</br> ৪. দ্রুত শ্বাস নেয়া বা </br>বুকের খাঁচা দেবে যাওয়া
								</br> ৫. নাভি পাকা
							</td>
							<td nowrap rowspan="5">
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc1_danger_sign"));%></span>
								</div>
							</td>
							<td nowrap rowspan="5">
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc2_danger_sign"));%></span>
								</div>
							</td>
							<td nowrap rowspan="5">
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc3_danger_sign"));%></span>
								</div>
							</td>
							<td nowrap rowspan="5">
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc4_danger_sign"));%></span>
								</div>
							</td>

						</tr>
						<tr>
							<td>স্তনের অবস্থা</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm1_breast_condition"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm2_breast_condition"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm3_breast_condition"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm4_breast_condition"));%></span>
								</div>
							</td>

						</tr>
						<tr>
							<td>জরায়ুর ইনভলিউশন</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm1_uterus_involution"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm2_uterus_involution"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm3_uterus_involution"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm4_uterus_involution"));%></span>
								</div>
							</td>

						</tr>
						<tr>
							<td>স্রাব/ রক্তস্রাব</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm1_hematuria"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm2_hematuria"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm3_hematuria"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm4_hematuria"));%></span>
								</div>
							</td>
						</tr>
						<tr>
							<td>পেরিনিয়াম</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm1_perineum"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncm2_perineum"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm3_perineum"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncm4_perineum"));%></span>
								</div>
							</td>
						</tr>
						<tr>
							<td >প: প: পদ্ধতি(পরামর্শ / পদ্ধতি প্রদান)</td>
							<td nowrap >

							</td>
							<td nowrap >

							</td>
							<td nowrap >

							</td>
							<td nowrap >

							</td>
							<td nowrap>শুধুমাত্র বুকের দুধ </br> খাওয়ানো</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc1_breastFeedingOnly"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"> <% out.println(singleRow.get("pncc2_breastFeedingOnly"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc3_breastFeedingOnly"));%></span>
								</div>
							</td>
							<td nowrap>
								<div class="col-md-12" >
									<span class="bold-italic"><% out.println(singleRow.get("pncc4_breastFeedingOnly"));%></span>
								</div>
							</td>
						</tr>
						<%--2nd client--%>
							<tr>
								<td nowrap rowspan="10" class="pad-bot-15">
									<div class="col-md-12" ><b>জন্ম ওজন: &nbsp;<span class="bold-italic"><%out.println(singleRow2.get("birthWeight"));%>(কেজি)</span></b></div>
									<div class="col-md-12" ><b>অপরিণত জন্ম(৩৭ সপ্তাহ পূর্ণ হওয়ার আগে): &nbsp;</b><span class="bold-italic"><%out.println(singleRow2.get("immatureBirth"));%></span></div>
									<div class="col-md-12" ><b>১মিনিটের মধ্যে মোছানো হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow2.get("dryingAfterBirth"));%></span></div>
									<div class="col-md-12" ><b>নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো</br> হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow2.get("skinTouch"));%></span></div>
									<div class="col-md-12" ><b>নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার</br> করা হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow2.get("chlorehexidin"));%></span></div>
									<div class="col-md-12" ><b>জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে: &nbsp;</b><span class="bold-italic"><%out.println(singleRow2.get("breastFeed"));%></span></div>
									<div class="col-md-12" ><b>রিসাসসিটেশন : &nbsp;</b><span class="bold-italic"><%out.println(singleRow2.get("resassitation"));%></span></div>
									<div class="col-md-12" >স্টিমুলেশন : &nbsp;<span class="bold-italic"><%out.println(singleRow2.get("stimulation"));%></span></div>
									<div class="col-md-12" >ব্যাগ ও মাস্ক ব্যাবহার: &nbsp;<span class="bold-italic"><%out.println(singleRow2.get("bagNMask"));%></span></div>
                                    <div class="col-md-12" ><b>রেফারঃ</b></br>
                                        <%
                                            if(singleRow2.get("nb_referCenterName").toString().equals("1")){
                                                out.println("<b>কেন্দ্রের নাম :</b> মেডিকেল কলেজ হাসপাতাল </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("2")){
                                                out.println("<b>কেন্দ্রের নাম :</b> জেলা সদর হাসপাতাল / সরকারি হাসপাতাল </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("3")){
                                                out.println("<b>কেন্দ্রের নাম :</b> মা ও শিশু কল্যাণ কেন্দ্র </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("4")){
                                                out.println("<b>কেন্দ্রের নাম :</b> উপজেলা স্বাস্থ্য কমপ্লেক্স </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("5")){
                                                out.println("<b>কেন্দ্রের নাম :</b> ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("6")){
                                                out.println("<b>কেন্দ্রের নাম :</b> কমিউনিটি ক্লিনিক </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("7")){
                                                out.println("<b>কেন্দ্রের নাম :</b> এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("8")){
                                                out.println("<b>কেন্দ্রের নাম :</b> প্রাইভেট ক্লিনিক / হাসপাতাল </br><b>কারন :</b> বিস্তারিত");
                                            }else if(singleRow2.get("nb_referCenterName").toString().equals("9")){
                                                out.println("<b>কেন্দ্রের নাম :</b> অন্যান্য </br><b>কারন :</b> বিস্তারিত");
                                            }
                                        %>
                                    </div>
								</td>
								<td  nowrap>সেবাগ্রহনের তারিখ</td>
								<td nowrap >
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm1_service_date"));%></span>
									</div>
								</td>
								<td nowrap >
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm2_service_date"));%></span>
									</div>
								</td>
								<td nowrap >
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm3_service_date"));%></span>
									</div>
								</td>
								<td nowrap >
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm4_service_date"));%></span>
									</div>
								</td>
								<td nowrap rowspan="10" >
									<div class="col-md-12 " >
										পরিদর্শন ১:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%

											symptom_str = "";
											pncm1_symptom = singleRow2.get("pncm1_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm1_symptom.length;sym++){
													if(pncm1_symptom[sym].length()>0){
														symptom_str += "</br>"+pncm_sym[Integer.parseInt(pncm1_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);
										%>
											</br>রোগঃ <% out.println(singleRow2.get("pncm1_disease"));%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										পরিদর্শন ২:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
											pncm2_symptom = singleRow2.get("pncm2_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm2_symptom.length;sym++){
													if(pncm2_symptom[sym].length()>0){
														symptom_str +=  "</br>"+pncm_sym[Integer.parseInt(pncm2_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);%>
											</br>রোগঃ <% out.println(singleRow2.get("pncm2_disease"));%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										পরিদর্শন ৩:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
											pncm3_symptom = singleRow2.get("pncm3_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm3_symptom.length;sym++){
													if(pncm3_symptom[sym].length()>0){
														symptom_str += "</br>"+ pncm_sym[Integer.parseInt(pncm3_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);%>
											</br>রোগঃ <% out.println(singleRow2.get("pncm3_disease"));%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										পরিদর্শন ৪:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
											pncm4_symptom = singleRow2.get("pncm4_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm4_symptom.length;sym++){
													if(pncm4_symptom[sym].length()>0){
														symptom_str += "</br>"+ pncm_sym[Integer.parseInt(pncm4_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);%>
											</br>রোগঃ <% out.println(singleRow2.get("pncm4_disease"));%>
									</span>
									</div>
								</td>
								<td rowspan="10" nowrap>
									<div class="col-md-12 nowrap" >
										<b>পরিদর্শন ১:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%

											treatment_str = "";
											advice_str = "";
											pncm1_treatment = singleRow2.get("pncm1_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm1_treatment.length;sym++){
													if(pncm1_treatment[sym].length()>0){
														treatment_str += "</br>"+ pncm_treatment[Integer.parseInt(pncm1_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str.toLowerCase());
										%>
											</br>পরামর্শঃ <%
											advice_str = "দেওয়া হয়েছে";
											out.println(advice_str);%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										<b>পরিদর্শন ২:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
											pncm2_treatment = singleRow2.get("pncm2_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm2_treatment.length;sym++){
													if(pncm2_treatment[sym].length()>0){
														treatment_str += "</br>"+ pncm_treatment[Integer.parseInt(pncm2_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str.toLowerCase());%>
											</br>পরামর্শঃ <%
											out.println(advice_str);%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										<b>পরিদর্শন ৩:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
											pncm3_treatment = singleRow2.get("pncm3_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm3_treatment.length;sym++){
													if(pncm3_treatment[sym].length()>0){
														treatment_str +=  "</br>"+ pncm_treatment[Integer.parseInt(pncm3_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str.toLowerCase());%>
											</br>পরামর্শঃ <%
											out.println(advice_str);%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										<b>পরিদর্শন ৪:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%
											treatment_str = "";
											pncm4_treatment = singleRow2.get("pncm4_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncm4_treatment.length;sym++){
													if(pncm4_treatment[sym].length()>0){
														treatment_str += "</br>"+ pncm_treatment[Integer.parseInt(pncm4_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str.toLowerCase());%>
											</br>পরামর্শঃ <%
											out.println(advice_str);%>
									</span>
										<br>রেফার(     )
										<br>কেন্দ্রের নাম ও কারনঃ
										<br>রেফার(     )
										<br>কেন্দ্রের নাম ও কারনঃ
									</div>
								</td>
								<td nowrap>সেবাগ্রহনের তারিখ</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc1_service_date"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc2_service_date"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc3_service_date"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc4_service_date"));%></span>
									</div>
								</td>
								<td rowspan="10" nowrap >
									<div class="col-md-12 nowrap" >
										পরিদর্শন ১:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%
											symptom_str = "";
											pncc1_symptom = singleRow2.get("pncc1_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc1_symptom.length;sym++){
													if(pncc1_symptom[sym].length()>0){
														symptom_str += "</br>"+pncc_sym[Integer.parseInt(pncc1_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);
										%>
											</br>রোগঃ <% out.println(singleRow2.get("pncc1_disease"));%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										পরিদর্শন ২:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
											pncc2_symptom = singleRow2.get("pncc2_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc2_symptom.length;sym++){
													if(pncc2_symptom[sym].length()>0){
														symptom_str +=  "</br>"+pncc_sym[Integer.parseInt(pncc2_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);%>
											</br>রোগঃ <% out.println(singleRow2.get("pncc2_disease"));%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										পরিদর্শন ৩:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
											pncc3_symptom = singleRow2.get("pncc3_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc3_symptom.length;sym++){
													if(pncc3_symptom[sym].length()>0){
														symptom_str += "</br>"+ pncc_sym[Integer.parseInt(pncc3_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);%>
											</br>রোগঃ <% out.println(singleRow2.get("pncc3_disease"));%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										পরিদর্শন ৪:
										<span class="bold-italic">
										</br>অসুবিধাঃ <%  symptom_str = "";
											pncc4_symptom = singleRow2.get("pncc4_symptom").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc4_symptom.length;sym++){
													if(pncc4_symptom[sym].length()>0){
														symptom_str += "</br>"+ pncc_sym[Integer.parseInt(pncc4_symptom[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(symptom_str);%>
											</br>রোগঃ <% out.println(singleRow2.get("pncc4_disease"));%>
									</span>
									</div>
								</td>
								<td rowspan="10" nowrap>
									<div class="col-md-12 nowrap" >
										<b>পরিদর্শন ১:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%

											treatment_str = "";
											advice_str = "";
											pncc1_treatment = singleRow2.get("pncc1_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc1_treatment.length;sym++){
													if(pncc1_treatment[sym].length()>0){
														treatment_str += "</br>"+ pncc_treatment[Integer.parseInt(pncc1_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str);
										%>
											</br>পরামর্শঃ <%

											out.println(advice_str);%>
									</span>
									</div>
									<div class="col-md-12" nowrap >
										<b>পরিদর্শন ২:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
											pncc2_treatment = singleRow2.get("pncc2_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc2_treatment.length;sym++){
													if(pncc2_treatment[sym].length()>0){
														treatment_str += "</br>"+ pncc_treatment[Integer.parseInt(pncc2_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str);%>
											</br>পরামর্শঃ <%
											out.println(advice_str);%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										<b>পরিদর্শন ৩:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%  treatment_str = "";
											pncc3_treatment = singleRow2.get("pncc3_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc3_treatment.length;sym++){
													if(pncc3_treatment[sym].length()>0){
														treatment_str +=  "</br>"+ pncc_treatment[Integer.parseInt(pncc3_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str);%>
											</br>পরামর্শঃ <%

											out.println(advice_str);%>
									</span>
									</div>
									<div class="col-md-12 nowrap" >
										<b>পরিদর্শন ৪:</b>
										<span class="bold-italic">
										</br>চিকিৎসাঃ <%
											treatment_str = "";
											pncc4_treatment = singleRow2.get("pncc4_treatment").toString().replace('[',' ').replace(']',' ').replace('"',' ').replaceAll("\\s+","").split(",");
											try{
												for(int sym=0;sym<pncc4_treatment.length;sym++){
													if(pncc4_treatment[sym].length()>0){
														treatment_str += "</br>"+ pncc_treatment[Integer.parseInt(pncc4_treatment[sym])];
													}
												}
											}catch (Exception e){
											}
											out.println(treatment_str);%>
											</br>পরামর্শঃ <%
											out.println(advice_str);%>
									</span>
										<br>রেফার(     )
										<br>কেন্দ্রের নাম ও কারনঃ
										<br>রেফার(     )
										<br>কেন্দ্রের নাম ও কারনঃ
									</div>
								</td>
								<td rowspan="10" nowrap>
									<%
										dt = "";
										coz="";
										place="";
										if(!singleRow2.get("deathDT_child").toString().equals("")) {
											dt = singleRow2.get("deathDT_child").toString();
											coz = singleRow2.get("causeOfDeath_child").toString();
											place = singleRow2.get("placeOfDeath_child").toString();
										}
									%>
									<div class="col-md-12" >
										তারিখঃ </br><span class="bold-italic"><% out.println(dt);%></span>
									</div>
									<div class="col-md-12" >
										স্থান: </br><span class="bold-italic"><% out.println(coz);%></span>
									</div>
									<div class="col-md-12" >
										কারন: </br><span class="bold-italic"><% out.println(place);%></span>
									</div>
								</td>
								<td rowspan="10" nowrap>
									<%
										dt = "";
										coz="";
										place="";
										if(singleRow2.get("deathOfPregWomen").toString().equals("1")){
											dt = singleRow2.get("deathDT").toString();
											coz = singleRow2.get("causeOfDeath").toString();
											place = singleRow2.get("placeOfDeath").toString();
										}
									%>
									<div class="col-md-12" >
										তারিখঃ </br><span class="bold-italic"><% out.println(dt);%></span>
									</div>
									<div class="col-md-12" >
										স্থান:</br> <span class="bold-italic"><% out.println(coz);%></span>
									</div>
									<div class="col-md-12" >
										কারন:</br> <span class="bold-italic"><% out.println(place);%></span>
									</div>
								</td>
								<td rowspan="10" class="nowrap">

								</td>
							</tr>
							<tr>
								<td>তাপমাত্রা</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm1_temp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm2_temp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm3_temp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm4_temp"));%></span>
									</div>
								</td>
								<td>তাপমাত্রা</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc1_temp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc2_temp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc3_temp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc4_temp"));%></span>
									</div>
								</td>
							</tr>
							<tr>
								<td>রক্তচাপ </td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm1_bp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm2_bp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm3_bp"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm4_bp"));%></span>
									</div>
								</td>
								<td nowrap>ওজন (কেজি)</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc1_weight"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc2_weight"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc3_weight"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc4_weight"));%></span>
									</div>
								</td>
							</tr>
							<tr>
								<td>রক্তস্বল্পতা</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow2.get("pncm1_anemia"));%>
										</span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow2.get("pncm2_anemia"));%>
										</span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow2.get("pncm3_anemia"));%>
										</span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%out.println(singleRow2.get("pncm4_anemia"));%>
										</span>
									</div>
								</td>
								<td nowrap>শ্বাস (মিনিটে কতবার)</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc1_breathing_per_minute"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc2_breathing_per_minute"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc3_breathing_per_minute"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc4_breathing_per_minute"));%></span>
									</div>
								</td>
							</tr>
							<tr>
								<td>হিমোগ্লোবিন</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("pncm1_hemoglobin"));
											%>
										</span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("pncm2_hemoglobin"));
											%>
										</span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("pncm3_hemoglobin"));
											%>
										</span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic">
											<%
												out.println(singleRow2.get("pncm4_hemoglobin"));
											%>
										</span>
									</div>
								</td>
								<td rowspan="5" nowrap>
									</br> <b> বিপদ  চিহ্নঃ (যদি</br> থাকে ডানে নম্বর উল্লেখ</br> করুন)</b>
									</br> ১. বুকের দুধ টানতে না</br> পারা নেতিয়ে পড়া
									</br> ২. খিছুনি ৩. জ্বর বা</br> শরীর ঠাণ্ডা হওয়া
									</br> ৪. দ্রুত শ্বাস নেয়া বা </br>বুকের খাঁচা দেবে যাওয়া
									</br> ৫. নাভি পাকা
								</td>
								<td nowrap rowspan="5">
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc1_danger_sign"));%></span>
									</div>
								</td>
								<td nowrap rowspan="5">
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc2_danger_sign"));%></span>
									</div>
								</td>
								<td nowrap rowspan="5">
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc3_danger_sign"));%></span>
									</div>
								</td>
								<td nowrap rowspan="5">
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc4_danger_sign"));%></span>
									</div>
								</td>

							</tr>
							<tr>
								<td>স্তনের অবস্থা</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm1_breast_condition"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm2_breast_condition"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm3_breast_condition"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm4_breast_condition"));%></span>
									</div>
								</td>

							</tr>
							<tr>
								<td>জরায়ুর ইনভলিউশন</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm1_uterus_involution"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm2_uterus_involution"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm3_uterus_involution"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm4_uterus_involution"));%></span>
									</div>
								</td>

							</tr>
							<tr>
								<td>স্রাব/ রক্তস্রাব</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm1_hematuria"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm2_hematuria"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm3_hematuria"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm4_hematuria"));%></span>
									</div>
								</td>
							</tr>
							<tr>
								<td>পেরিনিয়াম</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm1_perineum"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncm2_perineum"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm3_perineum"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncm4_perineum"));%></span>
									</div>
								</td>
							</tr>
							<tr>
								<td >প: প: পদ্ধতি(পরামর্শ / পদ্ধতি প্রদান)</td>
								<td nowrap >

								</td>
								<td nowrap >

								</td>
								<td nowrap >

								</td>
								<td nowrap >

								</td>
								<td nowrap>শুধুমাত্র বুকের দুধ </br> খাওয়ানো</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc1_breastFeedingOnly"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"> <% out.println(singleRow2.get("pncc2_breastFeedingOnly"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc3_breastFeedingOnly"));%></span>
									</div>
								</td>
								<td nowrap>
									<div class="col-md-12" >
										<span class="bold-italic"><% out.println(singleRow2.get("pncc4_breastFeedingOnly"));%></span>
									</div>
								</td>
							</tr>
					</tbody>
				</table>
				<%}%>

			</div>
		</div>
	</div>

<%--</div>--%>



<script type="text/javascript">


	function PrintElem(elem) {
//        $('button').hide();
		Popup($(elem).html());
	}

	function doExport(selector, params) {
		var options = {
			//ignoreRow: [1,11,12,-2],
			//ignoreColumn: [0,-1],
			//pdfmake: {enabled: true},
			tableName: 'MNC Register',
			worksheetName: 'MNC Register'
		};

		$.extend(true, options, params);

		$('#printDiv').tableExport(options);
	}


	function Popup(data) {
		var mywindow = window.open('', 'my div');
		mywindow.document.write('<html><head><title></title>');
		/*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
		mywindow.document.write('</head><body >');
		mywindow.document.write(data);
		mywindow.document.write('</body></html>');
		mywindow.print();
		mywindow.close();

		return true;
	}
	$(document).on("click", '.button_next' ,function (){
		$('#mnc-table-left').hide();
		$('#mnc-table-right').show();
		var button_val = $(".button_next").val();
		if(button_val == 1){
			$(".button_next").val(2);
			$(".button_pre").val(1);
			$(".button_next").attr("disabled","disabled");
			$(".button_pre").removeAttr("disabled");
			$(".showing_page").text("2 of 2  ");
		}
	});

	$(document).on("click", '.button_pre' ,function (){
		$('#mnc-table-left').show();
		$('#mnc-table-right').hide();
		var button_val = $(".button_pre").val();
		if(button_val>0)
		{
			$(".button_pre").val(0);
			$(".button_next").val(1);
			$(".button_next").removeAttr("disabled");
			$(".button_pre").attr("disabled","disabled");
			$(".showing_page").text("1 of 2  ");
		}
	});



</script>


