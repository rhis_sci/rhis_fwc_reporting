<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.sci.rhis.util.EnglishtoBangla" %>
<%@ page import="org.sci.rhis.db.DBSchemaInfo" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8" %>
<script src="js/lib/ddtf.js"></script>
<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript">

    function PrintElem(elem) {
//        $('button').hide();
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div');
        mywindow.document.write('<html><head><title></title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();

        return true;
    }
    $(document).on("click", '.button_next' ,function (){
        $('#mnc-table-left').hide();
        $('#mnc-table-right').show();
        var button_val = $(".button_next").val();
        if(button_val == 1){
            $(".button_next").val(2);
            $(".button_pre").val(1);
            $(".button_next").attr("disabled","disabled");
            $(".button_pre").removeAttr("disabled");
            $(".showing_page").text("2 of 2  ");
        }
    });

    $(document).on("click", '.button_pre' ,function (){
        $('#mnc-table-left').show();
        $('#mnc-table-right').hide();
        var button_val = $(".button_pre").val();
        if(button_val>0)
        {
            $(".button_pre").val(0);
            $(".button_next").val(1);
            $(".button_next").removeAttr("disabled");
            $(".button_pre").attr("disabled","disabled");
            $(".showing_page").text("1 of 2  ");
        }
    });
</script>

<style>
	.mnc-table-left th,.mnc-table-right th{
		border: 1px solid black!important;
		text-align: center!important;
		vertical-align: middle!important;
		padding: 2px;
	}
	.mnc-table-left td,.mnc-table-right td{
		vertical-align: top!important;
		padding-left: 2px;
	}
	.nowrap{
		white-space: nowrap!important;
	}
	.inner_table td{
		border: 1px solid black!important;
		text-align: left!important;
		vertical-align: middle!important;
		padding-left: 2px;
	}
	.pad-bot-15 div{
		padding-bottom: 10px;
	}
	.pad-bot-5 div{
		padding-bottom: 5px;
	}
	.bold-italic{
		font-weight: bold;
		font-style: italic;
		padding-left: 4px;
		font-size: 110%;
	}
</style>
<%

%>
<div class="tile-body nopadding" id="reportTable" style="overflow: hidden;">
	<div class="row">
		<div class="col-md-12" style="margin-top: 2%;">
			<div class="dropdown" style="float: right; margin-right: 2%;">
				<button class="btn btn-primary" type="button" id="btnPrint" onclick="PrintElem('#printDiv')"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
					<span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding-top:20px;">
			<div id="printDiv">
				<style>
					/*#mnc-table-left th,#mnc-table-right th{*/
					/*border: 1px solid black!important;*/
					/*text-align: center!important;*/
					/*vertical-align: middle!important;*/
					/*padding: 2px;*/
					/*}*/
					.mnc-table-left td,.mnc-table-right td{
						vertical-align: top!important;
					}
					.mnc-table-left{
						min-height: 8.5in;
					}
					.pad-bot-15 div{
						padding-bottom: 10px;
					}
					table { /* Or specify a table class */
						max-height: 100%!important;
						overflow: hidden!important;
						page-break-after: always!important;
					}
					/*.pad-bot-5 div{*/
					/*padding-bottom: 5px;*/
					/*}*/
					/*.bold-italic{*/
					/*font-weight: bold;*/
					/*font-style: italic;*/
					/*padding-left: 4px;*/
					/*font-size: 110%;*/
					/*}*/
				</style>
				<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'>
					<tr>

					</tr>
				</table>
				</br>
				<%
					String data = request.getAttribute("Result").toString();
					String advice_str = "দেওয়া হয়েছে";
					JSONArray json = new JSONArray(data);
					String [] drug = {"","Cap.Amoxicillin250mg","Syp.Amoxicillin","AmoxicillinPaediatricDrop","Cap.Cloxacillin500mg","Tab.Ciprofloxacin500mg","Cap.Doxycycline100mg","Tab.Co-trimoxazole120mg","Tab.Co-trimoxazole480mg","Syp.Co-trimoxazole","Tab.Phenoxymethylpenicillin250mg","Tab.Phenoxymethylpenicillin500mg","Tab.Azithromycin1gm","Tab.Azithromycin500mg","Tab.Erythromycin500mg","Cap.Tetracycline500mg","Tab.Cefixime200mg","Tab.Cefixime400mg","Inj.Ceftriaxone250mg","Inj.Spectinomycin2gm","Inj.BenzathinePenicillinG2.4millionunits","Tab.Acyclovir200mg","Tab.Acyclovir400mg","Tab.Valaciclovir1mg","Tab.Famciclovir250mg","Tab.Metronidazole400mg","Tab.Salbutamol4mg","Syp.Salbutamol","Tab.Aminophylline100mg","Tab.Aminophylline175mg","Tab.Aminophylline350mg","Tab.Theophylline200mg","Tab.Theophylline300mg","Syp.Theophylline","Tab.DicycloverineHcl10mg","Tab.HyoscineNButylbromide10mg","Tab.HyoscineNButylbromide20mg","Tab.VitminBComplex","Tab.Mebendazole100mg","Syp.Mebendazole100mg","Tab.Levamisole40mg","Syp.Levamisole","Tab.Albendazole400mg","Tab.Antacid","Tab.Ranitidine150mg","Tab.Paracetamol500mg","Syp.Paracetamol","Tab.Aspirin75mg","Tab.Aspirin100mg","Tab.Diclofenac50mg","Tab.Diclofenac100mg","TabIbuprofen400mg","TabDiazepam5mg","Tab.FerusFumarate200mg+FolicAcid0.20mg","Tab.ChlorpheniramineMaleate4mg","Oint.NeomycinSulphate5mg+BacitracinZinc500IU","ChloramphenicolEyeOint","ChloramphenicolEarDrop","LotionBenzylBenzoate25%","Oint.Benzoic&SalicylicAcid","LotionGentianViolet2%","Tab.Fluconazole50mg","Tab.Fluconazole150mg","Cap.Fluconazole50mg","Cap.Fluconazole150mg","Tab.Myconazole","Tab.Clotrimazole","ClotrimazoleCream","Tab.Secnidazole","Tab.Tinidazole","Tab.PenicillinV250mg","Tab.CalciumLactate300mg","Tab.Zinc","ORS","Cap.VitA2lacIU"};
					for (int i = 0; i < json.length()-json.length()%4; i=i+4) {
						JSONObject singleRow = new JSONObject();
						JSONObject singleRow2 = new JSONObject();
						JSONObject singleRow3 = new JSONObject();
						JSONObject singleRow4 = new JSONObject();
						try {
							singleRow = json.getJSONObject(i);
							singleRow2 = json.getJSONObject(i+1);
							singleRow3 = json.getJSONObject(i+2);
							singleRow4 = json.getJSONObject(i+3);


							singleRow.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_age").toString()));
							singleRow2.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_age").toString()));
							singleRow3.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_age").toString()));
							singleRow4.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("client_age").toString()));

							singleRow.put("visit_date",EnglishtoBangla.getEngToBanDate(singleRow.get("visit_date").toString()));
							singleRow2.put("visit_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("visit_date").toString()));
							singleRow3.put("visit_date",EnglishtoBangla.getEngToBanDate(singleRow3.get("visit_date").toString()));
							singleRow4.put("visit_date",EnglishtoBangla.getEngToBanDate(singleRow4.get("visit_date").toString()));

							singleRow.put("bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("bp").toString()));
							singleRow2.put("bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("bp").toString()));
							singleRow3.put("bp",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("bp").toString()));
							singleRow4.put("bp",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("bp").toString()));

							singleRow.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pulse").toString()));
							singleRow2.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pulse").toString()));
							singleRow3.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("pulse").toString()));
							singleRow4.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("pulse").toString()));

							singleRow.put("temperature",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("temperature").toString()));
							singleRow2.put("temperature",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("temperature").toString()));
							singleRow3.put("temperature",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("temperature").toString()));
							singleRow4.put("temperature",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("temperature").toString()));



						} catch (Exception e) {
							e.printStackTrace();
						}

				%>
				<h1 align="middle">সাধারণ রোগী রেজিস্টার</h1>
				<table   class="mnc-table-left"  border="1" cellspacing="0" style="font-size:11px; min-width: 8.5in; min-height:14in;table-layout:fixed;">
					<tbody >
						<tr>
							<th width="8%" >তারিখ</th>
							<th width="10%" >রেজিঃ নং(দৈনিক/মাসিক)</th>
							<th width="12%" class="nowrap1"><b>সেবা গ্রহীতার সাধারণ পরিচিতি</b></th>
							<th width="25%"  ><b>অসুবিধাসমুহ</b></th>
							<th width="8%" ><b>দৈহিক পরীক্ষা</b></th>
							<th width="27%"><b>চিকিৎসা ও উপদেশ</b></th>
							<th width="10%" ><b>মন্তব্য</b></th>
						</tr>
						<%--Frist Client--%>
						<tr>
							<td rowspan="11">
								<span class="bold-italic"><% out.println(singleRow.get("visit_date"));%></span>
							</td>
							<td rowspan="11"></td>
							<td rowspan="11">
								<div class="row">
									<div class="col-md-12">
										নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow.get("name"));%>(<% out.println(singleRow.get("healthid"));%>)</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_age"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামী/পিতার নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("husbandname"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("village_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন/পৌরসভা: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("union_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("upazila_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										জেলা : &nbsp;<span class="bold-italic"><% out.println(singleRow.get("zilla_name"));%></span>
									</div>
								</div>

							</td>
							<td rowspan="11">অসুবিধাঃ<span class="bold-italic">-&nbsp;&nbsp;<p><% out.println(singleRow.get("symptom"));%></p>&nbsp;&nbsp; </span></td>
							<td>
								রক্তস্বল্পতা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("gp_anemia"));%> </span>
							</td>
							<td rowspan="11">চিকিৎসাঃ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("treatment"));%></span></br>পরামর্শঃ<span class="bold-italic"><%
								out.println(advice_str);%> </span></td>
							<td rowspan="11"></td>
						</tr>
						<tr>
							<td>জন্ডিস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("gp_jaundice"));%> </span> </td>
						</tr>
						<tr>
							<td>ইডিমা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("gp_edema"));%></span></td>
						</tr>
						<tr>
							<td>নাড়ির গতি<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("pulse"));%></span></td>
						</tr>
						<tr>
							<td>তাপমাত্রা <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("temperature"));%></span></td>
						</tr>
						<tr>
							<td>রক্ত চাপ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("bp"));%></span> </td>
						</tr>
						<tr>
							<td> ফুসফুস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("gp_lungs"));%></span></td>
						</tr>
						<tr>
							<td>লিভার<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("gp_liver"));%></span></td>
						</tr>
						<tr>
							<td>প্লীহা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("gp_splin"));%></span></td>
						</tr>
						<tr>
							<td>টনসিল<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("gp_tonsil"));%></span></td>
						</tr>
						<tr>
							<td>অন্যান্য</td>
						</tr>



						<%--2nd Client--%>
						<tr>
							<td rowspan="11">
								<span class="bold-italic"><% out.println(singleRow2.get("visit_date"));%></span>
							</td>
							<td rowspan="11"></td>
							<td rowspan="11">
								<div class="row">
									<div class="col-md-12">
                                        নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("name"));%>(<% out.println(singleRow2.get("healthid"));%>)</span>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_age"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামী/পিতার নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("husbandname"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("village_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন/পৌরসভা: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("union_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("upazila_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										জেলা : &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("zilla_name"));%></span>
									</div>
								</div>

							</td>
							<td rowspan="11">অসুবিধাঃ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("symptom"));%> &nbsp;&nbsp;</span></td>
							<td>
								রক্তস্বল্পতা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("gp_anemia"));%> </span>
							</td>
							<td rowspan="11">চিকিৎসাঃ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("treatment"));%></span></br>পরামর্শঃ<span class="bold-italic"> <%
								out.println(advice_str);%> </span></td>
							<td rowspan="11"></td>
						</tr>
						<tr>
							<td>জন্ডিস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("gp_jaundice"));%> </span> </td>
						</tr>
						<tr>
							<td>ইডিমা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("gp_edema"));%></span></td>
						</tr>
						<tr>
							<td>নাড়ির গতি<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("pulse"));%></span></td>
						</tr>
						<tr>
							<td>তাপমাত্রা <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("temperature"));%></span></td>
						</tr>
						<tr>
							<td>রক্ত চাপ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("bp"));%></span> </td>
						</tr>
						<tr>
							<td> ফুসফুস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("gp_lungs"));%></span></td>
						</tr>
						<tr>
							<td>লিভার<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("gp_liver"));%></span></td>
						</tr>
						<tr>
							<td>প্লীহা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("gp_splin"));%></span></td>
						</tr>
						<tr>
							<td>টনসিল<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("gp_tonsil"));%></span></td>
						</tr>
						<tr>
							<td>অন্যান্য</td>
						</tr>



						<%--3rd Client--%>
						<tr>
							<td rowspan="11">
								<span class="bold-italic"><% out.println(singleRow3.get("visit_date"));%></span>
							</td>
							<td rowspan="11"></td>
							<td rowspan="11">
								<div class="row">
									<div class="col-md-12">
                                        নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("name"));%>(<% out.println(singleRow3.get("healthid"));%>)</span>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_age"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামী/পিতার নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("husbandname"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("village_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন/পৌরসভা: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("union_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("upazila_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										জেলা : &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("zilla_name"));%></span>
									</div>
								</div>

							</td>
							<td rowspan="11">অসুবিধাঃ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("symptom"));%>&nbsp;&nbsp; </span></td>
							<td>
								রক্তস্বল্পতা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("gp_anemia"));%> </span>
							</td>
							<td rowspan="11">চিকিৎসাঃ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("treatment"));%></span></br>পরামর্শঃ<span class="bold-italic"> <%
								out.println(advice_str);%> </span></td>
							<td rowspan="11"></td>
						</tr>
						<tr>
							<td>জন্ডিস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("gp_jaundice"));%> </span> </td>
						</tr>
						<tr>
							<td>ইডিমা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("gp_edema"));%></span></td>
						</tr>
						<tr>
							<td>নাড়ির গতি<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("pulse"));%></span></td>
						</tr>
						<tr>
							<td>তাপমাত্রা <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("temperature"));%></span></td>
						</tr>
						<tr>
							<td>রক্ত চাপ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("bp"));%></span> </td>
						</tr>
						<tr>
							<td> ফুসফুস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("gp_lungs"));%></span></td>
						</tr>
						<tr>
							<td>লিভার<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("gp_liver"));%></span></td>
						</tr>
						<tr>
							<td>প্লীহা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("gp_splin"));%></span></td>
						</tr>
						<tr>
							<td>টনসিল<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("gp_tonsil"));%></span></td>
						</tr>
						<tr>
							<td>অন্যান্য</td>
						</tr>

						<%--4th Client--%>
						<tr>
							<td rowspan="11">
								<span class="bold-italic"><% out.println(singleRow4.get("visit_date"));%></span>
							</td>
							<td rowspan="11"></td>
							<td rowspan="11">
								<div class="row">
									<div class="col-md-12">
                                        নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("name"));%>(<% out.println(singleRow4.get("healthid"));%>)</span>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("client_age"));%> </span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামী/পিতার নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("husbandname"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("village_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন/পৌরসভা: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("union_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("upazila_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										জেলা : &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("zilla_name"));%></span>
									</div>
								</div>

							</td>
							<td rowspan="11">অসুবিধাঃ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("symptom"));%>&nbsp;&nbsp; </span></td>
							<td>
								রক্তস্বল্পতা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("gp_anemia"));%> </span>
							</td>
							<td rowspan="11">চিকিৎসাঃ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("treatment"));%></span></br>পরামর্শঃ<span class="bold-italic"> <%
								out.println(advice_str);%> </span></td>
							<td rowspan="11"></td>
						</tr>
						<tr>
							<td>জন্ডিস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("gp_jaundice"));%> </span> </td>
						</tr>
						<tr>
							<td>ইডিমা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("gp_edema"));%></span></td>
						</tr>
						<tr>
							<td>নাড়ির গতি<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("pulse"));%></span></td>
						</tr>
						<tr>
							<td>তাপমাত্রা <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("temperature"));%></span></td>
						</tr>
						<tr>
							<td>রক্ত চাপ<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("bp"));%></span> </td>
						</tr>
						<tr>
							<td> ফুসফুস<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("gp_lungs"));%></span></td>
						</tr>
						<tr>
							<td>লিভার<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("gp_liver"));%></span></td>
						</tr>
						<tr>
							<td>প্লীহা<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("gp_splin"));%></span></td>
						</tr>
						<tr>
							<td>টনসিল<span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("gp_tonsil"));%></span></td>
						</tr>
						<tr>
							<td>অন্যান্য</td>
						</tr>


					</tbody>
				</table>
				<%}%>

			</div>
		</div>
	</div>
</div>


