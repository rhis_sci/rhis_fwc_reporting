<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/8/2021
  Time: 11:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<% int menuType = (Integer) request.getAttribute("type"); %>
<% int Type = (Integer) request.getAttribute("type"); %>
<% int userType = (Integer) request.getAttribute("utype"); %>

<style>
    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type="number"] {
        -moz-appearance: textfield;
    }
</style>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title" id="box_title">Add Provider</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body search_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- geo location-->
                    <%@include file="geoSection.jsp" %>
                    <!--/geo location--->

                    <!-- facility type-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-6" id="provider_facility_type" style="display: none;">
                                <label>Facility Type</label>
                                <select class="form-control select2 changeFacility" id="providerFacility"
                                        style="width: 100%;">
                                </select>
                            </div>
                            <div class="form-group col-md-6" id="FacName">
                                <label>Facility Name</label>
                                <select class="form-control select2" id="facilityName" required="required">
                                    <option value="">--Select Facility Name--</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/facility type--->

                    <!--provider type--->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-6" id="provider_type">
                                <label>Provider Type</label>
                                <select class="form-control select2" id="providerType" required="required">
                                    <option value="">--Select Provider Type--</option>
                                    <option value="4">4 - FWV</option>
                                    <option value="5">5 - S.A.C.M.O (FP)</option>
                                    <option value="6">6 - S.A.C.M.O (HS)</option>
                                    <option value="101">101 - PARAMEDIC</option>
                                    <option value="17">17 - Midwife</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6" id="provider_code">
                                <label>Provider Code</label>
                                <div id="hints">
                                    <input type="number" class="form-control" id="ProvCode" required="required"
                                           placeholder="Provider Code">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/provider type--->

                    <!--provider info--->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-4" id="Pname">
                                <label>Provider Name</label>
                                <input type="text" class="form-control" id="ProvName" required="required"
                                       placeholder="Provider Name">
                            </div>
                            <div class="form-group col-md-4" id="Ppass">
                                <label>Provider Password</label>
                                <input type="text" class="form-control" id="ProvPass" required="required"
                                       placeholder="Provider Password">
                            </div>
                            <div class="form-group col-md-4" id="Pmob">
                                <label>Provider Mobile No.</label>
                                <input type="text" maxlength="11" class="form-control" id="ProvMob" required="required"
                                       placeholder="Provider Mobile No.">
                            </div>
                        </div>
                    </div>
                    <!--/provider info--->
                    <!--version--->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-4" id="version_name" style="display: none;">
                                <label>Version</label>
                                <input type="text" class="form-control" id="version" placeholder="Version">
                            </div>
                        </div>
                    </div>
                    <!--/version--->

                    <%@include file="submitButton.jsp" %>

                </div>
            </div>
        </div>
    </div>

    <script>
        $(".select2").select2();


        $('#providerType').on('change', function(){
            <!-- @Author: Evana ,@date: 09/07/2019, @Title: For Provider Code insert -->
            var type = $("#menuType").val();
                // loadDivision("#RdivName");

                // $('#RzillaName').find('option').remove().end();
                // $('#RupaZilaName').find('option').remove().end();
                // $('#RunionName').find('option').remove().end();
                getProviderHints();

                $("#hints").html("<input type='number' class='form-control' id='ProvCode' value=''>");
        });


        $( document ).ready(function() {

            $("#unionFacilityName").hide();
            $("#providerFacility").empty();

            var output = "<option value ='' selected></option>";

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + "<option value = " + key + "_" + val + ">" + val + "</option>";
            });

            $("#providerFacility").append(output);
        });



        $(".changeFacility").on('change', function(){
            if($('#FacName').is(':visible')){
                var forminfo = new Object();
                forminfo.zillaid = $('#RzillaName').val();
                forminfo.upazilaid = $('#RupaZilaName').val();
                forminfo.unionid = $('#RunionName').val();

                if($('#facility_type').is(':visible'))
                    forminfo.facilityType = $('#facilityType').val().split("_")[0];
                else if($('#provider_facility_type').is(':visible'))
                    forminfo.facilityType = $('#providerFacility').val().split("_")[0];
                forminfo.type = "6";

                $.ajax({
                    type: "POST",
                    url:"getFacilityInfo",
                    timeout:60000, //60 seconds timeout
                    data:{"forminfo":JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        facilityJS = JSON.parse(response);
                        $("#facilityName").empty();
                        if((<% out.println(userType); %>)=="999"){
                            var output = "<option value ='' selected></option>";
                            // output = output + "<option value ='মা-মনি কার্যালয়, গুলশান , ঢাকা'>মা-মনি কার্যালয়, গুলশান , ঢাকা</option>";
                        }
                        else
                            var output = "<option value ='' selected></option>";

                        $.each(facilityJS, function(key, val) {
                            output = output + "<option value = " + key + ">" + val + "</option>";
                        });

                        $("#facilityName").append(output);
                    },
                    complete: function() {
                        preventMultiCall = 0;
                    },
                    error: function(xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });
            }
        });

        /**
         @Author: Evana
         @Date : 07/07/2019
         @Title: Provider code hints based on provider type and District selection
         ***/

        function getProviderHints()
        {
            //alert("hi");
            var forminfo = new Object();
            forminfo.zillaid = $('#RzillaName').val();
            forminfo.upazilaid = $('#RupaZilaName').val();
            forminfo.unionid = $('#RunionName').val();
            forminfo.providerType = $('#providerType').val();

            var provtype = $('#providerType').val();
            //alert(zillaid);

            if(provtype==4 || provtype==5)
            {

                $.ajax({
                    type: "POST",
                    url:"getProviderCode",
                    timeout:60000, //60 seconds timeout
                    data:{"forminfo":JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        // alert(response);
                        var output = "";
                        var parsedData = JSON.parse(response);

                        // $("#hint").html("<input type='text' class='form-control' readonly='readonly' value='Hint* "+ parsedData.code+ "'>");
                        $("#hints").html("<input type='number' class='form-control' id='ProvCode' value='"+ parsedData.code+"'>");


                    },
                    complete: function() {
                        preventMultiCall = 0;
                    },
                    error: function(xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });
                return false;
            }
            else
            {
                return false;
            }
        }


        $("#RupaZilaName").on('change', function(){
            var forminfo = new Object();
            forminfo.zillaid = $('#RzillaName').val();
            forminfo.upazilaid = $('#RupaZilaName').val();
            forminfo.providerType = "All";
            forminfo.providerListType = $('#retiredType').val();

            $.ajax({
                type: "POST",
                url: "getProviderInfo",
                timeout: 60000, //60 seconds timeout
                data: {"forminfo": JSON.stringify(forminfo)},
                success: function (response) {
                    console.log(response);
                    providerInfoJS = JSON.parse(response);
                    $("#providerID").empty();
                    var output = "";
                    $.each(providerInfoJS, function (key, val) {
                        if($('#retiredType').val()=="2")
                            output = output + "<option value = " + key + ">" + val + "</option>";
                        else
                            output = output + "<option value = " + key + ">" + val + "(" + key + ")" + "</option>";
                    });

                    $("#providerID").append(output);
                },
                complete: function () {
                    preventMultiCall = 0;
                },
                error: function (xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
        });


    </script>


</div>
