<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.sci.rhis.util.EnglishtoBangla" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.lang.reflect.Array" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8" %>
<script src="js/lib/ddtf.js"></script>
<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript">

    function PrintElem(elem) {
//        $('button').hide();
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div');
        mywindow.document.write('<html><head><title></title>');
		/*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();

        return true;
    }
    $(document).on("click", '.button_next' ,function (){
        $('#mnc-table-left').hide();
        $('#mnc-table-right').show();
        var button_val = $(".button_next").val();
        if(button_val == 1){
            $(".button_next").val(2);
            $(".button_pre").val(1);
            $(".button_next").attr("disabled","disabled");
            $(".button_pre").removeAttr("disabled");
            $(".showing_page").text("2 of 2  ");
        }
    });

    $(document).on("click", '.button_pre' ,function (){
        $('#mnc-table-left').show();
        $('#mnc-table-right').hide();
        var button_val = $(".button_pre").val();
        if(button_val>0)
        {
            $(".button_pre").val(0);
            $(".button_next").val(1);
            $(".button_next").removeAttr("disabled");
            $(".button_pre").attr("disabled","disabled");
            $(".showing_page").text("1 of 2  ");
        }
    });
</script>

<style>
	.mnc-table-left th,.mnc-table-right th{
		border: 1px solid black!important;
		text-align: center!important;
		vertical-align: middle!important;
		padding: 2px;
	}
	.mnc-table-left td,.mnc-table-right td{
		vertical-align: top!important;
		padding-left: 2px;
	}
	.nowrap{
		white-space: nowrap!important;
	}
	.inner_table td{
		border: 1px solid black!important;
		text-align: left!important;
		vertical-align: middle!important;
		padding-left: 2px;
	}
	.pad-bot-15 div{
		padding-bottom: 10px;
	}
	.pad-bot-5 div{
		padding-bottom: 5px;
	}
	.bold-italic{
		font-weight: bold;
		font-style: italic;
		padding-left: 4px;
		font-size: 110%;
	}
</style>
<%

%>
<div class="tile-body nopadding" id="reportTable">
	<div class="row">
		<div class="col-md-12">
			<div class="dropdown" style="float: right; margin-right: 2%;">
				<button class="btn btn-primary" type="button" id="btnPrint" onclick="PrintElem('#printDiv')"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
					<span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="printDiv" style="margin-top: 2%;">
				<style>
					/*#mnc-table-left th,#mnc-table-right th{*/
						/*border: 1px solid black!important;*/
						/*text-align: center!important;*/
						/*vertical-align: middle!important;*/
						/*padding: 2px;*/
					/*}*/
					.mnc-table-left td,.mnc-table-right td{
						vertical-align: top!important;
					}
					.mnc-table-left{
						min-height: 8.5in;
					}
					.pad-bot-15 div{
						padding-bottom: 10px;
					}
					table { /* Or specify a table class */
						max-height: 100%!important;
						overflow: hidden!important;
						page-break-after: always!important;
					}
                    td {
                        padding: 10px;
                    }

					.rotate {
						/* Safari */
						-webkit-transform: rotate(-90deg);

						/* Firefox */
						-moz-transform: rotate(-90deg);

						/* IE */
						-ms-transform: rotate(-90deg);

						/* Opera */
						-o-transform: rotate(-90deg);

						float: left;

					}
					/*.pad-bot-5 div{*/
						/*padding-bottom: 5px;*/
					/*}*/
					/*.bold-italic{*/
						/*font-weight: bold;*/
						/*font-style: italic;*/
						/*padding-left: 4px;*/
						/*font-size: 110%;*/
					/*}*/
				</style>
				<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'>
					<tr>
						<%--<td width="20%">--%>
							<%--<div style='border: 1px solid black;text-align: center;'>দুটি সন্তানের বেশি নয়<br>একটি হলে ভাল হয়।</div>--%>
						<%--</td>--%>
						<%--<td width="50%" style='text-align: center;'>--%>
							<%--<img width='60' src='image/dgfp_logo.png'></br>--%>
							<%--<h1>মা ও নবজাতক সেবা রেজিস্টার</h1>--%>
							<%--গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>পরিবার পরিকল্পনা অধিদপ্তর--%>
						<%--</td>--%>
						<%--<td width="30%" style='text-align: right;' class='page_no'></td>--%>
					</tr>
				</table>
				</br>
				<%
					String data = request.getAttribute("Result").toString();
					JSONArray json = new JSONArray(data);
					for (int i = 0; i < json.length(); i=i+2) {
						JSONObject singleRow = new JSONObject();
						JSONObject singleRow2 = new JSONObject();
						JSONArray firstRowData = new JSONArray();
                        JSONArray secondRowData = new JSONArray();
                        JSONObject nextRow = new JSONObject();
						try {

							singleRow = json.getJSONObject(i);
							for(int j= 0; j<json.length(); j++){
                                if(i == json.length()-1){break;}
							    nextRow = json.getJSONObject(i+1);
							    if(singleRow.get("healthid").equals(nextRow.get("healthid"))){
                                    firstRowData.put(j,nextRow);
                                    i++;
                                    System.out.println("FIRST"+ i);
                                }else{
                                    break;
                                }
                            }
                            if(i != json.length()-1){
								singleRow2 = json.getJSONObject(i + 1);
							}


                            for(int j= 0; j<json.length(); j++){
                                if(i == json.length()-1){break;}
                                nextRow = json.getJSONObject(i+1);
                                if(singleRow.get("healthid").equals(nextRow.get("healthid"))){
                                    secondRowData.put(j,nextRow);
                                    i++;
                                    System.out.println("SECOND"+ i);
                                }else{
                                    break;
                                }
                            }

							singleRow.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_age").toString()));
							singleRow2.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_age").toString()));

							singleRow.put("elcono",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("elcono").toString()));
							singleRow2.put("elcono",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("elcono").toString()));

							singleRow.put("son",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("son").toString()));
							singleRow2.put("son",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("son").toString()));

							singleRow.put("dau",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("dau").toString()));
							singleRow2.put("dau",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("dau").toString()));

							singleRow.put("implant_date",EnglishtoBangla.getEngToBanDate(singleRow.get("implant_date").toString()));
							singleRow2.put("implant_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("implant_date").toString()));

							singleRow.put("first_fixed_date",EnglishtoBangla.getEngToBanDate(singleRow.get("first_fixed_date").toString()));
							singleRow2.put("first_fixed_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("first_fixed_date").toString()));

							singleRow.put("second_fixed_date",EnglishtoBangla.getEngToBanDate(singleRow.get("second_fixed_date").toString()));
							singleRow2.put("second_fixed_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("second_fixed_date").toString()));

							singleRow.put("third_fixed_date",EnglishtoBangla.getEngToBanDate(singleRow.get("third_fixed_date").toString()));
							singleRow2.put("third_fixed_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("third_fixed_date").toString()));

							singleRow.put("implant_remover_date",EnglishtoBangla.getEngToBanDate(singleRow.get("implant_remover_date").toString()));
							singleRow2.put("implant_remover_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("implant_remover_date").toString()));

							singleRow.put("followup_date",EnglishtoBangla.getEngToBanDate(singleRow.get("followup_date").toString()));
							singleRow2.put("followup_date",EnglishtoBangla.getEngToBanDate(singleRow2.get("followup_date").toString()));

						} catch (Exception e) {
							e.printStackTrace();
						}
				%>
				<h1 align="middle">ইমপ্ল্যান্ট রেজিস্টার</h1>
				<table   class="mnc-table-left"  border="1" cellspacing="0" style="font-size:11px; min-height: 8.5in;table-layout:fixed;">
					<tbody >
					<tr>
						<th rowspan="3" >তারিখ</th>
						<th rowspan="3" >রেজি নং </th>
						<th rowspan="3" >গ্রহীতার পূর্ণ বিবরণ  </th>
						<th rowspan="3">জীবন্ত সন্তান <%--<br>--%> সংখ্যা </th>
						<th  colspan="5" class="nowrap"><b>প্রয়োগ</b></th>
					</tr>
					<tr>
						<th rowspan="2" class="nowrap">ইমপ্ল্যান্টের নাম</th>
						<th rowspan="2" class="nowrap">ইমপ্ল্যান্টে প্রয়োগের <br>সময় (ইন্তারভাল / <br> প্রসব পরবর্তী )</th>
						<th rowspan="2" class="nowrap">প্রয়োগের  তারিখ</th>
						<th rowspan="2" class="nowrap">গ্রহীতার যাতায়াত <br> ভাতা  ও স্বাক্ষর / <br> টিপসহি</th>
						<th rowspan="2" class="nowrap">প্রয়োগকারীর নাম ও <br> স্বাক্ষর</th>
					</tr>
					<tr>

					</tr>
					<tr>
						<th class="nowrap">১</th>
						<th class="nowrap">২</th>
						<th class="nowrap">৩</th>
						<th class="nowrap">৪</th>
						<th class="nowrap">৫</th>
						<th >৬</th>
						<th >৭</th>
						<th class="nowrap">৮</th>
						<th class="nowrap">৯</th>
					</tr>
						<%--Frist Client--%>
					<tr>
						<td  class="nowrap">
							<div class="row">
								<div class="col-md-12">
									<span class="bold-italic" style="text-align: center">
										<% out.println(singleRow.get("implant_date")); %>
                                    </span>
								</div>
							</div>
						</td>
						<td nowrap  class="pad-bot-15">

						</td>
						<td nowrap  class="pad-bot-5">
							<div class="row">
								<div class="col-md-12">
									নাম ও বয়স: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("name"));%>(<%out.println(singleRow.get("client_age"));%>)</span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									দম্পতি / জাতীয় পরিচয় পত্র / জন্ম নিবন্ধন নম্বর :
									&nbsp;<span class="bold-italic"><% out.println(singleRow.get("elcono"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									স্বামীর নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("husbandname"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									গ্রাম/মহল্লা: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("villagenameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									ইউনিট: &nbsp;  &nbsp; ওয়ার্ড নম্বর: &nbsp;
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									ইউনিয়ন / পৌরসভা :  &nbsp;<span class="bold-italic"><% out.println(singleRow.get("unionnameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									উপজেলা / থানা :  &nbsp; <span class="bold-italic"><% out.println(singleRow.get("upazilanameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									জেলা :  &nbsp; <span class="bold-italic"><% out.println(singleRow.get("zillanameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									মোবাইল ফোন  নম্বর : &nbsp;<span class="bold-italic">0<% out.println(singleRow.get("mobileno"));%></span>
								</div>
							</div>
						</td>

						<td >
							ছেলে : <br>&nbsp; <span class="bold-italic"> <% out.println(singleRow.get("son"));%></span>
							<br><br>
							মেয়ে:<span class="bold-italic"> <% out.println(singleRow.get("dau"));%></span>
						</td>
						<td >
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("implant_name")); %></span>
							</div>
						</td>
						<td >
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("implant_after_deli")); %></span>
							</div>
						</td>
						<td  >
							<div class="col-md-12" >
								<span class="bold-italic"><% out.println(singleRow.get("implant_date")); %></span>
							</div>
						</td>
						<td >
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("client_allowance")); %></span>
							</div>
						</td>
						<td>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow.get("attendantname")); %></span>
							</div>
						</td>
					</tr>
		<%--2nd Client--%>
                    <% if(singleRow2.has("implant_date")){
                    %>
                    <tr>
                        <td  class="nowrap">
                            <div class="row">
                                <div class="col-md-12">
									<span class="bold-italic" style="text-align: center">
										<% out.println(singleRow2.get("implant_date")); %>
                                    </span>
                                    </br>
                                    <span class="bold-italic"></span>
                                </div>
                            </div>
                        </td>
                        <td nowrap  class="pad-bot-15">

                        </td>
                        <td nowrap  class="pad-bot-5">
							<div class="row">
								<div class="col-md-12">
									নাম ও বয়স: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("name"));%>(<%out.println(singleRow2.get("client_age"));%>)</span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									দম্পতি / জাতীয় পরিচয় পত্র / জন্ম নিবন্ধন নম্বর :
									&nbsp;<span class="bold-italic"><% out.println(singleRow2.get("elcono"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									স্বামীর নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("husbandname"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									গ্রাম/মহল্লা: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("villagenameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									ইউনিট: &nbsp;  &nbsp; ওয়ার্ড নম্বর: &nbsp;
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									ইউনিয়ন / পৌরসভা :  &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("unionnameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									উপজেলা / থানা :  &nbsp; <span class="bold-italic"><% out.println(singleRow2.get("upazilanameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									জেলা :  &nbsp; <span class="bold-italic"><% out.println(singleRow2.get("zillanameeng"));%></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									মোবাইল ফোন  নম্বর : &nbsp;<span class="bold-italic">0<% out.println(singleRow2.get("mobileno"));%></span>
								</div>
							</div>
                        </td>

						<td >
							ছেলে : <br>&nbsp; <span class="bold-italic"> <% out.println(singleRow2.get("son"));%></span>
							<br><br>
							মেয়ে:<span class="bold-italic"> <% out.println(singleRow2.get("dau"));%></span>
						</td>
                        <td >
                            <div class="col-md-12" >
                                <span class="bold-italic"> <% out.println(singleRow2.get("implant_name")); %> </span>
                            </div>
                        </td>
                        <td >
                            <div class="col-md-12" >
                                <span class="bold-italic"> <% out.println(singleRow2.get("implant_after_deli")); %> </span>
                            </div>
                        </td>
                        <td  >
                            <div class="col-md-12" >
                                <span class="bold-italic"><% out.println(singleRow2.get("implant_date")); %></span>
                            </div>
                        </td>
                        <td >
                            <div class="col-md-12" >
                                <span class="bold-italic"> <% out.println(singleRow2.get("client_allowance")); %></span>
                            </div>
                        </td>
                        <td>
							<div class="col-md-12" >
								<span class="bold-italic"> <% out.println(singleRow2.get("attendantname")); %></span>
							</div>
						</td>
                    </tr>
                    <% }%>

					</tbody>
				</table>
				<h1 align="middle">ইমপ্ল্যান্ট রেজিস্টার</h1>
				<table  class="mnc-table-right"  border="1" cellspacing="0" style="font-size:11px;">
					<thead>
						<tr>
							<th colspan="6" nowrap><b>ফলো - আপ</b></th>
							<th colspan="2" nowrap><b> জটিলতা ও চিকিৎসা</b></th>
							<th colspan="3" nowrap><b>অপসারণ </b></th>
							<th rowspan="2" nowrap><b>আয়ন - ব্যয়ন  কর্মকর্তার <br> মন্তব্য ও স্বাক্ষর </b></th>
						</tr>
						<tr>
                            <th colspan="2"></th>
							<th><b>নির্দিষ্ট তারিখ</b></th>
                            <th><b>প্রকৃত তারিখ</b></th>
                            <th><b>গ্রহিতার ভাতা ও <br>
								স্বাক্ষর  / টিপসহি</b></th>
                            <th><b>ফলোআপকারির <br> নাম ও স্বাক্ষর </b></th>
                            <th><b>জটিলতার  বিবরণ</b></th>
                            <th><b>চিকিৎসা</b></th>
                            <th><b>অপসারণের <br> তারিখ  ও কারণ </b></th>
                            <th><b>অপসারণের পর <br> পঃ পঃ পদ্ধতি দেয়া <br>হলে তার নাম </b></th>
                            <th><b>অপসারণকারীর  নাম <br>পদবী ও স্বাক্ষর  </b></th>
						</tr>
						<tr>
							<th colspan="2">১০</th>
							<th></th>
							<th >১১</th>
							<th >১২</th>
							<th nowrap>১৩</th>
							<th nowrap>১৪</th>
							<th nowrap>১৫</th>
							<th nowrap>১৬</th>
							<th nowrap>১৭</th>
							<th >১৮</th>
							<th >১৯</th>
					</thead>
					<tbody>
						<%--first client--%>
						<tr>
							<td rowspan="3"><p class="rotate">নিয়মিত</p></td>
							<td> ১ ম </td>
							<td>
								<% out.println(singleRow.get("first_fixed_date"));%>
							</td>
							<td>
                                <% out.println(singleRow.get("followup_date"));%>
							</td>
							<td>
                                <%
									if(!singleRow.get("followup_date").toString().isEmpty()){
										out.println(singleRow.get("client_allowance"));
									}

								%>
                            </td>
							<td>
                                <% if(!singleRow.get("followup_date").toString().isEmpty()){
									out.println(singleRow.get("attendantname"));}
								%>
                            </td>
							<td rowspan="4">
                                <ul>
                                <%
                                   String complications = singleRow.get("complication").toString().replace("[","");
                                   complications = complications.replace("]","");
                                   String[] complicationsArray = complications.split(",");

                                    if(firstRowData.length()>0){
                                        for(int k=0 ; k<firstRowData.length();k++){
                                            JSONObject firstFollowUp = firstRowData.getJSONObject(k);
                                            String followUpcomplications = firstFollowUp.get("complication").toString().replace("[","");
                                            followUpcomplications = followUpcomplications.replace("]","");
                                            String[] tempcomplicationsArray = followUpcomplications.split(",");

                                                for (String t: tempcomplicationsArray){
                                                    List<String> list = Arrays.asList(complicationsArray);
                                                    if(list.contains(t)){
                                                        System.out.println("Hello A");
                                                    }else {
                                                        list.add(t);
                                                        complicationsArray = list.toArray(new String[0]);
                                                    }

                                                }

                                        }

                                    }



                                    for (String c: complicationsArray){
                                        if(c.equals("\"1\"")){
                                            out.println("<li> মাসিক বন্ধ হয়ে যাওয়া </li>");
                                        }else if(c.equals("\"2\"")){
                                            out.println("<li> ইমপ্লান্ট রড স্থাপনের জায়গায় সংক্রমণ </li>");
                                        }else if(c.equals("\"3\"")){
                                            out.println("<li> তলপেটে প্রচন্ড ব্যাথা </li>");
                                        }else if(c.equals("\"4\"")){
                                            out.println("<li> ইমপ্লান্টের কোন রড বেড় হয়ে আসা </li>");
                                        }else if(c.equals("\"5\"")){
                                            out.println("<li> অতিরিক্ত রক্তস্রাব </li>");
                                        }else if(c.equals("\"6\"")){
                                            out.println("<li> প্রচন্ড মাথা ব্যাথা বা চোখে ঝাপসা দেখা </li>");
                                        }
                                    }
//                                    out.println(singleRow.get("complication"));
                                %>
                                </ul>
                            </td>
							<td rowspan="4">
                                <ul>
                                    <%
                                        String treatments = singleRow.get("treatment").toString().replace("[","");
                                        treatments = treatments.replace("]","");
                                        String[] treatmentsArray = treatments.split(",");

                                        if(firstRowData.length()>0){
                                            for(int k=0 ; k<firstRowData.length();k++){
                                                JSONObject firstFollowUp = firstRowData.getJSONObject(k);
                                                String followUptreatments = firstFollowUp.get("treatment").toString().replace("[","");
                                                followUptreatments = followUptreatments.replace("]","");
                                                String[] temptreatmentArray = followUptreatments.split(",");

                                                for (String t: temptreatmentArray){
                                                    List<String> list = Arrays.asList(treatmentsArray);
                                                    if(list.contains(t)){
                                                        System.out.println("Hello A");
                                                    }else {
                                                        list.add(t);
                                                        treatmentsArray = list.toArray(new String[0]);
                                                    }

                                                }

                                            }

                                        }



                                        for (String c: treatmentsArray){
                                            if(c.equals("\"1\"")){
                                                out.println("<li> Tab. Paracetamol 500 mg </li>");
                                            }else if(c.equals("\"2\"")){
                                                out.println("<li> Tab. Ibuprofen 400 mg </li>");
                                            }else if(c.equals("\"3\"")){
                                                out.println("<li> Tab. Iron Folic Acid </li>");
                                            }else if(c.equals("\"4\"")) {
                                                out.println("<li> Other </li>");
                                            }
                                        }
//                                    out.println(singleRow.get("complication"));
                                    %>
                                </ul>

                            </td>
							<td rowspan="4">
								<% out.println(singleRow.get("implant_remover_reason"));%>
								<br>
								<% out.println(singleRow.get("implant_remover_date"));%>
							</td>
							<td rowspan="4"><% out.println(singleRow.get("method_name"));%></td>
							<td rowspan="4">
								<% out.println(singleRow.get("implantremovername"));%>
								<br>
								<% out.println(singleRow.get("implant_remover_deg"));%>
							</td>
							<td rowspan="4"><% out.println(singleRow.get("monitoringofficername"));%></td>
						</tr>
						<tr>
							<td>২ য় </td>
							<td> <% out.println(singleRow.get("second_fixed_date"));%> </td>
							<td>
                                <%
                                    if(firstRowData.length()>0){
                                        JSONObject firstFollowUp = firstRowData.getJSONObject(0);
                                        firstFollowUp.put("followup_date",EnglishtoBangla.getEngToBanDate(firstFollowUp.get("followup_date").toString()));
                                        out.println(firstFollowUp.get("followup_date"));
                                    }

                                %>
                            </td>
							<td>
                                <%
                                    if(firstRowData.length()>0){
                                        JSONObject firstFollowUp = firstRowData.getJSONObject(0);
                                        out.println(firstFollowUp.get("client_allowance"));
                                    }
                                %>
                            </td>
							<td>
                                <%
                                    if(firstRowData.length()>0){
                                        JSONObject firstFollowUp = firstRowData.getJSONObject(0);
                                        out.println(firstFollowUp.get("attendantname"));
                                    }
                                %>
                            </td>
						</tr>
						<tr>
							<td>৩ য় </td>
							<td><% out.println(singleRow.get("third_fixed_date"));%></td>
							<td>
                                <%
                                    if(firstRowData.length()>1){
                                        JSONObject firstFollowUp = firstRowData.getJSONObject(1);
                                        firstFollowUp.put("followup_date",EnglishtoBangla.getEngToBanDate(firstFollowUp.get("followup_date").toString()));
                                        out.println(firstFollowUp.get("followup_date"));
                                    }

                                %>
                            </td>
							<td>
                                <%
                                    if(firstRowData.length()>1){
                                        JSONObject firstFollowUp = firstRowData.getJSONObject(1);
                                        out.println(firstFollowUp.get("client_allowance"));
                                    }
                                %>
                            </td>
							<td>
                                <%
                                    if(firstRowData.length()>1){
                                        JSONObject firstFollowUp = firstRowData.getJSONObject(1);
                                        out.println(firstFollowUp.get("attendantName"));
                                    }
                                %>
                            </td>
						</tr>
						<tr>
							<td colspan="2"><p class="rotate"> অনিয়মিত  </p></td>
							<td></td>
							<td>
                                <%
                                    if(firstRowData.length()>2){
                                        for(int k=2 ; k<firstRowData.length();k++){
                                            JSONObject firstFollowUp = firstRowData.getJSONObject(k);
                                            firstFollowUp.put("followup_date",EnglishtoBangla.getEngToBanDate(firstFollowUp.get("followup_date").toString()));
                                            out.println(firstFollowUp.get("followup_date"));
                                        }

                                    }

                                %>
                            </td>
							<td></td>
						</tr>
					<%--2nd client--%>
                        <% if(singleRow2.has("implant_date")){
                        %>
                            <tr>
                                <td rowspan="3"><p class="rotate">নিয়মিত</p></td>
                                <td> ১ ম </td>
                                <td><% out.println(singleRow2.get("first_fixed_date"));%></td>
                                <td>
                                    <%
                                        if(!singleRow2.get("followup_date").toString().isEmpty()) {
                                            out.println(singleRow2.get("followup_date"));
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(!singleRow2.get("followup_date").toString().isEmpty()) {
                                            out.println(singleRow2.get("client_allowance"));
                                        }
                                    %>
                                </td>
                                <td>
                                    <% if(!singleRow2.get("followup_date").toString().isEmpty()) {
                                        out.println(singleRow2.get("attendantName"));
                                    }
                                    %>
                                </td>
                                <td rowspan="4">
                                    <ul>
                                        <%
                                            String complications2 = singleRow2.get("complication").toString().replace("[","");
                                            complications2 = complications2.replace("]","");
                                            String[] complications2Array = complications2.split(",");

                                            if(secondRowData.length()>0){
                                                for(int k=0 ; k<secondRowData.length();k++){
                                                    JSONObject secondFollowUp = secondRowData.getJSONObject(k);
                                                    String followUpcomplications2 = secondFollowUp.get("complication").toString().replace("[","");
                                                    followUpcomplications2 = followUpcomplications2.replace("]","");
                                                    String[] tempcomplications2Array = followUpcomplications2.split(",");

                                                    for (String t: tempcomplications2Array){
                                                        List<String> list = Arrays.asList(complications2Array);
                                                        if(list.contains(t)){
                                                            System.out.println("Hello A");
                                                        }else {
                                                            list.add(t);
                                                            complications2Array = list.toArray(new String[0]);
                                                        }

                                                    }

                                                }

                                            }



                                            for (String c: complications2Array){
                                                if(c.equals("\"1\"")){
                                                    out.println("<li> মাসিক বন্ধ হয়ে যাওয়া </li>");
                                                }else if(c.equals("\"2\"")){
                                                    out.println("<li> ইমপ্লান্ট রড স্থাপনের জায়গায় সংক্রমণ </li>");
                                                }else if(c.equals("\"3\"")){
                                                    out.println("<li> তলপেটে প্রচন্ড ব্যাথা </li>");
                                                }else if(c.equals("\"4\"")){
                                                    out.println("<li> ইমপ্লান্টের কোন রড বেড় হয়ে আসা </li>");
                                                }else if(c.equals("\"5\"")){
                                                    out.println("<li> অতিরিক্ত রক্তস্রাব </li>");
                                                }else if(c.equals("\"6\"")){
                                                    out.println("<li> প্রচন্ড মাথা ব্যাথা বা চোখে ঝাপসা দেখা </li>");
                                                }
                                            }
    //                                    out.println(singleRow.get("complication"));
                                        %>
                                    </ul>
                                </td>
                                <td rowspan="4">
                                    <ul>
                                        <%
                                            String treatments2 = singleRow2.get("treatment").toString().replace("[","");
                                            treatments2 = treatments2.replace("]","");
                                            String[] treatments2Array = treatments2.split(",");

                                            if(secondRowData.length()>0){
                                                for(int k=0 ; k<secondRowData.length();k++){
                                                    JSONObject secondFollowUp = secondRowData.getJSONObject(k);
                                                    String followUp2treatments = secondFollowUp.get("treatment").toString().replace("[","");
                                                    followUp2treatments = followUp2treatments.replace("]","");
                                                    String[] temptreatment2Array = followUp2treatments.split(",");

                                                    for (String t: temptreatment2Array){
                                                        List<String> list = Arrays.asList(treatments2Array);
                                                        if(list.contains(t)){
                                                            System.out.println("Hello A");
                                                        }else {
                                                            list.add(t);
                                                            treatments2Array = list.toArray(new String[0]);
                                                        }

                                                    }

                                                }

                                            }



                                            for (String c: treatments2Array){
                                                if(c.equals("\"1\"")){
                                                    out.println("<li> Tab. Paracetamol 500 mg </li>");
                                                }else if(c.equals("\"2\"")){
                                                    out.println("<li> Tab. Ibuprofen 400 mg </li>");
                                                }else if(c.equals("\"3\"")){
                                                    out.println("<li> Tab. Iron Folic Acid </li>");
                                                }else if(c.equals("\"4\"")) {
                                                    out.println("<li> Other </li>");
                                                }
                                            }
    //                                    out.println(singleRow.get("complication"));
                                        %>
                                    </ul>
                                </td>
                                <td rowspan="4">
                                    <% out.println(singleRow2.get("implant_remover_reason"));%>
                                    <br>
                                    <% out.println(singleRow2.get("implant_remover_date"));%>
                                </td>
                                <td rowspan="4"><% out.println(singleRow2.get("method_name"));%></td>
                                <td rowspan="4">
                                    <% out.println(singleRow2.get("implantremovername"));%>
                                    <br>
                                    <% out.println(singleRow2.get("implant_remover_deg"));%>
                                </td>
                                <td rowspan="4"><% out.println(singleRow2.get("monitoringofficername"));%></td>
                            </tr>
                            <tr>
                                <td>২ য় </td>
                                <td><% out.println(singleRow2.get("second_fixed_date"));%></td>
                                <td>
                                    <%
                                        if(secondRowData.length()>0){
                                            JSONObject secondFollowUp = secondRowData.getJSONObject(0);
                                            secondFollowUp.put("followup_date",EnglishtoBangla.getEngToBanDate(secondFollowUp.get("followup_date").toString()));
                                            out.println(secondFollowUp.get("followup_date"));
                                        }

                                    %>
                                </td>
                                <td>
                                    <%
                                        if(secondRowData.length()>0){
                                            JSONObject secondFollowUp = secondRowData.getJSONObject(0);
                                            out.println(secondFollowUp.get("client_allowance"));
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(secondRowData.length()>0){
                                            JSONObject secondFollowUp = secondRowData.getJSONObject(0);
                                            out.println(secondFollowUp.get("attendantname"));
                                        }
                                    %>
                                </td>
                            </tr>
                            <tr>
                                <td>৩ য় </td>
                                <td><% out.println(singleRow2.get("third_fixed_date"));%></td>
                                <td>
                                    <%
                                        if(secondRowData.length()>1){
                                            JSONObject secondFollowUp = secondRowData.getJSONObject(1);
                                            secondFollowUp.put("followup_date",EnglishtoBangla.getEngToBanDate(secondFollowUp.get("followup_date").toString()));
                                            out.println(secondFollowUp.get("followup_date"));
                                        }

                                    %>
                                </td>
                                <td>
                                    <%
                                        if(secondRowData.length()>1){
                                            JSONObject secondFollowUp = secondRowData.getJSONObject(1);
                                            out.println(secondFollowUp.get("client_allowance"));
                                        }
                                    %>
                                </td>
                                <td>
                                    <%
                                        if(secondRowData.length()>1){
                                            JSONObject secondFollowUp = secondRowData.getJSONObject(1);
                                            out.println(secondFollowUp.get("attendantname"));
                                        }
                                    %>
                                </td>
                            </tr>
                            <tr>
							<td colspan="2"><p class="rotate"> অনিয়মিত  </p></td>
							<td></td>
							<td>
                                <%
                                    if(secondRowData.length()>2){
                                        for(int k=2 ; k<secondRowData.length();k++){
                                            JSONObject secondFollowUp = secondRowData.getJSONObject(k);
                                            secondFollowUp.put("followup_date",EnglishtoBangla.getEngToBanDate(secondFollowUp.get("followup_date").toString()));
                                            out.println(secondFollowUp.get("followup_date"));
                                        }

                                    }

                                %>
                            </td>
							<td></td>
						</tr>
                        <%  }%>
					</tbody>
				</table>
				<%
//                        if(i==json.length()){
//                            break;
//                        }
					}
                %>

			</div>
		</div>
	</div>
</div>


