<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 3/2/2021
  Time: 3:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
            </div>
        </div>


        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">
                    <!--geo location-->
                    <%@include file="/jsp/util/geoSection.jsp" %>
                    <!--/geo location--->

                    <!--facility info-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="facility_type" style="display: none;">
                                <label>Facility Type</label>
                                <select class="form-control select2" id="facilityType" style="width: 100%;">
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="facility_name" style="display: none;">
                                <label>Facility Name</label>
                                <select class="form-control select2" id="facilityName" style="width: 100%;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/facility info-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-6" id="mis3Type" style="display: none;">
                                <label>MIS3 View Type</label><br/>
                                <input type="radio" name="mis3ViewType" value="1" checked class="flat-red">&nbsp;&nbsp;&nbsp;পরিদর্শন
                                অনুযায়ী&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="mis3ViewType" value="3" class="flat-red">&nbsp;&nbsp;&nbsp;Submitted
                                MIS3
                            </div>
                        </div>
                    </div>
                    <!--date section-->
                    <%@include file="/jsp/util/date.jsp" %>
                    <!--/date section-->

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>


    <script>

        $(".select2").select2();

        $(document).ready(function () {
            $("#facilityType").empty();

            var output = "<option value ='' selected></option>";

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function (key, val) {
                output = output + '<option value = "' + key + '_' + val + '">' + val + '</option>';
            });

            $("#facilityType").append(output);
        });

        $("#RdivName, #RzillaName, #RupaZilaName").on('change', function () {
            $("#facilityName").empty();
        });

        $("#RunionName").on('change', function () {
            getFacilityInfo();
        });

        $("#facilityType").on('change', function () {
            getFacilityInfo();
        });

        function getFacilityInfo() {

            //alert("Zillaid="+ $('#RzillaName').val());
            //alert("Upazilaid="+ $('#RupaZilaName').val());

            var forminfo = new Object();
            forminfo.zillaid = $('#RzillaName').val();
            forminfo.upazilaid = $('#RupaZilaName').val();
            forminfo.unionid = $('#RunionName').val();
            if ($('#facility_type').is(':visible'))
                forminfo.facilityType = $('#facilityType').val().split("_")[0];
            else if ($('#provider_facility_type').is(':visible'))
                forminfo.facilityType = $('#providerFacility').val().split("_")[0];
            else
                forminfo.facilityType = 0;

            forminfo.type = "6";

            $.ajax({
                type: "POST",
                url: "getFacilityInfo",
                timeout: 60000, //60 seconds timeout
                data: {"forminfo": JSON.stringify(forminfo)},
                success: function (response) {
                    console.log(response);
                    facilityJS = JSON.parse(response);
                    $("#facilityName").empty();
                    var output = "";

                    $.each(facilityJS, function (key, val) {
                        output = output + "<option value = " + key + ">" + val + "</option>";
                    });

                    $("#facilityName").append(output);
                },
                complete: function () {
                    preventMultiCall = 0;
                },
                error: function (xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
        }
    </script>

</div>

