<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<style>
    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type="number"] {
        -moz-appearance: textfield;
    }
</style>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title" id="box_title"> Assign Additional Facility</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body search_form">
            <% if(request.getAttribute("type").equals("7")){ %>
            <h3>Select Provider & Provider's Current Location</h3>
            <% } else{ %>
           <!-- <h3>Select Provider</h3>-->
            <% } %>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group col-md-12" id="provider_type">
                        <label>Provider Type</label>
                        <select class="form-control select2" id="providerType" required="required">
                            <option value="">--Select Provider Type--</option>
                            <% if(request.getAttribute("type").equals("7")){ %>
                            <option value="20">DDFP</option>
                            <option value="14">MOMCH</option>
                            <option value="15">UFPO</option>
                            <% } %>
                            <option value="4">FWV</option>
                            <option value="5">S.A.C.M.O (FP)</option>
                            <option value="6">S.A.C.M.O (HS)</option>
                            <option value="101">PARAMEDIC</option>
                            <option value="17">Midwife</option>
                        </select>
                    </div>
					 <div class="form-group col-md-4">
                        <label>Division</label>
                        <select class="form-control select2" id="divID" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>District</label>
                        <select class="form-control select2" id="distID" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-4" id="upzName">
                        <label>Upazila</label>
                        <select class="form-control select2" id="upzID" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-4" id="unName">
                        <label>Union</label>
                        <select class="form-control select2" id="unID" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="Pname">
                        <label>Provider Name</label>
                        <select class="form-control select2" id="providerID" required="required">
                            <option value="">--Select Provider--</option>
                        </select>
                    </div>
                </div>
            </div>
            <% if(request.getAttribute("type").equals("7")){ %>
            <h3>Select Provider's Transfer Area</h3>
            <% } else{ %>
            <!--<h3>Select Additional SDP</h3>-->
			 <h4>Additional Facility</h4>
            <% } %>
            <div class="row">
                <div class="col-md-12">
				 <div class="form-group col-md-4">
                        <label>Division</label>
                        <select class="form-control select2" id="RdivName" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>District</label>
                        <select class="form-control select2" id="RzillaName" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-4" id="upazilaName">
                        <label>Upazila</label>
                        <select class="form-control select2" id="RupaZilaName" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-4" id="unionName">
                        <label>Union</label>
                        <select class="form-control select2" id="RunionName" required="required">
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="provider_facility_type">
                        <label> Facility Type</label>
                        <select class="form-control select2 changeFacility" id="providerFacility">
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="FacName">
                        <label>Facility Name</label>
                        <select class="form-control select2" id="facilityName" required="required">
                            <option value="">--Select Facility Name--</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6 transfer_date" id="TransferDate">
                        <label>
                            <% if(request.getAttribute("type").equals("7")){ %>
                            Transfer Date
                            <% } else{ %>
                            Started From
                            <% } %>
                        </label>
                        <br>
                        <input type="text" id="transfer_date" class="form-control pull-right" readonly />
                    </div>
                    <div class="box-footer col-md-12">
                        <input type="hidden" id="formType" value="<% out.print(request.getAttribute("type")); %>">
                        <input type="button" id="ProvEntryForm" class="btn btn-primary" value="Submit">
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>

    <div id="model_box"></div>

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="library/datepicker/datepicker.min.css">
    <!-- bootstrap datepicker -->
    <script src="library/datepicker/bootstrap-datepicker.min.js"></script>

    <script>

       loadDivision("#divID");
        //loadZilla("#distID");

		$(document).on("change", "#divID" ,function (){
            loadFilterZilla("#divID","#distID");
        });

        $(document).on("change", "#distID" ,function (){
            loadUpazila("#divID","#distID","#upzID");
        });

        $(document).on("change", "#upzID" ,function (){
            loadUnion("#divID","#distID","#upzID","#unID");
        });


        $(".select2").select2();

        function show_dpu(type)
        {
            if(type==1)
            {
                $("#upazilaName").hide();
                $("#unionName").hide();
                $("#provider_code").hide();
            }
            else if(type==2)
            {
                $("#upazilaName").show();
                $("#unionName").hide();
                $("#provider_code").hide();
            }
            else if(type==3)
            {
                $("#upazilaName").show();
                $("#unionName").show();
                $("#provider_code").hide();
            }
            else if(type==4)
            {
                $("#upazilaName").show();
                $("#unionName").show();
                $("#provider_code").show();
            }
        }

        $('#providerType').on('change', function(){
            if($('#providerType').val()=="20"){
                $("#upazilaName").hide();
                $("#unionName").hide();
                $("#provider_facility_type").hide();
                $("#FacName").hide();
                $("#upzName").hide();
                $("#unName").hide();
                $("#upzName").val("");
                $("#unName").val("");
            }
            else if($('#providerType').val()=="14" || $('#providerType').val()=="15"){
                $("#upazilaName").show();
                $("#unionName").hide();
                $("#provider_facility_type").hide();
                $("#FacName").hide();
                $("#upzName").show();
                $("#unName").hide();
                $("#unName").val("");
            }
            else{
                $("#upazilaName").show();
                $("#unionName").show();
                $("#provider_facility_type").show();
                $("#FacName").show();
                $("#upzName").show();
                $("#unName").show();
            }
        });

        $("#RzillaName").on('change', function () {
            if($("#RzillaName").val() == "99"){
                $("#upazilaName").hide();
                $("#unionName").hide();
                $("#provider_facility_type").hide();
                $("#FacName").hide();
            }
            else{
                if($('#providerType').val()=="20"){
                    $("#upazilaName").hide();
                    $("#unionName").hide();
                    $("#provider_facility_type").hide();
                    $("#FacName").hide();
                    $("#upzName").hide();
                    $("#unName").hide();
                    $("#upzName").val("");
                    $("#unName").val("");
                }
                else if($('#providerType').val()=="14" || $('#providerType').val()=="15"){
                    $("#upazilaName").show();
                    $("#unionName").hide();
                    $("#provider_facility_type").hide();
                    $("#FacName").hide();
                    $("#upzName").show();
                    $("#unName").hide();
                    $("#unName").val("");
                }
                else{
                    $("#upazilaName").show();
                    $("#unionName").show();
                    $("#provider_facility_type").show();
                    $("#FacName").show();
                    $("#upzName").show();
                    $("#unName").show();
                }
            }
        });

        $('#transfer_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        $( document ).ready(function() {
            $("#providerFacility").empty();

            var output = "<option value ='' selected></option>";

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + "<option value = " + key + "_" + val + ">" + val + "</option>";
            });

            $("#providerFacility").append(output);
        });

        $("#providerType, #distID, #upzID, #unID").on('change', function(){
            var forminfo = new Object();
            forminfo.zillaid = $('#distID').val();
            forminfo.upazilaid = $('#upzID').val();
            forminfo.unionid = $('#unID').val();
            forminfo.providerType = $('#providerType').val();
			
			if($('#distID').val() == null)
			{
			forminfo.zillaid = 0;
			}
			else
			{
				forminfo.zillaid = $('#distID').val();
			}
			
			if($('#upzID').val() == null)
			{
			forminfo.upazilaid = 0;
			}
			else
			{
				forminfo.upazilaid = $('#upzID').val();
			}
			
			if($('#unID').val() == null)
			{
			forminfo.unionid = 0;
			}
			else
			{
				forminfo.unionid = $('#unID').val();
			}

            if($('#distID').val() != null) {
                $.ajax({
                    type: "POST",
                    url: "getProviderInfo",
                    timeout: 60000, //60 seconds timeout
                    data: {"forminfo": JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        facilityJS = JSON.parse(response);
                        $("#providerID").empty();
                        var output = "";
                        $.each(facilityJS, function (key, val) {
                            output = output + "<option value = " + key + ">" + val + "(" + key + ")" + "</option>";
                        });

                        $("#providerID").append(output);
                    },
                    complete: function () {
                        preventMultiCall = 0;
                    },
                    error: function (xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });
            } else { return false; }
        });

        $(".changeFacility").on('change', function(){
            if($('#FacName').is(':visible')){
                var forminfo = new Object();
                forminfo.zillaid = $('#RzillaName').val();
                forminfo.upazilaid = $('#RupaZilaName').val();
                forminfo.unionid = $('#RunionName').val();
                if($('#facility_type').is(':visible'))
                    forminfo.facilityType = $('#facilityType').val().split("_")[0];
                else if($('#provider_facility_type').is(':visible'))
                    forminfo.facilityType = $('#providerFacility').val().split("_")[0];
                forminfo.type = "6";

                $.ajax({
                    type: "POST",
                    url:"getFacilityInfo",
                    timeout:60000, //60 seconds timeout
                    data:{"forminfo":JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        facilityJS = JSON.parse(response);
                        $("#facilityName").empty();
                        var output = "<option value ='' selected></option>";

                        $.each(facilityJS, function(key, val) {
                            output = output + "<option value = " + key + ">" + val + "</option>";
                        });

                        $("#facilityName").append(output);
                    },
                    complete: function() {
                        preventMultiCall = 0;
                    },
                    error: function(xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });
            }
        });

    </script>

</div>



