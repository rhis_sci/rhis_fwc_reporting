<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 1/25/2023
  Time: 10:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
            </div>
        </div>


        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->

            <!--geo location-->
            <%@include file="/jsp/util/geoSection.jsp" %>
            <!--<div class="row">-->
            <!--<div class="col-md-12">-->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group col-md-4" id="ageRange">
                        <label>Search Option</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <select  class="form-control select2" id="searchType" >
                            <option value="0">healthid</option>
                            <option value="1">NRCid</option>
                            <option value="2">mobileNo</option>
                            <option value="3">Name</option>
                            <option value="4">Age</option>

                        </select>
                    </div>


                </div>
            </div><!--/row -->


            <!--/geo location--->
            <%@include file="/jsp/util/submitButton.jsp" %>

            <!--</div> -->
            <!-- </div>-->
        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>

    <script>
        $(".select2").select2();
    </script>
</div>
