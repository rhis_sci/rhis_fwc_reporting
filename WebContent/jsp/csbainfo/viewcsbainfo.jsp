<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 10/16/2022
  Time: 11:26 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title">Add CSBA</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">
                    <!--geo group-->
                    <%@include file="/jsp/util/geoSection.jsp" %>

                    <div class="form-group col-md-3">
                        <label>version</label>
                        <input type="text" class="form-control" id="version_no">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Has community module  &nbsp;</label><br>
                        <input type="checkbox" style="height: 30px;" id="community_active" name="info1_type" value="1" checked="1">

                    </div>
                    <div class="box-footer col-md-12">
                        <input type="hidden" id="formType" value="<% out.print(request.getAttribute("type")); %>">
                        <input type="button" id="ProvEntryForm" class="btn btn-primary" value="Submit">
                    </div>


                    <!--/geo group-->
                    <!--date section-->


                </div>
            </div>
        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>

    <script>

        $(".select2").select2();
    </script>
</div>

