<!DOCTYPE html>
<%
	int zillaCount = (Integer)request.getAttribute("countAllZilla");
	int upazilaCount = (Integer)request.getAttribute("countAllUpazilla");

%>

<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!--<script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous"></script>
     Bootstrap
    <link href="dashboard/bootstrap-3.3.7/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="dashboard/bootstrap-3.3.7/js/bootstrap-toggle.min.js"></script>-->

	<!-- Leaflet -->
	<link rel="stylesheet" href="dashboard/leaflet/leaflet.css" />
	<link rel="stylesheet" href="dashboard/dashboard.css" />
	<script src="dashboard/leaflet/leaflet.js"></script>

	<!-- Chart JS -->
	<script src="dashboard/chartjs/chart.min.js"></script>
	<script>
		months = [
			{id:1,name: 'January'},
			{id:2,name: 'February'},
			{id:3,name: 'March'},
			{id:4,name: 'April'},
			{id:5,name: 'May'},
			{id:6,name: 'June'},
			{id:7,name: 'July'},
			{id:8,name: 'August'},
			{id:9,name: 'September'},
			{id:10,name: 'October'},
			{id:11,name: 'November'},
			{id:12,name: 'December'}
		];

	</script>
</head>
<body>
<div id="pre-loader" class="hide">
	<img src="image/dashboard/preloader.gif" id="preloader-image">
	<div id="preloader-text">Loading...</div>
</div>
<div  style="margin-top: 60px;">
	<!-- heading area -->
	<div class="panel panel-default panel-site-title" >
		<div class="panel-body title-panel-body">


			<div class="col-xs-8" >
				<h3 class="district-title" id="area-name"></h3>
			</div>

			<div class="col-xs-2">
				<div class="form-group">
					<select class="form-control input-sm hilight" id="current-year" ></select>
				</div>
			</div>

			<div class="col-xs-2">
				<div class="form-group">
					<select class="form-control input-sm hilight" id="current-month" ></select>
				</div>
			</div>


		</div>
	</div>
	<!-- /Heading area -->

</div>

<div class="row" >
	<div class="col-md-5" >
		<div class="bread-crumb">
			<ul id="bread-crumb" class="breadcrumb"></ul>
		</div>

		<div class="panel panel-default panel-map">

			<div class="panel-body">
				<div id="map-container" style="height: 500px" ></div>
			</div>
		</div>

		<div class="col-md-12" >
			<!-- Map Animation
             <input type="checkbox" data-toggle="toggle" id="map-anim-switch" data-size="mini"> -->

			<h4 style="margin-bottom: 15px; color: #0d6aad; cursor: pointer" data-toggle="collapse" data-target="#FAQ-collaps" aria-expanded="false" aria-controls="FAQ-collaps">How to navigate Map?</h4>
			<div class="collapse" id="FAQ-collaps">
				<div class="well">
					<ul>
						<li>Tap/Click on map to get specific information.</li>
						<li>Double Tap/Click on map to get into upazila and unions.</li>
						<li>Pinch to zoom in/out</li>
					</ul>
				</div>
			</div>




		</div>
	</div>

	<!--  right panel -->
	<div class="col-md-7">
		<div class="row">
			<div class="col-lg-12">

				<!-- cards -->
				<div class="cards-containers">
					<div class="cards cards-primary">
						<div class="cards-heading">Estimated Population</div>
						<div class="cards-icon"><img src="image/dashboard/icons/total_population.png"></div>
						<div class="cards-body" id="cards-population"></div>

					</div>

					<div class="cards cards-secondary">
						<div class="cards-heading">Delivery</div>
						<div class="cards-icon"><img src="image/dashboard/icons/mnh.png"></div>
						<div class="cards-body" id="cards-delivery"></div>
					</div>

					<div class="cards cards-third">
						<div class="cards-heading">eMIS Coverage</div>
						<div class="cards-icon"><img src="image/dashboard/icons/earth.png"></div>
						<div class="cards-body" id="cards-emis-coverage">
							<%= zillaCount %>/<%= upazilaCount %>
						</div>
						<div class="cards-footer">[Zilla / Upazila]</div>
					</div>


				</div>
				<!-- /cards -->

			</div><!--  /col-md-12 -->

			<div class="col-lg-6">
				<div class="panel panel-default population-panel">
					<div class="panel-heading">
						<div class="panel-title"><img src="image/dashboard/icons/total_population.png" class="icons" > Estimated Population</div>
					</div>

					<div class="panel-body" >
						<!-- TABS -->
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#pop-graph" aria-controls="pop-graph" role="tab" data-toggle="tab" >
								<span class="glyphicon glyphicon-stats"></span> Chart View
							</a></li>
							<li role="presentation"><a href="#pop-tabular" aria-controls="pop-tabular" role="tab" data-toggle="tab" >
								<span class="glyphicon glyphicon-calendar"></span> Tabular View
							</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content tab-body">
							<div role="tabpanel" class="tab-pane active" id="pop-graph">
								<div id="pop-chart-container">
									<canvas id="pop-chart" ></canvas>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane " id="pop-tabular">
								<table class="table table-bordered tbl-hover white_table tbl-hover" width="100%" >
									<tr>
										<td width="70%">Male</td>
										<td id="pop-male" align="right"></td>
									</tr>
									<tr>
										<td>Female</td>
										<td id="pop-female" align="right"></td>
									</tr>
									<tr>
										<td >Total Population</td>
										<td id="pop-total" align="right"></td>
									</tr>
									<tr>
										<td>Elco</td>
										<td id="pop-elco" align="right"></td>
									</tr>

								</table>
							</div>

						</div>
						<!-- /Tabs -->
					</div>
				</div><!-- /panel -->

				<!-- eMIS Coverage -->
				<!-- hide for test
                <div class="panel panel-default emis-coverage-panel">
                     <div class="panel-heading">
                         <div class="panel-title"><img src="image/dashboard/icons/earth.png" class="icons" > Total eMIS Coverage</div>
                     </div>

                     <div class="panel-body" id="emis-coverage">

                     </div>
                 </div>
                  /panel -->
				<!-- //eMIS Coverage -->

				<!-- Facility Assessment -->
				<div class="panel panel-default facility-assessment">
					<div class="panel-heading">
						<div class="panel-title">Facility Assessment</div>
					</div>

					<div class="panel-body">

						<!-- TABS -->
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#fa-graph" aria-controls="fa-graph" role="tab" data-toggle="tab" >
								<span class="glyphicon glyphicon-stats"></span> Chart View
							</a></li>
							<li role="presentation"><a href="#fa-tabular" aria-controls="fa-tabular" role="tab" data-toggle="tab" >
								<span class="glyphicon glyphicon-calendar"></span> Tabular View
							</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content tab-body">
							<div role="tabpanel" class="tab-pane active" id="fa-graph">
								<div id="fa-canvas-container">
									<canvas id="fa-canvas" ></canvas>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane " id="fa-tabular">
								<table class="table table-bordered tbl-hover white_table tbl-hover" width="100%" >
									<tr>
										<td width="70%">Category A</td>
										<td align="right" id="cat-a"></td>
									</tr>

									<tr>
										<td>Category B</td>
										<td align="right" id="cat-b"></td>
									</tr>

									<tr>
										<td>Category C</td>
										<td align="right" id="cat-c"></td>
									</tr>

									<tr>
										<td>Total</td>
										<td align="right" id="cat-total"></td>
									</tr>
								</table>
							</div>

						</div>
						<!-- /Tabs -->
					</div>
				</div>
				<!-- end of  /Facility Assessment -->

				<!-- GP -->
				<div class="panel panel-default gp-panel">
					<div class="panel-heading">
						<div class="panel-title"><img src="image/dashboard/icons/patient.png" class="icons" > General Patient</div>
					</div>

					<div class="panel-body">
						<table class="table table-bordered tbl-hover white_table tbl-hover" width="100%" border="0">
							<tr>
								<td width="70%">Male</td>
								<td align="right" id="gp-male"></td>
							</tr>
							<tr>
								<td>Female</td>
								<td align="right" id="gp-female"></td>
							</tr>
							<tr>
								<td>Total</td>
								<td align="right" id="gp-total"></td>
							</tr>
						</table>
					</div>
				</div><!-- /panel -->
				<!-- /GP -->



			</div><!--  /col-md-6 -->


			<div class="col-lg-6">
				<!--<div class="delivery-well" >
                    <img src="image/dashboard/icons/mnh.png" class="icons" style="width: 30px;height: 30px;margin-right: 0px;" >
                    <h3 style="display: inline-block;">Delivery <span class="label label-primary" id="mnhc_delivery"></span></h3>
                </div> -->

				<!-- MNH -->
				<div class="panel panel-default mnh-panel">
					<div class="panel-heading">
						<div class="panel-title"><img src="image/dashboard/icons/mnh.png" class="icons"  > MNH (Maternal & Newborn Health)</div>
					</div>

					<div class="panel-body">

						<!-- TABS -->
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#mnh-graph" aria-controls="mnh-graph" role="tab" data-toggle="tab">
								<span class="glyphicon glyphicon-stats"></span> Chart View
							</a></li>
							<li role="presentation"><a href="#mnh-tabular" aria-controls="mnh-tabular" role="tab" data-toggle="tab" >
								<span class="glyphicon glyphicon-calendar"></span> Tabular View
							</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content tab-body">
							<div role="tabpanel" class="tab-pane active" id="mnh-graph">
								<div id="mnh-canvas-container">
									<canvas id="mnh-canvas" ></canvas>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane " id="mnh-tabular">
								<table width="100%" border="0" class="table table-bordered tbl-hover white_table tbl-hover">
									<thead>
									<tr>
										<td width="30%"></td>
										<td align="center" width="15%">Visit 1</td>
										<td align="center" width="15%">Visit 2</td>
										<td align="center" width="15%">Visit 3</td>
										<td align="center" width="15%">Visit 4</td>
									</tr>
									</thead>
									<tr>
										<td>ANC</td>
										<td id="mnhc_anc1" align="right" ></td>
										<td id="mnhc_anc2" align="right"></td>
										<td id="mnhc_anc3" align="right"></td>
										<td id="mnhc_anc4" align="right"></td>
									</tr>
									<tr>
										<td>PNC (M) </td>
										<td id="mnhc_pnc1m" align="right"></td>
										<td id="mnhc_pnc2m" align="right"></td>
										<td id="mnhc_pnc3m" align="right"></td>
										<td id="mnhc_pnc4m" align="right"></td>
									</tr>
									<tr>
										<td>PNC (N)</td>
										<td id="mnhc_pnc1n" align="right"></td>
										<td id="mnhc_pnc2n" align="right"></td>
										<td id="mnhc_pnc3n" align="right"></td>
										<td id="mnhc_pnc4n" align="right"></td>
									</tr>
								</table>
							</div>

						</div>
						<!-- /Tabs -->
					</div>
				</div><!-- panel -->
				<!-- /MNH -->

				<!-- FP -->
				<div class="panel panel-default fp-panel">
					<div class="panel-heading">
						<div class="panel-title"><img src="image/dashboard/icons/family.png" class="icons" > Family Planning</div>
					</div>

					<div class="panel-body">
						<!-- TABS -->
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#fp-graph" aria-controls="fp-graph" role="tab" data-toggle="tab">
								<span class="glyphicon glyphicon-stats"></span> Chart View
							</a></li>
							<li role="presentation"><a href="#fp-tabular" aria-controls="fp-tabular" role="tab" data-toggle="tab">
								<span class="glyphicon glyphicon-calendar"></span> Tabular View
							</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content tab-body">
							<div role="tabpanel" class="tab-pane active" id="fp-graph">
								<div id="fp-canvas-container">
									<canvas id="fp-canvas" ></canvas>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane " id="fp-tabular">
								<table class="table table-bordered tbl-hover white_table tbl-hover" width="100%" border="0">
									<tr>
										<td>Condom</td>
										<td id="fp_condom" align="right"></td>
									</tr>
									<tr>
										<td width="70%">Pill</td>
										<td id="fp_pill" align="right"></td>
									</tr>

									<tr>
										<td>Injectable</td>
										<td id="fp_injectable" align="right"></td>
									</tr>

									<tr>
										<td>IUD</td>
										<td id="fp_iud" align="right"></td>
									</tr>

									<tr>
										<td>Implant</td>
										<td id="fp_implant" align="right"></td>
									</tr>

									<tr>
										<td>IUD Followup</td>
										<td id="fp_iud_followup" align="right"></td>
									</tr>

									<tr>
										<td>Implant Followup</td>
										<td id="fp_implant_followup" align="right"></td>
									</tr>
								</table>
							</div>

						</div>
						<!-- /Tabs -->



					</div>
				</div><!-- panel -->
				<!-- /FP -->



			</div><!--  /col-md-6 -->

		</div><!-- /row -->
	</div>
</div>

<script src="dashboard/js/leflet-map.js"></script>
<script src="dashboard/js/map_data_handler.js"></script>
<script>

	function event_mapChange(){
		tname = breadCrumb[breadCrumb.length-1].name;

		if(tname == "Home")
		{
			tname = "Bangladesh";
			$("#area-name").html(tname);
		}
	}

	$(function() {
		$('#map-anim-switch').change(function() {
			//console.log('Toggle: ' + $(this).prop('checked'))

			if($(this).prop('checked') == true)
				map_interval = setInterval(map_animation_controller,15000);

			if($(this).prop('checked') == false)
				clearInterval(map_interval);

		})
	})

	$(document).ready(function () {
		var cd = new Date();
		var month = cd.getMonth()+1;
		var year = cd.getFullYear();

		$("#map-container-button").click(function () {
			$("#map-container").show();
		});

		for (i=2016; i<=year; i++){
			if(year == i)
				selected = 'selected="selected"';
			else
				selected = "";

			$('#current-year').append('<option value="'+i+'" '+selected+'>'+i+'</option>');
		}

		//Current Month
		$.each(months,function (index, val) {
			if(month == val.id)
				selected = 'selected="selected"';
			else
				selected = "";

			$('#current-month').append('<option value="'+val.id+'" '+selected+'>'+val.name+'</option>');
		});

		//This function is working on any changed of year and month on Dashboard
		$("#current-month, #current-year").on('change', function (){
			//Set initial map parameter
			var map_parameters ={
				zilla : 0,
				upazila : 0,
				union : 0,
				methodType:0
			};
			//Remove previous map layer
			remove_previous_map_layer();
			//Remove marker like facility marker
			removeMarker();
			//hide Facility Assessment
			showHideFacilityAssessment(0);
			//Load the map
			getMap(map_parameters);
			//Set Dashboard heading title as Bangladesh
			$("#area-name").html("Bangladesh");
			//Set the Map Breadcrumb empty
			$("#bread-crumb").empty();
			//Set the Map Breadcrumb as Home
			$("#bread-crumb").append("<li data-zillaid="+0+" data-upazilaid="+0+">Home</li>");
			//Load the Dashboard data
			load_dashboard();
		});



		//Automatic collapse sidebar
		//$("#emis-site-body").removeClass('sidebar-mini').addClass('sidebar-collapse');
	});
</script>

</body>
</html>