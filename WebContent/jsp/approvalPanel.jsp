<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="library/datepicker/datepicker.min.css">

<style>
	.input-daterange input,#monthSelect{
		background: url('image/calendar.png') no-repeat;
		background-size: 30px 30px;
		padding-left: 30px;
		text-align: center;
		background-color: #ffffff!important;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

	body {
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
</style>
<div class="remove_div">
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title" id="box_title"></h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body search_form table-responsive">
			<div class="row">
				<div class="col-md-12">
					<%
						String approvalType = (String) request.getAttribute("type");
						if(!approvalType.equals("1") && !approvalType.equals("3")){
					%>
					<div class="form-group col-md-4" id="districtName">
						<label>District</label>
						<select class="form-control select2" id="RzillaName" style="width: 100%;">
						</select>
					</div>
					<div class="form-group col-md-4" id="upazilaName">
						<label>Upazila</label>
						<select class="form-control select2" id="RupaZilaName" style="width: 100%;">
						</select>
					</div>
					<div class="form-group col-md-4" id="facility_name">
						<label>Facility Name</label>
						<select class="form-control select2" id="facilityName" style="width: 100%;">
						</select>
					</div>
					<div class="form-group date col-md-2 month_wise">
						<label class="control-label">Month</label>
						<input type="text" id="monthSelect" class="form-control" readonly>
					</div>
					<div class="box-footer col-md-12">
						<input type="button" id="submitApproval" class="btn btn-primary" value="Submit">
					</div>

					<% } else if(approvalType.equals("1") || approvalType.equals("3")){ %>
					<div class="form-group date col-md-2 month_wise">
						<label class="control-label">Month</label>
						<input type="text" id="monthSelect" value="<% out.print(request.getAttribute("month")); %>" class="form-control" readonly>
					</div>
					<div class="box-footer col-md-12" id="approvalList">
					<% out.print(request.getAttribute("approvalList")); %>
					</div>
					<% } %>
					<input type="hidden" id="approvalType" value="<% out.print(request.getAttribute("type")); %>">
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>

	<div class="row" id="table_row" style="display:none;">
		<div class="col-md-12">
			<%int planningid = 0;   %>
            <%String upazilaid = "";   %>
            <%String zillaid = ""; %>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title" id="table_title"></h3>
				</div>
				<div class="form-group col-md-4" id="comment_txt" style="display: none;margin-bottom: 0;padding-top: 1em;" title="Tell us the reason why you want to reject">
							<textarea id="reject_comment" cols="48" rows="3" style="margin-top: 0.5em;"></textarea>

					<%--<label>Helpline</label>--%>
					<%--<input type="text" class="form-control" id="comment_box" placeholder="Helpline">--%>
				</div>
				<div class="box" id="satelite_box" style="text-align: right;padding-right: 19em; display: none;">
					<input type='button' id='approveSatelite' class='btn btn-success' value='Approve'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id='reject' class='btn btn-danger' value='Reject'><br>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding" id="table_append">
					<div id="sateliteCalendar" style="display: none;">
						<div id='calendar'></div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="model_box" title="Basic dialog"></div>

	<!-- bootstrap datepicker -->
	<script src="library/datepicker/bootstrap-datepicker.min.js"></script>

	<script>


        $(document).on("click", '#approveSatelite' ,function (){approveSatelite(1)});
        $(document).on("click", '#reject' ,function (){
            approveSatelite(2);

        });

        var table = $('#reportApprovalDataTable').DataTable( {
            //responsive: true
        });
        
        $(".select2").select2();		

        $('#monthSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            minViewMode: 1,
            format: 'yyyy-mm'
        });

        $("#monthSelect").on("change", function() {
            loadGif()

            var utype = ($("#acc_ty").val()).trim();

            var infoApprovalObj = new Object();
            infoApprovalObj.utype = ($("#acc_ty").val()).trim();
            infoApprovalObj.supervisorId = ($("#ProviderID").text()).trim();
            infoApprovalObj.type = $("#approvalType").val();
            infoApprovalObj.month = $('#monthSelect').val();

            $.ajax({
                type: "POST",
                url:"approval",
                timeout:300000, //60 seconds timeout
                data:{"infoApproval":JSON.stringify(infoApprovalObj)},
                success: function (response) {
                    $(".remove_div").remove();
                    $( "#append_report" ).html(response);
                    //$("#approvalType").val(1);

					if(utype=="14"){
						$( "#districtName" ).hide();
						$( "#upazilaName" ).hide();
					}

                    var months_value = {'01':'Jan', '02':'Feb', '03':'Mar', '04':'Apr', '05':'May', '06':'Jun', '07':'Jul', '08':'Aug', '09':'Sep', '10':'Oct', '11':'Nov', '12':'Dec'};
                    var month_value = months_value[$('#monthSelect').val().split("-")[1]];
                    var year_value = $('#monthSelect').val().split("-")[0];

                    if($("#approvalType").val() == 3){
                        $("#head_title").html('Satelite<span> Approval</span>');
                        $("#box_title").html('<strong>Satelite </strong>Approval ('+month_value+', '+year_value+')');

					}else {

                        $("#head_title").html('MIS3<span> Approval</span>');
                        $("#box_title").html('<strong>MIS3 </strong>Approval (' + month_value + ', ' + year_value + ')');
                    }

                    var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                    var bottom = $el.position().top + $el.outerHeight(true);
                    $(".main-sidebar").css('height',bottom+"px");

                    loadDivision("#RdivName");
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
            //alert(preventMultiC);
            return preventMultiCall;
        });
        
        function getFacilityInfo(supervisorId){
			var forminfo = new Object();
			forminfo.zillaid = $('#RzillaName').val();
			forminfo.upazilaid = $('#RupaZilaName').val();
			forminfo.unionid = $('#RunionName').val();
			forminfo.facilityType = "0";
			forminfo.supervisorId = supervisorId;
			//console.log(forminfo);
			$.ajax({
	           type: "POST",
	           url:"getFacilityInfo",
	           timeout:60000, //60 seconds timeout
	           data:{"forminfo":JSON.stringify(forminfo)},
	           success: function (response) {
	        	   //console.log(response);
	        	   facilityJS = JSON.parse(response);
	        	   
	        	   $("#facilityName").empty();
					var output = "";
					
					$.each(facilityJS, function(key, val) {
						output = output + "<option value = " + key + ">" + val + "</option>"; 			
					});						
					
					$("#facilityName").append(output);
	           },
	           complete: function() {
	        	   preventMultiCall = 0;
	           },
	           error: function(xhr, status, error) {
                   checkInternetConnection(xhr)
	           }
			});
		}
		
		$("#RupaZilaName").on('change', function(){
				getFacilityInfo("0");		
		});
		
		$( document ).ready(function() {
			if(($("#acc_ty").val()).trim()=="14"){
				getFacilityInfo($("#ProviderID").text());
			}
		});

        function approveSatelite(approvestatus){
            var infoApprovalObj = new Object();
            infoApprovalObj.utype = ($("#acc_ty").val()).trim();
            infoApprovalObj.supervisorId = ($("#ProviderID").text()).trim();
            infoApprovalObj.type = $("#approvalType").val();
            infoApprovalObj.month = $('#monthSelect').val();
            infoApprovalObj.approve_status = approvestatus;
            infoApprovalObj.planning_id = planningid;
            infoApprovalObj.upazila_id = upazilaid;
            infoApprovalObj.zillaid = zillaid;

			var ajxCall =  function () {
            $.ajax({
                type: "POST",
                url:"approval",
                timeout:300000, //60 seconds timeout
                data:{"infoApproval":JSON.stringify(infoApprovalObj)},
                success: function (response) {
                    if(!(approvestatus==null)) {
                        if (approvestatus === 1)
                            callDialog("You have approved satelite session planning");
                    }
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });};

            //reject interceptor
            if(approvestatus===2){
                $("#comment_txt").dialog({
                    resizable: false,
                    // height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Ok": function () {
                            // infoApprovalObj.reject = 1;
                            infoApprovalObj.reject_comment = $("#reject_comment").val();
                            $(this).dialog("close");
                            ajxCall();
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });

            }else {
                ajxCall()
			}


            //alert(preventMultiC);
            return preventMultiCall;

        }
        function viewMIS3(zilla_id, facility_id, mis3_year_month){

            loadGif();

            //var preventMultiC = 1;
            var reportObj = new Object();
            reportObj.report1_zilla = zilla_id;
            reportObj.utype = ($("#acc_ty").val()).trim();
            reportObj.facilityId = facility_id;
            reportObj.supervisorId = ($("#ProviderID").text()).trim();
            reportObj.report1_month = mis3_year_month;
            reportObj.approvalType = $("#approvalType").val();

            //console.log(reportObj);
            $.ajax({
                type: "POST",
                url:"submittedApprovalList",
                timeout:60000, //60 seconds timeout
                data:{"approvalInfo":JSON.stringify(reportObj)},
                success: function (response) {
                    //alert(response);
                    $(".box-default").addClass("collapsed-box");
                    $(".search_form").hide();
                    $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

                    $("#reportTable").remove();
                    $("#table_row").show();
                    $("#table_append" ).html(response);

                    $("#exportOption").hide();

                    var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                    var bottom = $el.position().top + $el.outerHeight(true);
                    $(".main-sidebar").css('height',bottom+"px");
                    //alert(bottom);

                    $('#table_row')[0].scrollIntoView( true );
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
            //alert(preventMultiC);
            return preventMultiCall;
        }

        function daysInMonth (month, year) {
            return new Date(year, month, 0).getDate();
        }

        function twoDigit(n){
            return n > 9 ? "" + n: "0" + n;
        }

        function dhis2Submit(zilla_id, upazila_id, facility_id, mis3_year_month) {
            loadGif();

            var dhis2 = new Object();
            dhis2.report1_zilla = zilla_id;
            dhis2.report1_upazila = upazila_id;
            dhis2.facilityId = facility_id;
            var year_month = mis3_year_month.split("-");
            dhis2.report1_start_date = year_month[0]+"-"+twoDigit(year_month[1])+"-01";
            dhis2.report1_end_date = year_month[0]+"-"+twoDigit(year_month[1])+"-"+daysInMonth(year_month[1],year_month[0]);
            dhis2.dhis2 = "1";

            $.ajax({
                contentType: 'application/json',
                type: "POST",
                url:"http://mchdrhis.icddrb.org:8091/emis2dhis2/interoperability/api/push-mis3/",
                timeout:60000, //60 seconds timeout
                data:'{"data":'+JSON.stringify(dhis2)+'}',
                dataType: "json",
                success: function (response) {
                    console.log(response.message.status);
                    if(response.message.status == "SUCCESS"){
                        dhis2SubmitMIS3Report("4", facility_id, zilla_id, mis3_year_month);
                    }
                    else{
                        $('#model_box').dialog({
                            modal: true,
                            title: "Message",
                            width: 420,
                            open: function () {
                                $(this).html("<b style='color:red'>Data could not submit to DHIS2 system. Please try again later.</b>");
                            },
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
        }

        function dhis2SubmitMIS3Report(submission_status, facility_id, zilla_id, mis3_year_month){
            var url = "";
            var getParams = "facilityId="+facility_id+"&report1_zilla="+zilla_id+"&report1_start_date="+mis3_year_month+"&submissionStatus="+submission_status+"&supervisorComment=";
            var params = $.base64.encode(getParams);
            url = "MIS3Submit?"+params;

            $.ajax({
                type: "GET",
                url:url,
                timeout:60000, //60 seconds timeout
                success: function (response) {
                    if(response=='true'){
                        $("#model_box").dialog({
                            modal: true,
                            title: "Message",
                            width: 420,
                            open: function () {
                                $(this).html("<b style='color:green'>Your MIS3 has been submitted to DHIS2 successfully.</b>");
                            },
                            buttons : {
                                Ok : function() {
                                    $(this).dialog('close');
                                    //reloadReport();
                                    $( ".approval1" ).trigger( "click" );
                                }
                            }
                        });
                    }
                    else{
                        $('#model_box').dialog({
                            modal: true,
                            title: "Message",
                            width: 420,
                            open: function () {
                                $(this).html("<b style='color:red'>Your MIS3 not submitted to DHIS2 successfully. Please re-submit your MIS3 to DHIS2.</b>");
                            },
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
        }

        function viewSatelitePlanning(zilla_id, upazila_id, planning_id, year){

            $(document).ajaxStart(function () {//alert('hi');
                $('#loading_div').html("<img src='image/loading.gif' />");
                $('#loading_div').show();
            }).ajaxStop(function () {
                $('#loading_div').html("");
                $('#loading_div').hide();
            });

            $(".box-default").addClass("collapsed-box");
            $(".search_form").hide();
            $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

            $("#reportTable").remove();
            $("#table_row").show();
			//for specific month selection approve,reject pannel should show
			$("#satelite_box").show();
            $("#sateliteCalendar" ).show();

            //var preventMultiC = 1;
			planningid = planning_id;
            upazilaid = upazila_id
            zillaid = zilla_id;
            var reportObj = new Object();
            reportObj.zilla = zilla_id;
            reportObj.upazila = upazila_id;
            reportObj.utype = ($("#acc_ty").val()).trim();
            reportObj.planningId = planning_id;
            reportObj.supervisorId = ($("#ProviderID").text()).trim();
            reportObj.approvalType = $("#approvalType").val();

            var all_events = (function() {
                var events = [];
                $.ajax({
                    async: false,
                    type: "POST",
                    url:"submittedApprovalList",
                    timeout:60000, //60 seconds timeout
                    dataType: "json",
                    data:{"approvalInfo":JSON.stringify(reportObj)},
                    success: function(response) {

                        $.each(response, function(i, item) {
                            events.push({
                                title: item.title,
                                start: item.start, // will be parsed
                                fwa: item.fwa,
                                village: item.village,
                                sateliteDate: item.start
                            })
                        })
                    }
                });
                return events;
            })();

            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();

            var current_date = all_events.length > 0 ? all_events[0].start: (year + '-' +
                ((''+month).length<2 ? '0' : '') + month + '-' +
                ((''+day).length<2 ? '0' : '') + day);

			$('#calendar').remove();
			$('#sateliteCalendar').append("<div id='calendar'></div>")

            $('#calendar').fullCalendar({
				eventClick: function(calEvent, jsEvent) {
					var message = "<b>Satelite Name:</b> "+calEvent.title+"<br /><b>FWA Name:</b> "+calEvent.fwa+"<br /><b>Village Name:</b> "+calEvent.village+"<br /><b>Satelite Date:</b> "+calEvent.sateliteDate;
					$('#model_box').dialog({
						modal: true,
						title: "Message",
						width: 420,
						open: function () {
							$(this).html(message);
						},
						buttons: {
							Ok: function () {
								$(this).dialog("close");
							}
						}
					});
				},
                defaultDate: current_date,
                // editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: all_events
            });

            $("#exportOption").hide();

            var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
            var bottom = $el.position().top + $el.outerHeight(true);
            $(".main-sidebar").css('height',bottom+"px");

            $('#table_row')[0].scrollIntoView( true );

            return preventMultiCall;
        }
		

	</script>

</div>
        
        
        
