<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="org.json.JSONObject" %>

		<style>
			input[type="number"]::-webkit-outer-spin-button,
			input[type="number"]::-webkit-inner-spin-button {
			    -webkit-appearance: none;
			    margin: 0;
			}
			input[type="number"] {
			    -moz-appearance: textfield;
			}

			.select2-container--default.select2-container--focus .select2-selection--multiple {
				border: 1px solid #333 !important;
			}
			.select2 .select2-container .select2-container--default .select2-container--focus {
				border: 1px solid #333 !important;
			}
			.select2-container--default .select2-selection--multiple {
				border: 1px solid #333 !important;
			}
			.select2-search__field {
				background-color: white !important;
			}
			.select2-dropdown .select2-dropdown--below {
				border: 1px solid #333 !important;
			}
			.select2-results {
				background-color: white;
				height: 100px;
				color: black;
			}
			.select2-results__options {
				height: 100px;
			}
		</style>
		<%
        //out.println(request.getAttribute("jsonString"));
        //out.println(request.getAttribute("Result.amtsl"));

        String jsondata = request.getAttribute("jsonString").toString();
        JSONObject jsonObj = new JSONObject(jsondata);
       
	   %>

	<div class="row" id="provider_div">
		<div class="col-md-12">
			<div id="profile_update_info"></div>
			<div class="form-group col-md-6" id="Pname">
				<label>Name</label>
				<input type="hidden" name="providercode" id="providercode" value="<%= jsonObj.getString("providercode") %>" />
				<input type="hidden" name="providertype" id="providertype" value="<%= jsonObj.getString("providerType") %>" />
				<input type="hidden" name="zillaid" id="zillaid" value="<%= jsonObj.getString("zillaid") %>" />
				<input type="hidden" id="upazilaid" value="<%= jsonObj.getString("upazilaid") %>" />
				<input type="text" class="form-control" id="editProvName" required="required" value="<%= jsonObj.getString("providerName") %>">
				<div id="provname_validate" align="left"></div>
			</div>

			<div class="form-group col-md-6" id="Pmob">
				<label>Mobile No.</label>
				<input type="text" maxlength="11" class="form-control" id="editProvMob" required="required" value="<%= jsonObj.getString("mobileno") %>">
				<div id="mobile_validate" align="left"></div>
			</div>
			<div class="form-group col-md-6" id="Pmob">
				<label>Password.</label>
				<input type="password" class="form-control" id="editProvPass" autocomplete="off" required="required" value="<%= jsonObj.getString("providerPass") %>">
				<div id="pass_validate" align="left"></div>
			</div>
			<% if(jsonObj.getString("providerType").equals("14") || jsonObj.getString("providerType").equals("15") || jsonObj.getString("providerType").equals("18") || jsonObj.getString("providerType").equals("20") || jsonObj.getString("providerType").equals("24") || jsonObj.getString("providerType").equals("25")){ %>
			<div class="form-group col-md-6">
				<label>MIS3 Approver</label>
				<br>
				<span class="levelYear"><input type="radio" id="active" name="active" <% if(jsonObj.getString("mis3_approver_active").equals("1")){ %>checked<% } %> value="1" class="flat-red">&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<span class="levelMonth"><input type="radio" id="inactive" name="active" value="2" class="flat-red">&nbsp;&nbsp;&nbsp;Inactive</span>
				<div id="activate_validate" align="left"></div>
			</div>
			<div class="form-group col-md-12" id="activate_inactive_date" style="display: none">
				<label id="active_title">MIS3 Approval Activate Date</label>
				<label id="inactive_title">MIS3 Approval Inactive Date</label>
				<input type="text" class="form-control" id="active_date" required="required">
			</div>
			<div class="form-group col-md-12" id="upazilaList" <% if(jsonObj.getString("mis3_approver_active").equals("2") || jsonObj.getString("mis3_approver_active").equals("")){ %>style="display: none"<% } %>>
				<label>Upazila</label>
				<input type="hidden" id="start_date" value="<%= jsonObj.has("start_date")?jsonObj.getString("start_date"):"" %>">
				<select class="form-control multi_upazila" id="approval_upazilas" multiple="multiple" required="required" style="width: 100%;">
				</select>
			</div>
			<% } %>

			<div class="box-footer col-md-12">
				<input type="hidden" id="formType" value="">
				<input type="button" id="ProvEditForm" class="btn btn-primary" value="Update">
			</div>
		</div>
	</div>

<script>

    $('#active').on('change', function(){
        $("#activate_inactive_date").show();
        $("#upazilaList").show();
        $("#inactive_title").hide();
	});

    $('#inactive').on('change', function(){
        $("#activate_inactive_date").show();
        $("#upazilaList").hide();
        $("#active_title").hide();
    });

    $('#active_date').datepicker({
        setDate: new Date(),
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });

    $(document).ready(function() {
        $('.multi_upazila').select2({
            placeholder: 'Select Approval Upazila',
            width: '100%',
            border: '1px solid #e4e5e7',
        });

        $('.multi_upazila').on("select2:select", function (e) {
            var data = e.params.data.text;
            if(data=='all'){
                $(".multi_upazila > option").prop("selected","selected");
                $(".multi_upazila").trigger("change");
            }
        });

        $('.multi_upazila').on('change', function(){
            console.log($('.multi_upazila').val());
		});

        var upzillaJS = JSON.parse(division.replace(/\s+/g, ""));

        var zillaid = $("#RzillaName").val();
        zillaid = zillaid.split("_")[0];

        var divid = $("#RdivName").val();

        $("#approval_upazilas").empty();

        var imp_upz = geo_data[zillaid].split(",");

        //var option_value = "Please Select Upazila";

		var a = <%= jsonObj.has("approval_upazilas")?jsonObj.getString("approval_upazilas"):"[]" %>;
		if(a==null)
		    a = "[]";

        var output = "";

        $.each(upzillaJS[0][divid]["Zilla"][zillaid]["Upazila"], function (key, val) {
            if (imp_upz.includes(key) || $("#formType").val() == "11") {
                if(a.includes(key))
                	output = output + "<option value = " + key + " selected>" + key + " - " + val["nameEnglishUpazila"] + "</option>";
                else
                    output = output + "<option value = " + key + ">" + key + " - " + val["nameEnglishUpazila"] + "</option>";
            }
        });

        $("#approval_upazilas").append(output);
    });
</script>