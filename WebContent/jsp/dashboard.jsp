<script>
    $(document).ready(function() {
        $( ".sidebar-toggle" ).trigger( "click" );
          
        var c = 0;
        var mnch = JSON.parse($("#mnch").val());
        //console.log("mnch "+ mnch);
        $.each(mnch, function (i, val) {
            var graphDivID = "mnch"+c;
            if(c==0) {
                //$("#MNCHCarousel").find(".carousel-indicators").append("<li data-target='#MNCHCarousel' data-slide-to='" + c + "' class='active'></li>");
                $("#MNCHCarousel").find(".carousel-inner").append("<div class='item active'><div id='"+graphDivID+"'></div></div>");
            }
            else {
                //$("#MNCHCarousel").find(".carousel-indicators").append("<li data-target='#MNCHCarousel' data-slide-to='" + c + "'></li>");
                $("#MNCHCarousel").find(".carousel-inner").append("<div class='item items active'><div id='"+graphDivID+"'></div></div>");

            }
            //console.log("mnch data "+ JSON.parse(val));
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(function(){drawVisualization(JSON.parse(val), graphDivID, i, "Maternal & Neonatal Health")});
            //getGraph();
            c++;
        });
        

        $('#MNCHCarousel').carousel({
            interval: 10000,
            cycle: true
        });

        $('#MNCHCarousel')[0].scrollIntoView( true );

        var c = 0;
        var fp = JSON.parse($("#fp").val());
        $.each(fp, function (i, val) {
            var graphDivID = "fp"+c;
            if(c==0) {
                //$("#FPCarousel").find(".carousel-indicators").append("<li data-target='#FPCarousel' data-slide-to='" + c + "' class='active'></li>");
                $("#FPCarousel").find(".carousel-inner").append("<div class='item active'><div id='"+graphDivID+"'></div></div>");
                
            }
            else {
                //$("#FPCarousel").find(".carousel-indicators").append("<li data-target='#FPCarousel' data-slide-to='" + c + "'></li>");
                $("#FPCarousel").find(".carousel-inner").append("<div class='item'><div id='"+graphDivID+"'></div></div>");
            }
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(function(){drawVisualization(JSON.parse(val), graphDivID, i, "Family Planing")});
            //getGraph(JSON.parse(mnch[i]), graphDivID, i, "FP");
            c++;
        });

        $('#FPCarousel').carousel({
            interval: 10000,
            cycle: true
        });

        var c = 0;
        var gp = JSON.parse($("#gp").val());
        $.each(gp, function (i, val) {
            var graphDivID = "gp"+c;
            if(c==0) {
                //$("#GPCarousel").find(".carousel-indicators").append("<li data-target='#GPCarousel' data-slide-to='" + c + "' class='active'></li>");
                $("#GPCarousel").find(".carousel-inner").append("<div class='item active'><div id='"+graphDivID+"'></div></div>");
            }
            else {
                //$("#GPCarousel").find(".carousel-indicators").append("<li data-target='#GPCarousel' data-slide-to='" + c + "'></li>");
                $("#GPCarousel").find(".carousel-inner").append("<div class='item'><div id='"+graphDivID+"'></div></div>");
            }
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(function(){drawVisualization(JSON.parse(val), graphDivID, i, "General Patients")});
            //getGraph(JSON.parse(mnch[i]), graphDivID, i, "GP");
            c++;
        });

        $('#GPCarousel').carousel({
            interval: 10000,
            cycle: true
        });



        function drawVisualization(response, div_id, graph_of, graph_title) {
            var heading = [];
            var get_data = [];
            var c = 0;
            //$.each(response, function (l, list) {
            $.each(response, function(i, item) {
                //console.log(i+" "+item[i]);
                c++;
//                if(c>1) {
//                    //heading += ",'" + i + "'";
//                    response_data += ",[";
//                }
//                else {
//                    //heading += "'" + i + "'";
//                    response_data += "[";
//                }
                var cc = 0;
                var response_data = [];
                //var item_list = item.split(", ");
                $.each(item, function (j, item_val) {
                    cc++;
                    //var get_item = item_val.split("=");
                    if(c==1) {
                        if (cc > 1)
                            heading.push(j); //heading += ",'" + j + "'";
                        else {
                            heading.push("Location");
                            heading.push(j);
//                            heading += "'Location'";
//                            heading += ",'" + j + "'";
                        }
                    }

                    if (cc > 1)
                        response_data.push(parseInt(item_val));//response_data += "," + item_val;
                    else{
                        response_data.push(i);
                        response_data.push(parseInt(item_val));
//                        response_data += "'" + i + "'";
//                        response_data += "," + item_val;
                    }
                });
                if(c==1) {
                    get_data.push(heading);
                    get_data.push(response_data);
                }
                else
                    get_data.push(response_data);
                //response_data += "]";
            });
            var data = google.visualization.arrayToDataTable(get_data);

            console.log(data);
            var options = {
                title : graph_title+' Service Statistics'+' ('+graph_of+')',
                 width : $("#MNCHCarousel").width(),
                 //height : $(".main-sidebar").height()-150,
                 height : $(".main-sidebar").height()-150,
                //width : 500,
                //height : 500,
                //legend: { position: 'bottom', alignment: 'start', maxLines: 2 },
                vAxis: {
                    title: 'Number of Service',
                    format: '0',

                },
                hAxis: {
                    //title: 'Location',
                    direction: -1,
                    slantedText: true,
                    //slantedTextAngle: 45
                    slantedTextAngle: 40
                },
                seriesType: 'bars',
                //series: {5: {type: 'line'}}
            };

            var chart = new google.visualization.ComboChart(document.getElementById(div_id));
            chart.draw(data, options);
        }

        var zillaInfo = JSON.parse(zilla.replace(/\s+/g,"")); //have to solve uncaught type error for zilla

        if($("#acc_ty").val()=="999")
            $(".dashboard_title").append("eMIS (District Wise)");
        else
            $(".dashboard_title").append(zillaInfo[0][<%= request.getAttribute("zilla_id") %>]["nameEnglish"]);

        setInterval(function() {
            $( ".dashboard" ).trigger( "click" );
            $( ".sidebar-toggle" ).trigger( "click" );
        }, 3600000); //1 hour auto reload
    });
</script>


<input type="hidden" id="mnch" value='<%= request.getAttribute("MNCH") %>'>
<input type="hidden" id="fp" value='<%= request.getAttribute("FP") %>'>
<input type="hidden" id="gp" value='<%= request.getAttribute("GP") %>'>
<input type="hidden" id="dashboardView" value="1">
<div class="row">
    <div class="col-md-12"><h2 class="dashboard_title">Service Statistics of </h2></div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#mnch_tab">Maternal & Neonatal Health</a></li>
        <li><a data-toggle="tab" href="#fp_tab">Family Planing</a></li>
        <li><a data-toggle="tab" href="#gp_tab">General Patients</a></li>
    </ul>

    <div class="tab-content">
        <div id="mnch_tab" class="tab-pane fade in active">
            <div id="MNCHCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <%--<ol class="carousel-indicators">--%>
                <%--</ol>--%>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                </div>
              
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#MNCHCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#MNCHCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div id="fp_tab" class="tab-pane fade">
            <div id="FPCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <%--<ol class="carousel-indicators">--%>
                <%--</ol>--%>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#FPCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#FPCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div id="gp_tab" class="tab-pane fade">
            <div id="GPCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <%--<ol class="carousel-indicators">--%>
                <%--</ol>--%>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#GPCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#GPCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

<script>

    setTimeout(function () {

        $(".items").removeClass('active');

    }, 1000);

</script>