<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" style="position: fixed;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="overflow: auto;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="image/user_pic.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-center info">
                <p><span id="nameAtMenu"><%= request.getAttribute("uName") %></span></p>
                <a href="javascript:void(0)"><span id="status"><i class="fa fa-circle text-success"></i>&nbsp;&nbsp;<b>Online</b></span></a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <!-- <li class="header">MAIN NAVIGATION</li>-->
            <%
//                String bypass_url_info = (String) request.getAttribute("bypass_url_info");
                int userType = (Integer)request.getAttribute("uType");
                int mis3_approver = (Integer)request.getAttribute("mis3_approver");
//                int providerid =  (Integer)request.getAttribute("uID");
                int providerid =  Integer.parseInt(request.getAttribute("uID").toString());
            %>

            <script>var global_user_id=<%= Integer.parseInt(request.getAttribute("uID").toString()) %>
                //console.log('Global ID '+global_user_id);
            </script>
            <input type="hidden" id="acc_ty" value="<%= userType %>" />

            <li class="treeview new-dash">
                <a href="javascript:void(0)">
                    <i class="fa fa-home"></i> <span>Home</span>
                </a>
            </li>

            <!--<li class="treeview home">
                <a href="javascript:void(0)">
                    <i class="fa fa-home"></i> <span>Home (Old)</span>
                </a>
            </li> -->

            <% if(userType == 20 || userType == 21 || userType == 999 || userType == 996 || userType == 0){ %>
            <!-- Dashboard section removed
            <li class="treeview dashboard">
                <a href="javascript:void(0)">
                    <i class="fa fa-tachometer"></i> <span>Dashboard</span>
                </a>
            </li> -->

            <%
                }
                if(mis3_approver == 1){
//                    if(userType == 999){
            %>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Supervisor Approval</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-angle-right"></i> <span>MIS3 Approval</span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="approval1 menu-list">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-angle-right"></i> <span>Submission List</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview">
                    <a href="#">
                    <i class="fa fa-angle-right"></i> <span>Satelite Session<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Planning Approval</span>
                    </a>
                    <ul class="treeview-menu">
                    <li class="approval3 menu-list">
                    <a href="javascript:void(0)">
                    <i class="fa fa-angle-right"></i> <span>Submission List</span>
                    </a>
                    </li>
                    </ul>
                    </li>
                </ul>
            </li>
            <% } %>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-line-chart"></i> <span>Reports</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>
                <ul class="treeview-menu">

                    <% //if(userType != 997){ %>
                    <% if(userType != 0){ %>
                    <li class="report1 menu-list active">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Provider Wise Performance</span>
                        </a>
                    </li>
                    <% } %>
                    <!-- <li class="report2 menu-list">
                      <a href="javascript:void(0)">
                        <i class="fa fa-angle-right"></i> <span>Aggregate MNC Report</span>
                      </a>
                    </li> -->
                    <li class="report3 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Service Statistics</span>
                        </a>
                    </li>

                    <% //if(userType != 0){ %>
                    <li class="report4 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>MIS-3 Report</span>
                        </a>
                    </li>
                    <li class="report26 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>MIS3 Submission Information</span>
                        </a>
                    </li>
                    <li class="report27 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Satelite planning Information</span>
                        </a>
                    </li>
                    <% //} %>
                    <% if(userType == 999 || userType == 994|| providerid == 269502 || providerid == 1717457252){ %>
                    <li class="report25 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Pregnant Women List</span>
                        </a>
                    </li>

                    <% if(providerid == 269701){ %>
                    <li class="report31 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Client Information</span>
                        </a>
                    </li>
                    <%}%>
                    <% } %>
                    <!--
                     <li class="report24 menu-list">
                      <a href="javascript:void(0)" id="newmisreport">
                        <i class="fa fa-angle-right"></i> <span>NEW MIS-3 Report</span>
                      </a>
                    </li>
                    -->
                    <!-- <li class="report8 menu-list">
                      <a href="javascript:void(0)">
                        <i class="fa fa-angle-right"></i> <span>MIS-4 Report</span>
                      </a>
                    </li> -->
                    <% if(userType != 0){ %>
                    <li class="report5 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Upazila Performance<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Month Wise)</span>
                        </a>
                    </li>
                    <li class="report9 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Upazila Performance By<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selected Indicators</span>
                        </a>
                    </li>
                    <% } %>
                    <%--<% if(providerid == 269701){ %>--%>
                    <li class="newborn_report menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Essential Newborn Care</span>
                        </a>
                    </li>

                    <li class="client_details menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Client Details</span>
                        </a>
                    </li>
                    <%--<%}%>--%>
                    <li class="report22 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Disease Trend</span>
                        </a>
                    </li>

                    <li class="facility_information menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Facility Information</span>
                        </a>
                    </li>

                    <li class="hr_information menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>HR Information</span>
                        </a>
                    </li>

                    <%--<li class="report10 menu-list">--%>
                    <%--<a href="javascript:void(0)">--%>
                    <%--<!-- <i class="fa fa-angle-right"></i> <span>FWC Reproductive Health<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Services</span> -->--%>
                    <%--<i class="fa fa-angle-right"></i> <span>Facility & Satellite<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clinic Report</span>--%>
                    <%--</a>--%>
                    <%--</li>--%>
                    <% if(userType != 0){ %>
                    <li class="report11 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>CSBA Performance</span>
                        </a>
                    </li>

                    <li class="mhealth-report" data-id="<%= providerid %>">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>M-Health</span>
                        </a>
                    </li>
                    <% } %>

                    <!-- QED -->
                    <li class="reportQed menu-list active">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>QED Indicator</span>
                        </a>
                    </li>
                    <!-- Fever/Cold/Cough -->
                    <%--<li class="reportFCC menu-list active">--%>
                    <%--<a href="javascript:void(0)">--%>
                    <%--<i class="fa fa-angle-right"></i> <span>Fever/Cold/Cough Report</span>--%>
                    <%--</a>--%>
                    <%--</li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-angle-right"></i> <span>Flu Like Symptom</span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="reportFCC menu-list">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-angle-right"></i> <span>Report</span>
                                </a>
                            </li>
                            <li class="reportFCCTrend menu-list">
                                <a href="javascript:void(0)">
                                    <i class="fa fa-angle-right"></i> <span>Trend</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- <li class="report15 menu-list">
                      <a href="javascript:void(0)">
                        <i class="fa fa-angle-right"></i> <span>Provider Login Tracking</span>
                      </a>
                    </li> -->
                </ul>
            </li>
            <%
                if(userType == 999 || userType == 998 || userType == 994 || userType == 995 || userType == 997 || userType == 14 || userType == 15) { %>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-md"></i> <span>Provider Management</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>
                <ul class="treeview-menu">
                    <% if(userType == 999 || userType == 998 || userType == 994){ %>
                    <li class="entry1 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Add Provider</span>
                        </a>
                    </li>
                    <li class="add_csba menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Add CSBA</span>
                        </a>
                    </li>

                    <li class="entry6 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View Provider Info</span>
                        </a>
                    </li>

                    <% if(userType == 999 ||userType == 994){ %>
                    <li class="entry3 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Update Application</span>
                        </a>
                    </li>
                    <% } %>

                    <li class="entry4 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Update Provider Status</span>
                        </a>
                    </li>
                    <!--  <li class="entry5 menu-list">
                          <a href="javascript:void(0)">
                              <i class="fa fa-angle-right"></i> <span>Add Facility</span>
                          </a>
                      </li>-->
                    <li class="entry7 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Transfer Provider</span>
                        </a>
                    </li>
                    <li class="entry10 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Assign Additional Facility</span>
                        </a>
                    </li>

                    <% } %>

                    <% if(userType == 999 || userType == 994){ %>

                    <li class="entry9 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Manage Offline Database</span>
                        </a>
                    </li>
                    <% } %>

                    <% if(userType == 999){ %>

                    <li class="entry12 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Manage Provider</span>
                        </a>
                    </li>
                    <% } %>

                    <% if(userType == 999 || userType == 998 || userType == 997 ||userType == 994 || userType == 995){ %>
                    <li class="report23 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View Registration Status</span>
                        </a>
                    </li>
                    <% } %>

                    <% if(userType == 999 || userType == 998 || userType == 997 ||userType == 994 || userType == 995 || userType == 14 || userType == 15) { %>
                    <li class="report13 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View Provider Login Info</span>
                        </a>
                    </li>
                    <li class="report14 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View Synchronization Status (SymmetricDS)</span>
                        </a>
                    </li>

                    <li class="storage_information menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View Provider Storage Info</span>
                        </a>
                    </li>

                    <li class="synchronization_status menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View Synchronization Status</span>
                        </a>
                    </li>


                    <% } %>
                </ul>
            </li>
            <%

                if(userType == 999 || userType == 998 || userType == 997 ||userType == 994 || userType == 995){ %>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>User Management</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>
                <ul class="treeview-menu">

                    <% if(userType == 999 || userType == 998 || userType == 994){ %>
                    <li class="entry11 menu-list">
                        <a href="javascript:void(0)"><i class="fa fa-angle-right"></i> <span>Add User</span></a>
                    </li>
                    <li class="entry26 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View User Info</span>
                        </a>
                    </li>
                    <% } %>

                    <li class="report12 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>View Monitoring Tool Login Info</span>
                        </a>
                    </li>

                </ul>
            </li>
            <%
                }

            %>

            <%

                if(userType == 999 || userType == 998 || userType == 994 || userType == 997 || userType == 996 || userType == 995 ){ %>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-medkit"></i> <span>Facility Management</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>
                <ul class="treeview-menu">

                    <% if(userType == 999 || userType == 998 || userType == 994){ %>
                    <li class="entry5 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-circle-o"></i> <span>Add Facility</span>
                        </a>
                    </li>
                    <% } %>

                    <!--<li class="entry6 menu-list">-->
                    <li class="entry27 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-circle-o"></i> <span>View Facility List</span>
                        </a>
                    </li>

                    <li class="entry28 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-circle-o"></i> <span>View Facility List with Provider</span>
                        </a>
                    </li>

                </ul>
            </li>
            <%
                }

            %>
            <% if(userType == 999){ %>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-database "></i> <span>SymmetricDS</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="db_rep menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Replicate Database</span>
                        </a>
                    </li>
                    <li class="sym1 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Create Symmetric<br>Channel Onetime</span>
                        </a>
                    </li>
                    <li class="sym2 menu-list">
                    <a href="javascript:void(0)">
                    <i class="fa fa-angle-right"></i> <span>Create Symmetric<br>Channel</span>
                    </a>
                    </li>
                </ul>
            </li>
            <% } } %>

            <% if(userType != 0){ %>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o "></i> <span>Register</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="report18 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Register MNC</span>
                        </a>
                    </li>
                    <li class="report16 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Register Pill Condom ECP</span>
                        </a>
                    </li>
                    <li class="report17 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Register GP</span>
                        </a>
                    </li>
                    <li class="report19  menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Register Injectable</span>
                        </a>
                    </li>
                    <li class="report20  menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Register IUD</span>
                        </a>
                    </li>
                    <li class="report21 menu-list">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Register Implant</span>
                        </a>
                    </li>
                </ul>
            </li>
            <% } %>


            <% if(userType == 999 || providerid == 269502){ %>
            <li class="loginTracking">
                <a href="javascript:void(0)">
                    <i class="fa fa-map-marker"></i> <span>Provider Login Tracking</span>
                    <!--<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					  </span>-->
                </a>
            </li>
            <% } %>

            <%--tools menu--%>
            <% if(userType == 999 || providerid == 269122 || providerid==269701 || providerid == 269102 || providerid == 269125 || providerid ==269100 || providerid == 269123){ %>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-medkit"></i> <span>Tools</span>
                    <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                </a>

                <ul class="treeview-menu">
                    <li class="server-status">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Server Status</span>
                        </a>
                    </li>

                    <!--<li class="entry6 menu-list">-->
                    <li class="service-statistics-cron">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>Service Statistics cron</span>
                        </a>
                    </li>

                    <!--<li class="entry6 menu-list">-->
                    <li class="dbdownload-info">
                        <a href="javascript:void(0)">
                            <i class="fa fa-angle-right"></i> <span>DBDownload Info</span>
                        </a>
                    </li>

                </ul>
            </li>
            <% } %>

<%--            <li class="">--%>
<%--                <a href="http://emis.icddrb.org:8080/emis-monitoringtools/bypass?passphrase=<%= bypass_url_info %>" target="_blank">--%>
<%--                    <i class="fa fa-desktop "></i> <span>Community Monitoring Tool</span>--%>

<%--                </a>--%>
<%--            </li>--%>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
