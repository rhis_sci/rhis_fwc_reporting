<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	if( request.getAttribute("uID") == null || request.getAttribute("uName") == null){
		response.sendRedirect("../index.html");     
    }
%>
		<!-- Make page fluid -->
      <!-- <div id="remove_div"> -->
      
        <style>
        	#bottom{
        		display: none !important;
        	}
        </style>
        
        <script>
        $(document).ready(function(){
        	$(document).on("click",'.sidebar-toggle', function(){
	        	if($("body").hasClass('sidebar-collapse'))
	        	{
	        		$(".sidebar").css('overflow','');
	        	}
	        	else
	        	{
	        		$(".sidebar").css('overflow','auto');
	        	}
        	});
        	var window_height = $( window ).height()-100;
	    	$(".sidebar").css('max-height',window_height);
        	$(window).on('resize', function(){
		        var window_height = $( window ).height()-100;
		    	$(".sidebar").css('max-height',window_height);
        	});
        });
        </script>
        <header class="main-header" style="position: fixed;width: 100%;top:0px;">
                <!-- Logo -->
                <a href="javascript:void(0)" class="logo">
                  <span class="logo-mini"><b>e</b>MIS</span>
                  <span class="logo-lg"><img src="image/logo_emis.png" width="90" height="42" alt=""></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                  <!-- Sidebar toggle button-->
                  <a href="javascript:void(0)" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                  </a>
                  <!-- Navbar Right Menu -->
                  <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                      <!-- User Account: style can be found in dropdown.less -->
                      <li class="dropdown user user-menu open">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                          <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                          <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu">
                          <!-- User image -->
                          <li class="user-header">
                            <img src="image/user_pic.png" class="img-circle" alt="User Image">
                            <p>
                                <span id="nameAtLogout"><%= request.getAttribute("uName") %></span><br>
                              <small>ProviderID: <strong id="ProviderID"><%= request.getAttribute("uID") %></strong></small>
                              <!-- <small>Member since : </small> -->
                            </p>
                          </li>
                          <!-- Menu Footer-->
                          <li class="user-footer">
                            <div class="row">
                              <div class="col-xs-3 text-center">
                                 <a href="javascript:void(0)" class="btn btn-default btn-flat">Profile</a>
                              </div>
                              <div class="col-xs-5 text-center">
                                  <a href='javascript:void(0)' class="btn btn-default btn-flat" id="change_password">Change Password</a>
                                  <input type="hidden" name="uid" id="uid" value="<%= request.getAttribute("uID") %>" />
                                   <!-- <a href="javascript:void(0)" class="btn btn-default btn-flat" id="change_password" onclick="">Change Password</a>-->
                              </div>
                              <div class="col-xs-4 text-center">
		                  			<a href="javascript:void(0)" id="logout" class="btn btn-default btn-flat">Sign out</a>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </nav>
            </header>
        <%-- <header class="main-header" style="position: fixed;width: 100%;">
		    <!-- Logo -->
		    <a href="javascript:void(0)" class="logo">
		      <!-- mini logo for sidebar mini 50x50 pixels -->
		      <span class="logo-mini"><b>e-MIS</b></span>
		      <!-- logo for regular state and mobile devices -->
		      <span class="logo-lg"><b>e-MIS</b> Reporting</span>
		    </a>
		    <!-- Header Navbar: style can be found in header.less -->
		    <nav class="navbar navbar-static-top">
		      <!-- Sidebar toggle button-->
		      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		        <span class="sr-only">Toggle navigation</span>
		      </a>
		      
		      <div class="navbar-custom-menu">
		        <ul class="nav navbar-nav">
		          <li class="dropdown user user-menu">
		            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
		              <span class=""><%= request.getAttribute("uName") %></span>
		            </a>
		            <ul class="dropdown-menu">
		              <!-- User image -->
		              <li class="user-header">
		                <p>
		                  <%= request.getAttribute("uName") %> - <%= request.getAttribute("faciltyName") %>
		                  <small>ProviderID: <%= request.getAttribute("uID") %></small>
		                </p>
		              </li>
		              <!-- Menu Footer-->
		              <li class="user-footer">
		                <div class="pull-right">
		                  <a href="" class="btn btn-default btn-flat">Sign out</a>
		                </div>
		              </li>
		            </ul>
		          </li>
		        </ul>
		      </div>
		    </nav>
		  </header> --%>
		  <c:set var="uType" value="<%= request.getAttribute("uType") %>" scope="request"/>
		  <%@include file="menu.jsp" %>
		  
		  <!-- Content Wrapper. Contains page content -->
		  <div class="content-wrapper">
		    <!-- Content Header (Page header) -->
		    <section class="content-header">
		      <h1 id="head_title">
		        
		      </h1>
		    </section>
		    <!-- Main content -->
    		<section class="content" id="append_report">
    			<% if(userType == 999){ %>
	   			
						<% } %>
    		</section>
		  </div>

            <div id="model_box"></div>
		  <div id="loading_div" class="loadPopup"></div>
		  
		  <footer class="main-footer">
		    <div class="pull-right hidden-xs">
		      <b>Version</b> 1.0
		    </div>
		    <div class="pull-left"><strong id="copyrigth"> </strong> <b>eMIS. All rights reserved.</b></div>
		  </footer>	
