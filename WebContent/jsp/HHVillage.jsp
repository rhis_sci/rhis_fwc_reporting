<%@ page import="java.util.LinkedHashMap" %>

<style>
    .td_color
    {
        background-color: #F2DEDE !important;
    };

    .rot
    {
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        white-space: nowrap;
        width:40px;
        margin-top: 100px;
    }

    .fixedHeader-floating{
        top: 7% !important;
    }
    .w3-container:after,.w3-container:before,.w3-panel:after,.w3-panel:before,.w3-row:after,.w3-row:before,.w3-row-padding:after,.w3-row-padding:before,
    .w3-container,.w3-panel{padding:0.01em 20px}
</style>
<link rel="stylesheet" href="library/bootstrap/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="library/bootstrap/css/fixedHeader.bootstrap.min.css">
<link rel="stylesheet" href="library/bootstrap/css/responsive.bootstrap.min.css">

<script src="library/js/jquery.dataTables.min.js"></script>
<script src="library/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="library/js/dataTables.fixedHeader.min.js"></script>
<script src="library/js/dataTables.responsive.min.js"></script>
<script src="library/bootstrap/js/responsive.bootstrap.min.js"></script>

<div class="tile-body nopadding" id="reportTable">
    <div class="row">
        <div class="col-md-12">
            <div id="printDiv" style="margin-top: 2%;">
                <table class="table table-striped table-bordered nowrap reportExport" id="reportDataTable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Village Name</th>
                            <th>Ward</th>
                            <th>Start HH</th>
                            <th>End HH</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                <%
                    LinkedHashMap<String, LinkedHashMap<String, String>> hhinfo = (LinkedHashMap<String, LinkedHashMap<String, String>>) request.getAttribute("HHInfo");
                    for (String key : hhinfo.keySet()) {
                %>
                    <tr>
                        <td><% out.print(hhinfo.get(key).get("village_name")); %></td>
                        <td>
                            <select class="form-control select2" id="<% out.print(key); %>_<% out.print(hhinfo.get(key).get("zilla_id")); %>_<% out.print(hhinfo.get(key).get("upazila_id")); %>_<% out.print(hhinfo.get(key).get("union_id")); %>_<% out.print(hhinfo.get(key).get("mouza_id")); %>_<% out.print(hhinfo.get(key).get("village_id")); %>_hhWard">
                                <option value="">--Select Ward--</option>
                                <option value="1" <% if(hhinfo.get(key).get("ward_id").equals("1") || hhinfo.get(key).get("ward_id").equals("01")){out.print("selected");} %>>1</option>
                                <option value="2" <% if(hhinfo.get(key).get("ward_id").equals("2") || hhinfo.get(key).get("ward_id").equals("02")){out.print("selected");} %>>2</option>
                                <option value="3" <% if(hhinfo.get(key).get("ward_id").equals("3") || hhinfo.get(key).get("ward_id").equals("03")){out.print("selected");} %>>3</option>
                            </select>
                        </td>
                        <td><input type="text" id="<% out.print(key); %>_<% out.print(hhinfo.get(key).get("zilla_id")); %>_<% out.print(hhinfo.get(key).get("upazila_id")); %>_<% out.print(hhinfo.get(key).get("union_id")); %>_<% out.print(hhinfo.get(key).get("mouza_id")); %>_<% out.print(hhinfo.get(key).get("village_id")); %>_StartHH" class="form-control" value="<% out.print(hhinfo.get(key).get("min_hhno")); %>"></td>
                        <td><input type="text" id="<% out.print(key); %>_<% out.print(hhinfo.get(key).get("zilla_id")); %>_<% out.print(hhinfo.get(key).get("upazila_id")); %>_<% out.print(hhinfo.get(key).get("union_id")); %>_<% out.print(hhinfo.get(key).get("mouza_id")); %>_<% out.print(hhinfo.get(key).get("village_id")); %>_EndHH" class="form-control" value="<% out.print(hhinfo.get(key).get("max_hhno")); %>"></td>
                        <td><input type="button" class="btn btn-info" onclick="saveHHWardInfo('<% out.print(key); %>',<% out.print(hhinfo.get(key).get("zilla_id")); %>,<% out.print(hhinfo.get(key).get("upazila_id")); %>,<% out.print(hhinfo.get(key).get("union_id")); %>,<% out.print(hhinfo.get(key).get("mouza_id")); %>,<% out.print(hhinfo.get(key).get("village_id")); %>)" value="Save"></td>
                    </tr>
                <%
                    }
                %>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="model_box" title="Basic dialog"></div>
    </div>
</div>
<script src="js/lib/ddtf.js"></script>
<script>
    $('.filterdTable').ddTableFilter();
</script>

<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>
<script type="text/javaScript">
    $('.clickable').click(function () {
        $('.clickable').css({"background-color": "rgb(255, 255, 255)"});
        $(this).css({"background-color": "rgb(0, 172, 214)"});
        $('.clickable > span').removeClass("bg-aqua").addClass("bg-gray");
        $(this).find("span:first").removeClass("bg-gray").addClass("bg-aqua");
        $('.clickable span img').each(function(index, element) {
            var url = $(element).prop("src").replace("png","jpg");
            $(element).attr("src",url);
        });
        url = $(this).find("span img").prop("src").replace("jpg","png");
        $(this).find("span img").attr("src",url);
        openTab($(this).attr("data"));
    });
    function openTab(tabName) {
        var tabcontent;
        tabcontent = document.getElementsByClassName("tab");
        for (var i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        document.getElementById(tabName).style.display = "block";
    }
    function doExport(selector, params) {
        var options = {
            //ignoreRow: [1,11,12,-2],
            //ignoreColumn: [0,-1],
            //pdfmake: {enabled: true},
            tableName: 'Service Statistics',
            worksheetName: 'Service Statistics'
        };

        $.extend(true, options, params);

        $(selector).tableExport(options);
    }

    function DoOnCellHtmlData(cell, row, col, data) {
        var result = "";
        if (data != "") {
            var html = $.parseHTML( data );

            $.each( html, function() {
                if ( typeof $(this).html() === 'undefined' )
                    result += $(this).text();
                else if ( $(this).is("input") )
                    result += $('#'+$(this).attr('id')).val();
                else if ( $(this).is("select") )
                    result += $('#'+$(this).attr('id')+" option:selected").text();
                else if ( $(this).hasClass('no_export') !== true )
                    result += $(this).html();
            });
        }
        return result;
    }

    function DoOnMsoNumberFormat(cell, row, col) {
        var result = "";
        if (row > 0 && col == 0)
            result = "\\@";
        return result;
    }

</script>

<script>
    $(document).ready(function() {
        //$('#reportDataTable').DataTable();

        var table = $('#reportDataTable').DataTable( {
            //responsive: true
        } );

        $('#reportDataTableFilter').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        } );
    } );

    function saveHHWardInfo(key,zilla_id,upazila_id,union_id,mouza_id,village_id)
    {
        loadGif();

        //var preventMultiC = 1;
        var hhInfo = new Object();
        hhInfo.zilla = zilla_id;
        hhInfo.upazila = upazila_id;
        hhInfo.union = union_id;
        hhInfo.mouza = mouza_id;
        hhInfo.village = village_id;
        hhInfo.ward = $('#'+key+'_'+zilla_id+'_'+upazila_id+'_'+union_id+'_'+mouza_id+'_'+village_id+'_hhWard').val();
        hhInfo.hhStart = $('#'+key+'_'+zilla_id+'_'+upazila_id+'_'+union_id+'_'+mouza_id+'_'+village_id+'_StartHH').val();
        hhInfo.hhEnd = $('#'+key+'_'+zilla_id+'_'+upazila_id+'_'+union_id+'_'+mouza_id+'_'+village_id+'_EndHH').val();
        hhInfo.submit = 1;

        //alert(reportObj.report1_union);

        $.ajax({
            type: "POST",
            url:"saveHHWardInfo",
            timeout:60000, //60 seconds timeout
            data:{"forminfo":JSON.stringify(hhInfo)},
            success: function (response) {
                if(response=="1")
                {
                    callDialog("<b style='color:green'>Data Update Successfully.</b>");
                    saveProviderInfo();
                }
                else
                    callDialog("<b style='color:red'>Data could not update. Please try again.</b>");
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                //alert(xhr.status);
                //alert(status);
                //alert(error);
                checkInternetConnection(xhr)
            }
        });
    }

    function saveProviderInfo ()
    {
        loadGif();

        //var preventMultiC = 1;
        var forminfo = new Object();
        forminfo.changetype = $('#changeType').val();
        forminfo.zillaid = $('#RzillaName').val();
        forminfo.zillaname = $('#RzillaName option:selected').text().split(" - ")[1].toLowerCase();
        forminfo.upazilaid = $('#RupaZilaName').val();
        forminfo.unionid = $('#RunionName').val();
        forminfo.unionname = $('#RunionName option:selected').text().split(" - ")[1];
        forminfo.providertype = $('#providerType').val();
        forminfo.providercode = $('#ProvCode').val();
        forminfo.providerpass = $('#ProvPass').val();
        forminfo.providername = $('#ProvName').val();
        forminfo.providermobile = $('#ProvMob').val();
        forminfo.facilityid = $('#facilityName').val();
        forminfo.facilityname = $('#facilityName option:selected').text();
        forminfo.facilitytype = $('#facilityType').val();
        forminfo.aav_providerType = $('#aav_providerType').val();
        forminfo.version = $('#version').val();
        forminfo.retireddate = $('#end_date').val();
        forminfo.type = $('#formType').val();

        //alert(reportObj.report1_union);

        if($('#formType').val() == "5")
            var url = "saveFacilityInfo";
        else if($('#formType').val() == "8")
            var url = "loadVill";
        else
            var url = "saveProviderInfo";

        $.ajax({
            type: "POST",
            url:url,
            timeout:60000, //60 seconds timeout
            data:{"forminfo":JSON.stringify(forminfo)},
            success: function (response) {
                if($('#formType').val() == "8")
                {
                    $(".box-default").addClass("collapsed-box");
                    $(".search_form").hide();
                    $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

                    $("#reportTable").remove();
                    $("#table_row").show();
                    $("#table_append" ).html(response);

                    var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                    var bottom = $el.position().top + $el.outerHeight(true);
                    $(".main-sidebar").css('height',bottom+"px");
                    //alert(bottom);

                    $('#table_row')[0].scrollIntoView( true );
                }
                else {
                    if (response == "1") {
                        callDialog("<b style='color:green'>Data Entry successfully.</b>");
                        userEntryForm($('#formType').val());
                    }
                    else
                        callDialog("<b style='color:red'>Data could not insert.</b>");
                }
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
    }
</script>
