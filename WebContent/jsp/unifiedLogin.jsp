<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <title>Routine Health Information System</title> -->
    <title>e-MIS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
            name="viewport">

    <link rel="SHORTCUT ICON" href="image/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="image/favicon.ico" type="image/ico" />

    <link rel="stylesheet" href="css/style.css">
    <!-- progress bar -->
    <link rel="stylesheet" href="nprogress/nprogress.css">
    <script type="text/javascript" src="nprogress/nprogress.js"></script>

    <!-- <link rel="stylesheet" href="css/scrollbar.css"> -->

    <link rel="stylesheet" href="css/jquery-ui.min.css">

    <link rel="stylesheet" href="library/css/skins/_all-skins.min.css">

    <!-- <link rel="stylesheet" href="css/jquery-ui.css"> -->

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="library/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="library/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="library/css/AdminLTE.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="library/select2/select2.min.css">


    <!-- jQuery 2.1.3 -->
    <script type="text/javascript" src="js/lib/jquery-2.2.4.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="library/bootstrap/js/bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="library/select2/select2.full.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script
            src="library/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="library/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="library/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="library/js/app.min.js"></script>
    <!-- Google Map js -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIJA8RDbXgFn-6qv2zlnKt5JGa4S3Y7wo"></script>


    <script type="text/javascript" src="js/lib/moment.js"></script>
    <script type="text/javascript" src="js/lib/combodate.js"></script>
    <script type="text/javascript" src="js/utilities.js"></script>
    <script type="text/javascript" src="json/zilla.json"></script>
    <!--<script type="text/javascript" src="json/division.json"></script>-->

    <script type="text/javascript" src="js/init.js"></script>
    <script type="text/javascript" src="library/js/canvasjs.min.js"></script>
    <script type="text/javascript" src="js/lib/d3.v3.min.js"></script>


    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <link href="//cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="//cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <!--<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="library/js/buttons.print.min.js"></script>

    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <!--<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js" type="text/javascript"></script>-->
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js" type="text/javascript"></script>
    <!--<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js" type="text/javascript"></script>-->

    <script type="text/javascript" src="library/js/pdfmake.min.js"></script>
    <script type="text/javascript" src="library/js/vfs_fonts.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Datatable -->
    <link rel="stylesheet" href="library/bootstrap/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="library/bootstrap/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="library/bootstrap/css/responsive.bootstrap.min.css">

    <!--<script src="library/js/jquery.dataTables.min.js"></script>-->
    <script src="library/bootstrap/js/dataTables.bootstrap.min.js"></script>
    <script src="library/js/dataTables.fixedHeader.min.js"></script>
    <script src="library/js/dataTables.responsive.min.js"></script>
    <script src="library/bootstrap/js/responsive.bootstrap.min.js"></script>

    <script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
    <script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
    <script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
    <script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
    <script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
    <script type="text/javascript" src="library/js/tableExport.js"></script>
    <script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>

    <link href='library/eventCalendar/css/fullcalendar.min.css' rel='stylesheet' />
    <link href='library/eventCalendar/css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='library/eventCalendar/js/moment_calendar.min.js'></script>
    <script src='library/eventCalendar/js/fullcalendar.min.js'></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>
        $(document).ready(function(){
            $.ajax({
                type: "POST",
                url:"newdashboard",
                timeout:60000,
                success: function (response) {
                    $(".remove_div").remove();
                    $( "#append_report" ).html(response);
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
        });
    </script>

</head>
<body class="hold-transition skin-blue sidebar-mini" style="background: #d2d6de;">
<div id="wrapper">
    <!-- <div id="after_append"></div> -->

    <%@ page language="java" contentType="text/html; charset=UTF-8"
             pageEncoding="UTF-8"%>

    <%
        if( request.getAttribute("uID") == null || request.getAttribute("uName") == null){
            response.sendRedirect("../index.html");
        }
    %>
    <!-- Make page fluid -->
    <!-- <div id="remove_div"> -->

    <style>
        #bottom{
            display: none !important;
        }
    </style>

    <!-- Left side column. contains the logo and sidebar -->
    <c:set var="uType" value="<%= request.getAttribute("uType") %>" scope="request"/>
    <%@include file="menu.jsp" %>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 id="head_title">

            </h1>
        </section>
        <!-- Main content -->
        <section class="content" id="append_report">
            <% if(userType == 999){ %>

            <% } %>
        </section>
    </div>

    <div id="loading_div" class="loadPopup"></div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong id="copyrigth"> </strong> <b>MaMoni - Health Systems Strengthening</b>
    </footer>

    <!-- /.login-box -->
</div>

</body>
</html>
