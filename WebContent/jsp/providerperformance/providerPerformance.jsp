<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/28/2021
  Time: 2:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<style>
    .input-daterange input, #monthSelect, #yearSelect {
        background: url('image/calendar.png') no-repeat;
        background-size: 30px 30px;
        padding-left: 30px;
        text-align: center;
        background-color: #ffffff !important;
    }
</style>
<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">
                    <!--geo group-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="divisionName">
                                <label>Division</label>
                                <select class="form-control select2" id="RdivName" style="width: 100%;">
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="districtName">
                                <label>District</label>
                                <select class="form-control select2" id="RzillaName" style="width: 100%;">
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="upazilaName">
                                <label>Upazila</label>
                                <select class="form-control select2" id="RupaZilaName" style="width: 100%;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/geo group-->

                    <!--service section-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="method_type" style="display: none;">
                                <label>Service</label>
                                <select class="form-control select2" id="methodType" style="width: 100%;">
                                    <option value="">--Select Method Type--</option>
                                    <option value="1">Maternal and Child Health</option>
                                    <option value="2">Family Planing</option>
                                    <option value="3">General Patient</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/service section-->

                    <!--date section-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-12 date_type">
                                <label>Select Date Type</label><br/>
                                <span class="levelYear"><input type="radio" id="year_type" name="date_type"
                                                               value="3" class="flat-red">&nbsp;&nbsp;&nbsp;Year Wise&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span class="levelMonth"><input type="radio" id="month_type" name="date_type"
                                                                value="1" class="flat-red">&nbsp;&nbsp;&nbsp;Month Wise&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <span class="levelDate"><input type="radio" id="date_type" name="date_type"
                                                               value="2"
                                                               class="flat-red">&nbsp;&nbsp;&nbsp;Date Wise</span>
                            </div>

                            <div class="form-group date col-md-2 year_wise" style="display: none">
                                <label class="control-label">Year</label>
                                <input type="text" id="yearSelect" class="form-control" readonly>
                            </div>
                            <div class="form-group date col-md-2 month_wise" style="display: none">
                                <label class="control-label">Month</label>
                                <input type="text" id="monthSelect" class="form-control" readonly>
                            </div>
                            <div class="form-group col-md-4 date_wise " style="display: none">
                                <label class="control-label" for="start_date">Date Range</label>
                                <div class="input-group input-daterange">
                                    <input type="text" id="start_date" class="form-control" readonly>
                                    <div class="input-group-addon">to</div>
                                    <input type="text" id="end_date" class="form-control" readonly>
                                </div>
                            </div>


                            <div class="form-group col-md-4">
                                <label class="control-label"></label>
                                <div class="input-group input-button">
                                    <input type="hidden" id="reportType"
                                           value="<% out.print(request.getAttribute("type")); %>">
                                    <div>
                                        <button type="button" id="submitReport1" class="btn btn-primary">
                                            <i class="fa fa-send"></i> Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/date section-->

                </div>
            </div>
            <!--/row-->
        </div>
    </div>

    <!--for showing table(performance) result-->
    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>
    <!--/for showing table(performance) result-->

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="library/datepicker/datepicker.min.css">
    <!-- bootstrap datepicker -->
    <script src="library/datepicker/bootstrap-datepicker.min.js"></script>

    <script>
        $(".select2").select2();

        $('#year_type, #monthly').on('change', function () {
            $(".year_wise").show();
            $('#end_date').datepicker('setDate', null);
            $('#start_date').datepicker('setDate', null);
            $('#monthSelect').datepicker('setDate', null);
            $('#yearSelect').datepicker('setDate', new Date().getFullYear().toString());
            $(".date_wise").hide();
            $(".month_wise").hide();
        });

        $('#month_type, #weekly').on('change', function () {
            $(".month_wise").show();
            $('#end_date').datepicker('setDate', null);
            $('#start_date').datepicker('setDate', null);
            $('#yearSelect').datepicker("setDate", null);
            $('#monthSelect').datepicker('setDate', new Date().getFullYear() + '-' + (new Date().getMonth() + 1).toString());
            $(".date_wise").hide();
            $(".year_wise").hide();
        });

        $('#date_type, #daily').on('change', function () {
            $(".date_wise").show();
            $('#end_date').datepicker("setDate", new Date());
            $('#monthSelect').datepicker("setDate", null);
            $('#yearSelect').datepicker("setDate", null);
            $(".month_wise").hide();
            $(".year_wise").hide();
        });

        $('#yearSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            viewMode: "years",
            minViewMode: "years",
            format: 'yyyy'
        }).datepicker('setDate', new Date().getFullYear().toString());

        $('#monthSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            minViewMode: 1,
            format: 'yyyy-mm'
        }).datepicker('setDate', new Date().getFullYear() + '-' + (new Date().getMonth() + 1).toString());

        $('#start_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
        $('#end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });
    </script>
</div>
