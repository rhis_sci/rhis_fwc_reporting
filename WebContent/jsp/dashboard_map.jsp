<link rel="stylesheet" href="css/map.css">

<div class="row">
	<div class="col-md-12">
		<h2 class="map_heading"></h2>
	</div>
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-1">
				<a href="javascript:void(0)" onclick="prev_dis()" class="btn btn-info prev_dis" style="display: none;">District</a>
			</div>
			<div class="col-md-1">
				<a href="javascript:void(0)" onclick="prev_upz()" class="btn btn-info prev_upz" style="display: none;">Upazila</a>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="row">
		    <div class="col-md-8">
		       	<div class="row">
			    	<div class="col-md-2">
			    		<div class="row">
			     			<div class="col-md-2" style="margin-top: 5%;">
								<img width="100" src="image/compass.png">
							</div>
							<div class="col-md-12 union_legend" style="margin-top: 5%; display: none;">
								<dl><b>Category : </b><br>
								<!-- <dt class="green"></dt> -->
								<dt><img src="image/FWC_CAT_A.png" width="15"></dt>
								<dd>A</dd><br>
								<!-- <dt class="yellow"></dt> -->
								<dt><img src="image/FWC_CAT_B.png" width="15"></dt>
								<dd>B</dd><br>
								<!-- <dt class="red"></dt> -->
								<dt><img src="image/FWC_CAT_C.png" width="15"></dt>
								<dd>C</dd><br>
								<dt><img src="image/UHC.png" width="15"></dt>
								<dd>UHC</dd><br>
								<!-- <dt class="white"></dt>
								<dd style="width: 6em !important; margin: 0 0 0 0em !important;font-size: 12px;">No UH&FWC</dd>
								</dl> -->
								</dl>
							</div>

							<div class="col-md-12 zilla_legend" style="margin-top: 5%; display: none;">
								<dt><img src="image/Implemented_Area.gif" height="40" width="40"></dt>
								<dd>eMIS Area</dd><br>
								</dl>
							</div>

						</div>
					</div>
					<div class="col-md-10">
		   				<map>
							<script src="js/map.js"></script>
						</map>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row" style="margin-top: 10%;">
					<div class="col-md-12">
						<div class="tooltip_view"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mouse_hover"></div>