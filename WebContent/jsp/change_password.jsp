<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

		<style>
			input[type="number"]::-webkit-outer-spin-button,
			input[type="number"]::-webkit-inner-spin-button {
			    -webkit-appearance: none;
			    margin: 0;
			}
			input[type="number"] {
			    -moz-appearance: textfield;
			}
		</style>
        
        <div class="remove_div">
	        <div class="box box-default">
		        <div class="box-header with-border">
		          <h3 class="box-title" id="box_title">Change Password  Form</h3>
		
		          <div class="box-tools pull-right">
		            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		          </div>
		        </div>
		        <!-- /.box-header -->
		        <div class="box-body search_form">
		          <div class="row">
		           		           
	                  <div class="form-group col-md-12" id="Pname">
	                    <label>Current Password <font color="red">*</font></label>
	                    <input type="password" class="form-control" id="CurrPass" autocomplete="off" required="required" placeholder="Current Password">
	                  </div>
					  
					  <div class="form-group col-md-6" id="Pname">
	                    <label>New Password<font color="red">*</font></label>
	                    <input type="password" class="form-control" id="NewPass" autocomplete="off" required="required" placeholder="New Password">
	                  </div>
	                  
	                   <div class="form-group col-md-6" id="Pname">
	                    <label>Confirm Password<font color="red">*</font></label>
	                    <input type="password" class="form-control" id="ConPass" autocomplete="off" required="required" placeholder="Confirm Password">
	                  </div>
	                
		            </div>
					  <div class="box-footer col-md-12">
		                <input type="hidden" id="formType" value="">
	                    <input type="button" id="ChangePasswordForm" class="btn btn-primary" value="Submit">
		              </div>
					
		          </div>
		          <!-- /.row -->
		        </div>
	        </div>
	        
	      
	   <!-- <div id="model_box"></div>-->

        
     </div>    
        
        
        
