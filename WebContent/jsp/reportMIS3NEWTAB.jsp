<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.json.JSONObject" %>
<%@ page pageEncoding="UTF-8" %>
<%--/to view only webend not in tab format--%>
<% String webview = (String) request.getAttribute("webview");
	String jsondata = request.getAttribute("Result").toString();
	JSONObject jsonObj = new JSONObject(jsondata);%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <title>Routine Health Information System</title> -->
    <title>MIS3</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<link rel="stylesheet" href="dist/accordion.css" />

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="library/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Jquery min css -->
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <!-- jQuery 2.1.3 -->
    <script type="text/javascript" src="js/lib/jquery-2.2.4.min.js"></script>
    <!-- jQuery ui -->
    <script type="text/javascript" src="js/lib/jquery-ui.min.js"></script>
    <!-- jQuery base64 -->
    <script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>
    <!-- utilities -->
    <script type="text/javascript" src="js/utilities.js"></script>

    <script type="text/javascript">
	
		report_show = 0;
		
        $(document).on("click", '.next_button' ,function (){
            var button_val = $(".next_button").val();
            //alert(parseInt(button_val)+1);
            if(button_val<4)
            {
                $(".next_button").val(parseInt(button_val)+1);
                $(".pre_button").val(parseInt(button_val)-1);
                $(".table_hide").hide();
                $(".page_"+button_val).show();
                $(".showing_page").text("  "+button_val+" of 3  ");
                $(".pre_button").removeAttr("disabled");
                if((parseInt(button_val)+1)>3)
                    $(".next_button").attr("disabled","disabled");

                $(".page_no").hide();
            }
        });

        $(document).on("click", '.pre_button' ,function (){
            var button_val = $(".pre_button").val();
            if(button_val>0)
            {
                $(".pre_button").val(parseInt(button_val)-1);
                $(".next_button").val(parseInt(button_val)+1);
                $(".table_hide").hide();
                $(".page_"+button_val).show();
                $(".showing_page").text("  "+button_val+" of 3  ");
                $(".next_button").removeAttr("disabled");
                if((parseInt(button_val)-1)<1)
                {
                    $(".pre_button").attr("disabled","disabled");
                    $(".page_no").show();
                }
            }
        });

        $(function() {
            var submitted_data = $("#submitted_data").val();
            if(submitted_data!=""){
				//alert(submitted_data); // Need to remove later
				
                var submitted_json = jQuery.parseJSON(submitted_data);
                $("#mis3_entry_form :input").each(function(e){
                    id = this.id;
                    $("#"+id).val(submitted_json[id]);
                });
            }

            $(".stock_table :input").css("width", "50px")
        });

        $(document).on("click", '#re_entry' ,function (){
		
			report_show = 1;
			
            $('#mis3_entry_form').dialog({
                modal: true,
                title: "MIS3 Entry Form",
               <!-- width: 'auto',-->
			    width: '100%',
                open: function () {
                    
                },
                buttons: {
                    "Save" : function () {
                        var id;
                        var post_data = {};
                        $("#mis3_entry_form :input").each(function(e){
                            id = this.id;
							//post_data[id] = this.value;
							if(id == "delivery_facility")
							{
								//alert(id);
								var selected = $("input[type='radio'][name='delivery_facility']:checked");
								if (selected.length > 0) {
									selectedVal = selected.val();
									post_data[id]= selectedVal;
									//alert(post_data[id]);
								}
								
							}
							else
								{
								post_data[id] = this.value;
								}
							
                        });

                        var post_info = {};
                        var current_url = window.location.href;
                        var params = current_url.split("?");
                        post_info["params"] = params[1];
                        post_info["entryData"] = post_data;

                        console.log(post_info);

                        $.ajax({
                            type: "POST",
                            url:"MIS3EntryData",
                            timeout:60000, //60 seconds timeout
                            data:{"entryInfo":JSON.stringify(post_info)},
                            success: function (response) {
                                //location.reload();
								//alert("Re-entry entry");
								$("#reportTable").load(location.href + " #reportTable");				
								
								$('#reportTable').show();
								
                            }
                        });

                        $(this).dialog('close');
                    },
					 "Cancel" : function() {
                        $(this).dialog('close');
						$('#reportTable').show();
                       
                    }
                }
            });
        });
		
		/***** Entry Form will display just after loading the url if submitted data is empty
		* Coded By: Evana
		* @Date: 19/05/2019
		*****/
		
		$(document).ready(function(){
		
			//alert("hello");
			
			var submitted_data = $("#submitted_data").val();
			// alert(submitted_data);
			 if(submitted_data!="")
			 {
			 	$('#reportTable').show();
				$('#mis3_entry_form').hide();
			 }
			 else
			 {
			 	entryForm();
			 }


		}); 
		
		function entryForm()
		{
			//alert("Hello");
			
			 $('#mis3_entry_form').dialog({
                modal: true,
                title: "MIS3 Entry Form",
                width: '100%',
				
                open: function () {
                    //$(this).append("#mis3_entry_form");
					 // $('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass("buttonPrimary");
                },
                buttons: {
                    "Save" : function () {
                        var id;
                        var post_data = {};
						var selectedVal = "";
                        $("#mis3_entry_form :input").each(function(e)
							{
                            id = this.id;
										
							if(id == "delivery_facility_yes" || id == "delivery_facility_no")
							{
								//alert(id);

								if($('#delivery_facility_yes').is(':checked'))
								    post_data["delivery_facility"] = 1;
								else if($('#delivery_facility_no').is(':checked'))
                                    post_data["delivery_facility"] = 2;
								else
                                    post_data["delivery_facility"] = 0;
								
								// var selected = $("input[type='radio'][name='delivery_facility']:checked");
								// console.log(selected.val());
								// if (selected.length > 0) {
								// 	selectedVal = selected.val();
								// 	post_data[id]= selectedVal;
								// 	//alert(post_data[id]);
								// }
								
							}
							else
							{
							post_data[id] = this.value;
							}
									
							
                        });

                        var post_info = {};
                        var current_url = window.location.href;
                        var params = current_url.split("?");
                        post_info["params"] = params[1];
                        post_info["entryData"] = post_data;

                        console.log(post_info);

                        $.ajax({
                            type: "POST",
                            url:"MIS3EntryData",
                            timeout:60000, //60 seconds timeout
                            data:{"entryInfo":JSON.stringify(post_info)},
                            success: function (response) {
                               // location.reload();
								//$(this).dialog('close');
								//alert("first entry");
								$("#reportTable").load(location.href + " #reportTable");
								$('#reportTable').show();
								//$('#mis3_entry_form').hide();
                            }
                        });
						
						
                        $(this).dialog('close');
						
						
						//$("#reportTable").load(window.location.href + " #reportTable");
						//$('#reportTable').show();
						//$('#mis3_entry_form').hide();
						//$('#reportTable').show();
                    },
					 "Cancel" : function() {
					 	
                        $(this).dialog('close');
						$('#reportTable').show();
						$("#reportTable").load(location.href + " #reportTable");
                       
                    }
                }
            });
			return false;
		}

        $(document).on("click", '#submitMIS3' ,function (){
            $("#model_box").dialog({
                modal: true,
                title: "Message",
                width: 'auto',
               /* open: function () {
                    $(this).html("সাবমিট করার পর রিপোর্ট এর তথ্য পরিবর্তনের কোনো সুযোগ নেই। <br> আপনি রিপোর্ট সাবমিটের ব্যাপারে নিশ্চিত হলে Ok চাপুন  অন্যথায় Cancel চাপুন। ");
                },*/
				 open: function () {
				 $(this).html("&#2488;&#2494;&#2476;&#2478;&#2495;&#2463; &#2453;&#2480;&#2494;&#2480; &#2474;&#2480; &#2480;&#2495;&#2474;&#2507;&#2480;&#2509;&#2463; &#2447;&#2480; &#2468;&#2469;&#2509;&#2479; &#2474;&#2480;&#2495;&#2476;&#2480;&#2509;&#2468;&#2472;&#2503;&#2480; &#2453;&#2507;&#2472;&#2507; &#2488;&#2497;&#2479;&#2507;&#2455; &#2472;&#2503;&#2439;&#2404; <br> &#2438;&#2474;&#2472;&#2495; &#2480;&#2495;&#2474;&#2507;&#2480;&#2509;&#2463; &#2488;&#2494;&#2476;&#2478;&#2495;&#2463;&#2503;&#2480; &#2476;&#2509;&#2479;&#2494;&#2474;&#2494;&#2480;&#2503; &#2472;&#2495;&#2486;&#2509;&#2458;&#2495;&#2468; &#2489;&#2482;&#2503; Ok &#2458;&#2494;&#2474;&#2497;&#2472;  &#2437;&#2472;&#2509;&#2479;&#2469;&#2494;&#2527; Cancel &#2458;&#2494;&#2474;&#2497;&#2472;&#2404;");
                },
                buttons : {
                    "Ok" : function() {
                        $(this).dialog('close');
                        var submission_status = "0";
                        if($("#submissionStatus").val()!="" && $("#submissionStatus").val()!==undefined)
                            submission_status = "3";
                        //alert(submission_status);
                        submitMIS3Report(submission_status);
                    },

					 "Cancel" : function() {
                        $(this).dialog('close');
                     }
					 
                  }

            });

        });

        /*$(document).on("click", '#approveMIS3' ,function (){
            submitMIS3Report("1");
        });

        $(document).on("click", '#rejectMIS3' ,function (){
            submitMIS3Report("2");
        });*/

        function submitMIS3Report(submission_status){
            //		var url = "";
            //if(submission_status=="0"){
            //			var current_url = window.location.href;
            //			var params = current_url.split("?");
            //url = "MIS3Submit?"+params[1];
            //console.log(params[1]);
            /*}
            else{
                var getParams = "facilityId="+$('#facilityName').val()+"&report1_zilla="+$('#report1_zilla').val()+"&report1_start_date="+$('#monthSelect').val()+"&submissionStatus="+submission_status+"&supervisorComment="+$('#supervisor_comment').val();
                var params = $.base64.encode(getParams);
                url = "MIS3Submit?"+params;
            }*/

            var post_info = {};
            var current_url = window.location.href;
            var params = current_url.split("?");
            var url = "MIS3Submit?"+params[1]+"&submissionStatus="+submission_status;
            post_info["params"] = params[1];
            post_info["entryData"] = jQuery.parseJSON($("#total_mis3_data").val());

            console.log(post_info);

            $.ajax({
                type: "POST",
                url:"MIS3EntryData",
                timeout:60000, //60 seconds timeout
                data:{"entryInfo":JSON.stringify(post_info)},
                success: function (response) {
                    $.ajax({
                        type: "GET",
                        url:url,
                        timeout:60000, //60 seconds timeout
                        //data:{"forminfo":JSON.stringify(forminfo)},
                        success: function (response) {
                            if(response=='true'){
                                $("#model_box").dialog({
                                    modal: true,
                                    title: "Message",
                                    width: 'auto',
                                    open: function () {
                                        $(this).html("<b style='color:green'>Your MIS3 Submit Successfully.</b>");
                                    },
                                    buttons : {
                                        Ok : function() {
                                            $(this).dialog('close');
											//location.reload();
											
											$('#mis3_entry_form').hide();
											
											$("#reportTable").load(location.href + " #reportTable");
											$('#reportTable').show();
											$('#submitMIS3').hide();
											$('#re_entry').hide();
											
                                        }
                                    }
                                });
                            }
                            else{
                                $('#model_box').dialog({
                                    modal: true,
                                    title: "Message",
                                    width: 'auto',
                                    open: function () {
                                        $(this).html("<b style='color:red'>Your MIS3 not submitted successfully. Please re-submit your MIS3.</b>");
                                    },
                                    buttons: {
                                        Ok: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            }
                        },
                        complete: function() {
                            preventMultiCall = 0;
                        },
                        error: function(xhr, status, error) {
                            checkInternetConnection(xhr)
                        }
                    });
                }
            });


        }
    </script>

    <style>
        .td_color
        {
            background-color: #F2DEDE !important;
        }


        .rot
        {
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            white-space: nowrap;
            width:40px;
            margin-top: 100px;
        }

		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button 
		{ 
		-webkit-appearance: none; 
		margin: 0; 
		}
		
		input[type="number"] { /****For Mozilla: number spinner remove. *****/
			-moz-appearance: textfield;
		}

        .mis3_field{
            min-width: 50px !important;
        }
.style2 {font-size: 12}

    </style>
</head>
<body>
<!--<div class="tile-body nopadding table-responsive" id="reportTable" style="margin: 10px; display:none;">-->
<div class="tile-body nopadding table-responsive" id="reportTable" style="margin: 10px;">
    <div id="printDiv">
        <jsp:include page="NewMIS3Report.jsp">
            <jsp:param name="Result" value="${Result}"/>
        </jsp:include>
    </div>

</div>
<!--to view only webend not in tab format-->
<% if(!(webview.equals("true"))){%>
<div id="model_box" title="Basic dialog"></div>

<div class="container" id="mis3_entry_form" style="display: none">

<%--<div class="container" id="mis3_entry_form">--%>
		
		<!-- Accordion begin -->
		<!--<ul class="demo-accordion accordionjs list-group">-->
		<br/>
		<div>
		<table class="table table-bordered" id="table_1" cellspacing="0" style='font-size:10px;'>
	
		  <tr>
			<td><h5>&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;&#2463;&#2495;&#2468;&#2503; &#2536;&#2538;/&#2541; &#2465;&#2503;&#2482;&#2495;&#2477;&#2503;&#2480;&#2496; &#2488;&#2503;&#2476;&#2494;</span> &#2470;&#2503;&#2527;&#2494; &#2489;&#2527; &#2453;&#2495;&#2472;&#2494;?</h5> </td>
			<td > <input type="radio" id="delivery_facility_yes" name="delivery_facility" value="1" class="mis3_field" checked="checked"/> &#2489;&#2509;&#2479;&#2494;</td>
			<td style='text-align: center;'><input type="radio" id="delivery_facility_no" name="delivery_facility" value="2" class="mis3_field" />&#2472;&#2494;</td>
			
		  </tr>
		</table>
		</div>
		<ul class="demo-accordion accordionjs list-group">

			<!-- Section 1 -->
			<li class="list-group-item acc_active">
				<div class="fa fa-minus">&nbsp;<b>&#2474;&#2480;&#2495;&#2476;&#2494;&#2480; &#2474;&#2480;&#2495;&#2453;&#2482;&#2509;&#2474;&#2472;&#2494; &#2474;&#2470;&#2509;&#2471;&#2468;&#2495;
				</b></div>
				<div>
		
			<!-- Accordion begin -->
			<ul class="demo-accordion accordionjs list-group">
			
				<!-- Section 2 -->
				<li class="list-group-item acc_active">
					<div class="fa fa-minus">&nbsp;<strong>&#2447;&#2478;&#2438;&#2480; &#2474;&#2480;&#2476;&#2480;&#2509;&#2468;&#2496; &#2474;&#2480;&#2495;&#2476;&#2494;&#2480; &#2474;&#2480;&#2495;&#2453;&#2482;&#2509;&#2474;&#2472;&#2494;</strong></div>
					<div class="table-responsive">
					
					<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
						<tr>
						<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
						<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
						<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
						</tr>
					  <tr>
						<td> &#2447;&#2478;&#2438;&#2480; &#2474;&#2480;&#2476;&#2480;&#2509;&#2468;&#2496; <br>&#2474;&#2480;&#2495;&#2476;&#2494;&#2480; &#2474;&#2480;&#2495;&#2453;&#2482;&#2509;&#2474;&#2472;&#2494;</td>
						<td><input type="number" class="form-control mis3_field" id="mr_after_fp"></td>
						<td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
					  </tr>
						
					  </table>
						
					</div>
				</li>

				<!-- Section 3 -->
				<li class="list-group-item">
					<div class="fa fa-plus">&nbsp;<strong>&#2447;&#2478;&#2438;&#2480;&#2447;&#2478; &#2474;&#2480;&#2476;&#2480;&#2509;&#2468;&#2496; &#2474;&#2480;&#2495;&#2476;&#2494;&#2480; &#2474;&#2480;&#2495;&#2453;&#2482;&#2509;&#2474;&#2472;&#2494;</strong></div>
					<div class="table-responsive">
					<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
						<tr>
						<td bgcolor="#E6E6F2"><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
						<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
						<td bgcolor="#E6E6F2"><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
						</tr>
						  <tr>
							<td> &#2447;&#2478;&#2438;&#2480;&#2447;&#2478; &#2474;&#2480;&#2476;&#2480;&#2509;&#2468;&#2496; <br>&#2474;&#2480;&#2495;&#2476;&#2494;&#2480; &#2474;&#2480;&#2495;&#2453;&#2482;&#2509;&#2474;&#2472;&#2494;</td>
							<td><input type="number" class="form-control mis3_field" id="mrm_after_fp"></td>
							<td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
						  </tr>
					  </table>
					</div>
				</li>

			</ul>
			</div>
			</li>

			<!-- Section 2 -->
			<!--<li>
				<div><h3>&#2474;&#2509;&#2480;&#2460;&#2472;&#2472; &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479; &#2488;&#2503;&#2476;&#2494; </h3></div>
				<div>
					
				</div>
			</li>-->
			
			<!-- Section 3 -->
			<li class="list-group-item">
				<div class="fa fa-plus">&nbsp;<b>&#2474;&#2509;&#2480;&#2460;&#2472;&#2472; &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479; &#2488;&#2503;&#2476;&#2494;</b></div>
				<div class="nodemohtml">

							<!-- Accordion begin -->
								<ul class="demo-accordion accordionjs list-group">

									<!-- Section 1 -->
									<!--<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<strong>&#2455;&#2480;&#2509;&#2477; &#2453;&#2494;&#2482;&#2496;&#2472; &#2488;&#2503;&#2476;&#2494;</strong></div>
										<div>
										<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>-->
				
											
												<!--<tr>
												<td style="text-align: left;vertical-align: middle;border-top: 1px solid black;">&#2536;&#2538;-&#2537;&#2538; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489;&#2503;&#2480; <br> &#2478;&#2471;&#2509;&#2479;&#2503; &#2439;&#2472;&#2460;&#2503;&#2453;&#2509;&#2486;&#2494;&#2472; <br> &#2447;&#2472;&#2509;&#2463;&#2495;&#2472;&#2503;&#2463;&#2494;&#2482; &#2453;&#2480;&#2463;&#2495;&#2453;&#2507; <br>&#2488;&#2509;&#2463;&#2503;&#2480;&#2527;&#2503;&#2465; &#2474;&#2509;&#2480;&#2494;&#2474;&#2509;&#2468; <br> &#2455;&#2480;&#2509;&#2477;&#2476;&#2468;&#2495; &#2478;&#2494;&#2527;&#2503;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</td>
												<td valign="top" style="text-align: left;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="pregnant_24_34_injection_center" ></td>
												<td valign="top" style="border-top: 1px solid black;"><input type="number" class="form-control mis3_field"  readonly="on"></td>
												</tr>-->
		
										<!--  </table>
										</div>
									</li>-->

									<!-- Section 2 -->
									<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<strong>&#2474;&#2509;&#2480;&#2488;&#2476; &#2488;&#2503;&#2476;&#2494;</strong></div>
										<div>
										<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
											<td style="text-align: left;">&#2437;&#2472;&#2509;&#2479;&#2494;&#2472;&#2509;&#2479; (&#2475;&#2480;&#2488;&#2503;&#2475;/&#2477;&#2509;&#2479;&#2494;&#2453;&#2497;&#2527;&#2494;&#2478;/&#2476;&#2509;&#2480;&#2495;&#2458;) </td>
											<td style="text-align: left;"><input type="number" class="form-control mis3_field" id="others_forcep_center"></td>
											<td style="border-top: 1px solid black;"><input type="number" class="form-control mis3_field" readonly="on"></td>
											</tr>
											<tr>
											<td style="text-align: left;">&#2474;&#2494;&#2480;&#2509;&#2463;&#2507;&#2455;&#2509;&#2480;&#2494;&#2475; &#2476;&#2509;&#2479;&#2476;&#2489;&#2494;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494; </td>
											<td style="text-align: left;"><input type="number" class="form-control mis3_field" id="partograph_using_center" ></td>
											<td style="border-top: 1px solid black;"><input type="number" class="form-control mis3_field" readonly="on"></td>
											</tr>
											
											<!--<tr>
											<td style="text-align: left;">&#2474;&#2509;&#2480;&#2488;&#2476; &#2453;&#2494;&#2482;&#2495;&#2472; &#2437;&#2468;&#2495;&#2480;&#2495;&#2453;&#2509;&#2468; &#2480;&#2453;&#2509;&#2468;&#2453;&#2509;&#2487;&#2480;&#2467; (IPH) <br> &#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2478;&#2494;&#2527;&#2503;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494; </td>
											<td style="text-align: left;"><input type="number" class="form-control mis3_field" id="delivery_iph" ></td>
											<td style="border-top: 1px solid black;"><input type="number" class="form-control mis3_field" readonly="on"></td>
											</tr>-->
											
										</table>
										</div>
									</li>

									<!-- Section 3 -->
									<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<strong>&#2447;&#2478; &#2438;&#2480; / &#2447;&#2478; &#2438;&#2480; &#2447;&#2478;  &#2488;&#2503;&#2476;&#2494;</strong></div>
										<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<!--<tr>
												<td style="text-align: left;vertical-align: middle;border-top: 1px solid black;">&#2474;&#2509;&#2480;&#2488;&#2476;&#2507;&#2451;&#2480; &#2437;&#2468;&#2495;&#2480;&#2495;&#2453;&#2509;&#2468; &#2480;&#2453;&#2509;&#2468;&#2453;&#2509;&#2487;&#2480;&#2467; <br>(PPH) &#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2478;&#2494;&#2527;&#2503;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;  </td>
												<td style="text-align: left;vertical-align: middle;"><input type="number" class="form-control mis3_field" id="pnc_mother_pph_center"></td>
												<td style="text-align: left;vertical-align: middle;"><input type="number" class="form-control mis3_field" id="pnc_mother_pph_satelite"></td>
											  </tr>
										-->
												
												<!--<tr>
												<td style="text-align: left;vertical-align: middle;border-top: 1px solid black;"> RTI / STI &#2488;&#2434;&#2457;&#2509;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2458;&#2495;&#2453;&#2495;&#2510;&#2488;&#2494; &#2474;&#2509;&#2480;&#2470;&#2494;&#2472;&#2453;&#2499;&#2468; &#2480;&#2507;&#2455;&#2496;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</td>
												 	<td><input type="number" class="form-control mis3_field" id="rti_sti_given_center"></td>
													<td><input type="number" class="form-control mis3_field" id="rti_sti_given_satelite"></td>
												</tr>-->
												
												
												
												<tr>
												<td style="text-align: left;vertical-align: middle;">&#2447;&#2478; &#2438;&#2480; (&#2447;&#2478; &#2477;&#2495;&#2447;) &#2488;&#2503;&#2476;&#2494; &#2474;&#2509;&#2480;&#2470;&#2494;&#2472;&#2503;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494; </td>
													<td><input type="number" class="form-control mis3_field" id="mrmva_given_center"></td>
													<td><input type="number" class="form-control mis3_field" id="mrmva_given_satelite"></td>
												</tr>
												
												<tr>
												<td style="text-align: left;"> &#2447;&#2478; &#2438;&#2480; &#2447;&#2478; (&#2452;&#2487;&#2471;&#2503;&#2480; &#2478;&#2494;&#2471;&#2509;&#2479;&#2478;&#2503; &#2447;&#2478; &#2438;&#2480;) <br>&#2488;&#2503;&#2476;&#2494; &#2474;&#2509;&#2480;&#2470;&#2494;&#2472;&#2503;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;  </td>
												<td><input type="number" class="form-control mis3_field" id="mrm_given_center"></td>
												<td><input type="number" class="form-control mis3_field" id="mrm_given_satelite"></td>
											</tr>
										</table>
										</div>
									</li>

									
									<!-- Section 5 -->
									<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<strong>&#2439;&#2488;&#2495;&#2474;&#2495; &#2455;&#2509;&#2480;&#2489;&#2472;&#2453;&#2494;&#2480;&#2496; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</strong></div>
										<div>
											<!-- Your text here. For this demo, the content is generated automatically. -->
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
											<td style="text-align: left;vertical-align: middle;">&#2439;&#2488;&#2495;&#2474;&#2495; &#2455;&#2509;&#2480;&#2489;&#2472;&#2453;&#2494;&#2480;&#2496; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</td>
											<td><input type="number" class="form-control mis3_field" id="ecp_taken_center"></td>
											<td><input type="number" class="form-control mis3_field" id="ecp_taken_satelite"></td>
											</tr>
											</table>
										</div>
									</li>
									<!-- Section 6 -->
									<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<strong>&#2476;&#2472;&#2509;&#2471;&#2509;&#2479;&#2494;&#2468;&#2509;&#2476; &#2488;&#2503;&#2476;&#2494;</strong></div>
										<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
												<td style="text-align: left;">&#2458;&#2495;&#2453;&#2495;&#2510;&#2488;&#2494; / &#2474;&#2480;&#2494;&#2478;&#2480;&#2509;&#2486;</td>
												<td style="text-align: left;"><input type="number" class="form-control mis3_field" id="infertility_treatment_center"></td>
												<td style="text-align: left;"><input type="number" class="form-control mis3_field" id="infertility_treatment_satelite"></td>
											  </tr>
												
												<tr>
												<td>&#2480;&#2503;&#2475;&#2494;&#2480;</td>
												<td><input type="number" class="form-control mis3_field" id="infertility_refer_center"></td>
												<td><input type="number" class="form-control mis3_field" id="infertility_refer_satelite"></td>	</tr>

											</table>
										</div>
									</li>
									
									<!-- Section 4 -->
									<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<strong>&#2460;&#2480;&#2494;&#2527;&#2497;&#2480; &#2478;&#2497;&#2454;  &#2451; &#2488;&#2509;&#2468;&#2472; &#2453;&#2509;&#2479;&#2494;&#2472;&#2509;&#2488;&#2494;&#2480; &#2488;&#2509;&#2453;&#2509;&#2480;&#2496;&#2472;&#2495;&#2434;</strong>
										</div>
										<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
											<td style="text-align: left;vertical-align: middle;"> &#2478;&#2507;&#2463; VIA &#2474;&#2460;&#2495;&#2463;&#2477; </td>
											 	<td><input type="number" class="form-control mis3_field" id="total_via_positive_center"></td>
												<td><input type="number" class="form-control mis3_field" id="total_via_positive_satelite"></td>
											</tr>
		
												<tr>
												<td style="text-align: left;vertical-align: middle;">&#2478;&#2507;&#2463; VIA &#2472;&#2503;&#2455;&#2503;&#2463;&#2495;&#2477;  </td>
												 	<td><input type="number" class="form-control mis3_field" id="total_via_negative_center"></td>
													<td><input type="number" class="form-control mis3_field" id="total_via_negative_satelite"></td>
												</tr>
												
												<tr>
												<td style="text-align: left;vertical-align: middle;"> &#2478;&#2507;&#2463; CBE &#2474;&#2460;&#2495;&#2463;&#2477;  </td>
												 <td><input type="number" class="form-control mis3_field" id="total_cbe_positive_center"></td>
													<td><input type="number" class="form-control mis3_field" id="total_cbe_positive_satelite"></td>
												</tr>
												
												<tr>
												<td style="text-align: left;vertical-align: middle;">&#2478;&#2507;&#2463; CBE  &#2472;&#2503;&#2455;&#2503;&#2463;&#2495;&#2477;  </td>
												  <td><input type="number" class="form-control mis3_field" id="total_cbe_negative_center"></td>
													<td><input type="number" class="form-control mis3_field" id="total_cbe_negative_satelite"></td>
												</tr>
										</table>
										</div>
									</li>

								</ul>
								
						
					<!-- Accordion end -->

				</div>
			</li>
			
			<!-- Section 3 -->
			<li class="list-group-item">
				<div class="fa fa-plus">&nbsp;<b>&#2486;&#2495;&#2486;&#2497; &#2488;&#2503;&#2476;&#2494;( &#2534;-&#2539;&#2543; &#2478;&#2494;&#2488;)</b></div>
					<div class="nodemohtml">

							<!-- Accordion begin -->
								<ul class="demo-accordion accordionjs list-group">

									<!-- Section 1 -->
									<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<strong>&#2453;&#2503;&#2447;&#2478;&#2488;&#2495; &#2488;&#2503;&#2476;&#2494;</strong></div>
										<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
											<td> &#2453;&#2503;&#2447;&#2478;&#2488;&#2495; &#2486;&#2497;&#2480;&#2497; &#2489;&#2527;&#2503;&#2459;&#2503; &#2447;&#2478;&#2472; <br>&#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494; </td>
											 <td><input type="number" class="form-control mis3_field" id="kmc_started_child_center"></td>
												<td><input type="number" class="form-control mis3_field" id="kmc_started_child_satelite"></td>
											</tr>

											<tr>
											<td>&#2447;&#2439; &#2453;&#2509;&#2482;&#2495;&#2472;&#2495;&#2453;/&#2489;&#2494;&#2488;&#2474;&#2494;&#2468;&#2494;&#2482;&#2503; &#2460;&#2472;&#2509;&#2478; <br>&#2455;&#2509;&#2480;&#2489;&#2472;&#2453;&#2494;&#2480;&#2496; &#2453;&#2503; &#2447;&#2478; &#2488;&#2495; &#2486;&#2497;&#2480;&#2497;<br> &#2453;&#2480;&#2494; &#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;  </td>
											 <td><input type="number" class="form-control mis3_field" id="kmc_birth_child_center"></td>
												<td><input type="number" class="form-control mis3_field" id="kmc_birth_child_satelite"></td>
											</tr>

											<tr>
											<td> &#2478;&#2507;&#2463; &#2453;&#2468;&#2460;&#2472; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2465;&#2495;&#2488;&#2458;&#2494;&#2480;&#2509;&#2460; <br>(&#2459;&#2494;&#2524;&#2474;&#2468;&#2509;&#2480;) &#2474;&#2509;&#2480;&#2470;&#2494;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;  </td>
											 <td><input type="number" class="form-control mis3_field" id="discharge_child_center"></td>
												<td><input type="number" class="form-control mis3_field" id="discharge_child_satelite"></td>
											</tr>

											<tr>
											<td>&#2465;&#2495;&#2488;&#2458;&#2494;&#2480;&#2509;&#2460; &#2437;&#2472; &#2480;&#2495;&#2453;&#2497;&#2527;&#2503;&#2488;&#2509;&#2463; (&#2437;&#2472;&#2497;&#2480;&#2507;&#2471;&#2503;&#2480;)<br>&#2447;&#2480; &#2477;&#2495;&#2468;&#2509;&#2468;&#2495;&#2468;&#2503; &#2465;&#2495;&#2488;&#2458;&#2494;&#2480;&#2509;&#2460; <br>&#2474;&#2509;&#2480;&#2470;&#2494;&#2472; &#2453;&#2480;&#2494; &#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494; </td>
											<td><input type="number" class="form-control mis3_field" id="discharge_onrequest_child_center"></td>
											<td><input type="number" class="form-control mis3_field" id="discharge_onrequest_child_satelite"></td>
											</tr>

											<tr>
												<td>&#2453;&#2503;&#2447;&#2478;&#2488;&#2495; &#2465;&#2495;&#2488;&#2453;&#2472;&#2509;&#2463;&#2495;&#2472;&#2495;&#2441;&#2527;&#2503;&#2486;&#2494;&#2472; <br>(&#2453;&#2503;&#2447;&#2478;&#2488;&#2495; &#2488;&#2503;&#2476;&#2494; &#2476;&#2509;&#2479;&#2494;&#2489;&#2468;) &#2489;&#2451;&#2527;&#2494; <br> &#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494; </td>
										 		<td><input type="number" class="form-control mis3_field" id="kmc_discontinuation_child_center"></td>
												<td><input type="number" class="form-control mis3_field" id="kmc_discontinuation_child_satelite"></td>
											</tr>

											<tr>
												<td>&#2453;&#2503;&#2447;&#2478;&#2488;&#2495; &#2488;&#2503;&#2476;&#2494;&#2474;&#2509;&#2480;&#2494;&#2474;&#2509;&#2468; &#2486;&#2495;&#2486;&#2497; <br>&#2478;&#2499;&#2468;&#2509;&#2479;&#2497;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;  </td>
										  		<td><input type="number" class="form-control mis3_field" id="complicated_child_center"></td>
												<td><input type="number" class="form-control mis3_field" id="complicated_child_satelite"></td>
											</tr>

											<tr>
												<td> &#2453;&#2468;&#2460;&#2472; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2453;&#2503;&#2447;&#2478;&#2488;&#2495; <br>&#2475;&#2482;&#2507;&#2438;&#2474; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;  </td>
										  		<td><input type="number" class="form-control mis3_field" id="kmc_treatment_child_center"></td>
										  		<td><input type="number" class="form-control mis3_field" id="kmc_treatment_child_satelite"></td>
											</tr>

											<tr>
												<td> &#2453;&#2468;&#2460;&#2472; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2453;&#2503;&#2447;&#2478;&#2488;&#2495; <br>&#2475;&#2482;&#2507;&#2438;&#2474; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;  </td>
										  		<td><input type="number" class="form-control mis3_field" id="kmc_followup_child_center"></td>
												<td><input type="number" class="form-control mis3_field" id="kmc_followup_child_satelite"></td>
											</tr>

											</table>
										</div>
									</li>

									<!-- Section 2 -->
									<li class="list-group-item">
										<div class="fa fa-plus">&nbsp;<b>&#2463;&#2495;&#2453;&#2494; &#2474;&#2509;&#2480;&#2494;&#2474;&#2509;&#2468; (&#2534;-&#2535;&#2539; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;)  &#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</b></div>


										<ul class="demo-accordion accordionjs list-group">
										<li class="list-group-item">
										<div class="fa fa-plus">
										&nbsp;&#2476;&#2495;&#2488;&#2495;&#2460;&#2495; (&#2460;&#2472;&#2509;&#2478;&#2503;&#2480; &#2474;&#2480; &#2469;&#2503;&#2453;&#2503;)</div>
										<div>
										<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
										<tr>
										<td> &#2476;&#2495;&#2488;&#2495;&#2460;&#2495; (&#2460;&#2472;&#2509;&#2478;&#2503;&#2480; &#2474;&#2480; &#2469;&#2503;&#2453;&#2503;) </td>
									  		<td><input type="number" class="form-control mis3_field" id="bcg_afterbirth_center"></td>
											<td><input type="number" class="form-control mis3_field" id="bcg_afterbirth_satelite"></td>
										</tr>
										</table>
										</div>
										</li>
										<li class="list-group-item">
											<div class="fa fa-plus">&nbsp; &#2474;&#2503;&#2472;&#2509;&#2463;&#2494;&#2477;&#2509;&#2479;&#2494;&#2482;&#2503;&#2472;&#2509;&#2463; (&#2465;&#2495;&#2474;&#2495;&#2463;&#2495;, &#2489;&#2503;&#2474;-&#2476;&#2495;,&#2489;&#2495;&#2476;)</div>
											<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
											<td>&#2535; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2540; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
											<td><input type="number" class="form-control mis3_field" id="pentavolent_one_given_center"></td>
												<td ><input type="number" class="form-control mis3_field" id="pentavolent_one_given_satelite"></td>
											</tr>

											<tr>
											<td>&#2536; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2535;&#2534; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
										   <td><input type="number" class="form-control mis3_field" id="pentavolent_two_given_center"></td>
												<td><input type="number" class="form-control mis3_field" id="pentavolent_two_given_satelite"></td>
											</tr>

											<tr>
											<td>&#2537; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2535;&#2538; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;) </td>
										   	<td><input type="number" class="form-control mis3_field" id="pentavolent_three_given_center"></td>
											<td><input type="number" class="form-control mis3_field" id="pentavolent_three_given_satelite"></td>
											</tr>


											</table>
											</div>
										</li>
										<li class="list-group-item">
											<div class="fa fa-plus">&nbsp; &#2474;&#2495;&#2488;&#2495;&#2477;&#2495; &#2463;&#2495;&#2453;&#2494; </div>
											<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
											<td>&#2535; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br> &#2540; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
											<td><input type="number" class="form-control mis3_field" id="pcv_one_given_center"></td>
												<td><input type="number" class="form-control mis3_field" id="pcv_one_given_satelite"></td>
											</tr>

											<tr>
											<td>&#2536; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2535;&#2534; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
											<td><input type="number" class="form-control mis3_field" id="pcv_two_given_center"></td>
											<td><input type="number" class="form-control mis3_field" id="pcv_two_given_satelite"></td>
											</tr>

											<tr>
											<td >&#2537; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2535;&#2538; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
										   <td > <input type="number" class="form-control mis3_field" id="pcv_three_given_center"></td>
											<td><input type="number" class="form-control mis3_field" id="pcv_three_given_satelite"></td>
											</tr>

											</table>
											</div>
										</li>
										<li class="list-group-item">
											<div class="fa fa-plus">&nbsp;&#2451;&#2474;&#2495;&#2477;&#2495;</div>
											<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
										<td>&#2535; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2540; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
										 <td> <input name="number" type="number" class="form-control mis3_field" id="opv_one_given_center"></td>
										 <td><input type="number" class="form-control mis3_field" id="opv_one_given_satelite"></td>
										 </tr>

										<tr>
										<td>&#2536; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2535;&#2534; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
									 	<td><input type="number" class="form-control mis3_field" id="opv_two_given_center"></td>
										<td><input type="number" class="form-control mis3_field" id="opv_two_given_satelite"></td>
										</tr>

										<tr>
										<td>&#2537; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2535;&#2538; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;) </td>
									 	 <td><input type="number" class="form-control mis3_field" id="opv_three_given_center"></td>
										 <td><input type="number" class="form-control mis3_field" id="opv_three_given_satelite"></td>
										</tr>
											</table>
											</div>
										</li>
										<li class="list-group-item">
											<div class="fa fa-plus">&nbsp;&#2438;&#2439;&#2474;&#2495;&#2477;&#2495; (&#2475;&#2509;&#2480;&#2494;&#2453;&#2486;&#2472;&#2494;&#2482;)</div>
											<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>

											<tr>
											<td>&#2535; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2540; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;) </td>
										 	<td><input type="number" class="form-control mis3_field" id="ipv_one_given_center"></td>
											<td><input type="number" class="form-control mis3_field" id="ipv_one_given_satelite"></td>
											</tr>

											<tr>
											<td>&#2536; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2535;&#2538; &#2488;&#2474;&#2509;&#2468;&#2494;&#2489; &#2489;&#2482;&#2503;)</td>
										 	<td><input type="number" class="form-control mis3_field" id="ipv_two_given_center"></td>
											<td><input type="number" class="form-control mis3_field" id="ipv_two_given_satelite"></td>
											</tr>


											</table>
											</div>
										</li>
										<li class="list-group-item">
											<div class="fa fa-plus">&nbsp;&#2447;&#2478; &#2438;&#2480; &#2463;&#2495;&#2453;&#2494;</div>
											<div>
											<table class="table table-bordered" border="0" cellpadding="4" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
											<tr>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
											<td bgcolor="#E6E6F2"><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
											<td bgcolor="#E6E6F2" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
											</tr>
											<tr>
											<td>&#2535; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>&#2543; &#2478;&#2494;&#2488; &#2474;&#2497;&#2480;&#2472; &#2489;&#2482;&#2503;)
											 </td>
										 		<td><input type="number" class="form-control mis3_field" id="mr_one_given_center"></td>
												<td><input type="number" class="form-control mis3_field" id="mr_one_given_satelite"></td>
											</tr>

											<tr>
											<td>&#2536; (&#2486;&#2495;&#2486;&#2497;&#2480; &#2476;&#2527;&#2488; <br>
											  &#2535;&#2539; &#2478;&#2494;&#2488; &#2474;&#2497;&#2480;&#2472; &#2489;&#2482;&#2503;) </td>
											<td><input type="number" class="form-control mis3_field" id="mr_two_given_center"></td>
											<td><input type="number" class="form-control mis3_field" id="mr_two_given_satelite"></td>
											</tr>
											</table>
											</div>
										</li>
										</ul>

									</li>


								</ul>


					<!-- Accordion end -->

				</div>
			</li>
			
			
			<!-- Section 4 -->
			<li class="list-group-item">
				<div class="fa fa-plus">&nbsp;<b>&#2437;&#2472;&#2509;&#2468;&#2480;&#2509;&#2476;&#2495;&#2477;&#2494;&#2455;&#2503; &#2477;&#2480;&#2509;&#2468;&#2495; &#2480;&#2507;&#2455;</b>
				</div>
				<div class="table-responsive">
				<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
						<tr>
						<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
						<td bgcolor="#DCDCED" ><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
						<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
						</tr>
						<tr>
						<td >&#2478;&#2489;&#2495;&#2482;&#2494;</td>
						<td><input type="number" class="form-control mis3_field" id="internal_female_patient_center"></td>
						<td><input type="number" class="form-control mis3_field" readonly="on"></td>
						</tr>
						
						<tr>
						<td >&#2486;&#2495;&#2486;&#2497; (&#2534; - &#2535; &#2476;&#2459;&#2480;) </td>
						<td><input type="number" class="form-control mis3_field" id="internal_oneyear_child_patient_center"></td>
						<td><input type="number" class="form-control mis3_field" readonly="on"></td>	
						</tr>
						
						<tr>
						<td>&#2486;&#2495;&#2486;&#2497; (&#2535; - &#2539; &#2476;&#2459;&#2480;) </td>
						<td><input type="number" class="form-control mis3_field" id="internal_onetofive_child_patient_center"></td>
						<td><input type="number" class="form-control mis3_field" readonly="on"></td>	
						</tr>
						
						<tr>
						<td>&#2478;&#2507;&#2463;  </td>
						<td><input type="number" class="form-control mis3_field" id="internal_total_patient_center"></td>
						<td><input type="number" class="form-control mis3_field" readonly="on"></td>	
						</tr>
						
				  </table>	
					<!-- Accordion end -->

				</div>
			</li>
			
			<!-- Section 5 -->
			<li class="list-group-item">
				<div class="fa fa-plus"> &nbsp;<b> &#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;&#2503; &#2453;&#2527;&#2463;&#2495; &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479; &#2486;&#2495;&#2453;&#2509;&#2487;&#2494; (&#2476;&#2495;&#2488;&#2495;&#2488;&#2495;) &#2453;&#2494;&#2480;&#2509;&#2479;&#2453;&#2509;&#2480;&#2478; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; </b></div>
			<div>
				<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
						<tr>
						<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
						<td bgcolor="#DCDCED" ><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
						<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
						</tr>
						<tr>
						<td>&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;&#2503; &#2453;&#2527;&#2463;&#2495; &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479; &#2486;&#2495;&#2453;&#2509;&#2487;&#2494; (&#2476;&#2495;&#2488;&#2495;&#2488;&#2495;) &#2453;&#2494;&#2480;&#2509;&#2479;&#2453;&#2509;&#2480;&#2478; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;</td>
						<td><input type="number" class="form-control mis3_field" id="bcc_done_center"></td>
						<td><input type="text" class="form-control mis3_field" readonly="on" disabled="disabled"></td>
						</tr>
					
						
			  </table>	
					<!-- Accordion end -->		
						
					<!-- Accordion end -->

			  </div>
			</li>
			
			<div align="left">
			  <!-- Section 6 -->
		  </div>
			<li class="list-group-item">
				<div class="fa fa-plus">&nbsp;<b>&#2447;&#2488;&#2447;&#2488;&#2495;&#2447;&#2478;&#2451; (SACMO) &#2453;&#2480;&#2509;&#2468;&#2499;&#2453; &#2453;&#2527;&#2463;&#2495; &#2488;&#2509;&#2453;&#2497;&#2482;&#2503;  &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479;  &#2486;&#2495;&#2453;&#2509;&#2487;&#2494; (&#2476;&#2495;&#2488;&#2495;&#2488;&#2495;) <br>
   &#2453;&#2494;&#2480;&#2509;&#2479;&#2453;&#2509;&#2480;&#2478; &#2488;&#2478;&#2509;&#2474;&#2494;&#2470;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; </b></div>
			 
				<div >

				<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
						<tr>
						<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
						<td bgcolor="#DCDCED" ><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
						<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
						</tr>
						<tr>
						<td>&#2447;&#2488;&#2447;&#2488;&#2495;&#2447;&#2478;&#2451; (SACMO) &#2453;&#2480;&#2509;&#2468;&#2499;&#2453; &#2488;&#2509;&#2453;&#2497;&#2482;&#2503;  &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479;  &#2486;&#2495;&#2453;&#2509;&#2487;&#2494; (&#2476;&#2495;&#2488;&#2495;&#2488;&#2495;) <br>&#2453;&#2494;&#2480;&#2509;&#2479;&#2453;&#2509;&#2480;&#2478; &#2488;&#2478;&#2509;&#2474;&#2494;&#2470;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;</td>
						<td><input type="number" class="form-control mis3_field" id="sacmo_done_center"></td>
						<td><input type="text" class="form-control mis3_field" readonly="on" disabled="disabled"></td>
						</tr>
	
				  </table>				

				</div>
		  </li>
			
			<li class="list-group-item">
				<div class="fa fa-plus">&nbsp;<b>&#2474;&#2497;&#2487;&#2509;&#2463;&#2495; &#2488;&#2503;&#2476;&#2494;</b></div>
				<div>
				<table class="table table-bordered" style="font-size:12px;border: 1px solid black;word-wrap:break-word; table-layout: fixed;width: 100%;" cellspacing="0" cellpadding="0">
				<tr>
				<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</div></td>
				<td bgcolor="#DCDCED" ><div align="center">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</div></td>
				<td bgcolor="#DCDCED" ><div align="center">&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</div></td>
				</tr>
				<tr>
				<td> &#2438;&#2439; &#2447;&#2475; &#2447;, &#2489;&#2494;&#2468; &#2471;&#2507;&#2527;&#2494; , <br>&#2478;&#2494;&#2527;&#2503;&#2480; &#2474;&#2497;&#2487;&#2509;&#2463;&#2495; <br>&#2476;&#2495;&#2487;&#2527;&#2453; &#2453;&#2494;&#2441;&#2472;&#2509;&#2488;&#2503;&#2482;&#2495;&#2434; </td>
				 <td><input type="number" class="form-control mis3_field" id="nutrition_counseling_center"></td>
				 <td><input type="number" class="form-control mis3_field" id="nutrition_counseling_satelite"></td>
		 
				</tr>
		
					<!--<tr>
					<td> &#2453;&#2468;&#2460;&#2472; &#2478;&#2494;&#2527;&#2503;&#2480; <br>(&#2455;&#2480;&#2509;&#2477;&#2476;&#2468;&#2495; &#2478;&#2489;&#2495;&#2482;&#2494;) <br>&#2451;&#2460;&#2472; &#2472;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; </td>
					 <td><input type="number" class="form-control mis3_field" id="pregnent_women_weight_taken_center"></td>
					 <td><input type="number" class="form-control mis3_field" id="pregnent_women_weight_taken_satelite"></td>
					</tr>-->
					
					<tr>
					<td> &#2438;&#2439;&#2451;&#2527;&#2494;&#2439;&#2488;&#2495; &#2447;&#2475; ,<br> &#2477;&#2495;&#2463;&#2494;&#2478;&#2495;&#2472; - &#2447; <br>&#2476;&#2495;&#2487;&#2527;&#2453; &#2478;&#2494;&#2527;&#2503;&#2480; &#2453;&#2494;&#2441;&#2472;&#2509;&#2488;&#2503;&#2482;&#2495;&#2434; </td>
					<td><input type="number" class="form-control mis3_field" id="iycf_counseling_center"></td>
					<td><input type="number" class="form-control mis3_field" id="iycf_counseling_satelite"></td>
					</tr>
					
					<!--<tr>
					<td>  &#2453;&#2468;&#2460;&#2472; &#2455;&#2480;&#2509;&#2477;&#2476;&#2468;&#2495; &#2478;&#2494;&#2453;&#2503; <br>&#2438;&#2527;&#2480;&#2472;, &#2475;&#2482;&#2495;&#2453; &#2447;&#2488;&#2495;&#2465;<br> &#2447;&#2476;&#2434; &#2453;&#2509;&#2479;&#2494;&#2482;&#2488;&#2495;&#2527;&#2494;&#2478; <br>&#2476;&#2524;&#2495; &#2470;&#2503;&#2451;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;</td>
					 <td><input type="number" class="form-control mis3_field" id="pregwomen_ifa_given_center"></td>
					 <td><input type="number" class="form-control mis3_field" id="pregwomen_ifa_given_satelite"></td>
					</tr>-->
					
					<tr>
					<td>&#2453;&#2468;&#2460;&#2472; &#2534; - &#2536;&#2537; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; <br>&#2486;&#2495;&#2486;&#2497;&#2480; <br>&#2478;&#2494;&#2453;&#2503; &#2438;&#2527;&#2480;&#2472;<br> &#2475;&#2482;&#2495;&#2453; &#2447;&#2488;&#2495;&#2465; &#2447;&#2476;&#2434; &#2453;&#2509;&#2479;&#2494;&#2482;&#2488;&#2495;&#2527;&#2494;&#2478; <br>&#2476;&#2524;&#2495; &#2470;&#2503;&#2451;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; </td>
					<td><input type="number" class="form-control mis3_field" id="0-23_month_monther_ifa_given_center"></td>
					<td><input type="number" class="form-control mis3_field" id="0-23_month_monther_ifa_given_satelite"></td>
					</tr>
					
					<tr>
					<td >&#2453;&#2468;&#2460;&#2472; &#2540;-&#2536;&#2537; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;<br> &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2478;&#2494;&#2482;&#2509;&#2463;&#2495;&#2474;&#2482;<br> &#2478;&#2494;&#2439;&#2453;&#2509;&#2480;&#2507;&#2472;&#2495;&#2441;&#2463;&#2509;&#2480;&#2495;&#2527;&#2503;&#2472;&#2509;&#2463; &#2474;&#2494;&#2441;&#2465;&#2494;&#2480; <br>(&#2447;&#2478;&#2447;&#2472;&#2474;&#2495;) &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;</td>
					   <td><input type="number" class="form-control mis3_field" id="6-23_month_child_mnp_given_center"></td>
						<td><input type="number" class="form-control mis3_field" id="6-23_month_child_mnp_given_satelite"></td>
					</tr>
					
					<tr>
					<td>&#2453;&#2468;&#2460;&#2472; &#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; <br> &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2477;&#2495;&#2463;&#2494;&#2478;&#2495;&#2472; - &#2447; <br>&#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;</td>
					<td><input type="number" class="form-control mis3_field" id="6-59_month_child_vitamin-a_given_center"></td>
					<td><input type="number" class="form-control mis3_field" id="6-59_month_child_vitamin-a_given_satelite"></td>
					</tr>
					
					<tr>
					<td> &#2453;&#2468;&#2460;&#2472; &#2536;&#2538;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;  <br> &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2453;&#2499;&#2478;&#2495; &#2472;&#2494;&#2486;&#2453;<br> &#2476;&#2524;&#2495; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; </td>
					<td><input type="number" class="form-control mis3_field" id="24-59_month_child_wormpills_given_center"></td>
					<td><input type="number" class="form-control mis3_field" id="24-59_month_child_wormpills_given_satelite"></td>
					</tr>
					
					<tr>
					<td> &#2453;&#2468;&#2460;&#2472; &#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; <br>&#2476;&#2527;&#2488;&#2496; &#2465;&#2494;&#2527;&#2480;&#2495;&#2527;&#2494; <br>&#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2454;&#2494;&#2476;&#2494;&#2480; &#2488;&#2509;&#2479;&#2482;&#2494;&#2439;&#2472;&#2503;&#2480; <br> &#2488;&#2494;&#2469;&#2503; &#2460;&#2495;&#2434;&#2453;  &#2476;&#2524;&#2495; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; </td>
						<td><input type="number" class="form-control mis3_field" id="6-59_child_saline_given_center"></td>
						<td><input type="number" class="form-control mis3_field" id="6-59_child_saline_given_satelite"></td>
					</tr>
					
					<tr>
					<td >&#2453;&#2468;&#2460;&#2472; &#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; <br>&#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2455;&#2509;&#2480;&#2507;&#2469; &#2478;&#2472;&#2495;&#2463;&#2480;&#2495;&#2434;<br> (GMP *) &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; 
					</td>
					<td ><input type="number" class="form-control mis3_field" id="6-59_gmp_center"></td>
					<td ><input type="number" class="form-control mis3_field" id="6-59_gmp__satelite"></td>
					</tr>
					
					<tr>
					<td>&#2453;&#2468;&#2460;&#2472; &#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; MAM <br>(&#2478;&#2494;&#2461;&#2494;&#2480;&#2495; &#2468;&#2495;&#2476;&#2509;&#2480; &#2437;&#2474;&#2497;&#2487;&#2509;&#2463;&#2495;) &#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468;  <br>&#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2447;&#2476;&#2434; <br>&#2458;&#2495;&#2453;&#2495;&#2510;&#2488;&#2494; &#2474;&#2509;&#2480;&#2470;&#2494;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;  
					</td>
					 <td><input type="number" class="form-control mis3_field" id="mam_center"></td>
					 <td><input type="number" class="form-control mis3_field" id="mam_satelite"></td>
					</tr>
					
					<tr>
					<td>&#2453;&#2468;&#2460;&#2472; &#2534;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; <br>SAM( &#2478;&#2494;&#2480;&#2468;&#2509;&#2476;&#2453; &#2468;&#2495;&#2476;&#2509;&#2480; &#2437;&#2474;&#2497;&#2487;&#2509;&#2463;&#2495; ) &#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503;<br> &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; &#2447;&#2476;&#2434; &#2480;&#2503;&#2475;&#2494;&#2480; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;
					</td>
					<td ><input type="number" class="form-control mis3_field" id="sam_center"></td>
					<td ><input type="number" class="form-control mis3_field" id="sam_satelite"></td>
					</tr>
					
					<tr>
					<td>&#2453;&#2468;&#2460;&#2472; &#2534;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2480; <br>
					Stunting ( &#2476;&#2527;&#2488;&#2503;&#2480; &#2468;&#2497;&#2482;&#2472;&#2494;&#2527; &#2453;&#2478; &#2441;&#2458;&#2509;&#2458;&#2468;&#2494; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472; )<br> 
					&#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;
					</td>
					 <td><input type="number" class="form-control mis3_field" id="stunting_center"></td>
					 <td><input type="number" class="form-control mis3_field" id="stunting_satelite"></td>
					</tr>
					
					<tr>
					<td>&#2453;&#2468;&#2460;&#2472; &#2534;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2480;  <br>wasting( &#2441;&#2458;&#2509;&#2458;&#2468;&#2494;&#2480; &#2468;&#2497;&#2482;&#2472;&#2494;&#2527; &#2453;&#2478; &#2451;&#2460;&#2472; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472; )<br> &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; 
					</td>
					 <td><input type="number" class="form-control mis3_field" id="wasting_center"></td>
					 <td><input type="number" class="form-control mis3_field" id="wasting_satelite"></td>
					</tr>
					
					<tr>
					<td>&#2453;&#2468;&#2460;&#2472; &#2534;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2480; <br>Underweight (&#2476;&#2527;&#2488;&#2503;&#2480; &#2468;&#2497;&#2482;&#2472;&#2494;&#2527; &#2453;&#2478; &#2451;&#2460;&#2472; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472; )<br> 
					&#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; 
					</td>
					 <td><input type="number" class="form-control mis3_field" id="under_weight_center"></td>
						<td><input type="number" class="form-control mis3_field" id="under_weight_satelite"></td>
					</tr>
				</table>
				</div>
			</li>

			<!--remove অসুস্থ শিশুর সমন্বিত চিকিৎসা ব্যবস্থাপনা (IMCI) -->
			<li class="list-group-item">
				<%--<div class="fa fa-plus">&nbsp;<b>&#2437;&#2488;&#2497;&#2488;&#2509;&#2469; &#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2478;&#2472;&#2509;&#2476;&#2495;&#2468; &#2458;&#2495;&#2453;&#2495;&#2510;&#2488;&#2494; &#2476;&#2509;&#2479;&#2476;&#2488;&#2509;&#2469;&#2494;&#2474;&#2472;&#2494; (IMCI)</b></div>--%>
				<div class="table-responsive">
			<%--<table class="table table-bordered"  cellspacing="0" style="margin-top: 10px; font-size: 13px;word-wrap:break-word; table-layout: fixed;width: 100%;">--%>
			<%----%>
			<%--<tr >--%>
				<%--<td rowspan="3"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 10px;margin-top: 40px;">&#2453;&#2509;&#2480;&#2478;&#2495;&#2453; &#2472;&#2434;</span></td>--%>
				<%--<td colspan="10">&#2486;&#2495;&#2486;&#2497; (&#2534;-&#2539;&#2543; &#2478;&#2494;&#2488;) &#2488;&#2503;&#2476;&#2494;</td>--%>
			<%--</tr>--%>
			<%--<tr >--%>
				<%--<td rowspan="2" colspan="2" style="text-align: center;border-top: 1px solid black;"> &#2480;&#2507;&#2455;&#2503;&#2480; <br>&#2472;&#2494;&#2478; </td>--%>
				<%--<td colspan="2">&#2534; - &#2536;&#2542;<br> &#2470;&#2495;&#2472; </td>--%>
				<%--<td colspan="2">&#2536;&#2543; - &#2539;&#2543; <br>&#2470;&#2495;&#2472; </td>--%>
				<%--<td colspan="2" >&#2536; &#2478;&#2494;&#2488; - &#2535; <br>&#2476;&#2459;&#2480; </td>--%>
				<%--<td colspan="2">&#2535;- &#2539; <br>&#2476;&#2459;&#2480;</td>--%>
			<%--</tr>--%>
			<%--<tr >--%>
				<%--<td >&#2459;&#2503;&#2482;&#2503; </td>--%>
				<%--<td >&#2478;&#2503;&#2527;&#2503;</td>--%>
				<%--<td >&#2459;&#2503;&#2482;&#2503; </td>--%>
				<%--<td >&#2478;&#2503;&#2527;&#2503;</td>--%>
				<%--<td >&#2459;&#2503;&#2482;&#2503; </td>--%>
				<%--<td >&#2478;&#2503;&#2527;&#2503;</td>--%>
				<%--<td >&#2459;&#2503;&#2482;&#2503; </td>--%>
				<%--<td >&#2478;&#2503;&#2527;&#2503;</td>--%>
			<%--</tr>--%>
			<%--<tr >--%>
				<%--<td>(&#2535;)</td>--%>
				<%--<td colspan="2" >(&#2536;)</td>--%>
				<%--<td>(&#2537;)</td>--%>
				<%--<td>(&#2538;)</td>--%>
				<%--<td>(&#2539;)</td>--%>
				<%--<td>(&#2540;)</td>--%>
				<%--<td>(&#2541;)</td>--%>
				<%--<td>(&#2542;)</td>--%>
				<%--<td>(&#2543;)</td>--%>
				<%--<td>(&#2535;&#2534;)</td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2535;.</td>--%>
				<%--<td colspan="2"> &#2454;&#2497;&#2476; &#2478;&#2494;&#2480;&#2494;&#2468;&#2509;&#2468;&#2453;<br> &#2480;&#2507;&#2455; - <br>&#2488;&#2434;&#2453;&#2494;&#2463;&#2474;&#2472;&#2509;&#2472; <br>&#2437;&#2488;&#2497;&#2488;&#2509;&#2469;&#2469;&#2494;</td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_0-28days_male" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_0-28days_female" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_29-59days_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_29-59days_female" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_2month-1year_male" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_2month-1year_female" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_1-5year_male" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_1-5year_female" ></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td >&#2536;.</td>--%>
				<%--<td colspan="2" > &#2454;&#2497;&#2476; &#2478;&#2494;&#2480;&#2494;&#2468;&#2509;&#2468;&#2453; <br>&#2480;&#2507;&#2455; - &#2488;&#2478;&#2509;&#2477;&#2494;&#2476;&#2509;&#2479;<br> &#2478;&#2494;&#2480;&#2494;&#2468;&#2509;&#2468;&#2453; <br>&#2488;&#2434;&#2453;&#2509;&#2480;&#2478;&#2472;</td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_infected_0-28days_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_infected_0-28days_female"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_infected_29-59days_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="very_serious_disease_infected_29-59days_female"></td>--%>
				<%--<!--<td><input type="text" class="form-control mis3_field" id="very_serious_disease_infected_2month-1year_male"  disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="very_serious_disease_infected_2month-1year_female"  disabled="disabled"></td>-->--%>
				<%--<td><input type="text" class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="very_serious_disease_infected_1-5year_male"  disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="very_serious_disease_infected_1-5year_female" disabled="disabled"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2537;.</td>--%>
				<%--<td colspan="2">&#2454;&#2497;&#2476; &#2478;&#2494;&#2480;&#2494;&#2468;&#2509;&#2468;&#2453; <br>&#2480;&#2507;&#2455; - &#2470;&#2509;&#2480;&#2497;&#2468; <br>&#2486;&#2509;&#2476;&#2494;&#2488;<br> &#2472;&#2495;&#2441;&#2478;&#2507;&#2472;&#2495;&#2527;&#2494;</td>--%>
				<%--<td ><input type="number" class="form-control mis3_field" id="very_serious_disease_neumonia_0-28days_male" ></td>--%>
				<%--<td ><input type="number" class="form-control mis3_field" id="very_serious_disease_neumonia_0-28days_female"></td>--%>
				<%--<td ><input type="number" class="form-control mis3_field" id="very_serious_disease_neumonia_29-59days_male"></td>--%>
				<%--<td ><input type="number" class="form-control mis3_field" id="very_serious_disease_neumonia_29-59days_female"></td>--%>
				<%----%>
				<%--<!--<td ><input type="text" class="form-control mis3_field" id="very_serious_disease_neumonia_2month-1year_male"  disabled="disabled"></td>--%>
				<%--<td ><input type="text" class="form-control mis3_field" id="very_serious_disease_neumonia_2month-1year_female" disabled="disabled"></td>--%>
				<%--<td ><input type="text" class="form-control mis3_field" id="very_serious_disease_neumonia_1-5year_male" disabled="disabled"></td>--%>
				<%--<td ><input type="text" class="form-control mis3_field" id="very_serious_disease_neumonia_1-5year_female"  disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td ><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td ><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td ><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td ><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2538;.</td>--%>
				<%--<td colspan="2"> &#2488;&#2509;&#2469;&#2494;&#2467;&#2496;&#2527; <br>&#2488;&#2496;&#2478;&#2495;&#2468; <br>&#2488;&#2434;&#2453;&#2509;&#2480;&#2478;&#2472;</td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="light_infection_0-28days_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="light_infection_0-28days_female" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="light_infection_29-59days_male" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="light_infection_29-59days_female" ></td>--%>
				<%----%>
				<%--<!--<td><input type="text" class="form-control mis3_field" id="light_infection_2month-1year_male" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="light_infection_2month-1year_female" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="light_infection_1-5year_male" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="light_infection_1-5year_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td><input type="text" class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field"  disabled="disabled"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2539;.</td>--%>
				<%--<td colspan="2" style="border-top: 1px solid black;">&#2480;&#2503;&#2475;&#2494;&#2480;&#2503; &#2480;&#2494;&#2460;&#2496; <br> &#2489;&#2451;&#2527;&#2494;</td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="refer_accepted_0-28days_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="refer_accepted_0-28days_female"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="refer_accepted_29-59days_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="refer_accepted_29-59days_female"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="refer_accepted_2month-1year_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="refer_accepted_2month-1year_female"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="refer_accepted_1-5year_male"></td>--%>
			<%--<!--	<td><input type="text" class="form-control mis3_field" id="refer_accepted_1-5year_female" disabled="disabled"></td>-->--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2540;.</td>--%>
				<%--<td  colspan="2">&#2535;&#2478; &#2465;&#2507;&#2460; <br>&#2438;&#2472;&#2509;&#2463;&#2495;&#2476;&#2494;&#2527;&#2463;&#2495;&#2453; <br>&#2439;&#2462;&#2509;&#2460;&#2503;&#2453;&#2486;&#2472; </td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="fst_dose_injection_0-28days_male" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="fst_dose_injection_0-28days_female" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="fst_dose_injection_29-59days_male" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="fst_dose_injection_29-59days_female" ></td>--%>
				<%----%>
				<%--<!--<td><input type="text" class="form-control mis3_field" id="fst_dose_injection_2month-1year_male" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="fst_dose_injection_2month-1year_female" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="fst_dose_injection_1-5year_male" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="fst_dose_injection_1-5year_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td >&#2541;.</td>--%>
				<%--<td colspan="2"> &#2536;&#2527; &#2465;&#2507;&#2460; <br>&#2438;&#2472;&#2509;&#2463;&#2495;&#2476;&#2494;&#2527;&#2463;&#2495;&#2453;<br> &#2439;&#2462;&#2509;&#2460;&#2503;&#2453;&#2486;&#2472;</td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="second_dose_injection_0-28days_male"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="second_dose_injection_0-28days_female" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="second_dose_injection_29-59days_male" ></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="second_dose_injection_29-59days_female" ></td>--%>
				<%----%>
				<%--<!--<td><input type="text" class="form-control mis3_field" id="second_dose_injection_2month-1year_male" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="second_dose_injection_2month-1year_female" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" id="second_dose_injection_1-5year_male" disabled="disabled"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" id="second_dose_injection_1-5year_female" disabled="disabled"></td>-->--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td><input type="text" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td><input type="number" class="form-control mis3_field" disabled="disabled"></td>--%>
			<%--</tr>--%>

			<%--<tr >--%>
				<%--<td> &#2542;.</td>--%>
				<%--<td  colspan="2">&#2472;&#2495;&#2441;&#2478;&#2507;&#2472;&#2495;&#2527;&#2494;</td>--%>
				<%--<!--<td><input type="number" class="form-control mis3_field" id="pneumonia_0-28days_male" disabled="disabled"></td>-->--%>
				<%--<td><input type="number" class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<!--<td><input type="number"  class="form-control mis3_field" id="pneumonia_0-28days_female" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="pneumonia_29-59days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="pneumonia_29-59days_female" disabled="disabled"></td>-->--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="pneumonia_2month-1year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="pneumonia_2month-1year_female"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="pneumonia_1-5year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="pneumonia_1-5year_female"></td>--%>
			<%--</tr>--%>

			<%--<tr>--%>
				<%--<td>&#2543;.</td>--%>
				<%--<td colspan="2"> &#2472;&#2495;&#2441;&#2478;&#2507;&#2472;&#2495;&#2527;&#2494; <br>&#2472;&#2527;, <br>&#2488;&#2480;&#2509;&#2470;&#2495; &#2453;&#2494;&#2486;&#2495; </td>--%>
				<%--<!--<td><input type="number"  class="form-control mis3_field" id="influenja_0-28days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="influenja_0-28days_female" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="influenja_29-59days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="influenja_29-59days_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="influenja_2month-1year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="influenja_2month-1year_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="influenja_1-5year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="influenja_1-5year_female"></td>--%>
			<%--</tr>--%>


			<%--<tr >--%>
				<%--<td >&#2535;&#2534;</td>--%>
				<%--<td colspan="2" >&#2465;&#2494;&#2527;&#2480;&#2495;&#2527;&#2494;</td>--%>
				<%--<!--<td><input type="number"  class="form-control mis3_field" id="diarrhea_0-28days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="diarrhea_0-28days_female" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="diarrhea_29-59days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="diarrhea_29-59days_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%----%>
				<%--<td><input type="number"  class="form-control mis3_field" id="diarrhea_2month-1year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="diarrhea_2month-1year_female"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="diarrhea_1-5year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="diarrhea_1-5year_female"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2535;&#2535;.</td>--%>
				<%--<td colspan="2">&#2460;&#2509;&#2476;&#2480; - <br>&#2478;&#2509;&#2479;&#2494;&#2482;&#2503;&#2480;&#2495;&#2527;&#2494; </td>--%>
				<%--<!--<td><input type="number"  class="form-control mis3_field" id="malaria_0-28days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malaria_0-28days_female" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malaria_29-59days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malaria_29-59days_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%----%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malaria_2month-1year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malaria_2month-1year_female"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malaria_1-5year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malaria_1-5year_female"></td>--%>
			<%--</tr>--%>
			<%--<tr >--%>
				<%--<td>&#2535;&#2536;.</td>--%>
				<%--<td colspan="2" >&#2460;&#2509;&#2476;&#2480; - <br>&#2478;&#2509;&#2479;&#2494;&#2482;&#2503;&#2480;&#2495;&#2527;&#2494; <br>&#2472;&#2527;</td>--%>
				<%--<!--<td ><input type="number"  class="form-control mis3_field" id="fever_not_malaria_0-28days_male" disabled="disabled"> </td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="fever_not_malaria_0-28days_female" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="fever_not_malaria_29-59days_male" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="fever_not_malaria_29-59days_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"> </td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="fever_not_malaria_2month-1year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="fever_not_malaria_2month-1year_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="fever_not_malaria_1-5year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="fever_not_malaria_1-5year_female"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2535;&#2537;.</td>--%>
				<%--<td colspan="2">&#2489;&#2494;&#2478;</td>--%>
				<%--<!--<td ><input type="number"  class="form-control mis3_field" id="measles_0-28days_male" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="measles_0-28days_female" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="measles_29-59days_male" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="measles_29-59days_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="measles_2month-1year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="measles_2month-1year_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="measles_1-5year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="measles_1-5year_female"></td>--%>
			<%--</tr>--%>
			<%--<tr >--%>
				<%--<td>&#2535;&#2538;.</td>--%>
				<%--<td colspan="2">&#2453;&#2494;&#2472;&#2503;&#2480; <br>&#2488;&#2478;&#2488;&#2509;&#2479;&#2494; </td>--%>
				<%--<!--<td ><input type="number"  class="form-control mis3_field" id="ear_problem_0-28days_male" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="ear_problem_0-28days_female" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="ear_problem_29-59days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="ear_problem_29-59days_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="ear_problem_2month-1year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="ear_problem_2month-1year_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="ear_problem_1-5year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="ear_problem_1-5year_female"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td >&#2535;&#2539;.</td>--%>
				<%--<td colspan="2" >&#2437;&#2474;&#2497;&#2487;&#2509;&#2463;&#2495;</td>--%>
				<%--<!--<td><input type="number"  class="form-control mis3_field" id="malnutrition_0-28days_male" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malnutrition_0-28days_female" disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malnutrition_29-59days_male" disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="malnutrition_29-59days_female" disabled="disabled"></td>-->--%>
				<%----%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field"  disabled="disabled"></td>--%>
				<%----%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="malnutrition_2month-1year_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="malnutrition_2month-1year_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="malnutrition_1-5year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="malnutrition_1-5year_female"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td >&#2535;&#2540;.</td>--%>
				<%--<td colspan="2">&#2437;&#2472;&#2509;&#2479;&#2494;&#2472;&#2509;&#2479;</td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="other_disease_0-28days_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="other_disease_0-28days_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="other_disease_29-59days_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="other_disease_29-59days_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="other_disease_2month-1year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="other_disease_2month-1year_female"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="other_disease_1-5year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="other_disease_1-5year_female"></td>--%>
			<%--</tr>--%>
			<%--<tr>--%>
				<%--<td>&#2535;&#2541;.</td>--%>
				<%--<td colspan="2" >&#2480;&#2503;&#2475;&#2494;&#2480;&#2453;&#2499;&#2468;</td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="imci_referred_0-28days_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="imci_referred_0-28days_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="imci_referred_29-59days_male"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="imci_referred_29-59days_female"></td>--%>
				<%--<td><input type="number"  class="form-control mis3_field" id="imci_referred_2month-1year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="imci_referred_2month-1year_female"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="imci_referred_1-5year_male"></td>--%>
				<%--<td ><input type="number"  class="form-control mis3_field" id="imci_referred_1-5year_female"></td>--%>
			<%--</tr>--%>

  			<%--</table>--%>
			<br>
				<table border="0" cellpadding="0" cellspacing="0" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;" class="table table-bordered">
				<tr>
				  <td>FWV &#2453;&#2480;&#2509;&#2468;&#2499;&#2453;  <br>&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463; <br>&#2453;&#2509;&#2482;&#2495;&#2472;&#2495;&#2453;&#2435; &#2482;&#2453;&#2509;&#2487;&#2509;&#2479;&#2478;&#2494;&#2468;&#2509;&#2480;&#2494;</td>
				  <td><input type="number" id="terget_clinic" class="form-control mis3_field"></td>
				  <td> &#2437;&#2472;&#2497;&#2487;&#2509;&#2464;&#2495;&#2468; </td>
				  <td><input type="number" id="achieved_clinic" class="form-control mis3_field"></td>
				</tr>
				<tr>
				  <td>SACMO &#2453;&#2480;&#2509;&#2468;&#2499;&#2453; NSV <br> &#2453;&#2509;&#2482;&#2494;&#2527;&#2503;&#2472;&#2509;&#2463; &#2480;&#2503;&#2475;&#2494;&#2480; : <br>&#2482;&#2453;&#2509;&#2487;&#2509;&#2479;&#2478;&#2494;&#2468;&#2509;&#2480;&#2494;</td>
					<td><input type="number" id="terget_sacmo_nsv_client_refer" class="form-control mis3_field"></td>
					<td> &#2437;&#2480;&#2509;&#2460;&#2472;</td>
					<td><input type="number" id="achieved_sacmo_nsv_client_refer" class="form-control mis3_field"></td>
				</tr>
			</table>
				</div>
			</li>
			
			<li class="list-group-item">
				<div class="fa fa-plus">&nbsp;<b>&#2478;&#2494;&#2488;&#2495;&#2453; &#2478;&#2451;&#2460;&#2497;&#2470; &#2451; &#2476;&#2495;&#2468;&#2480;&#2467;&#2503;&#2480; &#2489;&#2495;&#2488;&#2494;&#2476; &#2476;&#2495;&#2487;&#2527;&#2453;</b></div>
			  <div class="table-responsive">
				<table border="0" cellpadding="3" cellspacing="0" class="table table-bordered" style="font-size:12px;word-wrap:break-word; table-layout: fixed;width: 100%;">
				<tr>
				<td colspan="2" rowspan="2" bgcolor="#F2F2F9" >&nbsp;</td>
				<td width="14%" rowspan="2" bgcolor="#F2F2F9"><div align="center">&#2474;&#2498;&#2480;&#2509;&#2476;&#2503;&#2480;<br> &#2478;&#2451;&#2460;&#2497;&#2470;</div></td>
				<td width="10%" rowspan="2" bgcolor="#F2F2F9">&#2458;&#2482;&#2468;&#2495; &#2478;&#2494;&#2488;&#2503;<br>
				  &#2474;&#2494;&#2451;&#2527;&#2494; &#2455;&#2503;&#2459;&#2503;<br> (+)</td>
				
				<td colspan="2" bgcolor="#F2F2F9"><div align="center">&#2488;&#2478;&#2472;&#2509;&#2476;&#2527;</div></td>
				<td width="14%" rowspan="2" bgcolor="#F2F2F9">&#2458;&#2482;&#2468;&#2495; &#2478;&#2494;&#2488;&#2503; <br>
				  &#2476;&#2495;&#2468;&#2480;&#2472; &#2453;&#2480;&#2494; <br>&#2489;&#2527;&#2503;&#2459;&#2503; (-)</td>
				<td width="14%" rowspan="2" bgcolor="#F2F2F9">&#2458;&#2482;&#2468;&#2495; &#2478;&#2494;&#2488;&#2503;<br>
				  &#2453;&#2454;&#2472;&#2451; &#2478;&#2451;&#2460;&#2497;&#2470;<br> &#2486;&#2498;&#2467;&#2509;&#2479; &#2489;&#2527;&#2503; <br>&#2469;&#2494;&#2453;&#2482;&#2503; (&#2453;&#2507;&#2465;)</td>
				</tr>
				<tr>
				  <td width="14%"  bgcolor="#F2F2F9" ><div align="center">(+)</div></td>
				  <td width="14%"  bgcolor="#F2F2F9" ><div align="center">(-)</div></td>
			   </tr>
				<tr>
				<td width="10%" rowspan="2" >&#2454;&#2494;&#2476;&#2494;&#2480; &#2476;&#2524;&#2495;<br> 
				  (&#2458;&#2453;&#2509;&#2480;)</td>
				<td width="9%">&#2488;&#2497;&#2454;&#2495;</td>
				<td><input type="number" class="form-control mis3_field" id="previous_stock_shukhi" value="<%= jsonObj.has("remaining_stock_shukhi")? jsonObj.getInt("remaining_stock_shukhi")>0? jsonObj.getInt("remaining_stock_shukhi"):0:0 %>"></td>
				<td><input type="number" class="form-control mis3_field" id="stock_in_shukhi"></td>
				<!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_shukhi" disabled="disabled">-->
				
				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_shukhi"></td>
				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_shukhi"></td>
				<td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				<td><input type="number" class="form-control mis3_field" id="zero_stock_code_shukhi"></td>
				</tr>
				
				<tr>
				<td>&#2438;&#2474;&#2472;</td>
				<td><input type="number" class="form-control mis3_field" id="previous_stock_apon" value="<%= jsonObj.has("remaining_stock_apon")? jsonObj.getInt("remaining_stock_apon")>0? jsonObj.getInt("remaining_stock_apon"):0:0 %>"></td>
				<td><input type="number" class="form-control mis3_field" id="stock_in_apon"></td>	
				<!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_apon" disabled="disabled" ></td>	-->
				
				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_apon"></td>
				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_apon"></td>
				<td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				<td><input type="number" class="form-control mis3_field" id="zero_stock_code_apon"></td>
				</tr>
		
				<tr>
				<td colspan="2">&#2453;&#2472;&#2465;&#2478; (&#2474;&#2495;&#2488;)</td>
				<td ><input type="number" class="form-control mis3_field" id="previous_stock_condom" value="<%= jsonObj.has("remaining_stock_condom")? jsonObj.getInt("remaining_stock_condom")>0? jsonObj.getInt("remaining_stock_condom"):0:0 %>"></td>
				<td><input type="number" class="form-control mis3_field" id="stock_in_condom"></td>
				<!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_condom" disabled="disabled" ></td>	-->	

				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_condom"></td>
				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_condom"></td>
				<td ><input type="number" class="form-control mis3_field" disabled="disabled" ></td>
				<td ><input type="number" class="form-control mis3_field" id="zero_stock_code_condom"></td>
				</tr>
		
				<tr>
				<td rowspan="2" >&#2439;&#2472;&#2460;&#2503;<br>&#2453;&#2463;&#2503;&#2476;&#2482;</td>
				<td>&#2477;&#2494;&#2527;&#2494;&#2482; </td>
				<td><input type="number" class="form-control mis3_field" id="previous_stock_viale" value="<%= jsonObj.has("remaining_stock_viale")? jsonObj.getInt("remaining_stock_viale")>0? jsonObj.getInt("remaining_stock_viale"):0 :0%>"></td>
				<td><input type="number" class="form-control mis3_field" id="stock_in_viale"></td>	
				<!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_viale" disabled="disabled"></td>-->
			
				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_viale"></td>
				<td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_viale"></td>
				<td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				<td><input type="number" class="form-control mis3_field" id="zero_stock_code_viale"></td>
				</tr>
				<tr>
				  <td>&#2488;&#2495;&#2480;&#2495;&#2462;&#2509;&#2460;</td>
				  <td><input type="number" class="form-control mis3_field" id="previous_stock_syringe" value="<%= jsonObj.has("remaining_stock_syringe")? jsonObj.getInt("remaining_stock_syringe")>0? jsonObj.getInt("remaining_stock_syringe"):0 :0%>"></td>
				  <td><input type="number" class="form-control mis3_field" id="stock_in_syringe"></td>
				  <!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_syringe" disabled="disabled"></td>-->
				 
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_syringe"></td>
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_syringe"></td>
				  <td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				  <td><input type="number" class="form-control mis3_field" id="zero_stock_code_syringe"></td>
			   </tr>
				<tr>
				  <td colspan="2" >&#2438;&#2439; &#2439;&#2441; &#2465;&#2495; (&#2474;&#2495;&#2488;)</td>
				  <td ><input type="number" class="form-control mis3_field" id="previous_stock_iud" value="<%= jsonObj.has("remaining_stock_iud")? jsonObj.getInt("remaining_stock_iud")>0? jsonObj.getInt("remaining_stock_iud"):0 :0%>"></td>
				  <td ><input type="number" class="form-control mis3_field" id="stock_in_iud"></td>
				 <!-- <td ><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_iud" disabled="disabled"></td>-->
				 
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_iud"></td> 
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_iud"></td>
				  <td ><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				  <td ><input type="number" class="form-control mis3_field" id="zero_stock_code_iud"></td>
			   </tr>
				<tr>
				  <td rowspan="2" >
				  &#2439;&#2478;&#2474;&#2509;&#2482;&#2509;&#2479;&#2494;&#2472;&#2509;&#2463; <br>(&#2488;&#2503;&#2463;)</td>
				  <td>&#2439;&#2478;&#2474;&#2509;&#2482;&#2509;&#2479;&#2494;&#2472;&#2472;</td>
				  <td><input type="number" class="form-control mis3_field" id="previous_stock_implanon" value="<%= jsonObj.has("remaining_stock_implanon")? jsonObj.getInt("remaining_stock_implanon")>0? jsonObj.getInt("remaining_stock_implanon"):0:0 %>"></td>
				  <td><input type="number" class="form-control mis3_field" id="stock_in_implanon"></td>
				 <!-- <td><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_implanon" disabled="disabled"></td>-->
				  
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_implanon"></td>
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_implanon"></td>
				  <td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				  <td><input type="number" class="form-control mis3_field" id="zero_stock_code_implanon"></td>
			   </tr>
				<tr>
				  <td>&#2460;&#2503;&#2465;&#2503;&#2482;</td>
				  <td ><input type="number" class="form-control mis3_field" id="previous_stock_jadelle" value="<%= jsonObj.has("remaining_stock_jadelle")? jsonObj.getInt("remaining_stock_jadelle")>0? jsonObj.getInt("remaining_stock_jadelle"):0 :0%>"></td>
				  <td ><input type="number" class="form-control mis3_field" id="stock_in_jadelle"></td>
				 <!-- <td ><input type="number" class="form-control mis3_field" id="cmonth_distribution_in_jadelle" disabled="disabled"></td>-->
				 
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_jadelle"></td>
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_jadelle"></td>
				  <td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				   <td><input type="number" class="form-control mis3_field" id="zero_stock_code_jadelle"></td>
			   </tr>
				<tr>
				  <td colspan="2">&#2439;&#2488;&#2495;&#2474;&#2495; (&#2465;&#2507;&#2460;)</td>
				  <td ><input type="number" class="form-control mis3_field" id="previous_stock_ecp" value="<%= jsonObj.has("remaining_stock_ecp")? jsonObj.getInt("remaining_stock_ecp")>0? jsonObj.getInt("remaining_stock_ecp"):0 :0%>"></td>
				  <td ><input type="number" class="form-control mis3_field" id="stock_in_ecp"></td>
				  
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_ecp"></td>
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_ecp"></td>
				  <td ><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				   <td ><input type="number" class="form-control mis3_field" id="zero_stock_code_ecp"></td>
			   </tr>
				<tr>
				  <td colspan="2">&#2478;&#2495;&#2488;&#2507;&#2474;&#2509;&#2480;&#2507;&#2488;&#2509;&#2463;&#2482;<br>(&#2465;&#2507;&#2460;)</td>
				  <td><input type="number" class="form-control mis3_field" id="previous_stock_misoprostol" value="<%= jsonObj.has("remaining_stock_misoprostol")? jsonObj.getInt("remaining_stock_misoprostol")>0? jsonObj.getInt("remaining_stock_misoprostol"):"0":"0" %>"></td>
				  <td><input type="number" class="form-control mis3_field" id="stock_in_misoprostol"></td>
				 <!-- <td><input type="number" class="form-control mis3_field" id="cmonth_distribution_misoprostol" disabled="disabled"></td>-->
				  
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_misoprostol"></td>
				  <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_misoprostol"></td>
				  <td ><input type="number" class="form-control mis3_field"  disabled="disabled"></td>
				    <td ><input type="number" class="form-control mis3_field" id="zero_stock_code_misoprostol"></td>
			   </tr>
				<tr>
				  <td colspan="2">&#2447;&#2478; &#2438;&#2480; &#2447;&#2478; (&#2474;&#2509;&#2479;&#2494;&#2453;)</td>
				  <td><input type="number" class="form-control mis3_field" id="previous_stock_mrm_pac" value="<%= jsonObj.has("remaining_stock_mrm_pac")? jsonObj.getInt("remaining_stock_mrm_pac")>0? jsonObj.getInt("remaining_stock_mrm_pac"):"0":"0" %>"></td>
				  <td><input type="number" class="form-control mis3_field" id="stock_in_mrm_pac"></td>
				  
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_mrm_pac"></td>
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_mrm_pac"></td>
				  <td><input type="number" class="form-control mis3_field" id="cmonth_distribution_mrm_pac"></td>
				   <td><input type="number" class="form-control mis3_field" id="zero_stock_code_mrm_pac"></td>
			   </tr>
				<tr>
				  <td colspan="2">&#2541;.&#2535;% &#2453;&#2509;&#2482;&#2507;&#2480;&#2507;&#2489;&#2503;&#2453;&#2509;&#2488;&#2495;&#2465;&#2495;&#2482;&#2495;&#2472; </td>
				  <td><input type="number" class="form-control mis3_field" id="previous_stock_chlorehexidin" value="<%= jsonObj.has("remaining_stock_chlorehexidin")? jsonObj.getInt("remaining_stock_chlorehexidin")>0? jsonObj.getInt("remaining_stock_chlorehexidin"):"0" :"0"%>"></td>
				  <td><input type="number" class="form-control mis3_field" id="stock_in_chlorehexidin"></td>
				
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_chlorehexidin"></td>
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_chlorehexidin"></td>
				  <td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				  <td><input type="number" class="form-control mis3_field" id="zero_stock_code_chlorehexidin"></td>
			   </tr>
				<tr>
				  <td colspan="2" >&#2439;&#2472;&#2460;&#2503;&#2453;&#2486;&#2472; MgSO4</td>
				  <td><input type="number" class="form-control mis3_field" id="previous_stock_mgso4" value="<%= jsonObj.has("remaining_stock_mgso4")? jsonObj.getInt("remaining_stock_mgso4")>0? jsonObj.getInt("remaining_stock_mgso4"):0:0 %>"></td>
				  <td><input type="number" class="form-control mis3_field" id="stock_in_mgso4"></td>
				  <!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_mgso4" disabled="disabled"></td>-->
				
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_mgso4"></td>
				  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_mgso4"></td>
				  <td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
				    <td><input type="number" class="form-control mis3_field" id="zero_stock_code_mgso4"></td>
			   </tr>
		<tr>

		  <td colspan="2" >&#2439;&#2472;&#2460;&#2503;&#2453;&#2486;&#2472; <br>&#2437;&#2453;&#2509;&#2488;&#2495;&#2463;&#2507;&#2453;&#2509;&#2488;&#2495;&#2472; <br> (&#2465;&#2507;&#2460;)</td>
		  <td><input type="number" class="form-control mis3_field" id="previous_stock_oxytocin" value="<%= jsonObj.has("remaining_stock_oxytocin")? jsonObj.getInt("remaining_stock_oxytocin")>0? jsonObj.getInt("remaining_stock_oxytocin"):0 :0%>"></td>
		  <td><input type="number" class="form-control mis3_field" id="stock_in_oxytocin"></td>
		  <!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_oxytocin" disabled="disabled"></td>-->
		  
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_oxytocin"></td>
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_oxytocin"></td>
	      <td><input type="number" class="form-control mis3_field"  disabled="disabled"></td>
		  <td><input type="number" class="form-control mis3_field" id="zero_stock_code_oxytocin"></td>
       </tr>
		<tr>
		  <td colspan="2" >&#2447;&#2478;&#2447;&#2472;&#2474;&#2495; (&#2488;&#2509;&#2479;&#2494;&#2488;&#2503;&#2463;)</td>
		  <td><input type="number" class="form-control mis3_field" id="previous_stock_mnp" value="<%= jsonObj.has("remaining_stock_mnp")? (jsonObj.getInt("remaining_stock_mnp"))>0? jsonObj.getInt("remaining_stock_mnp"):0:0 %>"></td>
		  <td><input type="number" class="form-control mis3_field" id="stock_in_mnp"></td>
		 
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_mnp"></td>
	      <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_mnp"></td>
	      <td ><input type="number" class="form-control mis3_field" id="cmonth_distribution_mnp"></td>
		  <td ><input type="number" class="form-control mis3_field" id="zero_stock_code_mnp"></td>
       </tr>
		<tr>
		  <td colspan="2" >&#2488;&#2509;&#2479;&#2494;&#2472;&#2495;&#2463;&#2494;&#2480;&#2496; &#2474;&#2509;&#2479;&#2494;&#2465; <br>(&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</td>
		  <td ><input type="number" class="form-control mis3_field" id="previous_stock_sanitary_pad" value="<%= jsonObj.has("remaining_stock_sanitary_pad")? jsonObj.getInt("remaining_stock_sanitary_pad")>0? jsonObj.getInt("remaining_stock_sanitary_pad"):0:0 %>"></td>
		  <td ><input type="number" class="form-control mis3_field" id="stock_in_sanitary_pad"></td>
		  <!--<td ><input type="number" class="form-control mis3_field" id="cmonth_distribution_sanitary_pad" disabled="disabled"></td>-->
	      
		  <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_sanitary_pad"></td>
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_sanitary_pad"></td>
	      <td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
		   <td ><input type="number" class="form-control mis3_field" id="zero_stock_code_sanitary_pad"></td>
       </tr>
		<tr>
		  <td colspan="2" >&#2447;&#2478; &#2438;&#2480; (&#2447;&#2478;&#2477;&#2495;&#2447;) <br>&#2453;&#2495;&#2463;<br> (&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</td>
		  <td ><input type="number" class="form-control mis3_field" id="previous_stock_mr_kit" value="<%= jsonObj.has("remaining_stock_mr_kit")? jsonObj.getInt("remaining_stock_mr_kit")>0? jsonObj.getInt("remaining_stock_mr_kit"):0:0 %>"></td>
		  <td ><input type="number" class="form-control mis3_field" id="stock_in_mr_kit"></td>
		  
	      <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_mr_kit"></td>
	      <td ><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_mr_kit"></td>
	      <td ><input type="number" class="form-control mis3_field" id="cmonth_distribution_mrm_kit"></td>
		  <td ><input type="number" class="form-control mis3_field" id="zero_stock_code_mr_kit"></td>
       </tr>
		<tr>
		  <td colspan="2" >&#2465;&#2503;&#2482;&#2495;&#2477;&#2494;&#2480;&#2496; &#2453;&#2495;&#2463; <br>(&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</td>
		  <td><input type="number" class="form-control mis3_field" id="previous_stock_delivery_kit" value="<%= jsonObj.has("remaining_stock_delivery_kit")? jsonObj.getInt("remaining_stock_delivery_kit")>0? jsonObj.getInt("remaining_stock_delivery_kit"):0:0 %>"></td>
		  <td><input type="number" class="form-control mis3_field" id="stock_in_delivery_kit"></td>
		  
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_delivery_kit"></td>
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_delivery_kit"></td>
	      <td><input type="number" class="form-control mis3_field" id="cmonth_distribution_delivery_kit"></td>
		   <td><input type="number" class="form-control mis3_field" id="zero_stock_code_delivery_kit"></td>
       </tr>
		<tr>
		  <td colspan="2" >&#2465;&#2495;&#2465;&#2495;&#2447;&#2488; &#2453;&#2495;&#2463; <br>(&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</td>
		  <td><input type="number" class="form-control mis3_field" id="previous_stock_dds_kit" value="<%= jsonObj.has("remaining_stock_dds_kit")? jsonObj.getInt("remaining_stock_dds_kit")>0? jsonObj.getInt("remaining_stock_dds_kit"):0:0 %>"></td>
		  <td><input type="number" class="form-control mis3_field" id="stock_in_dds_kit"></td>
		 
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_plus_dds_kit"></td>
	      <td><input type="number" class="form-control mis3_field" id="adjustment_stock_minus_dds_kit"></td>
	      <td><input type="number" class="form-control mis3_field" id="cmonth_distribution_dds_kit"></td>
		  <td><input type="number" class="form-control mis3_field" id="zero_stock_code_dds_kit"></td>
       </tr>
		<tr>
		  <td colspan="2"> &#2439;&#2472;&#2460;&#2503;&#2453;&#2486;&#2472; <br>&#2447;&#2509;&#2479;&#2494;&#2472;&#2509;&#2463;&#2495;&#2472;&#2503;&#2463;&#2494;&#2482; <br/>
   &#2453;&#2480;&#2463;&#2495;&#2453;&#2507;&#2488;&#2509;&#2463;&#2503;&#2480;&#2527;&#2503;&#2465; </td>
		  <td><input type="number" class="form-control mis3_field" id="previous_injection_antinatal" value="<%= jsonObj.has("remaining_injection_antinatal")? jsonObj.getInt("remaining_injection_antinatal")>0? jsonObj.getInt("remaining_injection_antinatal"):0:0 %>"></td>
		  <td><input type="number" class="form-control mis3_field" id="stock_injection_antinatal"></td>
		  <!--<td><input type="number" class="form-control mis3_field" id="cmonth_distribution_injection_antinatal" disabled="disabled"></td>-->
		
	      <td><input type="number" class="form-control mis3_field" id="adjust_stock_injection_plus_antinatal" ></td>
	      <td><input type="number" class="form-control mis3_field" id="adjust_stock_injection_minus_antinatal"></td>
	      <td><input type="number" class="form-control mis3_field" disabled="disabled"></td>
		  <td><input type="number" class="form-control mis3_field" id="zero_stock_injection_antinatal"></td>
       </tr>
		<tr>
		  <td colspan="2"> &#2439;&#2472;&#2460;&#2503;&#2453;&#2486;&#2472; <br/>
 &#2460;&#2503;&#2472;&#2509;&#2463;&#2494;&#2478;&#2494;&#2439;&#2488;&#2495;&#2472;</td>
		  <td><input type="number" class="form-control mis3_field" id="previous_injection_jentamysin" value="<%= jsonObj.has("remaining_injection_jentamysin")? jsonObj.getInt("remaining_injection_jentamysin")>0? jsonObj.getInt("remaining_injection_jentamysin"):0:0 %>"></td>
		  <td><input type="number" class="form-control mis3_field" id="stock_injection_jentamysin"></td>
		  
	      <td><input type="number" class="form-control mis3_field" id="adjustment_injection_plus_jentamysin"></td>
	      <td><input type="number" class="form-control mis3_field" id="adjustment_injection_minus_jentamysin"></td>
	      <td><input type="number" class="form-control mis3_field" id="cmonth_distribution_injection_jentamysin"></td>
		  <td><input type="number" class="form-control mis3_field" id="zero_stock_injection_jentamysin"></td>
       </tr>
		
  </table> 
		
	</div>
	</li>
 </ul>
<!-- Accordion end -->
</div>
<%}%>
<!--to view only webend not in tab format-->



<!--<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
-->	<script type="text/javascript" src="dist/accordion.js"></script>
	
	<script type="text/javascript">
		jQuery(document).ready(function($){

			$(".demo-accordion").accordionjs();

			// Demo text. Let's save some space to make the code readable. ;)
			<!--$('.acc_content').not('.nodemohtml').html('<p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Fusce aliquet neque et accumsan fermentum. Aliquam lobortis neque in nulla  tempus, molestie fermentum purus euismod.</p>');-->
		});
	</script>
</body>
</html>