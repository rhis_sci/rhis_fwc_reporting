<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>

<% int menuType = (Integer)request.getAttribute("type"); %>

<style>
	input[type="number"]::-webkit-outer-spin-button,
	input[type="number"]::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	input[type="number"] {
		-moz-appearance: textfield;
	}
</style>

<div class="remove_div">
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title" id="box_title">Provider Entry Form</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body search_form">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group col-md-4" id="facility_type" style="display: none;">
						<input type="hidden" name="menuType" id="menuType" value="<%= menuType %>" />
						<label>Facility Type</label>

						<select class="form-control select2" id="facilityType" style="width: 100%;">
						</select>
					</div>
					<div class="form-group col-md-4" id="retired_type" style="display: none;">
						<label>Status Type</label>
						<select class="form-control select2" id="retiredType" style="width: 100%;">
							<option value="">--Select Type--</option>
							<option value="1">Retired from System</option>
							<option value="2">Retired from Additional Responsibility</option>
							<option value="3">Long Term Leave</option>
							<option value="4">Return from Long Term Leave</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="leave_type" style="display: none;">
						<label>Leave Type</label>
						<select class="form-control select2" id="leaveType" style="width: 100%;">
							<option value="">--Select Type--</option>
							<option value="1">Maternal Leave</option>
							<option value="2">Training Leave</option>
							<option value="3">Long Term Sick Leave</option>
							<option value="4">Other</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="change_type" style="display: none;">
						<label>Updated By</label>
						<select class="form-control select2" id="changeType" onchange="show_dpu(this.value)" style="width: 100%;">
							<option value="">--Select Type--</option>
							<option value="1">District Wise</option>
							<option value="2">Upazila Wise</option>
							<option value="3">Union Wise</option>
							<option value="4">Provider Wise</option>
						</select>
					</div>
					<div class="form-group col-md-10" id="provider_type">
						<label>Provider Type</label>
						<select class="form-control select2" id="providerType" required="required">
							<option value="">--Select Provider Type--</option>
							<% int Type = (Integer)request.getAttribute("type"); %>
							<% int userType = (Integer)request.getAttribute("utype"); %>
							<% if(Type == 1 || Type == 6){ %>
							<% if(userType == 999){ %>
							<!-- <option value="999">Super User</option>
                             <option value="998">Admin</option>-->
							<% } %>
							<!-- <option value="997">Report Viewer</option>
                             <option value="21">Civil Surgeon</option>
                             <option value="20">DDFP</option>
                             <option value="14">MOMCH</option>
                             <option value="15">UFPO</option>
                             <option value="19">UFPA</option>
                             <option value="16">UHFPO</option>-->
							<% if(Type == 6){ %>
							<option value="0">0 - ALL</option>
							<%}%>
							<option value="4">4 - FWV</option>
							<option value="5">5 - S.A.C.M.O (FP)</option>
							<option value="6">6 - S.A.C.M.O (HS)</option>
							<option value="101">101 - PARAMEDIC</option>
							<option value="17">17 - Midwife</option>
							<% if(Type == 6){ %>
							<option value="2">2 - HA(CSBA)</option>
							<option value="3">3 - FWA(CSBA)</option>
							<!--<option value="22">Statistician_HS</option>
                            <option value="23">Statistician_FP</option>-->
							<% } } else if(Type == 11 || Type == 26)
							{
							%>
							<option value="999">999 - Super User</option>
							<option value="998">998 - Admin</option>
							<option value="994">994 - Admin MNE</option>
							<option value="995">995 - TroubleShooter</option>
							<option value="996">996 - Central Level </option>
							<option value="997">997 - Report Viewer</option>
							<option value="0">0 - Guest Viewer</option>
							<option value="21">21 - Civil Surgeon</option>
							<option value="20">20 - DDFP</option>
							<option value="14">14 - MOMCH</option>
							<option value="15">15 - UFPO</option>
							<option value="19">19 - UFPA</option>
							<option value="16">16 - UHFPO</option>
							<%
							}
							else{ %>
							<!-- <option value="3">FWA</option>
                             <option value="2">HA</option>-->
							<option value="2">2 - HA(CSBA)</option>
							<option value="3">3 - FWA(CSBA)</option>
							<% } %>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label>Division</label>
						<select class="form-control select2" id="RdivName" required="required" style="width: 100%;">
						</select>
					</div>
					<%
						if(Type == 1)
						{
					%>
					<div class="form-group col-md-4">
						<label>District</label>
						<select class="form-control select2" id="RzillaName" required="required" style="width: 100%;" onchange="return getProviderHints();">
						</select>
					</div>
					<%
					}
					else
					{
					%>
					<div class="form-group col-md-4">
						<label>District</label>
						<select class="form-control select2" id="RzillaName" required="required" style="width: 100%;">
						</select>
					</div>
					<%

						}
					%>

					<div class="form-group col-md-4" id="upazilaName">
						<label>Upazila</label>
						<select class="form-control select2" id="RupaZilaName" required="required" style="width: 100%;"></select>

					</div>
					<div class="form-group col-md-4" id="unionName">
						<label>Union</label>
						<select class="form-control select2" id="RunionName" required="required" style="width: 100%;">
						</select>

					</div>
					<div class="form-group col-md-4" id="unionFacilityName">
						<label>Facility Name</label>
						<input type="text" class="form-control" id="UnionFacilityName" required="required" placeholder="Facility Name">
					</div>
					<div class="form-group col-md-6" id="provider_facility_type" style="display: none;">
						<label>Facility Type</label>
						<select class="form-control select2 changeFacility" id="providerFacility" style="width: 100%;">
						</select>
					</div>
					<div class="form-group col-md-6" id="Pname">
						<label>Provider Name</label>
						<input type="text" class="form-control" id="ProvName" required="required" placeholder="Provider Name">
					</div>
					<div class="form-group col-md-6" id="Pmob">
						<label>Provider Mobile No.</label>
						<input type="text" maxlength="11" class="form-control" id="ProvMob" required="required" placeholder="Provider Mobile No.">
					</div>
					<div class="form-group col-md-6" id="provider_code">
						<label>Provider Code</label><div id="hints">
						<input type="number" class="form-control" id="ProvCode" required="required" placeholder="Provider Code"></div>
						<!--<input type="text" class="form-control" id="ProvCode" required="required">-->
						<!--<div class="form-group col-m3-6" id="hint"></div>-->
					</div>
					<div class="form-group col-md-6" id="Ppass">
						<label>Provider Password</label>
						<input type="text" class="form-control" id="ProvPass" required="required" placeholder="Provider Password">
					</div>
					<div class="form-group col-md-6" id="FacName">
						<label>Facility Name</label>
						<select class="form-control select2" id="facilityName" required="required">
							<option value="">--Select Facility Name--</option>

							<!-- <option value="ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যাণ কেন্দ্র">ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যাণ কেন্দ্র</option>
                            <option value="পরিবার কল্যাণ কেন্দ্র (বিভিন্ন)">পরিবার কল্যাণ কেন্দ্র (বিভিন্ন)</option>
                            <option value="ইউনিয়ন সাব-সেন্টার">ইউনিয়ন সাব-সেন্টার</option>
                            <option value="রুরাল ডিসপেন্সারি">রুরাল ডিসপেন্সারি</option>
                            <option value="উপজেলা স্বাস্থ্য কমপ্লেক্স">উপজেলা স্বাস্থ্য কমপ্লেক্স</option> -->
							<!-- <option value="কমিউনিটি ক্লিনিক">কমিউনিটি ক্লিনিক</option> -->
						</select>
					</div>
					<div class="form-group col-md-6" id="aav_provider_type">
						<label>Provider Type</label>
						<select class="form-control select2" id="aav_providerType" required="required">
							<option value="">ALL</option>
							<option value="4">FWV</option>
							<option value="5">S.A.C.M.O (FP)</option>
							<option value="6">S.A.C.M.O (HS)</option>
							<option value="101">PARAMEDIC</option>
							<option value="17">Midwife</option>
							<option value="2">HA(CSBA)</option>
							<option value="3">FWA(CSBA)</option>
						</select>
					</div>

					<div class="form-group col-md-6" id="provider_id" style="display: none">
						<label>Provider Name</label>
						<select class="form-control select2" id="providerID" required="required" style="width: 100%;">
							<option value="">--Select Provider--</option>
						</select>
					</div>

					<div class="form-group col-md-6" id="version_name" style="display: none;"><label>Version</label>
						<input type="text" class="form-control" id="version" placeholder="Version">
					</div>
					<div class="form-group col-md-6" id="db_upload_download">
						<label>Request Type</label>
						<select class="form-control select2" id="upload_download_status" required="required">
							<option value="">-Select--</option>
							<option value="1">DB Download</option>
							<% if(userType == 999){ %>
							<%--<option value="2">Data Upload</option> --%>
							<% } %>
							<option value="3">DB Upload</option>
						</select>
					</div>
					<%--anjum--%>

					<div class="form-group col-md-12 info_type" style="display: none">
						<label>Which info you want to set</label><br />
						<input type="checkbox" id="notice_info" name="info1_type" value="1">&nbsp;&nbsp;&nbsp;Notice&nbsp;&nbsp;&nbsp;
						<input type="checkbox" id="sql_info" name="info2_type" value="2">&nbsp;&nbsp;&nbsp;Sql&nbsp;&nbsp;&nbsp;
						<input type="checkbox" id="helpline_info" name="info3_type" value="3">&nbsp;&nbsp;&nbsp;Helpline
					</div>

					<div class="form-group col-md-4" id="notice_txt" style="display: none;"><label>Notice</label>
						<input type="text" style="height: 50px;" class="form-control" id="notice_txt_input" placeholder="Notice">
					</div>
					<div class="form-group col-md-4" id="sql_txt" style="display: none;"><label>Sql</label>
						<input type="text" class="form-control" id="sql_txt_input" placeholder="Sql">
					</div>
					<div class="form-group col-md-4" id="helpline_txt" style="display: none;"><label>Helpline</label>
						<input type="text" class="form-control" id="helpline_txt_input" placeholder="Helpline">
					</div>
					<%--anjum--%>
					<div class="form-group col-md-6 retired_date" id="retired_date" style="display: none">
						<label id="dateLabel">Retired Date</label>
						<br>
						<input type="text" id="end_date" class="form-control pull-right" readonly />
					</div>
					<div class="form-group col-md-6" id="leaveCause" style="display: none">
						<label>Cause of Leave</label>
						<textarea class="form-control" id="leaveComment" required="required" placeholder="Leave Comment"></textarea>
					</div>
					<div class="box-footer col-md-12">
						<input type="hidden" id="formType" value="">
						<input type="button" id="ProvEntryForm" class="btn btn-primary" value="Submit">
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>

	<div class="row" id="table_row" style="display:none;">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title" id="table_title"></h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding" id="table_append"></div>
			</div>
		</div>
	</div>

	<div id="model_box"></div>

	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="library/datepicker/datepicker.min.css">
	<!-- bootstrap datepicker -->
	<script src="library/datepicker/bootstrap-datepicker.min.js"></script>

	<script>
		var menuType = <%=menuType%>;
        $(".select2").select2();

        function show_dpu(type)
        {
            if(type==1)
            {
                $("#upazilaName").hide();
                $("#unionName").hide();
                $("#provider_code").hide();
                //$("#unionFacilityName").hide();
            }
            else if(type==2)
            {
                $("#upazilaName").show();
                $("#unionName").hide();
                $("#provider_code").hide();

            }
            else if(type==3)
            {
                $("#upazilaName").show();
                $("#unionName").show();
                $("#provider_code").hide();
            }
            else if(type==4)
            {
                $("#upazilaName").show();
                $("#unionName").hide();
                $("#provider_code").show();
                $("#aav_provider_type").hide();
            }
        }

        $('#providerType').on('change', function(){

            <!-- @Author: Evana ,@date: 09/07/2019, @Title: For Provider Code insert -->
            var type = $("#menuType").val();

            if(type == 1)
            {

                loadDivision("#RdivName");

                $('#RzillaName').find('option').remove().end();
                $('#RupaZilaName').find('option').remove().end();
                $('#RunionName').find('option').remove().end();

                $("#hints").html("<input type='number' class='form-control' id='ProvCode' value=''>");
            }


            if($('#formType').val()!="6"){
                if($('#providerType').val()=="999" || $('#providerType').val()=="998" ||$('#providerType').val()=="994"|| $('#providerType').val()=="997" || $('#providerType').val()=="0" || $('#providerType').val()=="20" || $('#providerType').val()=="21" || $('#providerType').val()=="23"){
                    $("#upazilaName").hide();
                    $("#unionName").hide();
                    $("#provider_facility_type").hide();
                    $("#FacName").hide();
                    //$("#unionFacilityName").hide();
                }
                else if($('#providerType').val()=="14" || $('#providerType').val()=="15" || $('#providerType').val()=="16" || $('#providerType').val()=="19" || $('#providerType').val()=="22"){
                    $("#upazilaName").show();
                    $("#unionName").hide();
                    $("#provider_facility_type").hide();
                    $("#FacName").hide();
                    //$("#unionFacilityName").hide();
                }
                else{
                    $("#upazilaName").show();
                    $("#unionName").show();
                    $("#provider_facility_type").show();
                    $("#FacName").show();
                    //$("#unionFacilityName").hide();
                }
            }
        });

        // $('#facilityType').on('change', function(){
        // 	if($('#formType').val()=="5"){
        // 		if($('#facilityType').val()=="5_UHC")
        // 			$("#unionName").hide();
        // 			/*
        // 		else if($('#facilityType').val()=="7_DH" || $('#facilityType').val()=="6_MCWC"){
        // 			$("#unionName").hide();
        // 			$("#upazilaName").hide();
        // 		}*/
        // 		 else if($('#facilityType').val()=="7_DH" ){
        // 			$("#unionName").hide();
        // 			//$("#upazilaName").hide();
        // 			//$("#unionName").show();
        // 			$("#upazilaName").show();
        // 		}
        //
        // 		else if($('#facilityType').val()=="6_MCWC"){
        // 			$("#unionName").show();
        // 			$("#upazilaName").show();
        // 		}
        // 		else{
        // 			$("#unionName").show();
        // 			$("#upazilaName").show();
        // 		}
        // 	}
        // });

        $('#month_type').on('change', function(){
            $(".month_wise").show();
            $(".date_wise").hide();
        });

        $('#date_type').on('change', function(){
            $(".date_wise").show();
            $(".month_wise").hide();
        });

        $('#monthSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            minViewMode: 1,
            format: 'yyyy-mm'
        })

        $('#start_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        $('#end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        /*$('#monthSelect').combodate({
            format: "YYYY-MM",
            template: "MMM YYYY",
            smartDays: true,
            //maxYear: 2025,
            value : moment()
        });

        $('#start_date').combodate({
            format: "YYYY-MM-DD",
            template: "DD MMM YYYY",
            smartDays: true,
            //maxYear: 2025,
            value : moment()
        });

        $('#end_date').combodate({
            format: "YYYY-MM-DD",
            template: "DD MMM YYYY",
            smartDays: true,
            //maxYear: 2025,
            value : moment()
        });*/

        $( document ).ready(function() {


            $("#unionFacilityName").show();

            $("#facilityType").empty();

            var output = "<option value ='' selected></option>";
            //for view facility menu option added 'All'
            if(menuType == 27 || menuType == 28)
                output = output + '<option value = " 0_All ">All</option>';

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + '<option value = "' + key + '_' + val + '">' + val + '</option>';
            });


            $("#facilityType").append(output);
        });

        $( document ).ready(function() {

            $("#unionFacilityName").hide();
            $("#providerFacility").empty();

            var output = "<option value ='' selected></option>";

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + "<option value = " + key + "_" + val + ">" + val + "</option>";
            });

            $("#providerFacility").append(output);
        });

        // @Evana, @Date: 28/07/2019
        function loadFacility()
        {
            $("#providerFacility").empty();
            var output = "";
            output = "<option value ='' selected></option>";

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + "<option value = " + key + "_" + val + ">" + val + "</option>";
            });

            $("#providerFacility").append(output);
            return false;
        }

        /**
         * show textbox if checkbox is checked (Anjum)
         * */
        function isChecked(checkboxid,textboxid){
            if(($("#"+checkboxid).is(':checked')))
                $("#"+textboxid).show();
            else
                $("#"+textboxid).hide();


        }

        $( document ).ready(function() {

            var formType = $("#menuType").val();

            if(formType == 5)
            {
                //alert("formType="+ formType);
                $("#unionFacilityName").show();
            }else if ( formType == 12){
                //new textbox added for update Application (Anjum)
                $("#box_title").html('Manage Provider');
                $(".info_type").show();
                $("#version_name").hide();
                $("input[type='checkbox']").change(function(){

                    isChecked('notice_info','notice_txt');
                    isChecked('sql_info','sql_txt');
                    isChecked('helpline_info','helpline_txt')
                })
            }
            else
            {
                //alert("Not shown="+ formType);
                $("#unionFacilityName").hide();
            }

        });

        $(".changeFacility").on('change', function(){
            if($('#FacName').is(':visible')){
                var forminfo = new Object();
                forminfo.zillaid = $('#RzillaName').val();
                forminfo.upazilaid = $('#RupaZilaName').val();
                forminfo.unionid = $('#RunionName').val();

                if($('#facility_type').is(':visible'))
                    forminfo.facilityType = $('#facilityType').val().split("_")[0];
                else if($('#provider_facility_type').is(':visible'))
                    forminfo.facilityType = $('#providerFacility').val().split("_")[0];
                forminfo.type = "6";

                $.ajax({
                    type: "POST",
                    url:"getFacilityInfo",
                    timeout:60000, //60 seconds timeout
                    data:{"forminfo":JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        facilityJS = JSON.parse(response);
                        $("#facilityName").empty();
                        if((<% out.println(userType); %>)=="999"){
                            var output = "<option value ='' selected></option>";
                            // output = output + "<option value ='মা-মনি কার্যালয়, গুলশান , ঢাকা'>মা-মনি কার্যালয়, গুলশান , ঢাকা</option>";
                        }
                        else
                            var output = "<option value ='' selected></option>";

                        $.each(facilityJS, function(key, val) {
                            output = output + "<option value = " + key + ">" + val + "</option>";
                        });

                        $("#facilityName").append(output);
                    },
                    complete: function() {
                        preventMultiCall = 0;
                    },
                    error: function(xhr, status, error) {
                               checkInternetConnection(xhr)
                    }
                });
            }
        });

        /**
         @Author: Evana
         @Date : 07/07/2019
         @Title: Provider code hints based on provider type and District selection
         ***/

        function getProviderHints()
        {
            //alert("hi");
            var forminfo = new Object();
            forminfo.zillaid = $('#RzillaName').val();
            forminfo.upazilaid = $('#RupaZilaName').val();
            forminfo.unionid = $('#RunionName').val();
            forminfo.providerType = $('#providerType').val();

            var provtype = $('#providerType').val();
            //alert(zillaid);

            if(provtype==4 || provtype==5 || provtype==17 || provtype==101)
            {

                $.ajax({
                    type: "POST",
                    url:"getProviderCode",
                    timeout:60000, //60 seconds timeout
                    data:{"forminfo":JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        // alert(response);
                        var output = "";
                        var parsedData = JSON.parse(response);

                        // $("#hint").html("<input type='text' class='form-control' readonly='readonly' value='Hint* "+ parsedData.code+ "'>");
                        $("#hints").html("<input type='number' class='form-control' id='ProvCode' value='"+ parsedData.code+"'>");


                    },
                    complete: function() {
                        preventMultiCall = 0;
                    },
                    error: function(xhr, status, error) {
                               checkInternetConnection(xhr)
                    }
                });
                return false;
            }
            else
            {
                return false;
            }
        }

        $('#retiredType').on('change', function () {
			if($('#retiredType').val()=="3"){
			    $('#leaveCause').show();
			    $('#leave_type').show();
			    $('#dateLabel').text("Leave Start Date");
			}
			else if($('#retiredType').val()=="4"){
                $('#leaveCause').hide();
                $('#leave_type').hide();
                $('#dateLabel').text("Leave End Date");
			}
			else {
                $('#leaveCause').hide();
                $('#leave_type').hide();
                $('#dateLabel').text("Retired Date");
			}
        });

        if(!(menuType == 27 || menuType == 28)) {
            $("#RupaZilaName").on('change', function () {
                var forminfo = new Object();
                forminfo.zillaid = $('#RzillaName').val();
                forminfo.upazilaid = $('#RupaZilaName').val();
                forminfo.providerType = "All";
                forminfo.providerListType = $('#retiredType').val();

                $.ajax({
                    type: "POST",
                    url: "getProviderInfo",
                    timeout: 60000, //60 seconds timeout
                    data: {"forminfo": JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        providerInfoJS = JSON.parse(response);
                        $("#providerID").empty();
                        var output = "";
                        $.each(providerInfoJS, function (key, val) {
                            if ($('#retiredType').val() == "2")
                                output = output + "<option value = " + key + ">" + val + "</option>";
                            else
                                output = output + "<option value = " + key + ">" + val + "(" + key + ")" + "</option>";
                        });

                        $("#providerID").append(output);
                    },
                    complete: function () {
                        preventMultiCall = 0;
                    },
                    error: function (xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });
            });
        }


	</script>

</div>



