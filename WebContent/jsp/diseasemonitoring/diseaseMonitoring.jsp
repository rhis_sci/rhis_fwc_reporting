<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/28/2021
  Time: 4:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>


        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">

                    <!--select type-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="view_level" style="display: none;">
                                <label>District/Upazila/Union Level</label>
                                <select class="form-control select2" id="reportViewLevel"
                                        onchange="show_dpu_level(this.value)" style="width: 100%;">
                                    <option value="">--Select Level--</option>
                                    <%--<option value="1">All</option>--%>
                                    <option value="2">District Level</option>
                                    <option value="3">Upazila Level</option>
                                    <option value="4">Union Level</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <!--/select type-->

                    <!--geo group-->
                    <%@include file="/jsp/util/geoSection.jsp" %>
                    <!--/geo group-->

                    <!--date section-->
                    <%@include file="/jsp/util/date.jsp" %>
                </div>

            </div>
            <!--row-->
        </div>
    </div>

    <!--for showing table(performance) result-->
    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>
    <!--/for showing table(performance) result-->

    <script>
        $(".select2").select2();

        function show_dpu_level(level) {
            if (level == 1) {
                $("#divisionName").hide();
                $("#districtName").hide();
                $("#upazilaName").hide();
                $("#unionName").hide();
            }
            else if (level == 2) {
                $("#districtName").show();
                $("#upazilaName").hide();
                $("#unionName").hide();

                //$('select').prop('selectedIndex', 0);
                $("#RupaZilaName")[0].selectedIndex = 0;
                $("#RunionName")[0].selectedIndex = 0;
            }
            else if (level == 3) {
                $("#divisionName").show();
                $("#districtName").show();
                $("#upazilaName").show();
                $("#unionName").hide();
                $("#RunionName")[0].selectedIndex = 0;
            }
            else if (level == 4) {
                $("#divisionName").show();
                $("#districtName").show();
                $("#upazilaName").show();
                $("#unionName").show();
            }
        }

    </script>
</div>

