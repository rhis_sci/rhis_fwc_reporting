<%@ page import="java.sql.ResultSet" %>
<%@ page import="static javax.swing.UIManager.getInt" %><%--
  Created by IntelliJ IDEA.
  User: Fazlur Rahman
  Date: 12/07/2022
  Time: 9:07 PM
  To change this template use File | Settings | File Templates.
--%>

<style>
    table, th, td {
        border: 1px solid black;
    }
    .heading {

        background-color: #75c1ed;

    }
    .upto150{
        background-color: #c99ce2;
    }
    .upto100{
        background-color: #de89c6;
    }
    .upto49{
       background-color: #f597b0;
    }



</style>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ResultSet rs = (ResultSet) request.getAttribute("STORAGEInfo");%>


<div class="remove_div">
    <div class="box box-default">
        <%--<div class="box-header with-border">--%>
        <h2 class="viewTitle" style="text-align: center;" ><span class="label label-default">Provider List with Low Storage (Free space<150)</span></h2>
        <%--<div class="box-tools pull-right">--%>
        <%--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--%>
        <%--</button>--%>
        <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>--%>
        <%--</button>--%>
        <%--</div>--%>
        <%--</div>--%>
            <div align="center" class="col-md-12">
                <ul class="legend">
                    <li><span class="upto150"></span>Storage between 150 and 100</li>
                    <li><span class="upto100"></span> Storage between 99 and 50 </li>
                    <li><span class="upto49"></span> Storage between 49 and 0 </li>
                </ul>
                <br><br>
            </div>


        <div class="box-body search_form table-responsive">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered nowrap reportExport" id="reportDataTable"
                           cellspacing="0" width="100%">
                        <thead class="heading">
                        <tr>

                            <th>ZILLA NAME</th>
                            <th> UPAZILA NAME</th>
                            <th> UNION NAME</th>
                            <th> PROVIDER ID</th>
                            <th> PROVIDER NAME</th>
                            <th> MOBILE NUMBER</th>
                            <th> LAST LOGIN TIME</th>
                            <th> STORAGE SPACE</th>

                        </tr>
                        </thead>
                        <tbody>
                        <% while (rs.next()) {
                        %>


                        <% if (rs.getInt("mb")>=100 && rs.getInt("mb")<=150){

                        %>

                        <tr class="" style="background-color: #c99ce2">

                            <td><%=rs.getString("zillanameeng")%>
                            </td>
                            <td><%=rs.getString("upazilanameeng")%>
                            </td>
                            <td><%=rs.getString("unionnameeng")%>
                            </td>
                            <td><%=rs.getString("providerid")%>
                            </td>
                            <td><%=rs.getString("provname")%>
                            </td>
                            <td><%=rs.getString("mobileno")%>
                            </td>
                            <td><%=rs.getString("login_time")%>
                            </td>
                            <td><%=rs.getString("storage_space")%>
                            </td>


                        </tr>
                        <%
                            }
                        %>

                        <%  if (rs.getInt("mb")>=50 && rs.getInt("mb")<=99){

                        %>

                        <tr style="background-color: #de89c6">

                            <td><%=rs.getString("zillanameeng")%>
                            </td>
                            <td><%=rs.getString("upazilanameeng")%>
                            </td>
                            <td><%=rs.getString("unionnameeng")%>
                            </td>
                            <td><%=rs.getString("providerid")%>
                            </td>
                            <td><%=rs.getString("provname")%>
                            </td>
                            <td><%=rs.getString("mobileno")%>
                            </td>
                            <td><%=rs.getString("login_time")%>
                            </td>
                            <td><%=rs.getString("storage_space")%>
                            </td>


                        </tr>

                        <%
                            }
                        %>

                        <%  if (rs.getInt("mb")>=0 && rs.getInt("mb")<=49){

                        %>

                        <tr style="background-color: #f597b0">

                            <td><%=rs.getString("zillanameeng")%>
                            </td>
                            <td><%=rs.getString("upazilanameeng")%>
                            </td>
                            <td><%=rs.getString("unionnameeng")%>
                            </td>
                            <td><%=rs.getString("providerid")%>
                            </td>
                            <td><%=rs.getString("provname")%>
                            </td>
                            <td><%=rs.getString("mobileno")%>
                            </td>
                            <td><%=rs.getString("login_time")%>
                            </td>
                            <td><%=rs.getString("storage_space")%>
                            </td>


                        </tr>

                        <%
                            }
                        %>

                        <%
                            }
                        %>
                        </tbody>

                    </table>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
</div>



<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>
<script type="text/javaScript">
    $('.clickable').click(function () {
        $('.clickable').css({"background-color": "rgb(255, 255, 255)"});
        $(this).css({"background-color": "rgb(0, 172, 214)"});
        $('.clickable > span').removeClass("bg-aqua").addClass("bg-gray");
        $(this).find("span:first").removeClass("bg-gray").addClass("bg-aqua");
        $('.clickable span img').each(function(index, element) {
            var url = $(element).prop("src").replace("png","jpg");
            $(element).attr("src",url);
        });
        url = $(this).find("span img").prop("src").replace("jpg","png");
        $(this).find("span img").attr("src",url);
        openTab($(this).attr("data"));
    });
    function openTab(tabName) {
        var tabcontent;
        tabcontent = document.getElementsByClassName("tab");
        for (var i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        document.getElementById(tabName).style.display = "block";
    }
    function doExport(selector, params) {
        var options = {
            //ignoreRow: [1,11,12,-2],
            //ignoreColumn: [0,-1],
            //pdfmake: {enabled: true},
            tableName: 'Facility Information',
            worksheetName: 'Facility Information'
        };

        $.extend(true, options, params);

        $(selector).tableExport(options);
    }

    function DoOnCellHtmlData(cell, row, col, data) {
        var result = "";
        if (data != "") {
            var html = $.parseHTML( data );

            $.each( html, function() {
                if ( typeof $(this).html() === 'undefined' )
                    result += $(this).text();
                else if ( $(this).is("input") )
                    result += $('#'+$(this).attr('id')).val();
                else if ( $(this).is("select") )
                    result += $('#'+$(this).attr('id')+" option:selected").text();
                else if ( $(this).hasClass('no_export') !== true )
                    result += $(this).html();
            });
        }
        return result;
    }

    function DoOnMsoNumberFormat(cell, row, col) {
        var result = "";
        if (row > 0 && col == 0)
            result = "\\@";
        return result;
    }

</script>



<script>
    $(document).ready(function() {
        //$('#reportDataTable').DataTable();


        $(document).ready(function() {
            var filterFunc = function (sData) {
                return sData.replace(/\n/g," ").replace( /<.*?>/g, "" );
            };
            var table = $('#reportDataTable').dataTable({
                "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
                "processing": true,
                "paging": true,
                "lengthMenu": [ 10, 25, 50, 75, 100 ],
                "language": {
                    "info": "Total _TOTAL_",
                    "infoEmpty": "Total _TOTAL_",
                    "zeroRecords": "No records",
                    "lengthMenu": "Show _MENU_ entries",
                    "searchIcon":"",
                    "paginate": {
                        "first": "First",
                        "last": "Last",
                        "next": "Next",
                        "previous": "Prev"
                    }
                },
                "initComplete": function(oSettings, json) {
                    $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                    $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                    $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");

                },
                "buttons": ['copy',{
                    extend: 'excelHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'csvHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    title:"Service Statistics",
                    pageSize: 'LEGAL'
                },{
                    extend: 'print',
                    text: 'Print',
                    autoPrint: true
                }
                ]
            });
        });
        var table = $('#reportDataTableFilter').dataTable({
            "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
            "processing": true,
            "paging": true,
            "lengthMenu": [ 10, 25, 50, 75, 100 ],
            "language": {
                "info": "Total _TOTAL_",
                "infoEmpty": "Total _TOTAL_",
                "zeroRecords": "No records",
                "lengthMenu": "Show _MENU_ entries",
                "searchIcon":"",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Prev"
                }
            },
            "initComplete": function(oSettings, json) {
                $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );

            },
            "buttons": ['copy',{
                extend: 'excelHtml5',
                title:"Facility Information"
            },{
                extend: 'csvHtml5',
                title:"Facility Information"
            },{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title:"Facility Information"
            }, {
                extend: 'print',
                text: 'Print',
                autoPrint: true
            }
            ]
        } );
    });




</script>


