<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 1/13/2021
  Time: 11:09 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>


<html>
<% ResultSet rs = (ResultSet) request.getAttribute("resultstring"); %>
<style>
    /*.row [class*="col-"]{*/
    /*margin-bottom: -99999px;*/
    /*padding-bottom: 99999px;*/
    /*}*/

    /*.row{*/
    /*overflow: hidden;*/
    /*}*/

</style>
<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title" id="box_title">Server Status</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i>
                </button>
            </div>
        </div>
        <div class="box-body search_form">
            <div class="row">


                <% int i = 1;
                    while (rs.next()) { %>
                <%--for(int i = 0; i<2; i++){%>--%>
                <div class="col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3><%= rs.getString("server_name") %>
                            </h3>
                        </div>

                        <div class="panel-body">
                            <pre style="background-color: #fff; border: none !important;"><%= rs.getString("server_status") %></pre>
                        </div>
                    </div>
                </div>
                <%
                    if (i++ % 2 == 0) {
                %>
            </div>
            <div class="row">
                <%
                    }
                %>

                <%}%>
                <% try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                } %>
            </div>
        </div>
    </div>
</div>

</html>
