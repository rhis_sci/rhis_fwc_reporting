<%@ page import="java.sql.ResultSet" %>
<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 1/11/2022
  Time: 4:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ResultSet rs = (ResultSet) request.getAttribute("meetingInfo");%>
<% Integer division = (Integer) request.getAttribute("division");%>
<% Integer zilla = (Integer) request.getAttribute("zilla");%>
<% Integer upazila = (Integer) request.getAttribute("upazila");%>
<% Integer union = (Integer) request.getAttribute("union");%>
<% String providerid = (String) request.getAttribute("providercode");%>
<% String designation = (String) request.getAttribute("designation");%>
<% Integer category = (Integer) request.getAttribute("category");%>
<% Integer year = (Integer) request.getAttribute("year");%>
<% Integer month = (Integer) request.getAttribute("month");%>
<% String url = "http://emis1.dgfp.gov.bd:8090/emis-monitoringtools/login?"; %>
<% String workingPaperUrl = "username=fwv_provider&division=" + division + "&district=" + zilla + "&upazila=" + upazila + "&union=" + union + "&providerCode=" + providerid + "&designation=" + designation +
        "&category=" + category + "&meetingYear=" + year + "&meetingMonth=" + month;%>

<body>
<script src="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/js/adminlte.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="library/datepicker/datepicker.min.css">
<!-- bootstrap datepicker -->
<script src="library/datepicker/bootstrap-datepicker.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/css/adminlte.min.css">

<div class="form-group date col-md-4 month_wise" style="padding-left: 20%;padding-top: 20px;">
    <label>&#x9ae;&#x9be;&#x9b8;:&nbsp;<span class="pull-right"><input type="button" id="monthSelect"
                                                                       class="form-control" readonly></span></label>
</div>


<div id="result_container" class="append_res">

    <div style="left: auto;padding-left: 30%;">

        <% while (rs.next()) {
            String meeting_no = rs.getString("meeting_no");
        %>

        <div class="row" style="padding-left:40px;">
            <div class="col-md-6">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;&#x9a4;&#x9be;&#x9b0;&#x9bf;&#x996;
                            :<%=rs.getString("meeting_date")%>&nbsp;<%=rs.getString("meeting_time")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;( <%=rs.getString("meeting_type")%> )</h5>
                    </div>
                    <div class="card-body">
                        <h6 class="card-title"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;&#x9b8;&#x9cd;&#x9a5;&#x9be;&#x9a8;
                            :<%=rs.getString("itemdes")%>
                        </h6><br><br>

                        <% if (rs.getString("is_meeting_minutes_circulate").equals("1") && rs.getString("status").equals("1")) {%>
                        <a class="btn btn-success"
                           href=<%=url+workingPaperUrl+"&targetReportUrl=meeting-notice&meetingNo="+meeting_no%>>&#x9ac;&#x9bf;&#x99c;&#x9cd;&#x99e;&#x9aa;&#x9cd;&#x9a4;&#x9bf;</a>
                        <a class="btn btn-success"
                           href=<%=url+workingPaperUrl+"&targetReportUrl=meeting-minutes&meetingNo="+meeting_no%>>&#x995;&#x9be;&#x9b000;&#x9cd;&#x9af;&#x9aa;&#x9a4;&#x9cd;&#x9b0;</a>
                        <%} else { %>
                        <a class="btn btn-primary"
                           href=<%=url+workingPaperUrl+"&targetReportUrl=meeting-notice&meetingNo="+meeting_no%>>&#x9ac;&#x9bf;&#x99c;&#x9cd;&#x99e;&#x9aa;&#x9cd;&#x9a4;&#x9bf;</a>
                        <a class="btn btn-primary"
                           href=<%=url+workingPaperUrl+"&targetReportUrl=working-paper&meetingNo="+meeting_no%>>&#x995;&#x9be;&#x9b0;&#x9cd;&#x9af;&#x9aa;&#x9a4;&#x9cd;&#x9b0;</a>
                        <%}%>
                    </div>

                </div>
            </div>
        </div>
        <%}%>


    </div>



    <script>

        window.meeting_minutes = {
            initBinds : function(){
                $(".month_wise").change(function(){
                    window.meeting_minutes.loadPage();
                });

                // set date picker bind
                $('#monthSelect').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    minViewMode: 1,
                    format: 'yyyy-mm'
                }).datepicker('setDate', new Date().getFullYear() + '-' + (new Date().getMonth() + 1).toString());
            },

            loadPage : function(){
                var date = $('#monthSelect').val().split('-')[0];
                var year = date[0];
                var month = date[1];
                <%out.println(month);%>;
                var division =<% out.println(division);%>;
                var zilla =<% out.println(zilla); %>;
                var upz =<% out.println(upazila); %>;
                var union = <% out.println(union); %>;
                var provcode = <% out.println(providerid); %>;
                var catg = <%out.println(category);%>;
                <%--var desg = <%out.println(designation);%>;--%>


                $.ajax({
                    type: "GET",
                    url: "http://localhost:8080/meetingminutes?division=" + division + "&district=" + zilla + "&upazila=" + upz + "&union=" + union + "&designation=FWV&category=" + catg + "&providerCode=" + provcode+"&year="+$('#monthSelect').val().split('-')[0]+"&month="+$('#monthSelect').val().split('-')[1],

                    timeout: 100000,
                    success: function (response) {
                        $("#result_container").html(response);
                    },
                    complete: function () {
                        preventMultiCall = 0;
                    },
                    error: function (xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });
            }
        }

        // single call in the lifecycle of the page
        if (window.meeting_minutes_init === undefined) {
            window.meeting_minutes.initBinds();
            window.meeting_minutes_init = 1;
        }
    </script>
</div>
</body>
