<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.sci.rhis.util.EnglishtoBangla" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8" %>
<script src="js/lib/ddtf.js"></script>
<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript">

    function PrintElem(elem) {
//        $('button').hide();
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div');
        mywindow.document.write('<html><head><title></title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();

        return true;
    }
    $(document).on("click", '.button_next' ,function (){
        $('#mnc-table-left').hide();
        $('#mnc-table-right').show();
        var button_val = $(".button_next").val();
        if(button_val == 1){
            $(".button_next").val(2);
            $(".button_pre").val(1);
            $(".button_next").attr("disabled","disabled");
            $(".button_pre").removeAttr("disabled");
            $(".showing_page").text("2 of 2  ");
        }
    });

    $(document).on("click", '.button_pre' ,function (){
        $('#mnc-table-left').show();
        $('#mnc-table-right').hide();
        var button_val = $(".button_pre").val();
        if(button_val>0)
        {
            $(".button_pre").val(0);
            $(".button_next").val(1);
            $(".button_next").removeAttr("disabled");
            $(".button_pre").attr("disabled","disabled");
            $(".showing_page").text("1 of 2  ");
        }
    });
</script>

<style>
	.mnc-table-left th,.mnc-table-right th{
		border: 1px solid black!important;
		text-align: center!important;
		vertical-align: middle!important;
		padding: 2px;
	}
	.mnc-table-left td,.mnc-table-right td{
		vertical-align: top!important;
		padding-left: 2px;
	}
	.nowrap{
		white-space: nowrap!important;
	}
	.inner_table td{
		border: 1px solid black!important;
		text-align: left!important;
		vertical-align: middle!important;
		padding-left: 2px;
	}
	.pad-bot-15 div{
		padding-bottom: 10px;
	}
	.pad-bot-5 div{
		padding-bottom: 5px;
	}
	.bold-italic{
		font-weight: bold;
		font-style: italic;
		padding-left: 4px;
		font-size: 110%;
	}
</style>
<%

%>
<div class="tile-body nopadding" id="reportTable">
	<div class="row">
		<div class="col-md-12">
			<div class="dropdown" style="float: right; margin-right: 2%;">
				<button class="btn btn-primary" type="button" id="btnPrint" onclick="PrintElem('#printDiv')"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
					<span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="printDiv" style="margin-top: 2%;">
				<style>
					/*#mnc-table-left th,#mnc-table-right th{*/
					/*border: 1px solid black!important;*/
					/*text-align: center!important;*/
					/*vertical-align: middle!important;*/
					/*padding: 2px;*/
					/*}*/
					.mnc-table-left td,.mnc-table-right td{
						vertical-align: top!important;
					}
					.mnc-table-left{
						min-height: 8.5in;
					}
					.pad-bot-15 div{
						padding-bottom: 10px;
					}
					table { /* Or specify a table class */
						max-height: 100%!important;
						overflow: hidden!important;
						page-break-after: always!important;
					}
					/*.pad-bot-5 div{*/
					/*padding-bottom: 5px;*/
					/*}*/
					/*.bold-italic{*/
					/*font-weight: bold;*/
					/*font-style: italic;*/
					/*padding-left: 4px;*/
					/*font-size: 110%;*/
					/*}*/
				</style>
				<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'>
					<tr>
						<%--<td width="20%">--%>
						<%--<div style='border: 1px solid black;text-align: center;'>দুটি সন্তানের বেশি নয়<br>একটি হলে ভাল হয়।</div>--%>
						<%--</td>--%>
						<%--<td width="50%" style='text-align: center;'>--%>
						<%--<img width='60' src='image/dgfp_logo.png'></br>--%>
						<%--<h1>মা ও নবজাতক সেবা রেজিস্টার</h1>--%>
						<%--গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>পরিবার পরিকল্পনা অধিদপ্তর--%>
						<%--</td>--%>
						<%--<td width="30%" style='text-align: right;' class='page_no'></td>--%>
					</tr>
				</table>
				</br>
				<%
					String data = request.getAttribute("Result").toString();
					JSONArray json = new JSONArray(data);
					for (int i = 0; i < json.length()-i%3; i=i+3) {
						JSONObject singleRow = new JSONObject();
						JSONObject singleRow2 = new JSONObject();
						JSONObject singleRow3 = new JSONObject();
						try {
							singleRow = json.getJSONObject(i);
							if (i + 1 >= json.length()) {
								Iterator keys = singleRow.keys();
								while (keys.hasNext()) {
									String key = (String) keys.next();
									singleRow2.put(key, "");
								}
							} else {
								singleRow2 = json.getJSONObject(i + 1);
							}
							if (i + 2 >= json.length()) {
								Iterator keys = singleRow.keys();
								while (keys.hasNext()) {
									String key = (String) keys.next();
									singleRow3.put(key, "");
								}
							} else {
								singleRow3 = json.getJSONObject(i + 2);
							}
							singleRow.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_son").toString()));
							singleRow2.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_son").toString()));
							singleRow3.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_son").toString()));

							singleRow.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("serial").toString()));
							singleRow2.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("serial").toString()));
							singleRow3.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("serial").toString()));

							singleRow.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_bp").toString()));
							singleRow2.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_bp").toString()));
							singleRow3.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_bp").toString()));

							singleRow.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_age").toString()));
							singleRow2.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_age").toString()));
							singleRow3.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_age").toString()));

							singleRow.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_dau").toString()));
							singleRow2.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_dau").toString()));
							singleRow3.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_dau").toString()));


						} catch (Exception e) {
							e.printStackTrace();
						}

				%>
				<h1 align="middle">গর্ভনিরোধক খাবার বড়ি, কনডম ও ইসিপি রেজিস্টার</h1>
				<table   class="mnc-table-left"  border="1" cellspacing="0" style="font-size:11px; min-height: 8.5in;table-layout:fixed;">
					<tbody >
						<tr>
							<th rowspan="2" >রেজিঃ নম্বর</br> (বাৎসরিক </br>ক্রমিক/সন) </br>ও</br> তারিখ</th>
							<th rowspan="2" class="nowrap"><b>সেবা গ্রহীতার সাধারণ পরিচিতি</b></th>
							<th rowspan="2"  ><b>জীবিত </br> সন্তান </br>সংখ্যা</b></th>
							<th rowspan="2" ><b>বাছাইকরণ </br>(উপযুক্ততা </br>যাচাই)</br>(হ্যাঁ/না)</b></th>
							<th rowspan="2" ><b>শারীরিক </br>পরীক্ষা </br> (হ্যাঁ/না)</b></th>
							<th  colspan="8" ><b>গর্ভনিরোধক খাবার বড়ি/কনডম বিতরনের সংকেতসহ পরিমান ও তারিখ (চক্র/পিস)</b></th>
							<th rowspan="2"  ><b>ইসিপি </br>বিতরনের </br>পরিমান ও</br> তারিখ</b></th>
							<th rowspan="2" style="width: 80px;" ><b>মন্তব্য</b></th>
						</tr>
						<tr>

							<th  nowrap style="width: 80px;">১ম বার</th>
							<th  nowrap style="width: 80px;">২য় বার</th>
							<th  nowrap style="width: 80px;">৩য় বার</th>
							<th  nowrap style="width: 80px;">৪র্থ বার</th>
							<th  nowrap style="width: 80px;">৫ম বার</th>
							<th  nowrap style="width: 80px;">৬ষ্ঠ বার</th>
							<th  nowrap style="width: 80px;">৭ম বার</th>
							<th  nowrap style="width: 80px;">৮ম বার</th>

						</tr>

						<tr>
							<th class="nowrap">১</th>
							<th class="nowrap">২</th>
							<th class="nowrap">৩</th>
							<th class="nowrap">৪</th>
							<th class="nowrap">৫</th>
							<th >৬</th>
							<th >৭</th>
							<th class="nowrap">৮</th>
							<th class="nowrap">৯</th>
							<th class="nowrap">১০</th>
							<th class="nowrap">১১</th>
							<th class="nowrap">১২</th>
							<th >১৩</th>
							<th >১৪</th>
							<th >১৫</th>
						</tr>
						<%--Frist Client--%>
						<tr>
							<td  class="nowrap">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_age"));%> বছর</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামী/পিতার নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_husband"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম/মহল্লা: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_village"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বাড়ি/জিআর/হোল্ডিং নম্বর: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_house_holding"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										দম্পতি নম্বর : &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										মোবাইল নম্বর : &nbsp;<span class="bold-italic">0<% out.println(singleRow.get("client_mobile"));%></span>
									</div>
								</div>

							</td>
							<td>
								</br>
								<div class="col-md-12">
									ছেলে: &nbsp; <span class="bold-italic"> <% out.println(singleRow.get("client_son"));%></span>
								</div>
								</br>
								</br>
								</br>
								<div class="col-md-12">
									মেয়ে:<span class="bold-italic"> <% out.println(singleRow.get("client_dau"));%></span>
								</div>

							</td>
							<td></td>
							<td>
								<div class="row">
									</br>
									<div class="col-md-12">
										রক্তচাপ: &nbsp; <span class="bold-italic"> <% out.println(singleRow.get("client_bp"));%></span>
									</div>
									</br>
									</br>
									<div class="col-md-12">
										জন্ডিস:<span class="bold-italic"> <% out.println(singleRow.get("client_jaundice"));%></span>
									</div>
									</br>
									</br>
									<div class="col-md-12">
										ডায়াবেটিস:<span class="bold-italic"> <% out.println(singleRow.get("client_diabetic"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_1st"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_2nd"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_3rd"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_4th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_5th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_6th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_7th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow.get("pil_8th"));%></span>
								</div>
							</td>
							<td></td>
							<td></td>
						</tr>
						<%--2nd Client--%>

						<tr>
							<td  class="nowrap">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow2.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_age"));%> বছর</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামী/পিতার নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_husband"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম/মহল্লা: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_village"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বাড়ি/জিআর/হোল্ডিং নম্বর: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_house_holding"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										দম্পতি নম্বর : &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										মোবাইল নম্বর : &nbsp;<span class="bold-italic">0<% out.println(singleRow2.get("client_mobile"));%></span>
									</div>
								</div>


							</td>
							<td>
								<div class="row">
									</br>
									<div class="col-md-12">
										ছেলে: &nbsp; <span class="bold-italic"> <% out.println(singleRow2.get("client_son"));%></span>
									</div>
									</br>
									</br>
									</br>
									<div class="col-md-12">
										মেয়ে:<span class="bold-italic"> <% out.println(singleRow2.get("client_dau"));%></span>
									</div>
								</div>

							</td>
							<td></td>
							<td>
								<div class="row">
									</br>
									<div class="col-md-12">
										রক্তচাপ: &nbsp; <span class="bold-italic"> <% out.println(singleRow2.get("client_bp"));%></span>
									</div>
									</br>
									</br>
									<div class="col-md-12">
										জন্ডিস:<span class="bold-italic"> <% out.println(singleRow2.get("client_jaundice"));%></span>
									</div>
									</br>
									</br>
									<div class="col-md-12">
										ডায়াবেটিস:<span class="bold-italic"> <% out.println(singleRow2.get("client_diabetic"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_1st"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_2nd"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_3rd"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_4th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_5th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_6th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_7th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow2.get("pil_8th"));%></span>
								</div>
							</td>
							<td></td>
							<td></td>
						</tr>

						<%--3rd Client--%>

						<tr>
							<td  class="nowrap">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow3.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										নাম : &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বয়স : &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_age"));%> বছর</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামী/পিতার নাম: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_husband"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										গ্রাম/মহল্লা: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_village"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										বাড়ি/জিআর/হোল্ডিং নম্বর: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_house_holding"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										দম্পতি নম্বর : &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										মোবাইল নম্বর : &nbsp;<span class="bold-italic">0<% out.println(singleRow3.get("client_mobile"));%></span>
									</div>
								</div>


							</td>
							<td>
								<div class="row">
									</br>
									<div class="col-md-12">
										ছেলে: &nbsp; <span class="bold-italic"> <% out.println(singleRow3.get("client_son"));%></span>
									</div>
									</br>
									</br>
									</br>
									<div class="col-md-12">
										মেয়ে:<span class="bold-italic"> <% out.println(singleRow3.get("client_dau"));%></span>
									</div>
								</div>
							</td>
							<td></td>
							<td>
								<div class="row">
									</br>
									<div class="col-md-12">
										রক্তচাপ: &nbsp; <span class="bold-italic"> <% out.println(singleRow3.get("client_bp"));%></span>
									</div>
									</br>
									</br>
									<div class="col-md-12">
										জন্ডিস:<span class="bold-italic"> <% out.println(singleRow3.get("client_jaundice"));%></span>
									</div>
									</br>
									</br>
									<div class="col-md-12">
										ডায়াবেটিস:<span class="bold-italic"> <% out.println(singleRow3.get("client_diabetic"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_1st"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_2nd"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_3rd"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_4th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_5th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_6th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_7th"));%></span>
								</div>
							</td>
							<td>
								<div class="col-md-12">
									</br><span class="bold-italic"><% out.println(singleRow3.get("pil_8th"));%></span>
								</div>
							</td>
							<td></td>
							<td></td>
						</tr>

					</tbody>
				</table>
				<%}%>

			</div>
		</div>
	</div>
</div>


