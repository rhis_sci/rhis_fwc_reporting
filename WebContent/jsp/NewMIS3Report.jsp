<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.json.JSONObject" %>
<%@ page pageEncoding="UTF-8" %>

<script type="text/javascript">

 function printDataMIS3()
	{
        var hide_1 = 0;
        var hide_2 = 0;
        var hide_3 = 0;
        var hide_4 = 0;
		var hide_5 = 0;
		var hide_6 = 0;
		var hide_7 = 0;
		var hide_8 = 0;
		var hide_9 = 0;
		var hide_10 = 0;
		
		
		
        if($("#table_1").css('display') == 'none')
            hide_1 = 1;
	 	if($("#table_2").css('display') == 'none')
            hide_2 = 1;	
		if($("#table_3").css('display') == 'none')
            hide_3 = 1;	
		if($("#table_4").css('display') == 'none')
            hide_4 = 1;		
		if($("#table_5").css('display') == 'none')
            hide_5 = 1;	
		if($("#table_6").css('display') == 'none')
            hide_6 = 1;		
		if($("#table_7").css('display') == 'none')
            hide_7 = 1;		
		if($("#table_8").css('display') == 'none')
            hide_8 = 1;		
		if($("#table_9").css('display') == 'none')
            hide_9 = 1;		
		if($("#table_10").css('display') == 'none')
            hide_10 = 1;										
			
      /*  if($("#table_2").css('display') == 'none')
            hide_2 = 1;
        if($("#table_3").css('display') == 'none')
            hide_3 = 1;
        if($("#table_4").css('display') == 'none')
            hide_4 = 1;*/
        if($(".page_no").css('display') == 'none')
            page_no = 1;

        $("#table_1").show();
		$("#table_2").show();
		$("#table_3").show();
		$("#table_4").show();
		$("#table_5").show();
		$("#table_6").show();
		$("#table_7").show();
		$("#table_8").show();
		$("#table_9").show();
		$("#table_10").show();
		$("#table_11").show();
		$("#table_12").show();
		$("#table_13").show();
		$("#table_14").show();
		$("#table_15").show();
		//$("#table_16").show();
		
       /* $("#table_2").show();
        $("#table_3").show();
        $("#table_4").show();*/
        $(".page_no").show();

        //var divToPrint=document.getElementById("printDiv");
        var header_1=document.getElementById("table_header_1");
        var header_2=document.getElementById("table_header_2");
        var table_1=document.getElementById("table_1");
		var table_2=document.getElementById("table_2");
		var table_3=document.getElementById("table_3");
		var table_4=document.getElementById("table_4");
		var table_5=document.getElementById("table_5");
		var table_6=document.getElementById("table_6");
		var table_7=document.getElementById("table_7");
		var table_8=document.getElementById("table_8");
		var table_9=document.getElementById("table_9");
		var table_10=document.getElementById("table_10");
		var table_11=document.getElementById("table_11");
		var table_12=document.getElementById("table_12");
		var table_13=document.getElementById("table_13");
		var table_14=document.getElementById("table_14");
		var table_15=document.getElementById("table_15");
		//var table_16=document.getElementById("table_16");
		
       /* var table_2=document.getElementById("table_2");
        var table_3=document.getElementById("table_3");
        var table_4=document.getElementById("table_4");*/
        newWin= window.open("");
        //newWin.document.write(divToPrint.outerHTML);
        newWin.document.write(header_1.outerHTML);
        newWin.document.write(header_2.outerHTML);
        newWin.document.write(table_1.outerHTML);
		newWin.document.write(table_2.outerHTML);
		newWin.document.write(table_3.outerHTML);
		newWin.document.write(table_4.outerHTML);
		newWin.document.write(table_5.outerHTML);
		newWin.document.write(table_6.outerHTML);
		newWin.document.write(table_7.outerHTML);
		newWin.document.write(table_8.outerHTML);
		newWin.document.write(table_9.outerHTML);
		newWin.document.write(table_10.outerHTML);
		newWin.document.write(table_11.outerHTML);
		newWin.document.write(table_12.outerHTML);
		newWin.document.write(table_13.outerHTML);
		newWin.document.write(table_14.outerHTML);
		newWin.document.write(table_15.outerHTML);
		//newWin.document.write(table_16.outerHTML);
		
       /* newWin.document.write(table_2.outerHTML);
        newWin.document.write(table_3.outerHTML);
        newWin.document.write(table_4.outerHTML);*/
        newWin.print();
        newWin.close();

        if(hide_1==1)
            $("#table_1").hide();
		if(hide_2==1)
            $("#table_2").hide();	
		if(hide_3==1)
            $("#table_3").hide();	
		if(hide_4==1)
            $("#table_4").hide();				
		if(hide_5==1)
            $("#table_5").hide();	
		if(hide_6==1)
            $("#table_6").hide();	
		if(hide_7==1)
            $("#table_7").hide();	
		if(hide_8==1)
            $("#table_8").hide();				
		if(hide_9==1)
            $("#table_9").hide();	
		if(hide_10==1)
            $("#table_10").hide();	
		if(hide_11==1)
            $("#table_11").hide();
		if(hide_12==1)
            $("#table_12").hide();	
		if(hide_13==1)
            $("#table_13").hide();	
		if(hide_14==1)
            $("#table_14").hide();	
		if(hide_15==1)
            $("#table_15").hide();	
			
		/*if(hide_16==1)
            $("#table_16").hide();	*/											
      
        if(page_no==1)
            $(".page_no").hide();
    }

    function printData()
    {
        //var tableToPrint=document.getElementById("reportDataTable");
        var tableToPrint=document.getElementsByClassName("reportExport")[0];
        newWin= window.open("");
        newWin.document.write(tableToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    $('#btnPrint').on('click',function(){
        if($('#reportType').val()==4)
            printDataMIS3();
        else
            printData();
    });

</script>

<style>
.printer {
  page-break-after: always;
}
</style>


<div id="result">
    <%
        //out.println(request.getAttribute("Result"));
        //out.println(request.getAttribute("Result.amtsl"));

        String jsondata = request.getAttribute("Result").toString();
        JSONObject jsonObj = new JSONObject(jsondata);


        Integer previous_stock_viale = (jsonObj.has("previous_stock_viale")? jsonObj.getInt("previous_stock_viale"):0);
        //out.println("previous_stock_viale="+previous_stock_viale);

        Integer stock_in_viale = (jsonObj.has("stock_in_viale")? jsonObj.getInt("stock_in_viale"):0);
        //out.println("stock_in_viale="+stock_in_viale);

        Integer adjustment_stock_plus_viale = (jsonObj.has("adjustment_stock_plus_viale")? jsonObj.getInt("adjustment_stock_plus_viale"):0);
        //out.println("adjustment_stock_plus_viale="+ adjustment_stock_plus_viale);

        Integer adjustment_stock_minus_viale = (jsonObj.has("adjustment_stock_minus_viale")? jsonObj.getInt("adjustment_stock_minus_viale"):0);
        //out.println("adjustment_stock_minus_viale="+adjustment_stock_minus_viale);

        Integer new_inject_center = (jsonObj.has("new_inject_center")? jsonObj.getInt("new_inject_center"):0);
//        out.println("new_inject_center="+new_inject_center);

        Integer old_inject_center = (jsonObj.has("old_inject_center")? jsonObj.getInt("old_inject_center"):0);
//        out.println("old_inject_center="+old_inject_center);

        Integer new_inject_satelite = (jsonObj.has("new_inject_satelite")? jsonObj.getInt("new_inject_satelite"):0);
//        out.println("new_inject_satelite="+ new_inject_satelite);

        Integer old_inject_satelite = (jsonObj.has("old_inject_satelite")? jsonObj.getInt("old_inject_satelite"):0);
//        out.println("old_inject_satelite"+old_inject_satelite);

        Integer new_inject_ppfp_center = (jsonObj.has("new_inject_ppfp_center")? jsonObj.getInt("new_inject_ppfp_center"):0);
//        out.println("new_inject_center="+new_inject_center);

        Integer old_inject_ppfp_center = (jsonObj.has("old_inject_ppfp_center")? jsonObj.getInt("old_inject_ppfp_center"):0);
//        out.println("old_inject_center="+old_inject_center);

        Integer new_inject_ppfp_satelite = (jsonObj.has("new_inject_ppfp_satelite")? jsonObj.getInt("new_inject_ppfp_satelite"):0);
//        out.println("new_inject_satelite="+ new_inject_satelite);

        Integer old_inject_ppfp_satelite = (jsonObj.has("old_inject_ppfp_satelite")? jsonObj.getInt("old_inject_ppfp_satelite"):0);
//        out.println("old_inject_satelite"+old_inject_satelite);

        Integer current_month_vial =(new_inject_center+ old_inject_center + new_inject_satelite + old_inject_satelite + new_inject_ppfp_center+ old_inject_ppfp_center + new_inject_ppfp_satelite + old_inject_ppfp_satelite);
        //out.println("Current_month_vial=" + current_month_vial);
		
		

        Integer adjustment_vial = previous_stock_viale + stock_in_viale + adjustment_stock_plus_viale - adjustment_stock_minus_viale - (current_month_vial);
        //out.println("rest_month_vial=" + adjustment_vial);

        // Injectable calculation
        Integer previous_stock_syringe = (jsonObj.has("previous_stock_syringe")? jsonObj.getInt("previous_stock_syringe"):0);
        //out.println("Previous Stock Syringe=" + previous_stock_syringe);

        Integer Stock_in_syringe = (jsonObj.has("stock_in_syringe")? jsonObj.getInt("stock_in_syringe"):0);
        //out.println("Stock in Syringe=" + Stock_in_syringe);

        Integer adjustment_stock_plus_syringe = (jsonObj.has("adjustment_stock_plus_syringe")? jsonObj.getInt("adjustment_stock_plus_syringe"):0);
        //out.println("Adjustment stock in Syringe=" + adjustment_stock_plus_syringe);

        Integer adjustment_stock_minus_syringe = (jsonObj.has("adjustment_stock_minus_syringe")? jsonObj.getInt("adjustment_stock_minus_syringe"):0);
        //out.println("Adjustment stock Minus Syringe=" + adjustment_stock_minus_syringe);
		
		//Integer adjusted_syringe = previous_stock_syringe + Stock_in_syringe + adjustment_stock_plus_syringe - adjustment_stock_minus_syringe;
        Integer adjusted_syringe = previous_stock_syringe + Stock_in_syringe + adjustment_stock_plus_syringe - adjustment_stock_minus_syringe - (current_month_vial);
        //out.println("Adjusted Syringe=" + adjusted_syringe);
		
		
		Integer delivery_facility_center = ((jsonObj.has("delivery_facility"))?jsonObj.getInt("delivery_facility"):0);
		//out.println("delivery_facility_center=" + delivery_facility_center);
    %>
</div>
<div class="tile-body nopadding" id="reportTable">
 <div class="row">
        <div class="col-md-12">
            <div class="dropdown" style="float: right; margin-right: 2%;">
                <button class="btn btn-primary" type="button" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
                <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
                    <%--<li><a class="btn btn-default buttons-pdf buttons-html5" tabindex="0" aria-controls="reportTable" href="#"><span>PDF</span></a></li>--%>
                </ul>
            </div>
        </div>
    </div>
</div>	

<div style='float: right;'>
    <button type="button" class="btn btn-default pre_button_new" value="1" disabled><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
    <span class="showing_page">&nbsp; &nbsp;1 of 5 &nbsp; &nbsp;</span><button type="button" class="btn btn-default next_button_new" value='2'>
    <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
</div>


<div class="container printer" id="DivIdToPrint">
    <%= ((jsonObj.has("html_string"))?jsonObj.getString("html_string"):"") %>
    <%= ((jsonObj.has("html_submissionStatus"))?jsonObj.getString("html_submissionStatus"):"") %>
	
    <table class="table" id="table_header_1" width="727" cellspacing="0" style='font-size:12px;'>

        <tr>
            <td width="91"><div style='border: 1px solid black;text-align: center;'>ছেলে হোক, মেয়ে হোক, <br> দুটি সন্তানই যথেষ্ট। </div></td>
            <td width="362" style='text-align: center;'><b> গণপ্রজাতন্ত্রী বাংলাদেশ সরকার <br>
                পরিবার পরিকল্পনা অধিদপ্তর <br>পরিবার পরিকল্পনা, মা ও শিশু স্বাস্থ্য কার্যক্রমের মাসিক অগ্রগতির প্রতিবেদন</b>
                <br>(UH&FWC এবং অন্যান্য প্রতিষ্ঠানের জন্য প্রযোজ্য)<br>
                <b>মাসঃ </b> <%= jsonObj.getString("bangla_month") %> <b>সালঃ</b> <%= jsonObj.getString("bangla_year") %> </td>
            <td width="266" class='page_no' style='text-align: right;'> এমআইএস ফরম-৩<br>
                পৃষ্ঠা-১ </td>
        </tr><tr><td colspan='3'><img width='60' src='./image/dgfp_logo.png'></td>
    </tr><tr></table>
    <br/>
    <table class="table" id="table_header_2" width="730" cellspacing="0" style='font-size:12px;'>
        <tr>
            <td style='width: 38%'><b>কেন্দ্রের নামঃ </b> <%= jsonObj.getString("facility") %> </td>
            <td><b>ইউনিয়নঃ</b> <%= ((jsonObj.has("union"))?jsonObj.getString("union"):"") %></td>
            <td style='text-align: center;'><b>উপজেলা/থানাঃ </b><%= ((jsonObj.has("upazilla"))?jsonObj.getString("upazilla"):"") %></td>
            <td style='text-align: right;'><b>জেলাঃ</b> <%= jsonObj.getString("zilla") %></td>
        </tr>
    </table>
    <br />

    <table class="table page_1 table_hide" id="table_1" cellspacing="0" style='font-size:12px;'>
        <tr>
            <td width="37%" style='width: 38%'> কেন্দ্রটিতে ২৪/৭ ডেলিভেরী সেবা দেয়া হয় কিনা? </td>
			
			<%
			if(delivery_facility_center == 1)
			{
			%>
			<td width="8%"><input type="checkbox" class="mis3_field" checked="checked" disabled="disabled"/> হ্যা </td>
            <td width="9%" style='text-align: center;'> <input type="checkbox" class="mis3_field" disabled="disabled"/> না </td>
            <td width="46%" style='text-align: right;'>&nbsp;</td>
			<%
			}
			else if(delivery_facility_center == 2)
			{
			%>
			<td width="8%"><input type="checkbox" class="mis3_field" disabled="disabled"/> হ্যা </td>
            <td width="9%" style='text-align: center;'> <input type="checkbox" class="mis3_field" checked="checked" disabled="disabled"/> না </td>
            <td width="46%" style='text-align: right;'>&nbsp;</td>
			
			<%
			}
			else{
			%>
            <td width="8%"><input type="checkbox" class="mis3_field" disabled="disabled"/> হ্যা </td>
            <td width="9%" style='text-align: center;'> <input type="checkbox" class="mis3_field" disabled="disabled"/> না </td>
            <td width="46%" style='text-align: right;'>&nbsp;</td>

            <%
			}
            %>
          
        </tr></table>
    <br />
    <div class="table-responsive">

        <table height="972" border="1" cellpadding="0" cellspacing="0" class="table page_1 table_hide" id="table_2" style="font-size:12px; border:solid; border-width:1px; border-color:#000000;">

            <tr class='danger'>
                <td height="31" colspan="6" style="border-top:1px solid black;"><div align="center"> সেবার ধরণ </div></td>
                <td width="63" style="border-top:1px solid black;"><div align="center"> ক্লিনিক / হাসপাতাল </div></td>
                <td width="70" style="border-top:1px solid black;"><div align="center"> স্যাটেলাইট  ক্লিনিক </div></td>
                <td width="56" style="border-top:1px solid black;"><div align="center"> মোট </div></td>
            </tr>
            <tr>
                <td width="131" rowspan="53" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black; width:56px;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> পরিবার পরিকল্পনা পদ্ধতি </span></td>
                <td colspan="2" rowspan="8" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'> খাবার বড়ি </td>
                <td class='danger' width="92" rowspan="3" style="border-right:1px solid black;border-top:1px solid black;"><div align="center"> স্বাভাবিক</div></td>
                <td class='success' colspan="2" style="border-bottom:1px solid black;border-top:1px solid black;"> পুরাতন </td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_old_center") %> </td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_old_satelite") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_old_center") + jsonObj.getInt("pill_old_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2"><div align="left">নতুন </div></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_new_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_new_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_new_center") + jsonObj.getInt("pill_new_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;" class='success'><div align="left"> মোট </div></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_new_center") + jsonObj.getInt("pill_old_center") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_new_satelite") + jsonObj.getInt("pill_old_satelite") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_new_center") + jsonObj.getInt("pill_old_center") + jsonObj.getInt("pill_new_satelite") + jsonObj.getInt("pill_old_satelite") %></td>
            </tr>
            <tr>
                <td rowspan="4" style="border-right:1px solid black;border-top:1px solid black;" class="danger"><div align="center">*প্রসব পরবর্তী </div></td>
                <td colspan="2" style="border-top:1px solid black;border-top:1px solid black;" ><div align="left">পুরাতন</div></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_old_center") %> </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_old_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_old_center") + jsonObj.getInt("pill_ppfp_old_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-right:1px solid black;border-top:1px solid black;" class='success'><div align="left">নতুন</div></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_ppfp_new_center") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_ppfp_new_satelite") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("pill_ppfp_new_center") + jsonObj.getInt("pill_ppfp_new_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-right:1px solid black;border-top:1px solid black;">মোট</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_new_center") + jsonObj.getInt("pill_ppfp_old_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_new_satelite") + jsonObj.getInt("pill_ppfp_old_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_new_center") + jsonObj.getInt("pill_ppfp_old_center") + jsonObj.getInt("pill_ppfp_new_satelite") + jsonObj.getInt("pill_ppfp_old_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-right:1px solid black;border-top:1px solid black;">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_expire_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_expire_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("pill_ppfp_expire_center") + jsonObj.getInt("pill_ppfp_expire_satelite") %></td>
            </tr>
            <tr>
                <td colspan="3" style='border-top: 1px solid black;'>  পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                <td style="border-top:1px solid black;">0</td>
                <td style="border-top:1px solid black;">0</td>
                <td style="border-top:1px solid black;">0</td>
            </tr>
            <tr>
                <td colspan="2" rowspan="7" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'>কনডম </td>
                <td rowspan="3" style="border-right:1px solid black;border-top:1px solid black;" class="danger"><div align="center">স্বাভাবিক </div></td>
                <td colspan="2" style="border-top:1px solid black;" class='success'> পুরাতন </td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("condom_old_center") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("condom_old_satelite") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("condom_old_center") + jsonObj.getInt("condom_old_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;"><div align="left"> নতুন </div></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_new_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_new_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_new_satelite") + jsonObj.getInt("condom_new_center") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;" class='success'> মোট </td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("condom_new_center") + jsonObj.getInt("condom_old_center") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("condom_new_satelite") + jsonObj.getInt("condom_old_satelite") %></td>
                <td style="border-top:1px solid black;" class='success'><%= jsonObj.getInt("condom_new_center") + jsonObj.getInt("condom_old_center") + jsonObj.getInt("condom_new_satelite") + jsonObj.getInt("condom_old_satelite") %></td>
            </tr>
            <tr>
                <td rowspan="4" style="border-top:1px solid black;border-right:1px solid black;" class="danger"><div align="center"> *প্রসব পরবর্তী </div></td>
                <td colspan="2" style="border-top:1px solid black;">পুরাতন </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_old_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_old_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_old_center") + jsonObj.getInt("condom_ppfp_old_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;">নতুন</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_new_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_new_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_new_satelite") + jsonObj.getInt("condom_ppfp_new_center") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;">মোট</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_new_center") + jsonObj.getInt("condom_ppfp_old_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_new_satelite") + jsonObj.getInt("condom_ppfp_old_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_new_center") + jsonObj.getInt("condom_ppfp_old_center") + jsonObj.getInt("condom_ppfp_new_satelite") + jsonObj.getInt("condom_ppfp_old_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_expire_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_expire_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("condom_ppfp_expire_center") + jsonObj.getInt("condom_ppfp_expire_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" rowspan="9" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'>ইনজেকটেবল</td>
                <td rowspan="3" style="border-top:1px solid black;border-right:1px solid black;" class="danger"><div align="center">স্বাভাবিক </div></td>
                <td colspan="2" style="border-top:1px solid black;">পুরাতন </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_center") + jsonObj.getInt("old_inject_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;">নতুন </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("new_inject_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("new_inject_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("new_inject_center") + jsonObj.getInt("new_inject_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;">মোট</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_center") + jsonObj.getInt("new_inject_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_satelite") + jsonObj.getInt("new_inject_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_center") + jsonObj.getInt("new_inject_center") + jsonObj.getInt("old_inject_satelite") + jsonObj.getInt("new_inject_satelite") %></td>
            </tr>
            <tr>
                <td rowspan="4" style="border-top:1px solid black;border-right:1px solid black;" class="danger"><div align="center">*প্রসব পরবর্তী </div></td>
                <td colspan="2" style="border-top:1px solid black;" class="success">পুরাতন </td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("old_inject_ppfp_center") %></td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("old_inject_ppfp_satelite") %></td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("old_inject_ppfp_center") + jsonObj.getInt("old_inject_ppfp_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;">নতুন</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("new_inject_ppfp_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("new_inject_ppfp_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("new_inject_ppfp_center") + jsonObj.getInt("new_inject_ppfp_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;">মোট</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_ppfp_center") + jsonObj.getInt("new_inject_ppfp_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_ppfp_satelite") + jsonObj.getInt("new_inject_ppfp_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("old_inject_ppfp_center") + jsonObj.getInt("new_inject_ppfp_center") + jsonObj.getInt("old_inject_ppfp_satelite") + jsonObj.getInt("new_inject_ppfp_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("inject_ppfp_expire_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("inject_ppfp_expire_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("inject_ppfp_expire_center") + jsonObj.getInt("inject_ppfp_expire_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top:1px solid black;"><div align="left">ফলোআপ </div></td>
                <td style="border-top:1px solid black;">0</td>
                <td style="border-top:1px solid black;">0</td>
                <td style="border-top:1px solid black;">0</td>
            </tr>
            <tr>
                <td colspan="3" style="border-top:1px solid black;"><div align="left">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা</div></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("sideEffect_inj_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("sideEffect_inj_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("sideEffect_inj_center") + jsonObj.getInt("sideEffect_inj_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" rowspan="8" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right:1px solid black;'> আই ইউ ডি </td>
                <td colspan="3" style="border-top:1px solid black;"> স্বাভাবিক </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_normal_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_normal_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_normal_satelite") + jsonObj.getInt("iud_normal_center") %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top:1px solid black;"><div align="left"> *প্রসব পরবর্তী </div></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_after_delivery_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_after_delivery_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_after_delivery_center") + jsonObj.getInt("iud_after_delivery_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top:1px solid black;">মোট </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_after_delivery_center") + jsonObj.getInt("iud_normal_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_after_delivery_satelite") + jsonObj.getInt("iud_normal_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_after_delivery_center") + jsonObj.getInt("iud_after_delivery_satelite") + jsonObj.getInt("iud_normal_center") + jsonObj.getInt("iud_normal_satelite") %></td>
            </tr>
            <tr>
                <td rowspan="2" style="border-top:1px solid black;border-right:1px solid black;" class="danger"><div align="center"> খুলে ফেলা </div></td>
                <td colspan="2" style="border-top:1px solid black;" class="success"> মেয়াদ পূর্ণ হওয়ার পর </td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("after_time_finish_center") %></td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("after_time_finish_satelite") %></td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("after_time_finish_center") + jsonObj.getInt("after_time_finish_satelite") %></td>
            </tr>
            <tr>
                <td height="22" colspan="2" style="border-top:1px solid black;"> মেয়াদ পূর্ণ হওয়ার পূর্বে </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("before_time_finish_center") %> </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("before_time_finish_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("before_time_finish_center") + jsonObj.getInt("before_time_finish_satelite") %> </td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top:1px solid black;">ফলোআপ </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_followup_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_followup_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_followup_center") + jsonObj.getInt("iud_followup_satelite") %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top:1px solid black;">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_ppfp_expire_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_ppfp_expire_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_ppfp_expire_center") + jsonObj.getInt("iud_ppfp_expire_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top:1px solid black;">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_sideeffect_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_sideeffect_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("iud_sideeffect_center") + jsonObj.getInt("iud_sideeffect_satelite") %></td>
            </tr>
            <tr>
                <td colspan="2" rowspan="8" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'> ইমপ্ল্যান্ট </td>
                <td colspan="3" style="border-top:1px solid black;">স্বাভাবিক </td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("implant_new") %></td>
                <td rowspan="5" style="border-top:1px solid black; background-color:#999999;" class="success"></td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.getInt("implant_new") %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top:1px solid black;">*প্রসব পরবর্তী </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_after_delivery") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_after_delivery") %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top:1px solid black;">মোট</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_after_delivery") + jsonObj.getInt("implant_new") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_after_delivery") + jsonObj.getInt("implant_new") %></td>
            </tr>
            <tr>
                <td rowspan="2" style="border-top:1px solid black;border-right:1px solid black;" class="danger"><div align="center"> খুলে ফেলা </div></td>
                <td colspan="2" style="border-top:1px solid black;"> মেয়াদ পূর্ণ হওয়ার পর </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_after_time_finish") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_after_time_finish") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;"> মেয়াদ পূর্ণ হওয়ার পূর্বে </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_before_time_finish") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_before_time_finish") %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top:1px solid black;"> ফলোআপ </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_followup_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_followup_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_followup_center") + jsonObj.getInt("implant_followup_satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top:1px solid black;">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_ppfp_expire_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_ppfp_expire_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_ppfp_expire_center") + jsonObj.getInt("implant_ppfp_expire_satelite") %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top:1px solid black;"> পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_sideeffect_center") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_sideeffect_satelite") %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.getInt("implant_sideeffect_center") + jsonObj.getInt("implant_sideeffect_satelite") %></td>
            </tr>
            <tr>
                <td width="80" rowspan="10" class="danger"  style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top:1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> স্থায়ি পদ্ধতি </span></td>
                <td width="63" rowspan="5" class="danger" style="border-right: 1px solid black;border-top:1px solid black;"><div align="center"> পুরুষ </div></td>
                <td rowspan="3" style="border-top:1px solid black;border-right:1px solid black;" class="danger"><div align="center"> সম্পাদন </div></td>
                <td colspan="2" style="border-top:1px solid black;" class="success"> স্বাভাবিক </td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.has("permanent_method_normal_male")? jsonObj.getInt("permanent_method_normal_male"): "0" %></td>
                <td rowspan="3" style="border-top:1px solid black; background-color:#999999;" class="success"></td>
                <td style="border-top:1px solid black;" class="success"><%= jsonObj.has("permanent_method_normal_male")? jsonObj.getInt("permanent_method_normal_male"): "0" %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;"> *প্রসব পরবর্তী </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_after_delivery_male")? jsonObj.getInt("permanent_method_after_delivery_male"): "0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_after_delivery_male")? jsonObj.getInt("permanent_method_after_delivery_male"): "0" %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;">মোট </td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_normal_male")? jsonObj.getInt("permanent_method_normal_male"): 0) + (jsonObj.has("permanent_method_after_delivery_male")? jsonObj.getInt("permanent_method_after_delivery_male"): 0) %></td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_normal_male")? jsonObj.getInt("permanent_method_normal_male"): 0) + (jsonObj.has("permanent_method_after_delivery_male")? jsonObj.getInt("permanent_method_after_delivery_male"): 0) %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top:1px solid black;"> ফলোআপ </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_followup_male_center")? jsonObj.getInt("permanent_method_followup_male_center"): "0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_male_followup_satelite")? jsonObj.getInt("permanent_male_followup_satelite"): "0" %></td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_followup_male_center")? jsonObj.getInt("permanent_method_followup_male_center"):0) + (jsonObj.has("permanent_male_followup_satelite")? jsonObj.getInt("permanent_male_followup_satelite"):0) %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top:1px solid black;">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_complication_male")? jsonObj.getInt("permanent_method_complication_male"): "0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_male_sideeffect_satelite")? jsonObj.getInt("permanent_male_sideeffect_satelite"): "0" %></td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_complication_male")? jsonObj.getInt("permanent_method_complication_male"):0) + (jsonObj.has("permanent_male_sideeffect_satelite")? jsonObj.getInt("permanent_male_sideeffect_satelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="5" style="border-right: 1px solid black;border-top: 1px solid black;" class="danger"><div align="center">মহিলা</div></td>
                <td rowspan="3" style="border-top:1px solid black;" class="danger"><div align="center">সম্পাদন</div></td>
                <td colspan="2" style="border-top:1px solid black;">স্বাভাবিক </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_normal_female")? jsonObj.getInt("permanent_method_normal_female"): "0" %></td>
                <td rowspan="3" style="border-top:1px solid black; background-color:#999999;" class="success"></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_normal_female")? jsonObj.getInt("permanent_method_normal_female"): "0" %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;"> *প্রসব পরবর্তী </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_after_delivery_female")? jsonObj.getInt("permanent_method_after_delivery_female"): "0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_after_delivery_female")? jsonObj.getInt("permanent_method_after_delivery_female"): "0" %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;">মোট </td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_normal_female")? jsonObj.getInt("permanent_method_normal_female"):0) + (jsonObj.has("permanent_method_after_delivery_female")? jsonObj.getInt("permanent_method_after_delivery_female"):0) %></td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_normal_female")? jsonObj.getInt("permanent_method_normal_female"):0) + (jsonObj.has("permanent_method_after_delivery_female")? jsonObj.getInt("permanent_method_after_delivery_female"):0) %></td>
            </tr>
            <tr class="success">
                <td height="16" colspan="3" style="border-top:1px solid black;"> ফলোআপ </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_followup_female_center")? jsonObj.getInt("permanent_method_followup_female_center"):"0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_female_followup_satelite")? jsonObj.getInt("permanent_female_followup_satelite"):"0" %></td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_followup_female_center")? jsonObj.getInt("permanent_method_followup_female_center"):0) + (jsonObj.has("permanent_female_followup_satelite")? jsonObj.getInt("permanent_female_followup_satelite"):0) %></td>
            </tr>
            <tr>
                <td height="26" colspan="3" style="border-top:1px solid black;"> পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_method_complication_female")? jsonObj.getInt("permanent_method_complication_female"):"0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("permanent_female_sideeffect_satelite")? jsonObj.getInt("permanent_female_sideeffect_satelite"):"0" %></td>
                <td style="border-top:1px solid black;"><%= (jsonObj.has("permanent_method_complication_female")? jsonObj.getInt("permanent_method_complication_female"):0) + (jsonObj.has("permanent_female_sideeffect_satelite")? jsonObj.getInt("permanent_female_sideeffect_satelite"):0) %></td>
            </tr>
            <tr class="success">
                <td height="22" colspan="5" style="border-top:1px solid black;">গর্ভপাত পরবর্তী (PAC) পরিবার পরিকল্পনা </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("pac_after_fp")? jsonObj.getInt("pac_after_fp"):"0" %></td>
                <td style="border-top:1px solid black;background-color:#999999;" rowspan="3">&nbsp;</td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("pac_after_fp")? jsonObj.getInt("pac_after_fp"):"0" %></td>
            </tr>
            <tr>
                <td height="23" colspan="5" style="border-top:1px solid black;">এমআর পরবর্তী পরিবার পরিকল্পনা </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("mr_after_fp")? jsonObj.getInt("mr_after_fp"):"0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("mr_after_fp")? jsonObj.getInt("mr_after_fp"):"0" %></td>
            </tr>
            <tr class="success">
                <td height="21" colspan="5" style="border-top:1px solid black;">এমআরএম পরবর্তী পরিবার পরিকল্পনা </td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("mrm_after_fp")? jsonObj.getInt("mrm_after_fp"):"0" %></td>
                <td style="border-top:1px solid black;"><%= jsonObj.has("mrm_after_fp")? jsonObj.getInt("mrm_after_fp"):"0" %></td>
            </tr>
			
			
           
        </table>
    </div>
    <table border="0" cellpadding="0"  class="table page_1 table_hide" id="table_3" style="font-size:11px;">
        <tr>
            <td> * প্রসব পরবর্তী > প্রসবের পর হতে ১ বছরের মধ্যে </td>
        </tr>

  </table>

   
		
  </table>

    <div class="printer">

        <table class="table page_2 table_hide" id="table_4" cellspacing="0" style='font-size:10px;display:none;'>
            <tr><td width="89">&nbsp;</td>
                <td width="359" style='text-align: center;'>&nbsp;</td>
                <!--<td width="273" class='page_no' style='text-align: right;'>?????? ???-?<br>-->
                <td width="273" style='text-align: right;'> এমআইএস ফরম-৩<br> পৃষ্ঠা-2 </td>
            <tr></table>

        <table width="730" height="1473" cellpadding="0" cellspacing="0" class="table page_2 table_hide" id="table_5" style="font-size:11px; border:solid; border-width:1px;display:none;">
            <tr class='danger'>
                <td height="44" colspan="4"  style="border-top: 1px solid black;border-right: 1px solid black;"><div align="center"> সেবার ধরণ </div></td>
                <td width="61" style="border-top: 1px solid black;border-right: 1px solid black;"><div align="center">  ক্লিনিক / হাসপাতাল </div></td>
                <td width="65" style="border-top: 1px solid black;border-right: 1px solid black;"><div align="center">স্যাটেলাইট ক্লিনিক </div></td>
                <td width="47" style="border-top: 1px solid black;"><div align="center"> মোট </div></td>
            </tr>
            <tr>
                <td class="danger" rowspan="54" style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;border-top: 1px solid black;width:56px;'>
                    <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> প্রজনন স্বাস্থ্য সেবা </span></td>
                <td width="101" rowspan="5" class="danger" style='text-align: left;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> টি টি প্রাপ্ত মহিলা </span></td>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;" class="success">১ম ডোজ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success">0</td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> ২য় ডোজ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;">৩য় ডোজ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> ৪ থ  ডোজ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> ৫ম ডোজ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">0</td>
            </tr>
            <tr>
                <td rowspan="8" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right:1px solid black;'>
                    <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> গর্ভ কালীন সেবা </span></td>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;">পরিদর্শন - ১ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc1center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc1satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc1center") + jsonObj.getInt("anc1satelite")%></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;">পরিদর্শন - ২ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc2center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc2satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc2center") + jsonObj.getInt("anc2satelite")%></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> পরিদর্শন - ৩ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc3center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc3satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc3center") + jsonObj.getInt("anc3satelite")%></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> পরিদর্শন - ৪ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc4center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc4satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anc4center") + jsonObj.getInt("anc4satelite")%></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি বিষয়ে কাউনসেলিং প্রাপ্ত মায়ের সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ancadvicecenter")? jsonObj.getInt("ancadvicecenter"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ancadvicesatelite")? jsonObj.getInt("ancadvicesatelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("ancadvicecenter")? jsonObj.getInt("ancadvicecenter"):0) + (jsonObj.has("ancadvicesatelite")? jsonObj.getInt("ancadvicesatelite"):0) %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> প্রসব পূর্ব রক্ত ক্ষরণ (APH) আক্রান্ত মায়ের সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ancAPHcenter")? jsonObj.getInt("ancAPHcenter"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ancAPHsatelite")? jsonObj.getInt("ancAPHsatelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("ancAPHcenter")? jsonObj.getInt("ancAPHcenter"):0) + (jsonObj.has("ancAPHsatelite")? jsonObj.getInt("ancAPHsatelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> ২৪-৩৪ সপ্তাহের মধ্যে ইনজেক্শান এন্টিনেটাল করটিকোস্টেরয়েড প্রাপ্ত গর্ভবতি মায়ের সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ancAntinatalKorticenter")? jsonObj.getInt("ancAntinatalKorticenter"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;"></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ancAntinatalKorticenter")? jsonObj.getInt("ancAntinatalKorticenter"):0 %>
                </td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black; border-right: 1px solid black;"> মিসোপ্রোস্টল বড়ি সরবরাহপ্রাপ্ত গর্ভবতীর সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("ancmisoprostolcenter") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("ancmisoprostolsatelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("ancmisoprostolcenter") + jsonObj.getInt("ancmisoprostolsatelite")%></td>
            </tr>
            <tr>
                <td rowspan="6" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> প্রসব সেবা</span></td>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> স্বাভাবিক </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("normaldelivery") %></td>
                <td rowspan="6" style="border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;">&nbsp;</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("normaldelivery") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> সিজারিয়ান </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black; "><%= jsonObj.getInt("csectiondelivery") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("csectiondelivery") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;"> অন্যান্য (ফরসেপ / ভ্যাকুয়াম / ব্রিচ) </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("others_forcep_center")? jsonObj.getInt("others_forcep_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("others_forcep_center")? jsonObj.getInt("others_forcep_center"):0 %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;">প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনা (AMTSL) অনুসরন করে প্রসব করানোর সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("amtsl") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("amtsl") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;">পার্টোগ্রাফ ব্যবহার সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("partograph_using_center")? jsonObj.getInt("partograph_using_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("partograph_using_center")? jsonObj.getInt("partograph_using_center"):0 %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;">প্রসব কালিন অতিরিক্ত রক্তক্ষরণ (IPH) আক্রান্ত মায়ের সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("deliveryIPHcenter")? jsonObj.getInt("deliveryIPHcenter"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("deliveryIPHcenter")? jsonObj.getInt("deliveryIPHcenter"):0 %></td>
            </tr>
            <tr>
                <td colspan="3" style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="left"> গর্ভপাত পরবর্তী সেবা (PAC) প্রদানের সংখ্যা </div></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pacservicecount") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("pacservicecountsatelite")? jsonObj.getInt("pacservicecountsatelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pacservicecount") + jsonObj.getInt("pacservicecountsatelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;border-right: 1px solid black;"><div align="left">মারাত্তক প্রি-একলাম্পসিয়া রোগীর সংখ্যা </div></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anchighpreeclampsiacenter") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anchighpreeclampsiasatelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("anchighpreeclampsiacenter") + jsonObj.getInt("anchighpreeclampsiasatelite") %></td>
            </tr>
            <tr>
                <td class='danger' height="24" rowspan="15" style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> প্রসবোত্তর সেবা </span></td>
                <td class='danger' width="89" rowspan="6" style='text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">মা </span></td>
                <td width="273" style="border-top: 1px solid black;border-right: 1px solid black;"> পরিদর্শন - ১ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother1center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother1satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother1center") + jsonObj.getInt("pncmother1satelite")%></td>
            </tr>
            <tr class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;">পরিদর্শন - ২ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother2center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother2satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother2center") + jsonObj.getInt("pncmother2satelite") %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> পরিদর্শন - ৩ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother3center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother3satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother3center") + jsonObj.getInt("pncmother3satelite") %></td>
            </tr>
            <tr class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;">পরিদর্শন - ৪ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother4center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother4satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmother4center") + jsonObj.getInt("pncmother4satelite") %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি বিষয়ে কাউনসেলিং প্রাপ্ত মায়ের সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmotherfpcenter")  %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmotherfpsatelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmotherfpcenter") + jsonObj.getInt("pncmotherfpsatelite") %></td>
            </tr>
            <tr class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;">প্রসবোওর অতিরিক্ত রক্তক্ষরণ (PPH) আক্রান্ত মায়ের সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("pncmotherPPHcenter")? jsonObj.getInt("pncmotherPPHcenter"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("pncmotherPPHsatelite")? jsonObj.getInt("pncmotherPPHsatelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("pncmotherPPHcenter")? jsonObj.getInt("pncmotherPPHcenter"):0)+(jsonObj.has("pncmotherPPHsatelite")? jsonObj.getInt("pncmotherPPHsatelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="9" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> নবজাতক </span></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">১ মিনিটের মধ্যে মোছানোর সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("dryingafterbirth") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;">&nbsp;</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("dryingafterbirth") %></td>
            </tr>
            <tr class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> নাড়িকাটার পর ৭.১% ক্লোরহেক্সিডিন ব্যবহারের সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("chlorehexidin") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black; background-color:#999999;">&nbsp;</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("chlorehexidin") %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">নাড়িকাটার পর মায়ের ত্বকে-ত্বক স্পর্শ প্রাপ্ত নবজাতকের সংখ্যা   </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("skintouch") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;">&nbsp;</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("skintouch") %></td>
            </tr>
            <tr class="success">
                <td height="26" style="border-top:1px solid black;border-right: 1px solid black;"> জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাউয়ানোর সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("breastfeed") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;">&nbsp;</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("breastfeed") %></td>
            </tr>
            <tr>
                <td height="28" style="border-top: 1px solid black;border-right: 1px solid black;"> জন্মকালীন শ্বাসকষ্টে আক্রান্ত শিশুকে ব্যাগ ও মাস্ক ব্যবহার করে রিসাসিটেইট করার সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("bagnmask") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;">&nbsp;</td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("bagnmask") %></td>
            </tr>
            <tr class="success">
                <td height="30" style="border-top:1px solid black;border-right: 1px solid black;">পরিদর্শন - ১ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild1center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild1satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild1center") + jsonObj.getInt("pncchild1satelite") %></td>
            </tr>
            <tr>
                <td height="23" style="border-top:1px solid black;border-right: 1px solid black;">পরিদর্শন - ২ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild2center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild2satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild2center") + jsonObj.getInt("pncchild2satelite") %></td>
            </tr>
            <tr class="success">
                <td height="31" style="border-top: 1px solid black;border-right: 1px solid black;">পরিদর্শন - ৩ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild3center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild3satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild3center") + jsonObj.getInt("pncchild3satelite") %></td>
            </tr>
            <tr>
                <td height="25" style="border-top:1px solid black;border-right: 1px solid black;"> পরিদর্শন - ৪ </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild4center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild4satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncchild4center") + jsonObj.getInt("pncchild4satelite") %></td>
            </tr>
            <tr>
                <td rowspan="5" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">রেফার </span></td>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;" class="success">গর্ভকালীন জতিলতার রেফার সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success"><%= jsonObj.getInt("ancrefercenter") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success"><%= jsonObj.getInt("ancrefersatelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success"><%= jsonObj.getInt("ancrefercenter") + jsonObj.getInt("ancrefersatelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;border-right: 1px solid black;"> প্রসবকালীন জতিলতার রেফার সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("deliveryrefercenter") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("deliveryrefersatelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("deliveryrefercenter") + jsonObj.getInt("deliveryrefersatelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top:1px solid black;border-right: 1px solid black;">  প্রসবোত্তর জতিলতার রেফার সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmotherrefercenter") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmotherrefersatelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("pncmotherrefercenter") + jsonObj.getInt("pncmotherrefersatelite") %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;border-right: 1px solid black;"> একলাম্পসিয়া রোগীকে লোডীং ডোজ MgSO4 ইনজেকশান দিয়ে রেফার সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("ancmgso4center") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("ancmgso4satelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("ancmgso4center") + jsonObj.getInt("ancmgso4satelite") %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;">নবজাতক কে জতিলতার জন্য রেফার সংখ্যা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("newbornreferalcenter") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("newbornreferalsatelite") %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.getInt("newbornreferalcenter") + jsonObj.getInt("newbornreferalsatelite") %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;border-right: 1px solid black;"><div align="left"> ইসিপি গ্রহন কারীর সংখ্যা </div></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ecp_taken_center")? jsonObj.getInt("ecp_taken_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("ecp_taken_satelite")? jsonObj.getInt("ecp_taken_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">
                    <%= (jsonObj.has("ecp_taken_center")? jsonObj.getInt("ecp_taken_center"):0) + (jsonObj.has("ecp_taken_satelite")? jsonObj.getInt("ecp_taken_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td rowspan="2" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'>বন্ধ্যা দম্পতি </td>
                <td colspan="2" style="border-top: 1px solid black;border-right: 1px solid black;" class="success"> চিকিৎসা / পরামর্শ প্রাপ্ত </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"  class="success">
                    <%= jsonObj.has("infertility_treatment_center")? jsonObj.getInt("infertility_treatment_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"  class="success">
                    <%= jsonObj.has("infertility_treatment_satelite")? jsonObj.getInt("infertility_treatment_satelite"):0 %>
                </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"  class="success">
                    <%= (jsonObj.has("infertility_treatment_center")? jsonObj.getInt("infertility_treatment_center"):0) + (jsonObj.has("infertility_treatment_satelite")? jsonObj.getInt("infertility_treatment_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top:1px solid black;border-right: 1px solid black;">রেফার কৃত </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">
                    <%= jsonObj.has("infertility_refer_center")? jsonObj.getInt("infertility_refer_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">
                    <%= jsonObj.has("infertility_refer_satelite")? jsonObj.getInt("infertility_refer_satelite"):0 %>
                </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">
                    <%= (jsonObj.has("infertility_refer_center")? jsonObj.getInt("infertility_refer_center"):0) + (jsonObj.has("infertility_refer_satelite")? jsonObj.getInt("infertility_refer_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="danger" rowspan="10" style='width:160px; text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'> <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> কিশোর / কিশোরীর সেবা <br />( ১০-১৯) বছর </span></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success"> বয়ঃসন্ধিকালীন পরিবর্তন বিষয়ে কাউনসেলিং </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success"><%= jsonObj.has("adolescent_change_counceling_center")? jsonObj.getInt("adolescent_change_counceling_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success"><%= jsonObj.has("adolescent_change_counceling_satelite")? jsonObj.getInt("adolescent_change_counceling_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;" class="success">
                    <%= (jsonObj.has("adolescent_change_counceling_center")? jsonObj.getInt("adolescent_change_counceling_center"):0) + (jsonObj.has("adolescent_change_counceling_satelite")? jsonObj.getInt("adolescent_change_counceling_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> বাল্য বিবাহ ও কিশোরী মাতৃত্বের কুফল বিষয়ে কাউন্সেলিং  </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_marraige_counceling_center")? jsonObj.getInt("adolescence_marraige_counceling_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_marraige_counceling_satelite")? jsonObj.getInt("adolescence_marraige_counceling_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_marraige_counceling_center")? jsonObj.getInt("adolescence_marraige_counceling_center"):0) + (jsonObj.has("adolescence_marraige_counceling_satelite")? jsonObj.getInt("adolescence_marraige_counceling_satelite"):0) %></td>
            </tr>
            <tr  class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> আয়রন ফলিক এসিড বড়ি সরবরাহ প্রাপ্ত কিশোরীর সংখ্যা  </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_folicacide_supply_center")? jsonObj.getInt("adolescence_folicacide_supply_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_folicacide_supply_satelite")? jsonObj.getInt("adolescence_folicacide_supply_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_folicacide_supply_center")? jsonObj.getInt("adolescence_folicacide_supply_center"):0) + (jsonObj.has("adolescence_folicacide_supply_satelite")? jsonObj.getInt("adolescence_folicacide_supply_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> প্রজননতন্ত্রের সংক্রামন ও যৌনবাহিত রোগ বিষয়ে কাউন্সেলিং </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_sexual_disease_counceling_center")? jsonObj.getInt("adolescence_sexual_disease_counceling_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_sexual_disease_counceling_satelite")? jsonObj.getInt("adolescence_sexual_disease_counceling_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_sexual_disease_counceling_center")? jsonObj.getInt("adolescence_sexual_disease_counceling_center"):0) + (jsonObj.has("adolescence_sexual_disease_counceling_satelite")? jsonObj.getInt("adolescence_sexual_disease_counceling_satelite"):0) %></td>
            </tr>
            <tr  class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> প্রজননতন্ত্রের সংক্রামন ও যৌনবাহিত রোগ নিরাময়ে চিকাৎসা </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_sexual_disease_treatment_center")? jsonObj.getInt("adolescence_sexual_disease_treatment_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_sexual_disease_treatment_satelite")? jsonObj.getInt("adolescence_sexual_disease_treatment_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_sexual_disease_treatment_center")? jsonObj.getInt("adolescence_sexual_disease_treatment_center"):0) + (jsonObj.has("adolescence_sexual_disease_treatment_satelite")? jsonObj.getInt("adolescence_sexual_disease_treatment_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">  বিভিন্ন ধরনের পুস্টিকর ও সুষম খাবার বিষয়ে কাউন্সেলিং </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_food_counceling_center")? jsonObj.getInt("adolescence_food_counceling_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_food_counceling_satelite")? jsonObj.getInt("adolescence_food_counceling_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_food_counceling_center")? jsonObj.getInt("adolescence_food_counceling_center"):0) + (jsonObj.has("adolescence_food_counceling_satelite")? jsonObj.getInt("adolescence_food_counceling_satelite"):0) %></td>
            </tr>
            <tr  class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> কৈশোর সহিংসতা প্রতিরোধ  বিষয়ে কাউনসেলিং </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_fighting_counceling_center")? jsonObj.getInt("adolescence_fighting_counceling_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_fighting_counceling_satelite")? jsonObj.getInt("adolescence_fighting_counceling_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_fighting_counceling_center")? jsonObj.getInt("adolescence_fighting_counceling_center"):0) + (jsonObj.has("adolescence_fighting_counceling_satelite")? jsonObj.getInt("adolescence_fighting_counceling_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> মানসিক সমস্যা এবং মাদকাসক্তি প্রতিরোধ ও নিরাময় কাউন্সেলিং </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_mentalhealth_counceling_center")? jsonObj.getInt("adolescence_mentalhealth_counceling_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_mentalhealth_counceling_satelite")? jsonObj.getInt("adolescence_mentalhealth_counceling_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_mentalhealth_counceling_center")? jsonObj.getInt("adolescence_mentalhealth_counceling_center"):0) + (jsonObj.has("adolescence_mentalhealth_counceling_satelite")? jsonObj.getInt("adolescence_mentalhealth_counceling_satelite"):0) %></td>
            </tr>
            <tr  class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> কিশোর - কিশোরীর ব্যক্তিগত পরিশকার পরিচছন্নতা ও কিশোরীর স্যানিটারি প্যাড ব্যাবহার বিষয়ে কাউন্সেলিং </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_sanitary_counceling_center")? jsonObj.getInt("adolescence_sanitary_counceling_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;">
                    <%= jsonObj.has("adolescence_sanitary_counceling_satelite")? jsonObj.getInt("adolescence_sanitary_counceling_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_sanitary_counceling_center")? jsonObj.getInt("adolescence_sanitary_counceling_center"):0) + (jsonObj.has("adolescence_sanitary_counceling_satelite")? jsonObj.getInt("adolescence_sanitary_counceling_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"> রেফার </td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_refer_center")? jsonObj.getInt("adolescence_refer_center"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= jsonObj.has("adolescence_refer_satelite")? jsonObj.getInt("adolescence_refer_satelite"):0 %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black;"><%= (jsonObj.has("adolescence_refer_center")? jsonObj.getInt("adolescence_refer_center"):0) + (jsonObj.has("adolescence_refer_satelite")? jsonObj.getInt("adolescence_refer_satelite"):0) %></td>
            </tr>
        </table>
		
    </div>


    <div class="printer">

        <table class="table page_3 table_hide" id="table_6" width="757" cellspacing="0" style='font-size:10px;display:none;'>
            <tr>
                <td width="86">&nbsp;</td>
                <td width="345" style='text-align: center;'>&nbsp;</td>
                <td width="318" style='text-align: right;'> এমআইএস ফরম-৩<br>
                    পৃষ্ঠা-৩ </td>
            <tr></table>

        <table max-width="730px" height="998" border="1" cellpadding="0" cellspacing="0" class="table page_3 table_hide" id="table_7" style="font-size:11px; border:solid; border-width:1px; border-color:#000000;display:none;">
            <tr class="danger">
                <td height="51" colspan="4" style="border-top: 1px solid black;"><div align="center">সেবার ধরণ</div></td>
                <td width="49" style="border-top: 1px solid black;"> <div align="center">ক্লিনিক / হাসপাতাল</div></td>
                <td width="63" style="border-top: 1px solid black;"><div align="center">স্যাটেলাইট  ক্লিনিক</div></td>
                <td width="49" style="border-top: 1px solid black;"><div align="center"><span style="text-align: center;">মোট</span></div></td>
            </tr>
            <tr>
                <td class="danger" rowspan="7" style='width:56px; text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;padding:20px;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">প্রজনন স্বাস্থ্য সেবা </span></td>
                <td class="success" colspan="3" style="border-top: 1px solid black;border-right: 1px solid black;">RTI / STI সংঙ্ক্রান্ত চিকিৎসা প্রদানকৃত রোগীর সংখ্যা </td>
                <td class="success" style="border-top: 1px solid black;"><%= jsonObj.getInt("gp_rta_sta_all_center") %></td>
                <td class="success" style="border-top: 1px solid black;"><%= jsonObj.getInt("gp_rta_sta_all_satelite") %></td>
                <td class="success" style="border-top: 1px solid black;"><%= jsonObj.getInt("gp_rta_sta_all_center") + jsonObj.getInt("gp_rta_sta_all_satelite") %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">এম আর (এম ভিএ) সেবা প্রদানের সংখ্যা </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("mr_mva_treatment_center")? jsonObj.getInt("mr_mva_treatment_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("mr_mva_treatment_satelite")? jsonObj.getInt("mr_mva_treatment_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("mr_mva_treatment_center")? jsonObj.getInt("mr_mva_treatment_center"):0) + (jsonObj.has("mr_mva_treatment_satelite")? jsonObj.getInt("mr_mva_treatment_satelite"):0) %> </td>
            </tr>
            <tr>
                <td class="success" colspan="3" style="border-top: 1px solid black;">এম আর এম (ঔষধের মাধ্যমে এম আর) সেবা প্রদানের সংখ্যা </td>
                <td class="success" style="border-top: 1px solid black;"><%= jsonObj.has("mrm_treatment")? jsonObj.getInt("mrm_treatment"):0 %></td>
                <td class="success" style="border-top: 1px solid black; background-color:#999999;">&nbsp;</td>
                <td class="success" style="border-top: 1px solid black;"><%= jsonObj.has("mrm_treatment")? jsonObj.getInt("mrm_treatment"):0 %></td>
            </tr>
            <tr>
                <td class="danger" width="134" rowspan="4" style="border-top: 1px solid black;border-right: 1px solid black;padding:23px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space:nowrap;width:40px;">
	জরায়ুর মুখ <br/>ও
	স্তন ক্যান্সার <br/>স্ক্রীনিং </span></td>
                <td colspan="2" style="border-top: 1px solid black;">মোট VIA পজিটিভ </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("total_via_positive_center")? jsonObj.getInt("total_via_positive_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("total_via_positive_satelite")? jsonObj.getInt("total_via_positive_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("total_via_positive_center")? jsonObj.getInt("total_via_positive_center"):0) + (jsonObj.has("total_via_positive_satelite")? jsonObj.getInt("total_via_positive_satelite"):0) %> </td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">মোট VIA নেগেটিভ</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("total_via_negative_center")? jsonObj.getInt("total_via_negative_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("total_via_negative_satelite")? jsonObj.getInt("total_via_negative_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("total_via_negative_center")? jsonObj.getInt("total_via_negative_center"):0) + (jsonObj.has("total_via_negative_satelite")? jsonObj.getInt("total_via_negative_satelite"):0) %> </td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">মোট CBE পজিটিভ</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("total_cbe_positive_center")? jsonObj.getInt("total_cbe_positive_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("total_cbe_positive_satelite")? jsonObj.getInt("total_cbe_positive_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("total_cbe_positive_center")? jsonObj.getInt("total_cbe_positive_center"):0) + (jsonObj.has("total_cbe_positive_satelite")? jsonObj.getInt("total_cbe_positive_satelite"):0) %> </td>
            </tr>
            <tr>
                <td colspan="2" class="success" style="border-top: 1px solid black;">মোট CBE  নেগেটিভ</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("total_cbe_negative_center")? jsonObj.getInt("total_cbe_negative_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("total_cbe_negative_satelite")? jsonObj.getInt("total_cbe_negative_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("total_cbe_negative_center")? jsonObj.getInt("total_cbe_negative_center"):0) + (jsonObj.has("total_cbe_negative_satelite")? jsonObj.getInt("total_cbe_negative_satelite"):0) %> </td>
            </tr>
            <tr>
                <td class="danger" rowspan="22" style="text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;padding:20px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">শিশু সেবা( ০-৫৯ মাস)</span></td>
                <td class="danger" rowspan="8" style="text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 30px;" > কেএমসি সেবা </span></td>
                <td colspan="2" style="border-top: 1px solid black;">কেএমসি শুরু হয়েছে এমন শিশুর সংখ্যা  </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("kmc_started_child_center")? jsonObj.getInt("kmc_started_child_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("kmc_started_child_satelite")? jsonObj.getInt("kmc_started_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("kmc_started_child_center")? jsonObj.getInt("kmc_started_child_center"):0) + (jsonObj.has("kmc_started_child_satelite")? jsonObj.getInt("kmc_started_child_satelite"):0) %> </td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">এই ক্লিনিক/হাসপাতালে জন্ম গ্রহনকারী কে এম সি শুরু করা শিশুর সংখ্যা  </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("kmc_birth_child_center")? jsonObj.getInt("kmc_birth_child_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("kmc_birth_child_satelite")? jsonObj.getInt("kmc_birth_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("kmc_birth_child_center")? jsonObj.getInt("kmc_birth_child_center"):0) + (jsonObj.has("kmc_birth_child_satelite")? jsonObj.getInt("kmc_birth_child_satelite"):0) %> </td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">মোট কতজন শিশুকে ডিসচার্জ (ছাড়পত্র)প্রদান করা হয়েছে</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("discharge_child_center")? jsonObj.getInt("discharge_child_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("discharge_child_satelite")? jsonObj.getInt("discharge_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("discharge_child_center")? jsonObj.getInt("discharge_child_center"):0) + (jsonObj.has("discharge_child_satelite")? jsonObj.getInt("discharge_child_satelite"):0) %> </td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">ডিসচার্জ অন রিকোয়েস্ট (অনুরোধের) এর ভিত্তিতে ডিসচার্জ প্রদান করা শিশুর সংখ্যা  </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("discharge_onrequest_child_center")? jsonObj.getInt("discharge_onrequest_child_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("discharge_onrequest_child_satelite")? jsonObj.getInt("discharge_onrequest_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("discharge_onrequest_child_center")? jsonObj.getInt("discharge_onrequest_child_center"):0) + (jsonObj.has("discharge_onrequest_child_satelite")? jsonObj.getInt("discharge_onrequest_child_satelite"):0) %> </td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">কেএমসি ডিসকন্টিনিউয়েশান (কেএমসি সেবা ব্যাহত)হওয়া শিশুর সংখ্যা </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("kmc_discontinuation_child_center")? jsonObj.getInt("kmc_discontinuation_child_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("kmc_discontinuation_child_satelite")? jsonObj.getInt("kmc_discontinuation_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("kmc_discontinuation_child_center")? jsonObj.getInt("kmc_discontinuation_child_center"):0) + (jsonObj.has("kmc_discontinuation_child_satelite")? jsonObj.getInt("kmc_discontinuation_child_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">কতজন শিশুকে জটিলতার জন্য রেফার করা হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("complicated_child_center")? jsonObj.getInt("complicated_child_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("complicated_child_satelite")? jsonObj.getInt("complicated_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("complicated_child_center")? jsonObj.getInt("complicated_child_center"):0) + (jsonObj.has("complicated_child_satelite")? jsonObj.getInt("complicated_child_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">কেএমসি সেবাপ্রাপ্ত শিশু মৃত্যুর সংখ্যা</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("kmc_treatment_child_center")? jsonObj.getInt("kmc_treatment_child_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("kmc_treatment_child_satelite")? jsonObj.getInt("kmc_treatment_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("kmc_treatment_child_center")? jsonObj.getInt("kmc_treatment_child_center"):0) + (jsonObj.has("kmc_treatment_child_satelite")? jsonObj.getInt("kmc_treatment_child_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">কতজন শিশুকে কেএমসি ফলোআপ করা হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("kmc_followup_child_center")? jsonObj.getInt("kmc_followup_child_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("kmc_followup_child_satelite")? jsonObj.getInt("kmc_followup_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("kmc_followup_child_center")? jsonObj.getInt("kmc_followup_child_center"):0) + (jsonObj.has("kmc_followup_child_satelite")? jsonObj.getInt("kmc_followup_child_satelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="14" style="text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;" class="danger"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space:nowrap;width:30px;">টিকা প্রাপ্ত (০-১৫ মাস বয়সী)<br/> শিশুর সংখ্যা  </span></td>
                <td colspan="2" style="border-top: 1px solid black;"> বিসিজি ( জন্মের পর থেকে ) </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("bcg_afterbirth_center")? jsonObj.getInt("bcg_afterbirth_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("bcg_afterbirth_satelite")? jsonObj.getInt("bcg_afterbirth_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;">
                    <%= (jsonObj.has("bcg_afterbirth_center")? jsonObj.getInt("bcg_afterbirth_center"):0) + (jsonObj.has("bcg_afterbirth_satelite")? jsonObj.getInt("bcg_afterbirth_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td class="danger" width="156" rowspan="3" style="border-top: 1px solid black;"> <div align="center">পেন্টাভ্যালেন্ট (ডিপিটি , হেপ-বি , হিব) </div></td>
                <td width="163" style="border-top: 1px solid black;" class="success">১ (শিশুর বয়স ৬ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("pentavolent_one_given_center")? jsonObj.getInt("pentavolent_one_given_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("pentavolent_one_given_satelite")? jsonObj.getInt("pentavolent_one_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("pentavolent_one_given_center")? jsonObj.getInt("pentavolent_one_given_center"):0) + (jsonObj.has("pentavolent_one_given_satelite")? jsonObj.getInt("pentavolent_one_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">২ (শিশুর বয়স ১০ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("pentavolent_two_given_center")? jsonObj.getInt("pentavolent_two_given_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("pentavolent_two_given_satelite")? jsonObj.getInt("pentavolent_two_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("pentavolent_two_given_center")? jsonObj.getInt("pentavolent_two_given_center"):0) + (jsonObj.has("pentavolent_two_given_satelite")? jsonObj.getInt("pentavolent_two_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">৩ (শিশুর বয়স ১৪ সপ্তাহ হলে) </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("pentavolent_three_given_center")? jsonObj.getInt("pentavolent_three_given_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("pentavolent_three_given_satelite")? jsonObj.getInt("pentavolent_three_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("pentavolent_three_given_center")? jsonObj.getInt("pentavolent_three_given_center"):0) + (jsonObj.has("pentavolent_three_given_satelite")? jsonObj.getInt("pentavolent_three_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="3" style="border-top: 1px solid black;" class="danger"> <div align="center"> পিসিভি টিকা </div></td>
                <td style="border-top: 1px solid black;">১ (শিশুর বয়স ৬ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("pcv_one_given_center")? jsonObj.getInt("pcv_one_given_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("pcv_one_given_satelite")? jsonObj.getInt("pcv_one_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("pcv_one_given_center")? jsonObj.getInt("pcv_one_given_center"):0) + (jsonObj.has("pcv_one_given_satelite")? jsonObj.getInt("pcv_one_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">২ (শিশুর বয়স ১০ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("pcv_two_given_center")? jsonObj.getInt("pcv_two_given_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("pcv_two_given_satelite")? jsonObj.getInt("pcv_two_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("pcv_two_given_center")? jsonObj.getInt("pcv_two_given_center"):0) + (jsonObj.has("pcv_two_given_satelite")? jsonObj.getInt("pcv_two_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">৩ (শিশুর বয়স ১৪ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("pcv_three_given_center")? jsonObj.getInt("pcv_three_given_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("pcv_three_given_satelite")? jsonObj.getInt("pcv_three_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("pcv_three_given_center")? jsonObj.getInt("pcv_three_given_center"):0) + (jsonObj.has("pcv_three_given_satelite")? jsonObj.getInt("pcv_three_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="3" style="border-top: 1px solid black;" class="danger"><div align="center">ওপিভি </div></td>
                <td style="border-top: 1px solid black;" class="success">১ (শিশুর বয়স ৬ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("opv_one_given_center")? jsonObj.getInt("opv_one_given_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("opv_one_given_satelite")? jsonObj.getInt("opv_one_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("opv_one_given_center")? jsonObj.getInt("opv_one_given_center"):0) + (jsonObj.has("opv_one_given_satelite")? jsonObj.getInt("opv_one_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">২ (শিশুর বয়স ১০ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("opv_two_given_center")? jsonObj.getInt("opv_two_given_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("opv_two_given_satelite")? jsonObj.getInt("opv_two_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("opv_two_given_center")? jsonObj.getInt("opv_two_given_center"):0) + (jsonObj.has("opv_two_given_satelite")? jsonObj.getInt("opv_two_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">৩ (শিশুর বয়স ১৪ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("opv_three_given_center")? jsonObj.getInt("opv_three_given_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("opv_three_given_satelite")? jsonObj.getInt("opv_three_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("opv_three_given_center")? jsonObj.getInt("opv_three_given_center"):0) + (jsonObj.has("opv_three_given_satelite")? jsonObj.getInt("opv_three_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="2" style="border-top: 1px solid black;" class="danger"><div align="center">আইপিভি (ফ্রাকশনাল)</div></td>
                <td style="border-top: 1px solid black;">১ (শিশুর বয়স ৬ সপ্তাহ হলে) </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("ipv_one_given_center")? jsonObj.getInt("ipv_one_given_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("ipv_one_given_satelite")? jsonObj.getInt("ipv_one_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("ipv_one_given_center")? jsonObj.getInt("ipv_one_given_center"):0) + (jsonObj.has("ipv_one_given_satelite")? jsonObj.getInt("ipv_one_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">২ (শিশুর বয়স ১৪ সপ্তাহ হলে)</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("ipv_one_given_center")? jsonObj.getInt("ipv_one_given_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("ipv_one_given_satelite")? jsonObj.getInt("ipv_one_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("ipv_one_given_center")? jsonObj.getInt("ipv_one_given_center"):0) + (jsonObj.has("ipv_one_given_satelite")? jsonObj.getInt("ipv_one_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="2" style="border-top: 1px solid black;" class="danger"><div align="center">এম আর টিকা </div></td>
                <td style="border-top: 1px solid black;">১ (শিশুর বয়স ৯ মাস পুরন হলে)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("mr_one_given_center")? jsonObj.getInt("mr_one_given_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("mr_one_given_satelite")? jsonObj.getInt("mr_one_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("mr_one_given_center")? jsonObj.getInt("mr_one_given_center"):0) + (jsonObj.has("mr_one_given_satelite")? jsonObj.getInt("mr_one_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">১ (শিশুর বয়স ১৫ মাস পুরন হলে)  </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("mr_two_given_center")? jsonObj.getInt("mr_two_given_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("mr_two_given_satelite")? jsonObj.getInt("mr_two_given_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("mr_two_given_center")? jsonObj.getInt("mr_two_given_center"):0) + (jsonObj.has("mr_two_given_satelite")? jsonObj.getInt("mr_two_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td class="danger" rowspan="9" style="text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">  জন্ম-মৄতু্ </span></td>
                <td colspan="3" style="border-top: 1px solid black;">জীবিত জন্ম ( Live Birth)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("livebirth")? jsonObj.getInt("livebirth"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("live_birth_satelite")? jsonObj.getInt("live_birth_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("livebirth")? jsonObj.getInt("livebirth"):0) + (jsonObj.has("live_birth_satelite")? jsonObj.getInt("live_birth_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;" class="success">কম জন্ম ওজনে ( জন্ম ওজন ২৫০০ গ্রামের কম) জন্ম গ্রহনকারী নবজাতক এর সংখ্যা </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("less_weight_2500_child_center")? jsonObj.getInt("less_weight_2500_child_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("less_weight_2500_child_satelite")? jsonObj.getInt("less_weight_2500_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("less_weight_2500_child_center")? jsonObj.getInt("less_weight_2500_child_center"):0) + (jsonObj.has("less_weight_2500_child_satelite")? jsonObj.getInt("less_weight_2500_child_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">কম জন্ম ওজনে ( জন্ম ওজন ২০০০ গ্রামের কম) জন্ম গ্রহনকারী নবজাতক এর সংখ্যা </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("less_weight_2000_child_center")? jsonObj.getInt("less_weight_2000_child_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("less_weight_2000_child_satelite")? jsonObj.getInt("less_weight_2000_child_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("less_weight_2000_child_center")? jsonObj.getInt("less_weight_2000_child_center"):0) + (jsonObj.has("less_weight_2000_child_satelite")? jsonObj.getInt("less_weight_2000_child_satelite"):0) %></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">অপরিনত ( ৩৭ সপ্তাহের পূর্বে জন্ম)  নবজাতক এর সংখ্যা </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("immaturebirth_center")? jsonObj.getInt("immaturebirth_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("immaturebirth_satelite")? jsonObj.getInt("immaturebirth_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("immaturebirth_center")? jsonObj.getInt("immaturebirth_center"):0) + (jsonObj.has("immaturebirth_satelite")? jsonObj.getInt("immaturebirth_satelite"):0) %></td>
            </tr>
            <tr>
                <td rowspan="3" style="text-align: center;vertical-align: middle;border-top: 1px solid black;" class="danger">মৃত - জন্ম  </td>
                <td colspan="2" style="border-top: 1px solid black;" class="success">ফ্রেশ </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("stillbirth_fresh_center")? jsonObj.getInt("stillbirth_fresh_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("stillbirth_fresh_satelite")? jsonObj.getInt("stillbirth_fresh_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("stillbirth_fresh_center")? jsonObj.getInt("stillbirth_fresh_center"):0) + (jsonObj.has("stillbirth_fresh_satelite")? jsonObj.getInt("stillbirth_fresh_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">ম্যাসারেটেড </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("stillbirth_macerated_center")? jsonObj.getInt("stillbirth_macerated_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("stillbirth_macerated_satelite")? jsonObj.getInt("stillbirth_macerated_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("stillbirth_macerated_center")? jsonObj.getInt("stillbirth_macerated_center"):0) + (jsonObj.has("stillbirth_macerated_satelite")? jsonObj.getInt("stillbirth_macerated_satelite"):0) %></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;">মোট মৃত জন্মের সংখ্যা  </td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("stillbirth_fresh_center")? jsonObj.getInt("stillbirth_fresh_center"):0) + (jsonObj.has("stillbirth_macerated_center")? jsonObj.getInt("stillbirth_macerated_center"):0) %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("stillbirth_fresh_satelite")? jsonObj.getInt("stillbirth_fresh_satelite"):0) + (jsonObj.has("stillbirth_macerated_satelite")? jsonObj.getInt("stillbirth_macerated_satelite"):0) %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("stillbirth_fresh_center")? jsonObj.getInt("stillbirth_fresh_center"):0) + (jsonObj.has("stillbirth_fresh_satelite")? jsonObj.getInt("stillbirth_fresh_satelite"):0) + (jsonObj.has("stillbirth_macerated_center")? jsonObj.getInt("stillbirth_macerated_center"):0) + (jsonObj.has("stillbirth_macerated_satelite")? jsonObj.getInt("stillbirth_macerated_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;" class="success">নব জাতকের মৃত্যু (০ - ২৮ দিন )</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("newborn_death")? jsonObj.getInt("newborn_death"):0 %></td>
                <td style="border-top: 1px solid black;" class="success">0</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("newborn_death")? jsonObj.getInt("newborn_death"):0 %></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;" >মাতৃমৃত্যু্র সংখ্যা</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("maternal_death")? jsonObj.getInt("maternal_death"):0 %></td>
                <td style="border-top: 1px solid black;">0</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("maternal_death")? jsonObj.getInt("maternal_death"):0 %></td>
            </tr>
            <tr>
                <td colspan="2" rowspan="6" style="text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;" class="danger"><div align="center">
				 বহির্বিভাগে সেবা গ্রহনকারী রোগী </div></td>
                <td colspan="2" style="border-top: 1px solid black;" class="success">পুরুষ</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("gp_male_center")? jsonObj.getInt("gp_male_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("gp_male_satelite")? jsonObj.getInt("gp_male_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("gp_male_center")? jsonObj.getInt("gp_male_center"):0) + (jsonObj.has("gp_male_satelite")? jsonObj.getInt("gp_male_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">মহিলা </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("gp_female_center")? jsonObj.getInt("gp_female_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("gp_female_satelite")? jsonObj.getInt("gp_female_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("gp_female_center")? jsonObj.getInt("gp_female_center"):0) + (jsonObj.has("gp_female_satelite")? jsonObj.getInt("gp_female_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">শিশু (০ - ১ বছর)</td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("gp_outdoor_child_0_1_center ")? jsonObj.getInt("gp_outdoor_child_0_1_center "):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("gp_outdoor_child_0_1_satelite ")? jsonObj.getInt("gp_outdoor_child_0_1_satelite "):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("gp_outdoor_child_0_1_center ")? jsonObj.getInt("gp_outdoor_child_0_1_center "):0) + (jsonObj.has("gp_outdoor_child_0_1_satelite ")? jsonObj.getInt("gp_outdoor_child_0_1_satelite "):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">শিশু (১ - ৫ বছর)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("gp_outdoor_child_1_5_center")? jsonObj.getInt("gp_outdoor_child_1_5_center"):0 %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("gp_outdoor_child_1_5_satelite")? jsonObj.getInt("gp_outdoor_child_1_5_satelite"):0 %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("gp_outdoor_child_1_5_center")? jsonObj.getInt("gp_outdoor_child_1_5_center"):0) + (jsonObj.has("gp_outdoor_child_1_5_satelite")? jsonObj.getInt("gp_outdoor_child_1_5_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">অন্যান্য </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("external_other_patient_center")? jsonObj.getInt("external_other_patient_center"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("external_other_patient_satelite")? jsonObj.getInt("external_other_patient_satelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("external_other_patient_center")? jsonObj.getInt("external_other_patient_center"):0) + (jsonObj.has("external_other_patient_satelite")? jsonObj.getInt("external_other_patient_satelite"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">মোট </td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("gp_male_center")? jsonObj.getInt("gp_male_center"):0) + (jsonObj.has("gp_female_center")? jsonObj.getInt("gp_female_center"):0) + (jsonObj.has("gp_outdoor_child_0_1_center ")? jsonObj.getInt("gp_outdoor_child_0_1_center "):0) + (jsonObj.has("gp_outdoor_child_1_5_center")? jsonObj.getInt("gp_outdoor_child_1_5_center"):0) %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("gp_male_satelite")? jsonObj.getInt("gp_male_satelite"):0) + (jsonObj.has("gp_female_satelite")? jsonObj.getInt("gp_female_satelite"):0) + (jsonObj.has("gp_outdoor_child_0_1_satelite ")? jsonObj.getInt("gp_outdoor_child_0_1_satelite "):0) + (jsonObj.has("gp_outdoor_child_1_5_satelite")? jsonObj.getInt("gp_outdoor_child_1_5_satelite"):0) %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("gp_male_center")? jsonObj.getInt("gp_male_center"):0) + (jsonObj.has("gp_male_satelite")? jsonObj.getInt("gp_male_satelite"):0) + (jsonObj.has("gp_female_center")? jsonObj.getInt("gp_female_center"):0) + (jsonObj.has("gp_female_satelite")? jsonObj.getInt("gp_female_satelite"):0) + (jsonObj.has("gp_outdoor_child_0_1_center ")? jsonObj.getInt("gp_outdoor_child_0_1_center "):0) + (jsonObj.has("gp_outdoor_child_0_1_satelite ")? jsonObj.getInt("gp_outdoor_child_0_1_satelite "):0) + (jsonObj.has("gp_outdoor_child_1_5_center")? jsonObj.getInt("gp_outdoor_child_1_5_center"):0) + (jsonObj.has("gp_outdoor_child_1_5_satelite")? jsonObj.getInt("gp_outdoor_child_1_5_satelite"):0) %></td>
            </tr>
            <tr>
                <td class="danger" colspan="2" rowspan="4" style="text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;"><div align="center">অন্তর্বিভাগে ভর্তি রোগী </div></td>
                <td colspan="2" style="border-top: 1px solid black;" class="success"> মহিলা </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("internal_female_patient_center")? jsonObj.getInt("internal_female_patient_center"):0 %></td>
                <td rowspan="6" style="border-top: 1px solid black;; background-color:#999999" class="success"></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("internal_female_patient_center")? jsonObj.getInt("internal_female_patient_center"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">শিশু (০ - ১ বছর)</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("internal_oneyear_child_patient_center")? jsonObj.getInt("internal_oneyear_child_patient_center"):0 %></td>
                <%--<td style="border-top: 1px solid black;"><%= jsonObj.has("internal_oneyear_child_patient_satelite")? jsonObj.getInt("internal_oneyear_child_patient_satelite"):0 %></td>--%>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("internal_oneyear_child_patient_center")? jsonObj.getInt("internal_oneyear_child_patient_center"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;" class="success">শিশু (১ - ৫ বছর) </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("internal_onetofive_child_patient_center")? jsonObj.getInt("internal_onetofive_child_patient_center"):0 %></td>
                <%--<td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("internal_onetofive_child_patient_satelite")? jsonObj.getInt("internal_onetofive_child_patient_satelite"):0 %></td>--%>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("internal_onetofive_child_patient_center")? jsonObj.getInt("internal_onetofive_child_patient_center"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" style="border-top: 1px solid black;">মোট </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("internal_total_patient_center")? jsonObj.getInt("internal_total_patient_center"):0 %></td>
                <%--<td style="border-top: 1px solid black;"><%= jsonObj.has("internal_total_patient_satelite")? jsonObj.getInt("internal_total_patient_satelite"):0 %></td>--%>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("internal_total_patient_center")? jsonObj.getInt("internal_total_patient_center"):0) %></td>
            </tr>
            <tr>
                <td height="23" colspan="4" style="border-top: 1px solid black;" class="success">কেন্দ্রে কয়টি স্বাস্থ্য শিক্ষা (বিসিসি) কার্যক্রম সম্পন্ন করা হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("bcc_done_center")? jsonObj.getInt("bcc_done_center"):0 %></td>
                <%--<td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("bcc_done_satelite")? jsonObj.getInt("bcc_done_satelite"):0 %></td>--%>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("bcc_done_center")? jsonObj.getInt("bcc_done_center"):0) %></td>
            </tr>
            <tr>
                <td colspan="4" style="border-top: 1px solid black;">এসএসিএমও (SACMO) কর্তৃক কয়টি স্কুলে স্বাস্থ্য শিক্ষা (বিসিসি) কার্যক্রম সম্পাদন করা হয়েছে</td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("sacmo_done_center")? jsonObj.getInt("sacmo_done_center"):0 %></td>
                <%--<td style="border-top: 1px solid black; background-color:#999999">&nbsp;</td>--%>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("sacmo_done_center")? jsonObj.getInt("sacmo_done_center"):0 %></td>
            </tr>
        </table>
	
    </div>

    <div class="printer">

        <table class="table page_4 table_hide" id="table_8" width="745" cellspacing="0" style='font-size:10px;display:none;'>
            <tr>
                <td width="86">&nbsp;</td>
                <td width="345" style='text-align: center;'>&nbsp;</td>
                <td width="306" style='text-align: right;'>এমআইএস ফরম-৩<br>
                    পৃষ্ঠা-৪</td>
            <tr></table>

        <table max-width="730px" border="1" cellspacing="0" cellpadding="0" class="table page_4 table_hide" id="table_9" style="font-size:11px; border:solid; border-width:1px; border-color:#000000;display:none;">
            <tr class="danger">
                <td height="41" colspan="2" style="border-top: 1px solid black;" ><div align="center">সেবার ধরণ</div></td>
                <td width="11%" style="border-top: 1px solid black;"><div align="center">ক্লিনিক / হাসপাতাল</div></td>
                <td width="11%" style="border-top: 1px solid black;"><div align="center">স্যাটেলাইট ক্লিনিক</div></td>
                <td width="10%" style="border-top: 1px solid black;"><div align="center">মোট</div></td>
            </tr>
            <tr>
                <td class="danger" width="11%" rowspan="15" style="text-align: center;vertical-align: middle;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">পুষ্টি সেবা </span></td>
                <td width="57%" style="border-top: 1px solid black;">আই এফ এ, হাত ধোয়া , মায়ের পুষ্টি বিষয়ক কাউন্সেলিং </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("nutrition_counseling_center")? jsonObj.getInt("nutrition_counseling_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("nutrition_counseling_satelite")? jsonObj.getInt("nutrition_counseling_satelite"):"0" %>
                </td>
                <td style="border-top: 1px solid black;">
                    <%= (jsonObj.has("nutrition_counseling_center")? jsonObj.getInt("nutrition_counseling_center"):0) + (jsonObj.has("nutrition_counseling_satelite")? jsonObj.getInt("nutrition_counseling_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">কতজন মায়ের (গর্ভবতি মহিলা) ওজন নেয়া হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("ancmotherweightcenter")? jsonObj.getInt("ancmotherweightcenter"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("ancmotherweightsatelite")? jsonObj.getInt("ancmotherweightsatelite"):0 %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("ancmotherweightcenter")? jsonObj.getInt("ancmotherweightcenter"):0) + (jsonObj.has("ancmotherweightsatelite")? jsonObj.getInt("ancmotherweightsatelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">আইওয়াইসি এফ , ভিটামিন - এ বিষয়ক মায়ের কাউন্সেলিং </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("iycf_counseling_center")? jsonObj.getInt("iycf_counseling_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("iycf_counseling_satelite")? jsonObj.getInt("iycf_counseling_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("iycf_counseling_center")? jsonObj.getInt("iycf_counseling_center"):0) + (jsonObj.has("iycf_counseling_satelite")? jsonObj.getInt("iycf_counseling_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">কতজন গর্ভবতি মাকে আয়রন ফলিক এসিড এবং ক্যালসিয়াম বড়ি দেওয়া হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= jsonObj.has("ancprgwomanayronfoliccenter")? jsonObj.getInt("ancprgwomanayronfoliccenter"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= jsonObj.has("ancprgwomanayronfolicsatelite")? jsonObj.getInt("ancprgwomanayronfolicsatelite"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= (jsonObj.has("ancprgwomanayronfoliccenter")? jsonObj.getInt("ancprgwomanayronfoliccenter"):0) + (jsonObj.has("ancprgwomanayronfolicsatelite")? jsonObj.getInt("ancprgwomanayronfolicsatelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">কতজন ০ - ২৩ মাস বয়সী শিশুর মাকে আয়রন ফলিক এসিড এবং ক্যালসিয়াম বড়ি দেওয়া হয়েছে </td>
                <td style="border-top: 1px solid black;">
                    <%= jsonObj.has("0-23_month_monther_ifa_given_center")? jsonObj.getInt("0-23_month_monther_ifa_given_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("0-23_month_monther_ifa_given_satelite")? jsonObj.getInt("0-23_month_monther_ifa_given_satelite"):"0" %>
                </td>
                <td style="border-top: 1px solid black;">
                    <%= (jsonObj.has("0-23_month_monther_ifa_given_center")? jsonObj.getInt("0-23_month_monther_ifa_given_center"):0) + (jsonObj.has("0-23_month_monther_ifa_given_satelite")? jsonObj.getInt("0-23_month_monther_ifa_given_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">কতজন ৬-২৩ মাস বয়সী শিশুকে মাল্টিপল মাইক্রোনিউট্রিয়েন্ট পাউডার (এমএনপি) দেয়া হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("6-23_month_child_mnp_given_center")? jsonObj.getInt("6-23_month_child_mnp_given_center"):"0" %>
                </td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= jsonObj.has("6-23_month_child_mnp_given_satelite")? jsonObj.getInt("6-23_month_child_mnp_given_satelite"):"0" %>
                </td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= (jsonObj.has("6-23_month_child_mnp_given_center")? jsonObj.getInt("6-23_month_child_mnp_given_center"):0) + (jsonObj.has("6-23_month_child_mnp_given_satelite")? jsonObj.getInt("6-23_month_child_mnp_given_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">কতজন ৬-৫৯ মাস বয়সী শিশুকে ভিটামিন - এ দেয়া হয়েছে </td>
                <td style="border-top: 1px solid black;">
                    <%= jsonObj.has("6-59_month_child_vitamin-a_given_center")? jsonObj.getInt("6-59_month_child_vitamin-a_given_center"):"0" %></td>
                <td style="border-top: 1px solid black;">
                    <%= jsonObj.has("6-59_month_child_vitamin-a_given_satelite")? jsonObj.getInt("6-59_month_child_vitamin-a_given_satelite"):"0" %>
                </td>
                <td style="border-top: 1px solid black;">
                    <%= (jsonObj.has("6-59_month_child_vitamin-a_given_center")? jsonObj.getInt("6-59_month_child_vitamin-a_given_center"):0) + (jsonObj.has("6-59_month_child_vitamin-a_given_satelite")? jsonObj.getInt("6-59_month_child_vitamin-a_given_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">কতজন ২৪-৫৯ মাস বয়সী শিশুকে কৃমি নাশক বড়ি দেয়া হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= jsonObj.has("24-59_month_child_wormpills_given_center")? jsonObj.getInt("24-59_month_child_wormpills_given_center"):"0" %>
                </td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= jsonObj.has("24-59_month_child_wormpills_given_satelite")? jsonObj.getInt("24-59_month_child_wormpills_given_satelite"):"0" %>
                </td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= (jsonObj.has("24-59_month_child_wormpills_given_center")? jsonObj.getInt("24-59_month_child_wormpills_given_center"):0) + (jsonObj.has("24-59_month_child_wormpills_given_satelite")? jsonObj.getInt("24-59_month_child_wormpills_given_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;"> কতজন ৬-৫৯ মাস বয়সী ডায়রিয়া আক্রান্ত শিশুকে খাবার স্যলাইনের সাথে জিংক বড়ি দেয়া হয়েছে </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("6-59_child_saline_given_center")? jsonObj.getInt("6-59_child_saline_given_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("6-59_child_saline_given_satelite")? jsonObj.getInt("6-59_child_saline_given_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;">
                    <%= (jsonObj.has("6-59_child_saline_given_center")? jsonObj.getInt("6-59_child_saline_given_center"):0) + (jsonObj.has("6-59_child_saline_given_satelite")? jsonObj.getInt("6-59_child_saline_given_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">কতজন ৬-৫৯ মাস বয়সী শিশুকে গ্রোথ মনিটরিং (GMP *) করা হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("6-59_gmp_center")? jsonObj.getInt("6-59_gmp_center"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("6-59_gmp__satelite")? jsonObj.getInt("6-59_gmp__satelite"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("6-59_gmp_center")? jsonObj.getInt("6-59_gmp_center"):0) + (jsonObj.has("6-59_gmp__satelite")? jsonObj.getInt("6-59_gmp__satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;"> কতজন ৬-৫৯ মাস বয়সী MAM (মাঝারি তিব্র অপুষ্টি) আক্রান্ত শিশুকে সনাক্ত এবং চিকিৎসা প্রদান করা হয়েছে </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("mam_center")? jsonObj.getInt("mam_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("mam_satelite")? jsonObj.getInt("mam_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("mam_center")? jsonObj.getInt("mam_center"):0) + (jsonObj.has("mam_satelite")? jsonObj.getInt("mam_satelite"):0) %></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success">কতজন ০-৫৯ মাস বয়সী SAM( মারাত্মক তীব্র অপুষ্টি )আক্রান্ত শিশুকে সনাক্ত করা হয়েছে এবং রেফার করা হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("sam_center")? jsonObj.getInt("sam_center"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("sam_satelite")? jsonObj.getInt("sam_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success">
                    <%= (jsonObj.has("sam_center")? jsonObj.getInt("sam_center"):0) + (jsonObj.has("sam_satelite")? jsonObj.getInt("sam_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">কতজন ০-৫৯ মাস বয়সী শিশুর Stunting (বয়সের তুলনায় কম উচ্চতা সম্পন্ন ) সনাক্ত করা হয়েছে </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("stunting_center")? jsonObj.getInt("stunting_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("stunting_satelite")? jsonObj.getInt("stunting_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;">
                    <%= (jsonObj.has("stunting_center")? jsonObj.getInt("stunting_center"):0) + (jsonObj.has("stunting_satelite")? jsonObj.getInt("stunting_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;" class="success"> কতজন ০-৫৯ মাস বয়সী শিশুর  wasting( উচ্চতার তুলনায় কম ওজন সম্পন্ন )সনাক্ত করা হয়েছে </td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("wasting_center")? jsonObj.getInt("wasting_center"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success"><%= jsonObj.has("wasting_satelite")? jsonObj.getInt("wasting_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;" class="success"><%= (jsonObj.has("wasting_center")? jsonObj.getInt("wasting_center"):0) + (jsonObj.has("wasting_satelite")? jsonObj.getInt("wasting_satelite"):0) %>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">কতজন ০-৫৯ মাস বয়সী শিশুর Underweight (বয়সের তুলনায় কম ওজন সম্পন্ন) সনাক্ত করা হয়েছে </td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("under_weight_center")? jsonObj.getInt("under_weight_center"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= jsonObj.has("under_weight_satelite")? jsonObj.getInt("under_weight_satelite"):"0" %></td>
                <td style="border-top: 1px solid black;"><%= (jsonObj.has("under_weight_center")? jsonObj.getInt("under_weight_center"):0) + (jsonObj.has("under_weight_satelite")? jsonObj.getInt("under_weight_satelite"):0) %>
                </td>
            </tr>
        </table>
		<p>&nbsp;</p>
    </div>
    <div class="table-responsive">

        <table style="font-size:11px;display:none;" class="table page_4 table_hide" id="table_10">
          <!--  <tr class='danger'>
                <td height="30" > &nbsp;অসুস্থ শিশুর সমন্নিত চিকৎসা ব্যবস্থাপনা (IMCI) : </td>
            </tr>-->
			
        </table>
        <table  height="717" cellpadding="0" cellspacing="0" class="table page_4 table_hide" id="table_11" style="font-size:11px; border:solid; border-width:1px;display:none;">
			<tr>
			<td colspan="13" height="30" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">&nbsp;অসুস্থ শিশুর সমন্নিত চিকৎসা ব্যবস্থাপনা (IMCI) : </div></td>
			</tr>
            <tr class='danger' rowspan="3" >
                <td rowspan="3" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ক্রমিক নং</span></td>
                <td height="25" colspan="12" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center"><span style="border-right: 1px solid black; height:19px;">শিশু (০-৫৯ মাস) সেবা </span></div></td>
            </tr>

            <tr class='danger' rowspan="3" >
                <td width="216" rowspan="2" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">রোগের নাম</div></td>
                <td height="25" colspan="2" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">০ - ২৮ দিন </div></td>
                <td colspan="2" valign="middle" style="border-top: 1px solid black;border-right: 1px solid black;height:19px;"><div align="center">২৯ - ৫৯ দিন </div></td>
                <td colspan="2" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">২ মাস &lt; ১ বছর </div></td>
                <td colspan="2" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১- ৫ বছর </div></td>
                <td colspan="2" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">মোট </div></td>
                <td width="56" rowspan="2" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">সর্ব মোট </div></td>
            </tr>
            <tr class='danger' rowspan="3" >
                <td width="38" height="25" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">ছেলে </div></td>
                <td width="35" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">মেয়ে </div></td>
                <td width="43" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">ছেলে </div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">মেয়ে </div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">ছেলে </div></td>
                <td width="44" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">মেয়ে </div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">ছেলে </div></td>
                <td width="42" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">মেয়ে </div></td>
                <td width="43" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">ছেলে </div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">মেয়ে </div></td>
            </tr>

            <tr class='danger' rowspan="3" >
                <td height="25" width="25" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১</div></td>
                <td width="216" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(২)</div></td>
                <td width="38" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(৩)</div></td>
                <td width="35" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(৪)</div></td>
                <td width="43" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(৫)</div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(৬)</div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(৭)</div></td>
                <td width="44" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(৮)</div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(৯)</div></td>
                <td width="42" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(১০)</div></td>
                <td width="43" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(১১)</div></td>
                <td width="41" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(১২)</div></td>
                <td width="56" valign="middle" style='border-top: 1px solid black;border-right: 1px solid black;'><div align="center">(১৩)</div></td>
            </tr>

            <%--Very Serious Desease Addition --%>
            <%
                Integer very_male_total = (jsonObj.has("very_serious_disease_0-28days_male")? jsonObj.getInt("very_serious_disease_0-28days_male"):0)
                        +(jsonObj.has("very_serious_disease_29-59days_male")? jsonObj.getInt("very_serious_disease_29-59days_male"):0)
                        +(jsonObj.has("very_serious_disease_2month-1year_male")? jsonObj.getInt("very_serious_disease_2month-1year_male"):0)
                        +(jsonObj.has("very_serious_disease_1-5year_male")? jsonObj.getInt("very_serious_disease_1-5year_male"):0);

                Integer very_female_total = (jsonObj.has("very_serious_disease_0-28days_female")? jsonObj.getInt("very_serious_disease_0-28days_female"):0)
                        +(jsonObj.has("very_serious_disease_29-59days_female")? jsonObj.getInt("very_serious_disease_29-59days_female"):0)
                        +(jsonObj.has("very_serious_disease_2month-1year_female")? jsonObj.getInt("very_serious_disease_2month-1year_female"):0)
                        +(jsonObj.has("very_serious_disease_1-5year_female")? jsonObj.getInt("very_serious_disease_1-5year_female"):0);

            %>

            <tr rowspan="3" >
                <td height="31" width="25" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১</div></td>
                <td width="216" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>খুব মারাত্মক রোগ - সংকাটপন্ন অসুস্থতা </span></td>
                <td width="38" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_0-28days_male")? jsonObj.getInt("very_serious_disease_0-28days_male"):"0" %></td>
                <td width="35" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_0-28days_female")? jsonObj.getInt("very_serious_disease_0-28days_female"):"0" %></td>
                <td width="43" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_29-59days_male")? jsonObj.getInt("very_serious_disease_29-59days_male"):"0" %></td>
                <td width="41" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_29-59days_female")? jsonObj.getInt("very_serious_disease_29-59days_female"):"0" %></td>
                <td width="41" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_2month-1year_male")? jsonObj.getInt("very_serious_disease_2month-1year_male"):"0" %></td>
                <td width="44" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_2month-1year_female")? jsonObj.getInt("very_serious_disease_2month-1year_female"):"0" %></td>
                <td width="41" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_1-5year_male")? jsonObj.getInt("very_serious_disease_1-5year_male"):"0" %></td>
                <td width="42" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_1-5year_female")? jsonObj.getInt("very_serious_disease_1-5year_female"):"0" %></td>
                <td width="43" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_male_total %></td>
                <td width="41" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_female_total %></td>
                <td width="56" valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_male_total + very_female_total %></td>
            </tr>

            <%--Very Serious Desease Addition --%>
            <%
                Integer very_infected_male_total = (jsonObj.has("very_serious_disease_infected_0-28days_male")? jsonObj.getInt("very_serious_disease_infected_0-28days_male"):0)
                        +(jsonObj.has("very_serious_disease_infected_29-59days_male")? jsonObj.getInt("very_serious_disease_infected_29-59days_male"):0)
                        +(jsonObj.has("very_serious_disease_infected_2month-1year_male")? jsonObj.getInt("very_serious_disease_infected_2month-1year_male"):0)
                        +(jsonObj.has("very_serious_disease_infected_1-5year_male")? jsonObj.getInt("very_serious_disease_infected_1-5year_male"):0);

                Integer very_infected_female_total = (jsonObj.has("very_serious_disease_infected_0-28days_female")? jsonObj.getInt("very_serious_disease_infected_0-28days_female"):0)
                        +(jsonObj.has("very_serious_disease_infected_29-59days_female")? jsonObj.getInt("very_serious_disease_infected_29-59days_female"):0)
                        +(jsonObj.has("very_serious_disease_infected_29-1year_female")? jsonObj.getInt("very_serious_disease_infected_29-1year_female"):0)
                        +(jsonObj.has("very_serious_disease_infected_1-5year_female")? jsonObj.getInt("very_serious_disease_infected_1-5year_female"):0);

            %>

            <tr rowspan="3" class="success">
                <td height="26" width="25" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">২</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>খুব মারাত্মক রোগ - সম্ভাব্য মারাত্মক সংক্রমন </span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_0-28days_male")? jsonObj.getInt("very_serious_disease_infected_0-28days_male"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_0-28days_female")? jsonObj.getInt("very_serious_disease_infected_0-28days_female"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_29-59days_male")? jsonObj.getInt("very_serious_disease_infected_29-59days_male"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_29-59days_female")? jsonObj.getInt("very_serious_disease_infected_29-59days_female"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_2month-1year_male")? jsonObj.getInt("very_serious_disease_infected_2month-1year_male"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_2month-1year_female")? jsonObj.getInt("very_serious_disease_infected_2month-1year_female"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_1-5year_male")? jsonObj.getInt("very_serious_disease_infected_1-5year_male"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_infected_1-5year_female")? jsonObj.getInt("very_serious_disease_infected_1-5year_female"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_infected_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_infected_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_infected_male_total + very_infected_female_total %></td>
            </tr>

            <%--Very Serious Neumonia Addition --%>
            <%
                Integer neumonia_male_total = (jsonObj.has("very_serious_disease_neumonia_0-28days_male")? jsonObj.getInt("very_serious_disease_neumonia_0-28days_male"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_29-59days_male")? jsonObj.getInt("very_serious_disease_neumonia_29-59days_male"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_2month-1year_male")? jsonObj.getInt("very_serious_disease_neumonia_2month-1year_male"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_1-5year_male")? jsonObj.getInt("very_serious_disease_neumonia_1-5year_male"):0);

                Integer neumonia_female_total = (jsonObj.has("very_serious_disease_neumonia_0-28days_female")? jsonObj.getInt("very_serious_disease_neumonia_0-28days_female"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_29-59days_female")? jsonObj.getInt("very_serious_disease_neumonia_29-59days_female"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_2month-1year_female")? jsonObj.getInt("very_serious_disease_neumonia_2month-1year_female"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_1-5year_female")? jsonObj.getInt("very_serious_disease_neumonia_1-5year_female"):0);

            %>

            <tr class="success" rowspan="3" >
                <td height="28" width="25" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'>
				<div align="center">৩</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>খুব মারাত্মক রোগ - দ্রুত শ্বাস নিউমোনিয়া </span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_neumonia_0-28days_male")? jsonObj.getInt("very_serious_disease_neumonia_0-28days_male"):"0" %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_neumonia_0-28days_female")? jsonObj.getInt("very_serious_disease_neumonia_0-28days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_neumonia_29-59days_male")? jsonObj.getInt("very_serious_disease_neumonia_29-59days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("very_serious_disease_neumonia_29-59days_female")? jsonObj.getInt("very_serious_disease_neumonia_29-59days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'>
				<span style="display:none;"><%= jsonObj.has("very_serious_disease_neumonia_2month-1year_male")? jsonObj.getInt("very_serious_disease_neumonia_2month-1year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("very_serious_disease_neumonia_2month-1year_female")? jsonObj.getInt("very_serious_disease_neumonia_2month-1year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("very_serious_disease_neumonia_1-5year_male")? jsonObj.getInt("very_serious_disease_neumonia_1-5year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'>
				<span style="display:none;"><%= jsonObj.has("very_serious_disease_neumonia_1-5year_female")? jsonObj.getInt("very_serious_disease_neumonia_1-5year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= neumonia_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= neumonia_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= neumonia_female_total + neumonia_male_total %></td>
            </tr>

            <%--Light Infection Addition --%>
            <%
                Integer infection_male_total = (jsonObj.has("light_infection_0-28days_male")? jsonObj.getInt("light_infection_0-28days_male"):0)
                        +(jsonObj.has("light_infection_29-59days_male")? jsonObj.getInt("light_infection_29-59days_male"):0)
                        +(jsonObj.has("light_infection_2month-1year_male")? jsonObj.getInt("light_infection_2month-1year_male"):0)
                        +(jsonObj.has("light_infection_1-5year_male")? jsonObj.getInt("light_infection_1-5year_male"):0);

                Integer infection_female_total = (jsonObj.has("light_infection_0-28days_female")? jsonObj.getInt("light_infection_0-28days_female"):0)
                        +(jsonObj.has("light_infection_29-59days_female")? jsonObj.getInt("light_infection_29-59days_female"):0)
                        +(jsonObj.has("light_infection_2month-1year_female")? jsonObj.getInt("light_infection_2month-1year_female"):0)
                        +(jsonObj.has("light_infection_1-5year_female")? jsonObj.getInt("light_infection_1-5year_female"):0);

            %>



            <tr rowspan="3" >
                <td height="28" width="25" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">৪</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>স্থানীয় সীমিত সংক্রমন </span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("light_infection_0-28days_male")? jsonObj.getInt("light_infection_0-28days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("light_infection_0-28days_female")? jsonObj.getInt("light_infection_0-28days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("light_infection_29-59days_male")? jsonObj.getInt("light_infection_29-59days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("light_infection_29-59days_female")? jsonObj.getInt("light_infection_29-59days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("light_infection_2month-1year_male")? jsonObj.getInt("light_infection_2month-1year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("light_infection_2month-1year_female")? jsonObj.getInt("light_infection_2month-1year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("light_infection_1-5year_male")? jsonObj.getInt("light_infection_1-5year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("light_infection_1-5year_female")? jsonObj.getInt("light_infection_1-5year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= infection_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= infection_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= infection_male_total + infection_female_total %></td>
            </tr>

            <%--Refer Accepted Addition --%>
            <%
                /*Integer refer_accept_male_total = (jsonObj.has("refer_accepted_0-28days_male")? jsonObj.getInt("refer_accepted_0-28days_male"):0)
                        +(jsonObj.has("refer_accepted_29-59days_male")? jsonObj.getInt("refer_accepted_29-59days_male"):0)
                        +(jsonObj.has("refer_accepted_2month-1year_male")? jsonObj.getInt("refer_accepted_2month-1year_male"):0)
                        +(jsonObj.has("refer_accepted_1-5year_male")? jsonObj.getInt("refer_accepted_1-5year_male"):0);*/
						
				
				 Integer refer_accept_male_total = (jsonObj.has("refer_accepted_0-28days_male")? jsonObj.getInt("refer_accepted_0-28days_male"):0)
                        +(jsonObj.has("refer_accepted_29-59days_male")? jsonObj.getInt("refer_accepted_29-59days_male"):0);			

               /* Integer refer_accept_female_total = (jsonObj.has("refer_accepted_0-28days_female")? jsonObj.getInt("refer_accepted_0-28days_female"):0)
                        +(jsonObj.has("refer_accepted_29-59days_female")? jsonObj.getInt("refer_accepted_29-59days_female"):0)
                        +(jsonObj.has("refer_accepted_2month-1year_female")? jsonObj.getInt("refer_accepted_2month-1year_female"):0)
                        +(jsonObj.has("refer_accepted_1-5year_female")? jsonObj.getInt("refer_accepted_1-5year_female"):0);*/
				
				 Integer refer_accept_female_total = (jsonObj.has("refer_accepted_0-28days_female")? jsonObj.getInt("refer_accepted_0-28days_female"):0)
                        +(jsonObj.has("refer_accepted_29-59days_female")? jsonObj.getInt("refer_accepted_29-59days_female"):0);		

            %>
            <tr class="success" rowspan="3" >
                <td width="25" height="33" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">৫</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>রেফারে রাজী হওয়া </span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("refer_accepted_0-28days_male")? jsonObj.getInt("refer_accepted_0-28days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("refer_accepted_0-28days_female")? jsonObj.getInt("refer_accepted_0-28days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("refer_accepted_29-59days_male")? jsonObj.getInt("refer_accepted_29-59days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("refer_accepted_29-59days_female")? jsonObj.getInt("refer_accepted_29-59days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("refer_accepted_2month-1year_male")? jsonObj.getInt("refer_accepted_2month-1year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("refer_accepted_2month-1year_female")? jsonObj.getInt("refer_accepted_2month-1year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("refer_accepted_1-5year_male")? jsonObj.getInt("refer_accepted_1-5year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("refer_accepted_1-5year_female")? jsonObj.getInt("refer_accepted_1-5year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= refer_accept_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= refer_accept_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= refer_accept_male_total + refer_accept_female_total %></td>
            </tr>

            <%-- 1st Dose Addition --%>
            <%
                Integer fdose_male_total = (jsonObj.has("fst_dose_injection_0-28days_male")? jsonObj.getInt("fst_dose_injection_0-28days_male"):0)
                        +(jsonObj.has("fst_dose_injection_29-59days_male")? jsonObj.getInt("fst_dose_injection_29-59days_male"):0)
                        +(jsonObj.has("fst_dose_injection_2month-1year_male")? jsonObj.getInt("fst_dose_injection_2month-1year_male"):0)
                        +(jsonObj.has("fst_dose_injection_1-5year_male")? jsonObj.getInt("fst_dose_injection_1-5year_male"):0);

                Integer fdose_female_total = (jsonObj.has("fst_dose_injection_0-28days_female")? jsonObj.getInt("fst_dose_injection_0-28days_female"):0)
                        +(jsonObj.has("fst_dose_injection_29-59days_female")? jsonObj.getInt("fst_dose_injection_29-59days_female"):0)
                        +(jsonObj.has("fst_dose_injection_2month-1year_female")? jsonObj.getInt("fst_dose_injection_2month-1year_female"):0)
                        +(jsonObj.has("fst_dose_injection_1-5year_female")? jsonObj.getInt("fst_dose_injection_1-5year_female"):0);

            %>
            <tr rowspan="3" >
                <td width="25" height="32" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">৬</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>১ম ডোজ এন্টিবায়োটিক ইঞ্জেকশন</td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fst_dose_injection_0-28days_male")? jsonObj.getInt("fst_dose_injection_0-28days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fst_dose_injection_0-28days_female")? jsonObj.getInt("fst_dose_injection_0-28days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fst_dose_injection_29-59days_male")? jsonObj.getInt("fst_dose_injection_29-59days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fst_dose_injection_29-59days_female")? jsonObj.getInt("fst_dose_injection_29-59days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("fst_dose_injection_2month-1year_male")? jsonObj.getInt("fst_dose_injection_2month-1year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("fst_dose_injection_2month-1year_female")? jsonObj.getInt("fst_dose_injection_2month-1year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("fst_dose_injection_1-5year_male")? jsonObj.getInt("fst_dose_injection_1-5year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("fst_dose_injection_1-5year_female")? jsonObj.getInt("fst_dose_injection_1-5year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= fdose_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= fdose_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= fdose_male_total + fdose_female_total %></td>
            </tr>

            <%-- 2nd Dose Addition --%>
            <%
                Integer scdose_male_total = (jsonObj.has("second_dose_injection_0-28days_male")? jsonObj.getInt("second_dose_injection_0-28days_male"):0)
                        +(jsonObj.has("second_dose_injection_29-59days_male")? jsonObj.getInt("second_dose_injection_29-59days_male"):0)
                        +(jsonObj.has("second_dose_injection_2month-1year_male")? jsonObj.getInt("second_dose_injection_2month-1year_male"):0)
                        +(jsonObj.has("second_dose_injection_1-5year_male")? jsonObj.getInt("second_dose_injection_1-5year_male"):0);

                Integer scdose_female_total = (jsonObj.has("second_dose_injection_0-28days_female")? jsonObj.getInt("second_dose_injection_0-28days_female"):0)
                        +(jsonObj.has("second_dose_injection_29-59days_female")? jsonObj.getInt("second_dose_injection_29-59days_female"):0)
                        +(jsonObj.has("second_dose_injection_2month-1year_female")? jsonObj.getInt("second_dose_injection_2month-1year_female"):0)
                        +(jsonObj.has("second_dose_injection_1-5year_female")? jsonObj.getInt("second_dose_injection_1-5year_female"):0);

            %>
            <tr class='success' rowspan="3" >
                <td width="25" height="26" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">৭</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>২য় ডোজ এন্টিবায়োটিক ইঞ্জেকশন</td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("second_dose_injection_0-28days_male")? jsonObj.getInt("second_dose_injection_0-28days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("second_dose_injection_0-28days_female")? jsonObj.getInt("second_dose_injection_0-28days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("second_dose_injection_29-59days_male")? jsonObj.getInt("second_dose_injection_29-59days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("second_dose_injection_29-59days_female")? jsonObj.getInt("second_dose_injection_29-59days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("second_dose_injection_2month-1year_male")? jsonObj.getInt("second_dose_injection_2month-1year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("second_dose_injection_2month-1year_female")? jsonObj.getInt("second_dose_injection_2month-1year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("second_dose_injection_1-5year_male")? jsonObj.getInt("second_dose_injection_1-5year_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("second_dose_injection_1-5year_female")? jsonObj.getInt("second_dose_injection_1-5year_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= scdose_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= scdose_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= scdose_male_total + scdose_female_total %></td>
            </tr>

            <%-- Nuemonia addition Addition --%>
            <%
                Integer neum_male_total = (jsonObj.has("pneumonia_0-28days_male")? jsonObj.getInt("pneumonia_0-28days_male"):0)
                        +(jsonObj.has("pneumonia_29-59days_male")? jsonObj.getInt("pneumonia_29-59days_male"):0)
                        +(jsonObj.has("pneumonia_2month-1year_male")? jsonObj.getInt("pneumonia_2month-1year_male"):0)
                        +(jsonObj.has("pneumonia_1-5year_male")? jsonObj.getInt("pneumonia_1-5year_male"):0);

                Integer neum_female_total = (jsonObj.has("pneumonia_0-28days_female")? jsonObj.getInt("pneumonia_0-28days_female"):0)
                        +(jsonObj.has("pneumonia_29-59days_female")? jsonObj.getInt("pneumonia_29-59days_female"):0)
                        +(jsonObj.has("pneumonia_2month-1year_female")? jsonObj.getInt("pneumonia_2month-1year_female"):0)
                        +(jsonObj.has("pneumonia_1-5year_female")? jsonObj.getInt("pneumonia_1-5year_female"):0);

                Integer neum_total = neum_male_total + neum_male_total;
            %>

            <tr rowspan="3" >
                <td width="25" height="33" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">৮</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>নিউমোনিয়া</td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("pneumonia_0-28days_male")? jsonObj.getInt("pneumonia_0-28days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("pneumonia_0-28days_female")? jsonObj.getInt("pneumonia_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("pneumonia_29-59days_male")? jsonObj.getInt("pneumonia_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("pneumonia_29-59days_female")? jsonObj.getInt("pneumonia_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("pneumonia_2month-1year_male")? jsonObj.getInt("pneumonia_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("pneumonia_2month-1year_female")? jsonObj.getInt("pneumonia_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("pneumonia_1-5year_male")? jsonObj.getInt("pneumonia_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("pneumonia_1-5year_female")? jsonObj.getInt("pneumonia_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= neum_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= neum_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= neum_male_total + neum_female_total %></td>
            </tr>

            <%-- Influenja addition Addition --%>
            <%
                Integer influenja_male_total = (jsonObj.has("influenja_0-28days_male")? jsonObj.getInt("influenja_0-28days_male"):0)
                        +(jsonObj.has("influenja_29-59days_male")? jsonObj.getInt("influenja_29-59days_male"):0)
                        +(jsonObj.has("influenja_2month-1year_male")? jsonObj.getInt("influenja_2month-1year_male"):0)
                        +(jsonObj.has("influenja_1-5year_male")? jsonObj.getInt("influenja_1-5year_male"):0);

                Integer influenja_female_total = (jsonObj.has("influenja_0-28days_female")? jsonObj.getInt("influenja_0-28days_female"):0)
                        +(jsonObj.has("influenja_29-59days_female")? jsonObj.getInt("influenja_29-59days_female"):0)
                        +(jsonObj.has("influenja_2month-1year_female")? jsonObj.getInt("influenja_2month-1year_female"):0)
                        +(jsonObj.has("influenja_1-5year_female")? jsonObj.getInt("influenja_1-5year_female"):0);

                Integer influenja_total = influenja_male_total + influenja_female_total;

            %>
            <tr class='success' rowspan="3" >
                <td width="25" height="27" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">৯</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>নিউমোনিয়া নয়, সর্দি কাশি</td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("influenja_0-28days_male")? jsonObj.getInt("influenja_0-28days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("influenja_0-28days_female")? jsonObj.getInt("influenja_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("influenja_29-59days_male")? jsonObj.getInt("influenja_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("influenja_29-59days_female")? jsonObj.getInt("influenja_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("influenja_2month-1year_male")? jsonObj.getInt("influenja_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("influenja_2month-1year_female")? jsonObj.getInt("influenja_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("influenja_1-5year_male")? jsonObj.getInt("influenja_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("influenja_1-5year_female")? jsonObj.getInt("influenja_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= influenja_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= influenja_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= influenja_female_total + influenja_male_total %></td>
            </tr>

            <%-- Diareah addition Addition --%>
            <%
                Integer diarrhea_male_total = (jsonObj.has("diarrhea_0-28days_male")? jsonObj.getInt("diarrhea_0-28days_male"):0)
                        +(jsonObj.has("diarrhea_29-59days_male")? jsonObj.getInt("diarrhea_29-59days_male"):0)
                        +(jsonObj.has("diarrhea_2month-1year_male")? jsonObj.getInt("diarrhea_2month-1year_male"):0)
                        +(jsonObj.has("diarrhea_1-5year_male")? jsonObj.getInt("diarrhea_1-5year_male"):0);

                Integer diarrhea_female_total = (jsonObj.has("diarrhea_0-28days_female")? jsonObj.getInt("diarrhea_0-28days_female"):0)
                        +(jsonObj.has("diarrhea_29-59days_female")? jsonObj.getInt("diarrhea_29-59days_female"):0)
                        +(jsonObj.has("diarrhea_2month-1year_female")? jsonObj.getInt("diarrhea_2month-1year_female"):0)
                        +(jsonObj.has("diarrhea_1-5year_female")? jsonObj.getInt("diarrhea_1-5year_female"):0);

                Integer diarrhea_total = diarrhea_male_total + diarrhea_female_total;

            %>
            <tr rowspan="3" >
                <td width="25" height="33" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১০</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>ডায়রিয়া </td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("diarrhea_0-28days_male")? jsonObj.getInt("diarrhea_0-28days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("diarrhea_0-28days_female")? jsonObj.getInt("diarrhea_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("diarrhea_29-59days_male")? jsonObj.getInt("diarrhea_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("diarrhea_29-59days_female")? jsonObj.getInt("diarrhea_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("diarrhea_2month-1year_male")? jsonObj.getInt("diarrhea_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("diarrhea_2month-1year_female")? jsonObj.getInt("diarrhea_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("diarrhea_1-5year_male")? jsonObj.getInt("diarrhea_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("diarrhea_1-5year_female")? jsonObj.getInt("diarrhea_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= diarrhea_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= diarrhea_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= diarrhea_male_total + diarrhea_female_total %></td>
            </tr>

            <%-- malaria addition Addition --%>
            <%
                Integer malaria_male_total = (jsonObj.has("malaria_0-28days_male")? jsonObj.getInt("malaria_0-28days_male"):0)
                        +(jsonObj.has("malaria_29-59days_male")? jsonObj.getInt("malaria_29-59days_male"):0)
                        +(jsonObj.has("malaria_2month-1year_male")? jsonObj.getInt("malaria_2month-1year_male"):0)
                        +(jsonObj.has("malaria_1-5year_male")? jsonObj.getInt("malaria_1-5year_male"):0);

                Integer malaria_female_total = (jsonObj.has("malaria_0-28days_female")? jsonObj.getInt("malaria_0-28days_female"):0)
                        +(jsonObj.has("malaria_29-59days_female")? jsonObj.getInt("malaria_29-59days_female"):0)
                        +(jsonObj.has("malaria_2month-1year_female")? jsonObj.getInt("malaria_2month-1year_female"):0)
                        +(jsonObj.has("malaria_1-5year_female")? jsonObj.getInt("malaria_1-5year_female"):0);

                Integer malaria_total = malaria_male_total + malaria_female_total;

            %>

            <tr class='success' rowspan="3" >
                <td width="25" height="29" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১১</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>জ্বর - ম্যালেরিয়া </td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malaria_0-28days_male")? jsonObj.getInt("malaria_0-28days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malaria_0-28days_female")? jsonObj.getInt("malaria_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malaria_29-59days_male")? jsonObj.getInt("malaria_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malaria_29-59days_female")? jsonObj.getInt("malaria_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malaria_2month-1year_male")? jsonObj.getInt("malaria_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malaria_2month-1year_female")? jsonObj.getInt("malaria_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malaria_1-5year_male")? jsonObj.getInt("malaria_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malaria_1-5year_female")? jsonObj.getInt("malaria_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= malaria_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= malaria_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= malaria_male_total + malaria_female_total %></td>
            </tr>

            <%-- fever addition Addition --%>
            <%
                Integer fever_male_total = (jsonObj.has("fever_not_malaria_0-28days_male")? jsonObj.getInt("fever_not_malaria_0-28days_male"):0)
                        +(jsonObj.has("fever_not_malaria_29-59days_male")? jsonObj.getInt("fever_not_malaria_29-59days_male"):0)
                        +(jsonObj.has("fever_not_malaria_2month-1year_male")? jsonObj.getInt("fever_not_malaria_2month-1year_male"):0)
                        +(jsonObj.has("fever_not_malaria_1-5year_male")? jsonObj.getInt("fever_not_malaria_1-5year_male"):0);

                Integer fever_female_total = (jsonObj.has("fever_not_malaria_0-28days_female")? jsonObj.getInt("fever_not_malaria_0-28days_female"):0)
                        +(jsonObj.has("fever_not_malaria_29-59days_female")? jsonObj.getInt("fever_not_malaria_29-59days_female"):0)
                        +(jsonObj.has("fever_not_malaria_2month-1year_female")? jsonObj.getInt("fever_not_malaria_2month-1year_female"):0)
                        +(jsonObj.has("fever_not_malaria_1-5year_female")? jsonObj.getInt("fever_not_malaria_1-5year_female"):0);

                Integer fever_total = fever_male_total + fever_female_total;
            %>
            <tr rowspan="3" >
                <td width="25" height="29" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১২</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>জ্বর - ম্যালেরিয়া নয় </td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("fever_not_malaria_0-28days_male")? jsonObj.getInt("fever_not_malaria_0-28days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;">
				<%= jsonObj.has("fever_not_malaria_0-28days_female")? jsonObj.getInt("fever_not_malaria_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("fever_not_malaria_29-59days_male")? jsonObj.getInt("fever_not_malaria_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("fever_not_malaria_29-59days_female")? jsonObj.getInt("fever_not_malaria_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fever_not_malaria_2month-1year_male")? jsonObj.getInt("fever_not_malaria_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fever_not_malaria_2month-1year_female")? jsonObj.getInt("fever_not_malaria_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fever_not_malaria_1-5year_male")? jsonObj.getInt("fever_not_malaria_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("fever_not_malaria_1-5year_female")? jsonObj.getInt("fever_not_malaria_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= fever_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= fever_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= fever_female_total + fever_male_total %></td>
            </tr>
            <%--measles Addition --%>
            <%
                Integer measles_male_total = (jsonObj.has("measles_0-28days_male")? jsonObj.getInt("measles_0-28days_male"):0)
                        +(jsonObj.has("measles_29-59days_male")? jsonObj.getInt("measles_29-59days_male"):0)
                        +(jsonObj.has("measles_2month-1year_male")? jsonObj.getInt("measles_2month-1year_male"):0)
                        +(jsonObj.has("measles_1-5year_male")? jsonObj.getInt("measles_1-5year_male"):0);

                Integer measles_female_total = (jsonObj.has("measles_0-28days_female")? jsonObj.getInt("measles_0-28days_female"):0)
                        +(jsonObj.has("measles_29-59days_female")? jsonObj.getInt("measles_29-59days_female"):0)
                        +(jsonObj.has("measles_2month-1year_female")? jsonObj.getInt("measles_2month-1year_female"):0)
                        +(jsonObj.has("measles_1-5year_female")? jsonObj.getInt("measles_1-5year_female"):0);

                Integer measles_total = measles_male_total + measles_female_total;

            %>
            <tr class='success' rowspan="3" >
                <td width="25" height="30" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১৩</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>হাম </td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("measles_0-28days_male")? jsonObj.getInt("measles_0-28days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("measles_0-28days_female")? jsonObj.getInt("measles_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("measles_29-59days_male")? jsonObj.getInt("measles_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("measles_29-59days_female")? jsonObj.getInt("measles_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("measles_2month-1year_male")? jsonObj.getInt("measles_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("measles_2month-1year_female")? jsonObj.getInt("measles_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("measles_1-5year_male")? jsonObj.getInt("measles_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("measles_1-5year_female")? jsonObj.getInt("measles_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= measles_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= measles_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= measles_female_total + measles_male_total %></td>
            </tr>

            <%--Ear Problem Addition --%>
            <%
                Integer ear_male_total = (jsonObj.has("ear_problem_0-28days_male")? jsonObj.getInt("ear_problem_0-28days_male"):0)
                        +(jsonObj.has("ear_problem_29-59days_male")? jsonObj.getInt("ear_problem_29-59days_male"):0)
                        +(jsonObj.has("ear_problem_2month-1year_male")? jsonObj.getInt("ear_problem_2month-1year_male"):0)
                        +(jsonObj.has("ear_problem_1-5year_male")? jsonObj.getInt("ear_problem_1-5year_male"):0);

                Integer ear_female_total = (jsonObj.has("ear_problem_0-28days_female")? jsonObj.getInt("ear_problem_0-28days_female"):0)
                        +(jsonObj.has("ear_problem_29-59days_female")? jsonObj.getInt("ear_problem_29-59days_female"):0)
                        +(jsonObj.has("ear_problem_2month-1year_female")? jsonObj.getInt("ear_problem_2month-1year_female"):0)
                        +(jsonObj.has("ear_problem_1-5year_female")? jsonObj.getInt("ear_problem_1-5year_female"):0);

                Integer ear_total = ear_male_total + ear_female_total;

            %>
            <tr rowspan="3" >
                <td width="25" height="31" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১৪</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>কানের সমস্যা </td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("ear_problem_0-28days_male")? jsonObj.getInt("ear_problem_0-28days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("ear_problem_0-28days_female")? jsonObj.getInt("ear_problem_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("ear_problem_29-59days_male")? jsonObj.getInt("ear_problem_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("ear_problem_29-59days_female")? jsonObj.getInt("ear_problem_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("ear_problem_2month-1year_male")? jsonObj.getInt("ear_problem_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("ear_problem_2month-1year_female")? jsonObj.getInt("ear_problem_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("ear_problem_1-5year_male")? jsonObj.getInt("ear_problem_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("ear_problem_1-5year_female")? jsonObj.getInt("ear_problem_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= ear_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= ear_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= ear_male_total + ear_female_total %></td>
            </tr>

            <%--Malnutrition Addition --%>
            <%
                Integer malnutrition_male_total = (jsonObj.has("malnutrition_0-28days_male")? jsonObj.getInt("malnutrition_0-28days_male"):0)
                        +(jsonObj.has("malnutrition_29-59days_male")? jsonObj.getInt("malnutrition_29-59days_male"):0)
                        +(jsonObj.has("malnutrition_2month-1year_male")? jsonObj.getInt("malnutrition_2month-1year_male"):0)
                        +(jsonObj.has("malnutrition_1-5year_male")? jsonObj.getInt("malnutrition_1-5year_male"):0);

                Integer malnutrition_female_total = (jsonObj.has("malnutrition_0-28days_female")? jsonObj.getInt("malnutrition_0-28days_female"):0)
                        +(jsonObj.has("malnutrition_29-59days_female")? jsonObj.getInt("malnutrition_29-59days_female"):0)
                        +(jsonObj.has("malnutrition_2month-1year_female")? jsonObj.getInt("malnutrition_2month-1year_female"):0)
                        +(jsonObj.has("malnutrition_1-5year_female")? jsonObj.getInt("malnutrition_1-5year_female"):0);

                Integer malnutrition_total = malnutrition_male_total + malnutrition_female_total;

            %>
            <tr class='success' rowspan="3" >
                <td width="25" height="32" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১৫</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>অপুষ্টি </td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malnutrition_0-28days_male")? jsonObj.getInt("malnutrition_0-28days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malnutrition_0-28days_female")? jsonObj.getInt("malnutrition_0-28days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malnutrition_29-59days_male")? jsonObj.getInt("malnutrition_29-59days_male"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;background-color:#999999;'><span style="display:none;"><%= jsonObj.has("malnutrition_29-59days_female")? jsonObj.getInt("malnutrition_29-59days_female"):0 %></span></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malnutrition_2month-1year_male")? jsonObj.getInt("malnutrition_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malnutrition_2month-1year_female")? jsonObj.getInt("malnutrition_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malnutrition_1-5year_male")? jsonObj.getInt("malnutrition_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("malnutrition_1-5year_female")? jsonObj.getInt("malnutrition_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= malnutrition_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= malnutrition_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= malnutrition_male_total + malnutrition_female_total %></td>
            </tr>

            <%--Others Addition --%>
            <%
                Integer other_male_total = (jsonObj.has("other_disease_0-28days_male")? jsonObj.getInt("other_disease_0-28days_male"):0)
                        +(jsonObj.has("other_disease_29-59days_male")? jsonObj.getInt("other_disease_29-59days_male"):0)
                        +(jsonObj.has("other_disease_2month-1year_male")? jsonObj.getInt("other_disease_2month-1year_male"):0)
                        +(jsonObj.has("other_disease_1-5year_male")? jsonObj.getInt("other_disease_1-5year_male"):0);

                Integer other_female_total = (jsonObj.has("other_disease_0-28days_female")? jsonObj.getInt("other_disease_0-28days_female"):0)
                        +(jsonObj.has("other_disease_29-59days_female")? jsonObj.getInt("other_disease_29-59days_female"):0)
                        +(jsonObj.has("other_disease_2month-1year_female")? jsonObj.getInt("other_disease_2month-1year_female"):0)
                        +(jsonObj.has("other_disease_1-5year_female")? jsonObj.getInt("other_disease_1-5year_female"):0);

                Integer other_total = other_male_total + other_female_total;
            %>
            <tr  rowspan="3" >
                <td width="25" height="29" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১৬</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>অন্যান্য </td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_0-28days_male")? jsonObj.getInt("other_disease_0-28days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_0-28days_female")? jsonObj.getInt("other_disease_0-28days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_29-59days_male")? jsonObj.getInt("other_disease_29-59days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_29-59days_female")? jsonObj.getInt("other_disease_29-59days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_2month-1year_male")? jsonObj.getInt("other_disease_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_2month-1year_female")? jsonObj.getInt("other_disease_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_1-5year_male")? jsonObj.getInt("other_disease_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("other_disease_1-5year_female")? jsonObj.getInt("other_disease_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= other_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= other_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= other_male_total + other_female_total %></td>
            </tr>

            <%--Refer Addition --%>
            <%
            Integer refer_male_total = (jsonObj.has("imci_referred_0-28days_male")? jsonObj.getInt("imci_referred_0-28days_male"):0)
                    +(jsonObj.has("imci_referred_29-59days_male")? jsonObj.getInt("imci_referred_29-59days_male"):0)
                +(jsonObj.has("imci_referred_2month-1year_male")? jsonObj.getInt("imci_referred_2month-1year_male"):0)
                    +(jsonObj.has("imci_referred_1-5year_male")? jsonObj.getInt("imci_referred_1-5year_male"):0);

            Integer refer_female_total = (jsonObj.has("imci_referred_0-28days_female")? jsonObj.getInt("imci_referred_0-28days_female"):0)
                        +(jsonObj.has("imci_referred_29-59days_female")? jsonObj.getInt("imci_referred_29-59days_female"):0)
                        +(jsonObj.has("imci_referred_2month-1year_female")? jsonObj.getInt("imci_referred_2month-1year_female"):0)
                        +(jsonObj.has("imci_referred_1-5year_female")? jsonObj.getInt("imci_referred_1-5year_female"):0);

            Integer refer_total = refer_male_total + refer_female_total;
            %>
            <tr class='success' rowspan="3" >
                <td width="25" height="28" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="center">১৭</div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'>রেফারকৃ্ত</td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_0-28days_male")? jsonObj.getInt("imci_referred_0-28days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_0-28days_female")? jsonObj.getInt("imci_referred_0-28days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_29-59days_male")? jsonObj.getInt("imci_referred_29-59days_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_29-59days_female")? jsonObj.getInt("imci_referred_29-59days_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_2month-1year_male")? jsonObj.getInt("imci_referred_2month-1year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_2month-1year_female")? jsonObj.getInt("imci_referred_2month-1year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_1-5year_male")? jsonObj.getInt("imci_referred_1-5year_male"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= jsonObj.has("imci_referred_1-5year_female")? jsonObj.getInt("imci_referred_1-5year_female"):0 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= refer_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= refer_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'> <%= refer_male_total + refer_female_total %></td>
            </tr>

            <%--Total Male Female Addition --%>
            <%
                Integer male_total_0_28 =(jsonObj.has("very_serious_disease_0-28days_male")? jsonObj.getInt("very_serious_disease_0-28days_male"):0)
                        +(jsonObj.has("very_serious_disease_infected_0-28days_male")? jsonObj.getInt("very_serious_disease_infected_0-28days_male"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_0-28days_male")? jsonObj.getInt("very_serious_disease_neumonia_0-28days_male"):0)
                        +(jsonObj.has("light_infection_0-28days_male")? jsonObj.getInt("light_infection_0-28days_male"):0)
                        +(jsonObj.has("refer_accepted_0-28days_male")? jsonObj.getInt("refer_accepted_0-28days_male"):0)
                        +(jsonObj.has("fst_dose_injection_0-28days_male")? jsonObj.getInt("fst_dose_injection_0-28days_male"):0)
                        +(jsonObj.has("second_dose_injection_0-28days_male")? jsonObj.getInt("second_dose_injection_0-28days_male"):0)
                        +(jsonObj.has("pneumonia_0-28days_male")? jsonObj.getInt("pneumonia_0-28days_male"):0)
                        +(jsonObj.has("influenja_0-28days_male")? jsonObj.getInt("influenja_0-28days_male"):0)
                        +(jsonObj.has("diarrhea_0-28days_male")? jsonObj.getInt("diarrhea_0-28days_male"):0)
                        +(jsonObj.has("malaria_0-28days_male")? jsonObj.getInt("malaria_0-28days_male"):0)
                        +(jsonObj.has("fever_not_malaria_0-28days_male")? jsonObj.getInt("fever_not_malaria_0-28days_male"):0)
                        +(jsonObj.has("measles_0-28days_male")? jsonObj.getInt("measles_0-28days_male"):0)
                        +(jsonObj.has("ear_problem_0-28days_male")? jsonObj.getInt("ear_problem_0-28days_male"):0)
                        +(jsonObj.has("malnutrition_0-28days_male")? jsonObj.getInt("malnutrition_0-28days_male"):0)
                        +(jsonObj.has("other_disease_0-28days_male")? jsonObj.getInt("other_disease_0-28days_male"):0)
                        +(jsonObj.has("imci_referred_0-28days_male")? jsonObj.getInt("imci_referred_0-28days_male"):0);


                Integer female_total_0_28 = (jsonObj.has("very_serious_disease_0-28days_female")? jsonObj.getInt("very_serious_disease_0-28days_female"):0)
                        + (jsonObj.has("very_serious_disease_infected_0-28days_female")? jsonObj.getInt("very_serious_disease_infected_0-28days_female"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_0-28days_female")? jsonObj.getInt("very_serious_disease_neumonia_0-28days_female"):0)
                        +(jsonObj.has("light_infection_0-28days_female")? jsonObj.getInt("light_infection_0-28days_female"):0)
                        +(jsonObj.has("refer_accepted_0-28days_female")? jsonObj.getInt("refer_accepted_0-28days_female"):0)
                        +(jsonObj.has("fst_dose_injection_0-28days_female")? jsonObj.getInt("fst_dose_injection_0-28days_female"):0)
                        +(jsonObj.has("second_dose_injection_0-28days_female")? jsonObj.getInt("second_dose_injection_0-28days_female"):0)
                        +(jsonObj.has("pneumonia_0-28days_female")? jsonObj.getInt("pneumonia_0-28days_female"):0)
                        +(jsonObj.has("influenja_0-28days_female")? jsonObj.getInt("influenja_0-28days_female"):0)
                        +(jsonObj.has("diarrhea_0-28days_female")? jsonObj.getInt("diarrhea_0-28days_female"):0)
                        +(jsonObj.has("malaria_0-28days_female")? jsonObj.getInt("malaria_0-28days_female"):0)
                        +(jsonObj.has("fever_not_malaria_0-28days_female")? jsonObj.getInt("fever_not_malaria_0-28days_female"):0)
                        +(jsonObj.has("measles_0-28days_female")? jsonObj.getInt("measles_0-28days_female"):0)
                        +(jsonObj.has("ear_problem_0-28days_female")? jsonObj.getInt("ear_problem_0-28days_female"):0)
                        +(jsonObj.has("malnutrition_0-28days_female")? jsonObj.getInt("malnutrition_0-28days_female"):0)
                        +(jsonObj.has("other_disease_0-28days_female")? jsonObj.getInt("other_disease_0-28days_female"):0)
                        +(jsonObj.has("imci_referred_0-28days_female")? jsonObj.getInt("imci_referred_0-28days_female"):0)
                        +(jsonObj.has("imci_referred_0-28days_female")? jsonObj.getInt("imci_referred_0-28days_female"):0);

                Integer male_total_29_59 = (jsonObj.has("very_serious_disease_29-59days_male")? jsonObj.getInt("very_serious_disease_29-59days_male"):0)
                        +(jsonObj.has("very_serious_disease_infected_29-59days_male")? jsonObj.getInt("very_serious_disease_infected_29-59days_male"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_29-59days_male")? jsonObj.getInt("very_serious_disease_neumonia_29-59days_male"):0)
                        +(jsonObj.has("light_infection_29-59days_male")? jsonObj.getInt("light_infection_29-59days_male"):0)
                        +(jsonObj.has("refer_accepted_29-59days_male")? jsonObj.getInt("refer_accepted_29-59days_male"):0)
                        +(jsonObj.has("fst_dose_injection_29-59days_male")? jsonObj.getInt("fst_dose_injection_29-59days_male"):0)
                        +(jsonObj.has("second_dose_injection_29-59days_male")? jsonObj.getInt("second_dose_injection_29-59days_male"):0)
                        +(jsonObj.has("pneumonia_29-59days_male")? jsonObj.getInt("pneumonia_29-59days_male"):0)
                        +(jsonObj.has("influenja_29-59days_male")? jsonObj.getInt("influenja_29-59days_male"):0)
                        +(jsonObj.has("diarrhea_29-59days_male")? jsonObj.getInt("diarrhea_29-59days_male"):0)
                        +(jsonObj.has("malaria_29-59days_male")? jsonObj.getInt("malaria_29-59days_male"):0)
                        +(jsonObj.has("fever_not_malaria_29-59days_male")? jsonObj.getInt("fever_not_malaria_29-59days_male"):0)
                        +(jsonObj.has("measles_29-59days_male")? jsonObj.getInt("measles_29-59days_male"):0)
                        +(jsonObj.has("ear_problem_29-59days_male")? jsonObj.getInt("ear_problem_29-59days_male"):0)
                        +(jsonObj.has("malnutrition_29-59days_male")? jsonObj.getInt("malnutrition_29-59days_male"):0)
                        +(jsonObj.has("other_disease_29-59days_male")? jsonObj.getInt("other_disease_29-59days_male"):0)
                        +(jsonObj.has("imci_referred_29-59days_male")? jsonObj.getInt("imci_referred_29-59days_male"):0);

                Integer female_total_29_59 = (jsonObj.has("very_serious_disease_29-59days_female")? jsonObj.getInt("very_serious_disease_29-59days_female"):0)
                        +(jsonObj.has("very_serious_disease_infected_29-59days_female")? jsonObj.getInt("very_serious_disease_infected_29-59days_female"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_29-59days_female")? jsonObj.getInt("very_serious_disease_neumonia_29-59days_female"):0)
                        +(jsonObj.has("light_infection_29-59days_female")? jsonObj.getInt("light_infection_29-59days_female"):0)
                        +(jsonObj.has("refer_accepted_29-59days_female")? jsonObj.getInt("refer_accepted_29-59days_female"):0)
                        +(jsonObj.has("fst_dose_injection_29-59days_female")? jsonObj.getInt("fst_dose_injection_29-59days_female"):0)
                        +(jsonObj.has("second_dose_injection_29-59days_female")? jsonObj.getInt("second_dose_injection_29-59days_female"):0)
                        +(jsonObj.has("pneumonia_29-59days_female")? jsonObj.getInt("pneumonia_29-59days_female"):0)
                        +(jsonObj.has("influenja_29-59days_female")? jsonObj.getInt("influenja_29-59days_female"):0)
                        +(jsonObj.has("diarrhea_29-59days_female")? jsonObj.getInt("diarrhea_29-59days_female"):0)
                        +(jsonObj.has("malaria_29-59days_female")? jsonObj.getInt("malaria_29-59days_female"):0)
                        +(jsonObj.has("fever_not_malaria_29-59days_female")? jsonObj.getInt("fever_not_malaria_29-59days_female"):0)
                        +(jsonObj.has("measles_29-59days_female")? jsonObj.getInt("measles_29-59days_female"):0)
                        +(jsonObj.has("ear_problem_29-59days_female")? jsonObj.getInt("ear_problem_29-59days_female"):0)
                        +(jsonObj.has("malnutrition_29-59days_female")? jsonObj.getInt("malnutrition_29-59days_female"):0)
                        +(jsonObj.has("other_disease_29-59days_female")? jsonObj.getInt("other_disease_29-59days_female"):0)
                        +(jsonObj.has("imci_referred_29-59days_female")? jsonObj.getInt("imci_referred_29-59days_female"):0);

                Integer male_total_2month = (jsonObj.has("very_serious_disease_2month-1year_male")? jsonObj.getInt("very_serious_disease_2month-1year_male"):0)
                        +(jsonObj.has("very_serious_disease_infected_2month-1year_male")? jsonObj.getInt("very_serious_disease_infected_2month-1year_male"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_2month-1year_male")? jsonObj.getInt("very_serious_disease_neumonia_2month-1year_male"):0)
                        +(jsonObj.has("light_infection_2month-1year_male")? jsonObj.getInt("light_infection_2month-1year_male"):0)
                        +(jsonObj.has("refer_accepted_2month-1year_male")? jsonObj.getInt("refer_accepted_2month-1year_male"):0)
                        +(jsonObj.has("fst_dose_injection_2month-1year_male")? jsonObj.getInt("fst_dose_injection_2month-1year_male"):0)
                        +(jsonObj.has("second_dose_injection_2month-1year_male")? jsonObj.getInt("second_dose_injection_2month-1year_male"):0)
                        +(jsonObj.has("pneumonia_2month-1year_male")? jsonObj.getInt("pneumonia_2month-1year_male"):0)
                        +(jsonObj.has("influenja_2month-1year_male")? jsonObj.getInt("influenja_2month-1year_male"):0)
                        +(jsonObj.has("diarrhea_2month-1year_male")? jsonObj.getInt("diarrhea_2month-1year_male"):0)
                        +(jsonObj.has("malaria_2month-1year_male")? jsonObj.getInt("malaria_2month-1year_male"):0)
                        +(jsonObj.has("fever_not_malaria_2month-1year_male")? jsonObj.getInt("fever_not_malaria_2month-1year_male"):0)
                        +(jsonObj.has("measles_2month-1year_male")? jsonObj.getInt("measles_2month-1year_male"):0)
                        +(jsonObj.has("ear_problem_2month-1year_male")? jsonObj.getInt("ear_problem_2month-1year_male"):0)
                        +(jsonObj.has("malnutrition_2month-1year_male")? jsonObj.getInt("malnutrition_2month-1year_male"):0)
                        +(jsonObj.has("other_disease_2month-1year_male")? jsonObj.getInt("other_disease_2month-1year_male"):0)
                        +(jsonObj.has("imci_referred_2month-1year_male")? jsonObj.getInt("imci_referred_2month-1year_male"):0);

                Integer female_total_2month = (jsonObj.has("very_serious_disease_2month-1year_female")? jsonObj.getInt("very_serious_disease_2month-1year_female"):0)
                        +(jsonObj.has("very_serious_disease_infected_2month-1year_female")? jsonObj.getInt("very_serious_disease_infected_2month-1year_female"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_2month-1year_female")? jsonObj.getInt("very_serious_disease_neumonia_2month-1year_female"):0)
                        +(jsonObj.has("light_infection_2month-1year_female")? jsonObj.getInt("light_infection_2month-1year_female"):0)
                        +(jsonObj.has("refer_accepted_2month-1year_female")? jsonObj.getInt("refer_accepted_2month-1year_female"):0)
                        +(jsonObj.has("fst_dose_injection_2month-1year_female")? jsonObj.getInt("fst_dose_injection_2month-1year_female"):0)
                        +(jsonObj.has("second_dose_injection_2month-1year_female")? jsonObj.getInt("second_dose_injection_2month-1year_female"):0)
                        +(jsonObj.has("pneumonia_2month-1year_female")? jsonObj.getInt("pneumonia_2month-1year_female"):0)
                        +(jsonObj.has("influenja_2month-1year_female")? jsonObj.getInt("influenja_2month-1year_female"):0)
                        +(jsonObj.has("diarrhea_2month-1year_female")? jsonObj.getInt("diarrhea_2month-1year_female"):0)
                        +(jsonObj.has("malaria_2month-1year_female")? jsonObj.getInt("malaria_2month-1year_female"):0)
                        +(jsonObj.has("fever_not_malaria_2month-1year_female")? jsonObj.getInt("fever_not_malaria_2month-1year_female"):0)
                        +(jsonObj.has("measles_2month-1year_female")? jsonObj.getInt("measles_2month-1year_female"):0)
                        +(jsonObj.has("ear_problem_2month-1year_female")? jsonObj.getInt("ear_problem_2month-1year_female"):0)
                        +(jsonObj.has("malnutrition_2month-1year_female")? jsonObj.getInt("malnutrition_2month-1year_female"):0)
                        +(jsonObj.has("other_disease_2month-1year_female")? jsonObj.getInt("other_disease_2month-1year_female"):0)
                        +(jsonObj.has("imci_referred_2month-1year_female")? jsonObj.getInt("imci_referred_2month-1year_female"):0);

                Integer male_total_1year = (jsonObj.has("very_serious_disease_1-5year_male")? jsonObj.getInt("very_serious_disease_1-5year_male"):0)
                        +(jsonObj.has("very_serious_disease_infected_1-5year_male")? jsonObj.getInt("very_serious_disease_infected_1-5year_male"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_1-5year_male")? jsonObj.getInt("very_serious_disease_neumonia_1-5year_male"):0)
                        +(jsonObj.has("light_infection_1-5year_male")? jsonObj.getInt("light_infection_1-5year_male"):0)
                        +(jsonObj.has("refer_accepted_1-5year_male")? jsonObj.getInt("refer_accepted_1-5year_male"):0)
                        +(jsonObj.has("fst_dose_injection_1-5year_male")? jsonObj.getInt("fst_dose_injection_1-5year_male"):0)
                        +(jsonObj.has("second_dose_injection_1-5year_male")? jsonObj.getInt("second_dose_injection_1-5year_male"):0)
                        +(jsonObj.has("pneumonia_1-5year_male")? jsonObj.getInt("pneumonia_1-5year_male"):0)
                        +(jsonObj.has("influenja_1-5year_male")? jsonObj.getInt("influenja_1-5year_male"):0)
                        +(jsonObj.has("diarrhea_1-5year_male")? jsonObj.getInt("diarrhea_1-5year_male"):0)
                        +(jsonObj.has("malaria_1-5year_male")? jsonObj.getInt("malaria_1-5year_male"):0)
                        +(jsonObj.has("fever_not_malaria_1-5year_male")? jsonObj.getInt("fever_not_malaria_1-5year_male"):0)
                        +(jsonObj.has("measles_1-5year_male")? jsonObj.getInt("measles_1-5year_male"):0)
                        +(jsonObj.has("ear_problem_1-5year_male")? jsonObj.getInt("ear_problem_1-5year_male"):0)
                        +(jsonObj.has("malnutrition_1-5year_male")? jsonObj.getInt("malnutrition_1-5year_male"):0)
                        +(jsonObj.has("other_disease_1-5year_male")? jsonObj.getInt("other_disease_1-5year_male"):0)
                        +(jsonObj.has("imci_referred_1-5year_male")? jsonObj.getInt("imci_referred_1-5year_male"):0);


                Integer female_total_1year = (jsonObj.has("very_serious_disease_1-5year_female")? jsonObj.getInt("very_serious_disease_1-5year_female"):0)
                        +(jsonObj.has("very_serious_disease_infected_1-5year_female")? jsonObj.getInt("very_serious_disease_infected_1-5year_female"):0)
                        +(jsonObj.has("very_serious_disease_neumonia_1-5year_female")? jsonObj.getInt("very_serious_disease_neumonia_1-5year_female"):0)
                        +(jsonObj.has("light_infection_1-5year_female")? jsonObj.getInt("light_infection_1-5year_female"):0)
                        +(jsonObj.has("refer_accepted_1-5year_female")? jsonObj.getInt("refer_accepted_1-5year_female"):0)
                        +(jsonObj.has("fst_dose_injection_1-5year_female")? jsonObj.getInt("fst_dose_injection_1-5year_female"):0)
                        +(jsonObj.has("second_dose_injection_1-5year_female")? jsonObj.getInt("second_dose_injection_1-5year_female"):0)
                        +(jsonObj.has("pneumonia_1-5year_female")? jsonObj.getInt("pneumonia_1-5year_female"):0)
                        +(jsonObj.has("influenja_1-5year_female")? jsonObj.getInt("influenja_1-5year_female"):0)
                        +(jsonObj.has("diarrhea_1-5year_female")? jsonObj.getInt("diarrhea_1-5year_female"):0)
                        +(jsonObj.has("malaria_1-5year_female")? jsonObj.getInt("malaria_1-5year_female"):0)
                        +(jsonObj.has("fever_not_malaria_1-5year_female")? jsonObj.getInt("fever_not_malaria_1-5year_female"):0)
                        +(jsonObj.has("measles_1-5year_female")? jsonObj.getInt("measles_1-5year_female"):0)
                        +(jsonObj.has("ear_problem_1-5year_female")? jsonObj.getInt("ear_problem_1-5year_female"):0)
                        +(jsonObj.has("malnutrition_1-5year_female")? jsonObj.getInt("malnutrition_1-5year_female"):0)
                        +(jsonObj.has("other_disease_1-5year_female")? jsonObj.getInt("other_disease_1-5year_female"):0)
                        +(jsonObj.has("imci_referred_1-5year_female")? jsonObj.getInt("imci_referred_1-5year_female"):0);

            %>
            <tr rowspan="3" >
                <td height="28" colspan="2" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;border-right: 1px solid black;'><div align="right">সর্ব মোট </div></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= male_total_0_28 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= female_total_0_28 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= male_total_29_59 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= female_total_29_59 %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= male_total_2month %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= female_total_2month %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= male_total_1year %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= female_total_1year %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_male_total + very_infected_male_total + neumonia_male_total + infection_male_total +
                        refer_accept_male_total + fdose_male_total + scdose_male_total + neum_male_total + influenja_male_total +
                        diarrhea_male_total + malaria_male_total + fever_male_total + measles_male_total + ear_male_total +
                        malnutrition_male_total + other_male_total + refer_male_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= very_female_total + very_infected_female_total + neumonia_female_total + infection_female_total +
                        refer_accept_female_total + fdose_female_total + scdose_female_total + neum_female_total + influenja_female_total +
                        diarrhea_female_total + malaria_female_total + fever_female_total + measles_female_total + ear_female_total +
                        malnutrition_female_total + other_female_total + refer_female_total %></td>
                <td valign="top" style='border-top: 1px solid black;border-right: 1px solid black;'><%= (very_male_total + very_infected_male_total + neumonia_male_total + infection_male_total +
                        refer_accept_male_total + fdose_male_total + scdose_male_total + neum_male_total + influenja_male_total +
                        diarrhea_male_total + malaria_male_total + fever_male_total + measles_male_total + ear_male_total +
                        malnutrition_male_total + other_male_total + refer_male_total) + (very_female_total + very_infected_female_total + neumonia_female_total + infection_female_total +
                        refer_accept_female_total + fdose_female_total + scdose_female_total + neum_female_total + influenja_female_total +
                        diarrhea_female_total + malaria_female_total + fever_female_total + measles_female_total + ear_female_total +
                        malnutrition_female_total + other_female_total + refer_female_total) %></td>
            </tr>
        </table>
        <p>&nbsp;</p>
        <table width="754" height="66" border="0" cellpadding="0" cellspacing="0" style="font-size:11px;display:none;" class="table page_4 table_hide" id="table_11">
            <tr>
                <td width="364" height="29" style="border-right: 1px solid black;"><div align="right">FWV কত্ক স্যাটেলাইট ক্লিনিকঃ  লক্ষ্যমাত্রা </div></td>
                <td width="67" style="border-right: 1px solid black;height:19px;"><%= jsonObj.has("terget_clinic")? jsonObj.getInt("terget_clinic"):0 %></td>
                <td width="230" style="border-right: 1px solid black;"><p align="right"> অনুষ্ঠিত </p> </td>
                <td width="75" style="border-right: 1px solid black; height:19px;"><%= jsonObj.has("achieved_clinic")? jsonObj.getInt("achieved_clinic"):0 %></td>
            </tr>
            <tr>
                <td height="29" style="border-right: 1px solid black;"><div align="right">SACMO কতৃক NSV ক্লায়েন্ট রেফার : লক্ষ্যমাত্রা </div></td>
                <td style="border-right: 1px solid black;border-top:1px solid black;height:19px;"><%= jsonObj.has("terget_sacmo_nsv_client_refer")? jsonObj.getInt("terget_sacmo_nsv_client_refer"):0 %></td>
                <td style="border-right: 1px solid black;"><div align="right">অর্জন</div></td>
        <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("achieved_sacmo_nsv_client_refer")? jsonObj.getInt("achieved_sacmo_nsv_client_refer"):0 %></td>
            </tr>
        </table>
    </div>

    <!-- <p>* Growth Monitoring and Promotion </p>-->

    <div class="printer">

        <table class="table page_5 table_hide" id="table_12" width="700" cellspacing="0" style='font-size:10px;display:none;'>
	   <!-- <table class="table page_5 table_hide" id="" width="700" cellspacing="0" style='font-size:10px;'>-->
		
            <tr>
				<td width="94">&nbsp;</td>
                <td width="376" style='text-align: center;'>&nbsp;</td>
                <td width="608" style='text-align: right;'>এমআইএস ফরম-৩<br>
                    পৃষ্ঠা-5</td>
            <tr></table>
      <!--  <table width="750px" height="42" border="0" style="font-size:11px;display:none;" class="table page_5 table_hide" id="table_13" >
            <tr>
                <td width="1154" height="38"><div align="center"><strong> মাসিক মওজুদ ও বিতরনের হিসাব বিষয়ক </strong></div></td>
            </tr>
        </table>-->
		
	<table height="528" cellpadding="0" cellspacing="0" class="table page_5 table_hide" id="table_13" style="font-size:11px;border: 1px solid black;display:none;">
	
	  
<!--<table max-width="750px" height="528" cellpadding="0" cellspacing="0" class="table page_5 table_hide" id="table_14" style="font-size:11px;border: 1px solid black;">-->
			<tr>
			<td colspan="23" height="38" style="border-top: 1px solid black;border-top: 1px solid black;"><div align="center"><strong> মাসিক মওজুদ ও বিতরনের হিসাব বিষয়ক </strong></div></td>
			</tr>
            <tr class='danger'>
                <td colspan="2" rowspan="2" style="border-right: 1px solid black;border-top: 1px solid black;">&nbsp;</td>
                <td height="49" colspan="2" style="border-right: 1px solid black;border-bottom: 1px solid black;border-top: 1px solid black;"><p>খাবার বড়ি (চক্র) </p>                </td>
                <td width="64" rowspan="2" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">কনডম(পিস)</span></td>
                <td colspan="2" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-bottom: 1px solid black;border-top: 1px solid black;">ইনজেকটেবল </td>
                <td width="86" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">আই ইউ ডি (পিস)</span></td>
                <td colspan="2" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-bottom: 1px solid black;border-top: 1px solid black;">ইমপ্লান্ট(সেট)</td>
                <td width="70" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইসিপি (ডোজ)</span></td>
                <td width="103" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;">
                    <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">মিসোপ্রোস্টল (ডোজ)</span></td>
                <td width="90" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;">
                    <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">এমআরএম (প্যাক)</span></td>
                <td width="110" rowspan="2" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">৭.১% ক্লোরোহেক্সিডিন </span></td>
                <td width="95" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন MgSO4</span></td>
                <td width="97" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;" ><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন <br/>
          অক্সিটোক্সিন(ডোজ)</span></td>
                <td width="88" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><div align="left"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">এমএনপি(স্যাসেট)</span></div></td>
                <td width="114" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;">
                    <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">স্যানিটারি প্যাড(সংখ্যা)					</span></td>
                <td width="84" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;">
					<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> এম আর(এমভিএ)<br/>
					কিট(সংখ্যা)</span></td>
                <td width="107" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> ডেলিভারি কিট(সংখ্যা)</span></td>
                <td width="100" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> ডিডিএস কিট(সংখ্যা)</span></td>
                <td width="121" rowspan="2" valign="middle" style="text-align: center;vertical-align: middle;border-right: 1px solid black;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন এ্যান্টিনেটাল<br/>
          করটিকোস্টেরয়েড </span></td>
                <td width="67" rowspan="2" style="text-align: center;vertical-align: middle;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন <br/>জেন্টামাইসিন</span></td>
            </tr>

            <tr>
                <td width="41" height="135" class="danger" style="border-right: 1px solid black;"><p><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">সুখী</span></p>                </td>
                <td width="41" class="danger" style="border-right: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">আপন</span></td>
                <td width="41" class="danger" style="border-right: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ভায়াল</span></td>
                <td width="41" class="danger" style="border-right: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">সিরিঞ্জ</span></td>
                <td width="44" class="danger" style="border-right: 1px solid black;">ইমপ্লানন</td>
                <td width="39" class="danger" style="border-right: 1px solid black;">জেডেল</td>
            </tr>

            <tr class="danger">
                <td height="31" colspan="2" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">২</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">৩</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">৪</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">৫</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">৬</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">৭</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">৮</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">৯</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১০</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১১</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১২</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১৩</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১৪</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১৫</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১৬</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১৭</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১৮</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">১৯</div></td>
                <td class="success" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">২০</div></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><div align="center">২১</div></td>
                <td class="success" style="border-top: 1px solid black; height:19px;"><div align="center">২২</div></td>
            </tr>
            <tr>
                <td colspan="2" class="danger" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">পূর্বের মউজুদ</td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_shukhi")? jsonObj.getInt("previous_stock_shukhi"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_apon")? jsonObj.getInt("previous_stock_apon"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" ><%= jsonObj.has("previous_stock_condom")? jsonObj.getInt("previous_stock_condom"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_viale")? jsonObj.getInt("previous_stock_viale"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" ><%= jsonObj.has("previous_stock_syringe")? jsonObj.getInt("previous_stock_syringe"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_iud")? jsonObj.getInt("previous_stock_iud"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_implanon")? jsonObj.getInt("previous_stock_implanon"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_jadelle")? jsonObj.getInt("previous_stock_jadelle"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" ><%= jsonObj.has("previous_stock_ecp")? jsonObj.getInt("previous_stock_ecp"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_misoprostol")? jsonObj.getInt("previous_stock_misoprostol"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_mrm_pac")? jsonObj.getInt("previous_stock_mrm_pac"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_chlorehexidin")? jsonObj.getInt("previous_stock_chlorehexidin"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_mgso4")? jsonObj.getInt("previous_stock_mgso4"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_oxytocin")? jsonObj.getInt("previous_stock_oxytocin"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" ><%= jsonObj.has("previous_stock_mnp")? jsonObj.getInt("previous_stock_mnp"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_sanitary_pad")? jsonObj.getInt("previous_stock_sanitary_pad"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_mr_kit")? jsonObj.getInt("previous_stock_mr_kit"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_delivery_kit")? jsonObj.getInt("previous_stock_delivery_kit"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_stock_dds_kit")? jsonObj.getInt("previous_stock_dds_kit"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("previous_injection_antinatal")? jsonObj.getInt("previous_injection_antinatal"):0 %></td>
                <td style="border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("previous_injection_jentamysin")? jsonObj.getInt("previous_injection_jentamysin"):0 %></td>
            </tr>
            <tr class="success">
                <td colspan="2" class="danger" style="border-right: 1px solid black;border-top: 1px solid black; height:41px;">চলতি মাসে পাওয়া গেছে (+) </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_shukhi")? jsonObj.getInt("stock_in_shukhi"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_apon")? jsonObj.getInt("stock_in_apon"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_condom")? jsonObj.getInt("stock_in_condom"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_viale")? jsonObj.getInt("stock_in_viale"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_syringe")? jsonObj.getInt("stock_in_syringe"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_iud")? jsonObj.getInt("stock_in_iud"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_implanon")? jsonObj.getInt("stock_in_implanon"):"0" %></td>

                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_jadelle")? jsonObj.getInt("stock_in_jadelle"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_ecp")? jsonObj.getInt("stock_in_ecp"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_misoprostol")? jsonObj.getInt("stock_in_misoprostol"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_mrm_pac")? jsonObj.getInt("stock_in_mrm_pac"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_chlorehexidin")? jsonObj.getInt("stock_in_chlorehexidin"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_mgso4")? jsonObj.getInt("stock_in_mgso4"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_oxytocin")? jsonObj.getInt("stock_in_oxytocin"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_mnp")? jsonObj.getInt("stock_in_mnp"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_sanitary_pad")? jsonObj.getInt("stock_in_sanitary_pad"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_mr_kit")? jsonObj.getInt("stock_in_mr_kit"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_delivery_kit")? jsonObj.getInt("stock_in_delivery_kit"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_in_dds_kit")? jsonObj.getInt("stock_in_dds_kit"):"0" %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black; height:19px;"><%= jsonObj.has("stock_injection_antinatal")? jsonObj.getInt("stock_injection_antinatal"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("stock_injection_jentamysin")? jsonObj.getInt("stock_injection_jentamysin"):"0" %></td>
            </tr>
            <tr>
                <td colspan="2" class="danger" style="border-right: 1px solid black;border-top: 1px solid black;border-bottom: 1px solid black; height:19px;">চলতি মাসের মোট মউজুদ </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_shukhi")? jsonObj.getInt("previous_stock_shukhi"):0) + (jsonObj.has("stock_in_shukhi")? jsonObj.getInt("stock_in_shukhi"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_apon")? jsonObj.getInt("previous_stock_apon"):0) + (jsonObj.has("stock_in_apon")? jsonObj.getInt("stock_in_apon"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_condom")? jsonObj.getInt("previous_stock_condom"):0) + (jsonObj.has("stock_in_condom")? jsonObj.getInt("stock_in_condom"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_viale")? jsonObj.getInt("previous_stock_viale"):0) + (jsonObj.has("stock_in_viale")? jsonObj.getInt("stock_in_viale"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_syringe")? jsonObj.getInt("previous_stock_syringe"):0) + (jsonObj.has("stock_in_syringe")? jsonObj.getInt("stock_in_syringe"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_iud")? jsonObj.getInt("previous_stock_iud"):0) + (jsonObj.has("stock_in_iud")? jsonObj.getInt("stock_in_iud"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_implanon")? jsonObj.getInt("previous_stock_implanon"):0) + (jsonObj.has("stock_in_implanon")? jsonObj.getInt("stock_in_implanon"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_jadelle")? jsonObj.getInt("previous_stock_jadelle"):0) + (jsonObj.has("stock_in_jadelle")? jsonObj.getInt("stock_in_jadelle"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_ecp")? jsonObj.getInt("previous_stock_ecp"):0) + (jsonObj.has("stock_in_ecp")? jsonObj.getInt("stock_in_ecp"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_misoprostol")? jsonObj.getInt("previous_stock_misoprostol"):0) + (jsonObj.has("stock_in_misoprostol")? jsonObj.getInt("stock_in_misoprostol"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_mrm_pac")? jsonObj.getInt("previous_stock_mrm_pac"):0) + (jsonObj.has("stock_in_mrm_pac")? jsonObj.getInt("stock_in_mrm_pac"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_chlorehexidin")? jsonObj.getInt("previous_stock_chlorehexidin"):0) + (jsonObj.has("stock_in_chlorehexidin")? jsonObj.getInt("stock_in_chlorehexidin"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_mgso4")? jsonObj.getInt("previous_stock_mgso4"):0) + (jsonObj.has("stock_in_mgso4")? jsonObj.getInt("stock_in_mgso4"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_oxytocin")? jsonObj.getInt("previous_stock_oxytocin"):0) + (jsonObj.has("stock_in_oxytocin")? jsonObj.getInt("stock_in_oxytocin"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_mnp")? jsonObj.getInt("previous_stock_mnp"):0) + (jsonObj.has("stock_in_mnp")? jsonObj.getInt("stock_in_mnp"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_sanitary_pad")? jsonObj.getInt("previous_stock_sanitary_pad"):0) + (jsonObj.has("stock_in_sanitary_pad")? jsonObj.getInt("stock_in_sanitary_pad"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_mr_kit")? jsonObj.getInt("previous_stock_mr_kit"):0) + (jsonObj.has("stock_in_mr_kit")? jsonObj.getInt("stock_in_mr_kit"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_delivery_kit")? jsonObj.getInt("previous_stock_delivery_kit"):0) + (jsonObj.has("stock_in_delivery_kit")? jsonObj.getInt("stock_in_delivery_kit"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" ><%= (jsonObj.has("previous_stock_dds_kit")? jsonObj.getInt("previous_stock_dds_kit"):0) + (jsonObj.has("stock_in_dds_kit")? jsonObj.getInt("stock_in_dds_kit"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_injection_antinatal")? jsonObj.getInt("previous_injection_antinatal"):0) + (jsonObj.has("stock_injection_antinatal")? jsonObj.getInt("stock_injection_antinatal"):0) %></td>
                <td style="border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_injection_jentamysin")? jsonObj.getInt("previous_injection_jentamysin"):0) + (jsonObj.has("stock_injection_jentamysin")? jsonObj.getInt("stock_injection_jentamysin"):0) %></td>
            </tr>
            <tr>

                <td width="46" rowspan="2" class="danger" style="border-top: 1px solid black;border-right: 1px solid black;">সমন্বয় 	</td>
                <td width="26" class="danger" style="border-top: 1px solid black;border-right: 1px solid black;">(+)</td>

                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_shukhi")? jsonObj.getInt("adjustment_stock_plus_shukhi"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_apon")? jsonObj.getInt("adjustment_stock_plus_apon"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_condom")? jsonObj.getInt("adjustment_stock_plus_condom"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_viale")? jsonObj.getInt("adjustment_stock_plus_viale"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_syringe")? jsonObj.getInt("adjustment_stock_plus_syringe"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_iud")? jsonObj.getInt("adjustment_stock_plus_iud"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_implanon")? jsonObj.getInt("adjustment_stock_plus_implanon"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_jadelle")? jsonObj.getInt("adjustment_stock_plus_jadelle"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_ecp")? jsonObj.getInt("adjustment_stock_plus_ecp"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_misoprostol")? jsonObj.getInt("adjustment_stock_plus_misoprostol"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_mrm_pac")? jsonObj.getInt("adjustment_stock_plus_mrm_pac"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_chlorehexidin")? jsonObj.getInt("adjustment_stock_plus_chlorehexidin"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_mgso4")? jsonObj.getInt("adjustment_stock_plus_mgso4"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_oxytocin")? jsonObj.getInt("adjustment_stock_plus_oxytocin"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_mnp")? jsonObj.getInt("adjustment_stock_plus_mnp"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_sanitary_pad")? jsonObj.getInt("adjustment_stock_plus_sanitary_pad"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_mr_kit")? jsonObj.getInt("adjustment_stock_plus_mr_kit"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_delivery_kit")? jsonObj.getInt("adjustment_stock_plus_delivery_kit"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_stock_plus_dds_kit")? jsonObj.getInt("adjustment_stock_plus_dds_kit"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjust_stock_injection_plus_antinatal")? jsonObj.getInt("adjust_stock_injection_plus_antinatal"):"0" %></td>
                <td style="border-top: 1px solid black; height:19px;" class="success"><%= jsonObj.has("adjustment_injection_plus_jentamysin")? jsonObj.getInt("adjustment_injection_plus_jentamysin"):"0" %></td>
            </tr>
            <tr>
              <td class="danger" style="border-top: 1px solid black;border-right: 1px solid black;">(-)</td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_shukhi")? jsonObj.getInt("adjustment_stock_minus_shukhi"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_apon")? jsonObj.getInt("adjustment_stock_minus_apon"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_condom")? jsonObj.getInt("adjustment_stock_minus_condom"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_viale")? jsonObj.getInt("adjustment_stock_minus_viale"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_syringe")? jsonObj.getInt("adjustment_stock_minus_syringe"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_iud")? jsonObj.getInt("adjustment_stock_minus_iud"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_implanon")? jsonObj.getInt("adjustment_stock_minus_implanon"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_jadelle")? jsonObj.getInt("adjustment_stock_minus_jadelle"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_ecp")? jsonObj.getInt("adjustment_stock_minus_ecp"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_misoprostol")? jsonObj.getInt("adjustment_stock_minus_misoprostol"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_mrm_pac")? jsonObj.getInt("adjustment_stock_minus_mrm_pac"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_chlorehexidin")? jsonObj.getInt("adjustment_stock_minus_chlorehexidin"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_mgso4")? jsonObj.getInt("adjustment_stock_minus_mgso4"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_oxytocin")? jsonObj.getInt("adjustment_stock_minus_oxytocin"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_mnp")? jsonObj.getInt("adjustment_stock_minus_mnp"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_sanitary_pad")? jsonObj.getInt("adjustment_stock_minus_sanitary_pad"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_mr_kit")? jsonObj.getInt("adjustment_stock_minus_mr_kit"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_delivery_kit")? jsonObj.getInt("adjustment_stock_minus_delivery_kit"):"0" %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_stock_minus_dds_kit")? jsonObj.getInt("adjustment_stock_minus_dds_kit"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjust_stock_injection_minus_antinatal")? jsonObj.getInt("adjust_stock_injection_minus_antinatal"):0 %></td>
                <td style="border-top: 1px solid black; height:19px;"><%= jsonObj.has("adjustment_injection_minus_jentamysin")? jsonObj.getInt("adjustment_injection_minus_jentamysin"):0 %></td>
            </tr>
            <tr>
                <td colspan="2" class="danger" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">সর্বমোট </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= (jsonObj.has("previous_stock_shukhi")? jsonObj.getInt("previous_stock_shukhi"):0) + (jsonObj.has("stock_in_shukhi")? jsonObj.getInt("stock_in_shukhi"):0) + (jsonObj.has("adjustment_stock_plus_shukhi")? jsonObj.getInt("adjustment_stock_plus_shukhi"):0) - (jsonObj.has("adjustment_stock_minus_shukhi")? jsonObj.getInt("adjustment_stock_minus_shukhi"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_apon")? jsonObj.getInt("previous_stock_apon"):0) + (jsonObj.has("stock_in_apon")? jsonObj.getInt("stock_in_apon"):0) + (jsonObj.has("adjustment_stock_plus_apon")? jsonObj.getInt("adjustment_stock_plus_apon"):0) - (jsonObj.has("adjustment_stock_minus_apon")? jsonObj.getInt("adjustment_stock_minus_apon"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_condom")? jsonObj.getInt("previous_stock_condom"):0) + (jsonObj.has("stock_in_condom")? jsonObj.getInt("stock_in_condom"):0) + (jsonObj.has("adjustment_stock_plus_condom")? jsonObj.getInt("adjustment_stock_plus_condom"):0) - (jsonObj.has("adjustment_stock_minus_condom")? jsonObj.getInt("adjustment_stock_minus_condom"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_viale")? jsonObj.getInt("previous_stock_viale"):0) + (jsonObj.has("stock_in_viale")? jsonObj.getInt("stock_in_viale"):0) + (jsonObj.has("adjustment_stock_plus_viale")? jsonObj.getInt("adjustment_stock_plus_viale"):0) - (jsonObj.has("adjustment_stock_minus_viale")? jsonObj.getInt("adjustment_stock_minus_viale"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_syringe")? jsonObj.getInt("previous_stock_syringe"):0) + (jsonObj.has("stock_in_syringe")? jsonObj.getInt("stock_in_syringe"):0) + (jsonObj.has("adjustment_stock_plus_syringe")? jsonObj.getInt("adjustment_stock_plus_syringe"):0) - (jsonObj.has("adjustment_stock_minus_syringe")? jsonObj.getInt("adjustment_stock_minus_syringe"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_iud")? jsonObj.getInt("previous_stock_iud"):0) + (jsonObj.has("stock_in_iud")? jsonObj.getInt("stock_in_iud"):0) + (jsonObj.has("adjustment_stock_plus_iud")? jsonObj.getInt("adjustment_stock_plus_iud"):0) - (jsonObj.has("adjustment_stock_minus_iud")? jsonObj.getInt("adjustment_stock_minus_iud"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_implanon")? jsonObj.getInt("previous_stock_implanon"):0) + (jsonObj.has("stock_in_implanon")? jsonObj.getInt("stock_in_implanon"):0) + (jsonObj.has("adjustment_stock_plus_implanon")? jsonObj.getInt("adjustment_stock_plus_implanon"):0) - (jsonObj.has("adjustment_stock_minus_implanon")? jsonObj.getInt("adjustment_stock_minus_implanon"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= (jsonObj.has("previous_stock_jadelle")? jsonObj.getInt("previous_stock_jadelle"):0) + (jsonObj.has("stock_in_jadelle")? jsonObj.getInt("stock_in_jadelle"):0) + (jsonObj.has("adjustment_stock_plus_jadelle")? jsonObj.getInt("adjustment_stock_plus_jadelle"):0) - (jsonObj.has("adjustment_stock_minus_jadelle")? jsonObj.getInt("adjustment_stock_minus_jadelle"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_ecp")? jsonObj.getInt("previous_stock_ecp"):0) + (jsonObj.has("stock_in_ecp")? jsonObj.getInt("stock_in_ecp"):0) + (jsonObj.has("adjustment_stock_plus_ecp")? jsonObj.getInt("adjustment_stock_plus_ecp"):0) - (jsonObj.has("adjustment_stock_minus_ecp")? jsonObj.getInt("adjustment_stock_minus_ecp"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_misoprostol")? jsonObj.getInt("previous_stock_misoprostol"):0) + (jsonObj.has("stock_in_misoprostol")? jsonObj.getInt("stock_in_misoprostol"):0) + (jsonObj.has("adjustment_stock_plus_misoprostol")? jsonObj.getInt("adjustment_stock_plus_misoprostol"):0) - (jsonObj.has("adjustment_stock_minus_misoprostol")? jsonObj.getInt("adjustment_stock_minus_misoprostol"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_mrm_pac")? jsonObj.getInt("previous_stock_mrm_pac"):0) + (jsonObj.has("stock_in_mrm_pac")? jsonObj.getInt("stock_in_mrm_pac"):0) + (jsonObj.has("adjustment_stock_plus_mrm_pac")? jsonObj.getInt("adjustment_stock_plus_mrm_pac"):0) - (jsonObj.has("adjustment_stock_minus_mrm_pac")? jsonObj.getInt("adjustment_stock_minus_mrm_pac"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_chlorehexidin")? jsonObj.getInt("previous_stock_chlorehexidin"):0) + (jsonObj.has("stock_in_chlorehexidin")? jsonObj.getInt("stock_in_chlorehexidin"):0) + (jsonObj.has("adjustment_stock_plus_chlorehexidin")? jsonObj.getInt("adjustment_stock_plus_chlorehexidin"):0) - (jsonObj.has("adjustment_stock_minus_chlorehexidin")? jsonObj.getInt("adjustment_stock_minus_chlorehexidin"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= (jsonObj.has("previous_stock_mgso4")? jsonObj.getInt("previous_stock_mgso4"):0) + (jsonObj.has("stock_in_mgso4")? jsonObj.getInt("stock_in_mgso4"):0) + (jsonObj.has("adjustment_stock_plus_mgso4")? jsonObj.getInt("adjustment_stock_plus_mgso4"):0) - (jsonObj.has("adjustment_stock_minus_mgso4")? jsonObj.getInt("adjustment_stock_minus_mgso4"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_oxytocin")? jsonObj.getInt("previous_stock_oxytocin"):0) + (jsonObj.has("stock_in_oxytocin")? jsonObj.getInt("stock_in_oxytocin"):0) + (jsonObj.has("adjustment_stock_plus_oxytocin")? jsonObj.getInt("adjustment_stock_plus_oxytocin"):0) - (jsonObj.has("adjustment_stock_minus_oxytocin")? jsonObj.getInt("adjustment_stock_minus_oxytocin"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_mnp")? jsonObj.getInt("previous_stock_mnp"):0) + (jsonObj.has("stock_in_mnp")? jsonObj.getInt("stock_in_mnp"):0) + (jsonObj.has("adjustment_stock_plus_mnp")? jsonObj.getInt("adjustment_stock_plus_mnp"):0) - (jsonObj.has("adjustment_stock_minus_mnp")? jsonObj.getInt("adjustment_stock_minus_mnp"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= (jsonObj.has("previous_stock_sanitary_pad")? jsonObj.getInt("previous_stock_sanitary_pad"):0) + (jsonObj.has("stock_in_sanitary_pad")? jsonObj.getInt("stock_in_sanitary_pad"):0) + (jsonObj.has("adjustment_stock_plus_sanitary_pad")? jsonObj.getInt("adjustment_stock_plus_sanitary_pad"):0) - (jsonObj.has("adjustment_stock_minus_sanitary_pad")? jsonObj.getInt("adjustment_stock_minus_sanitary_pad"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_mr_kit")? jsonObj.getInt("previous_stock_mr_kit"):0) + (jsonObj.has("stock_in_mr_kit")? jsonObj.getInt("stock_in_mr_kit"):0) + (jsonObj.has("adjustment_stock_plus_mr_kit")? jsonObj.getInt("adjustment_stock_plus_mr_kit"):0) - (jsonObj.has("adjustment_stock_minus_mr_kit")? jsonObj.getInt("adjustment_stock_minus_mr_kit"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_delivery_kit")? jsonObj.getInt("previous_stock_delivery_kit"):0) + (jsonObj.has("stock_in_delivery_kit")? jsonObj.getInt("stock_in_delivery_kit"):0) + (jsonObj.has("adjustment_stock_plus_delivery_kit")? jsonObj.getInt("adjustment_stock_plus_delivery_kit"):0) - (jsonObj.has("adjustment_stock_minus_delivery_kit")? jsonObj.getInt("adjustment_stock_minus_delivery_kit"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success">
                    <%= (jsonObj.has("previous_stock_dds_kit")? jsonObj.getInt("previous_stock_dds_kit"):0) + (jsonObj.has("stock_in_dds_kit")? jsonObj.getInt("stock_in_dds_kit"):0) + (jsonObj.has("adjustment_stock_plus_dds_kit")? jsonObj.getInt("adjustment_stock_plus_dds_kit"):0) - (jsonObj.has("adjustment_stock_minus_dds_kit")? jsonObj.getInt("adjustment_stock_minus_dds_kit"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" class="success"><%= (jsonObj.has("previous_injection_antinatal")? jsonObj.getInt("previous_injection_antinatal"):0) + (jsonObj.has("stock_injection_antinatal")? jsonObj.getInt("stock_injection_antinatal"):0) + (jsonObj.has("adjust_stock_injection_plus_antinatal")? jsonObj.getInt("adjust_stock_injection_plus_antinatal"):0) - (jsonObj.has("adjust_stock_injection_minus_antinatal")? jsonObj.getInt("adjust_stock_injection_minus_antinatal"):0) %></td>
                <td style="border-top: 1px solid black; height:19px;" class="success">
				<%= (jsonObj.has("previous_injection_jentamysin")? jsonObj.getInt("previous_injection_jentamysin"):0) + (jsonObj.has("stock_injection_jentamysin")? jsonObj.getInt("stock_injection_jentamysin"):0) + (jsonObj.has("adjustment_injection_plus_jentamysin")? jsonObj.getInt("adjustment_injection_plus_jentamysin"):0) - (jsonObj.has("adjustment_injection_minus_jentamysin")? jsonObj.getInt("adjustment_injection_minus_jentamysin"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" class="danger" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">চলতি মাসে বিতরন করা হয়েছে (-) </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= jsonObj.has("pill_shukhi_total")? jsonObj.getInt("pill_shukhi_total"):0 %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= jsonObj.has("pill_apon_total")? jsonObj.getInt("pill_apon_total"):0 %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= jsonObj.has("condom_total")? jsonObj.getInt("condom_total"):0 %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= current_month_vial %>  </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= current_month_vial %>
				</td>

                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= jsonObj.getInt("iud_normal_center") + jsonObj.getInt("iud_normal_satelite") + jsonObj.getInt("iud_after_delivery_center") + jsonObj.getInt("iud_after_delivery_satelite") %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= jsonObj.getInt("implant_implanon") %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= jsonObj.getInt("implant_jadelle") %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("ecp_taken_center")? jsonObj.getInt("ecp_taken_center"):0) + (jsonObj.has("ecp_taken_satelite")? jsonObj.getInt("ecp_taken_satelite"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("ancmisoprostolcenter")? jsonObj.getInt("ancmisoprostolcenter"):0) + (jsonObj.has("ancmisoprostolsatelite")? jsonObj.getInt("ancmisoprostolsatelite"):0) + (jsonObj.has("deliverymisoprostol")? jsonObj.getInt("deliverymisoprostol"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
				<%= (jsonObj.has("cmonth_distribution_mrm_pac")? jsonObj.getInt("cmonth_distribution_mrm_pac"):0)%></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("chlorehexidin")? jsonObj.getInt("chlorehexidin"):0)%></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("ancmgso4center")? jsonObj.getInt("ancmgso4center"):0) +
                        (jsonObj.has("ancmgso4satelite")? jsonObj.getInt("ancmgso4satelite"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("deliverycxytocin")? jsonObj.getInt("deliverycxytocin"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("cmonth_distribution_mnp")? jsonObj.getInt("cmonth_distribution_mnp"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("sanitary_pad")? jsonObj.getInt("sanitary_pad"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("cmonth_distribution_mrm_kit")? jsonObj.getInt("cmonth_distribution_mrm_kit"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("cmonth_distribution_delivery_kit")? jsonObj.getInt("cmonth_distribution_delivery_kit"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("cmonth_distribution_dds_kit")? jsonObj.getInt("cmonth_distribution_dds_kit"):0) %></td>
                <td style="border-top: 1px solid black;border-right: 1px solid black; height:19px;">&nbsp;</td>
                <td style="border-top: 1px solid black; height:19px;"><%= (jsonObj.has("cmonth_distribution_injection_jentamysin")? jsonObj.getInt("cmonth_distribution_injection_jentamysin"):0) %></td>
            </tr>
            <tr class="success">

                <td colspan="2" class="danger" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">অবশিষ্ট </td>

                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
				<%= (jsonObj.has("previous_stock_shukhi")? jsonObj.getInt("previous_stock_shukhi"):0) + (jsonObj.has("stock_in_shukhi")? jsonObj.getInt("stock_in_shukhi"):0) + (jsonObj.has("adjustment_stock_plus_shukhi")? jsonObj.getInt("adjustment_stock_plus_shukhi"):0) - (jsonObj.has("adjustment_stock_minus_shukhi")? jsonObj.getInt("adjustment_stock_minus_shukhi"):0) - jsonObj.getInt("pill_shukhi_total") %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_apon")? jsonObj.getInt("previous_stock_apon"):0) + (jsonObj.has("stock_in_apon")? jsonObj.getInt("stock_in_apon"):0) + (jsonObj.has("adjustment_stock_plus_apon")? jsonObj.getInt("adjustment_stock_plus_apon"):0) - (jsonObj.has("adjustment_stock_minus_apon")? jsonObj.getInt("adjustment_stock_minus_apon"):0) - jsonObj.getInt("pill_apon_total") %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_condom")? jsonObj.getInt("previous_stock_condom"):0) + (jsonObj.has("stock_in_condom")? jsonObj.getInt("stock_in_condom"):0) + (jsonObj.has("adjustment_stock_plus_condom")? jsonObj.getInt("adjustment_stock_plus_condom"):0) - (jsonObj.has("adjustment_stock_minus_condom")? jsonObj.getInt("adjustment_stock_minus_condom"):0) - jsonObj.getInt("condom_total") %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= adjustment_vial%>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= adjusted_syringe %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_iud")? jsonObj.getInt("previous_stock_iud"):0) + (jsonObj.has("stock_in_iud")? jsonObj.getInt("stock_in_iud"):0) + (jsonObj.has("adjustment_stock_plus_iud")? jsonObj.getInt("adjustment_stock_plus_iud"):0) - (jsonObj.has("adjustment_stock_minus_iud")? jsonObj.getInt("adjustment_stock_minus_iud"):0) - (jsonObj.has("iud_normal_center")? jsonObj.getInt("iud_normal_center"):0)- (jsonObj.has("iud_normal_satelite")? jsonObj.getInt("iud_normal_satelite"):0) - (jsonObj.has("iud_after_delivery_center")? jsonObj.getInt("iud_after_delivery_center"):0) - (jsonObj.has("iud_after_delivery_satelite")? jsonObj.getInt("iud_after_delivery_satelite"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_implanon")? jsonObj.getInt("previous_stock_implanon"):0) + (jsonObj.has("stock_in_implanon")? jsonObj.getInt("stock_in_implanon"):0) + (jsonObj.has("adjustment_stock_plus_implanon")? jsonObj.getInt("adjustment_stock_plus_implanon"):0) - (jsonObj.has("adjustment_stock_minus_implanon")? jsonObj.getInt("adjustment_stock_minus_implanon"):0) - (jsonObj.has("implant_implanon")? jsonObj.getInt("implant_implanon"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;" >
                    <%= (jsonObj.has("previous_stock_jadelle")? jsonObj.getInt("previous_stock_jadelle"):0) + (jsonObj.has("stock_in_jadelle")? jsonObj.getInt("stock_in_jadelle"):0) + (jsonObj.has("adjustment_stock_plus_jadelle")? jsonObj.getInt("adjustment_stock_plus_jadelle"):0) - (jsonObj.has("adjustment_stock_minus_jadelle")? jsonObj.getInt("adjustment_stock_minus_jadelle"):0) - (jsonObj.has("implant_jadelle")? jsonObj.getInt("implant_jadelle"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_ecp")? jsonObj.getInt("previous_stock_ecp"):0) + (jsonObj.has("stock_in_ecp")? jsonObj.getInt("stock_in_ecp"):0) + (jsonObj.has("adjustment_stock_plus_ecp")? jsonObj.getInt("adjustment_stock_plus_ecp"):0) - (jsonObj.has("adjustment_stock_minus_ecp")? jsonObj.getInt("adjustment_stock_minus_ecp"):0) - (jsonObj.has("ecp_taken_center")? jsonObj.getInt("ecp_taken_center"):0)-(jsonObj.has("ecp_taken_satelite")? jsonObj.getInt("ecp_taken_satelite"):0) %> </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_misoprostol")? jsonObj.getInt("previous_stock_misoprostol"):0) + (jsonObj.has("stock_in_misoprostol")? jsonObj.getInt("stock_in_misoprostol"):0) + (jsonObj.has("adjustment_stock_plus_misoprostol")? jsonObj.getInt("adjustment_stock_plus_misoprostol"):0) - (jsonObj.has("adjustment_stock_minus_misoprostol")? jsonObj.getInt("adjustment_stock_minus_misoprostol"):0) - (jsonObj.has("ancmisoprostolcenter")? jsonObj.getInt("ancmisoprostolcenter"):0) -(jsonObj.has("ancmisoprostolsatelite")? jsonObj.getInt("ancmisoprostolsatelite"):0)-(jsonObj.has("deliverymisoprostol")? jsonObj.getInt("deliverymisoprostol"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_mrm_pac")? jsonObj.getInt("previous_stock_mrm_pac"):0) + (jsonObj.has("stock_in_mrm_pac")? jsonObj.getInt("stock_in_mrm_pac"):0) + (jsonObj.has("adjustment_stock_plus_mrm_pac")? jsonObj.getInt("adjustment_stock_plus_mrm_pac"):0) - (jsonObj.has("adjustment_stock_minus_mrm_pac")? jsonObj.getInt("adjustment_stock_minus_mrm_pac"):0) - (jsonObj.has("cmonth_distribution_mrm_pac")? jsonObj.getInt("cmonth_distribution_mrm_pac"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_chlorehexidin")? jsonObj.getInt("previous_stock_chlorehexidin"):0) + (jsonObj.has("stock_in_chlorehexidin")? jsonObj.getInt("stock_in_chlorehexidin"):0) + (jsonObj.has("adjustment_stock_plus_chlorehexidin")? jsonObj.getInt("adjustment_stock_plus_chlorehexidin"):0) - (jsonObj.has("adjustment_stock_minus_chlorehexidin")? jsonObj.getInt("adjustment_stock_minus_chlorehexidin"):0) - (jsonObj.has("chlorehexidin")? jsonObj.getInt("chlorehexidin"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_mgso4")? jsonObj.getInt("previous_stock_mgso4"):0) + (jsonObj.has("stock_in_mgso4")? jsonObj.getInt("stock_in_mgso4"):0) + (jsonObj.has("adjustment_stock_plus_mgso4")? jsonObj.getInt("adjustment_stock_plus_mgso4"):0) - (jsonObj.has("adjustment_stock_minus_mgso4")? jsonObj.getInt("adjustment_stock_minus_mgso4"):0) - (jsonObj.has("ancmgso4center")? jsonObj.getInt("ancmgso4center"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_oxytocin")? jsonObj.getInt("previous_stock_oxytocin"):0) + (jsonObj.has("stock_in_oxytocin")? jsonObj.getInt("stock_in_oxytocin"):0) + (jsonObj.has("adjustment_stock_plus_oxytocin")? jsonObj.getInt("adjustment_stock_plus_oxytocin"):0) - (jsonObj.has("adjustment_stock_minus_oxytocin")? jsonObj.getInt("adjustment_stock_minus_oxytocin"):0) - (jsonObj.has("deliverycxytocin")? jsonObj.getInt("deliverycxytocin"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_mnp")? jsonObj.getInt("previous_stock_mnp"):0) + (jsonObj.has("stock_in_mnp")? jsonObj.getInt("stock_in_mnp"):0) + (jsonObj.has("adjustment_stock_plus_mnp")? jsonObj.getInt("adjustment_stock_plus_mnp"):0) - (jsonObj.has("adjustment_stock_minus_mnp")? jsonObj.getInt("adjustment_stock_minus_mnp"):0) - (jsonObj.has("cmonth_distribution_mnp")? jsonObj.getInt("cmonth_distribution_mnp"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_sanitary_pad")? jsonObj.getInt("previous_stock_sanitary_pad"):0) + (jsonObj.has("stock_in_sanitary_pad")? jsonObj.getInt("stock_in_sanitary_pad"):0) + (jsonObj.has("adjustment_stock_plus_sanitary_pad")? jsonObj.getInt("adjustment_stock_plus_sanitary_pad"):0) - (jsonObj.has("adjustment_stock_minus_sanitary_pad")? jsonObj.getInt("adjustment_stock_minus_sanitary_pad"):0) - (jsonObj.has("sanitary_pad")? jsonObj.getInt("sanitary_pad"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_stock_mr_kit")? jsonObj.getInt("previous_stock_mr_kit"):0) + (jsonObj.has("stock_in_mr_kit")? jsonObj.getInt("stock_in_mr_kit"):0) + (jsonObj.has("adjustment_stock_plus_mr_kit")? jsonObj.getInt("adjustment_stock_plus_mr_kit"):0) - (jsonObj.has("adjustment_stock_minus_mr_kit")? jsonObj.getInt("adjustment_stock_minus_mr_kit"):0) - (jsonObj.has("cmonth_distribution_mrm_kit")? jsonObj.getInt("cmonth_distribution_mrm_kit"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_delivery_kit")? jsonObj.getInt("previous_stock_delivery_kit"):0) + (jsonObj.has("stock_in_delivery_kit")? jsonObj.getInt("stock_in_delivery_kit"):0) + (jsonObj.has("adjustment_stock_plus_delivery_kit")? jsonObj.getInt("adjustment_stock_plus_delivery_kit"):0) - (jsonObj.has("adjustment_stock_minus_delivery_kit")? jsonObj.getInt("adjustment_stock_minus_delivery_kit"):0) - (jsonObj.has("cmonth_distribution_delivery_kit")? jsonObj.getInt("cmonth_distribution_delivery_kit"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
                    <%= (jsonObj.has("previous_stock_dds_kit")? jsonObj.getInt("previous_stock_dds_kit"):0) + (jsonObj.has("stock_in_dds_kit")? jsonObj.getInt("stock_in_dds_kit"):0) + (jsonObj.has("adjustment_stock_plus_dds_kit")? jsonObj.getInt("adjustment_stock_plus_dds_kit"):0) - (jsonObj.has("adjustment_stock_minus_dds_kit")? jsonObj.getInt("adjustment_stock_minus_dds_kit"):0) - (jsonObj.has("cmonth_distribution_dds_kit")? jsonObj.getInt("cmonth_distribution_dds_kit"):0) %>                </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">
				<%= (jsonObj.has("previous_injection_antinatal")? jsonObj.getInt("previous_injection_antinatal"):0) + (jsonObj.has("stock_injection_antinatal")? jsonObj.getInt("stock_injection_antinatal"):0) + (jsonObj.has("adjust_stock_injection_plus_antinatal")? jsonObj.getInt("adjust_stock_injection_plus_antinatal"):0) - (jsonObj.has("adjust_stock_injection_minus_antinatal")? jsonObj.getInt("adjust_stock_injection_minus_antinatal"):0) %>
				
				</td>
                <td style="border-top: 1px solid black; height:19px;"><%= (jsonObj.has("previous_injection_jentamysin")? jsonObj.getInt("previous_injection_jentamysin"):0) + (jsonObj.has("stock_injection_jentamysin")? jsonObj.getInt("stock_injection_jentamysin"):0) + (jsonObj.has("adjustment_injection_plus_jentamysin")? jsonObj.getInt("adjustment_injection_plus_jentamysin"):0) - (jsonObj.has("adjustment_injection_minus_jentamysin")? jsonObj.getInt("adjustment_injection_minus_jentamysin"):0) - (jsonObj.has("cmonth_distribution_injection_jentamysin")? jsonObj.getInt("cmonth_distribution_injection_jentamysin"):0) %></td>
            </tr>
            <tr>
                <td colspan="2" class="danger" style="border-right: 1px solid black;border-top: 1px solid black; height:19px;">চলতি মাসে কখন ও মউজুদ শূন্য হয়ে থাকলে(কোড) </td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_shukhi")? jsonObj.getInt("zero_stock_code_shukhi"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_apon")? jsonObj.getInt("zero_stock_code_apon"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_condom")? jsonObj.getInt("zero_stock_code_condom"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_viale")? jsonObj.getInt("zero_stock_code_viale"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_syringe")? jsonObj.getInt("zero_stock_code_syringe"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_iud")? jsonObj.getInt("zero_stock_code_iud"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_iud")? jsonObj.getInt("zero_stock_code_iud"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_implanon")? jsonObj.getInt("zero_stock_code_implanon"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_jadelle")? jsonObj.getInt("zero_stock_code_jadelle"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_ecp")? jsonObj.getInt("zero_stock_code_ecp"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_misoprostol")? jsonObj.getInt("zero_stock_code_misoprostol"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_mrm_pac")? jsonObj.getInt("zero_stock_code_mrm_pac"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_chlorehexidin")? jsonObj.getInt("zero_stock_code_chlorehexidin"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_mgso4")? jsonObj.getInt("zero_stock_code_mgso4"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_oxytocin")? jsonObj.getInt("zero_stock_code_oxytocin"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_mnp")? jsonObj.getInt("zero_stock_code_mnp"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_sanitary_pad")? jsonObj.getInt("zero_stock_code_sanitary_pad"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_mr_kit")? jsonObj.getInt("zero_stock_code_mr_kit"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_delivery_kit")? jsonObj.getInt("zero_stock_code_delivery_kit"):0) %></td>
                <td style="border-right: 1px solid black;border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_code_dds_kit")? jsonObj.getInt("zero_stock_code_dds_kit"):0) %></td>
                <td style="border-top: 1px solid black; height:19px;"><%= (jsonObj.has("zero_stock_injection_jentamysin")? jsonObj.getInt("zero_stock_injection_jentamysin"):0) %></td>
            </tr>
      </table>
        </td>
        </tr>
        </table>

        <table width="749" style="font-size:11px;display:none;" class="table page_5 table_hide" id="table_14">

            <tr>
                <td width="160">মউজুদ শূন্যূতার কোড : </td>
                <td width="42">ক</td>
                <td width="117"> সরবরাহ পাওয়া যায়নি </td>
                <td width="26">খ</td>
                <td width="113"> <p>অপর্যাপ্ত সরবরাহ</p></td>
                <td width="30">গ</td>
                <td width="136">হঠাৎ চাহিদা বৃদ্ধি পাওয়া </td>
                <td width="28">ঘ</td>
                <td width="57">আন্যান্য</td>
            </tr>
        </table>
        <p>&nbsp;</p>

        <table style="font-size:11px;display:none;" width="748" class="table page_5 table_hide" id="table_15">

            <tr>
                <td width="166" rowspan="3" valign="top"><div align="center">তারিখঃ </div></td>
                <td width="433">স্বাক্ষর: </td>
                <td width="133"><%= (jsonObj.has("signature")? jsonObj.getString("signature"):"") %></td>
            </tr>
            <tr>
                <td>নামঃ </td>
                <td><%= (jsonObj.has("name")? jsonObj.getString("name"):"") %></td>
            </tr>
            <tr>
                <td><p>MO (Clinic)/MO(FW)/SACMO/FWV/দায়িত্ব প্রাপ্ত কর্মকর্তা </p>
                    (যেটি প্রযোজ্য টিকা দিন)</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
	
	
</div>
<div style='float: right;'>
	<button type="button" class="btn btn-default pre_button_new" value="1" disabled><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
	<span class="showing_page">&nbsp; &nbsp;1 of 5 &nbsp; &nbsp;</span><button type="button" class="btn btn-default next_button_new" value='2'>
	<i class="fa fa-chevron-right" aria-hidden="true"></i></button>
</div>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>

<!--<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>-->

<script>
    $(document).ready(function() {

        $(document).on("click", '#approveMIS3' ,function (){
            submitMIS3Report("1");
        });

        $(document).on("click", '#rejectMIS3' ,function (){
            submitMIS3Report("2");
        });

        function submitMIS3Report(submission_status){
            var url = "";
            var getParams = "facilityId="+$('#facilityName').val()+"&report1_zilla="+$('#report1_zilla').val()+"&report1_start_date="+$('#mis3ReportMonth').val()+"&submissionStatus="+submission_status+"&supervisorComment="+$('#supervisor_comment').val();
            var params = $.base64.encode(getParams);
            url = "MIS3Submit?"+params;

            $.ajax({
                type: "GET",
                url:url,
                timeout:60000, //60 seconds timeout
                //data:{"forminfo":JSON.stringify(forminfo)},
                success: function (response) {
                    if(response=='true'){
                        $("#model_box").dialog({
                            modal: true,
                            title: "Message",
                            width: 420,
                            open: function () {
                                if(submission_status==1)
                                    $(this).html("<b style='color:green'>Your approved this MIS3 report successfully.</b>");
                                else if(submission_status==2)
                                    $(this).html("<b style='color:red'>Your rejected this MIS3 report successfully.</b>");
                            },
                            buttons : {
                                Ok : function() {
                                    $(this).dialog('close');
                                    $("#table_append" ).empty();
                                    //reloadReport();
                                    $( ".approval1" ).trigger( "click" );
                                }
                            }
                        });
                    }
                    else{
                        $('#model_box').dialog({
                            modal: true,
                            title: "Message",
                            width: 420,
                            open: function () {
                                $(this).html("<b style='color:red'>Your MIS3 not submitted successfully. Please re-submit your MIS3.</b>");
                            },
                            buttons: {
                                Ok: function () {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    }
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
        }

        function reloadReport(){

            loadGif();


            //var preventMultiC = 1;
            var reportObj = new Object();
            reportObj.report1_zilla = $('#RzillaName').val();
            reportObj.facilityId = $('#facilityName').val();
            reportObj.supervisorId = ($("#ProviderID").text()).trim();
            reportObj.report1_month = $('#monthSelect').val();
            console.log(reportObj);
            $.ajax({
                type: "POST",
                url:"submittedmis3",
                timeout:60000, //60 seconds timeout
                data:{"approvalInfo":JSON.stringify(reportObj)},
                success: function (response) {
                    //alert(response);
                    $(".box-default").addClass("collapsed-box");
                    $(".search_form").hide();
                    $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

                    $("#reportTable").remove();
                    $("#table_row").show();
                    $("#table_append" ).html(response);

                    $("#exportOption").hide();

                    var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                    var bottom = $el.position().top + $el.outerHeight(true);
                    $(".main-sidebar").css('height',bottom+"px");
                    //alert(bottom);

                    $('#table_row')[0].scrollIntoView( true );
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
            //alert(preventMultiC);
            return preventMultiCall;
        }
    });
	
	
	
	function doExport(selector, params) {
        var options = {
            //ignoreRow: [1,11,12,-2],
            //ignoreColumn: [0,-1],
            //pdfmake: {enabled: true},
            tableName: 'MIS3 Report',
            worksheetName: 'MIS3 Report'
        };

        $.extend(true, options, params);

        //$(selector).tableExport(options);
		//$("table").tableExport(options);
		$("#DivIdToPrint").tableExport(options);
		//TableExport(document.getElementsByTagName("table"));
    }
</script>
