<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 3/7/2021
  Time: 4:01 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!--high Chart JS -->
<script type="text/javascript" src="library/highchart/v9.2.2/highcharts.js"></script>
<script type="text/javascript" src="library/highchart/v9.2.2/accessibility.js"></script>
<script type="text/javascript" src="library/highchart/v9.2.2/exporting.js"></script>
<script type="text/javascript" src="library/highchart/v9.2.2/export-data.js"></script>

<!--chart JS-->
<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title">Essential Newborn Care</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">
                    <!--geo group-->
                    <%@include file="/jsp/util/geoSection.jsp" %>

                    <!--/geo group-->
                    <!--date section-->
                    <%@include file="/jsp/util/date.jsp" %>

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>

    <script>

        $(".select2").select2();


    </script>
</div>
