<%@ page import="org.json.JSONObject" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 9/3/2021
  Time: 11:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% List<JSONObject> dataList = (List<JSONObject>) request.getAttribute("chart_data"); %>
<%--<% String[] keyArr = {"Year", "Month", "Union", "Live Birth(%)", "Chlorehexidin(%)", "Breast Feed(%)", "Skin Touch(%)"};%>--%>
<% String zilla = (String) request.getAttribute("zilla");%>
<% String upazila = (String) request.getAttribute("upazila");%>
<% String union = (String) request.getAttribute("union");%>


<%--<div class="box-body search_form table-responsive" id="shadow_table_view">--%>
<%--<div class="row">--%>
<%--<div class="col-md-12">--%>
<%--<div id ="table_container" style="display:none">--%>
<table class="table table-striped table-bordered nowrap reportExport" id="reportDataTable"
       style="display:none" cellspacing="0" width="100% ">
    <thead style="background-color: #3c8dbc;">
    <tr>

        <% if (!zilla.equals("null") && upazila.equals("null") && union.equals("null")) {%>
        <th> Division</th>
        <%}%>
        <th> Zilla</th>

        <% if (!upazila.equals("null")) {%>
        <th> Upazilla</th>
        <%}%>
        <% if (!union.equals("null")) {%>
        <th> Union</th>

        <%}%>
        <th> Live Birth</th>
        <th> Chlorehexidin</th>
        <th> Breast Feed</th>
        <th> Skin Touch</th>

    </tr>
    </thead>
    <tbody>
    <% for (JSONObject data : dataList) {
//                    Iterator<String> keys = data.keys();
//
//                        int i = 0;
//                            while (keys.hasNext()) {

//                                String key = keys.next();
//                                if (key.equals(keyArr[i])) {
//                                    i++;

    %>

    <tr>
        <%--<td><%=data.get("Year")%>--%>
        <%--</td>--%>
        <%--<td><%=data.get("month")%>--%>
        <%--</td>--%>
        <% if (!zilla.equals("null") && upazila.equals("null") && union.equals("null")) {%>
        <td><%=data.get("divisioneng")%>
        </td>
        <%}%>

        <td><%=data.get("zillanameeng")%>
        </td>
        <% if (!upazila.equals("null")) {%>
        <td><%=data.get("upazilanameeng")%>
        </td>
        <%}%>
        <% if (!union.equals("null")) {%>
        <td><%=data.get("upazilanameeng")%>
        <td><%=data.get("unionnameeng")%>
        </td>
        <%}%>
        <td><%=data.get("Livebirth")%>
        </td>

        <td><%=data.get("Chlorehexidin")%>
        </td>
        <%--<td><%= (data.has("Chlorehexidin"))?((data.get("Chlorehexidin")==null)?"0":data.get("Chlorehexidin")):0%>--%>
        </td>
        <td><%=data.get("Breastfeed")%>
        </td>
        <td><%=data.get("Skintouch") %>
        </td>


    </tr>
    <%
            //                            }

        }
    %>

    </tbody>

</table>
<%--</div>--%>
<%--</div>--%>
<%--<!-- /.row -->--%>
<%--</div>--%>


<!-- Custom TAB with data tables -->
<%@include file='../util/customDataView.jsp' %>


<!-- Custom CHARTS and DataTables's data handler -->
<script>

    function exec_graph_view() {
        var geoList = [];
        var upzList = [];
        var unionList = [];
        var livebirth = [];
        var breastfeed = [];
        var skintouch = [];
        var chlorehexidin = [];
        var dbData = <%out.print(request.getAttribute("chart_data"));%>
            dbData.forEach(function (dData, idx) {
                console.log(dData)
                console.log(dData["divisioneng"])


                // currently only checking for 0th index in the db data
                if(dData["divisioneng"]) {
                    geoList.push(dData["zillanameeng"]);

                } else if (!(dData["zillanameeng"] == null) && !(dData["upazilanameeng"] == null) && (dData["unionnameeng"] == null)) {
                    geoList.push(dData["upazilanameeng"]);

                } else if (!(dData["upazilanameeng"] == null) && !(dData["unionnameeng"] == null)) {
                    // unionList.push(dData["unionnameeng"]);
                    geoList.push(dData["unionnameeng"]);
                }

                console.log("hi "+geoList);

                if (!(dData["Livebirth"] == null))
                    livebirth.push(parseFloat(dData["Livebirth"]));
                if (!(dData["Chlorehexidin"] == null))
                    chlorehexidin.push(parseFloat(dData["Chlorehexidin"]));
                if (!(dData["Breastfeed"] == null))
                    breastfeed.push(parseFloat(dData["Breastfeed"]));
                if (!(dData["Skintouch"] == null))
                    skintouch.push(parseFloat(dData["Skintouch"]));


            });

        // if(unionList!=null)
        //     geoList = unionList;
        // if(upzList!=null) {
        //     geoList = upzList;
        //     console.log("oo"+geoList)
        // }

        Highcharts.chart('graph_container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Essential Newborn Care '
            },
            subtitle: {
                // text: 'Source: WorldClimate.com'
            },
            xAxis: {
                title: {
                    // text: '<b> Union Name </b>'
                },
                categories: geoList,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<b>Service</b>'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Live Birth',
                data: livebirth

            }, {
                name: 'Chlorehexidin',
                data: chlorehexidin

            }, {
                name: 'Breast Feed',
                data: breastfeed

            }, {
                name: 'Skin Touch',
                data: skintouch

            }]
        });
        //hide export button and credits text
        $(".highcharts-credits").hide() //.highcharts-exporting-group
    }

    function exec_table_view() {
        $(document).ready(function () {
            let table = $('#reportDataTable');
            $('#table_container').append(table);
            table.show();

            //init data-table
            table.dataTable({
                "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
                "processing": true,
                "paging": true,
                "lengthMenu": [10, 25, 50, 75, 100],
                "language": {
                    "info": "Total _TOTAL_",
                    "infoEmpty": "Total _TOTAL_",
                    "zeroRecords": "No records",
                    "lengthMenu": "Show _MENU_ entries",
                    "searchIcon": "",
                    "paginate": {
                        "first": "First",
                        "last": "Last",
                        "next": "Next",
                        "previous": "Prev"
                    }
                },
                "initComplete": function (oSettings, json) {
                    $("#reportDataTable_wrapper .dataTables_filter input")
                        .wrap('<div class="input-group"></div>')
                        .before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>')
                        .attr("placeholder", "Search");

                },
                "buttons": ['copy', {
                    extend: 'excelHtml5',
                    title: "Service Statistics"
                }, {
                    extend: 'csvHtml5',
                    title: "Service Statistics"
                }, {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    title: "Service Statistics",
                    pageSize: 'LEGAL'
                }, {
                    extend: 'print',
                    text: 'Print',
                    autoPrint: true
                }
                ]
            });
        });

        $('#reportDataTableFilter').dataTable({
            "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
            "processing": true,
            "paging": true,
            "lengthMenu": [10, 25, 50, 75, 100],
            "language": {
                "info": "Total _TOTAL_",
                "infoEmpty": "Total _TOTAL_",
                "zeroRecords": "No records",
                "lengthMenu": "Show _MENU_ entries",
                "searchIcon": "",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Prev"
                }
            },
            "initComplete": function (oSettings, json) {
                $("#reportDataTable_wrapper .dataTables_filter input")
                    .wrap('<div class="input-group"></div>')
                    .before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>')
                    .attr("placeholder", "Search");

                this.api().columns().every(function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });

            },
            "buttons": ['copy', {
                extend: 'excelHtml5',
                title: "Facility Information"
            }, {
                extend: 'csvHtml5',
                title: "Facility Information"
            }, {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title: "Facility Information"
            }, {
                extend: 'print',
                text: 'Print',
                autoPrint: true
            }
            ]
        });
    }

    // click the first tab after document load
    $(document).ready(function () {
        $('.clickable').eq("0").click();
    });
</script>


