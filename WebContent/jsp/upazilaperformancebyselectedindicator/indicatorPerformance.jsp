<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/28/2021
  Time: 4:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>


        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">

                    <!--select type-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="indType" style="display: none;">
                                <label>Indicator</label>
                                <select class="form-control select2" id="indicatorType" style="width: 100%;">
                                    <option value="">--Select Indicator Type--</option>
                                    <option value="HBP">High Blood Pressure</option>
                                    <option value="DAN">Delivery & Newborn</option>
                                    <option value="LBW">Low Birth Weight Baby</option>
                                    <% int userTypeReport = (Integer)request.getAttribute("uType"); %>
                                    <% if(userTypeReport == 999 || userTypeReport == 998 || userTypeReport == 994){ %>
                                    <option value="NRC">Service By NRC & HID</option>
                                    <% } %>
                                </select>
                            </div>
                            <!--service section-->
                            <div class="form-group col-md-3" id="serType" style="display: none;">
                                <label>Service</label>
                                <select class="form-control select2" id="serviceType" style="width: 100%;">
                                    <option value="">--Select Service Type--</option>
                                </select>
                            </div>
                            <!--view source-->
                            <div class="form-group col-md-3" id="service_by" style="display: none;">
                                <label>View Service Record By</label>
                                <select class="form-control select2" id="viewServiceBy" style="width: 100%;">
                                    <option value="">--Select--</option>
                                    <option value="1">Union Wise</option>
                                    <option value="2">Provider Wise</option>
                                </select>
                            </div>
                           <!--delivery place-->
                            <div class="form-group col-md-3" id="delivery_place" style="display: none;">
                                <label>Delivery Place</label>
                                <select class="form-control select2" id="deliveryPlace" style="width: 100%;">
                                    <option value="">ALL</option>
                                    <option value="1">Home</option>
                                    <option value="2">Facility</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/select type-->

                    <!--geo group-->
                    <%@include file="/jsp/util/geoSection.jsp" %>
                    <!--/geo group-->

                    <!--date section-->
                    <%@include file="/jsp/util/date.jsp" %>

                </div>
            </div>
            <!--row-->
        </div>
    </div>

    <!--for showing table(performance) result-->
    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>
    <!--/for showing table(performance) result-->

    <script>
        $(".select2").select2();

        serviceTypeList1 = '[{"ALL":"ALL","ANC":"ANC","PNC":"PNC","Delivery":"Delivery","PNC-N":"PNC-N","PILLCON":"Pill Condom","IUD":"IUD","IUDFOL":"IUD Followup","Implant":"Implant","IMPFOL":"Implant Followup","INJ":"Injectable","GP":"General Patient"}]';
        serviceTypeJS1 = JSON.parse(serviceTypeList1);

        serviceTypeList2 = '[{"ANC":"ANC","PNC":"PNC"}]';
        serviceTypeJS2 = JSON.parse(serviceTypeList2);

        function loadServiceTable(serviceTableJS){
            $("#serviceType").empty();
            var output = "<option value ='' selected></option>";

            $.each(serviceTableJS[0], function(key, val) {
                output = output + "<option value = " + key + ">" + val + "</option>";
            });

            $("#serviceType").append(output);
        }


        $('#indicatorType').on('change', function(){
            if($('#indicatorType').val()=="NRC")
            {
                $("#unionName").hide();
                $("#serType").show();
                $("#service_by").show();
                $("#sba_nsba").hide();
                $("#delivery_place").hide();
                loadServiceTable(serviceTypeJS1);
            }
            else if($('#indicatorType').val()=="HBP")
            {
                $("#unionName").show();
                $("#serType").show();
                $("#service_by").hide();
                $("#sba_nsba").hide();
                $("#delivery_place").hide();
                loadServiceTable(serviceTypeJS2);
            }
            else if($('#indicatorType').val()=="DAN")
            {
                $("#unionName").show();
                $("#serType").hide();
                $("#service_by").hide();
                //$("#sba_nsba").show();
                $("#delivery_place").show();
                loadServiceTable(serviceTypeJS2);
            }else if($('#indicatorType').val()=="LBW"){
                $("#unionName").show();
                $("#serType").hide();
                $("#service_by").hide();
                //$("#sba_nsba").show();
                $("#delivery_place").show();
//                loadServiceTable(serviceTypeJS2);
            }
        });

    </script>
</div>

