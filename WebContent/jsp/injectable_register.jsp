<%@ page import="org.json.JSONObject" %>
<%@ page import="org.json.JSONArray" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.sci.rhis.util.EnglishtoBangla" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8" %>
<script src="js/lib/ddtf.js"></script>
<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript">

    function PrintElem(elem) {
//        $('button').hide();
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div');
        mywindow.document.write('<html><head><title></title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');
        mywindow.print();
        mywindow.close();

        return true;
    }
    $(document).on("click", '.button_next' ,function (){
        $('#mnc-table-left').hide();
        $('#mnc-table-right').show();
        var button_val = $(".button_next").val();
        if(button_val == 1){
            $(".button_next").val(2);
            $(".button_pre").val(1);
            $(".button_next").attr("disabled","disabled");
            $(".button_pre").removeAttr("disabled");
            $(".showing_page").text("2 of 2  ");
        }
    });

    $(document).on("click", '.button_pre' ,function (){
        $('#mnc-table-left').show();
        $('#mnc-table-right').hide();
        var button_val = $(".button_pre").val();
        if(button_val>0)
        {
            $(".button_pre").val(0);
            $(".button_next").val(1);
            $(".button_next").removeAttr("disabled");
            $(".button_pre").attr("disabled","disabled");
            $(".showing_page").text("1 of 2  ");
        }
    });
</script>

<style>
	.mnc-table-left th,.mnc-table-right th{
		border: 1px solid black!important;
		text-align: center!important;
		vertical-align: middle!important;
		padding: 2px;
	}
	.mnc-table-left td,.mnc-table-right td{
		vertical-align: top!important;
		padding-left: 2px;
	}
	.nowrap{
		white-space: nowrap!important;
	}
	.inner_table td{
		border: 1px solid black!important;
		text-align: left!important;
		vertical-align: middle!important;
		padding-left: 2px;
	}
	.pad-bot-15 div{
		padding-bottom: 10px;
	}
	.pad-bot-5 div{
		padding-bottom: 5px;
	}
	.bold-italic{
		font-weight: bold;
		font-style: italic;
		padding-left: 4px;
		font-size: 110%;
	}
</style>
<%

%>
<div class="tile-body nopadding" id="reportTable">
	<div class="row">
		<div class="col-md-12">
			<div class="dropdown" style="float: right; margin-right: 2%;">
				<button class="btn btn-primary" type="button" id="btnPrint" onclick="PrintElem('#printDiv')"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
					<span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
					<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="printDiv" style="margin-top: 2%;">
				<style>
					/*#mnc-table-left th,#mnc-table-right th{*/
					/*border: 1px solid black!important;*/
					/*text-align: center!important;*/
					/*vertical-align: middle!important;*/
					/*padding: 2px;*/
					/*}*/
					.mnc-table-left td,.mnc-table-right td{
						vertical-align: top!important;
					}
					.mnc-table-left{
						min-height: 8.5in;
					}
					.pad-bot-15 div{
						padding-bottom: 10px;
					}
					table { /* Or specify a table class */
						max-height: 100%!important;
						overflow: hidden!important;
						page-break-after: always!important;
					}
					/*.pad-bot-5 div{*/
					/*padding-bottom: 5px;*/
					/*}*/
					/*.bold-italic{*/
					/*font-weight: bold;*/
					/*font-style: italic;*/
					/*padding-left: 4px;*/
					/*font-size: 110%;*/
					/*}*/
				</style>
				<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'>
					<tr>

					</tr>
				</table>
				</br>
				<%
					String data = request.getAttribute("Result").toString();
					JSONArray json = new JSONArray(data);
					for (int i = 0; i < json.length()-i%5; i=i+5) {
						JSONObject singleRow = new JSONObject();
						JSONObject singleRow2 = new JSONObject();
						JSONObject singleRow3 = new JSONObject();
						JSONObject singleRow4 = new JSONObject();
						JSONObject singleRow5 = new JSONObject();
						try {
							singleRow = json.getJSONObject(i);
							singleRow2 = json.getJSONObject(i + 1);
							singleRow3 = json.getJSONObject(i + 2);
							singleRow4 = json.getJSONObject(i + 3);
							singleRow5 = json.getJSONObject(i + 4);

							singleRow.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_son").toString()));
							singleRow2.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_son").toString()));
							singleRow3.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_son").toString()));
							singleRow4.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("client_son").toString()));
							singleRow5.put("client_son",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("client_son").toString()));

							singleRow.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_dau").toString()));
							singleRow2.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_dau").toString()));
							singleRow3.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_dau").toString()));
							singleRow4.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("client_dau").toString()));
							singleRow5.put("client_dau",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("client_dau").toString()));

							singleRow.put("weight",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("weight").toString()));
							singleRow2.put("weight",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("weight").toString()));
							singleRow3.put("weight",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("weight").toString()));
							singleRow4.put("weight",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("weight").toString()));
							singleRow5.put("weight",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("weight").toString()));

							singleRow.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("pulse").toString()));
							singleRow2.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("pulse").toString()));
							singleRow3.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("pulse").toString()));
							singleRow4.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("pulse").toString()));
							singleRow5.put("pulse",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("pulse").toString()));

							singleRow.put("elcoNo",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("elcono").toString()));
							singleRow2.put("elcoNo",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("elcono").toString()));
							singleRow3.put("elcoNo",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("elcono").toString()));
							singleRow4.put("elcoNo",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("elcono").toString()));
							singleRow5.put("elcoNo",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("elcono").toString()));



							singleRow.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("serial").toString()));
							singleRow2.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("serial").toString()));
							singleRow3.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("serial").toString()));
							singleRow4.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("serial").toString()));
							singleRow5.put("serial",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("serial").toString()));

							singleRow.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_bp").toString()));
							singleRow2.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_bp").toString()));
							singleRow3.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_bp").toString()));
							singleRow4.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("client_bp").toString()));
							singleRow5.put("client_bp",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("client_bp").toString()));

							singleRow.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow.get("client_age").toString()));
							singleRow2.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow2.get("client_age").toString()));
							singleRow3.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow3.get("client_age").toString()));
							singleRow4.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow4.get("client_age").toString()));
							singleRow5.put("client_age",EnglishtoBangla.ConvertNumberToBangla(singleRow5.get("client_age").toString()));

							singleRow.put("client_lmp",EnglishtoBangla.getEngToBanDate(singleRow.get("client_lmp").toString()));
							singleRow2.put("client_lmp",EnglishtoBangla.getEngToBanDate(singleRow2.get("client_lmp").toString()));
							singleRow3.put("client_lmp",EnglishtoBangla.getEngToBanDate(singleRow3.get("client_lmp").toString()));
							singleRow4.put("client_lmp",EnglishtoBangla.getEngToBanDate(singleRow4.get("client_lmp").toString()));
							singleRow5.put("client_lmp",EnglishtoBangla.getEngToBanDate(singleRow5.get("client_lmp").toString()));

							singleRow.put("date1",EnglishtoBangla.getEngToBanDate(singleRow.get("date1").toString()));
							singleRow.put("date2",EnglishtoBangla.getEngToBanDate(singleRow.get("date2").toString()));
							singleRow.put("date3",EnglishtoBangla.getEngToBanDate(singleRow.get("date3").toString()));
							singleRow.put("date4",EnglishtoBangla.getEngToBanDate(singleRow.get("date4").toString()));
							singleRow.put("date5",EnglishtoBangla.getEngToBanDate(singleRow.get("date5").toString()));
							singleRow.put("date6",EnglishtoBangla.getEngToBanDate(singleRow.get("date6").toString()));
							singleRow.put("date7",EnglishtoBangla.getEngToBanDate(singleRow.get("date7").toString()));
							singleRow.put("date8",EnglishtoBangla.getEngToBanDate(singleRow.get("date8").toString()));
							singleRow.put("date9",EnglishtoBangla.getEngToBanDate(singleRow.get("date9").toString()));
							singleRow.put("date10",EnglishtoBangla.getEngToBanDate(singleRow.get("date10").toString()));


							singleRow2.put("date1",EnglishtoBangla.getEngToBanDate(singleRow2.get("date1").toString()));
							singleRow2.put("date2",EnglishtoBangla.getEngToBanDate(singleRow2.get("date2").toString()));
							singleRow2.put("date3",EnglishtoBangla.getEngToBanDate(singleRow2.get("date3").toString()));
							singleRow2.put("date4",EnglishtoBangla.getEngToBanDate(singleRow2.get("date4").toString()));
							singleRow2.put("date5",EnglishtoBangla.getEngToBanDate(singleRow2.get("date5").toString()));
							singleRow2.put("date6",EnglishtoBangla.getEngToBanDate(singleRow2.get("date6").toString()));
							singleRow2.put("date7",EnglishtoBangla.getEngToBanDate(singleRow2.get("date7").toString()));
							singleRow2.put("date8",EnglishtoBangla.getEngToBanDate(singleRow2.get("date8").toString()));
							singleRow2.put("date9",EnglishtoBangla.getEngToBanDate(singleRow2.get("date9").toString()));
							singleRow2.put("date10",EnglishtoBangla.getEngToBanDate(singleRow2.get("date10").toString()));


							singleRow3.put("date1",EnglishtoBangla.getEngToBanDate(singleRow3.get("date1").toString()));
							singleRow3.put("date2",EnglishtoBangla.getEngToBanDate(singleRow3.get("date2").toString()));
							singleRow3.put("date3",EnglishtoBangla.getEngToBanDate(singleRow3.get("date3").toString()));
							singleRow3.put("date4",EnglishtoBangla.getEngToBanDate(singleRow3.get("date4").toString()));
							singleRow3.put("date5",EnglishtoBangla.getEngToBanDate(singleRow3.get("date5").toString()));
							singleRow3.put("date6",EnglishtoBangla.getEngToBanDate(singleRow3.get("date6").toString()));
							singleRow3.put("date7",EnglishtoBangla.getEngToBanDate(singleRow3.get("date7").toString()));
							singleRow3.put("date8",EnglishtoBangla.getEngToBanDate(singleRow3.get("date8").toString()));
							singleRow3.put("date9",EnglishtoBangla.getEngToBanDate(singleRow3.get("date9").toString()));
							singleRow3.put("date10",EnglishtoBangla.getEngToBanDate(singleRow3.get("date10").toString()));


							singleRow4.put("date1",EnglishtoBangla.getEngToBanDate(singleRow4.get("date1").toString()));
							singleRow4.put("date2",EnglishtoBangla.getEngToBanDate(singleRow4.get("date2").toString()));
							singleRow4.put("date3",EnglishtoBangla.getEngToBanDate(singleRow4.get("date3").toString()));
							singleRow4.put("date4",EnglishtoBangla.getEngToBanDate(singleRow4.get("date4").toString()));
							singleRow4.put("date5",EnglishtoBangla.getEngToBanDate(singleRow4.get("date5").toString()));
							singleRow4.put("date6",EnglishtoBangla.getEngToBanDate(singleRow4.get("date6").toString()));
							singleRow4.put("date7",EnglishtoBangla.getEngToBanDate(singleRow4.get("date7").toString()));
							singleRow4.put("date8",EnglishtoBangla.getEngToBanDate(singleRow4.get("date8").toString()));
							singleRow4.put("date9",EnglishtoBangla.getEngToBanDate(singleRow4.get("date9").toString()));
							singleRow4.put("date10",EnglishtoBangla.getEngToBanDate(singleRow4.get("date10").toString()));


							singleRow5.put("date1",EnglishtoBangla.getEngToBanDate(singleRow5.get("date1").toString()));
							singleRow5.put("date2",EnglishtoBangla.getEngToBanDate(singleRow5.get("date2").toString()));
							singleRow5.put("date3",EnglishtoBangla.getEngToBanDate(singleRow5.get("date3").toString()));
							singleRow5.put("date4",EnglishtoBangla.getEngToBanDate(singleRow5.get("date4").toString()));
							singleRow5.put("date5",EnglishtoBangla.getEngToBanDate(singleRow5.get("date5").toString()));
							singleRow5.put("date6",EnglishtoBangla.getEngToBanDate(singleRow5.get("date6").toString()));
							singleRow5.put("date7",EnglishtoBangla.getEngToBanDate(singleRow5.get("date7").toString()));
							singleRow5.put("date8",EnglishtoBangla.getEngToBanDate(singleRow5.get("date8").toString()));
							singleRow5.put("date9",EnglishtoBangla.getEngToBanDate(singleRow5.get("date9").toString()));
							singleRow5.put("date10",EnglishtoBangla.getEngToBanDate(singleRow5.get("date10").toString()));
							//due date
							singleRow.put("due_date2",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date2").toString()));
							singleRow.put("due_date3",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date3").toString()));
							singleRow.put("due_date4",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date4").toString()));
							singleRow.put("due_date5",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date5").toString()));
							singleRow.put("due_date6",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date6").toString()));
							singleRow.put("due_date7",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date7").toString()));
							singleRow.put("due_date8",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date8").toString()));
							singleRow.put("due_date9",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date9").toString()));
							singleRow.put("due_date10",EnglishtoBangla.getEngToBanDate(singleRow.get("due_date10").toString()));


							singleRow2.put("due_date2",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date2").toString()));
							singleRow2.put("due_date3",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date3").toString()));
							singleRow2.put("due_date4",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date4").toString()));
							singleRow2.put("due_date5",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date5").toString()));
							singleRow2.put("due_date6",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date6").toString()));
							singleRow2.put("due_date7",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date7").toString()));
							singleRow2.put("due_date8",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date8").toString()));
							singleRow2.put("due_date9",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date9").toString()));
							singleRow2.put("due_date10",EnglishtoBangla.getEngToBanDate(singleRow2.get("due_date10").toString()));


							singleRow3.put("due_date2",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date2").toString()));
							singleRow3.put("due_date3",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date3").toString()));
							singleRow3.put("due_date4",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date4").toString()));
							singleRow3.put("due_date5",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date5").toString()));
							singleRow3.put("due_date6",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date6").toString()));
							singleRow3.put("due_date7",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date7").toString()));
							singleRow3.put("due_date8",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date8").toString()));
							singleRow3.put("due_date9",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date9").toString()));
							singleRow3.put("due_date10",EnglishtoBangla.getEngToBanDate(singleRow3.get("due_date10").toString()));


							singleRow4.put("due_date2",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date2").toString()));
							singleRow4.put("due_date3",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date3").toString()));
							singleRow4.put("due_date4",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date4").toString()));
							singleRow4.put("due_date5",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date5").toString()));
							singleRow4.put("due_date6",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date6").toString()));
							singleRow4.put("due_date7",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date7").toString()));
							singleRow4.put("due_date8",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date8").toString()));
							singleRow4.put("due_date9",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date9").toString()));
							singleRow4.put("due_date10",EnglishtoBangla.getEngToBanDate(singleRow4.get("due_date10").toString()));


							singleRow5.put("due_date2",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date2").toString()));
							singleRow5.put("due_date3",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date3").toString()));
							singleRow5.put("due_date4",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date4").toString()));
							singleRow5.put("due_date5",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date5").toString()));
							singleRow5.put("due_date6",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date6").toString()));
							singleRow5.put("due_date7",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date7").toString()));
							singleRow5.put("due_date8",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date8").toString()));
							singleRow5.put("due_date9",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date9").toString()));
							singleRow5.put("due_date10",EnglishtoBangla.getEngToBanDate(singleRow5.get("due_date10").toString()));



						} catch (Exception e) {
							e.printStackTrace();
						}

				%>
				<h1 align="middle">ইঞ্জেক্টেবল রেজিস্টার</h1>
				<table   class="mnc-table-left"  border="1" cellspacing="0" style="font-size:11px; min-height: 8.5in;table-layout:fixed;">
					<tbody >
						<tr>
							<th style="width: 100px">ক্রমিক নং</th>
							<th class="nowrap" style="width: 300px"><b>গ্রহীতার নাম ও ঠিকানা</b></th>
							<th colspan="2" style="width: 200px"><b>গ্রহিতার বয়স</b></th>
							<th ><b>শারীরিক তথ্য</b></th>
							<th ><b>শেষ মাসিকের তারিখ</b></th>
						</tr>
						<%--client 1--%>
						<tr>
							<td rowspan="6">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td >
								<div class="row">
									<div class="col-md-12">
										নাম :<span class="bold-italic"><% out.println(singleRow.get("name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামীর/পিতার নাম:<span class="bold-italic"><% out.println(singleRow.get("fathername"));%></span>
									</div>
								</div>
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										</br><span class="bold-italic"><% out.println(singleRow.get("client_age"));%></span>
									</div>
								</div>
							</td>
							<td rowspan="6">
								</br>ওজন : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("weight"));%></span>
								</br></br>নাড়ির গতি : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("pulse"));%></span>
								</br></br>রক্তচাপ : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("client_bp"));%></span>
								</br></br>স্তনে চাকা : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow.get("client_breast"));%></span>
								</br></br>পি, ভি পরীক্ষা : <span class="bold-italic">-&nbsp;&nbsp;</span>
							</td>
							<td rowspan="6">
								</br></br> <span class="bold-italic">&nbsp;&nbsp;<% out.println(singleRow.get("client_lmp"));%></span>
							</td>
						</tr>
						<tr>
							<td rowspan="3">
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("villagenameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("unionnameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("upazilanameeng"));%></span>
										&nbsp;&nbsp;&nbsp;&nbsp;জেলা: &nbsp;<span class="bold-italic"><% out.println(singleRow.get("zillanameeng"));%></span>
									</div>
								</div>
							</td>
							<td colspan="2">জীবিত সন্তান</td>
						</tr>
						<tr>
							<td>ছেলে</td>
							<td>মেয়ে</td>
						</tr>
						<tr>
							<td>
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_son"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										&nbsp;<span class="bold-italic"><% out.println(singleRow.get("client_dau"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								বাড়ি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow.get("hahhno"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								সক্ষম দম্পতি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow.get("elcono"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<%--client 2--%>
						<tr>
							<td rowspan="6">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow2.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td >
								<div class="row">
									<div class="col-md-12">
										নাম :<span class="bold-italic"><% out.println(singleRow2.get("name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামীর/পিতার নাম:<span class="bold-italic"><% out.println(singleRow2.get("fathername"));%></span>
									</div>
								</div>
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										</br><span class="bold-italic"><% out.println(singleRow2.get("client_age"));%></span>
									</div>
								</div>
							</td>
							<td rowspan="6">
								</br>ওজন : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("weight"));%></span>
								</br>নাড়ির গতি : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("pulse"));%></span>
								</br>রক্তচাপ : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("client_bp"));%></span>
								</br>স্তনে চাকা : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow2.get("client_breast"));%></span>
								</br>পি, ভি পরীক্ষা : <span class="bold-italic">-&nbsp;&nbsp;</span>
							</td>
							<td rowspan="6">
								</br></br> <span class="bold-italic">&nbsp;&nbsp;<% out.println(singleRow2.get("client_lmp"));%></span>
							</td>
						</tr>
						<tr>
							<td rowspan="3">
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("villagenameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("unionnameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("upazilanameeng"));%></span>
										&nbsp;&nbsp;&nbsp;&nbsp;জেলা: &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("zillanameeng"));%></span>

									</div>
								</div>
							</td>
							<td colspan="2">জীবিত সন্তান</td>
						</tr>
						<tr>
							<td>ছেলে</td>
							<td>মেয়ে</td>
						</tr>
						<tr>
							<td>
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_son"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										&nbsp;<span class="bold-italic"><% out.println(singleRow2.get("client_dau"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								বাড়ি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("hahhno"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								সক্ষম দম্পতি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow2.get("elcono"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<%--client 3--%>
						<tr>
							<td rowspan="6">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow3.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td >
								<div class="row">
									<div class="col-md-12">
										নাম :<span class="bold-italic"><% out.println(singleRow3.get("name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামীর/পিতার নাম:<span class="bold-italic"><% out.println(singleRow3.get("fathername"));%></span>
									</div>
								</div>
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										</br><span class="bold-italic"><% out.println(singleRow3.get("client_age"));%></span>
									</div>
								</div>
							</td>
							<td rowspan="6">
								</br>ওজন : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("weight"));%></span>
								</br>নাড়ির গতি : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("pulse"));%></span>
								</br>রক্তচাপ : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("client_bp"));%></span>
								</br>স্তনে চাকা : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow3.get("client_breast"));%></span>
								</br>পি, ভি পরীক্ষা : <span class="bold-italic">-&nbsp;&nbsp;</span>
							</td>
							<td rowspan="6">
								</br></br> <span class="bold-italic">&nbsp;&nbsp;<% out.println(singleRow3.get("client_lmp"));%></span>
							</td>
						</tr>
						<tr>
							<td rowspan="3">
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("villagenameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("unionnameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("upazilanameeng"));%></span>
										&nbsp;&nbsp;&nbsp;&nbsp;জেলা: &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("zillanameeng"));%></span>

									</div>
								</div>
							</td>
							<td colspan="2">জীবিত সন্তান</td>
						</tr>
						<tr>
							<td>ছেলে</td>
							<td>মেয়ে</td>
						</tr>
						<tr>
							<td>
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_son"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										&nbsp;<span class="bold-italic"><% out.println(singleRow3.get("client_dau"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								বাড়ি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("hahhno"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								সক্ষম দম্পতি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow3.get("elcono"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<%--client 4--%>
						<tr>
							<td rowspan="6">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow4.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td >
								<div class="row">
									<div class="col-md-12">
										নাম :<span class="bold-italic"><% out.println(singleRow4.get("name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামীর/পিতার নাম:<span class="bold-italic"><% out.println(singleRow4.get("fathername"));%></span>
									</div>
								</div>
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										</br><span class="bold-italic"><% out.println(singleRow4.get("client_age"));%></span>
									</div>
								</div>
							</td>
							<td rowspan="6">
								</br>ওজন : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("weight"));%></span>
								</br>নাড়ির গতি : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("pulse"));%></span>
								</br>রক্তচাপ : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("client_bp"));%></span>
								</br>স্তনে চাকা : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow4.get("client_breast"));%></span>
								</br>পি, ভি পরীক্ষা : <span class="bold-italic">-&nbsp;&nbsp;</span>
							</td>
							<td rowspan="6">
								</br></br> <span class="bold-italic">&nbsp;&nbsp;<% out.println(singleRow4.get("client_lmp"));%></span>
							</td>
						</tr>
						<tr>
							<td rowspan="3">
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("villagenameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("unionnameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("upazilanameeng"));%></span>
										&nbsp;&nbsp;&nbsp;&nbsp;জেলা: &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("zillanameeng"));%></span>

									</div>
								</div>
							</td>
							<td colspan="2">জীবিত সন্তান</td>
						</tr>
						<tr>
							<td>ছেলে</td>
							<td>মেয়ে</td>
						</tr>
						<tr>
							<td>
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("client_son"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										&nbsp;<span class="bold-italic"><% out.println(singleRow4.get("client_dau"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								বাড়ি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("hahhno"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								সক্ষম দম্পতি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow4.get("elcono"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<%--client 5--%>
						<tr>
							<td rowspan="6">
								<div class="row">
									<div class="col-md-12">
										<span class="bold-italic"><% out.println(singleRow5.get("serial"));%></span>
									</div>
								</div>
							</td>
							<td >
								<div class="row">
									<div class="col-md-12">
										নাম :<span class="bold-italic"><% out.println(singleRow5.get("name"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										স্বামীর/পিতার নাম:<span class="bold-italic"><% out.println(singleRow5.get("fathername"));%></span>
									</div>
								</div>
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										</br><span class="bold-italic"><% out.println(singleRow5.get("client_age"));%></span>
									</div>
								</div>
							</td>
							<td rowspan="6">
								</br>ওজন : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow5.get("weight"));%></span>
								</br>নাড়ির গতি : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow5.get("pulse"));%></span>
								</br>রক্তচাপ : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow5.get("client_bp"));%></span>
								</br>স্তনে চাকা : <span class="bold-italic">-&nbsp;&nbsp;<% out.println(singleRow5.get("client_breast"));%></span>
								</br>পি, ভি পরীক্ষা : <span class="bold-italic">-&nbsp;&nbsp;</span>
							</td>
							<td rowspan="6">
								</br></br> <span class="bold-italic">&nbsp;&nbsp;<% out.println(singleRow5.get("client_lmp"));%></span>
							</td>
						</tr>
						<tr>
							<td rowspan="3">
								<div class="row">
									<div class="col-md-12">
										গ্রাম: &nbsp;<span class="bold-italic"><% out.println(singleRow5.get("villagenameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিট/ওয়ার্ড নম্বর: &nbsp;
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										ইউনিয়ন: &nbsp;<span class="bold-italic"><% out.println(singleRow5.get("unionnameeng"));%></span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										উপজেলা/থানা: &nbsp;<span class="bold-italic"><% out.println(singleRow5.get("upazilanameeng"));%></span>
										&nbsp;&nbsp;&nbsp;&nbsp;জেলা: &nbsp;<span class="bold-italic"><% out.println(singleRow5.get("zillanameeng"));%></span>

									</div>
								</div>
							</td>
							<td colspan="2">জীবিত সন্তান</td>
						</tr>
						<tr>
							<td>ছেলে</td>
							<td>মেয়ে</td>
						</tr>
						<tr>
							<td>
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow5.get("client_son"));%></span>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-md-12">
										&nbsp;<span class="bold-italic"><% out.println(singleRow5.get("client_dau"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								বাড়ি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow5.get("hahhno"));%></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								সক্ষম দম্পতি নং :
							</td>
							<td colspan="2">
								<div class="row">
									<div class="col-md-12">
										 &nbsp;<span class="bold-italic"><% out.println(singleRow5.get("elcono"));%></span>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<h1 align="middle">ইঞ্জেক্টেবল রেজিস্টার</h1>
				<table   class="mnc-table-left"  border="1" cellspacing="0" style="font-size:11px; min-height: 8.5in;table-layout:fixed;">
					<tbody >
						<%--client 1 --%>
						<tr>
							<th >ইঞ্জেকশন ডিউ ডোজের তারিখ</th>
							<th style="width: 80px;"><b></b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date2"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date3"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date4"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date5"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date6"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date7"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date8"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date9"));%> </b></th>
							<th style="width: 80px;"><b><% out.println(singleRow.get("due_date10"));%> </b></th>
						</tr>
						<tr>
							<th >ইঞ্জেকশন প্রদানের প্রকৃত তারিখ</th>
							<th ><b> <% out.println(singleRow.get("date1"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date2"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date3"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date4"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date5"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date6"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date7"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date8"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date9"));%> </b></th>
							<th ><b> <% out.println(singleRow.get("date10"));%> </b></th>
						</tr>
						<tr>
							<th >ইঞ্জেকশনের নাম</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date1").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date2").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date3").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date4").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date5").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date6").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date7").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date8").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date9").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>
							<th >
								<b>
								<%
									if(!singleRow.get("date10").equals(""))
									     out.println("ডি এম পি এ");
								%>
								</b>
							</th>

						</tr>

							<%--client 2 --%>
							<tr>
								<th >ইঞ্জেকশন ডিউ ডোজের তারিখ</th>
								<th style="width: 80px;"><b></b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date2"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date3"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date4"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date5"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date6"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date7"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date8"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date9"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow2.get("due_date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশন প্রদানের প্রকৃত তারিখ</th>
								<th ><b> <% out.println(singleRow2.get("date1"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date2"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date3"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date4"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date5"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date6"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date7"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date8"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date9"));%> </b></th>
								<th ><b> <% out.println(singleRow2.get("date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশনের নাম</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date1").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date2").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date3").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date4").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date5").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date6").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date7").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date8").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date9").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow2.get("date10").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>

							</tr>

							<%--client 3 --%>
							<tr>
								<th >ইঞ্জেকশন ডিউ ডোজের তারিখ</th>
								<th style="width: 80px;"><b></b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date2"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date3"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date4"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date5"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date6"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date7"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date8"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date9"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow3.get("due_date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশন প্রদানের প্রকৃত তারিখ</th>
								<th ><b> <% out.println(singleRow3.get("date1"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date2"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date3"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date4"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date5"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date6"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date7"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date8"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date9"));%> </b></th>
								<th ><b> <% out.println(singleRow3.get("date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশনের নাম</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date1").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date2").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date3").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date4").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date5").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date6").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date7").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date8").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date9").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow3.get("date10").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>

							</tr>

							<%--client 4 --%>
							<tr>
								<th >ইঞ্জেকশন ডিউ ডোজের তারিখ</th>
								<th style="width: 80px;"><b></b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date2"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date3"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date4"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date5"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date6"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date7"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date8"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date9"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow4.get("due_date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশন প্রদানের প্রকৃত তারিখ</th>
								<th ><b> <% out.println(singleRow4.get("date1"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date2"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date3"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date4"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date5"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date6"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date7"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date8"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date9"));%> </b></th>
								<th ><b> <% out.println(singleRow4.get("date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশনের নাম</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date1").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date2").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date3").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date4").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date5").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date6").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date7").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date8").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date9").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow4.get("date10").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>

							</tr>

							<%--client 5 --%>
							<tr>
								<th >ইঞ্জেকশন ডিউ ডোজের তারিখ</th>
								<th style="width: 80px;"><b></b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date2"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date3"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date4"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date5"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date6"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date7"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date8"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date9"));%> </b></th>
								<th style="width: 80px;"><b><% out.println(singleRow5.get("due_date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশন প্রদানের প্রকৃত তারিখ</th>
								<th ><b> <% out.println(singleRow5.get("date1"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date2"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date3"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date4"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date5"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date6"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date7"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date8"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date9"));%> </b></th>
								<th ><b> <% out.println(singleRow5.get("date10"));%> </b></th>
							</tr>
							<tr>
								<th >ইঞ্জেকশনের নাম</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date1").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date2").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date3").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date4").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date5").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date6").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date7").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date8").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date9").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>
								<th >
									<b>
										<%
											if(!singleRow5.get("date10").equals(""))
												out.println("ডি এম পি এ");
										%>
									</b>
								</th>

							</tr>
					</tbody>
				</table>
				<%}%>

			</div>
		</div>
	</div>
</div>


