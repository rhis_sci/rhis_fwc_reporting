<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

        <style>
        	.select2 .select2-container{
        		width: 100% !important;
        	}
        </style>
        
        <div class="box box-default remove_div">
	        <div class="box-header with-border">
	          <h3 class="box-title" id="box_title"></h3>
	
	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
	          </div>
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body search_form">
	          <div class="row">
	            <div class="col-md-12">
				 <div class="form-group col-md-6">
	                <label>Division</label>
	                <select class="form-control select2" id="RdivName" style="width: 100%;">
	                </select>
	              </div>
	              <div class="form-group col-md-6">
	                <label>District</label>
	                <select class="form-control select2" id="RzillaName" style="width: 100%;">
	                </select>
	              </div>
	              <div class="form-group col-md-6">
	                <label>Upazila</label>
	                <select class="form-control select2 upazilaDD" id="RupaZilaName" style="width: 100%;">
	                </select>
	              </div>
	              <div class="form-group col-md-6 TableName">
                    <label>Table Name</label>
                    <br>
                    <%--<input type="radio" name="tableType" class="tableType" value="1">&nbsp;&nbsp;Table for Both Sync&nbsp;&nbsp;&nbsp;--%>
                    <%--<input type="radio" name="tableType" class="tableType" value="2">&nbsp;&nbsp;Table for Server to Node Sync&nbsp;&nbsp;&nbsp;--%>
                    <input type="radio" name="tableType" class="tableType" value="3">&nbsp;&nbsp;Single Table Sync
                    <input type="text" class="form-control" id="tbs" value="" style="display: none;">
                    <input type="text" class="form-control" id="tsns" value="" style="display: none;">
                    <div style="display: none;" id="sts"><% out.println(request.getAttribute("Dropdown")); %></div>
                    <!-- <input type="text" class="form-control" id="TableName" required="required" placeholder="Table Name"> -->
                  </div>
                  <div class="form-group col-md-6 syncType">
	                <label>Sync Type</label>
	                <select class="form-control" id="syncType" required="required">
	                	<option value="">--Select Sync Type--</option>
	                	<option value="1">Server to Node</option>
	                	<option value="2">Node to Server</option>
	                	<option value="3">Both</option>
	                </select>
	              </div>
	              <div class="box-footer col-md-12">
	              	<input type="hidden" id="formType" value="">
                    <input type="button" id="SymmetricDS" class="btn btn-primary" value="Submit">
	              </div>
	            </div>
	          </div>
	          <!-- /.row -->
	        </div>
	    
	    <div id="model_box"></div> 
	        
     </div>    
     
     <script>
	     $(document).ready(function(){
	     	$(".select2").select2();
	     	
	     	$(".tableType").on('click', function(){
	     		if($(".tableType:checked").val()=="1")
     			{
     				$("#tbs").show();
     				$("#tsns").hide();
     				$("#sts").hide();
     				$("#syncType").val("3");
     			}
	     		else if($(".tableType:checked").val()=="2")
	     		{
	     			$("#tsns").show();
     				$("#tbs").hide();
     				$("#sts").hide();
     				$("#syncType").val("1");
	     		}
	     		else
	     		{
	     			$("#sts").show();
     				$("#tbs").hide();
     				$("#tsns").hide();
     				$("#syncType").val("");
	     		}	
	     	});

	     	$("#RzillaName").on('change', function () {
//				if($("#RzillaName").val()=="69" || $("#RzillaName").val()=="42"){
                    $("#tbs").val("ancservice,clientmap,clientmap_extension,death,delivery,elco,fpinfo,immunizationhistory,newborn,pacservice,pncservicechild,pncservicemother,pregwomen,regserial,fpmethods,fpmapping,fpexamination,gpservice,iudservice,iudfollowupservice,implantservice,implantfollowupservice,pillcondomservice,womaninjectable,permanent_method_service,permanent_method_followup_service,child_care_service,child_care_service_detail");
                    $("#tsns").val("member,treatmentlist,diseaselist,symptomlist");
//				}
//				else{
//				    $("#tbs").val("ancService,clientMap,clientmap_extension,death,delivery,elco,fpInfo,immunizationHistory,newBorn,pacService,pncServiceChild,pncServiceMother,pregWomen,regSerial,FPMethods,FPMapping,FPExamination,gpService,iudService,iudFollowupService,implantService,implantFollowupService,pillCondomService,womanInjectable");
//				    $("#tsns").val("Member,treatmentList,diseaseList,symptomList");
//				}
            })

	     });	
     </script>   
        
        
