<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/28/2021
  Time: 4:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>


        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">

                    <!--geo group-->
                    <%@include file="/jsp/util/geoSection.jsp" %>
                    <!--/geo group-->

                    <!--select type-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="facility_name" style="display: none;">
                                <label>Facility Name</label>
                                <select class="form-control select2" id="facilityName" style="width: 100%;">
                                </select>
                            </div>
                        </div>
                    </div>

                    <!--date section-->
                    <%@include file="/jsp/util/date.jsp" %>
                </div>

            </div>
            <!--row-->
        </div>
    </div>

    <!--for showing table(performance) result-->
    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>
    <!--/for showing table(performance) result-->

    <script>
        $(".select2").select2();

        $("#RdivName, #RzillaName, #RupaZilaName, #facilityType").on('change', function(){
            $("#facilityName").empty();
        });

        $("#RunionName").on('change', function(){
            getFacilityInfo();
        });

        function getFacilityInfo(){

            //alert("Zillaid="+ $('#RzillaName').val());
            //alert("Upazilaid="+ $('#RupaZilaName').val());

            var forminfo = new Object();
            forminfo.zillaid = $('#RzillaName').val();
            forminfo.upazilaid = $('#RupaZilaName').val();
            forminfo.unionid = $('#RunionName').val();
            if($('#facility_type').is(':visible'))
                forminfo.facilityType = $('#facilityType').val().split("_")[0];
            else if($('#provider_facility_type').is(':visible'))
                forminfo.facilityType = $('#providerFacility').val().split("_")[0];
            else
                forminfo.facilityType = 0;

            forminfo.type = "6";

            $.ajax({
                type: "POST",
                url:"getFacilityInfo",
                timeout:60000, //60 seconds timeout
                data:{"forminfo":JSON.stringify(forminfo)},
                success: function (response) {
                    console.log(response);
                    facilityJS = JSON.parse(response);
                    $("#facilityName").empty();
                    var output = "";

                    $.each(facilityJS, function(key, val) {
                        output = output + "<option value = " + key + ">" + val + "</option>";
                    });

                    $("#facilityName").append(output);
                },
                complete: function() {
                    preventMultiCall = 0;
                },
                error: function(xhr, status, error) {
                    checkInternetConnection(xhr)
                }
            });
        }


    </script>
</div>

