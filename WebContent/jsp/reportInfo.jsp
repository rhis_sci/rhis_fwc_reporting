<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<style>
	.input-daterange input,#monthSelect,#yearSelect{
		background: url('image/calendar.png') no-repeat;
		background-size: 30px 30px;
		padding-left: 30px;
		text-align: center;
		background-color: #ffffff!important;
	}
</style>
<div class="remove_div">
	<div class="box box-default">
		<div class="box-header with-border">
			<!--<h3 class="box-title" id="box_title"></h3>-->
			<h3 class="box-title" id="box_title"></h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body search_form">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group col-md-4" id="view_type" style="display: none;">
						<label>District/Upazila/Union/Facility</label>
						<select class="form-control select2" id="reportViewType" onchange="show_dpu(this.value)" style="width: 100%;">
							<option value="">--Select Type--</option>
							<option value="1">District Wise</option>
							<option value="2">Upazila Wise</option>
							<option value="3">Union Wise</option>
							<option value="4">Facility Wise</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="view_level" style="display: none;">
						<label>District/Upazila/Union Level</label>
						<select class="form-control select2" id="reportViewLevel" onchange="show_dpu_level(this.value)" style="width: 100%;">
							<option value="">--Select Level--</option>
							<%--<option value="1">All</option>--%>
							<option value="2">District Level</option>
							<option value="3">Upazila Level</option>
							<option value="4">Union Level</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="indType" style="display: none;">
						<label>Indicator Type</label>
						<select class="form-control select2" id="indicatorType" style="width: 100%;">
							<option value="">--Select Indicator Type--</option>
							<option value="HBP">High Blood Pressure</option>
							<option value="DAN">Delivery & Newborn</option>
							<option value="LBW">Low Birth Weight Baby</option>
							<% int userTypeReport = (Integer)request.getAttribute("uType"); %>
							<% if(userTypeReport == 999 || userTypeReport == 998 || userTypeReport == 994){ %>
							<option value="NRC">Service By NRC & HID</option>
							<% } %>
						</select>
					</div>
					<div class="form-group col-md-4" id="serType" style="display: none;">
						<label>Service Type</label>
						<select class="form-control select2" id="serviceType" style="width: 100%;">
							<option value="">--Select Service Type--</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="service_by" style="display: none;">
						<label>View Service Record By</label>
						<select class="form-control select2" id="viewServiceBy" style="width: 100%;">
							<option value="">--Select--</option>
							<option value="1">Union Wise</option>
							<option value="2">Provider Wise</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="method_type" style="display: none;">
						<label>Service</label>
						<select class="form-control select2" id="methodType" style="width: 100%;">
							<option value="">--Select Method Type--</option>
							<option value="1">Maternal and Child Health</option>
							<option value="2">Family Planing</option>
							<option value="3">General Patient</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="sba_nsba" style="display: none;">
						<label>SBA/Non-SBA</label>
						<select class="form-control select2" id="sbaType" style="width: 100%;">
							<option value="">All</option>
							<option value="1">SBA</option>
							<option value="2">TBA</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="delivery_place" style="display: none;">
						<label>Delivery Place</label>
						<select class="form-control select2" id="deliveryPlace" style="width: 100%;">
							<option value="">ALL</option>
							<option value="1">Home</option>
							<option value="2">Facility</option>
						</select>
					</div>
					<div class="form-group col-md-4" id="facility_type" style="display: none;">
		                <label>Facility Type</label>
		                <select class="form-control select2" id="facilityType" style="width: 100%;">
		                </select>
		              </div>
					 <div class="form-group col-md-4" id="divisionName">
						<label>Division</label>
						<select class="form-control select2" id="RdivName" style="width: 100%;">
						</select>
					</div> 
					<div class="form-group col-md-4" id="districtName">
						<label>District</label>
						<select class="form-control select2" id="RzillaName" style="width: 100%;">
						</select>
					</div>
					<div class="form-group col-md-4" id="upazilaName">
						<label>Upazila</label>
						<select class="form-control select2" id="RupaZilaName" style="width: 100%;">
						</select>
					</div>
					<div class="form-group col-md-4" id="unionName">
						<label>Union</label>
						<select class="form-control select2" id="RunionName" style="width: 100%;">
						</select>
					</div>
					<div class="form-group col-md-4" id="facility_name" style="display: none;">
						<label>Facility Name</label>
						<select class="form-control select2" id="facilityName" style="width: 100%;">
						</select>
					</div>
	              	<div class="form-group col-md-6" id="provider_type" style="display: none;">
		                <label>Provider Type</label>
		                <select class="form-control select2" id="providerType" style="width: 100%;">
		                	<!-- <option value="">ALL</option>
		                	<option value="999">Super Admin</option>
		                	<option value="998">Admin</option>
		                	<option value="997">Report Viewer</option>
		                	<option value="14">MOMCH</option> -->
		                	<!-- <option value="4">FWV</option> -->
		                </select>
		          	</div>
                    <%--<div class="form-group col-md-4" id="FacName" <% if(!request.getAttribute("type").toString().equals("18") && !request.getAttribute("type").toString().equals("16") && !request.getAttribute("type").toString().equals("17")){%>style="display: none"<%}%>>--%>
                        <%--<label>Facility Name</label>--%>
                        <%--<select class="form-control select2" id="facilityName" required="required">--%>

                            <%--&lt;%&ndash;<% if(userType == 999){ %>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<option value="মা-মনি কার্যালয়, গুলশান , ঢাকা">মা-মনি কার্যালয়, গুলশান , ঢাকা</option>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<% } %>&ndash;%&gt;--%>
                        <%--</select>--%>
                    <%--</div>--%>
		          	<div class="form-group col-md-6" id="mis3Type" style="display: none;">
		                <label>MIS3 View Type</label><br />
		                <input type="radio" name="mis3ViewType" value="1" checked class="flat-red">&nbsp;&nbsp;&nbsp;পরিদর্শন অনুযায়ী&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                	<!--<input type="radio" name="mis3ViewType" value="2" class="flat-red">&nbsp;&nbsp;&nbsp;আদর্শ সময়কাল অনুযায়ী&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
						<input type="radio" name="mis3ViewType" value="3" class="flat-red">&nbsp;&nbsp;&nbsp;Submitted MIS3
		                <!-- <select class="form-control select2" id="mis3ViewType" style="width: 100%;">
		                	<option value="1">পরিদর্শন অনুযায়ী</option>
		                	<option value="2">আদর্শ সময়কাল অনুযায়ী</option>
		                </select> -->
		          	</div>
					<div class="form-group col-md-12 date_type">
						<label>Select Date Type</label><br />
						<span class="levelYear"><input type="radio" id="year_type" name="date_type" value="3" class="flat-red">&nbsp;&nbsp;&nbsp;Year Wise&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<span class="levelMonth"><input type="radio" id="month_type" name="date_type" value="1" class="flat-red">&nbsp;&nbsp;&nbsp;Month Wise&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<span class="levelDate"><input type="radio" id="date_type" name="date_type" value="2" class="flat-red">&nbsp;&nbsp;&nbsp;Date Wise</span>
					</div>
					<div class="form-group col-md-12 graph_view_type" style="display: none;">
						<label>Select Trend View Type</label><br />
						<span class="levelMonthly"><input type="radio" id="monthly" name="date_type" value="1" class="flat-red">&nbsp;&nbsp;&nbsp;Monthly&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<span class="levelWeekly"><input type="radio" id="weekly" name="date_type" value="2" class="flat-red">&nbsp;&nbsp;&nbsp;Weekly&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<span class="levelDaily"><input type="radio" id="daily" name="date_type" value="3" class="flat-red">&nbsp;&nbsp;&nbsp;Daily</span>
					</div>
					<div class="form-group date col-md-2 year_wise" style="display: none">
						<label class="control-label">Year</label>
						<input type="text" id="yearSelect" class="form-control" readonly>
					</div>
					<div class="form-group date col-md-2 month_wise" style="display: none">
						<label class="control-label">Month</label>
						<input type="text" id="monthSelect" class="form-control" readonly>
					</div>
					<div class="form-group col-md-4 date_wise " style="display: none">
						<label class="control-label" for="start_date">Date Range</label>
						<div class="input-group input-daterange">
							<input type="text" id="start_date" class="form-control" readonly >
							<div class="input-group-addon">to</div>
							<input type="text" id="end_date" class="form-control" readonly >
						</div>
					</div>

					<div class="form-group col-md-4">
						<label class="control-label"></label>
						<div class="input-group input-button">
							<input type="hidden" id="reportType" value="<% out.print(request.getAttribute("type")); %>">
							<div>
								<button type="button" id="submitReport1" class="btn btn-primary">
									<i class="fa fa-send"></i> Submit
								</button>
							</div>
						</div>
					</div>
					<%--<div class="box-footer col-md-12">--%>
						<%--<input type="hidden" id="reportType" value="<% out.print(request.getAttribute("type")); %>">--%>
						<%--<input type="button" id="submitReport1" class="btn btn-primary" value="Submit">--%>
					<%--</div>--%>
				</div>
			</div>
			<!-- /.row -->
		</div>
	</div>

	<div class="row" id="table_row" style="display:none;">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title" id="table_title"></h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding" id="table_append"></div>
			</div>
		</div>
	</div>
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="library/datepicker/datepicker.min.css">
	<!-- bootstrap datepicker -->
	<script src="library/datepicker/bootstrap-datepicker.min.js"></script>

	<script>

        $(".select2").select2();

        serviceTypeList1 = '[{"ALL":"ALL","ANC":"ANC","PNC":"PNC","Delivery":"Delivery","PNC-N":"PNC-N","PILLCON":"Pill Condom","IUD":"IUD","IUDFOL":"IUD Followup","Implant":"Implant","IMPFOL":"Implant Followup","INJ":"Injectable","GP":"General Patient"}]';
        serviceTypeJS1 = JSON.parse(serviceTypeList1);

        serviceTypeList2 = '[{"ANC":"ANC","PNC":"PNC"}]';
        serviceTypeJS2 = JSON.parse(serviceTypeList2);

		function loadServiceTable(serviceTableJS){
			$("#serviceType").empty();
			var output = "<option value ='' selected></option>";

			$.each(serviceTableJS[0], function(key, val) {
				output = output + "<option value = " + key + ">" + val + "</option>";
			});

			$("#serviceType").append(output);
		}
				
        function show_dpu(type)
        {
            if(type==1)
            {
				$("#divisionName").hide();
				$("#districtName").hide();
                $("#upazilaName").hide();
                $("#unionName").hide();
				
				
            }
            else if(type==2)
            {
				$("#divisionName").show();			
                $("#districtName").show();
				$("#upazilaName").hide();
                $("#unionName").hide();
            }
            else if(type==3 || type==4)
            {
				$("#divisionName").show();
                $("#districtName").show();
                $("#upazilaName").show();
                $("#unionName").hide();
            }
        }

        function show_dpu_level(level)
        {
            if(level==1)
            {
				$("#divisionName").hide();
                $("#districtName").hide();
                $("#upazilaName").hide();
                $("#unionName").hide();
            }
            else if(level==2)
            {
                $("#districtName").show();
                $("#upazilaName").hide();
                $("#unionName").hide();
				
				//$('select').prop('selectedIndex', 0);
				$("#RupaZilaName")[0].selectedIndex = 0;
				$("#RunionName")[0].selectedIndex = 0;
            }
            else if(level==3)
            {
				$("#divisionName").show();
                $("#districtName").show();
                $("#upazilaName").show();
                $("#unionName").hide();
				$("#RunionName")[0].selectedIndex = 0;
            }
            else if(level==4)
            {
				$("#divisionName").show();
                $("#districtName").show();
                $("#upazilaName").show();
                $("#unionName").show();
            }
        }

        $('#year_type, #monthly').on('change', function(){
            $(".year_wise").show();
            $('#end_date').datepicker('setDate', null);
            $('#start_date').datepicker('setDate', null);
            $('#monthSelect').datepicker('setDate', null);
            $('#yearSelect').datepicker('setDate',  new Date().getFullYear().toString());
            $(".date_wise").hide();
            $(".month_wise").hide();
        });

        $('#month_type, #weekly').on('change', function(){
            $(".month_wise").show();
            $('#end_date').datepicker('setDate', null);
            $('#start_date').datepicker('setDate', null);
            $('#yearSelect').datepicker("setDate", null);
            $('#monthSelect').datepicker('setDate',  new Date().getFullYear() + '-' +  (new Date().getMonth()+1).toString());
            $(".date_wise").hide();
            $(".year_wise").hide();
        });

        $('#date_type, #daily').on('change', function(){
            $(".date_wise").show();
            $('#end_date').datepicker("setDate", new Date());
            $('#monthSelect').datepicker("setDate", null);
            $('#yearSelect').datepicker("setDate", null);
            $(".month_wise").hide();
            $(".year_wise").hide();
        });

        $('#indicatorType').on('change', function(){
            if($('#indicatorType').val()=="NRC")
            {
                $("#unionName").hide();
                $("#serType").show();
                $("#service_by").show();
                $("#sba_nsba").hide();
                $("#delivery_place").hide();
                loadServiceTable(serviceTypeJS1);
            }
            else if($('#indicatorType').val()=="HBP")
            {
                $("#unionName").show();
                $("#serType").show();
                $("#service_by").hide();
                $("#sba_nsba").hide();
                $("#delivery_place").hide();
                loadServiceTable(serviceTypeJS2);
            }
            else if($('#indicatorType').val()=="DAN")
            {
                $("#unionName").show();
                $("#serType").hide();
                $("#service_by").hide();
                //$("#sba_nsba").show();
                $("#delivery_place").show();
                loadServiceTable(serviceTypeJS2);
            }else if($('#indicatorType').val()=="LBW"){
                $("#unionName").show();
                $("#serType").hide();
                $("#service_by").hide();
                //$("#sba_nsba").show();
                $("#delivery_place").show();
//                loadServiceTable(serviceTypeJS2);
            }
        });

		providerType1 = '[{"999":"Super Admin","998":"Admin","994":"Admin MNE","997":"Report Viewer","14":"MOMCH","15":"UFPO","19":"UPFA","18":"AUFPO"}]';
		providerTypeJS1 = JSON.parse(providerType1);

		providerType2 = '[{"4":"FWV","5":"S.A.C.M.O (FP)","6":"S.A.C.M.O (HS)","17":"Midwife","101":"PARAMEDIC"}]';
		providerTypeJS2 = JSON.parse(providerType2);

        providerType3 = '[{"14":"MOMCH","15":"UPFPO","19":"UPFA","18":"AUFPO"}]';
        providerTypeJS3 = JSON.parse(providerType3);

        providerType4 = '[{"997":"Report Viewer","14":"MOMCH","15":"UPFPO","19":"UPFA","18":"AUFPO"}]';
        providerTypeJS4 = JSON.parse(providerType4);
		
		if($("#reportType").val()=="13" || $("#reportType").val()=="23")
			loadProviderType(providerTypeJS2);
		else if($("#reportType").val()=="12" && (($("#acc_ty").val()).trim()=="997" || ($("#acc_ty").val()).trim()=="996"))
            loadProviderType(providerTypeJS3);
        else if($("#reportType").val()=="12" && ($("#acc_ty").val()).trim()=="998" && ($("#acc_ty").val()).trim()=="994")
            loadProviderType(providerTypeJS4);
		else
			loadProviderType(providerTypeJS1);

		function loadProviderType(providerTypeJS){
			$("#providerType").empty();
			var output = "<option value ='' selected>All</option>";

			$.each(providerTypeJS[0], function(key, val) {
               output = output + "<option value = " + key + ">" + val + "</option>";
           });

				$("#providerType").append(output);
		}

		$( document ).ready(function() {
			$("#facilityType").empty();

			var output = "<option value ='' selected></option>";

			$.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + '<option value = "' + key + '_' + val + '">' + val + '</option>';
			});

			$("#facilityType").append(output);
		});

		// $('#facilityType').on('change', function(){
		// 	if($('#facilityType').val()=="5_UHC")
		// 		$("#unionName").hide();
		// 	else if($('#facilityType').val()=="7_DH" || $('#facilityType').val()=="6_MCWC"){
		// 		$("#unionName").hide();
		// 		$("#upazilaName").hide();
		// 	}
		// 	else{
		// 		$("#unionName").show();
		// 		$("#upazilaName").show();
		// 	}
		// });

		$("#RdivName, #RzillaName, #RupaZilaName, #facilityType").on('change', function(){
            $("#facilityName").empty();
		});

		var showFacilityReportIds = ['4', '16', '17', '18','19','20','21'];

		// $("#RzillaName").on('change', function(){
		// 	if(($('#facilityType').val()=="6_MCWC" || $('#facilityType').val()=="7_DH") && showFacilityReportIds.indexOf($("#reportType").val())>=0)
		// 		getFacilityInfo();
		// });
		// $("#RupaZilaName").on('change', function(){
		// 	if($('#facilityType').val()=="5_UHC" && showFacilityReportIds.indexOf($("#reportType").val())>=0)
		// 		getFacilityInfo();
		// });
		$("#RunionName").on('change', function(){
			// if($('#facilityType').val()!="5_UHC" && $('#facilityType').val()!="6_MCWC" && $('#facilityType').val()!="7_DH" && showFacilityReportIds.indexOf($("#reportType").val())>=0)
				getFacilityInfo();
		});

		function getFacilityInfo(){
		
			//alert("Zillaid="+ $('#RzillaName').val());
			//alert("Upazilaid="+ $('#RupaZilaName').val());
			
			var forminfo = new Object();
			forminfo.zillaid = $('#RzillaName').val();
			forminfo.upazilaid = $('#RupaZilaName').val();
			forminfo.unionid = $('#RunionName').val();
			if($('#facility_type').is(':visible'))
				forminfo.facilityType = $('#facilityType').val().split("_")[0];
			else if($('#provider_facility_type').is(':visible'))
				forminfo.facilityType = $('#providerFacility').val().split("_")[0];
			else
                forminfo.facilityType = 0;

			forminfo.type = "6";

			$.ajax({
	           type: "POST",
	           url:"getFacilityInfo",
	           timeout:60000, //60 seconds timeout
	           data:{"forminfo":JSON.stringify(forminfo)},
	           success: function (response) {
	        	   console.log(response);
	        	   facilityJS = JSON.parse(response);
                   $("#facilityName").empty();
					var output = "";

					$.each(facilityJS, function(key, val) {
						output = output + "<option value = " + key + ">" + val + "</option>";
					});

					$("#facilityName").append(output);
	           },
	           complete: function() {
	        	   preventMultiCall = 0;
	           },
	           error: function(xhr, status, error) {
                   checkInternetConnection(xhr)
	           }
			});
		}

		/*$("#RunionName").on('change', function(){
				var forminfo = new Object();
				forminfo.zillaid = $('#RzillaName').val();
				forminfo.upazilaid = $('#RupaZilaName').val();
				forminfo.unionid = $('#RunionName').val();
				forminfo.facilityType = "0";
				forminfo.type = "6";

				$.ajax({
		           type: "POST",
		           url:"saveProviderInfo",
		           timeout:60000, //60 seconds timeout
		           data:{"forminfo":JSON.stringify(forminfo)},
		           success: function (response) {
		        	   console.log(response);
		        	   facilityJS = JSON.parse(response);
		        	   $("#facilityName").empty();
					   var output = "";

						$.each(facilityJS, function(key, val) {
							output = output + "<option value = " + key + ">" + val + "</option>";
						});

						$("#facilityName").append(output);
		           },
		           complete: function() {
		        	   preventMultiCall = 0;
		           },
		           error: function(xhr, status, error) {
						alert(xhr.status);
		        	   	alert(status);
						//alert(error);
		           }
				});
		});*/

        $('#yearSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            viewMode: "years",
            minViewMode: "years",
            format: 'yyyy'
        }).datepicker('setDate',  new Date().getFullYear().toString());

        $('#monthSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            minViewMode: 1,
            format: 'yyyy-mm'
        }).datepicker('setDate',  new Date().getFullYear() + '-' +  (new Date().getMonth()+1).toString());

        $('#start_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
        $('#end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });

	</script>

</div>
        
        
        
