<script type="text/javascript">
    function printDataMIS3()
    {
        var hide_1 = 0;
        var hide_2 = 0;
        var hide_3 = 0;
        var hide_4 = 0;
        var page_no = 0;
        if($("#table_1").css('display') == 'none')
            hide_1 = 1;
        if($("#table_2").css('display') == 'none')
            hide_2 = 1;
        if($("#table_3").css('display') == 'none')
            hide_3 = 1;
        if($("#table_4").css('display') == 'none')
            hide_4 = 1;
        if($(".page_no").css('display') == 'none')
            page_no = 1;

        $("#table_1").show();
        $("#table_2").show();
        $("#table_3").show();
        $("#table_4").show();
        $(".page_no").show();

        //var divToPrint=document.getElementById("printDiv");
        var header_1=document.getElementById("table_header_1");
        var header_2=document.getElementById("table_header_2");
        var table_1=document.getElementById("table_1");
        var table_2=document.getElementById("table_2");
        var table_3=document.getElementById("table_3");
        var table_4=document.getElementById("table_4");
        newWin= window.open("");
        //newWin.document.write(divToPrint.outerHTML);
        newWin.document.write(header_1.outerHTML);
        newWin.document.write(header_2.outerHTML);
        newWin.document.write(table_1.outerHTML);
        newWin.document.write(table_2.outerHTML);
        newWin.document.write(table_3.outerHTML);
        newWin.document.write(table_4.outerHTML);
        newWin.print();
        newWin.close();

        if(hide_1==1)
            $("#table_1").hide();
        if(hide_2==1)
            $("#table_2").hide();
        if(hide_3==1)
            $("#table_3").hide();
        if(hide_4==1)
            $("#table_4").hide();
        if(page_no==1)
            $(".page_no").hide();
    }

    function printData()
    {
        //var tableToPrint=document.getElementById("reportDataTable");
        var tableToPrint=document.getElementsByClassName("reportExport")[0];
        newWin= window.open("");
        newWin.document.write(tableToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    $('#btnPrint').on('click',function(){
        if($('#reportType').val()==4)
            printDataMIS3();
        else
            printData();
    });
</script>

<style>
    .td_color
    {
        background-color: #F2DEDE !important;
    }

    .rot
    {
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        white-space: nowrap;
        width:40px;
        margin-top: 100px;
    }

    /*thead, tbody { display: block; }

    tbody {
        height: 400px;
        overflow-y: auto;
        overflow-x: hidden;
    }*/

    /*.fixed_header tr {
    width: 100%;
    display: inline-table;
    height:60px;
    table-layout: fixed;
    overflow: auto;
    }

    .fixed_header table {
     height:500px;
     overflow: auto;
     display: -moz-groupbox;
    }
    .fixed_header tbody{
      overflow: auto;
      height: 450px;
      width: 100%;
      position: absolute;
    }*/

    .fixedHeader-floating{
        top: 7% !important;
    }
    .w3-container:after,.w3-container:before,.w3-panel:after,.w3-panel:before,.w3-row:after,.w3-row:before,.w3-row-padding:after,.w3-row-padding:before,
    .w3-container,.w3-panel{padding:0.01em 20px}
    /*tbody, thead tr {*/
    /*display: table;*/
    /*width: 100%;*/
    /*table-layout: fixed;*/
    /*}*/
</style>
<%--<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>--%>
<%--&lt;%&ndash;<script src="library/js/jquery.dataTables.min.js"></script>&ndash;%&gt;--%>
<%--<script src="library/js/dataTables.fixedHeader.min.js"></script>--%>
<%--<script src="library/js/dataTables.responsive.min.js"></script>--%>
<%--<script src="library/bootstrap/js/responsive.bootstrap.min.js"></script>--%>

<% if ("3".equals(request.getAttribute("type")) && "1".equals(request.getAttribute("methodType"))) { %>
<div class="w3-container">
    <div class="w3-row">
        <div class="" id="presentationType" style="display: block;">
            <div style="text-align: center;">
                <%--<div class="col-md-4">--%>
                    <%--<div class="info-box bg-white clickable" style="background-color: rgb(255, 255, 255);" data="mapview">--%>
                            <%--<span class="info-box-icon bg-gray" id="mapImgBackground">--%>
                                <%--<img src="image/map.jpg" alt="">--%>
                            <%--</span>--%>

                        <%--<div class="info-box-content">--%>
                            <%--<span class="info-box-text"><h4></h4></span><br>--%>
                            <%--<span class="info-box-number">Map View</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>

                <div class="col-md-6">
                    <div class="info-box bg-white clickable" style="background-color: rgb(0, 172, 214);" data="graphview">
                            <span class="info-box-icon bg-aqua" id="graphImgBackground">
                                <img id="graphImg" src="image/graph.png" alt="">
                            </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><h4></h4></span><br>
                            <span class="info-box-number">Graph View</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="info-box bg-white clickable" style="background-color: rgb(255, 255, 255);" data="tableview">
                            <span class="bg-gray info-box-icon" id="tableImgBackground">
                                <img src="image/table.jpg" alt="">
                            </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><h4></h4></span><br>
                            <span class="info-box-number">Table View</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="graphview" class="w3-container tab" >
        <div class="tile-body nopadding" id="graphTable">
            <div id="chartContainer" style="height: 600px; width: 100%; overflow-x: hidden;">
            </div>
        </div>
    </div>

    <div id="tableview" class="w3-container tab" style="display:none">
        <div class="tile-body nopadding" id="reportTable">
            <%--<div class="row">--%>
            <%--<div class="col-md-12">--%>
            <%--<div class="dropdown" style="float: right; margin-right: 2%;">--%>
            <%--<button class="btn btn-primary" type="button" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i> Print</button>--%>
            <%--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As--%>
            <%--<span class="caret"></span></button>--%>
            <%--<ul class="dropdown-menu">--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a></li>--%>
            <%--</ul>--%>
            <%--</div>--%>
            <%--</div>--%>
            <%--</div>--%>
            <div class="row">
                <div class="col-md-12">
                    <div id="printDiv" style="margin-top: 2%;" >
                        <% out.println(request.getAttribute("Result")); %>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<div id="mapview" class="w3-container tab" style="display:none">--%>
        <%--<link rel="stylesheet" href="css/map2.css">--%>
        <%--<div class="row">--%>
            <%--<div class="col-md-12">--%>
                <%--<h2 class="map_heading"></h2>--%>
            <%--</div>--%>
            <%--<div class="col-md-12">--%>
                <%--<div class="row">--%>
                    <%--<div class="col-md-1">--%>
                        <%--&lt;%&ndash;<a href="javascript:void(0)" onclick="prev_dis()" class="btn btn-info prev_dis" style="display: none;">District</a>&ndash;%&gt;--%>
                    <%--</div>--%>
                    <%--<div class="col-md-1">--%>
                        <%--&lt;%&ndash;<a href="javascript:void(0)" onclick="prev_upz()" class="btn btn-info prev_upz" style="display: none;">Upazila</a>&ndash;%&gt;--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class="col-md-12">--%>
                <%--<div class="row">--%>
                    <%--<div class="col-md-8">--%>
                        <%--<div class="row">--%>
                            <%--<div class="col-md-2">--%>
                                <%--<div class="row">--%>
                                    <%--<div class="col-md-12 union_legend" style="margin-top: 5%; display: none;">--%>
                                        <%--<dl>--%>
                                            <%--<b>Category : </b><br>--%>
                                            <%--<!-- <dt class="green"></dt> -->--%>
                                            <%--<dt><img src="image/FWC_CAT_A.png" width="15"></dt>--%>
                                            <%--<dd>A</dd><br>--%>
                                            <%--<!-- <dt class="yellow"></dt> -->--%>
                                            <%--<dt><img src="image/FWC_CAT_B.png" width="15"></dt>--%>
                                            <%--<dd>B</dd><br>--%>
                                            <%--<!-- <dt class="red"></dt> -->--%>
                                            <%--<dt><img src="image/FWC_CAT_C.png" width="15"></dt>--%>
                                            <%--<dd>C</dd><br>--%>
                                            <%--<dt><img src="image/UHC.png" width="15"></dt>--%>
                                            <%--<dd>UHC</dd><br>--%>
                                        <%--</dl>--%>
                                    <%--</div>--%>

                                    <%--<div class="col-md-12 zilla_legend" style="margin-top: 5%; display: none;">--%>
                                        <%--<dt><img src="image/Implemented_Area.gif" height="40" width="40"></dt>--%>
                                        <%--<dd>eMIS Area</dd><br>--%>
                                        <%--</dl>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                            <%--<div class="col-md-10">--%>
                                <%--<map>--%>
                                    <%--<script src="js/map2.js"></script>--%>
                                <%--</map>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-md-4">--%>
                        <%--<div class="row" >--%>
                            <%--<div class="col-md-12">--%>
                                <%--<div class="tooltip_view"></div>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</div>--%>
        <%--<div class="mouse_hover"></div>--%>
    <%--</div>--%>
</div>
<% } else if ("1".equals(request.getAttribute("type")) || "11".equals(request.getAttribute("type")) || "22".equals(request.getAttribute("type")) || "30".equals(request.getAttribute("type")) || ("3".equals(request.getAttribute("type")) && ("2".equals(request.getAttribute("methodType")) || "3".equals(request.getAttribute("methodType")) || ("6".equals(request.getAttribute("methodType")))))) { %>
<div class="w3-container">
    <div class="w3-row">
        <div class="" id="presentationType" style="display: block;">
            <div style="text-align: center;">
                <div class="col-md-6">
                    <div class="info-box bg-white clickable" style="background-color: rgb(0, 172, 214);" data="graphview">
                            <span class="info-box-icon bg-aqua" id="graphImgBackground">
                                <img id="graphImg" src="image/graph.png" alt="">
                            </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><h4></h4></span><br>
                            <span class="info-box-number">Graph View</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="info-box bg-white clickable" style="background-color: rgb(255, 255, 255);" data="tableview">
                            <span class="bg-gray info-box-icon" id="tableImgBackground">
                                <img src="image/table.jpg" alt="">
                            </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><h4></h4></span><br>
                            <span class="info-box-number">Table View</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="graphview" class="w3-container tab" >
        <div class="tile-body nopadding" id="graphTable">
            <div id="chartContainer" style="height: 600px; width: 100%; overflow-x: hidden;">
            </div>
        </div>
    </div>

    <div id="tableview" class="w3-container tab" style="display:none">
        <div class="tile-body nopadding" id="reportTable">
            <%--<div class="row">--%>
            <%--<div class="col-md-12">--%>
            <%--<div class="dropdown" style="float: right; margin-right: 2%;">--%>
            <%--<button class="btn btn-primary" type="button" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i> Print</button>--%>
            <%--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As--%>
            <%--<span class="caret"></span></button>--%>
            <%--<ul class="dropdown-menu">--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>--%>
            <%--<li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'pdf',jspdf: {orientation: 'l',margins: {right: 10, left: 10, top: 40, bottom: 40},autotable: {tableWidth: 'auto'}}});"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a></li>--%>
            <%--</ul>--%>
            <%--</div>--%>
            <%--</div>--%>
            <%--</div>--%>
            <div class="row">
                <div class="col-md-12">
                    <div id="printDiv" style="margin-top: 2%;" >
                        <% out.println(request.getAttribute("Result")); %>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mapview" class="w3-container tab" style="display:none">
        <link rel="stylesheet" href="css/map2.css">
        <div class="row">
            <div class="col-md-12">
                <h2 class="map_heading"></h2>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <%--<a href="javascript:void(0)" onclick="prev_dis()" class="btn btn-info prev_dis" style="display: none;">District</a>--%>
                    </div>
                    <div class="col-md-1">
                        <%--<a href="javascript:void(0)" onclick="prev_upz()" class="btn btn-info prev_upz" style="display: none;">Upazila</a>--%>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-12 union_legend" style="margin-top: 5%; display: none;">
                                        <dl>
                                            <b>Category : </b><br>
                                            <!-- <dt class="green"></dt> -->
                                            <dt><img src="image/FWC_CAT_A.png" width="15"></dt>
                                            <dd>A</dd><br>
                                            <!-- <dt class="yellow"></dt> -->
                                            <dt><img src="image/FWC_CAT_B.png" width="15"></dt>
                                            <dd>B</dd><br>
                                            <!-- <dt class="red"></dt> -->
                                            <dt><img src="image/FWC_CAT_C.png" width="15"></dt>
                                            <dd>C</dd><br>
                                            <dt><img src="image/UHC.png" width="15"></dt>
                                            <dd>UHC</dd><br>
                                        </dl>
                                    </div>

                                    <div class="col-md-12 zilla_legend" style="margin-top: 5%; display: none;">
                                        <dt><img src="image/Implemented_Area.gif" height="40" width="40"></dt>
                                        <dd>eMIS Area</dd><br>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <map>
                                    <script src="js/map2.js"></script>
                                </map>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row" >
                            <div class="col-md-12">
                                <div class="tooltip_view"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mouse_hover"></div>
    </div>
</div>
<% } else { %>
<div class="tile-body nopadding" id="reportTable">
    <div class="row">
        <div class="col-md-12">
            <div class="dropdown" style="float: right; margin-right: 2%;">
                <button class="btn btn-primary" type="button" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
                <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
                    <%--<li><a class="btn btn-default buttons-pdf buttons-html5" tabindex="0" aria-controls="reportTable" href="#"><span>PDF</span></a></li>--%>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="printDiv" style="margin-top: 2%;">
                <% out.println(request.getAttribute("Result")); %>
            </div>
        </div>
        <div id="model_box" title="Basic dialog"></div>
    </div>
</div>
<% } %>
<input type="hidden" id="graphResultSet" value='<% out.println(request.getAttribute("Graph")); %>'>
<input type="hidden" id="mapResultSet" value='<% out.println(request.getAttribute("Map")); %>'>
<input type="hidden" id="divisionSet" value='<% out.println(request.getAttribute("Division")); %>'>
<script src="js/lib/ddtf.js"></script>
<script>
    $('.filterdTable').ddTableFilter();
</script>

<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>

<script type="text/javaScript">
    $('.clickable').click(function () {
        $('.clickable').css({"background-color": "rgb(255, 255, 255)"});
        $(this).css({"background-color": "rgb(0, 172, 214)"});
        $('.clickable > span').removeClass("bg-aqua").addClass("bg-gray");
        $(this).find("span:first").removeClass("bg-gray").addClass("bg-aqua");
        $('.clickable span img').each(function(index, element) {
            var url = $(element).prop("src").replace("png","jpg");
            $(element).attr("src",url);
        });
        url = $(this).find("span img").prop("src").replace("jpg","png");
        $(this).find("span img").attr("src",url);
        openTab($(this).attr("data"));
    });
    function openTab(tabName) {
        var tabcontent;
        tabcontent = document.getElementsByClassName("tab");
        for (var i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        document.getElementById(tabName).style.display = "block";
    }
    function doExport(selector, params) {
        var options = {
            //ignoreRow: [1,11,12,-2],
            //ignoreColumn: [0,-1],
            //pdfmake: {enabled: true},
            tableName: 'Service Statistics',
            worksheetName: 'Service Statistics'
        };

        $.extend(true, options, params);

        $(selector).tableExport(options);
    }

    function DoOnCellHtmlData(cell, row, col, data) {
        var result = "";
        if (data != "") {
            var html = $.parseHTML( data );

            $.each( html, function() {
                if ( typeof $(this).html() === 'undefined' )
                    result += $(this).text();
                else if ( $(this).is("input") )
                    result += $('#'+$(this).attr('id')).val();
                else if ( $(this).is("select") )
                    result += $('#'+$(this).attr('id')+" option:selected").text();
                else if ( $(this).hasClass('no_export') !== true )
                    result += $(this).html();
            });
        }
        return result;
    }

    function DoOnMsoNumberFormat(cell, row, col) {
        var result = "";
        if (row > 0 && col == 0)
            result = "\\@";
        return result;
    }

</script>



<script>
    $(document).ready(function() {
        //$('#reportDataTable').DataTable();


        $(document).ready(function() {
            var filterFunc = function (sData) {
                return sData.replace(/\n/g," ").replace( /<.*?>/g, "" );
            };
            var table = $('#reportDataTable').dataTable({
                "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
                "processing": true,
                "paging": true,
                "lengthMenu": [ 10, 25, 50, 75, 100 ],
                "language": {
                    "info": "Total _TOTAL_",
                    "infoEmpty": "Total _TOTAL_",
                    "zeroRecords": "No records",
                    "lengthMenu": "Show _MENU_ entries",
                    "searchIcon":"",
                    "paginate": {
                        "first": "First",
                        "last": "Last",
                        "next": "Next",
                        "previous": "Prev"
                    }
                },
                "initComplete": function(oSettings, json) {
                    $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                    $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                    $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");

                },
                "buttons": ['copy',{
                    extend: 'excelHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'csvHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    title:"Service Statistics",
                    pageSize: 'LEGAL'
                },{
                    extend: 'print',
                    text: 'Print',
                    autoPrint: true
                }
                ]
            });
        });
        var table = $('#reportDataTableFilter').dataTable({
            "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
            "processing": true,
            "paging": true,
            "lengthMenu": [ 10, 25, 50, 75, 100 ],
            "language": {
                "info": "Total _TOTAL_",
                "infoEmpty": "Total _TOTAL_",
                "zeroRecords": "No records",
                "lengthMenu": "Show _MENU_ entries",
                "searchIcon":"",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Prev"
                }
            },
            "initComplete": function(oSettings, json) {
                $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );

            },
            "buttons": ['copy',{
                extend: 'excelHtml5',
                title:"Service Statistics"
            },{
                extend: 'csvHtml5',
                title:"Service Statistics"
            },{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title:"Service Statistics"
            }, {
                extend: 'print',
                text: 'Print',
                autoPrint: true
            }
            ]
    } );

    /*$(document).on("click", '#approveMIS3' ,function (){
        submitMIS3Report("1");
    });

    $(document).on("click", '#rejectMIS3' ,function (){
        submitMIS3Report("2");
    });

    function submitMIS3Report(submission_status){
        var url = "";
        var getParams = "facilityId="+$('#facilityName').val()+"&report1_zilla="+$('#report1_zilla').val()+"&report1_start_date="+$('#monthSelect').val()+"&submissionStatus="+submission_status+"&supervisorComment="+$('#supervisor_comment').val();
        var params = $.base64.encode(getParams);
        url = "MIS3Submit?"+params;

        $.ajax({
            type: "GET",
            url:url,
            timeout:60000, //60 seconds timeout
            //data:{"forminfo":JSON.stringify(forminfo)},
            success: function (response) {
                if(response=='true'){
                    $("#model_box").dialog({
                        modal: true,
                        title: "Message",
                        width: 420,
                        open: function () {
                            $(this).html("<b style='color:green'>Your MIS3 Submit Successfully.</b>");
                        },
                        buttons : {
                            Ok : function() {
                                $(this).dialog('close');
                                //reloadReport();
                                $( ".approval1" ).trigger( "click" );
                            }
                        }
                    });
                }
                else{
                    $('#model_box').dialog({
                        modal: true,
                        title: "Message",
                        width: 420,
                        open: function () {
                            $(this).html("<b style='color:red'>Your MIS3 not submitted successfully. Please re-submit your MIS3.</b>");
                        },
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
    }*/

    function reloadReport(){

        loadGif();

        //var preventMultiC = 1;
        var reportObj = new Object();
        reportObj.report1_zilla = $('#RzillaName').val();
        reportObj.facilityId = $('#facilityName').val();
        reportObj.supervisorId = ($("#ProviderID").text()).trim();
        reportObj.report1_month = $('#monthSelect').val();
        console.log(reportObj);
        $.ajax({
            type: "POST",
            url:"submittedmis3",
            timeout:60000, //60 seconds timeout
            data:{"approvalInfo":JSON.stringify(reportObj)},
            success: function (response) {
                //alert(response);
                $(".box-default").addClass("collapsed-box");
                $(".search_form").hide();
                $(".box-tools").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 1000, "easeInOutQuad" );

                $("#reportTable").remove();
                $("#table_row").show();
                $("#table_append" ).html(response);

                $("#exportOption").hide();

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");
                //alert(bottom);

                $('#table_row')[0].scrollIntoView( true );
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //alert(preventMultiC);
        return preventMultiCall;
    }
    });

    /******** Edit Provider Information *********/
    function editProvider(providercode,zillaid,providertype)
    {
        //alert("ProviderCode="+ providercode);
		 //alert("zilla="+ zillaid);
		  //alert("Providertype="+ providertype);

        var provObj = new Object();
        provObj.providercode = providercode;
        provObj.zillaid = zillaid;
        provObj.providertype = providertype;

        $.ajax({
			cache:false,
            type: "POST",
            url:"editProvider",
            //data:{ providercode: providercode,zillaid: zillaid },
            data:{"provfo":JSON.stringify(provObj)},
            timeout:60000,
            success: function (response) {

               // $(".remove_div").remove();
               // $( "#append_report" ).html(response);
                $("#model_box").dialog({
                    modal: true,
                    title: "Profile Information Edit",
                    width: 460,
                    open: function () {
                        $(this).html(response);
                    },
                    buttons : {
                       "Close" : function() {
                            $(this).dialog("close");
							$(".ui-dialog-content").dialog().dialog("destroy");
							$(".ui-dialog-content").dialog().dialog("close");
							$("#provider_div").hide();
                       }
                    }
                });
                return false;
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //return false;
    }

    /******** Add CSBA *********/
    function addCSBA(zillaid,upazilaid,unionid,providercode,version,comunityactive)
    {
        var provObj = new Object();
        provObj.providercode = providercode;
        provObj.zillaid = zillaid;
        provObj.upazilaid = upazilaid;
        provObj.unionid = unionid;
        provObj.version = version;
        provObj.comunityactive = comunityactive;

        $.ajax({
            cache:false,
            type: "POST",
            url:"addcsba",
            data:{"provInfo":JSON.stringify(provObj)},
            timeout:60000,
            success: function (response) {
                $("#model_box").dialog({
                    modal: true,
                    title: "Add CSBA",
                    width: 460,
                    open: function () {
                        $(this).html("you have added CSBA");
                    },
                    buttons : {
                        "Close" : function() {
                            $(this).dialog("close");
                            $(".ui-dialog-content").dialog().dialog("destroy");
                            $(".ui-dialog-content").dialog().dialog("close");
                            $("#provider_div").hide();
                        }
                    }
                });
                return false;
            },
            complete: function() {
                preventMultiCall = 0;

            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
    }


 /******** Edit Facility Information *********/
    function editfacility(facilityid,zillaid)
    {
          //alert("facilityid="+ facilityid);
		 // alert("zilla="+ zillaid);
		// alert("upazila="+ upazila);
		// alert("facilityType="+ facilityType);
		// alert("facilityName="+ facilityName);
		  //alert("Providertype="+ providertype);

        var provObj = new Object();
		provObj.facilityId = facilityid;
		provObj.zillaid = zillaid;
		//provObj.upazila = upazila;
		//provObj.facilityType = facilityType;
		//provObj.facilityName = facilityName;
       

        $.ajax({
			cache:false,
            type: "POST",
            url:"modifyFacilityInfo",
            //data:{ providercode: providercode,zillaid: zillaid },
            data:{"facilityinfo":JSON.stringify(provObj)},
            timeout:60000,
            success: function (response) {

               // $(".remove_div").remove();
               // $( "#append_report" ).html(response);
                $("#model_box").dialog({
                    modal: true,
                    title: "Facility Information Edit",
                    width: 568,
					open: function () {
                        $(this).html(response);
                    },
                    buttons : {
                       "Close" : function() {
                            $(this).dialog("close");
							
							 
							$(".ui-dialog-content").dialog().dialog("destroy");
							$(".ui-dialog-content").dialog().dialog("close");
							$("#facility_div").hide();
                       }
                    }
                });
                return false;
            },
            complete: function() {
                preventMultiCall = 0;
            },
            error: function(xhr, status, error) {
                checkInternetConnection(xhr)
            }
        });
        //return false;
    }



</script>
                 