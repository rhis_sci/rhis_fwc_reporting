<%@ page import="org.json.JSONObject" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <title>Routine Health Information System</title> -->
    <title>e-MIS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />

    <link rel="SHORTCUT ICON" href="image/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="image/favicon.ico" type="image/ico" />

    <link rel="stylesheet" href="css/style.css">
    <!-- progress bar -->
    <link rel="stylesheet" href="nprogress/nprogress.css">
    <script type="text/javascript" src="nprogress/nprogress.js"></script>

    <!-- <link rel="stylesheet" href="css/scrollbar.css"> -->

    <link rel="stylesheet" href="css/jquery-ui.min.css">

    <link rel="stylesheet" href="library/css/skins/_all-skins.min.css">

    <!-- <link rel="stylesheet" href="css/jquery-ui.css"> -->

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="library/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="library/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="library/css/AdminLTE.min.css">


    <!-- jQuery 2.1.3 -->
    <script type="text/javascript" src="js/lib/jquery-2.2.4.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="library/bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script
            src="library/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="library/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="library/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="library/js/app.min.js"></script>


    <script type="text/javascript" src="js/lib/moment.js"></script>
    <script type="text/javascript" src="js/utilities.js"></script>
    <!--<script type="text/javascript" src="json/division.json"></script>-->

    <script type="text/javascript" src="js/init.js"></script>


    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <link href="//cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="//cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <!--<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="library/js/buttons.print.min.js"></script>

    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.bootstrap.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <!--<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js" type="text/javascript"></script>-->
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js" type="text/javascript"></script>
    <!--<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js" type="text/javascript"></script>-->

    <script type="text/javascript" src="library/js/pdfmake.min.js"></script>
    <script type="text/javascript" src="library/js/vfs_fonts.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Datatable -->
    <link rel="stylesheet" href="library/bootstrap/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="library/bootstrap/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="library/bootstrap/css/responsive.bootstrap.min.css">

    <!--<script src="library/js/jquery.dataTables.min.js"></script>-->
    <script src="library/bootstrap/js/dataTables.bootstrap.min.js"></script>
    <script src="library/js/dataTables.fixedHeader.min.js"></script>
    <script src="library/js/dataTables.responsive.min.js"></script>
    <script src="library/bootstrap/js/responsive.bootstrap.min.js"></script>
    <style>
        .box-footer{
            margin-top: 10%;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini" id="emis-site-body" style="background: #d2d6de;">
    <div class="box-body table-responsive no-padding">
        <div class="tile-body nopadding">
            <div class="row" id="table_report_row">
                <div class="col-md-12">
                    <%
                        JSONObject result = new JSONObject((String) request.getAttribute("Result"));
                        JSONObject result_aggregated = new JSONObject(result.get("Aggregated").toString());
                        String result_details = result.get("Individual").toString();
                    %>
                    <div class="row" style="margin-left: 1%; margin-right: 1%">
                        <div class="col-md-4" style="">
                            <div class="small-box bg-blue">
                                <div class="inner">
                                    <h3><i class="fa fa-hospital-o" aria-hidden="true"></i> <%= result_aggregated.get("total_facility") %></h3>
                                    <p>Total Facility</p>
                                </div>
                                <%--<a href="#" id="0" class="small-box-footer">View details <i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><i class="fa fa-list-alt" aria-hidden="true"></i> <%= result_aggregated.get("submited") %></h3>
                                    <p>Submitted</p>
                                </div>
                                <%--<a href="#" id="0" class="small-box-footer">View details <i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3><i class="fa fa-check-square-o" aria-hidden="true"></i> <%= result_aggregated.get("approved") %></h3>
                                    <p>Approved</p>
                                </div>
                                <%--<a href="#" id="0" class="small-box-footer">View details <i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-md-3" style="">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><i class="fa fa-clock-o" aria-hidden="true"></i> <%= result_aggregated.get("approve_waiting") %></h3>
                                    <p>Waiting</p>
                                </div>
                                <%--<a href="#" id="0" class="small-box-footer">View details <i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-md-3" style="">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3><i class="fa fa-close" aria-hidden="true"></i> <%= result_aggregated.get("rejected") %></h3>
                                    <p>Rejected</p>
                                </div>
                                <%--<a href="#" id="0" class="small-box-footer">View details <i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-md-3" style="">
                            <div class="small-box bg-orange">
                                <div class="inner">
                                    <h3><i class="fa fa-close" aria-hidden="true"></i> <%= result_aggregated.get("not_submited") %></h3>
                                    <p>Not submitted</p>
                                </div>
                                <%--<a href="#" id="0" class="small-box-footer">View details <i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                        </div>
                        <div class="col-md-3" style="">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><i class="fa fa-clock-o" aria-hidden="true"></i> <%= result_aggregated.get("dhis2_submit") %></h3>
                                    <p>DHIS2 Submit Remaining</p>
                                </div>
                                <%--<a href="#" id="0" class="small-box-footer">View details <i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div id="printDiv" style="margin-top: 2%;">
                        <%= result_details %>
                    </div>
                </div>
            </div>

            <div class="row" id="table_row" style="display:none;">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title" id="table_title"></h3>
                        </div>
                        <button class="btn btn-info" id="back">Back</button>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding" id="table_append">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>

<script>

    function viewMIS3(zilla_id, facility_id, mis3_year_month){

        loadGif();

        var reportObj = new Object();
        reportObj.report1_zilla = zilla_id;
        reportObj.facilityId = facility_id;
        reportObj.report1_month = mis3_year_month;
        reportObj.approvalType = 1;
        reportObj.viewOnly = 1;

        $.ajax({
            type: "POST",
            url:"submittedApprovalList",
            timeout:60000, //60 seconds timeout
            data:{"approvalInfo":JSON.stringify(reportObj)},
            success: function (response) {
                //alert(response);
                $(".box-default").addClass("collapsed-box");

                $("#reportTable").remove();
                $("#table_row").show();
                $("#table_append" ).html(response);

                $("#exportOption").hide();

                var $el = $('.table-responsive');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height',bottom+"px");

                $('#table_row')[0].scrollIntoView( true );
            }
        });
    }

    $(document).on("click", '#back' ,function (){
        $('#table_report_row')[0].scrollIntoView( true );
    });

    $(document).ready(function() {
        var filterFunc = function (sData) {
            return sData.replace(/\n/g," ").replace( /<.*?>/g, "" );
        };
        var table = $('#reportDataTable').dataTable({
            "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
            "processing": true,
            "paging": true,
            "lengthMenu": [ 10, 25, 50, 75, 100 ],
            "language": {
                "info": "Total _TOTAL_",
                "infoEmpty": "Total _TOTAL_",
                "zeroRecords": "No records",
                "lengthMenu": "Show _MENU_ entries",
                "searchIcon":"",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Prev"
                }
            },
            "initComplete": function(oSettings, json) {
                $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");

            },
            "buttons": ['copy',{
                extend: 'excelHtml5',
                title:"Service Statistics"
            },{
                extend: 'csvHtml5',
                title:"Service Statistics"
            },{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                title:"Service Statistics",
                pageSize: 'LEGAL'
            },{
                extend: 'print',
                text: 'Print',
                autoPrint: true
            }
            ]
        });
    });
    var table = $('#reportDataTableFilter').dataTable({
        "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
        "processing": true,
        "paging": true,
        "lengthMenu": [ 10, 25, 50, 75, 100 ],
        "language": {
            "info": "Total _TOTAL_",
            "infoEmpty": "Total _TOTAL_",
            "zeroRecords": "No records",
            "lengthMenu": "Show _MENU_ entries",
            "searchIcon":"",
            "paginate": {
                "first": "First",
                "last": "Last",
                "next": "Next",
                "previous": "Prev"
            }
        },
        "initComplete": function(oSettings, json) {
            $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
            $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
            $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );

        },
        "buttons": ['copy',{
            extend: 'excelHtml5',
            title:"Service Statistics"
        },{
            extend: 'csvHtml5',
            title:"Service Statistics"
        },{
            extend: 'pdfHtml5',
            orientation: 'landscape',
            pageSize: 'LEGAL',
            title:"Service Statistics"
        }, {
            extend: 'print',
            text: 'Print',
            autoPrint: true
        }
        ]
    } );
</script>
