<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 3/7/2021
  Time: 4:01 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">
                    <!--geo location-->

                    <div class="form-group col-md-3" id="divisionName">
                        <label>Division</label>
                        <select class="form-control select2" id="RdivName" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group col-md-3" id="districtName">
                        <label>District</label>
                        <select class="form-control select2" id="RzillaName" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group col-md-3" id="upazilaName">
                        <label>Upazila</label>
                        <select class="form-control select2" id="RupaZilaName" style="width: 100%;">
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label class="control-label"></label>
                        <div class="input-group input-button">
                            <input type="hidden" id="reportType" value="<% out.print(request.getAttribute("type")); %>">
                            <div>
                                <button type="button" id="submitReport1" class="btn btn-primary">
                                    <i class="fa fa-send"></i> Submit
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>

    <script>

        $(".select2").select2();
    </script>
</div>
