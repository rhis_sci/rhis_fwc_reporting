<script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
      text: "Provider Wise MNC Graph"   
      },
      axisY:{
        title:"Service Number"   
      },
      animationEnabled: true,
      data: [{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Anamika Biswash",showInLegend: "true",dataPoints: [{ y: 16, label: "ANC"},{ y: 5, label: "PNC"},{ y: 5, label: "PNC-N"},{ y: 2, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Kamrun Nasrin",showInLegend: "true",dataPoints: [{ y: 30, label: "ANC"},{ y: 11, label: "PNC"},{ y: 11, label: "PNC-N"},{ y: 6, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Mahmuda Akther",showInLegend: "true",dataPoints: [{ y: 19, label: "ANC"},{ y: 4, label: "PNC"},{ y: 4, label: "PNC-N"},{ y: 4, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "RUBIA KHATUN",showInLegend: "true",dataPoints: [{ y: 0, label: "ANC"},{ y: 2, label: "PNC"},{ y: 2, label: "PNC-N"},{ y: 2, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Anita Banik",showInLegend: "true",dataPoints: [{ y: 23, label: "ANC"},{ y: 1, label: "PNC"},{ y: 1, label: "PNC-N"},{ y: 0, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Shanti Rani Deb",showInLegend: "true",dataPoints: [{ y: 21, label: "ANC"},{ y: 2, label: "PNC"},{ y: 2, label: "PNC-N"},{ y: 2, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Rahima Khatun",showInLegend: "true",dataPoints: [{ y: 39, label: "ANC"},{ y: 8, label: "PNC"},{ y: 7, label: "PNC-N"},{ y: 2, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Monowara Begum",showInLegend: "true",dataPoints: [{ y: 1, label: "ANC"},{ y: 0, label: "PNC"},{ y: 0, label: "PNC-N"},{ y: 0, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "SEEMA RANI DEB",showInLegend: "true",dataPoints: [{ y: 51, label: "ANC"},{ y: 5, label: "PNC"},{ y: 4, label: "PNC-N"},{ y: 2, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Jahanara Begum",showInLegend: "true",dataPoints: [{ y: 34, label: "ANC"},{ y: 2, label: "PNC"},{ y: 0, label: "PNC-N"},{ y: 0, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Meena Roy",showInLegend: "true",dataPoints: [{ y: 19, label: "ANC"},{ y: 0, label: "PNC"},{ y: 0, label: "PNC-N"},{ y: 0, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Shikha Banik",showInLegend: "true",dataPoints: [{ y: 33, label: "ANC"},{ y: 5, label: "PNC"},{ y: 5, label: "PNC-N"},{ y: 3, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Gita Rani Deb Nath",showInLegend: "true",dataPoints: [{ y: 17, label: "ANC"},{ y: 0, label: "PNC"},{ y: 0, label: "PNC-N"},{ y: 0, label: "Delivery"},] },{type: "stackedColumn",toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'><strong>{name}</strong></span>: {y}mn tonnes",name: "Lipi Rani Paul",showInLegend: "true",dataPoints: [{ y: 11, label: "ANC"},{ y: 0, label: "PNC"},{ y: 0, label: "PNC-N"},{ y: 0, label: "Delivery"},] },]
      ,
      legend:{
        cursor:"pointer",
        itemclick: function(e) {
          if (typeof (e.dataSeries.visible) ===  "undefined" || e.dataSeries.visible) {
	          e.dataSeries.visible = false;
          }
          else
          {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
      }
    });

    chart.render();
  }
  </script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.min.js"></script>
<div id="chartContainer" style="height: 300px; width: 100%;">
	</div>