<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <title>Routine Health Information System</title> -->
    <title>e-MIS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
            name="viewport">

    <link rel="SHORTCUT ICON" href="image/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="image/favicon.ico" type="image/ico" />

    <link rel="stylesheet" href="css/style.css">
    <!-- progress bar -->
    <link rel="stylesheet" href="nprogress/nprogress.css">
    <script type="text/javascript" src="nprogress/nprogress.js"></script>

    <!-- <link rel="stylesheet" href="css/scrollbar.css"> -->

    <link rel="stylesheet" href="css/jquery-ui.min.css">

    <link rel="stylesheet" href="library/css/skins/_all-skins.min.css">

    <!-- <link rel="stylesheet" href="css/jquery-ui.css"> -->

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="library/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="library/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="library/css/AdminLTE.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="library/select2/select2.min.css">


    <!-- jQuery 2.1.3 -->
    <script type="text/javascript" src="js/lib/jquery-2.2.4.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="library/bootstrap/js/bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="library/select2/select2.full.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script
            src="library/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="library/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="library/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="library/js/app.min.js"></script>
    <!-- Google Map js -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIJA8RDbXgFn-6qv2zlnKt5JGa4S3Y7wo"></script>


    <script type="text/javascript" src="js/lib/moment.js"></script>
    <script type="text/javascript" src="js/lib/combodate.js"></script>
    <script type="text/javascript" src="js/utilities.js"></script>
    <script type="text/javascript" src="json/zilla.json"></script>
    <script type="text/javascript" src="js/init.js"></script>
    <script type="text/javascript" src="library/js/canvasjs.min.js"></script>
    <script type="text/javascript" src="js/lib/d3.v3.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Datatable -->
    <link rel="stylesheet" href="library/bootstrap/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="library/bootstrap/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="library/bootstrap/css/responsive.bootstrap.min.css">

    <script src="library/js/jquery.dataTables.min.js"></script>
    <script src="library/bootstrap/js/dataTables.bootstrap.min.js"></script>
    <script src="library/js/dataTables.fixedHeader.min.js"></script>
    <script src="library/js/dataTables.responsive.min.js"></script>
    <script src="library/bootstrap/js/responsive.bootstrap.min.js"></script>

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="library/datepicker/datepicker.min.css">
    <!-- bootstrap datepicker -->
    <script src="library/datepicker/bootstrap-datepicker.min.js"></script>


    <link href='library/eventCalendar/css/fullcalendar.min.css' rel='stylesheet' />
    <link href='library/eventCalendar/css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='library/eventCalendar/js/moment_calendar.min.js'></script>
    <script src='library/eventCalendar/js/fullcalendar.min.js'></script>
    <script>

        $(document).ready(function() {

            $('#calendar').fullCalendar({
                defaultDate: '2018-03-12',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: [
                    {
                        title: 'All Day Event',
                        start: '2018-03-01'
                    },
                    {
                        title: 'Long Event',
                        start: '2018-03-07',
                        end: '2018-03-10'
                    },
                    {
                        id: 999,
                        title: 'Repeating Event',
                        start: '2018-03-09T16:00:00'
                    },
                    {
                        id: 999,
                        title: 'Repeating Event',
                        start: '2018-03-16T16:00:00'
                    },
                    {
                        title: 'Conference',
                        start: '2018-03-11',
                        end: '2018-03-13'
                    },
                    {
                        title: 'Meeting',
                        start: '2018-03-12T10:30:00',
                        end: '2018-03-12T12:30:00'
                    },
                    {
                        title: 'Lunch',
                        start: '2018-03-12T12:00:00'
                    },
                    {
                        title: 'Meeting',
                        start: '2018-03-12T14:30:00'
                    },
                    {
                        title: 'Happy Hour',
                        start: '2018-03-12T17:30:00'
                    },
                    {
                        title: 'Dinner',
                        start: '2018-03-12T20:00:00'
                    },
                    {
                        title: 'Birthday Party',
                        start: '2018-03-13T07:00:00'
                    },
                    {
                        title: 'Click for Google',
                        url: 'http://google.com/',
                        start: '2018-03-28'
                    }
                ]
            });

        });

    </script>
    <style>

        body {
            margin: 40px 10px;
            padding: 0;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }

    </style>

</head>
<body class="hold-transition skin-blue sidebar-mini"
      style="background: #d2d6de;">
<div id="wrapper">
    <!-- <div id="after_append"></div> -->

    <div class="login-box" id="remove_div">
        <div class="center-block">
            <br>
            <br>
            <br>
            <br>
        </div>
        <style>
            #bottom {
                background: #d2d6de;
            }
            .login-box-body{
                background: #fff;
                padding: 20px;
                border-top: 0;
                border: .5px solid #6baee0;
                box-shadow: 7px 7px 20px #888888;
                color: #666;
            }
        </style>
        <div id='calendar'></div>



        <div id="loading_div" class="loadPopup"></div>
        <%--<script type="text/javascript" src="js/lib/jquery-ui.min.js"></script>--%>


    </div>
    <!-- /.login-box -->
</div>

</body>
</html>
