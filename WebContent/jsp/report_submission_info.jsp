<%@ page import="org.json.JSONObject" %>
<div class="tile-body nopadding">
    <div class="row" style="padding:15px;">
        <%
            JSONObject result = new JSONObject((String) request.getAttribute("Result"));
            JSONObject result_aggregated = new JSONObject(result.get("Aggregated").toString());
            String result_details = result.get("Individual").toString();
        %>

        <div class="col-md-4 ">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-hospital-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" style="font-size: 18px;"><b>Facility </b><span
                            class="label label-info"><%= result_aggregated.get("total_facility") %></span></span>
                    <br>
                    <div style="border-top: solid 1px #DDD; padding-top:5px; font-size: 18px; font-weight: bold; padding-left: 0px;">
                        <div class="col-md-6" style="padding-left: 0px;">Active <span
                                class="label label-success"><%= result_aggregated.get("active_facility") %></span></div>
                        <div class="col-md-6">Inactive <span
                                class="label label-danger custom-text"><%= result_aggregated.get("inactive_facility") %></span>
                        </div>

                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="glyphicon glyphicon-folder-open"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" style="font-size: 18px;"><b>Submitted </b><span
                            class="label label-warning"><%= result_aggregated.get("submited") %></span></span>
                    <br>
                    <div style="border-top: solid 1px #DDD; padding-top:5px;font-size: 18px; font-weight: bold; padding-left: 0px;">
                        <!-- <div class="col-md-4">Waiting <span class="label label-warning pull-right custom-text"><%= result_aggregated.get("approve_waiting") %></span></div>-->
                        <div class="col-md-12" style="padding-left: 0px;">Not Submitted <span
                                class="label label-danger custom-text"><%= result_aggregated.get("not_submited") %></span>
                        </div>


                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-ok"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text" style="font-size: 18px;"><b>Approved </b><span
                            class="label label-success"><%= result_aggregated.get("approved") %></span></span>
                    <br>

                    <div style="border-top: solid 1px #DDD; padding-top:5px;font-size: 18px; font-weight: bold;padding-left: 0px;">

                        <div class="col-md-6" style="padding-left: 0px;">Rejected <span
                                class="label label-danger custom-text"><%= result_aggregated.get("rejected") %></span>
                        </div>
                        <div class="col-md-6">Waiting <span
                                class="label label-warning"><%= result_aggregated.get("approve_waiting") %></span></div>

                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div id="printDiv" style="margin-top: 2%; padding-left: 18px;">
            <%= result_details %>
        </div>

    </div>
</div>

<div class="row" id="table_row_mis3" style="display:none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title" id="table_title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding" id="table_append_mis3">
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>

<script>

    function viewMIS3(zilla_id, facility_id, mis3_year_month) {

        loadGif();

        var reportObj = new Object();
        reportObj.report1_zilla = zilla_id;
        reportObj.facilityId = facility_id;
        reportObj.report1_month = mis3_year_month;
        reportObj.approvalType = 1;
        reportObj.viewOnly = 1;

        $.ajax({
            type: "POST",
            url: "submittedApprovalList",
            timeout: 60000, //60 seconds timeout
            data: {"approvalInfo": JSON.stringify(reportObj)},
            success: function (response) {
                //alert(response);
                $(".box-default").addClass("collapsed-box");

                $("#reportTable").remove();
                $("#table_row_mis3").show();
                $("#table_append_mis3").html(response);

                $("#exportOption").hide();

                var $el = $('#wrapper');  //record the elem so you don't crawl the DOM everytime
                var bottom = $el.position().top + $el.outerHeight(true);
                $(".main-sidebar").css('height', bottom + "px");

                $('#table_row_mis3')[0].scrollIntoView(true);
            }
        });
    }

    $(document).ready(function () {
        var filterFunc = function (sData) {
            return sData.replace(/\n/g, " ").replace(/<.*?>/g, "");
        };
        var table = $('#reportDataTable').dataTable({
            "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
            "processing": true,
            "paging": true,
            "lengthMenu": [10, 25, 50, 75, 100],
            "language": {
                "info": "Total _TOTAL_",
                "infoEmpty": "Total _TOTAL_",
                "zeroRecords": "No records",
                "lengthMenu": "Show _MENU_ entries",
                "searchIcon": "",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Prev"
                }
            },
            "initComplete": function (oSettings, json) {
                $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");

            },
            "buttons": ['copy', {
                extend: 'excelHtml5',
                title: "Service Statistics"
            }, {
                extend: 'csvHtml5',
                title: "Service Statistics"
            }, {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                title: "Service Statistics",
                pageSize: 'LEGAL'
            }, {
                extend: 'print',
                text: 'Print',
                autoPrint: true
            }
            ]
        });
    });
    var table = $('#reportDataTableFilter').dataTable({
        "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
        "processing": true,
        "paging": true,
        "lengthMenu": [10, 25, 50, 75, 100],
        "language": {
            "info": "Total _TOTAL_",
            "infoEmpty": "Total _TOTAL_",
            "zeroRecords": "No records",
            "lengthMenu": "Show _MENU_ entries",
            "searchIcon": "",
            "paginate": {
                "first": "First",
                "last": "Last",
                "next": "Next",
                "previous": "Prev"
            }
        },
        "initComplete": function (oSettings, json) {
            $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
            $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
            $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        },
        "buttons": ['copy', {
            extend: 'excelHtml5',
            title: "Service Statistics"
        }, {
            extend: 'csvHtml5',
            title: "Service Statistics"
        }, {
            extend: 'pdfHtml5',
            orientation: 'landscape',
            pageSize: 'LEGAL',
            title: "Service Statistics"
        }, {
            extend: 'print',
            text: 'Print',
            autoPrint: true
        }
        ]
    });
</script>
