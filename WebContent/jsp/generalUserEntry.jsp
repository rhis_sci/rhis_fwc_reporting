<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<style>
    input[type="number"]::-webkit-outer-spin-button,
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type="number"] {
        -moz-appearance: textfield;
    }
</style>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title" id="box_title">User Entry Form</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body search_form">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group col-md-4" id="facility_type" style="display: none;">
                        <label>Facility Type</label>
                        <select class="form-control select2" id="facilityType" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group col-md-4" id="change_type" style="display: none;">
                        <label>Version Given By</label>
                        <select class="form-control select2" id="changeType" onchange="show_dpu(this.value)" style="width: 100%;">
                            <option value="">--Select Type--</option>
                            <option value="1">District Wise</option>
                            <option value="2">Upazila Wise</option>
                            <option value="3">Union Wise</option>
                            <option value="4">Provider Wise</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12" id="provider_type">
                        <label>User Type</label>
                        <select class="form-control select2" id="providerType" required="required">
                            <option value="">--Select User Type--</option>
                            <% int Type = (Integer)request.getAttribute("type"); %>
                            <% int userType = (Integer)request.getAttribute("utype"); %>
                            <% if(Type == 11 || Type == 26){ %>
                            <% if(userType == 999){ %>
                            <option value="999">999 - Super User</option>
                            <option value="998">998 - Admin</option>
                            <option value="994">994 - Admin MNE </option>
                            <option value="995">995 - TroubleShooter </option>
                            <% } %>

                            <option value="996">996 - Central Level </option>
                            <option value="997">997 - Report Viewer</option>
                            <option value="0">0 - Guest Viewer</option>
                            <option value="21">21 - Civil Surgeon</option>
                            <option value="20">20 - DDFP</option>
                            <option value="24">24 - ADCC</option>
                            <option value="25">25 - ADFP</option>
                            <option value="14">14 - MOMCH</option>
                            <option value="15">15 - UFPO</option>
                            <option value="15">18 - AUFPO</option>
                            <option value="19">19 - UFPA</option>
                            <option value="16">16 - UHFPO</option>
                            <option value="22">22 - Statistician_HS</option>
                            <option value="23">23 - Statistician_FP</option>
                            <% } %>
                        </select>
                    </div>
					 <div class="form-group col-md-4">
		                <label>Division</label>
		                <select class="form-control select2" id="RdivName" required="required" style="width: 100%;">
		                </select>
		              </div>
                    <div class="form-group col-md-4" id="zilaName">
                        <label>District</label>
                        <select class="form-control select2" id="RzillaName" required="required" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group col-md-4" id="upazilaName">
                        <label>Upazila</label>
                        <select class="form-control select2" id="RupaZilaName" required="required" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group col-md-4" id="unionName">
                        <label>Union</label>
                        <select class="form-control select2" id="RunionName" required="required" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="provider_facility_type" style="display: none;">
                        <label>User Facility Type</label>
                        <select class="form-control select2 changeFacility" id="providerFacility" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="Pname">
                        <label>User Name</label>
                        <input type="text" class="form-control" id="ProvName" required="required" placeholder="User Name">
                    </div>
                    <div class="form-group col-md-6" id="Pmob">
                        <label>User Mobile No.</label>
                        <input type="text" maxlength="11" class="form-control" id="ProvMob" required="required" placeholder="User Mobile No.">
                    </div>
                    <div class="form-group col-md-6" id="provider_code">
                        <label>User Code</label>
                        <input type="number" class="form-control" id="ProvCode" required="required" placeholder="User Code">
                    </div>
                    <div class="form-group col-md-6" id="Ppass">
                        <label>User Password</label>
                        <input type="text" class="form-control" id="ProvPass" required="required" placeholder="User Password">
                    </div>
                  <!--  <div class="form-group col-md-6" id="FacName">
                        <label>Facility Name</label>
                        <select class="form-control select2" id="facilityName" required="required">
                            <option value="">--Select Facility Name--</option>
                            <% if(userType == 999){ %>
                            <option value="মা-মনি কার্যালয়, গুলশান , ঢাকা">মা-মনি কার্যালয়, গুলশান , ঢাকা</option>
                            <% } %>-->
                            <!-- <option value="ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যাণ কেন্দ্র">ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যাণ কেন্দ্র</option>
                            <option value="পরিবার কল্যাণ কেন্দ্র (বিভিন্ন)">পরিবার কল্যাণ কেন্দ্র (বিভিন্ন)</option>
                            <option value="ইউনিয়ন সাব-সেন্টার">ইউনিয়ন সাব-সেন্টার</option>
                            <option value="রুরাল ডিসপেন্সারি">রুরাল ডিসপেন্সারি</option>
                            <option value="উপজেলা স্বাস্থ্য কমপ্লেক্স">উপজেলা স্বাস্থ্য কমপ্লেক্স</option> -->
                            <!-- <option value="কমিউনিটি ক্লিনিক">কমিউনিটি ক্লিনিক</option> -->
<!--                        </select>
                    </div>-->
                    <div class="form-group col-md-6" id="aav_provider_type">
                        <label>User Type</label>
                        <select class="form-control select2" id="aav_providerType" required="required">
                            <option value="">ALL</option>
                            <option value="4">FWV</option>
                            <option value="5">S.A.C.M.O (FP)</option>
                            <option value="6">S.A.C.M.O (HS)</option>
                            <option value="101">PARAMEDIC</option>
                            <option value="17">Midwife</option>
                            <option value="2">HA(CSBA)</option>
                            <option value="3">FWA(CSBA)</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="version_name" style="display: none;">
                        <label>Version Name</label>
                        <input type="text" class="form-control" id="version" placeholder="Version Name">
                    </div>
                    <div class="form-group col-md-6" id="db_upload_download">
                        <label>Upload/Download</label>
                        <select class="form-control select2" id="upload_download_status" required="required">
                            <option value="">-Select--</option>
                            <option value="1">Download</option>
                            <option value="2">Data Upload</option>
                            <option value="3">Both</option>
                            <option value="4">DB Upload</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6 retired_date" id="retired_date" style="display: none">
                        <label>Retired Date</label>
                        <br>
                        <input type="text" id="end_date" class="form-control pull-right" readonly />
                    </div>
                    <div class="box-footer col-md-12">
                        <input type="hidden" id="formType" value="">
                        <input type="button" id="UserEntryForm" class="btn btn-primary" value="Submit">
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>

    <div id="model_box"></div>

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="library/datepicker/datepicker.min.css">
    <!-- bootstrap datepicker -->
    <script src="library/datepicker/bootstrap-datepicker.min.js"></script>

    <script>
        $(".select2").select2();

        function show_dpu(type)
        {
            if(type==1)
            {
                $("#upazilaName").hide();
                $("#unionName").hide();
                $("#provider_code").hide();
            }
            else if(type==2)
            {
                $("#upazilaName").show();
                $("#unionName").hide();
                $("#provider_code").hide();
            }
            else if(type==3)
            {
                $("#upazilaName").show();
                $("#unionName").show();
                $("#provider_code").hide();
            }
            else if(type==4)
            {
                $("#upazilaName").show();
                $("#unionName").hide();
                $("#provider_code").show();
                $("#aav_provider_type").hide();
            }
        }

        $('#providerType').on('change', function(){
			//alert($('#providerType').val());
            if($('#formType').val()!="6"){
				
				
               if($('#providerType').val()=="999" || $('#providerType').val()=="998" || $('#providerType').val()=="994" || $('#providerType').val()=="997" || $('#providerType').val()=="0" || $('#providerType').val()=="20" || $('#providerType').val()=="21" || $('#providerType').val()=="23" || $('#providerType').val()=="24" || $('#providerType').val()=="25" || $("#formType").val() == "26"){
			  <!-- if($('#providerType').val()=="998" || $('#providerType').val()=="997" || $('#providerType').val()=="20" || $('#providerType').val()=="21" || $('#providerType').val()=="23"){-->
					//$("#RzillaName").hide();
                    $("#upazilaName").hide();
                    $("#unionName").hide();
                    $("#provider_facility_type").hide();
                    $("#FacName").hide();
                }
				 /*else if($('#providerType').val()=="999"){
				 	//alert("Super User Inside=="+$('#providerType').val());
				 	$("#zilaName").hide();
                    $("#upazilaName").hide();
                    $("#unionName").hide();
                    $("#provider_facility_type").hide();
                    $("#FacName").hide();
                }*/
                else if($('#providerType').val()=="14" || $('#providerType').val()=="15" || $('#providerType').val()=="16" || $('#providerType').val()=="19" || $('#providerType').val()=="22"){
                    $("#upazilaName").show();
                    $("#unionName").hide();
                    $("#provider_facility_type").hide();
                    $("#FacName").hide();
                }
                else{
					//alert("Outside"+$('#providerType').val());
                    $("#upazilaName").show();
                    $("#unionName").show();
                    $("#provider_facility_type").show();
                    $("#FacName").show();
                }
            }
        });

        $('#facilityType').on('change', function(){
            if($('#formType').val()=="5"){
                if($('#facilityType').val()=="5_UHC")
                    $("#unionName").hide();
                else if($('#facilityType').val()=="7_DH" || $('#facilityType').val()=="6_MCWC"){
                    $("#unionName").hide();
                    $("#upazilaName").hide();
                }
                else{
                    $("#unionName").show();
                    $("#upazilaName").show();
                }
            }
        });

        $('#month_type').on('change', function(){
            $(".month_wise").show();
            $(".date_wise").hide();
        });

        $('#date_type').on('change', function(){
            $(".date_wise").show();
            $(".month_wise").hide();
        });

        $('#monthSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            minViewMode: 1,
            format: 'yyyy-mm'
        })

        $('#start_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        $('#end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        /*$('#monthSelect').combodate({
            format: "YYYY-MM",
            template: "MMM YYYY",
            smartDays: true,
            //maxYear: 2025,
            value : moment()
        });

        $('#start_date').combodate({
            format: "YYYY-MM-DD",
            template: "DD MMM YYYY",
            smartDays: true,
            //maxYear: 2025,
            value : moment()
        });

        $('#end_date').combodate({
            format: "YYYY-MM-DD",
            template: "DD MMM YYYY",
            smartDays: true,
            //maxYear: 2025,
            value : moment()
        });*/

        $( document ).ready(function() {
            $("#facilityType").empty();

            var output = "<option value ='' selected></option>";

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + "<option value = " + key + "_" + val + ">" + val + "</option>";
            });

            $("#facilityType").append(output);
        });

        $( document ).ready(function() {
            $("#providerFacility").empty();

            var output = "<option value ='' selected></option>";

            $.each(<% out.println(request.getAttribute("facilityType")); %>, function(key, val) {
                output = output + "<option value = " + key + "_" + val + ">" + val + "</option>";
            });

            $("#providerFacility").append(output);
        });

        $(".changeFacility").on('change', function(){
            if($('#FacName').is(':visible')){
                var forminfo = new Object();
                forminfo.zillaid = $('#RzillaName').val();
                forminfo.upazilaid = $('#RupaZilaName').val();
                forminfo.unionid = $('#RunionName').val();
                if($('#facility_type').is(':visible'))
                    forminfo.facilityType = $('#facilityType').val().split("_")[0];
                else if($('#provider_facility_type').is(':visible'))
                    forminfo.facilityType = $('#providerFacility').val().split("_")[0];
                forminfo.type = "6";

                $.ajax({
                    type: "POST",
                    url:"getFacilityInfo",
                    timeout:60000, //60 seconds timeout
                    data:{"forminfo":JSON.stringify(forminfo)},
                    success: function (response) {
                        console.log(response);
                        facilityJS = JSON.parse(response);
                        $("#facilityName").empty();
                        if((<% out.println(userType); %>)=="999"){
                            var output = "<option value ='' selected></option>";
                            output = output + "<option value ='মা-মনি কার্যালয়, গুলশান , ঢাকা'>মা-মনি কার্যালয়, গুলশান , ঢাকা</option>";
                        }
                        else
                            var output = "<option value ='' selected></option>";

                        $.each(facilityJS, function(key, val) {
                            output = output + "<option value = " + key + ">" + val + "</option>";
                        });

                        $("#facilityName").append(output);
                    },
                    complete: function() {
                        preventMultiCall = 0;
                    },
                    error: function(xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });
            }
        });

    </script>

</div>



