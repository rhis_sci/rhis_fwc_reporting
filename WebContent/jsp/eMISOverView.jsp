<%@page import="org.json.JSONObject"%>
<%@ page import="org.json.JSONArray" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%
    JSONObject info_json=(JSONObject)request.getAttribute("info");
    JSONObject info = new JSONObject(info_json.get("Bangladesh").toString());
    //info.put(info_json);
%>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-bookmark"></span> eMIS OverView
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row" id="tab1">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-flag"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><h2>Bangladesh</h2></div>
                                            <div><strong># of Zilla: </strong><%= info.get("Zilla#") %></div>
                                            <div><strong># of Upazila: </strong><%= info.get("Upazila#") %></div>
                                            <div><strong># of Union: </strong><%= info.get("Union#") %></div>
                                            <div><strong># of Facility: </strong><%= info.get("Facility#") %></div>
                                            <div><strong># of Provider: </strong><%= info.get("Provider#") %></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <%
                        //Map all_bangladesh = ((Map)info_json.getJSONObject("Bangladesh"));
                        JSONObject all_bangladesh = new JSONObject(info_json.getJSONObject("Bangladesh").toString());
                        Iterator<String> info_zilla = all_bangladesh.keys();
                        String zilla_name = "";
                        String upazila_name = "";
                        while(info_zilla.hasNext()){
                            String info_zilla_key = info_zilla.next();
                            //JSONObject info_zilla_details = new JSONObject(info_zilla.getJSONObject(i).toString());

                            if(!info_zilla_key.equals("Zilla#") && !info_zilla_key.equals("Upazila#") && !info_zilla_key.equals("Union#") && !info_zilla_key.equals("Facility#") && !info_zilla_key.equals("Provider#")){
                                zilla_name = info_zilla_key;
                                //Map zilla_details_map = ((Map)((Map) info_json.get("Bangladesh")).get(zilla_name));
                                JSONObject zilla_details = new JSONObject(all_bangladesh.get(info_zilla_key).toString());
                                //Iterator<String> zilla_details = info_zilla_key.keys();

                    %>
                    <div class="row" id="tab1">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-flag"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><h2><%= zilla_details.get("Name") %></h2></div>
                                            <div><strong># of Upazila: </strong><%= zilla_details.get("Upazila#") %></div>
                                            <div><strong># of Union: </strong><%= zilla_details.get("Union#") %></div>
                                            <div><strong># of Facility: </strong><%= zilla_details.get("Facility#") %></div>
                                            <div><strong># of Provider: </strong><%= zilla_details.get("Provider#") %></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <%
                        Iterator<String> info_upazila = zilla_details.keys();
                        while(info_upazila.hasNext()){
                            String info_upazila_key = info_upazila.next();

                            if(!info_upazila_key.equals("Name") && !info_upazila_key.equals("ID") && !info_upazila_key.equals("Upazila#") && !info_upazila_key.equals("Union#") && !info_upazila_key.equals("Facility#") && !info_upazila_key.equals("Provider#")){
                                upazila_name = info_upazila_key;
                                JSONObject upazila_details = new JSONObject(zilla_details.get(info_upazila_key).toString());

//                                Map upazila_details_map = ((Map)((Map)((Map)info_json.get("Bangladesh")).get(zilla_name)).get(upazila_name));
//                                Iterator<Map.Entry> upazila_details_iterator = upazila_details_map.entrySet().iterator();
                    %>
                    <%--<div class="row" id="tab1">--%>
                        <%--<div class="col-lg-3 col-md-6">--%>
                            <%--<div class="panel panel-primary">--%>
                                <%--<div class="panel-heading">--%>
                                    <%--<div class="row">--%>
                                        <%--<div class="col-xs-3">--%>
                                            <%--<i class="fa fa-flag"></i>--%>
                                        <%--</div>--%>
                                        <%--<div class="col-xs-9 text-right">--%>
                                            <%--<div class="huge"><h2><%= upazila_details.get("Name") %></h2></div>--%>
                                            <%--<div><strong># of Union: </strong><%= upazila_details.get("Union#") %></div>--%>
                                            <%--<div><strong># of Facility: </strong><%= upazila_details.get("Facility#") %></div>--%>
                                            <%--<div><strong># of Provider: </strong><%= upazila_details.get("Provider#") %></div>--%>
                                        <%--</div>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<a href="#">--%>
                                    <%--<div class="panel-footer">--%>
                                        <%--<span class="pull-left">View Details</span>--%>
                                        <%--<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>--%>
                                        <%--<div class="clearfix"></div>--%>
                                    <%--</div>--%>
                                <%--</a>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                    <%
                        }}}}
                    %>
                    <!--<a href="http://www.jquery2dotnet.com/" class="btn btn-success btn-lg btn-block" role="button"><span class="glyphicon glyphicon-globe"></span> Website</a>-->
                </div>
            </div>
        </div>
    </div>
</div>