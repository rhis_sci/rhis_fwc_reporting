<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/28/2021
  Time: 4:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<style>
    .input-daterange input, #monthSelect, #yearSelect {
        background: url('image/calendar.png') no-repeat;
        background-size: 30px 30px;
        padding-left: 30px;
        text-align: center;
        background-color: #ffffff !important;
    }
</style>
<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>


        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">

                    <!--select type-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="view_type" style="display: none;">
                                <label>District/Upazila/Union/Facility</label>
                                <select class="form-control select2" id="reportViewType" onchange="show_dpu(this.value)"
                                        style="width: 100%;">
                                    <option value="">--Select Type--</option>
                                    <option value="1">District Wise</option>
                                    <option value="2">Upazila Wise</option>
                                    <option value="3">Union Wise</option>
                                    <option value="4">Facility Wise</option>
                                </select>
                            </div>
                            <!--service section-->
                            <div class="form-group col-md-3" id="method_type" style="display: none;">
                                <label>Service</label>
                                <select class="form-control select2" id="methodType" style="width: 100%;">
                                    <option value="">--Select Method Type--</option>
                                    <option value="1">Maternal and Child Health</option>
                                    <option value="2">Family Planing</option>
                                    <option value="3">General Patient</option>
                                    <!--its used as a methodType value..4&5 already used-->
                                    <option value="6">Child Care service</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/select type-->

                    <!--geo group-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-3" id="divisionName">
                                <label>Division</label>
                                <select class="form-control select2" id="RdivName" disabled style="width: 100%;">
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="districtName">
                                <label>District</label>
                                <select class="form-control select2" id="RzillaName" disabled style="width: 100%;">
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="upazilaName">
                                <label>Upazila</label>
                                <select class="form-control select2" id="RupaZilaName" disabled style="width: 100%;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/geo group-->

                    <!--report view-->
                    <div class="row">

                        <div class="col-md-3">
                            <%@include file="/jsp/util/date.jsp" %>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group col-md-12 report_view_option">
                                    <label>Select Report View Option</label><br/>
                                    <span class="levelView"><input type="radio" id="aggregate_report" name="view_type"
                                                                   value="1" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;Aggregate Report &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <%--<input type="radio" id="aggregate_report">&nbsp;&nbsp;Aggregate Report&nbsp;&nbsp;--%>
                                    <span class="levelView"><input type="radio" id="aggregate_report_monthwise" name="view_type"
                                                                   value="2" class="flat-red">&nbsp;&nbsp;&nbsp;Month wise Report &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <%--<input type="radio" id="aggregate_report_monthwise"><label--%>
                                        <%--for="aggregate_report_monthwise">&nbsp;&nbsp;Month wise Report&nbsp;&nbsp;</label>--%>
                                </div>
                                <%@include file="/jsp/util/submitButton.jsp" %>
                            </div>
                        </div>


                    </div>
                    <!--/report view-->

                    <!--date section-->
                    <%--<%@include file="/jsp/util/date.jsp" %>--%>

                </div>
            </div>
            <!--row-->
        </div>
    </div>

    <!--for showing table(performance) result-->
    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>
    <!--/for showing table(performance) result-->
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="library/datepicker/datepicker.min.css">
    <!-- bootstrap datepicker -->
    <script src="library/datepicker/bootstrap-datepicker.min.js"></script>

    <script>
        $(".select2").select2();
        // $(document).on('click', 'input[type="checkbox"]', function () {
        //     $('input[type="checkbox"]').not(this).prop('checked', false);
        // });

        function show_dpu(type) {
            if (type == 1) {
                $("#divisionName").children().prop("disabled", true);
                // document.getElementById("RdivName").disabled = false;
                $("#districtName").children().prop("disabled", true);
                $("#upazilaName").children().prop("disabled", true);


            }
            else if (type == 2) {
                $("#divisionName").children().prop("disabled", false);
                $("#districtName").children().prop("disabled", false);
                $("#upazilaName").children().prop("disabled", true);


            }
            else if (type == 3 || type == 4) {
                $("#divisionName").children().prop("disabled", false);
                $("#districtName").children().prop("disabled", false);
                $("#upazilaName").children().prop("disabled", false);
            }
        }

        $('#year_type, #monthly').on('change', function () {
            $(".year_wise").show();
            $('#end_date').datepicker('setDate', null);
            $('#start_date').datepicker('setDate', null);
            $('#monthSelect').datepicker('setDate', null);
            $('#yearSelect').datepicker('setDate', new Date().getFullYear().toString());
            $(".date_wise").hide();
            $(".month_wise").hide();
        });

        $('#month_type, #weekly').on('change', function () {
            $(".month_wise").show();
            $('#end_date').datepicker('setDate', null);
            $('#start_date').datepicker('setDate', null);
            $('#yearSelect').datepicker("setDate", null);
            $('#monthSelect').datepicker('setDate', new Date().getFullYear() + '-' + (new Date().getMonth() + 1).toString());
            $(".date_wise").hide();
            $(".year_wise").hide();
        });

        $('#date_type, #daily').on('change', function () {
            $(".date_wise").show();
            $('#end_date').datepicker("setDate", new Date());
            $('#monthSelect').datepicker("setDate", null);
            $('#yearSelect').datepicker("setDate", null);
            $(".month_wise").hide();
            $(".year_wise").hide();
        });

        $('#yearSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            viewMode: "years",
            minViewMode: "years",
            format: 'yyyy'
        }).datepicker('setDate', new Date().getFullYear().toString());

        $('#monthSelect').datepicker({
            autoclose: true,
            todayHighlight: true,
            minViewMode: 1,
            format: 'yyyy-mm'
        }).datepicker('setDate', new Date().getFullYear() + '-' + (new Date().getMonth() + 1).toString());

        $('#start_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
        $('#end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });
    </script>
</div>

