<title>Login Location</title>
<style>
	/*#map-login-location {*/
	/*	height: 100%;*/
	/*	position: inherit !important;*/
	/*}*/
</style>
<div class="row" >
    <div class="col-md-12" >
        <div class="bread-crumb">
            <ul id="bread-crumb" class="breadcrumb"></ul>
        </div>

        <div class="panel panel-default panel-map">

            <div class="panel-body">
                <div id="map-container" style="height: 500px" ></div>
            </div>
        </div>

        <div class="col-md-12" >
            <!-- Map Animation
             <input type="checkbox" data-toggle="toggle" id="map-anim-switch" data-size="mini"> -->

            <h4 style="margin-bottom: 15px; color: #0d6aad; cursor: pointer" data-toggle="collapse" data-target="#FAQ-collaps" aria-expanded="false" aria-controls="FAQ-collaps">How to navigate Map?</h4>
            <div class="collapse" id="FAQ-collaps">
                <div class="well">
                    <ul>
                        <li>Tap/Click on map to get specific information.</li>
                        <li>Double Tap/Click on map to get into upazila and unions.</li>
                        <li>Pinch to zoom in/out</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<div id="map-container" style = "width:900px; height:580px;"></div>--%>
<%--<link rel = "stylesheet" href = "http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />--%>
<%--<script src = "http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>--%>

<link rel="stylesheet" href="dashboard/leaflet/leaflet.css" />
<link rel="stylesheet" href="dashboard/dashboard.css" />
<script type="text/javascript" src="json/zilla_upazila_union.json"></script>
<script src="dashboard/leaflet/leaflet.js"></script>
<script src="js/leflet-login-location-map.js"></script>
<script src="js/map_data_handler_login_location.js"></script>

<script>

    function event_mapChange(){
        tname = breadCrumb[breadCrumb.length-1].name;

        if(tname == "Home")
        {
            tname = "Bangladesh";
            $("#area-name").html(tname);
        }
    }



    function getGeoName(geocode){
        zillaJS = JSON.parse(zilla.replace(/\s+/g,""));
        return (zillaJS[0][geocode.split(",")[0]]["Upazila"][geocode.split(",")[1]]["nameBanglaUpazila"] + "," + zillaJS[0][geocode.split(",")[0]]["nameBangla"]);
    }

    $(function() {
        $('#map-anim-switch').change(function() {
            //console.log('Toggle: ' + $(this).prop('checked'))

            if($(this).prop('checked') == true)
                map_interval = setInterval(map_animation_controller,15000);

            if($(this).prop('checked') == false)
                clearInterval(map_interval);

        })
    });
</script>

<%--<script>--%>
<%--    // Creating map options--%>
<%--    var mapOptions = {--%>
<%--        center: [23.791394, 90.4115023],--%>
<%--        zoom: 10--%>
<%--    }--%>

<%--    // Creating a map object--%>
<%--    var map = new L.map('map-login-location', mapOptions);--%>

<%--    // Creating a Layer object--%>
<%--    var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');--%>

<%--    // Adding layer to the map--%>
<%--    map.addLayer(layer);--%>
<%--</script>--%>

<!--
<script>

    var gmarkers = [];
    // initMap();

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: {lat: 23.6850, lng: 90.3563}
        });

        // map.data.loadGeoJson('json/Habiganj/Habiganj_Upazila.geojson');
        // map.data.loadGeoJson('json/Noakhali/Noakhali_Upazila.geojson');
        // map.data.loadGeoJson('json/Lakshmipur/Lakshmipur_Upazila.geojson');
        // map.data.loadGeoJson('json/Tangail/Tangail_Upazila.geojson');
        // map.data.loadGeoJson('json/Jhalokati/Jhalokati_Upazila.geojson');

        map.data.setStyle(function(feature) { //error here

            return {
                fillColor: '#DBDBDB',
                fillOpacity: .25,
                strokeWeight: 1
            };
        });

        setMarkers(map);
        setInterval(function() {
            reloadMarkers();
            setMarkers(map);
        }, 900000);
    }

    function reloadMarkers() {

        for (var i=0; i<gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }
        gmarkers = [];
    }

    function getGeoName(geocode){
        zillaJS = JSON.parse(zilla.replace(/\s+/g,""));
        return (zillaJS[0][geocode.split(",")[0]]["Upazila"][geocode.split(",")[1]]["nameBanglaUpazila"] + "," + zillaJS[0][geocode.split(",")[0]]["nameBangla"]);
    }

    function setMarkers(map) {

        var Obj = new Object();

        Obj.zillaid = "All";
        Obj.upazilaid = "All";

        var response = (function () {
            var json = null;
            $.ajax({
                type : "POST",
                async: false,
                global: false,
                // url : "http://119.148.43.35:8080/eMIS/mapplot",
                url : "http://localhost:8081/mapplot",
                timeout:15000, //15 seconds timeout
                dataType : "json",
                data : {"info" : JSON.stringify(Obj)},
                success : function(server_response) {
                    json = server_response;
                },
                error : function(xhr, status, error) {
                    alert(status);
                    alert(error);
                }
            });
            return json;
        })();
        console.log(response);
        for (var key in response){
            for (var key1 in response[key]){
                if(key1 != "zillaid" && key1 != "upazilaid"){
                    if(response[key][key1]["satellite_name"]==""){
                        var image = "image/facility_pointer.png";
                    }
                    else{
                        var image = "image/satelite_pointer.png";
                    }

                    var provider_name = response[key][key1]["ProvName"].toUpperCase();
                    var facility_name = response[key][key1]["FacilityName"].toUpperCase();
                    var satelite_name = response[key][key1]["satellite_name"].toUpperCase();

                    var pointerTitle = provider_name + " - " + response[key][key1]["provider_id"];
                    if(response[key][key1]["satellite_name"]==""){
                        pointerTitle = pointerTitle + "\n" + facility_name;
                    }
                    else{
                        pointerTitle = pointerTitle + "\n" + satelite_name + " (Satelite Center)";
                    }
                    pointerTitle = pointerTitle + "\n" + getGeoName(response[key][key1]["zillaid"] + "," + response[key][key1]["upazilaid"]);

                    var location_marker = [];
                    location_marker = new google.maps.Marker({
                        position: {lat: parseFloat(response[key][key1]["latitude"]), lng: parseFloat(response[key][key1]["longitude"])},
                        map: map,
                        icon: image,
                        title: pointerTitle
                    });

                    gmarkers.push(location_marker);
                }
            }
        }
    }
</script>
-->