<%--
  Created by IntelliJ IDEA.
  User: shahed.chaklader
  Date: 2/20/2020
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--%>


<style>
    #mhealth-resut-table td, #mhealth-resut-table th{
        text-align: center;
    }
    .m-box-panel{
        padding: 15px;
        margin-bottom: 20px;
    }

    .m-info-box-title{
        font-size: 16px;
        font-weight: bold;
    }

    .m-info-box-value{
        font-size: 2em;
        font-weight: bold;
        text-align: right;
        color:#6B2F8C;
    }


</style>


    <!-- box header -->
<div class="col-md-12" style="margin-top: 30px">
        <div class="box box-default">
            <div class="box-header with-border">
                <!--<h3 class="box-title" id="box_title"></h3>-->
                <h3 class="box-title" id="box_title">mHealth Report</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body search_form">
                <!--geo location-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group col-md-3" id="divisionName">
                            <label>Division<sup class="text-danger">*</sup></label>
                            <select class="form-control select2" id="RdivName" style="width: 100%;">
                            </select>
                        </div>
                        <div class="form-group col-md-3" id="districtName">
                            <label>District<sup class="text-danger">*</sup></label>
                            <select class="form-control select2" id="RzillaName" style="width: 100%;">
                            </select>
                        </div>
                        <div class="form-group col-md-3" id="upazilaName">
                            <label>Upazila</label>
                            <select class="form-control select2" id="RupaZilaName" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                </div>

                <!--/geo location--->

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group col-md-4 date_wise ">
                            <label class="control-label" for="start_date">Date Range</label>
                            <div class="input-group input-daterange">
                                <input type="text" id="start_date" class="form-control datepicker" readonly placeholder="Start Date" >
                                <div class="input-group-addon">to</div>
                                <input type="text" id="end_date" class="form-control datepicker" readonly placeholder="End Date">
                            </div>
                        </div>
                        <div class="box-footer col-md-12">
                            <input type="hidden" id="reportType" value="mHealth">
                            <input type="button" id="submitReport" class="btn btn-primary" value="Submit">
                        </div>
                    </div>



                </div>
            </div>
        </div>
</div>
    <!--/box header -->

<div id="mhealth-ajax-response">

</div>

<script type="text/javascript" src="js/utilities.js"></script>

        <script>

            $(".select2").select2();
            $('#start_date').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                dateFormat: 'yy-mm-dd',
                setDate: new Date()
            });
            $('#end_date').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                dateFormat: 'yy-mm-dd',
                setDate: new Date()
            });


            function getReportMhealth() {

                var forminfo = new Object();
                forminfo.start_date = $('#start_date').val();
                forminfo.end_date = $('#end_date').val();

                forminfo.zillaid = $('#RzillaName').val();
                forminfo.upazilaid = $('#RupaZilaName').val();
                forminfo.unionid = $('#RunionName').val();

                forminfo.ajax=true;

                $.ajax({
                    type: "POST",
                    url:"mhealth",
                    timeout:60000, //60 seconds timeout
                    // data:{"start_date": forminfo.start_date, "end_date":forminfo.end_date,"ajax_request":true, "zillaid" : forminfo.zillaid, "upazilaid":forminfo.upazilaid, "unionid":forminfo.unionid},
                    data:{"forminfo":JSON.stringify(forminfo)},
                    beforeSend: function(  ) {
                        $('#loading_div').html("<img src='image/loading.gif' />");
                        $('#loading_div').show();
                    },
                    success: function (response) {
                        console.log(response);
                        $("#mhealth-ajax-response").html(response);
                        $('#loading_div').hide();
                    },
                    complete: function() {

                    },
                    error: function(xhr, status, error) {
                        checkInternetConnection(xhr)
                    }
                });

            }


            $(document).ready(function () {

                //set date field as today
                today = new Date();
                $("#start_date").val(today.toISOString().split('T')[0]);
                $("#end_date").val(today.toISOString().split('T')[0]);



                loadDivision("#RdivName");
                getReportMhealth();

                $("#submitReport").click(function () {

                    start_date = new Date($("#start_date").val());
                    end_date = new Date ($("#end_date").val());


                    if(end_date<start_date)
                    {
                        alert("Start date can not be smaller than end date.");
                        return false;
                    }


                    if($("#RdivName").val() !="" && $("#RzillaName").val()==""){
                        alert("Please select District");
                        return;
                    }


                    getReportMhealth();
                })
            });


        </script>

