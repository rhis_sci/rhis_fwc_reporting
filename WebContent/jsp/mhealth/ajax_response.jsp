<%--<div class="col-md-3">--%>
    <%--<div class="m-box-panel bg-success">--%>
        <%--<div class="m-info-box-title">Total Client Reached </div>--%>
        <%--<div class="m-info-box-value"><%= request.getAttribute("m_count") %></div>--%>
    <%--</div>--%>
<%--</div>--%>

<%--<div class="col-md-3">--%>
    <%--<div class="m-box-panel bg-info">--%>
        <%--<div class="m-info-box-title">Total SMS Sent </div>--%>
        <%--<div class="m-info-box-value"><%= request.getAttribute("m_total_sms_sent") %></div>--%>
    <%--</div>--%>
<%--</div>--%>

<%--<div class="col-md-12">--%>
    <%--<div class="panel">--%>

        <%--<div class="panel-body">--%>


            <%--<br>--%>

            <%--<h4><strong>Service wise message delivery</strong></h4>--%>
            <%--<%= request.getAttribute("m_count_group_by") %>--%>

        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>

<div class="col-md-12">

    <div class="row">
            <!-- sms type coount -->
            <div class="col-md-4">
                <div class="box box-default">
                    <div class="box-header">Message by type </div>

                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>SMS Type</th>
                                <th>Total</th>

                            </tr>
                            </thead>
                            <tbody>
                            <%= request.getAttribute("sms_type_count") %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- sms by service -->
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header">Message by service</div>

                    <div class="box-body">
                        <table class="table ">
                            <%= request.getAttribute("sms_service_wise") %>
                        </table>
                    </div>
                </div>
            </div>

    </div>

</div>

<div>
    <!-- sms type coount -->
    <div class="col-md-12" style="">
        <div class="box box-default"   >
            <div class="box-header">Location wise distribution</div>

            <div class="box-body">
                <table class="table table-hover" id="datatabledistributed">
                    <thead>
                    <tr>
                        <th>Zilla</th>
                        <th>Upazila</th>
                        <th>Total SMS</th>
                        <th>Text</th>
                        <th>Text Delivered</th>
                        <th>Text Failed</th>
                        <th>Voice</th>
                        <th>Voice Delivered</th>
                        <th>Voice Failed</th>

                    </tr>
                    </thead>
                    <tbody>
                    <%= request.getAttribute("sms_location_wise") %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- SMS waiting -->
    <%--<div class="col-md-12">--%>
        <%--<div class="box box-default">--%>
            <%--<div class="box-header">SMS processing today</div>--%>

            <%--<div class="box-body" >--%>
                <%--<table class="table table-hover" id="datatabletoday">--%>
                    <%--<thead>--%>
                    <%--<tr>--%>
                        <%--<th>Request ID</th>--%>
                        <%--<th>Eligible Service</th>--%>
                        <%--<th>Zilla</th>--%>
                        <%--<th>Upazila</th>--%>
                        <%--<th>Union</th>--%>
                        <%--<th>Date</th>--%>
                        <%--<th>SMS Type</th>--%>
                        <%--<th>Status</th>--%>

                    <%--</tr>--%>
                    <%--</thead>--%>

                    <%--<tbody>--%>

                    <%--<%= request.getAttribute("sms_waiting") %>--%>
                    <%--</tbody>--%>
                <%--</table>--%>
            <%--</div>--%>

        <%--</div>--%>
    <%--</div>--%>
</div>

<script>

    $(document).ready( function () {

        $('#datatabletoday, #datatabledistributed').DataTable({

            dom: 'Blfrtip',
            responsive: true,
            pageLength: 10,
            buttons: [
                {
                    //className:'btn-info',
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy'
                },
                {
                    //className:'btn-info',
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                },
                {
                    // className:'btn-info',
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV'
                },
                {
                    //className:'btn-info',
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                },
//            {
//                extend: 'print',
//                text: 'Print current page',
//                exportOptions: {
//                    modifier: {
//                        page: 'current'
//                    }
//                }
//            }
                {
                    text: '<i class="fa fa-print"></i>',
                    extend: 'print',
                    className: 'btn-print',
                    titleAttr: 'Print',

                }


            ],
            "order": [],
            "columnDefs": [
                { "searchable": false, "targets": [0] }  // Disable search on first and last columns
            ]
            //lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]

        });

    } );

</script>


