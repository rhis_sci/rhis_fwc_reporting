<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <title>Routine Health Information System</title> -->
	<title>MIS3</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="library/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<!-- Jquery min css -->
	<link rel="stylesheet" href="css/jquery-ui.min.css">
	<!-- jQuery 2.1.3 -->
	<script type="text/javascript" src="js/lib/jquery-2.2.4.min.js"></script>
	<!-- jQuery ui -->
	<script type="text/javascript" src="js/lib/jquery-ui.min.js"></script>
	<!-- jQuery base64 -->
	<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>
	
	<script type="text/javascript">
		$(document).on("click", '.next_button' ,function (){
			var button_val = $(".next_button").val();
			//alert(parseInt(button_val)+1);
			if(button_val<4)
			{
				$(".next_button").val(parseInt(button_val)+1);
				$(".pre_button").val(parseInt(button_val)-1);
				$(".table_hide").hide();
				$(".page_"+button_val).show();
				$(".showing_page").text("  "+button_val+" of 3  ");
				$(".pre_button").removeAttr("disabled");
				if((parseInt(button_val)+1)>3)
					$(".next_button").attr("disabled","disabled");

				$(".page_no").hide();
			}
		});

		$(document).on("click", '.pre_button' ,function (){
			var button_val = $(".pre_button").val();
			if(button_val>0)
			{
				$(".pre_button").val(parseInt(button_val)-1);
				$(".next_button").val(parseInt(button_val)+1);
				$(".table_hide").hide();
				$(".page_"+button_val).show();
				$(".showing_page").text("  "+button_val+" of 3  ");
				$(".next_button").removeAttr("disabled");
				if((parseInt(button_val)-1)<1)
				{
					$(".pre_button").attr("disabled","disabled");
					$(".page_no").show();
				}
			}
		});

		$(function() {
			var submitted_data = $("#submitted_data").val();
			if(submitted_data!=""){
				var submitted_json = jQuery.parseJSON(submitted_data);
				$("#mis3_entry_form :input").each(function(e){
					id = this.id;
					$("#"+id).val(submitted_json[id]);
				});
			}

			$(".stock_table :input").css("width", "50px")
		});

		$(document).on("click", '#re_entry' ,function (){
			$('#mis3_entry_form').dialog({
				modal: true,
				title: "MIS3 Entry Form",
				width: 'auto',
				open: function () {
					//$(this).append("#mis3_entry_form");
				},
				buttons: {
					"Submit" : function () {
						var id;
						var post_data = {};
						$("#mis3_entry_form :input").each(function(e){
							id = this.id;
							post_data[id] = this.value;
						});

						var post_info = {};
						var current_url = window.location.href;
						var params = current_url.split("?");
						post_info["params"] = params[1];
						post_info["entryData"] = post_data;

						console.log(post_info);

						$.ajax({
							type: "POST",
							url:"MIS3EntryData",
							timeout:60000, //60 seconds timeout
							data:{"entryInfo":JSON.stringify(post_info)},
							success: function (response) {
								location.reload();
							}
						});

						$(this).dialog('close');
					}
				}
			});
		});

		$(document).on("click", '#submitMIS3' ,function (){
			$("#model_box").dialog({
				modal: true,
				title: "Message",
				width: 'auto',
				open: function () {
					$(this).html("If you want to submit MIS3 then click Submit MIS3 or if you want to fill up not reportable field then click Entry");
				},
				buttons : {
					"Submit MIS3" : function() {
						$(this).dialog('close');
						var submission_status = "0";
						if($("#submissionStatus").val()!="" && $("#submissionStatus").val()!==undefined)
                            submission_status = "3";
						//alert(submission_status);
						submitMIS3Report(submission_status);
					},
					"Entry" : function() {
						$(this).dialog("close");
						$('#mis3_entry_form').dialog({
							modal: true,
							title: "MIS3 Entry Form",
							width: 'auto',
							open: function () {
								//$(this).append("#mis3_entry_form");
							},
							buttons: {
								"Submit" : function () {
									var id;
									var post_data = {};
									$("#mis3_entry_form :input").each(function(e){
										id = this.id;
										post_data[id] = this.value;
									});

									var post_info = {};
									var current_url = window.location.href;
									var params = current_url.split("?");
									post_info["params"] = params[1];
									post_info["entryData"] = post_data;

									console.log(post_info);

									$.ajax({
										type: "POST",
										url:"MIS3EntryData",
										timeout:60000, //60 seconds timeout
										data:{"entryInfo":JSON.stringify(post_info)},
										success: function (response) {
											location.reload();
										}
									});

									$(this).dialog('close');
								}
							}
						});
					}
				}
			});

		});

		/*$(document).on("click", '#approveMIS3' ,function (){
			submitMIS3Report("1");
		});

		$(document).on("click", '#rejectMIS3' ,function (){
			submitMIS3Report("2");
		});*/

		function submitMIS3Report(submission_status){
	//		var url = "";
			//if(submission_status=="0"){
	//			var current_url = window.location.href;
	//			var params = current_url.split("?");
				//url = "MIS3Submit?"+params[1];
				//console.log(params[1]);
			/*}
			else{
				var getParams = "facilityId="+$('#facilityName').val()+"&report1_zilla="+$('#report1_zilla').val()+"&report1_start_date="+$('#monthSelect').val()+"&submissionStatus="+submission_status+"&supervisorComment="+$('#supervisor_comment').val();
				var params = $.base64.encode(getParams);
				url = "MIS3Submit?"+params;
			}*/

			var post_info = {};
			var current_url = window.location.href;
			var params = current_url.split("?");
			var url = "MIS3Submit?"+params[1]+"&submissionStatus="+submission_status;
			post_info["params"] = params[1];
			post_info["entryData"] = jQuery.parseJSON($("#total_mis3_data").val());

			console.log(post_info);

			$.ajax({
				type: "POST",
				url:"MIS3EntryData",
				timeout:60000, //60 seconds timeout
				data:{"entryInfo":JSON.stringify(post_info)},
				success: function (response) {
					$.ajax({
						type: "GET",
						url:url,
						timeout:60000, //60 seconds timeout
						//data:{"forminfo":JSON.stringify(forminfo)},
						success: function (response) {
							if(response=='true'){
								$("#model_box").dialog({
									modal: true,
									title: "Message",
									width: 'auto',
									open: function () {
										$(this).html("<b style='color:green'>You have submitted MIS3 Successfully.</b>");
									},
									buttons : {
										Ok : function() {
											$(this).dialog('close');
											location.reload();
										}
									}
								});
							}
							else{
								$('#model_box').dialog({
									modal: true,
									title: "Message",
									width: 'auto',
									open: function () {
										$(this).html("<b style='color:red'>Your MIS3 not submitted successfully. Please re-submit your MIS3.</b>");
									},
									buttons: {
										Ok: function () {
											$(this).dialog("close");
										}
									}
								});
							}
						},
						complete: function() {
							preventMultiCall = 0;
						},
						error: function(xhr, status, error) {
                            checkInternetConnection(xhr)
						}
					});
				}
			});


		}
	</script>
	
	<style>
	.td_color
	{
	    background-color: #F2DEDE !important;                 
	}
	
	.rot
	{
	    -webkit-transform: rotate(-90deg); 
		-moz-transform: rotate(-90deg);    
		white-space: nowrap;
	    width:40px;
	    margin-top: 100px;
	}

	input[type=number]::-webkit-inner-spin-button,
	input[type=number]::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	</style>		
</head>
<body>                  
	<div class="tile-body nopadding" id="reportTable" style="margin: 10px;">
		<div id="printDiv">
			<% out.println(request.getAttribute("Result")); %>
		</div>
	</div>
	<!--<div id="model_box" title="Basic dialog"></div>-->
	<!--<div id="mis3_entry_form" style="display: none;">-->


    <div id="model_box" title="Basic dialog"></div>
    <div id="mis3_entry_form" style="display: none">
	
        <table class="table" width="700" cellspacing="0" style='font-size:10px;border: 1px solid black;'>
            <tr class='danger'>
                <td colspan='4' style='text-align: center;border: 1px solid black;'>&#2488;&#2503;&#2476;&#2494;&#2480; &#2471;&#2480;&#2467;</td>
                <td style='text-align: center;border: 1px solid black;'>&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;</td>
                <td style='text-align: center;border: 1px solid black;'>&#2488;&#2509;&#2479;&#2494;&#2463;&#2503;&#2482;&#2494;&#2439;&#2463;</td>
            </tr>
            <tr>
                <td rowspan="7" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;">
                    <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">&#2488;&#2509;&#2469;&#2494;&#2479;&#2492;&#2496; &#2474;&#2470;&#2509;&#2471;&#2468;&#2495;</span>
                </td>
                <td rowspan="3" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;">&#2474;&#2497;&#2480;&#2497;&#2487;</td>
                <td colspan="2" style="border-top: 1px solid black;">&#2488;&#2478;&#2509;&#2474;&#2494;&#2470;&#2472;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="permanent_male_execution" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;background-color: gray;"></td>
            </tr>
            <tr class="success">
                <td colspan="2">&#2437;&#2472;&#2497;&#2488;&#2480;&#2467;</td>
                <td align="center"><input type="number" class="form-control" id="permanent_male_followup_center" class="mis3_field"></td>
                <td align="center"><input type="number" class="form-control" id="permanent_male_followup_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="2">&#2474;&#2494;&#2480;&#2509;&#2486;&#2509;&#2476; &#2474;&#2509;&#2480;&#2468;&#2495;&#2453;&#2509;&#2480;&#2495;&#2479;&#2492;&#2494;/&#2460;&#2463;&#2495;&#2482;&#2468;&#2494;&#2480; &#2460;&#2472;&#2509;&#2479; &#2488;&#2503;&#2476;&#2494;</td>
                <td align="center"><input type="number" class="form-control" id="permanent_male_sideeffect_center" class="mis3_field"></td>
                <td align="center"><input type="number" class="form-control" id="permanent_male_sideeffect_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td rowspan="4" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;">&#2478;&#2489;&#2495;&#2482;&#2494;</td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;">&#2488;&#2478;&#2509;&#2474;&#2494;&#2470;&#2472;</td>
                <td style="border-top: 1px solid black;">&#2488;&#2509;&#2476;&#2494;&#2477;&#2494;&#2476;&#2495;&#2453;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="permanent_female_execution_normal" class="mis3_field"></td>
                <td bgcolor="gray" rowspan="2" align="center" style="border-top: 1px solid black;"></td>
            </tr>
            <tr>
                <td>&#2474;&#2509;&#2480;&#2488;&#2476; &#2474;&#2480;&#2476;&#2480;&#2509;&#2468;&#2496;</td>
                <td align="center"><input type="number" class="form-control" id="permanent_female_execution_after_delivery" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="2" style="border-top: 1px solid black;">&#2437;&#2472;&#2497;&#2488;&#2480;&#2467;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="permanent_female_followup_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="permanent_female_followup_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="2">&#2474;&#2494;&#2480;&#2509;&#2486;&#2509;&#2476; &#2474;&#2509;&#2480;&#2468;&#2495;&#2453;&#2509;&#2480;&#2495;&#2479;&#2492;&#2494;/&#2460;&#2463;&#2495;&#2482;&#2468;&#2494;&#2480; &#2460;&#2472;&#2509;&#2479; &#2488;&#2503;&#2476;&#2494;</td>
                <td align="center"><input type="number" class="form-control" id="permanent_female_sideeffect_center" class="mis3_field"></td>
                <td align="center"><input type="number" class="form-control" id="permanent_female_sideeffect_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;">&#2476;&#2472;&#2509;&#2471;&#2509;&#2479;&#2494;&#2468;&#2509;&#2476; &#2488;&#2503;&#2476;&#2494;</td>
                <td colspan="3" style="border-top: 1px solid black;">&#2458;&#2495;&#2453;&#2495;&#2510;&#2488;&#2494; / &#2474;&#2480;&#2494;&#2478;&#2480;&#2509;&#2486;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="infertility_treatment_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="infertility_treatment_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="3">&#2480;&#2503;&#2475;&#2494;&#2480;</td>
                <td align="center"><input type="number" class="form-control" id="infertility_refer_center" class="mis3_field"></td>
                <td align="center"><input type="number" class="form-control" id="infertility_refer_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="4">&#2439;&#2488;&#2495;&#2474;&#2495; &#2455;&#2509;&#2480;&#2489;&#2472;&#2453;&#2494;&#2480;&#2496; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</td>
                <td align="center"><input type="number" class="form-control" id="ecp_taken_center" class="mis3_field"></td>
                <td align="center"><input type="number" class="form-control" id="ecp_taken_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="4">&#2447;&#2478; &#2438;&#2480; ( &#2447;&#2478; &#2477;&#2495; &#2447; ) &#2488;&#2503;&#2476;&#2494; &#2474;&#2509;&#2480;&#2470;&#2494;&#2472;&#2503;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</td>
                <td align="center"><input type="number" class="form-control" id="mr_mva_treatment_center" class="mis3_field"></td>
                <td align="center"><input type="number" class="form-control" id="mr_mva_treatment_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="4">&#2447;&#2478; &#2438;&#2480; &#2447;&#2478; ( &#2452;&#2487;&#2471;&#2503;&#2480; &#2478;&#2494;&#2471;&#2509;&#2479;&#2478;&#2503; &#2447;&#2478; &#2438;&#2480;) &#2488;&#2503;&#2476;&#2494; &#2474;&#2509;&#2480;&#2470;&#2494;&#2472;&#2503;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</td>
                <td align="center"><input type="number" class="form-control" id="mrm_treatment" class="mis3_field"></td>
                <td align="center" style="background-color: gray;"></td>
            </tr>
            <tr>
                <td colspan="4" class="danger" style="border-top: 1px solid black;">&#2453;&#2503;&#2472;&#2509;&#2470;&#2509;&#2480;&#2503; &#2453;&#2527;&#2463;&#2495; &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479; &#2486;&#2495;&#2453;&#2509;&#2487;&#2494; (&#2476;&#2495;&#2488;&#2495;&#2488;&#2495;) &#2453;&#2494;&#2480;&#2509;&#2479;&#2453;&#2509;&#2480;&#2478; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="bcc_from_center" class="mis3_field"></td>
                <td align="center" style="background-color: gray;"></td>
            </tr>
            <tr class="success">
                <td colspan="4" class="danger" style="border-top: 1px solid black;">&#2447;&#2488;&#2447;&#2488;&#2488;&#2495; &#2447;&#2478;&#2451; &#2453;&#2480;&#2509;&#2468;&#2499;&#2453; &#2453;&#2527;&#2463;&#2495; &#2488;&#2509;&#2453;&#2497;&#2482; &#2488;&#2509;&#2476;&#2494;&#2488;&#2509;&#2469;&#2509;&#2479; &#2486;&#2495;&#2453;&#2509;&#2487;&#2494; (&#2476;&#2495;&#2488;&#2495;&#2488;&#2495;) &#2453;&#2494;&#2480;&#2509;&#2479;&#2453;&#2509;&#2480;&#2478; &#2488;&#2478;&#2509;&#2474;&#2494;&#2470;&#2472; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;</td>
                <td colspan="2" align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="bcc_from_sscmo" class="mis3_field"></td>
            </tr>
            <tr class="danger">
                <td rowspan="13" style="text-align: center;vertical-align: middle;border: 1px solid black;">
                    <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">&#2474;&#2497;&#2487;&#2509;&#2463;&#2495; &#2488;&#2503;&#2476;&#2494;</span>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">&#2438;&#2439; &#2451;&#2527;&#2494;&#2439; &#2488;&#2495; &#2447;&#2475; , &#2438;&#2439; &#2447;&#2475; &#2447; , &#2477;&#2495;&#2463;&#2494;&#2478;&#2495;&#2472; -&#2447; , &#2489;&#2494;&#2468; &#2471;&#2507;&#2527;&#2494; , &#2478;&#2494;&#2527;&#2503;&#2480; &#2474;&#2497;&#2487;&#2509;&#2463;&#2495; &#2476;&#2495;&#2487;&#2527;&#2453; &#2453;&#2494;&#2441;&#2472;&#2509;&#2488;&#2503;&#2482;&#2495;&#2434;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="nutrition_counseling_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="nutrition_counseling_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">&#2455;&#2480;&#2509;&#2477;&#2476;&#2468;&#2495; &#2478;&#2494;&#2453;&#2503; &#2438;&#2527;&#2480;&#2472; &#2451; &#2475;&#2482;&#2495;&#2453; &#2447;&#2488;&#2495;&#2465; &#2476;&#2524;&#2495; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; &#2453;&#2495; &#2472;&#2494;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="pregwomen_ifa_given_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="pregwomen_ifa_given_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">&#2534;-&#2537; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2480; &#2478;&#2494;&#2453;&#2503; &#2438;&#2527;&#2480;&#2472; &#2451; &#2475;&#2482;&#2495;&#2453; &#2447;&#2488;&#2495;&#2465; &#2476;&#2524;&#2495; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; &#2453;&#2495; &#2472;&#2494;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="0-3_month_monther_ifa_given_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="0-3_month_monther_ifa_given_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">&#2540;-&#2536;&#2537; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2478;&#2494;&#2482;&#2509;&#2463;&#2495;&#2474;&#2482; &#2478;&#2494;&#2439;&#2453;&#2509;&#2480;&#2507;&#2472;&#2495;&#2441;&#2463;&#2509;&#2480;&#2527;&#2503;&#2472;&#2509;&#2463; &#2474;&#2494;&#2441;&#2465;&#2494;&#2480; (&#2447;&#2478; &#2447;&#2472; &#2474;&#2495; ) &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; &#2404;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="6-23_month_child_mnp_given_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="6-23_month_child_mnp_given_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">&#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2477;&#2495;&#2463;&#2494;&#2478;&#2495;&#2472;- &#2447; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; &#2453;&#2495;&#2472;&#2494;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="6-59_month_child_vitamin-a_given_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="6-59_month_child_vitamin-a_given_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">&#2536;&#2538;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2453;&#2499;&#2478;&#2495; &#2472;&#2494;&#2486;&#2453; &#2476;&#2524;&#2495; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; &#2453;&#2495;&#2472;&#2494;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="24-59_month_child_wormpills_given_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="24-59_month_child_wormpills_given_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">&#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496; &#2465;&#2494;&#2527;&#2480;&#2495;&#2527;&#2494; &#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2454;&#2494;&#2476;&#2494;&#2480; &#2488;&#2509;&#2479;&#2494;&#2482;&#2494;&#2439;&#2472;&#2503;&#2480; &#2488;&#2494;&#2469;&#2503; &#2460;&#2495;&#2434;&#2453; &#2476;&#2524;&#2495; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; &#2453;&#2495;&#2472;&#2494;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="6-59_child_saline_given_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="6-59_child_saline_given_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">MAM(&#2478;&#2494;&#2461;&#2494;&#2480;&#2495; &#2468;&#2496;&#2476;&#2509;&#2480; &#2437;&#2474;&#2497;&#2487;&#2509;&#2463;&#2495;) &#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;  &#2447;&#2476;&#2434; &#2458;&#2495;&#2453;&#2495;&#2510;&#2488;&#2494; &#2470;&#2503;&#2527;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; (&#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;)</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="mam_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="mam_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">SAM(&#2478;&#2494;&#2480;&#2494;&#2468;&#2509;&#2476;&#2453; &#2468;&#2496;&#2476;&#2509;&#2480; &#2437;&#2474;&#2497;&#2487;&#2509;&#2463;&#2495;) &#2438;&#2453;&#2509;&#2480;&#2494;&#2472;&#2509;&#2468; &#2486;&#2495;&#2486;&#2497;&#2453;&#2503; &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;  &#2447;&#2476;&#2434; &#2480;&#2503;&#2475;&#2494;&#2480; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503;  &#2489;&#2527;&#2503;&#2459;&#2503; (&#2540;-&#2539;&#2543; &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;)</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="sam_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="sam_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">&#2486;&#2495;&#2486;&#2497;&#2480; Stunting (&#2476;&#2527;&#2488;&#2503;&#2480; &#2468;&#2497;&#2482;&#2472;&#2494;&#2527; &#2453;&#2478; &#2441;&#2458;&#2509;&#2458;&#2468;&#2494; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472;) &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; (&#2534;-&#2539;&#2543;  &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;)</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="stunting_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="stunting_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">&#2486;&#2495;&#2486;&#2497;&#2480; Wasting ( &#2441;&#2458;&#2509;&#2458;&#2468;&#2494;&#2480; &#2468;&#2497;&#2482;&#2472;&#2494;&#2527; &#2453;&#2478; &#2451;&#2460;&#2472; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472;) &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; (&#2534;-&#2539;&#2543;  &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;)</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="wasting_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="wasting_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">&#2486;&#2495;&#2486;&#2497;&#2480; Under weight (&#2476;&#2527;&#2488;&#2503;&#2480; &#2468;&#2497;&#2482;&#2472;&#2494;&#2527; &#2453;&#2478; &#2451;&#2460;&#2472; &#2488;&#2478;&#2509;&#2474;&#2472;&#2509;&#2472;) &#2488;&#2472;&#2494;&#2453;&#2509;&#2468; &#2453;&#2480;&#2494; &#2489;&#2527;&#2503;&#2459;&#2503; (&#2534;-&#2539;&#2543;  &#2478;&#2494;&#2488; &#2476;&#2527;&#2488;&#2496;)</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="under_weight_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="under_weight_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td rowspan="7" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 50px;margin-top: 85px;">&#2463;&#2495;&#2453;&#2494; &#2474;&#2509;&#2480;&#2494;&#2474;&#2509;&#2468; (0-18 &#2478;&#2494;&#2488;) <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2434;&#2454;&#2509;&#2479;&#2494;</span></td>
                <td colspan="3" style="border: 1px solid black;">&#2476;&#2495;&#2488;&#2495;&#2460;&#2495;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="bcg_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="bcg_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td rowspan="3" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;width: 12%;">&#2451;&#2474;&#2495;&#2465;&#2495; &#2451; &#2474;&#2509;&#2479;&#2494;&#2472;&#2509;&#2463;&#2494;&#2477;&#2503;&#2482;&#2503;&#2472;&#2509;&#2463;<br>(&#2465;&#2495;&#2474;&#2495;&#2465;&#2495;,&#2489;&#2503;&#2474;-&#2476;&#2495;,&#2489;&#2495;&#2476;)</td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: middle;border: 1px solid black;width: 10%;">&#2474;&#2495;&#2488;&#2495;&#2477;&#2495;</td>
                <td style="border-top: 1px solid black;width: 50%;">&#2535;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="opv_pcv_1_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="opv_pcv_1_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;">&#2536;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="opv_pcv_2_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="opv_pcv_2_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td style="border-top: 1px solid black;border-right: 1px solid rgba(255,255,255,0);"></td>
                <td style="border-top: 1px solid black;">&#2537;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="opv_3_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="opv_3_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td style="border-top: 1px solid black;border-right: 1px solid rgba(255,255,255,0);"></td>
                <td colspan="2" style="border-top: 1px solid black;">&#2474;&#2495;&#2488;&#2495;&#2477;&#2495;-&#2537;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="pcv_3_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="pcv_3_satelite" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="3" style="border-top: 1px solid black;">&#2447;&#2478; &#2438;&#2480; &#2451; &#2451;&#2474;&#2495;&#2477;&#2495;-&#2538;</td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="mro_opv_center" class="mis3_field"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="mro_opv_satelite" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-top: 1px solid black;">&#2489;&#2494;&#2478;&#2503;&#2480; &#2463;&#2495;&#2453;&#2494;</td>
                <td align="center" style="border-top: 1px solid black;"></td>
                <td align="center" style="border-top: 1px solid black;"><input type="number" class="form-control" id="measles_vaccine_satelite" class="mis3_field"></td>
            </tr>
        </table>

        <table class="table page_3 table_hide" id="table_3" width="700" border="1" cellspacing="0" style="margin-top: 10px; font-size: 13px;">
            <tbody>
            <tr>
                <td colspan="13" style="text-align: left;border-right: 1px solid rgba(255,255,255,0);border-left: 1px solid rgba(255,255,255,0);border-top: 1px solid rgba(255,255,255,0);">
                    <h3>&#2437;&#2488;&#2497;&#2488;&#2509;&#2469; &#2486;&#2495;&#2486;&#2497;&#2480; &#2488;&#2478;&#2472;&#2509;&#2476;&#2495;&#2468; &#2458;&#2495;&#2453;&#2495;&#2510;&#2488;&#2494; &#2476;&#2509;&#2479;&#2476;&#2488;&#2509;&#2469;&#2494;&#2474;&#2472;&#2494; (IMCI)</h3>
                </td>
            </tr>
            <tr class="danger">
                <td rowspan="3" class="danger" style="text-align: center;vertical-align: middle;border-top: 1px solid black;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 10px;margin-top: 40px;">&#2453;&#2509;&#2480;&#2478;&#2495;&#2453; &#2472;&#2434;</span></td>
                <td colspan="12" style="text-align: center;border-top: 1px solid black;border-left: 1px solid black;">&#2486;&#2495;&#2486;&#2497; (&#2534;-&#2539;&#2543; &#2478;&#2494;&#2488;) &#2488;&#2503;&#2476;&#2494;</td>
            </tr>
            <tr class="danger">
                <td rowspan="2" style="text-align: center;border-top: 1px solid black;">&#2480;&#2507;&#2455;&#2503;&#2480; &#2472;&#2494;&#2478;</td>
                <td colspan="2" style="text-align: center;border-top: 1px solid black;">&#2534;-&#2536;&#2542; &#2470;&#2495;&#2472;</td>
                <td colspan="2" style="text-align: center;border-top: 1px solid black;">&#2536;&#2543;-&#2539;&#2543; &#2470;&#2495;&#2472;</td>
                <td colspan="2" style="text-align: center;border-top: 1px solid black;">&#2536;&#2478;&#2494;&#2488; - <&#2535; &#2476;&#2510;&#2488;&#2480;</td>
                <td colspan="2" style="text-align: center;border-top: 1px solid black;">&#2535; - &#2539; &#2476;&#2510;&#2488;&#2480;</td>
            </tr>
            <tr class="danger">
                <td style="text-align: center;border-top: 1px solid black;">&#2459;&#2503;&#2482;&#2503; </td>
                <td style="text-align: center;border-top: 1px solid black;">&#2478;&#2503;&#2527;&#2503;</td>
                <td style="text-align: center;border-top: 1px solid black;">&#2459;&#2503;&#2482;&#2503; </td>
                <td style="text-align: center;border-top: 1px solid black;">&#2478;&#2503;&#2527;&#2503;</td>
                <td style="text-align: center;border-top: 1px solid black;">&#2459;&#2503;&#2482;&#2503; </td>
                <td style="text-align: center;border-top: 1px solid black;">&#2478;&#2503;&#2527;&#2503;</td>
                <td style="text-align: center;border-top: 1px solid black;">&#2459;&#2503;&#2482;&#2503; </td>
                <td style="text-align: center;border-top: 1px solid black;">&#2478;&#2503;&#2527;&#2503;</td>
            </tr>
            <tr class="danger">
                <td style="text-align: center;border-top: 1px solid black;">(&#2535;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2536;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2537;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2538;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2539;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2540;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2541;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2542;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2543;)</td>
                <td style="text-align: center;border-top: 1px solid black;">(&#2535;&#2534;)</td>
            </tr>
            <tr>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2535;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2454;&#2497;&#2476; &#2478;&#2494;&#2480;&#2494;&#2468;&#2509;&#2478;&#2453; &#2480;&#2507;&#2455;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_0-28days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_0-28days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_29-59days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_29-59days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_2month-1year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_2month-1year_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_1-5year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="very_serious_disease_1-5year_female" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2536;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2472;&#2495;&#2441;&#2478;&#2507;&#2472;&#2495;&#2527;&#2494;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_0-28days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_0-28days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_29-59days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_29-59days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_2month-1year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_2month-1year_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_1-5year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="pneumonia_1-5year_female" class="mis3_field"></td>
            </tr>
            <tr>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2537;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2488;&#2480;&#2509;&#2470;&#2495; &#2453;&#2494;&#2486;&#2495; (&#2472;&#2495;&#2441;&#2478;&#2507;&#2472;&#2495;&#2527;&#2494; &#2472;&#2527;)</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_0-28days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_0-28days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_29-59days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_29-59days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_2month-1year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_2month-1year_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_1-5year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="cold_cough_1-5year_female" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2538;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2465;&#2494;&#2527;&#2480;&#2495;&#2527;&#2494;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_0-28days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_0-28days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_29-59days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_29-59days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_2month-1year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_2month-1year_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_1-5year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="diarrhea_1-5year_female" class="mis3_field"></td>
            </tr>
            <tr>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2539;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2460;&#2509;&#2476;&#2480; - &#2478;&#2509;&#2479;&#2494;&#2482;&#2503;&#2480;&#2495;&#2527;&#2494;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_0-28days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_0-28days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_29-59days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_29-59days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_2month-1year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_2month-1year_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_1-5year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="malaria_1-5year_female" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2540;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2460;&#2509;&#2476;&#2480; - &#2478;&#2509;&#2479;&#2494;&#2482;&#2503;&#2480;&#2495;&#2527;&#2494; &#2472;&#2527;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_0-28days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_0-28days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_29-59days_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_29-59days_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_2month-1year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_2month-1year_female" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_1-5year_male" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="fever_not_malaria_1-5year_female" class="mis3_field"></td>
            </tr>
            <tr>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2541;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2489;&#2494;&#2478;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_0-28days_male" ></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_0-28days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_29-59days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_29-59days_female" ></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_2month-1year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_2month-1year_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_1-5year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="measles_1-5year_female"></td>
            </tr>
            <tr class="success">
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2542;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2453;&#2494;&#2472;&#2503;&#2480; &#2488;&#2478;&#2488;&#2509;&#2479;&#2494;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_0-28days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_0-28days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_29-59days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_29-59days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_2month-1year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_2month-1year_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_1-5year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="ear_problem_1-5year_female"></td>
            </tr>
            <tr>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2543;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2437;&#2474;&#2497;&#2487;&#2509;&#2463;&#2495;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_0-28days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_0-28days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_29-59days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_29-59days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_2month-1year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_2month-1year_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_1-5year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="malnutrition_1-5year_female"></td>
            </tr>
            <tr class="success">
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2535;&#2534;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2437;&#2472;&#2509;&#2479;&#2494;&#2472;&#2509;&#2479;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_0-28days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_0-28days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_29-59days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_29-59days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_2month-1year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_2month-1year_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_1-5year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="other_disease_1-5year_female"></td>
            </tr>
            <tr>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">&#2535;&#2535;.</td>
                <td class="danger" style="border-top: 1px solid black;">&#2480;&#2503;&#2475;&#2494;&#2480;&#2453;&#2499;&#2468;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_0-28days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_0-28days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_29-59days_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_29-59days_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_2month-1year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_2month-1year_female"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_1-5year_male"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control mis3_field" id="imci_referred_1-5year_female"></td>
            </tr>
            </tbody>
        </table>
		
	
        <table class="table page_3 table_hide stock_table" id="table_4" width="700" border="1" cellspacing="0" style="margin-top: 40px; font-size: 13px;">
            <tbody>
            <tr>
                <td colspan="21" style="text-align: center;border-right: 1px solid rgba(255,255,255,0);border-left: 1px solid rgba(255,255,255,0);border-top: 1px solid rgba(255,255,255,0);">
                    <h3>&#2478;&#2494;&#2488;&#2495;&#2453; &#2478;&#2451;&#2460;&#2497;&#2470; &#2451; &#2476;&#2495;&#2468;&#2480;&#2467;&#2503;&#2480; &#2489;&#2495;&#2488;&#2494;&#2476; &#2476;&#2495;&#2487;&#2527;&#2453;</h3>
                </td>
            </tr>
            <tr class="danger">
                <td colspan="2" rowspan="2" style="border-top: 1px solid black;border-left: 1px solid black;"></td>
                <td colspan="2" style="text-align: center;border-top: 1px solid black;border-left: 1px solid black;">&#2454;&#2494;&#2476;&#2494;&#2480; &#2476;&#2524;&#2495; (&#2458;&#2453;&#2509;&#2480;)</td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2453;&#2472;&#2465;&#2478; (&#2474;&#2495;&#2488;)</span></td>
                <td colspan="2" style="text-align: center;border-top: 1px solid black;border-left: 1px solid black;">&#2439;&#2472;&#2460;&#2503;&#2453;&#2463;&#2503;&#2476;&#2482;</td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2438;&#2439; &#2439;&#2441; &#2465;&#2495; (&#2474;&#2495;&#2488;)</span></td>
                <td colspan="2" style="text-align: center;border-top: 1px solid black;border-left: 1px solid black;">&#2439;&#2478;&#2474;&#2509;&#2482;&#2509;&#2479;&#2494;&#2472;&#2509;&#2463; (&#2488;&#2503;&#2463;)</td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2439;&#2488;&#2495;&#2474;&#2495; (&#2465;&#2507;&#2460;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2478;&#2495;&#2488;&#2507;-&#2474;&#2509;&#2480;&#2507;&#2488;&#2509;&#2463;&#2482; (&#2465;&#2507;&#2460;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2447;&#2478; &#2438;&#2480; &#2447;&#2478; (&#2474;&#2509;&#2479;&#2494;&#2453;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2541;.&#2535;% &#2453;&#2509;&#2482;&#2507;&#2480;&#2489;&#2503;&#2453;&#2509;&#2488;&#2495;&#2465;&#2495;&#2472;</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2439;&#2472;&#2460;&#2503;&#2453;&#2486;&#2472; MgSO4</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2439;&#2472;&#2460;&#2503;&#2453;&#2486;&#2472; &#2437;&#2453;&#2509;&#2488;&#2495;&#2463;&#2507;&#2488;&#2495;&#2472; (&#2465;&#2507;&#2460;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2447;&#2478;&#2447;&#2472;&#2474;&#2495; (&#2488;&#2509;&#2479;&#2494;&#2488;&#2503;&#2463;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2488;&#2509;&#2479;&#2494;&#2472;&#2495;&#2463;&#2494;&#2480;&#2496; &#2474;&#2509;&#2479;&#2494;&#2465; (&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2447;&#2478; &#2438;&#2480; (&#2447;&#2478;&#2477;&#2495;&#2447;) &#2453;&#2495;&#2463; (&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2465;&#2503;&#2482;&#2495;&#2477;&#2494;&#2480;&#2496; &#2453;&#2495;&#2463; (&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</span></td>
                <td rowspan="2" class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2465;&#2495;&#2465;&#2495;&#2447;&#2488; &#2453;&#2495;&#2463; (&#2488;&#2434;&#2454;&#2509;&#2479;&#2494;)</span></td>
            </tr>
            <tr class="danger">
                <td class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2488;&#2497;&#2454;&#2495;</span></td>
                <td class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2438;&#2474;&#2472;</span></td>
                <td class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2477;&#2494;&#2527;&#2494;&#2482;</span></td>
                <td class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2488;&#2495;&#2480;&#2495;&#2462;&#2509;&#2460;</span></td>
                <td class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2439;&#2478;&#2474;&#2509;&#2482;&#2509;&#2479;&#2494;&#2472;&#2472;</span></td>
                <td class="danger" style="text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;">&#2460;&#2503;&#2465;&#2503;&#2482;</span></td>
            </tr>
            <tr>
                <td colspan="2" class="danger" style="text-align: left;border-top: 1px solid black;">&#2474;&#2498;&#2480;&#2509;&#2476;&#2503;&#2480; &#2478;&#2451;&#2460;&#2497;&#2470;</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_shukhi" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_apon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_condom" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_viale" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_syringe" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_iud" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_implanon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_jadelle" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_ecp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_misoprostol" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_mrm_pac" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_chlorehexidin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_mgso4" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_oxytocin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_mnp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_sanitary_pad" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_mr_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_delivery_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="previous_stock_dds_kit" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td colspan="2" class="danger" style="text-align: left;border-top: 1px solid black;">&#2458;&#2482;&#2468;&#2495; &#2478;&#2494;&#2488;&#2503;<br>&#2474;&#2494;&#2451;&#2527;&#2494; &#2455;&#2503;&#2459;&#2503; (+)</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_shukhi" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_apon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_condom" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_viale" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_syringe" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_iud" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_implanon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_jadelle" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_ecp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_misoprostol" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_mrm_pac" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_chlorehexidin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_mgso4" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_oxytocin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_mnp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_sanitary_pad" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_mr_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_delivery_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="stock_in_dds_kit" class="mis3_field"></td>
            </tr>
            <tr class="success">
                <td rowspan="2" class="danger" style="text-align: left;border-top: 1px solid black;">&#2488;&#2478;&#2472;&#2509;&#2476;&#2527;</td>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">(+)</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_shukhi" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_apon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_condom" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_viale" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_syringe" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_iud" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_implanon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_jadelle" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_ecp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_misoprostol" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_mrm_pac" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_chlorehexidin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_mgso4" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_oxytocin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_mnp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_sanitary_pad" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_mr_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_delivery_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_plus_dds_kit" class="mis3_field"></td>
            </tr>
            <tr>
                <td class="danger" style="text-align: center;border-top: 1px solid black;">(-)</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_shukhi" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_apon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_condom" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_viale" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_syringe" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_iud" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_implanon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_jadelle" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_ecp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_misoprostol" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_mrm_pac" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_chlorehexidin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_mgso4" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_oxytocin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_mnp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_sanitary_pad" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_mr_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_delivery_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="adjustment_stock_minus_dds_kit" class="mis3_field"></td>
            </tr>
            <tr>
                <td colspan="2" class="danger" style="text-align: left;border-top: 1px solid black;">&#2458;&#2482;&#2468;&#2495; &#2478;&#2494;&#2488;&#2503;<br>&#2453;&#2454;&#2472;&#2451; &#2478;&#2451;&#2460;&#2497;&#2470;<br>&#2486;&#2498;&#2467;&#2509;&#2479; &#2489;&#2527;&#2503; &#2469;&#2494;&#2453;&#2482;&#2503;<br>(&#2453;&#2507;&#2465;)</td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_shukhi" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_apon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_condom" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_viale" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_syringe" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_iud" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_implanon" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_jadelle" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_ecp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_misoprostol" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_mrm_pac" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_chlorehexidin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_mgso4" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_oxytocin" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_mnp" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_sanitary_pad" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_mr_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_delivery_kit" class="mis3_field"></td>
                <td style="text-align: center;border-top: 1px solid black;"><input type="number" class="form-control" id="zero_stock_code_dds_kit" class="mis3_field"></td>
            </tr>
            </tbody>
        </table>
		
    </div>
</body>
</html>