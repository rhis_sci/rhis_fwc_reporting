<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<table class=\"table\" id=\"table_header_1\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
									+ "<td width="94"><div style='border: 1px solid black;text-align: center;'>দুটি সন্তানের বেশি নয়<br>একটি হলে ভাল হয়।</div></td>"
									+ "<td width="376" style='text-align: center;'><b>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>
									  পরিবার পরিকল্পনা অধিদপ্তর<br>পরিবার পরিকল্পনা, মা ও শিশু স্বাস্থ্য কার্যক্রমের মাসিক অগ্রগতির প্রতিবেদন</b>"
									+ "<br>(UH&FWC এবং অন্যান্য প্রতিষ্ঠানের জন্য প্রযোজ্য)<br>"
									+ "<b>মাসঃ</b> "+bangla_month+" <b>সালঃ</b> "+bangla_year+"</td>"
									+ "<td width="247" class='page_no' style='text-align: right;'>এমআইএস ফরম-৩<br>
									  পৃষ্ঠা-১</td>"
									+ "</tr><tr><td colspan='3'><img width='60' src='./image/dgfp_logo.png'></td>
									</tr><tr></table>
									<br/>
<table class=\"table\" id=\"table_header_2\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
									+ "<td style='width: 38%'><b>কেন্দ্রের নামঃ  </b> "+facilityName+"</td>"
									+ "<td><b>ইউনিয়নঃ</b> "+union_info.get("union_name")+"</td>"
									+ "<td style='text-align: center;'><b>উপজেলা/থানাঃ </b> "+upazila_info.get("upazila_name")+"</td>"
									+ "<td style='text-align: right;'><b>জেলাঃ</b> "+zilla_info.get("zilla_name")+"</td>"
									+"</tr></table>	
									<br />
									<br />

								<table width="730" height="1240" cellpadding="0" class="table page_1 table_hide" id="table_1" style="font-size:11px;">
									<tr class='danger'>
									<td colspan="5" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সেবার ধরণ</td>
									<td width="43"   style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>
									<td width="59" style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>
									<td width="43" style='text-align: center;border-top: 1px solid black;'>মোট</td>
									</tr>
									<tr class='danger'>
									<td width="120" rowspan='30' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">পরিবার পরিকল্পনা পদ্ধতি</span></td></tr>
									<tr class='success' rowspan="3" >
									<td width="118" height="103" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>খাবার বড়ি </td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="326" height="185" border="0">
                                      <tr>
                                        <td width="80" rowspan="3">
										<div align="left" style="float:left; width:80px; height:60px; vertical-align:middle;" >স্বাভাবিক </div>										</td>
                                        <td width="230"><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td height="18"><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td rowspan="4"><div align="left" style="float:left; width:80px; height:80px; vertical-align:middle;">*প্রসব পরবর্তী </div></td>
                                        <td><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td height="24"><div align="left">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</div></td>
                                      </tr>
                                      <tr>
                                        <td height="27" colspan="2"><div align="left">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </div></td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="35">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="30">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="57">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="22">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="25">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top">
									<table border="0">
                                      <tr>
                                        <td width="49">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="23">&nbsp;</td>
                                      </tr>
                                    </table>									</td>
									</tr>
									<tr rowspan="3">
									<td height="73" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>কনডম</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top">
									<table width="326" height="185" border="0">
                                      <tr>
                                        <td width="80" rowspan="3">
										<div align="left" style="float:left; width:80px; height:60px; vertical-align:middle;" >স্বাভাবিক </div>										</td>
                                        <td width="230"><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td height="18"><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td rowspan="4"><div align="left" style="float:left; width:80px; height:80px; vertical-align:middle;">*প্রসব পরবর্তী </div></td>
                                        <td><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td height="24"><div align="left">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</div></td>
                                      </tr>
                                    </table>									</td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                     
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                   
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                     
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="75" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইনজেকটেবল</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top">
									<table width="326" height="185" border="0">
                                      <tr>
                                        <td width="80" rowspan="3">
										<div align="left" style="float:left; width:80px; height:60px; vertical-align:middle;" >স্বাভাবিক </div>										</td>
                                        <td width="230"><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td height="18"><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td rowspan="4"><div align="left" style="float:left; width:80px; height:80px; vertical-align:middle;">*প্রসব পরবর্তী </div></td>
                                        <td><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">মোট </div></td>
                                      </tr>
                                      <tr>
                                        <td height="24"><div align="left">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ </div></td>
                                      </tr>
									   <tr>
                                        <td height="27" colspan="2"><div align="left">ফলোআপ </div></td>
                                      </tr>
                                      <tr>
                                        <td height="27" colspan="2"><div align="left">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা</div></td>
                                      </tr>
                                    </table>
									</td>
									<td style='border-top: 1px solid black;' valign="top"><table height="209" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top">
									<table height="212" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table>									</td>
									<td style='border-top: 1px solid black;' valign="top">
									<table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									    <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="94" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>আই ইউ ডি</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="328" height="181" border="0">
                                      <tr>
                                        <td colspan="2">স্বাভাবিক</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">*প্রসব পরবর্তী </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">মোট</td>
                                      </tr>
                                      <tr>
                                        <td width="97" rowspan="2">খুলে ফেলা </td>
                                        <td width="215" height="24">মেয়াদ পূর্ণ  হওয়ার পর </td>
                                      </tr>
                                      <tr>
                                        <td>মেয়াদ পূর্ণ  হওয়ার পূর্বে </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="74" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইমপ্ল্যান্ট</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top">
									<table width="328" height="181" border="0">
                                      <tr>
                                        <td colspan="2">স্বাভাবিক </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">*প্রসব পরবর্তী </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">মোট</td>
                                      </tr>
                                      <tr>
                                        <td width="94" rowspan="2">খুলে ফেলা </td>
                                        <td width="218" height="24">মেয়াদ পূর্ণ  হওয়ার পর </td>
                                      </tr>
                                      <tr>
                                        <td>মেয়াদ পূর্ণ  হওয়ার পূর্বে </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                    </table>
									</td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="48" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<table width="110" height="209" border="0">
									  <tr>
										<td width="44" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">স্থায়ি পদ্ধতি</span></td>
										<td width="50" height="104">পুরুষ</td>
									  </tr>
									  <tr>
									    <td height="96">মহিলা</td>
								      </tr>
									</table>

									
									</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="329" border="0">
                                      <tr>
                                        <td width="91" rowspan="3">সম্পাদন</td>
                                        <td width="222">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ </td>
                                      </tr>
                                      <tr>
                                        <td height="24" colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                      <tr>
                                        <td rowspan="3">সম্পাদন</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ</td>
                                      </tr>
                                      <tr>
                                        <td height="18" colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr>
									<td height="25" colspan="4">গর্ভপাত পরবর্তী (PAC) পরিবার পরিকল্পনা পদ্ধতি</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td height="19" colspan="4">এমআর পরবর্তী পরিবার পরিকল্পনা পদ্ধতি</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td height="20" colspan="4">এমআরএম পরবর্তী পরিবার পরিকল্পনা পদ্ধতি</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									</tr>
</table>
<table width="730" cellpadding="0" class="table_page_1 table_hide" id="table_1" style="font-size:11px;">
<tr>
<td> * প্রসব পরবর্তী > প্রসবের পর হতে ১ বছরের মধে্ </td>
</tr>
</table>													
</body>
</html>
