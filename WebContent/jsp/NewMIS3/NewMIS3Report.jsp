<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<div id="mis_form1">
<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'><tr>
									<td width="94"><div style='border: 1px solid black;text-align: center;'>দুটি সন্তানের বেশি নয়<br>একটি হলে ভাল হয়।</div></td>
									<td width="376" style='text-align: center;'><b>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>
									  পরিবার পরিকল্পনা অধিদপ্তর<br>পরিবার পরিকল্পনা, মা ও শিশু স্বাস্থ্য কার্যক্রমের মাসিক অগ্রগতির প্রতিবেদন</b>
									<br>(UH&FWC এবং অন্যান্য প্রতিষ্ঠানের জন্য প্রযোজ্য)<br>
									<b>মাসঃ</b> "+bangla_month+" <b>সালঃ</b> "+bangla_year+"</td>"
									+ "<td width="247" class='page_no' style='text-align: right;'>এমআইএস ফরম-৩<br>
									  পৃষ্ঠা-১</td>"
									+ "</tr><tr><td colspan='3'><img width='60' src='./image/dgfp_logo.png'></td>
									</tr><tr></table>
									<br/>
<table class=\"table\" id=\"table_header_2\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
									+ "<td style='width: 38%'><b>কেন্দ্রের নামঃ  </b> "+facilityName+"</td>"
									+ "<td><b>ইউনিয়নঃ</b> "+union_info.get("union_name")+"</td>"
									+ "<td style='text-align: center;'><b>উপজেলা/থানাঃ </b> "+upazila_info.get("upazila_name")+"</td>"
									+ "<td style='text-align: right;'><b>জেলাঃ</b> "+zilla_info.get("zilla_name")+"</td>"
									+"</tr></table>	
									<br />
									<br />

								<table width="730" height="1240" cellpadding="0" class="table page_1 table_hide" id="table_1" style="font-size:11px;">
									<tr class='danger'>
									<td colspan="5" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সেবার ধরণ</td>
									<td width="43"   style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>
									<td width="59" style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>
									<td width="43" style='text-align: center;border-top: 1px solid black;'>মোট</td>
									</tr>
									<tr class='danger'>
									<td width="120" rowspan='30' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">পরিবার পরিকল্পনা পদ্ধতি</span></td></tr>
									<tr class='success' rowspan="3" >
									<td width="118" height="103" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>খাবার বড়ি </td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="326" height="185" border="0">
                                      <tr>
                                        <td width="80" rowspan="3">
										<div align="left" style="float:left; width:80px; height:60px; vertical-align:middle;" >স্বাভাবিক </div>										</td>
                                        <td width="230"><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td height="18"><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td rowspan="4"><div align="left" style="float:left; width:80px; height:80px; vertical-align:middle;">*প্রসব পরবর্তী </div></td>
                                        <td><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td height="24"><div align="left">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</div></td>
                                      </tr>
                                      <tr>
                                        <td height="27" colspan="2"><div align="left">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </div></td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="35">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="30">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="57">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="22">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="25">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top">
									<table border="0">
                                      <tr>
                                        <td width="49">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="23">&nbsp;</td>
                                      </tr>
                                    </table>									</td>
									</tr>
									<tr rowspan="3">
									<td height="73" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>কনডম</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top">
									<table width="326" height="185" border="0">
                                      <tr>
                                        <td width="80" rowspan="3">
										<div align="left" style="float:left; width:80px; height:60px; vertical-align:middle;" >স্বাভাবিক </div>										</td>
                                        <td width="230"><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td height="18"><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td rowspan="4"><div align="left" style="float:left; width:80px; height:80px; vertical-align:middle;">*প্রসব পরবর্তী </div></td>
                                        <td><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td height="24"><div align="left">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ</div></td>
                                      </tr>
                                    </table>									</td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                     
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                   
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="186" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                     
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="75" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইনজেকটেবল</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top">
									<table width="326" height="185" border="0">
                                      <tr>
                                        <td width="80" rowspan="3">
										<div align="left" style="float:left; width:80px; height:60px; vertical-align:middle;" >স্বাভাবিক </div>										</td>
                                        <td width="230"><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td height="18"><div align="left">মোট</div></td>
                                      </tr>
                                      <tr>
                                        <td rowspan="4"><div align="left" style="float:left; width:80px; height:80px; vertical-align:middle;">*প্রসব পরবর্তী </div></td>
                                        <td><div align="left">পুরাতন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">নতুন</div></td>
                                      </tr>
                                      <tr>
                                        <td><div align="left">মোট </div></td>
                                      </tr>
                                      <tr>
                                        <td height="24"><div align="left">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ </div></td>
                                      </tr>
									   <tr>
                                        <td height="27" colspan="2"><div align="left">ফলোআপ </div></td>
                                      </tr>
                                      <tr>
                                        <td height="27" colspan="2"><div align="left">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা</div></td>
                                      </tr>
                                    </table>
									</td>
									<td style='border-top: 1px solid black;' valign="top"><table height="209" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top">
									<table height="212" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table>									</td>
									<td style='border-top: 1px solid black;' valign="top">
									<table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
									   <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									    <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="94" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>আই ইউ ডি</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="328" height="181" border="0">
                                      <tr>
                                        <td colspan="2">স্বাভাবিক</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">*প্রসব পরবর্তী </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">মোট</td>
                                      </tr>
                                      <tr>
                                        <td width="97" rowspan="2">খুলে ফেলা </td>
                                        <td width="215" height="24">মেয়াদ পূর্ণ  হওয়ার পর </td>
                                      </tr>
                                      <tr>
                                        <td>মেয়াদ পূর্ণ  হওয়ার পূর্বে </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="74" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইমপ্ল্যান্ট</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top">
									<table width="328" height="181" border="0">
                                      <tr>
                                        <td colspan="2">স্বাভাবিক </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">*প্রসব পরবর্তী </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">মোট</td>
                                      </tr>
                                      <tr>
                                        <td width="94" rowspan="2">খুলে ফেলা </td>
                                        <td width="218" height="24">মেয়াদ পূর্ণ  হওয়ার পর </td>
                                      </tr>
                                      <tr>
                                        <td>মেয়াদ পূর্ণ  হওয়ার পূর্বে </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">প্রসব পরবর্তী মেয়াদ উত্তীর্ণ </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                    </table>
									</td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="48" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<table width="110" height="209" border="0">
									  <tr>
										<td width="44" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">স্থায়ি পদ্ধতি</span></td>
										<td width="50" height="104">পুরুষ</td>
									  </tr>
									  <tr>
									    <td height="96">মহিলা</td>
								      </tr>
									</table>

									
									</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="329" border="0">
                                      <tr>
                                        <td width="91" rowspan="3">সম্পাদন</td>
                                        <td width="222">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ </td>
                                      </tr>
                                      <tr>
                                        <td height="24" colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                      <tr>
                                        <td rowspan="3">সম্পাদন</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ফলোআপ</td>
                                      </tr>
                                      <tr>
                                        <td height="18" colspan="2">পার্শ্ব প্রতিক্রিয়া / জতিলতার জন্য সেবা </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									  <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
									
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="177" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr>
									<td height="25" colspan="4">গর্ভপাত পরবর্তী (PAC) পরিবার পরিকল্পনা পদ্ধতি</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td height="19" colspan="4">এমআর পরবর্তী পরিবার পরিকল্পনা পদ্ধতি</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									</tr>
									<tr>
									<td height="20" colspan="4">এমআরএম পরবর্তী পরিবার পরিকল্পনা পদ্ধতি</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									</tr>
</table>
<table width="730" cellpadding="0" class="table_page_1 table_hide" id="table_1" style="font-size:11px;">
<tr>
<td> * প্রসব পরবর্তী > প্রসবের পর হতে ১ বছরের মধে্ </td>
</tr>
</table>
</div>	
<div id="mis_form2">
						<table class="table" id="table_header_2" width="729" cellspacing="0" style='font-size:10px;'>
							<tr><td width="89">&nbsp;</td>
							<td width="359" style='text-align: center;'>&nbsp;</td>
							<td width="273" class='page_no' style='text-align: right;'>এমআইএস ফরম-৩<br>
							পৃষ্ঠা-2</td>
							<tr></table>		
							
							<table width="730" height="1165" cellpadding="0" class="table page_2 table_hide" id="table_2" style="font-size:11px;">
									<tr class='danger'>
									<td colspan="5" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সেবার ধরণ</td>
									<td width="43"   style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>
									<td width="59" style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>
									<td width="43" style='text-align: center;border-top: 1px solid black;'>মোট</td>
									</tr>
									<tr class='danger'>
									<td width="128" rowspan='30' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">প্রজনন স্বাস্থ্য সেবা </span></td>
									</tr>
									<tr class='success' rowspan="3" >
									<td width="110" height="103" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">টি টি প্রাপ্ত মহিলা </span></td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="331" border="0">
                                      <tr>
                                        <td width="321">১ম ডোজ </td>
                                      </tr>
                                      <tr>
                                        <td>২য় ডোজ </td>
                                      </tr>
                                      <tr>
                                        <td> ৩য় ডোজ </td>
                                      </tr>
                                      <tr>
                                        <td>৪ থ  ডোজ </td>
                                      </tr>
                                      <tr>
                                        <td height="23">৫ম ডোজ </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="96" border="0">
                                      <tr>
                                        <td width="35" height="17">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="57">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="22">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top">
									<table border="0">
                                      <tr>
                                        <td width="49">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table>									</td>
									</tr>
									<tr rowspan="3">
									<td height="73" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'> <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">গর্ভ কালীন সেবা </span></td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="330" height="193" border="0">
                                      <tr>
                                        <td width="319">পরিদর্শন - ১ </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ২ </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ৩ </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ৪ </td>
                                      </tr>
                                      <tr>
                                        <td>প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি বিষয়ে কাউনসেলিং প্রাপ্ত মায়ের সংখ্যা                                        </td>
                                      </tr>
                                      <tr>
                                        <td>প্রসব পূর্ব রক্ত ক্ষরণ (APH)  আক্রান্ত মায়ের সংখ্যা                                       </td>
                                      </tr>
                                      <tr>
                                        <td height="25">২৪-৩৪ সপ্তাহের মধ্যে ইনজেক্শান এন্টিনেটাল করটিকোস্টেরয়েড প্রাপ্ত গর্ভবতি মায়ের সংখ্যা                                        </td>
                                      </tr>
                                      <tr>
                                        <td>মিসোপ্রোস্টল বড়ি সরবারহ প্রাপ্ত গর্ভবতির সংখ্যা                                        </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="193" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="17">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                     
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="194" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="17">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                   
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="191" border="0">
                                      <tr>
                                        <td width="63">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="22">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                     
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="75" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">প্রসব সেবা 
									</span>
									</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="330" height="125" border="0">
                                      <tr>
                                        <td>স্বাভাবিক </td>
                                      </tr>
                                      <tr>
                                        <td>সিজারিয়ান </td>
                                      </tr>
                                      <tr>
                                        <td>অন্যান্য (ফরসেফ / ভ্যাকুয়াম / ব্রিচ)
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনা (AMTSL) অনুসরন করে প্রসব করানোর সংখ্যা 
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>পার্টোগ্রাফ ব্যবহার সংখ্যা
                                       </td>
                                      </tr>
                                      <tr>
                                        <td>প্রসব কালিন অতিরিক্ত  রক্ত ক্ষরণ (IPH) আক্রান্ত মায়ের সংখ্যা </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="41">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="45">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="25" colspan="4" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><div align="left">গর্ভপাত পরবর্তী সেবা (PAC) প্রদানের সংখ্যা 
									 </div></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="41" border="0">
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="55" border="0">
                                      <tr>
                                        <td width="48">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="54" border="0">
                                      <tr>
                                        <td width="44">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="28" colspan="4" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><div align="left">মারাত্তক প্রি-একলাম্পসিয়া রোগীর সংখ্যা </div></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="41" border="0">
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="55" border="0">
                                      <tr>
                                        <td width="47">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="54" border="0">
                                      <tr>
                                        <td width="44">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="94" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<table width="110" height="314" border="0">
									  <tr>
										<td width="44" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">প্রসবোত্তর সেবা</span></td>
										<td width="50" height="135" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">মা</span></td>
									  </tr>
									  <tr>
									    <td height="173" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">নবজাতক</span></td>
								      </tr>
									</table>
									</td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="329" height="316" border="0">
                                      <tr>
                                        <td width="323">পরিদর্শন - ১ </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ২ </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ৩ </td>
                                      </tr>

                                      <tr>
                                        <td>পরিদর্শন - ৪ </td>
                                      </tr>
                                      <tr>
                                        <td>প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি বিষয়ে কাউনসেলিং প্রাপ্ত মায়ের সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td>প্রসবোওর অতিরিক্ত রক্তক্ষরণ (PPH) আক্রান্ত মায়ের সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td>১ মিনিটের মধ্যে মোছানোর সংখ্যা  </td>
                                      </tr>
                                      <tr>
                                        <td>নাড়িকাটার পর মায়ের ত্বকে-ত্বক স্পর্শ প্রাপ্ত নবজাতকের সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td>জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাউয়ানোর সংখ্যা  </td> 
                                      </tr>
                                      <tr>
                                        <td>জন্মকালীন শ্বাসকষ্টে আক্রান্ত শিশুকে ব্যাগ ও মাস্ক ব্যবহার করে রিসাসিটেইট করার সংখ্যা 
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ১ </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ২</td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ৩ </td>
                                      </tr>
                                      <tr>
                                        <td>পরিদর্শন - ৪</td>
                                      </tr>
                                     
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="45" height="318" border="0">
                                      <tr>
                                        <td width="35">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="25">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="17">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="30">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="23">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                     
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="55" height="320" border="0">
                                      <tr>
                                        <td width="46">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="30">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="56" height="317" border="0">
                                      <tr>
                                        <td width="44">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="25">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="24">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="25">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="23">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="74" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'> <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">রেফার </span></td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="331" border="0">
                                      <tr>
                                        <td width="321">গর্ভকালীন জতিলতার রেফার সংখ্যা  </td>
                                      </tr>
                                      <tr>
                                        <td>  প্রসবকালীন জতিলতার রেফার সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td><p>প্রসবোত্তর জতিলতার রেফার সংখ্যা </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>একলাম্পসিয়া রোগীকে লোডীং ডোজ MgSO4 ইনজেকশান দিয়ে রেফার সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td height="25">নবজাতক কে জতিলতার জন্য  রেফার সংখ্যা
                                       </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="123" border="0">
                                      <tr>
                                        <td width="63" height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="23">&nbsp;</td>
                                      </tr>
                                      
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="122" border="0">
                                      <tr>
                                        <td width="63" height="20">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="17">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="20">&nbsp;</td>
                                      </tr>
                                      
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="122" border="0">
                                      <tr>
                                        <td width="63" height="22">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      
                                    </table></td>
									</tr>
									<tr>
									<td height="25" colspan="4" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><div align="left">ইসিপি গ্রহন কারীর সংখ্যা
									</div></td>
									<td style='border-top: 1px solid black;'>&nbsp;</td>
									<td style='border-top: 1px solid black;'>&nbsp;</td>
									<td style='border-top: 1px solid black;'>&nbsp;</td>
									</tr>
									<tr rowspan="3">
									<td height="48" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>বন্ধ্যা দম্পতি </td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="334" border="0">
                                      <tr>
                                        <td width="324">চিকিৎসা / পরামর্শ প্রাপ্ত
                                      </td>
                                      </tr>
                                      <tr>
                                        <td>রেফার কৃত
                                        </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="38">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="45">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="47">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									
									<tr rowspan="3">
									<td height="48" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>কিশোর / কিশোরীর সেবা ( ১০-১৯) বছর </td>
									<td colspan='3' style='border-top: 1px solid black;' valign="top"><table width="334" border="0">
                                      <tr>
                                        <td width="324">বয়ঃসন্ধিকালীন পরিবর্তন বিষয়ে কাউনসেলিং </td>
                                      </tr>
                                      <tr>
                                        <td>বাল্য বিবাহ ও কিশোরী মাতৃত্বের কুফল বিষয়ে কাউন্সেলিং                                        </td>
                                      </tr>
                                      <tr>
                                        <td>আয়রন ফলিক এসিড বড়ি সরবরাহ প্রাপ্ত কিশোরীর সংখ্যা</td>
                                      </tr>
                                      <tr>
                                        <td><p>প্রজননতন্ত্রের সংক্রামন ও যৌনবাহিত রোগ বিষয়ে কাউন্সেলিং                                        </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>প্রজননতন্ত্রের সংক্রামন ও যৌনবাহিত রোগ নিরাময়ে চিকাৎসা</td>
                                      </tr>
                                      <tr>
                                        <td>বিভিন্ন ধরনের পুস্টিকর ও সুষম খাবার বিষয়ে কাউন্সেলিং </td>
                                      </tr>
                                      <tr>
                                        <td>কৈশোর সহিংসতা প্রতিরোধ  বিষয়ে কাউনসেলিং</td>
                                      </tr>
                                      <tr>
                                        <td>মানসিক সমস্যা এবং মাদকাসক্তি প্রতিরোধ ও নিরাময় কাউন্সেলিং</td>
                                      </tr>
                                      <tr>
                                        <td>কিশোর - কিশোরীর ব্যক্তিগত পরিশকার পরিচছন্নতা ও কিশোরীর স্যানিটারি প্যাড ব্যাবহার বিষয়ে কাউন্সেলিং
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>রেফার</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="38">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="45">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="47">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									
									
</table>
</div>		
<div id="mis_form3">
<table class=\"table\" id=\"table_header_1\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
									+ "
									<td width="94">&nbsp;</td>
									"
									+ "<td width="376" style='text-align: center;'>&nbsp;</td>
									"
									+ "<td width="279" class='page_no' style='text-align: right;'>এমআইএস ফরম-৩<br>
									  পৃষ্ঠা-৩</td>
									"
									+ "<tr></table>
									

<table width="730" height="1254" cellpadding="0" class="table page_3 table_hide" id="table_3" style="font-size:11px;">
									<tr class='danger'>
									<td height="30" colspan="2" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সেবার ধরণ</td>
									<td width="61"   style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>
									<td width="58" style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>
									<td width="43" style='text-align: center;border-top: 1px solid black;'>মোট</td>
									</tr>
									
									<tr class='success' rowspan="3" >
									<td width="110" height="133" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">প্রজনন স্বাস্থ্য সেবা </span></td>
									<td width="444" valign="top" style='border-top: 1px solid black;'><table width="466" height="143" border="০">
                                      <tr>
                                        <td colspan="2">RTI / STI সংঙ্ক্রান্ত চিকিৎসা প্রদানকৃত রোগীর সংখ্যা                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">এম আর (এম ভিএ) সেবা প্রদানের সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">এম আর এম (ঔষধের মাধ্যমে এম আর) সেবা প্রদানের সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td width="201" rowspan="4">
										<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">
										জরায়ুর মুখ ও স্তন কান্সার স্ক্রীনিং										</span>										</td>
                                        <td width="248">মোট VIA পজিটভ   </td>
                                      </tr>
                                      <tr>
                                        <td>মোট VIA নেগেটিভ </td>
                                      </tr>
                                      <tr>
                                        <td>মোট CBE পজিটভ   </td>
                                      </tr>
                                      <tr>
                                        <td>মোট CBE নেগেটিভ </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="62" height="146" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="61" height="146" border="0">
                                      <tr>
                                        <td width="47">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="41" height="144" border="0">
                                      <tr>
                                        <td width="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="162" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'> <span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> শিশু সেবা( ০-৫৯ মাস) </span></td>
									<td valign="top" style='border-top: 1px solid black;'><table width="466" height="627" border="0">
                                      <tr>
                                        <td width="199" rowspan="8"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">কেএমসি সেবা </span></td>
                                        <td colspan="2">কেএমসি শুরু হয়েছে এমন শিশুর সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">এই ক্লিনিক / হাসপাতালে জন্ম গ্রহনকারী কে এম সি শুরু করা শিশুর সংখ্যা  </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">মোট কতজন শিশুকে ডিসচারজ ( ছাড়পত্র ) প্রদান করা হয়েছে  </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">ডিসচাজ অন রিকোয়েস্ট (অনুরোধের) এর ভিত্তিতে ডিসচাজ প্রদান করা শিশুর সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">কেএমসি ডিসকন্টিনিউয়েশান ( কেএমসি সেবা ব্যাহত ) হওয়া শিশুর সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">কতজন শিশুকে জটিলতার জন্য রেফার করা হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">কেএমসি সেবাপ্রাপ্ত শিশু মৃত্যুর সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">কতজন শিশুকে কেএমসি ফলোআপ করা হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td rowspan="14"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">টিকা প্রাপ্ত (০-১৫ মাস বয়সী) শিশুর সংখ্যা </span></td>
                                        <td colspan="2">বিসিজি ( জন্মের পর থেকে ) </td>
                                      </tr>
                                      <tr>
                                        <td width="103" rowspan="3">পেন্টাভ্যালেন্ট (ডিপিটি , হেপ-বি , হিব) </td>
                                        <td width="136">১ (শিশুর বয়স ৬ সপ্তাহ হলে)  </td>
                                      </tr>
                                      <tr>
                                        <td>২ (শিশুর বয়স ১০ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td>৩ (শিশুর বয়স ১৪ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td rowspan="3">পিসিভি টিকা </td>
                                        <td>১ (শিশুর বয়স ৬ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td>২ (শিশুর বয়স ১০ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td>৩ (শিশুর বয়স ১৪ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td rowspan="3">ওপিভি </td>
                                        <td>১ (শিশুর বয়স ৬ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td>২ (শিশুর বয়স ১০ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td>৩ (শিশুর বয়স ১৪ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td rowspan="2">আইপিভি (ফ্রাক শনাল) </td>
                                        <td>১ (শিশুর বয়স ৬ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td>২ (শিশুর বয়স ১৪ সপ্তাহ হলে) </td>
                                      </tr>
                                      <tr>
                                        <td rowspan="2">এম আর টিকা </td>
                                        <td>১ (শিশুর বয়স ৯ মাস পুরন হলে) </td>
                                      </tr>
                                      <tr>
                                        <td>১ (শিশুর বয়স ১৫ মাস পুরন হলে) </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="62" height="622" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="29">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="62" height="622" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="29">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="44" height="622" border="0">
                                      <tr>
                                        <td width="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="33">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="32">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="29">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="34">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="87" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> জন্ম-মৄতু্ </span></td>
									<td valign="top" style='border-top: 1px solid black;'><table width="463" border="0">
                                      <tr>
                                        <td colspan="2">জীবিত জন্ম ( Live Birth) </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">কম জন্ম ওজনে ( জন্ম ওজন ২৫০০ গ্রামের কম) জন্ম গ্রহনকারী নবজাতক এর সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">কম জন্ম ওজনে ( জন্ম ওজন ২০০০ গ্রামের কম) জন্ম গ্রহনকারী নবজাতক এর সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">অপরিনত ( ৩৭ সপ্তাহের পুরবে জন্ম)  নবজাতক এর সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td width="199" rowspan="3">মৃত - জন্ম </td>
                                        <td width="248">ফ্রেশ </td>
                                      </tr>
                                      <tr>
                                        <td>ম্যাসারেটেড </td>
                                      </tr>
                                      <tr>
                                        <td>মোট মৃত জন্মের সংখ্যা </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">নব জাতকের মৃত্যু (০ - ২৮ দিন ) </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">মাতৃ মৃত্যু্র জন্মের সংখ্যা
                                        </td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									
									<tr rowspan="3">
									<td height="87" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>বহীর বিভাগে সেবা গ্রহনকারী রোগী </td>
									<td valign="top" style='border-top: 1px solid black;'><table width="467" border="0">
                                      <tr>
                                        <td width="457">পুরুষ </td>
                                      </tr>
                                      <tr>
                                        <td>মহিলা </td>
                                      </tr>
                                      <tr>
                                        <td>শিশু (০ - ১ বছর) </td>
                                      </tr>
                                      <tr>
                                        <td>শিশু (১ - ৫ বছর) </td>
                                      </tr>
                                      <tr>
                                        <td>অন্যান্য</td>
                                      </tr>
                                      <tr>
                                        <td>মোট</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="79" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>অন্তর বিভাগে ভর্তি রোগী </td>
									<td valign="top" style='border-top: 1px solid black;'><table width="464" border="0">
                                      <tr>
                                        <td>মহিলা </td>
                                      </tr>
                                      <tr>
                                        <td>শিশু (০ - ১ বছর) </td>
                                      </tr>
                                      <tr>
                                        <td>শিশু (১ - ৫ বছর) </td>
                                      </tr>
                                      <tr>
                                        <td>মোট</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      
                                      
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="35" colspan="2" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><div align="left">কেন্দ্রে কয়টি স্বাস্থ্য শিক্ষা (বিসিসি) কার্যক্রম সুম্পান্ন করা হয়েছে 
									</div></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="35" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="35" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="35" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
									<tr rowspan="3">
									<td height="38" colspan="2" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><div align="left">এসএসিএমও (SACMO) কত্ক কয়টি স্কুলে  স্বাস্থ্য  শিক্ষা (বিসিসি) কার্যক্রম সম্পাদন করা হয়েছে
									 </div></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="35" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="35" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table height="35" border="0">
                                      <tr>
                                        <td width="52">&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
</table>
</div>	
<div id="mis_form4">
<table class="table" id="table_header_1" width="759" cellspacing="0" style='font-size:10px;'>
									<tr>
									<td width="86">&nbsp;</td>
									<td width="345" style='text-align: center;'>&nbsp;</td>
									<td width="320" class='page_no' style='text-align: right;'>এমআইএস ফরম-৩<br>
									পৃষ্ঠা-৪</td>
  <tr></table>
									

<table width="758" height="171" cellpadding="1" class="table page_4 table_hide" id="table_4" style="font-size:11px;">
									<tr class='danger'>
									<td height="30" colspan="2" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সেবার ধরণ</td>
									<td width="62"   style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>
									<td width="61" style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>
									<td width="64" style='text-align: center;border-top: 1px solid black;'>মোট</td>
									</tr>
									
									<tr class='success' rowspan="3" >
									<td width="67" height="133" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">পুষ্টি সেবা </span></td>
									<td width="490" valign="top" style='border-top: 1px solid black;'><table width="490" height="341">
                                      <tr>
                                        <td width="481">আই এফ এ, হাত ধোয়া , মায়ের পুষ্টি বিষয়ক কাউন্সেলিং </td>
                                      </tr>
                                      <tr>
                                        <td><p>কতজন মায়ের (গর্ভবতি মহিলা) ওজন নেয়া হয়েছে </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>আইওয়াইসি এফ , ভিটামিন - এ বিষয়ক মায়ের কাউন্সেলিং </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন গর্ভবতি মাকে আয়রন ফলিক এসিড এবং ক্যালসিয়াম বড়ি দেওয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০ - ২৩ মাস বয়সী শিশুর মাকে আয়রন ফলিক এসিড এবং ক্যালসিয়াম বড়ি দেওয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-২৩ মাস  বয়সী শিশুকে মাল্টিপল মাইক্রনিউট্রিয়েন্ট পাউডার (এমএনপি) দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী শিশুকে ভিটামিন - এ দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ২৪-৫৯ মাস বয়সী শিশুকে কৃমি নাশক বড়ি দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী ডায়রিয়া আক্রান্ত শিশুকে খাবার স্যলাইনের  সাথে জিংক  বড়ি দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী শিশুকে গ্রোথ   মনিটরিং (GMP *) করা হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী MAM (মাঝারি তিব্র অপুষ্টি) আক্রান্ত শিশুকে সনাক্ত এবং চিকিৎসা প্রদান করা হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td><p>কতজন ০-৫৯ মাস বয়সী SAM   ( মারত্বক তিব্র অপুষ্টি ) আক্রান্ত শিশুকে সনাক্ত করা হয়েছে এবং রেফার করা হয়েছে </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০-৫৯ মাস বয়সী শিশুর <span class="style1">Stunting (বয়সের তুলনায় কম উচ্চতা সম্পান্ন) সনাক্ত করা হয়েছে </span></td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০-৫৯ মাস বয়সী শিশুর <span class="style1">wasting  ( উচ্চতার তুলনায় কম ওজন সম্পান্ন) সনাক্ত করা হয়েছে </span></td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০-৫৯ মাস বয়সী শিশুর <span class="style1">Underweight (বয়সের তুলনায় কম ওজন সম্পান্ন) সনাক্ত করা হয়েছে</span></td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="65" height="337" border="0">
                                      <tr>
                                        <td width="59">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="22">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="29">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="61" height="336" border="0">
                                      <tr>
                                        <td width="47">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="62" height="341" border="0">
                                      <tr>
                                        <td width="56">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
</table>
<p></p>
<table>
  <tr class='danger'>
    <td width="749" height="30" > &nbsp;অসুস্থ শিশুর সমন্নিত চিকৎসা ব্যবস্থাপনা (IMCI) : </td>
  </tr>
</table>
<table width="753" height="91" cellpadding="0" class="table page_4 table_hide" id="table_4" style="font-size:11px;">

  <tr class='danger' rowspan="3" >
    <td width="60" height="85" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ক্রমিক নং</span></td>
    <td width="685" valign="top" style='border-top: 1px solid black;'><table width="680" border="0">
      <tr>
        <td height="20" colspan="14"><div align="center">শিশু (০-৫৯ মাস) সেবা </div></td>
        </tr>
      <tr>
        <td colspan="3" rowspan="3"> রোগের নাম </td>
        <td colspan="2"><div align="center">০ - ২৮ দিন </div></td>
        <td colspan="2"><div align="center">২৯ - ৫৯ দিন </div></td>
        <td colspan="2"> <div align="center">২ মাস &lt; ১ বছর </div></td>
        <td colspan="2"> <div align="center">১- ৫ বছর </div></td>
        <td colspan="2"> <div align="center">মোট </div></td>
		 <td width="46" rowspan="2" ><div align="center">সর্ব মোট </div></td>
        </tr>
      <tr>
        <td width="41" height="38"><div align="center">ছেলে </div></td>
        <td width="37"><div align="center">মেয়ে </div></td>
        <td width="39"><div align="center">ছেলে </div></td>
        <td width="38"><div align="center">মেয়ে </div></td>
        <td width="38"><div align="center">ছেলে </div></td>
        <td width="40"><div align="center">মেয়ে </div></td>
        <td width="35"><div align="center">ছেলে </div></td>
        <td width="29"><div align="center">মেয়ে </div></td>
        <td width="36"><div align="center">ছেলে </div></td>
        <td width="41"><div align="center">মেয়ে </div></td>
        </tr>
     
      
    </table></td>
  </tr>
   <tr class='danger' rowspan="3" >
    <td width="60" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><div align="center">(১)</div></td>
    <td width="685" valign="top" style='border-top: 1px solid black;'><table width="680" border="0">
      <tr>
        <td width="212"><div align="center">(২)</div></td>
        <td width="37"><div align="center">(৩)</div></td>
        <td width="38"><div align="center">(৪)</div></td>
        <td width="40"><div align="center">(৫)</div></td>
        <td width="40"><div align="center">(৬)</div></td>
        <td width="36"><div align="center">(৭)</div></td>
        <td width="42"><div align="center">(৮)</div></td>
        <td width="33"><div align="center">(৯)</div></td>
        <td width="36"><div align="center">(১০)</div></td>
        <td width="32"><div align="center">(১১)</div></td>
        <td width="40"><div align="center">(১২)</div></td>
        <td width="44"><div align="center">(১৩)</div></td>
        </tr>
      

    </table></td>
  </tr>
</table>
<table width="754" border="0">
  <tr>
    <td width="59"><div align="center">১</div></td>
    <td width="139">খুব মারাত্তক রোগ - সংকাটপন্ন অসুস্থথা </td>
    <td width="42">&nbsp;</td>
    <td width="46">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="49">&nbsp;</td>
    <td width="35">&nbsp;</td>
    <td width="45">&nbsp;</td>
    <td width="33">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="52">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">২</div></td>
    <td>খুব মারাত্তক রোগ - সম্ভাব্য মারাত্তক সংক্রমন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৩</div></td>
    <td>খুব মারাত্তক রোগ - দ্রুত শাস নিউমোনিয়া </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৪</div></td>
    <td>স্থানিয় সীমিত সংক্রমন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৫</div></td>
    <td>রেফারে রাজী হওয়া </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৬</div></td>
    <td>১ম ডোজ আন্টিবায়টিক ইঞ্জেকশন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৭</div></td>
    <td>২য় ডোজ আন্টিবায়টিক ইঞ্জেকশন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৮</div></td>
    <td>নিউমোনিয়া</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৯</div></td>
    <td>নিউমোনিয়া নয়, সর্দি কাশি </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১০</div></td>
    <td>ডায়রিয়া</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১১</div></td>
    <td>জ্বর - ম্যালেরিয়া
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১২</div></td>
    <td>জ্বর - ম্যালেরিয়া নয় </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৩</div></td>
    <td>হাম</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৪</div></td>
    <td>কানের সমস্যা</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৫</div></td>
    <td>অপুষ্টি</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৬</div></td>
    <td>অন্যান্য</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৭</div></td>
    <td>রেফারকৃ্ত</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><p align="right">সর্ব মোট </p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="754">
  <tr>
    <td width="360"><div align="right">FWV কত্ক স্যাটেলাইট ক্লিনিক ঃ লক্ষমাত্রা </div></td>
    <td width="149">&nbsp;</td>
    <td width="104"><p align="right">অনুষঠিত </p>    </td>
    <td width="113">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">SACMO কতৃক NSV ক্লায়েন্ট রেফার ঃ লক্ষমাত্রা </div></td>
    <td>&nbsp;</td>
    <td><div align="right">অর্জন</div></td>
    <td>&nbsp;</td>
  </tr>
</table>

<p>* Growth Monitoring and Promotion </p>
</div>	
<div id="mis_form5">
<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'>
  <tr>
									
									<td width="94">&nbsp;</td>
									<td width="376" style='text-align: center;'>&nbsp;</td>
									<td width="608" class='page_no' style='text-align: right;'>এমআইএস ফরম-৩<br>
									  পৃষ্ঠা-5</td>
									<tr></table>
									

<p>&nbsp;</p>
<table width="1160" border="0">
  <tr>
    <td width="1154"><div align="center">মাসিক মওজুদ ও বিতরনের হিসাব বিষয়ক</div></td>
  </tr>
</table>
<table width="1160" height="487" cellpadding="1" class="table page_5 table_hide" id="table_5" style="font-size:11px; border:thin; border-color:#333333; border-width:1px;">
									
				 <tr class='danger'>
				   <td width="223" rowspan="2">&nbsp;</td>
                    <td height="49" colspan="2"><p>খাবার বড়ি (চক্র) </p>
                    </td>
                    <td width="63" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">কনডম(পিস)</span></td>
                    <td colspan="2">ইনজেকটেবল </td>
                    <td width="45" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">আই ইউ ডি (পিস)</span></td>
                    <td colspan="2">ইমপ্লান্ট(সেট)</td>
                    <td width="36" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ই
					সিপি (ডোজ)</span></td>
                    <td width="42" rowspan="2">
					<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">মিসোপ্রাস্টল(ডোজ)</span></td>
                    <td width="31" rowspan="2">
					<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">এমআরএম (প্যাক)</span></td>
                    <td width="36" rowspan="2"><span style="disp
					lay: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">৭.১% ক্লরোহেক্সিডিন</span></td>
                    <td width="34" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন MgSO4</span></td>
                    <td width="38" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন অক্সিটোক্সিন(ডোজ)</span></td>
                    <td width="39" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">এমএনপি(স্যাসেট)</span></td>
                    <td width="41" rowspan="2">
					<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">স্যানিটারি প্যাড(সংখ্যা)
					</span></td>
                    <td width="38" rowspan="2">
					<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> এম আর(এমভিএ)কিট(সংখ্যা)
					</span></td>
                    <td width="40" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> ডেলিভারি কিট(সংখ্যা)</span></td>
                    <td width="42" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;"> ডিডিএস কিট(সংখ্যা)</span></td>
                    <td width="38" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন এ্যান্টিনেটাল করটিকোস্টেরয়েড </span></td>
                    <td width="54" rowspan="2"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ইনজেকশন জেন্টামাইসিন</span></td>
  </tr>
					
					<tr>
                      <td width="42" height="67"><p><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">সুখী</span></p>
                     </td>
                      <td width="40"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">আপন</span></td>
                      <td width="42"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ভায়াল</span></td>
                      <td width="37"><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">সিরিঞ্জ</span></td>
                      <td width="33">ইমপ্লানন</td>
                      <td width="34">জেডেল</td>
                    </tr>
					
                  <tr>
                    <td height="31"><div align="center">১</div></td>
                    <td><div align="center">২</div></td>
                    <td><div align="center">৩</div></td>
                    <td><div align="center">৪</div></td>
                    <td><div align="center">৫</div></td>
                    <td><div align="center">৬</div></td>
                    <td><div align="center">৭</div></td>
                    <td><div align="center">৮</div></td>
                    <td><div align="center">৯</div></td>
                    <td><div align="center">১০</div></td>
                    <td><div align="center">১১</div></td>
                    <td><div align="center">১২</div></td>
                    <td><div align="center">১৩</div></td>
                    <td><div align="center">১৪</div></td>
                    <td><div align="center">১৫</div></td>
                    <td><div align="center">১৬</div></td>
                    <td><div align="center">১৭</div></td>
                    <td><div align="center">১৮</div></td>
                    <td><div align="center">১৯</div></td>
                    <td><div align="center">২০</div></td>
                    <td><div align="center">২১</div></td>
                    <td><div align="center">২২</div></td>
                  </tr>
                  <tr>
                    <td height="30">পূর্বের মউজুদ</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="36">চলতি মাসে পাওয়া গেছে (+) </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="36">চলতি মাসের মোট মউজুদ </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td rowspan="2"><table width="127" height="60">
                      <tr>
                        <td width="69" rowspan="2"><div align="center">সমন্বয় </div></td>
                        <td width="42"><div align="center">(+)</div></td>
                      </tr>
                      <tr>
                        <td><div align="center">(-)</div></td>
                      </tr>
                    </table></td>
                    <td height="29">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="19">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="29">সর্বমোট </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="35">চলতি মাসে বিতরন করা হয়েছে (-) </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="38">অবশিষ্ট </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="48">চলতি মাসে কখন ও মউজুদ শূন্য হয়ে থাকলে(কোড) </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
</table>
</td>					
				</tr>
									
									
</table>
<table width="749">
  <tr>
    <td width="171">মউজুদ শূন্যূতার কোড : </td>
    <td width="31">ক</td>
    <td width="117"> সরবরাহ পাওয়া যায়নি </td>
    <td width="26">খ</td>
    <td width="113"> <p>অপর্যাপ্ত সরবরাহ</p>
    </td>
    <td width="30">গ</td>
    <td width="136">হঠাৎ চাহিদা বৃদ্ধি পাওয়া </td>
    <td width="28">ঘ</td>
    <td width="57">আন্যান্য</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="748">
  <tr>
    <td width="166" rowspan="3" valign="top"><div align="center">তারিখ ঃ </div></td>
    <td width="433">স্বাক্ষর ঃ </td>
    <td width="133">&nbsp;</td>
  </tr>
  <tr>
    <td>নাম ঃ </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p>MO (Clinic)/MO(FW)/SACMO/FWV/দায়িত্ব প্রাপ্ত কর্মকর্তা </p>
    (জেটি পযোজ্য টিকা দিন)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>								
</body>
</html>
