<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
.style1 {font-size: 12px}
-->
</style>
</head>

<body>
<table class="table" id="table_header_1" width="700" cellspacing="0" style='font-size:10px;'>
									<tr>
									<td width="94">&nbsp;</td>
									<td width="376" style='text-align: center;'>&nbsp;</td>
									<td width="279" class='page_no' style='text-align: right;'>এমআইএস ফরম-৩<br>
									পৃষ্ঠা-৪</td>
									<tr></table>
									

<table width="758" height="171" cellpadding="1" class="table page_4 table_hide" id="table_4" style="font-size:11px;">
									<tr class='danger'>
									<td height="30" colspan="2" style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সেবার ধরণ</td>
									<td width="62"   style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>
									<td width="61" style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>
									<td width="64" style='text-align: center;border-top: 1px solid black;'>মোট</td>
									</tr>
									
									<tr class='success' rowspan="3" >
									<td width="67" height="133" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>
									<span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">পুষ্টি সেবা </span></td>
									<td width="490" valign="top" style='border-top: 1px solid black;'><table width="490" height="341">
                                      <tr>
                                        <td width="481">আই এফ এ, হাত ধোয়া , মায়ের পুষ্টি বিষয়ক কাউন্সেলিং </td>
                                      </tr>
                                      <tr>
                                        <td><p>কতজন মায়ের (গর্ভবতি মহিলা) ওজন নেয়া হয়েছে </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>আইওয়াইসি এফ , ভিটামিন - এ বিষয়ক মায়ের কাউন্সেলিং </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন গর্ভবতি মাকে আয়রন ফলিক এসিড এবং ক্যালসিয়াম বড়ি দেওয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০ - ২৩ মাস বয়সী শিশুর মাকে আয়রন ফলিক এসিড এবং ক্যালসিয়াম বড়ি দেওয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-২৩ মাস  বয়সী শিশুকে মাল্টিপল মাইক্রনিউট্রিয়েন্ট পাউডার (এমএনপি) দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী শিশুকে ভিটামিন - এ দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ২৪-৫৯ মাস বয়সী শিশুকে কৃমি নাশক বড়ি দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী ডায়রিয়া আক্রান্ত শিশুকে খাবার স্যলাইনের  সাথে জিংক  বড়ি দেয়া হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী শিশুকে গ্রোথ   মনিটরিং (GMP *) করা হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ৬-৫৯ মাস বয়সী MAM (মাঝারি তিব্র অপুষ্টি) আক্রান্ত শিশুকে সনাক্ত এবং চিকিৎসা প্রদান করা হয়েছে </td>
                                      </tr>
                                      <tr>
                                        <td><p>কতজন ০-৫৯ মাস বয়সী SAM   ( মারত্বক তিব্র অপুষ্টি ) আক্রান্ত শিশুকে সনাক্ত করা হয়েছে এবং রেফার করা হয়েছে </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০-৫৯ মাস বয়সী শিশুর <span class="style1">Stunting (বয়সের তুলনায় কম উচ্চতা সম্পান্ন) সনাক্ত করা হয়েছে </span></td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০-৫৯ মাস বয়সী শিশুর <span class="style1">wasting  ( উচ্চতার তুলনায় কম ওজন সম্পান্ন) সনাক্ত করা হয়েছে </span></td>
                                      </tr>
                                      <tr>
                                        <td>কতজন ০-৫৯ মাস বয়সী শিশুর <span class="style1">Underweight (বয়সের তুলনায় কম ওজন সম্পান্ন) সনাক্ত করা হয়েছে</span></td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="65" height="337" border="0">
                                      <tr>
                                        <td width="59">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="22">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="21">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="16">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="31">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="26">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="19">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="18">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td height="29">&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="61" height="336" border="0">
                                      <tr>
                                        <td width="47">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									<td style='border-top: 1px solid black;' valign="top"><table width="62" height="341" border="0">
                                      <tr>
                                        <td width="56">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table></td>
									</tr>
</table>
<p></p>
<table>
  <tr class='danger'>
    <td width="749" height="30" > &nbsp;অসুস্থ শিশুর সমন্নিত চিকৎসা ব্যবস্থাপনা (IMCI) : </td>
  </tr>
</table>
<table width="753" height="91" cellpadding="0" class="table page_4 table_hide" id="table_4" style="font-size:11px;">

  <tr class='danger' rowspan="3" >
    <td width="60" height="85" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style="display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;">ক্রমিক নং</span></td>
    <td width="685" valign="top" style='border-top: 1px solid black;'><table width="680" border="0">
      <tr>
        <td height="20" colspan="14"><div align="center">শিশু (০-৫৯ মাস) সেবা </div></td>
        </tr>
      <tr>
        <td colspan="3" rowspan="3"> রোগের নাম </td>
        <td colspan="2"><div align="center">০ - ২৮ দিন </div></td>
        <td colspan="2"><div align="center">২৯ - ৫৯ দিন </div></td>
        <td colspan="2"> <div align="center">২ মাস &lt; ১ বছর </div></td>
        <td colspan="2"> <div align="center">১- ৫ বছর </div></td>
        <td colspan="2"> <div align="center">মোট </div></td>
		 <td width="46" rowspan="2" ><div align="center">সর্ব মোট </div></td>
        </tr>
      <tr>
        <td width="41" height="38"><div align="center">ছেলে </div></td>
        <td width="37"><div align="center">মেয়ে </div></td>
        <td width="39"><div align="center">ছেলে </div></td>
        <td width="38"><div align="center">মেয়ে </div></td>
        <td width="38"><div align="center">ছেলে </div></td>
        <td width="40"><div align="center">মেয়ে </div></td>
        <td width="35"><div align="center">ছেলে </div></td>
        <td width="29"><div align="center">মেয়ে </div></td>
        <td width="36"><div align="center">ছেলে </div></td>
        <td width="41"><div align="center">মেয়ে </div></td>
        </tr>
     
      
    </table></td>
  </tr>
   <tr class='danger' rowspan="3" >
    <td width="60" valign="top" class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><div align="center">(১)</div></td>
    <td width="685" valign="top" style='border-top: 1px solid black;'><table width="680" border="0">
      <tr>
        <td width="212"><div align="center">(২)</div></td>
        <td width="37"><div align="center">(৩)</div></td>
        <td width="38"><div align="center">(৪)</div></td>
        <td width="40"><div align="center">(৫)</div></td>
        <td width="40"><div align="center">(৬)</div></td>
        <td width="36"><div align="center">(৭)</div></td>
        <td width="42"><div align="center">(৮)</div></td>
        <td width="33"><div align="center">(৯)</div></td>
        <td width="36"><div align="center">(১০)</div></td>
        <td width="32"><div align="center">(১১)</div></td>
        <td width="40"><div align="center">(১২)</div></td>
        <td width="44"><div align="center">(১৩)</div></td>
        </tr>
      

    </table></td>
  </tr>
</table>
<table width="754" border="0">
  <tr>
    <td width="59"><div align="center">১</div></td>
    <td width="139">খুব মারাত্তক রোগ - সংকাটপন্ন অসুস্থথা </div></td>
    <td width="42">&nbsp;</td>
    <td width="46">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="49">&nbsp;</td>
    <td width="35">&nbsp;</td>
    <td width="45">&nbsp;</td>
    <td width="33">&nbsp;</td>
    <td width="43">&nbsp;</td>
    <td width="52">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">২</div></td>
    <td>খুব মারাত্তক রোগ - সম্ভাব্য মারাত্তক সংক্রমন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৩</div></td>
    <td>খুব মারাত্তক রোগ - দ্রুত শাস নিউমোনিয়া </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৪</div></td>
    <td>স্থানিয় সীমিত সংক্রমন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৫</div></td>
    <td>রেফারে রাজী হওয়া </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৬</div></td>
    <td>১ম ডোজ আন্টিবায়টিক ইঞ্জেকশন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৭</div></td>
    <td>২য় ডোজ আন্টিবায়টিক ইঞ্জেকশন </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৮</div></td>
    <td>নিউমোনিয়া</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">৯</div></td>
    <td>নিউমোনিয়া নয়, সর্দি কাশি </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১০</div></td>
    <td>ডায়রিয়া</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১১</div></td>
    <td>জ্বর - ম্যালেরিয়া
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১২</div></td>
    <td>জ্বর - ম্যালেরিয়া নয় </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৩</div></td>
    <td>হাম</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৪</div></td>
    <td>কানের সমস্যা</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৫</div></td>
    <td>অপুষ্টি</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৬</div></td>
    <td>অন্যান্য</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center">১৭</div></td>
    <td>রেফারকৃ্ত</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><p align="right">সর্ব মোট </p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="754">
  <tr>
    <td width="360"><div align="right">FWV কত্ক স্যাটেলাইট ক্লিনিক ঃ লক্ষমাত্রা </div></td>
    <td width="149">&nbsp;</td>
    <td width="104"><p align="right">অনুষঠিত </p>    </td>
    <td width="113">&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">SACMO কতৃক NSV ক্লায়েন্ট রেফার ঃ লক্ষমাত্রা </div></td>
    <td>&nbsp;</td>
    <td><div align="right">অর্জন</div></td>
    <td>&nbsp;</td>
  </tr>
</table>

<p>* Growth Monitoring and Promotion </p>
</body>
</html>
