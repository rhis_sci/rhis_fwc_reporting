<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

        <style>
        	.select2 .select2-container{
        		width: 100% !important;
        	}
        </style>
        
        <div class="box box-default remove_div">
	        <div class="box-header with-border">
	          <h3 class="box-title" id="box_title"></h3>
	
	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
	          </div>
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body search_form">
	          <div class="row">
	            <div class="col-md-12">
				 <div class="form-group col-md-6">
	                <label>Division</label>
	                <select class="form-control select2" id="RdivName" style="width: 100%;">
	                </select>
	              </div>
	              <div class="form-group col-md-6">
	                <label>District</label>
	                <select class="form-control select2" id="RzillaName" style="width: 100%;">
	                </select>
	              </div>
	              <div class="form-group col-md-6">
	                <label>Upazila</label>
	                <select class="form-control select2" id="RupaZilaName" style="width: 100%;">
	                </select>
	              </div>
	              <div class="form-group col-md-4" id="unionName">
		                <label>Union</label>
		                <select class="form-control select2" id="RunionName" required="required" style="width: 100%;">
		                </select>
		          </div>
                 
	              <div class="box-footer col-md-12">
	              	<input type="hidden" id="formType" value="">
                    <input type="button" id="FacilityList" class="btn btn-primary" value="Submit">
	              </div>
	            </div>
	          </div>
	          <!-- /.row -->
	        </div>
			
			   <div class="row" id="table_row" style="display:none;">
		        <div class="col-md-12">
		          <div class="box">
		            <div class="box-header with-border">
		              <h3 class="box-title" id="table_title"></h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body table-responsive no-padding" id="table_append"></div>
		          </div>
		        </div>   
		     </div> 
	    
	    <div id="model_box"></div> 
		   <div class="row" id="table_row" style="display:none;">
+		        <div class="col-md-12">
+		          <div class="box">
+		            <div class="box-header with-border">
+		              <h3 class="box-title" id="table_title"></h3>
+		            </div>
+		            <!-- /.box-header -->
+		            <div class="box-body table-responsive no-padding" id="table_append"></div>
+		          </div>
+		        </div>   
+		     </div> 
	        
     </div>    
     
     <script>
	     $(document).ready(function(){
	     	$(".select2").select2();
	     	
	     	$(".tableType").on('click', function(){
	     		if($(".tableType:checked").val()=="1")
     			{
     				$("#tbs").show();
     				$("#tsns").hide();
     				$("#sts").hide();
     				$("#syncType").val("3");
     			}
	     		else if($(".tableType:checked").val()=="2")
	     		{
	     			$("#tsns").show();
     				$("#tbs").hide();
     				$("#sts").hide();
     				$("#syncType").val("1");
	     		}
	     		else
	     		{
	     			$("#sts").show();
     				$("#tbs").hide();
     				$("#tsns").hide();
     				$("#syncType").val("");
	     		}	
	     	});

	     	$("#RzillaName").on('change', function () {
//				if($("#RzillaName").val()=="69" || $("#RzillaName").val()=="42"){
                    $("#tbs").val("ancservice,clientmap,clientmap_extension,death,delivery,elco,fpinfo,immunizationhistory,newborn,pacservice,pncservicechild,pncservicemother,pregwomen,regserial,fpmethods,fpmapping,fpexamination,gpservice,iudservice,iudfollowupservice,implantservice,implantfollowupservice,pillcondomservice,womaninjectable,permanent_method_service,permanent_method_followup_service");
                    $("#tsns").val("member,treatmentlist,diseaselist,symptomlist");
//				}
//				else{
//				    $("#tbs").val("ancService,clientMap,clientmap_extension,death,delivery,elco,fpInfo,immunizationHistory,newBorn,pacService,pncServiceChild,pncServiceMother,pregWomen,regSerial,FPMethods,FPMapping,FPExamination,gpService,iudService,iudFollowupService,implantService,implantFollowupService,pillCondomService,womanInjectable");
//				    $("#tsns").val("Member,treatmentList,diseaseList,symptomList");
//				}
            })

	     });	
     </script>   
        
        
