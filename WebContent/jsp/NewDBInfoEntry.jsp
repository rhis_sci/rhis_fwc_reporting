<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

        
        <div class="box box-default remove_div">
	        <div class="box-header with-border">
	          <h3 class="box-title" id="box_title">New DB Info Entry Form</h3>
	
	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
	          </div>
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body search_form">
	          <div class="row">
	            <div class="col-md-12">
				 <div class="form-group col-md-4">
	                <label>Division</label>
	                <select class="form-control select2" id="RdivName" style="width: 100%;">
	                </select>
	              </div>
				  
	              <div class="form-group col-md-4">
	                <label>District</label>
	                <select class="form-control select2" id="RzillaName" style="width: 100%;">
	                </select>
	              </div>
	              <div class="form-group col-md-4">
	                <label>Upazila</label>
	                <select class="form-control select2" id="RupaZilaName" style="width: 100%;">
	                </select>
	              </div>
                  <div class="form-group col-md-12">
                    <label>DB URL</label>
                    <input type="text" class="form-control" id="DBUrl" required="required" placeholder="DB URL">
                  </div>
                  <div class="form-group col-md-6">
                    <label>DB User Name</label>
                    <input type="text" class="form-control" id="DBUserName" required="required" placeholder="DB User Name">
                  </div>
                  <div class="form-group col-md-6">
                    <label>DB User Password</label>
                    <input type="text" class="form-control" id="DBUserPass" required="required" placeholder="DB User Password">
                  </div>
	              <div class="box-footer col-md-12">
                    <input type="button" id="NewDBEntryForm" class="btn btn-primary" value="Submit">
	              </div>
	            </div>
	          </div>
	          <!-- /.row -->
	        </div>
	    
	    <div id="model_box"></div> 
	        
     </div>    
     
     <script>
     	$(".select2").select2();
     </script>   
        
        
