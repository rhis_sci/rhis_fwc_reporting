<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/12/2022
  Time: 1:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.sci.rhis.report.PregnantWomenList" %>
<%--<% String i = (String) request.getAttribute("m");--%>
    <%--out.print(request.getAttribute("m"));%>--%>


<% int zillaid = Integer.parseInt((String) request.getAttribute("zilla"));%>
<% int upazilaid = Integer.parseInt((String) request.getAttribute("upazila"));%>


<script type="text/javascript">

    function printData()
    {
        //var tableToPrint=document.getElementById("reportDataTable");
        var tableToPrint=document.getElementsByClassName("reportExport")[0];
        newWin= window.open("");
        newWin.document.write(tableToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    $('#btnPrint').on('click',function(){
        if($('#reportType').val()==4)
            printDataMIS3();
        else
            printData();
    });
</script>

<style>

    .td_color
    {
        background-color: #F2DEDE !important;
    }

    .rot
    {
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        white-space: nowrap;
        width:40px;
        margin-top: 100px;
    }



    .fixedHeader-floating{
        top: 7% !important;
    }
    .w3-container:after,.w3-container:before,.w3-panel:after,.w3-panel:before,.w3-row:after,.w3-row:before,.w3-row-padding:after,.w3-row-padding:before,
    .w3-container,.w3-panel{padding:0.01em 20px}
    /*tbody, thead tr {*/
    /*display: table;*/
    /*width: 100%;*/
    /*table-layout: fixed;*/
    /*}*/
</style>


<div class="tile-body nopadding" id="reportTable">
    <div class="row">
        <div class="col-md-12">
            <div class="dropdown" style="float: right; margin-right: 2%;">
                <button class="btn btn-primary" type="button" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Export As
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'xlsx'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> XLSX</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'csv'});"><i class="fa fa-file-excel-o" aria-hidden="true"></i> CSV</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'txt'});"><i class="fa fa-file-text-o" aria-hidden="true"></i> TXT</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'doc'});"><i class="fa fa-file-word-o" aria-hidden="true"></i> Word</a></li>
                    <li><a href="javascript:void(0)" onClick="doExport('.reportExport', {type: 'json'});"><img src='image/json.png' alt="JSON" style="width:24px"> JSON</a></li>
                    <%--<li><a class="btn btn-default buttons-pdf buttons-html5" tabindex="0" aria-controls="reportTable" href="#"><span>PDF</span></a></li>--%>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="printDiv" style="margin-top: 2%;">
                <% out.println(request.getAttribute("Result")); %>
            </div>
        </div>
        <div id="model_box" title="Basic dialog"></div>
    </div>
</div>

<div id ="pregdetail">

</div>




<%--<!-- Modal -->--%>
<%--<%@include file="/jsp/pregnantwomenlist/pregwomenModalView.jsp" %>--%>





<input type="hidden" id="graphResultSet" value='<% out.println(request.getAttribute("Graph")); %>'>
<input type="hidden" id="mapResultSet" value='<% out.println(request.getAttribute("Map")); %>'>
<input type="hidden" id="divisionSet" value='<% out.println(request.getAttribute("Division")); %>'>
<script src="js/lib/ddtf.js"></script>
<script>
    $('.filterdTable').ddTableFilter();
</script>

<script type="text/javascript" src="library/js-xlsx/xlsx.core.min.js"></script>
<script type="text/javascript" src="library/FileSaver/FileSaver.min.js"></script>
<script type="text/javascript" src="library/jsPDF/jspdf.min.js"></script>
<script type="text/javascript" src="library/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<script type="text/javascript" src="library/html2canvas/html2canvas.min.js"></script>
<script type="text/javascript" src="library/js/tableExport.js"></script>
<script type="text/javascript" src="js/lib/jquery.base64.min.js"></script>

<script type="text/javaScript">
    $('.clickable').click(function () {
        $('.clickable').css({"background-color": "rgb(255, 255, 255)"});
        $(this).css({"background-color": "rgb(0, 172, 214)"});
        $('.clickable > span').removeClass("bg-aqua").addClass("bg-gray");
        $(this).find("span:first").removeClass("bg-gray").addClass("bg-aqua");
        $('.clickable span img').each(function(index, element) {
            var url = $(element).prop("src").replace("png","jpg");
            $(element).attr("src",url);
        });
        url = $(this).find("span img").prop("src").replace("jpg","png");
        $(this).find("span img").attr("src",url);
        openTab($(this).attr("data"));
    });
    function openTab(tabName) {
        var tabcontent;
        tabcontent = document.getElementsByClassName("tab");
        for (var i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        document.getElementById(tabName).style.display = "block";
    }
    function doExport(selector, params) {
        var options = {
            //ignoreRow: [1,11,12,-2],
            //ignoreColumn: [0,-1],
            //pdfmake: {enabled: true},
            tableName: 'Service Statistics',
            worksheetName: 'Service Statistics'
        };

        $.extend(true, options, params);

        $(selector).tableExport(options);
    }

    function DoOnCellHtmlData(cell, row, col, data) {
        var result = "";
        if (data != "") {
            var html = $.parseHTML( data );

            $.each( html, function() {
                if ( typeof $(this).html() === 'undefined' )
                    result += $(this).text();
                else if ( $(this).is("input") )
                    result += $('#'+$(this).attr('id')).val();
                else if ( $(this).is("select") )
                    result += $('#'+$(this).attr('id')+" option:selected").text();
                else if ( $(this).hasClass('no_export') !== true )
                    result += $(this).html();
            });
        }
        return result;
    }

    function DoOnMsoNumberFormat(cell, row, col) {
        var result = "";
        if (row > 0 && col == 0)
            result = "\\@";
        return result;
    }

</script>



<script>
    $(document).ready(function() {
        //$('#reportDataTable').DataTable();


        $(document).ready(function() {
            var filterFunc = function (sData) {
                return sData.replace(/\n/g," ").replace( /<.*?>/g, "" );
            };
            var table = $('#reportDataTable').dataTable({
                "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
                "processing": true,
                "paging": true,
                "lengthMenu": [ 10, 25, 50, 75, 100 ],
                "language": {
                    "info": "Total _TOTAL_",
                    "infoEmpty": "Total _TOTAL_",
                    "zeroRecords": "No records",
                    "lengthMenu": "Show _MENU_ entries",
                    "searchIcon":"",
                    "paginate": {
                        "first": "First",
                        "last": "Last",
                        "next": "Next",
                        "previous": "Prev"
                    }
                },
                "initComplete": function(oSettings, json) {
                    $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                    $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                    $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");

                },
                "buttons": ['copy',{
                    extend: 'excelHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'csvHtml5',
                    title:"Service Statistics"
                },{
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    title:"Service Statistics",
                    pageSize: 'LEGAL'
                },{
                    extend: 'print',
                    text: 'Print',
                    autoPrint: true
                }
                ]
            });
        });
        var table = $('#reportDataTableFilter').dataTable({
            "dom": "<'row'<'col-md-2'l><'col-md-2'><'col-md-3'B><'col-md-2'><'col-md-2'f>>rt<'row'<'col-md-6'i>p>",
            "processing": true,
            "paging": true,
            "lengthMenu": [ 10, 25, 50, 75, 100 ],
            "language": {
                "info": "Total _TOTAL_",
                "infoEmpty": "Total _TOTAL_",
                "zeroRecords": "No records",
                "lengthMenu": "Show _MENU_ entries",
                "searchIcon":"",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Prev"
                }
            },
            "initComplete": function(oSettings, json) {
                $("#reportDataTable_wrapper .dataTables_filter input").wrap('<div class="input-group"></div>');
                $("#reportDataTable_wrapper .dataTables_filter input").before('<span class="input-group-addon filter_icon"><i class="glyphicon glyphicon-search"></i></span>');
                $("#reportDataTable_wrapper .dataTables_filter input").attr("placeholder", "Search");
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );

            },
            "buttons": ['copy',{
                extend: 'excelHtml5',
                title:"Service Statistics"
            },{
                extend: 'csvHtml5',
                title:"Service Statistics"
            },{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title:"Service Statistics"
            }, {
                extend: 'print',
                text: 'Print',
                autoPrint: true
            }
            ]
        } );




        $('#reportDataTable').on('click', 'tbody tr', function () {
            var zilla=<% out.println(zillaid); %>;
            var upazila = <% out.println(upazilaid); %>;
            var row = $('#reportDataTable').DataTable().row($(this)).data();
            console.log(row);   //full row of array data
            console.log("pp"+row[1]);   //EmployeeId
           <!--modal-->
           // $('#exampleModalCenter').show();
           //  $('#detailInfoModalCenter').modal('show');
            console.log("data"+zilla+upazila+row[1]+row[10]+row[11]);
            var reportObj = new Object();

            reportObj.healthid = row[0];
            reportObj.nrcid = row[1];
            reportObj.lmp = row[10];
            reportObj.edd = row[11];
            reportObj.zilla = zilla;
            reportObj.client = row[6];
            reportObj.age = row[9];
            reportObj.hus = row[7];
            reportObj.para = row[12];
            reportObj.gravida = row[13];
            reportObj.number_of_anc = row[14];
            reportObj.upazilaid = upazila;

            $.ajax({
                url: "pregwomenserviceinfo",
                type: "post",
                data: {"reportInfo":JSON. stringify(reportObj)},
                success: function (data) {
                    $('#pregdetail').html(data);
                    $('#detailInfoModalCenter').modal('show');
                    
                }

            });


            <%--<%PregnantWomenList.getPregwomenServiceDetails(<%out.print(js);%>--%>




        });
    });



</script>
