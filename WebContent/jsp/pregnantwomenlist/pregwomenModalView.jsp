<%@ page import="org.json.JSONObject" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/13/2022
  Time: 1:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String client = (String) request.getAttribute("client");%>
<% String hus = (String) request.getAttribute("hus");%>
<% String age = (String) request.getAttribute("age");%>
<% String lmp = (String) request.getAttribute("lmp");%>
<% String edd = (String) request.getAttribute("edd");%>
<% String para = (String) request.getAttribute("para");%>
<% String gravida = (String) request.getAttribute("gravida");%>
<% String healthid = (String) request.getAttribute("healthid");%>
<%--<% String refercentername = (String) request.getAttribute("refercentername");%>--%>
<% List<JSONObject> dataList = (List<JSONObject>) request.getAttribute("result"); %>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="detailInfoModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
    <div class="modal-dialog modal-lg" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><b>Client Data</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <%--<div class="modal-body">--%>
            <%--<div class="container-fluid">--%>

            <div class="col-md-12" style=" background-color: honeydew;">

                <table class="table">
                    <tr>
                        <td class="text-bold">NRC/Health ID:</td>
                        <td class="pull-left"><%=healthid%></td>
                        <td></td>
                        <td class="text-bold">Name:</td>
                        <td class="pull-left"><%=client%></td>
                        <td></td>
                        <td class="text-bold">Husband Name:</td>
                        <td class="pull-left"><%=hus%></td>
                        <td></td>
                        <td class="text-bold">Age:</td>
                        <td class="pull-left"><%=age%></td>
                    </tr>

                    <tr>
                        <td class="text-bold">LMP:</td>
                        <td class="pull-left"><%=lmp%></td>
                        <td></td>
                        <td class="text-bold">EDD:</td>
                        <td class="pull-left"><%=edd%></td>
                        <td></td>
                        <td class="text-bold">PARA:</td>
                        <td class="pull-left"><%=para%></td>
                        <td></td>
                        <td class="text-bold">Gravida:</td>
                        <td class="pull-left"><%=gravida%></td>
                    </tr>

                    <%--<tr>--%>
                        <%--<td class="text-bold">Zilla:</td>--%>
                        <%--<td class="pull-left">MANIKGANJ</td>--%>
                        <%--<td></td>--%>
                        <%--<td class="text-bold">Upazila:</td>--%>
                        <%--<td class="pull-left">DAULATPUR</td>--%>
                        <%--<td></td>--%>
                        <%--<td class="text-bold">Union:</td>--%>
                        <%--<td class="pull-left">BACHAMARA</td>--%>
                        <%--<td></td>--%>
                        <%--<td class="text-bold">Village:</td>--%>
                        <%--<td class="pull-left">PURAN PARA</td>--%>
                    <%--</tr>--%>

                </table>

            </div>


            <!-- TAB -->
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#anc" data-toggle="tab">ANC</a></li>
                        <li ><a href="#delivery" data-toggle="tab">DELIVERY</a></li>
                        <li ><a href="#pnc" data-toggle="tab">PNC</a></li>
                        <li ><a href="#ppfp" data-toggle="tab">PPFP</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="anc">
                            <table class="table table-hover">
                                <thead>
                                <tr class="">
                                    <th>Visit Date</th>
                                    <th>BP</th>
                                    <th>Urine Sugar</th>
                                    <th>Urine Albumin</th>

                                    <th>Weight</th>
                                    <th>Edema</th>
                                    <th>Uterus height</th>
                                    <th>Fetus heart rate</th>
                                    <th>Refer</th>
                                </tr>

                                </thead>
                                <tbody>
                                <% for (JSONObject data : dataList){%>
                                <tr class="test" data-nrc="1234678952" data-edd="80" data-client_name="Test client">
                                    <td><%=data.get("visitdate")%></td>
                                    <td><%=data.get("bpsystolic")%>/<%=data.get("bpdiastolic")%></td>
                                    <td><%=data.get("urinesugar")%></td>
                                    <td><%=data.get("urinealbumin")%></td>

                                    <td><%=data.get("weight")%></td>
                                    <td><%=data.get("edema")%></td>
                                    <td><%=data.get("uterusheight")%></td>
                                    <td><%=data.get("fetusheartrate")%></td>
                                    <td><%=data.get("refer")%></td>
                                    <%--<td><%=data.get("refercentername")%></td>--%>
                                </tr>
                                <%}%>

                                </tbody>
                            </table>
                        </div>
                        <%--<div class="active tab-pane" id="delivery">
                            <table class="table table-hover">
                                <thead>
                                <tr class="">
                                    <th>Visit Date</th>
                                    <th>BP</th>
                                    <th>Urine Sugar</th>
                                    <th>Urine Albumin</th>

                                    <th>Weight</th>
                                    <th>Edema</th>
                                    <th>Uterus height</th>
                                    <th>Fetus heart rate</th>
                                    <th>Refer</th>
                                </tr>

                                </thead>
                                <tbody>
                                <% for (JSONObject data : dataList){%>
                                <tr class="test" data-nrc="1234678952" data-edd="80" data-client_name="Test client">
                                    <td><%=data.get("visitdate")%></td>
                                    <td><%=data.get("bpsystolic")%>/<%=data.get("bpdiastolic")%></td>
                                    <td><%=data.get("urinesugar")%></td>
                                    <td><%=data.get("urinealbumin")%></td>

                                    <td><%=data.get("weight")%></td>
                                    <td><%=data.get("edema")%></td>
                                    <td><%=data.get("uterusheight")%></td>
                                    <td><%=data.get("fetusheartrate")%></td>
                                    <td><%=data.get("refer")%></td>
                                </tr>
                                <%}%>

                                </tbody>
                            </table>
                        </div>--%>
                        <div class="tab-pane" id="pnc">
                            PNC
                        </div>
                        <div class="tab-pane" id="ppfp">
                            PPFP
                        </div>

                    </div>


                </div>
            </div>


            <%--</div>--%>

            <%--</div>--%>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <%--<button type="button" class="btn btn-primary">Save changes</button>--%>
            </div>
        </div>
    </div>
</div>

<%--<script>--%>
    <%--$(window).on('load', function() {--%>
        <%--$('#detailInfoModalCenter').modal('show');--%>
    <%--});--%>


<%--</script>--%>




