<%--
  Created by IntelliJ IDEA.
  User: arjuda.anjum
  Date: 2/2/2021
  Time: 4:15 PM
  To change this template use File | Settings | File Templates.
--%>
<!--geo group-->
<div class="row">
    <div class="col-md-12">
        <div class="form-group col-md-3" id="divisionName">
            <label>Division</label>
            <select class="form-control select2" id="RdivName" style="width: 100%;">
            </select>
        </div>
        <div class="form-group col-md-3" id="districtName">
            <label>District</label>
            <select class="form-control select2" id="RzillaName" style="width: 100%;">
            </select>
        </div>
        <div class="form-group col-md-3" id="upazilaName">
            <label>Upazila</label>
            <select class="form-control select2" id="RupaZilaName" style="width: 100%;">
            </select>
        </div>
        <div class="form-group col-md-3" id="unionName">
            <label>Union</label>
            <select class="form-control select2" id="RunionName" style="width: 100%;">
            </select>
        </div>
    </div>
</div>
<!--/geo group-->
