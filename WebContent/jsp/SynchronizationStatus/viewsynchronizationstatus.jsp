<%--
  Created by IntelliJ IDEA.
  User: Fazlur Rahman
  Date: 16/08/2022
  Time: 9:07 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="remove_div">
    <div class="box box-default">
        <div class="box-header with-border">
            <!--<h3 class="box-title" id="box_title"></h3>-->
            <h3 class="box-title" id="box_title">View Synchronization Status</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body search_form">
            <!--row-->
            <div class="row">
                <div class="col-md-12">
                    <!--geo group-->
                    <%@include file="/jsp/util/geoSection.jsp" %>

                    <!--/geo group-->
                    <!--date section-->

                    <div class="form-group col-md-12 date_type">
                        <label>Select Service</label><br/>

                        <span class="levelYear"><input type="checkbox" id="delivery" onclick="checkCheckbox()" name="date_type" checked="true"
                                                        class="flat-red">&nbsp;DELIVERY&nbsp;&nbsp;</span>
                        <div  style="display: none;"><input type="text" name="delivery_text" id="delivery_text" value="1" /></div>

                        <span class="levelDate"><input type="checkbox" id="gp" onclick="checkCheckbox()" name="date_type" checked="true"
                                                       value="1"  class="flat-red">&nbsp;GP&nbsp;&nbsp;</span>
                        <div  style="display: none;"><input type="text" name="gp_text" id="gp_text" value="1" /></div>

                        <span class="levelMonth"><input type="checkbox" id="anc" onclick="checkCheckbox()" name="date_type" checked="true"
                                                        value="1"  class="flat-red">&nbsp;ANC&nbsp;&nbsp;</span>
                        <div  style="display: none;"><input type="text" name="anc_text" id="anc_text" value="1" /></div>

                        <span class="levelMonth"><input type="checkbox" id="pillcondom" onclick="checkCheckbox()" name="date_type" checked="true"
                                                        value="1" class="flat-red">&nbsp;PILLCONDOM&nbsp;&nbsp;</span>
                        <div  style="display: none;"><input type="text" name="pillcondom_text" id="pillcondom_text" value="1" /></div>

                        <span class="levelYear"><input type="checkbox" id="iud" onclick="checkCheckbox()" name="date_type" checked="true"
                                                       value="1" class="flat-red">&nbsp;IUD&nbsp;&nbsp;&nbsp;</span>
                        <div  style="display: none;"><input type="text" name="iud_text" id="iud_text"  value="1"/></div>

                        <span class="levelMonth"><input type="checkbox" id="pregwomen" onclick="checkCheckbox()" name="date_type" checked="true"
                                                        value="1" class="flat-red">&nbsp;PREGWOMEN&nbsp;&nbsp;&nbsp;</span>
                        <div  style="display: none;"><input type="text" name="pregwomen_text" id="pregwomen_text" value="1" /></div>

                    </div>



                    <div class="form-group col-md-6 transfer_date" id="TransferDate">
                        <label class="control-label" for="end_date">Date</label>

                            <input type="text" id="end_date" class="form-control" readonly>
                    </div>

                    <%@include file="/jsp/util/submitButton.jsp" %>

                </div>
            </div>



        </div>
    </div>

    <div class="row" id="table_row" style="display:none;">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" id="table_title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="table_append"></div>
            </div>
        </div>
    </div>

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="library/datepicker/datepicker.min.css">
    <!-- bootstrap datepicker -->
    <script src="library/datepicker/bootstrap-datepicker.min.js"></script>

    <script>

        $(".select2").select2();


  /*      $("input[type='checkbox']").change(function(){

            isChecked('delivery');
            isChecked('gp');
            isChecked('anc')
            isChecked('pillcondom');
            isChecked('iud');
            isChecked('pregwomen')
        })


        function isChecked(checkboxid){
            if(($("#"+checkboxid).is(':checked')))
                $("#"+checkboxid).is()
            else
                $("#"+checkboxid).value="0";


        }*/

        function checkCheckbox() {
            var delivery = document.getElementById("delivery");
            var gp = document.getElementById("gp");
            var anc = document.getElementById("anc");
            var pillcondom = document.getElementById("pillcondom");
            var iud = document.getElementById("iud");
            var pregwomen = document.getElementById("pregwomen");

            if (delivery.checked == true) {

                document.getElementById('delivery_text').value = "1";
            }
            else
            {
                document.getElementById('delivery_text').value = "0";
            }

            if (gp.checked == true) {

                document.getElementById('gp_text').value = "1";
            }
            else
            {
                document.getElementById('gp_text').value = "0";
            }

            if (anc.checked == true) {

                document.getElementById('anc_text').value = "1";
            }
            else
            {
                document.getElementById('anc_text').value = "0";
            }

            if (pillcondom.checked == true) {

                document.getElementById('pillcondom_text').value = "1";
            }
            else
            {
                document.getElementById('pillcondom_text').value = "0";
            }

            if (iud.checked == true) {

                document.getElementById('iud_text').value = "1";
            }
            else
            {
                document.getElementById('iud_text').value = "0";
            }

            if (pregwomen.checked == true) {

                document.getElementById('pregwomen_text').value = "1";
            }
            else
            {
                document.getElementById('pregwomen_text').value = "0";
            }


            /*if (gp.checked == true) {
                $('#gp').value=1;
            }
            else
            {
                $('#gp').value==2;
            }*/
        }

        $('#end_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });


    </script>
</div>
