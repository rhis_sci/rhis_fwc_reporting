var mnh_data;
var population = [];
var family_planning;
var general_patient;
var facility_assessment;
var emisCoverageJson={
    "03":[4,14,51,73,89,91,95],
    // "12":[33,85,90],
    "12":[2,4,7,13,33,63,85,90,94],
    "13":[47,49,95],
    "27":[10,12,17,21,30,38,43,47,56,60,64,69,77],
    "29":[21,56,62],
    "30":[14,51,94],
    "36":[2,5,11,26,44,68,71,77],
    "42":[40,43,73,84],
    "44":[14,19,33,42,71,80],
    "49":[6,8,9,18,52,61,77,79,94],
    "50":[15,71,94],
    "51":[33,43,58,65,73],
    "54":[40,54,80,87],
    "56":[10,46,70,78],
    "58":[14,35,56,65,74,80,83],
    "69":[9,15,41,44,55,63,91],
    "75":[7,10,21,36,47,80,83,85,87],
    "85":[3,27,42,49,58,73,76,92],
    "91":[8,27,38,41],
    "93":[9,19,23,25,28,38,47,57,66,76,85,95],
    "94":[8,51,82,86,94]
};
var cd = new Date();
var month = cd.getMonth()+1;
var year = cd.getFullYear();

//This function will return GO ID zilla_upazila_union
function getGoID(feature) {
    // Current GO ID zillaid_upazillaID_unionid
    zillaid=feature.properties.zillaid;
    upazilaid=feature.properties.upazilaid;
    unionid=feature.properties.unionid;

    goid="";

    if (zillaid!=0 && upazilaid ==0 && typeof unionid === 'undefined' )
        goid = zillaid;
    else if(zillaid!=0 && upazilaid !=0 && typeof unionid === 'undefined' )
        goid = zillaid + "_"+ upazilaid;
    else if(zillaid!=0 && upazilaid !=0 && unionid !=0 )
        goid = zillaid + "_"+ upazilaid+ "_"+ unionid;

    //console.log(zillaid);
    // console.log(upazilaid);
    //console.log(unionid);
    console.log ("current id : " + goid);

    return goid;
}

function load_map_data(zilla,upazila,union,type,processType,apiURL,appendData){
    var geoInfo = new Object();
    geoInfo.zilla = zilla;
    geoInfo.upazila = upazila;
    geoInfo.union = union;
    geoInfo.type = type;                 // Type 0 = All, 1 = ZIlla, 2 = Upazila, 3= union
    // geoInfo.methodType=methodType;      // 1= MNH, 2 = FP, 3 = GP,
    geoInfo.processType=processType;    // 1 = population, 2 = MNH, Family Planning, General Patient 3= Facility Assessment
    geoInfo.year=$("#current-year").val()!=null?$("#current-year").val():year;
    geoInfo.month=$("#current-month").val()!=null?$("#current-month").val():month;

    if(apiURL==1)
        data_url = "mapInfo";
    else if (apiURL==2)
        data_url = "dashboardData";

    $.ajax(
        data_url,
        {
            data: {"geoInfo":JSON.stringify(geoInfo)},
            dataType: "json",
            type : "POST",
            'async': true,
            // 'global': false,
            beforeSend: function() {
                preloader(1); //Show loading
            },
            success: function (data){
                //setTimeout(function() {
                //console.log(data);
                if (processType == 1) {
                    population = data;
                    //console.log(population);
                    //Add total of Population to population Json in bd index
                    calculateTotalPopulation();
                } else if (processType == 2) {
                    if(appendData == 1){

                        //var mnh_data;
                        console.log ("Append Data");
                        console.log(mnh_data);
                        console.log(family_planning);
                        console.log(general_patient);

                        for (var key in data["MNH"]){
                            if(isNaN(mnh_data[key]))
                                mnh_data[key]= data["MNH"][key];

                            if(isNaN(family_planning[key]))
                                family_planning[key] = data["FP"][key];

                            if(isNaN(general_patient[key]))
                                general_patient[key] = data["GP"][key];
                        }

                    }

                    else{
                        mnh_data = data["MNH"];
                        family_planning = data["FP"];
                        general_patient = data["GP"];
                    }

                }
                else if (processType == 3) { //Processing Facility Assessment Data
                    facility_assessment = data;
                }
                //});
            },
            complete: function() {
                if(processType == 2 && appendData == 0){

                    //Adding total MNH value in bd index
                    calculateTotalMNH();
                    calculateTotalGP();
                    calculateTotalFamilyPlanning();

                    //Load First screen value total of all panels
                    loadToatalData("bd");
                }

                preloader(0); //Hide loading
            },
            error:function(e){
                alert(e);
            }
        }
    );
}
function cleanPopulation() {
    $("#pop-male").html("");
    $("#pop-female").html("");
    $("#pop-elco").html("");
    $("#pop-total").html("");
}
function update_population_widget(id){
    if (typeof population[id] === 'undefined')
    {
        clearChart('pop-chart')
        cleanPopulation();
        return false;
    }

    //console.log ("Population ");
    //console.log(population[id]);

    male = parseFloat(population[id].Male).toFixed(0);
    female = parseFloat(population[id].Female).toFixed(0);

    male = isNaN(male)?"":male;
    female = isNaN(female)?"":female;
    total= Number(male)+ Number(female);
    elco = Math.round(total*(26.7/100));

    //Load PIE Chart
    data={
        labels: ["Male", "Female"],
        datasets: [{
            label: "Population (Millions)",
            backgroundColor: ["rgb(54, 162, 235)", "rgb(255, 165, 0)"],
            data: [male,female]
        }]
    };

    if(male !=0 || female !=0 )
        getChart('pop-chart',data,'pie','Total:'+total,'right',16,true);



    $("#pop-male").html(Number(male).toLocaleString());
    $("#pop-female").html(Number(female).toLocaleString());
    $("#pop-elco").html(Number(elco).toLocaleString());
    $("#pop-total").html(Number(total).toLocaleString());
    $("#cards-population").html(Number(total).toLocaleString());
}

function commaFormattedNumber(num){
    return num;
    return num==0?"":num.toLocaleString();
}

var geojsonFeature =
    [
        {
            "type": "Feature",
            "properties": {
                "name": "Coors Field",
                "amenity": "Baseball Stadium",
                "popupContent": "This is where the Rockies play!"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [91.52052,24.66503]
            }
        },
        {
            "type": "Feature",
            "properties": {
                "name": "Coors Field",
                "amenity": "Baseball Stadium",
                "popupContent": "This is where the Rockies play!"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [91.1483442433061,24.0738398702368]
            }
        }

    ];

$(document).ready(function () {

    load_dashboard();

    //loadToatalData("bd");

});

function load_dashboard(){
    //Load Population Data
    load_map_data(0,0,0,0,1,1,0);

    //Load MNH, FP and GP data
    load_map_data(0,0,0,1,2,2,0);

    //Hide Facility Assessment as first
    $(".facility-assessment").hide();
}




function event_click(feature,layer) {
    cleanCards();
    loadDataToWidget(feature,layer);
    //tapLayer(feature,layer);
}

function event_mouseover(feature,layer) {
    //loadDataToWidget(feature,layer);
}

function tapLayer(feature,layer) {
    id = getGoID(feature);
    layer.bindPopup('<div style="border-bottom: solid 1px #DDD; padding: 3px; font-weight: bold; margin-bottom: 15px;">'+
        feature.properties.name+'</div>' +
        '<div style="max-height: 500px; overflow: scroll; padding: 10px;">'+
        show_population(id) +
        showMNH(id)+
        showFP(id)+
        showGP(id)+
        '</div>'
    );
}

//This function will load marker in map in upazila and union level in double click on map
function load_marker(zilla,upazila,union) {

    json = {"zilla":zilla,"upazila":upazila,"unions":union,"type":4,"processType":3,
        "year":$("#current-year").val()!=null?$("#current-year").val():year,"month":$("#current-month").val()!=null?$("#current-month").val():month};
    $.ajax(
        "mapInfo",
        {
            data: {"geoInfo": JSON.stringify(json)},
            dataType: "json",
            'async': true,

            type : "POST",
            beforeSend: function() {
                preloader(1); //Show loading
            },
            success: function (data){
                addMarker(data);
                //console.log(data);
                preloader(0); //hide loading
            },
            error:function(e){
                alert(e);
            }
        }
    );
}



function show_population(id) {
    if (typeof population[id] === 'undefined')
        return '';

    male = parseFloat(population[id].Male).toFixed(0);
    female = parseFloat(population[id].Female).toFixed(0);
    total = parseFloat(population[id].Total).toFixed(0 );

    male = isNaN(male)?"":male;
    female = isNaN(female)?"":female;
    total= Number(male)+ Number(female);
    elco = commaFormattedNumber(Math.round(total*(26.7/100)));

    pop= '<div class="panel panel-default">'+
        '<div class="panel-body">'+
        '<div class="popoup-panel-title">Population</div>' +
        '<table class="table table-bordered tbl-hover white_table tbl-hover" width="100%" >' +
        '<tr>' +
        '<td width="30%">Male</td>'+
        '<td width="15%" align="right">'+male+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td>Female</td>'+
        '<td align="right">'+female+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td ><b>Total</b></td>'+
        '<td align="right"><b>'+total+'</b></td>'+
        '</tr>'+
        '<tr>'+
        '<td>Elco</td>'+
        '<td align="right">'+elco+'</td>'+
        '</tr>'+
        '</table>'+
        '</div>'+
        '</div>';

    return pop;
}

function showFP(id) {
    if (typeof family_planning[id] === 'undefined')
        return '';

    fp=   '<div class="panel panel-default">'+
        '<div class="panel-body">'+
        '<div class="popoup-panel-title">Family Planning</div>'+
        '<table class="table table-bordered tbl-hover white_table tbl-hover" width="100%" border="0">'+
        ' <tr>'+
        '<td>Pill</td>'+
        '<td align="right">'+family_planning[id].Pill+'</td>'+
        '</tr>'+

        '<tr>'+
        '<td>Condom</td>'+
        '<td  align="right">'+family_planning[id].Condom+'</td>'+
        '</tr>'+

        '<tr>'+
        '<td>IUD</td>'+
        '<td align="right">'+family_planning[id].IUD+'</td>'+
        '</tr>'+

        '<tr>'+
        '<td>IUD Followup</td>'+
        '<td align="right">'+family_planning[id]["IUD Followup"]+'</td>'+
        '</tr>'+

        '<tr>'+
        '<td>Implant</td>'+
        '<td align="right">'+family_planning[id].IMPLANT+'</td>'+
        '</tr>'+

        '<tr>'+
        '<td>Implant Followup</td>'+
        '<td align="right">'+family_planning[id]["IMPLANT Followup"]+'</td>'+
        '</tr>'+

        '<tr>'+
        '<td>Injectable</td>'+
        '<td align="right">'+family_planning[id].Injectable+'</td>'+
        '</tr>'+
        '</table>'+
        '</div>'+
        '</div>';
    return fp;
}

function showGP(id) {
    if (typeof general_patient[id] === 'undefined')
        return '';

    male = general_patient[id].Male;
    female = general_patient[id].Female;
    total = Number(male) + Number(female);

    gp='<div class="panel panel-default">' +

        '<div class="panel-body">' +
        '<div class="popoup-panel-title">General Patient</div></hr>' +
        '<table class="table table-bordered tbl-hover white_table tbl-hover" width="100%" border="0">' +
        '<tr>' +
        '<td>Male</td>' +
        '<td align="right">'+male+'</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Female</td>' +
        '<td align="right">'+female+'</td>' +
        '</tr>' +
        '<tr>' +
        '<td><b>Total</b></td>' +
        '<td align="right"><b>'+total+'</b></td>' +
        '</tr>' +
        '</table>' +
        '</div>' +
        '</div>'
    return gp;
}

function showMNH(id) {
    if (typeof mnh_data[id] === 'undefined')
        return '';

    anc1= mnh_data[id].ANC1;
    anc2 = mnh_data[id].ANC2;
    anc3 = mnh_data[id].ANC3;
    anc4 = mnh_data[id].ANC4;

    pnc1m = mnh_data[id]["PNC1"];
    pnc2m = mnh_data[id]["PNC2"];
    pnc3m = mnh_data[id]["PNC3"];
    pnc4m = mnh_data[id]["PNC4"];
    console.log('sdsdsd'+pnc1m);

    pnc1n = mnh_data[id]["PNC1-N"];
    pnc2n = mnh_data[id]["PNC2-N"];
    pnc3n = mnh_data[id]["PNC3-N"];
    pnc4n = mnh_data[id]["PNC4-N"];

    delivery = mnh_data[id].Delivery;

    mnh='<div class="panel panel-default">' +
        '<div class="panel-body">' +
        '<div class="popoup-panel-title">MNH (Maternal & Newborn Health)</div>' +
        '<table width="100%" border="0" class="table table-bordered tbl-hover white_table tbl-hover">' +
        '<thead >' +
        '<tr>' +
        '<td width="30%"></td>' +
        '<td align="center" width="15%">Visit 1</td>' +
        '<td align="center" width="15%">Visit 2</td>' +
        '<td align="center" width="15%">Visit 3</td>' +
        '<td align="center" width="15%">Visit 4</td>' +
        '</tr>' +
        '</thead>' +

        '<tr>' +
        '<td>ANC</td>' +
        '<td id="mnhc_anc1" align="right" >'+anc1+'</td>' +
        '<td id="mnhc_anc2" align="right">'+anc2+'</td>' +
        '<td id="mnhc_anc3" align="right">'+anc3+'</td>' +
        '<td id="mnhc_anc4" align="right">'+anc4+'</td>' +
        '</tr>' +
        '<tr>' +
        '<td>PNC (M) </td>' +
        '<td id="mnhc_pnc1m" align="right">'+pnc1m+'</td>' +
        '<td id="mnhc_pnc2m" align="right">'+pnc2m+'</td>' +
        '<td id="mnhc_pnc3m" align="right">'+pnc3m+'</td>' +
        '<td id="mnhc_pnc4m" align="right">'+pnc4m+'</td>' +
        '</tr>' +
        '<tr>' +
        '<td>PNC (N)</td>' +
        '<td id="mnhc_pnc1n" align="right">'+pnc1n+'</td>' +
        '<td id="mnhc_pnc2n" align="right">'+pnc2n+'</td>' +
        '<td id="mnhc_pnc3n" align="right">'+pnc3n+'</td>' +
        '<td id="mnhc_pnc4n" align="right">'+pnc4n+'</td>' +
        '</tr>' +
        '</table>' +

        '<table class="table tbl-hover">' +
        '<tr width="30%">' +
        '<td>Delivery</td>' +
        '<td align="center" width="15%">'+delivery+'</td>' +
        '</tr>' +
        '</table>' +
        '</div>' +
        '</div>';

    return mnh;
}

function loadDataToWidget(feature,layer){
    goid = getGoID(feature);
    update_MNC_widget(goid);
    update_population_widget(goid);
    update_FP_widget(goid);
    update_GP_widget(goid);

    if(feature.properties.upazilaid == 0)
        propName = "District: " + "<span class=\"area-name\">"+feature.properties.name+"</span>";

    if(feature.properties.upazilaid != 0)
        propName = "Upazila: " + "<span class=\"area-name\">"+feature.properties.name+"</span>";

    //console.log(emisCoverage(feature.properties.zillaid));

    $("#area-name").html(propName);
}

function  update_FA_widget(catA,catB,catC){
    if (typeof facility_assessment === 'undefined')
    {
        clearChart("fa-canvas");
        //fp_clean();
        //return false;
    }

    //Load BAR Chart
    chart_data ={
        labels: ["Category A", "Category B", "Category C"],
        datasets: [{
            label: '',
            data: [catA,catB,catC],
            backgroundColor: [
                'rgba(235, 224, 255, 1)',
                'rgba(215, 236, 251, 1)',
                'rgba(255, 224, 230, 1)'

            ],
            borderColor: [
                'rgba(235, 224, 255, 1)',
                'rgba(215, 236, 251, 1)',
                'rgba(255, 224, 230, 1)'
            ],
            borderWidth: 1
        }]
    };
    getChart('fa-canvas',chart_data,'horizontalBar','','bottom',10,false);
}


function update_FP_widget(id){
    if (typeof family_planning[id] === 'undefined')
    {
        clearChart("fp-canvas");
        fp_clean();
        return false;
    }

    //Load BAR Chart
    chart_data ={
        labels: ["Condom", "Pill", "Injectable", "IUD","Implant"],
        datasets: [{
            label: '',
            data: [family_planning[id].Condom,family_planning[id].Pill,family_planning[id].Injectable,family_planning[id].IUD,family_planning[id].IMPLANT],
            backgroundColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    };
    getChart('fp-canvas',chart_data,'bar','','bottom',10,false);

    $("#fp_pill").html(family_planning[id].Pill);
    $("#fp_condom").html(family_planning[id].Condom);
    $("#fp_iud").html(family_planning[id].IUD);
    $("#fp_iud_followup").html(family_planning[id]["IUD Followup"]);
    $("#fp_implant").html(family_planning[id].IMPLANT);
    $("#fp_implant_followup").html(family_planning[id]["IMPLANT Followup"]);
    $("#fp_injectable").html(family_planning[id].Injectable);

}

function fp_clean() {
    $("#fp_pill").html("");
    $("#fp_condom").html("");
    $("#fp_iud").html("");
    $("#fp_iud_followup").html("");
    $("#fp_implant").html("");
    $("#fp_implant_followup").html("");
    $("#fp_injectable").html("");
}

function update_MNC_widget(id){
    if (typeof mnh_data[id] === 'undefined')
    {
        cleanMNH();
        clearChart("mnh-canvas");
        return false;
    }
    //Load BAR Chart
    anc_data_set ={
        label: "ANC",
        data: [mnh_data[id].ANC1, mnh_data[id].ANC2, mnh_data[id].ANC3, mnh_data[id].ANC4],
        lineTension: 1,
        borderColor: 'rgba(255, 99, 132,1)',
        backgroundColor:'rgba(255, 99, 132,1)',
        fill: false,
        pointBorderColor: '#ff6384',
    };

    pncM_data_set ={
        label: "PNC Mother",
        data: [mnh_data[id]["PNC1"], mnh_data[id]["PNC2"], mnh_data[id]["PNC3"], mnh_data[id]["PNC4"]],
        lineTension: 1,
        borderColor: 'rgba(54, 162, 235,1)',
        backgroundColor: 'rgba(54, 162, 235,1)',
        fill: false,
        pointBorderColor: '#36a2eb',

    };

    pncN_data_set ={
        label: "PNC Newborn",
        data: [mnh_data[id]["PNC1-N"], mnh_data[id]["PNC2-N"], mnh_data[id]["PNC3-N"], mnh_data[id]["PNC4-N"]],
        lineTension: 1,
        borderColor: 'rgba(255, 165, 0,1)',
        backgroundColor: 'rgba(255, 165, 0,1)',
        fill: false,
        pointBorderColor: 'orange',

    };

    mnh_chart_data = {
        labels: ["Visit 1", "Visit 2", "Visit 3", "Visit 4"],
        datasets: [anc_data_set,pncM_data_set,pncN_data_set],

    }
    getChart('mnh-canvas',mnh_chart_data,'bar','MNH','bottom',10,true);

    $("#mnhc_anc1").html(mnh_data[id].ANC1);
    $("#mnhc_anc2").html(mnh_data[id].ANC2);
    $("#mnhc_anc3").html(mnh_data[id].ANC3);
    $("#mnhc_anc4").html(mnh_data[id].ANC4);

    $("#mnhc_pnc1m").html(mnh_data[id]["PNC1"]);
    $("#mnhc_pnc2m").html(mnh_data[id]["PNC2"]);
    $("#mnhc_pnc3m").html(mnh_data[id]["PNC3"]);
    $("#mnhc_pnc4m").html(mnh_data[id]["PNC4"]);

    $("#mnhc_pnc1n").html(mnh_data[id]["PNC1-N"]);
    $("#mnhc_pnc2n").html(mnh_data[id]["PNC2-N"]);
    $("#mnhc_pnc3n").html(mnh_data[id]["PNC3-N"]);
    $("#mnhc_pnc4n").html(mnh_data[id]["PNC4-N"]);

    $("#mnhc_delivery").html(mnh_data[id].Delivery);

    $("#cards-delivery").html(mnh_data[id].Delivery);

}

function cleanMNH(){
    $("#mnhc_anc1").html("");
    $("#mnhc_anc2").html("");
    $("#mnhc_anc3").html("");
    $("#mnhc_anc4").html("");

    $("#mnhc_pnc1m").html("");
    $("#mnhc_pnc2m").html("");
    $("#mnhc_pnc3m").html("");
    $("#mnhc_pnc4m").html("");

    $("#mnhc_pnc1n").html("");
    $("#mnhc_pnc2n").html("");
    $("#mnhc_pnc3n").html("");
    $("#mnhc_pnc4n").html("");
    $("#mnhc_delivery").html("");
}

function cleanCards() {
    $("#cards-population").html(commaFormattedNumber(""));
    $("#cards-delivery").html("");

}


function update_GP_widget(id){
    if (typeof general_patient[id] === 'undefined')
    {
        cleanGP();
        return false;
    }

    male = general_patient[id].Male;
    female = general_patient[id].Female;
    total = Number(male) + Number(female);

    $("#gp-male").html(general_patient[id].Male);
    $("#gp-female").html(general_patient[id].Female);
    $("#gp-total").html(total);

}

function cleanGP() {
    $("#gp-male").html('');
    $("#gp-female").html('');
    $("#gp-total").html('');
}

function emisCoverage(zillaid) {
    if(zillaid == 0){
        total = 0;
        $.each(emisCoverageJson,function (index, val) {
            total += val.length;
        });
        return total;
    }
    else{
        if(emisCoverageJson[zillaid] === undefined)
        {
            return 0;
        }
        else
            return emisCoverageJson[zillaid].length;
    }
}
function eMISwidgetUpdate(){

    html = '<table width="100%" border="0" class="table table-bordered tbl-hover white_table tbl-hover">'+
        '<tr>'+
        '<td width="70%">Zilla</td>'+
        '<td align="right">20</td>' +
        '</tr>'+

        '<tr>'+
        '<td>Upazila</td>'+
        '<td align="right">'+emisCoverage(0)+'</td>' +
        '</tr>' +
        '</table>';

    $("#emis-coverage").html(html);

}

eMISwidgetUpdate();

function event_mouseover_outside_mamoni(feature,layer) {
    //alert('');

    //console.log('cl');
    //cleanMNH();
    //cleanPopulation();
    //fp_clean();
    //cleanGP();
}

//This function will remove previous chart and create new chart canvas
function clearChart(container) {
    $('#'+container).remove();
    $('#'+container+'-container').append('<canvas id="'+container+'"><canvas>');
}

function getChart(container,dataset,type,title,legend_pos,fontsize,legendShowHide) {

    clearChart(container);

    new Chart(document.getElementById(container), {
        type: type,
        data: dataset,
        options: {
            responsive: true,
            maintainAspectRatio: true,
            title: {
                display: false,
                text: title
            },
            legend: {
                position: legend_pos,
                display:legendShowHide,
                labels: {
                    fontColor: "black",
                    fontSize: fontsize,
                    boxWidth: 20,
                    padding: 10
                }
            }
        }
    });

}

//This function will fire in double click on any map layer
function event_double_click(feature, layer) {
    zillaid = feature.properties.zillaid;
    upazilaid = feature.properties.upazilaid;


    if(upazilaid == 0)
    {
        methodType=2;
    }
    else
    {
        methodType=3;
    }


    //console.log(feature.properties);

    //Load MNH, FP and GP data
    load_map_data(zillaid,upazilaid,0,methodType,2, 2,1);

    //Load Facility Assessment Data
    //load_map_data(zillaid,upazilaid,0,4,3, 1,0);
    load_marker(zillaid,upazilaid,0);

    //Show Facility Assessment Panel
    showHideFacilityAssessment(1);
}

function clear_all() {
    cleanPopulation();
    cleanMNH();
    cleanGP();
    fp_clean();
    $("#mnh-canvas").html("");
    $("#pop-chart").html("");
}

//This function will call while clicking on breadcrumb text
function event_breadCrumb_click(zillaid, upazilaid,unionid) {
    if(zillaid == 0){
        loadToatalData("bd");

        //hide Facility Assessment
        showHideFacilityAssessment(0);
    }

    removeMarker();
    load_marker(zillaid,upazilaid,unionid)


}

function showHideFacilityAssessment(status){
    if(status == 1)
        $(".facility-assessment").show('slow');
    else
        $(".facility-assessment").hide('slow');

}

function calculateTotalPopulation() {
    male = 0;
    female = 0;
    total = 0;
    elco = 0;

    $.each(population,function (key, val) {
        if(key.length<=2)
        {
            male = Number(male) + Number(val.Male);
            female =  Number(female) + Number(val.Female);
            total =  Number(total) + Number(val.Total);
        }
    });
    //console.log (typeof population);
    population["bd"] =  {"Male":male, "Female": female, "Total":total}
}

function calculateTotalMNH() {

    // console.log(console.log(mnh_data));
    anc1=0;
    anc2=0;
    anc3=0;
    anc4=0;
    delivery=0;
    pnc1_m=0;
    pnc2_m=0;
    pnc3_m=0;
    pnc4_m=0;
    pnc1_n = 0;
    pnc2_n = 0;
    pnc3_n = 0;
    pnc4_n = 0;

    $.each(mnh_data,function (key, val) {
        if(key.length<=2)
        {
            anc1 = Number(anc1) + Number(val.ANC1);
            anc2 = Number(anc2) + Number(val.ANC2);
            anc3 = Number(anc3) + Number(val.ANC3);
            anc4 = Number(anc4) + Number(val.ANC4);

            delivery = Number(delivery) + Number(val.Delivery);

            pnc1_m = Number(pnc1_m) + Number(val["PNC1"]);
            pnc2_m = Number(pnc2_m) + Number(val["PNC2"]);
            pnc3_m = Number(pnc3_m) + Number(val["PNC3"]);
            pnc4_m = Number(pnc4_m) + Number(val["PNC4"]);

            pnc1_n = Number(pnc1_n) + Number(val["PNC1-N"]);
            pnc2_n = Number(pnc2_n) + Number(val["PNC1-N"]);
            pnc3_n = Number(pnc3_n) + Number(val["PNC1-N"]);
            pnc4_n = Number(pnc4_n) + Number(val["PNC1-N"]);
        }
    });

    mnh ={
        "ANC1": anc1,
        "ANC2": anc2,
        "ANC3": anc3,
        "ANC4": anc4,
        "Delivery": delivery,
        "PNC1": pnc1_m,
        "PNC1-N": pnc1_n,
        "PNC2": pnc2_m,
        "PNC2-N": pnc2_n,
        "PNC3": pnc3_m,
        "PNC3-N": pnc3_n,
        "PNC4": pnc4_m,
        "PNC4-N": pnc4_n
    };

    mnh_data["bd"] = mnh;

    //console.log((mnh_data));
}

function calculateTotalGP() {
    male = 0;
    female = 0;
    total = 0;

    $.each(general_patient,function (key, val) {
        if(key.length<=2)
        {
            male = Number(male) + Number(val.Male);
            female =  Number(female) + Number(val.Female);
            total =  Number(total) + Number(val.Total);
        }
    });

    general_patient["bd"] = {"Male":male, "Female": female, "Total":total}
    //console.log(general_patient);
}

function loadToatalData(id) {
    update_MNC_widget(id);
    update_population_widget(id);
    update_GP_widget(id);
    update_FP_widget("bd");

}

function calculateTotalFamilyPlanning() {
    //console.log(family_planning);
    pill = 0;
    condom = 0;
    iud = 0;
    iud_followup = 0;
    implant = 0;
    implant_followup =0;
    injectable = 0;

    $.each(family_planning,function (key, val) {
        if(key.length<=2)
        {
            implant = Number(implant) + Number(val.IMPLANT);
            implant_followup = Number(implant_followup) + Number(val["IMPLANT Followup"]);
            condom =  Number(condom) + Number(val.Condom);
            iud =  Number(iud) + Number(val.IUD);
            iud_followup =  Number(iud_followup) + Number(val["IUD Followup"]);
            injectable =  Number(injectable) + Number(val.Injectable);
            pill =  Number(pill) + Number(val.Pill);

        }
    });
    family_planning["bd"] =  {"IMPLANT":implant, "Condom": condom, "IUD":iud,"Injectable":injectable,"Pill":pill,"IUD Followup":iud_followup,"IMPLANT Followup":implant_followup};

}

function removeMarker() {
    //Remove previous marker
    if(map.hasLayer(markerGroup))
        map.removeLayer(markerGroup);
}

//THis function will add marker in map
function addMarker(data){
    removeMarker();

    var catA = 0;
    var catB = 0;
    var catC = 0;
    var total =0;
    markerGroup = L.layerGroup().addTo(map);

    $.each(data['Facility_info'],function (index, obj) {
        iconurl = "";

        if (typeof obj.facility_category === 'undefined')
            iconurl = 'image/dashboard/icons/catu.png';

        total++;


        if(obj.facility_category == 'A'){
            iconurl = 'image/dashboard/icons/cata.png';
            catA++;
        }

        if(obj.facility_category == 'B'){
            iconurl = 'image/dashboard/icons/catb.png';
            catB++
        }

        if(obj.facility_category == 'C'){
            iconurl = 'image/dashboard/icons/catc.png';
            catC++;
        }


        var icon = L.icon({
            iconUrl: iconurl,
            //shadowUrl: 'leaf-shadow.png',

            iconSize:     [20, 25], // size of the icon
            shadowSize:   [23, 23], // size of the shadow
            //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
            //shadowAnchor: [4, 62],  // the same for the shadow
            //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });


        if(parseFloat(obj.lat) != 0 && parseFloat(obj.lon) !=0 && !isNaN(obj.lat) && !isNaN(obj.lat)){

            marker = L.marker([parseFloat(obj.lat),parseFloat(obj.lon)], {icon: icon});

            var popup = marker.bindPopup('<b>'+obj.Facility_Name+'</b><br />' +
                '<table class="table">'+
                '<tr><td>Category</td><td>' + obj.facility_category+ ' (Last Update: '+obj.last_update_date+')</td></tr>'+
                '<tr><td>Type</td><td>' + obj.facility_type + '</td></tr>'+
                '<tr><td><img src="'+iconurl+'" width="15px" /></td><td>' + obj.lat + "," + obj.lon + '</td></tr>'+
                '</table><br />'+

                '<a href="https://maps.google.com/?q='+obj.lat+','+obj.lon+'" target="_blank" title="'+obj.Facility_Name+'">Show in Google Map</a>');

            markerGroup.addLayer(marker);
        }

    });

    var overlay = {'Facilities': markerGroup};
    L.layerGroup(markerGroup).addTo(map);

    //console.log("Total"+ total)
    update_FA_widget(catA,catB, catC);

    $("#cat-a").html(catA);
    $("#cat-b").html(catB);
    $("#cat-c").html(catC);
    $("#cat-total").html(total);

}