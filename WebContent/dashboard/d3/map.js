/**
 * Created by masum.billah on 10/18/2017.
 */

//$("body").disableSelection();


function load_dis(){
    $(".map_heading").text("Map of Bangladesh");
    hideTooltip();
    $(".zilla_legend").show();
    $(".union_legend").hide();
    //Map dimensions (in pixels)
    w = 426;
    var width = w,
        height = 600;//w*1.41;

    //Map projection
    var projection = d3.geo.mercator()
        .scale(23262.83313561068)
        .center([90.56198764383417,23.348253082500126]) //projection center
        .translate([width/2,height/2]) //translate to center the map in view

    //Generate paths based on projection
    var path = d3.geo.path()
        .projection(projection);

    //Create an SVG
    var svg = d3.select("map").append("svg")
        .attr("width", width)
        .attr("height", height);

    //Group for the map features
    var features = svg.append("g")
        .attr("class","features");


    d3.json("District_Map.geojson",function(error,geodata) {
        if (error) return console.log(error); 

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

            var tapped=false
            //Create a path for each map feature in the data
            features.selectAll("path")
                .data(geodata.features)
                .enter()
                .append("path")
                .attr("d",path)
                .on("mouseover",showTooltip)
                .on("click",showFixedTooltip)
                .style("fill", function(d){
                    if(d.properties.DIVID==10)
                        var color="#f39dc2";
                    else if(d.properties.DIVID==20)
                        var color="#c56340";
                    else if(d.properties.DIVID==30)
                        var color="#fcdba8";
                    else if(d.properties.DIVID==40)
                        var color="#81c242";
                    else if(d.properties.DIVID==50)
                        var color="#a085b2";
                    else if(d.properties.DIVID==55)
                        var color="#e1cf3d";
                    else if(d.properties.DIVID==60)
                        var color="#fef58e";
                    else if(d.properties.DIVID==99)
                        var color="#00daff";
                    else if(d.properties.DIVID==30)
                            var color="#FF0000";
                    

                    return color;
                })
                .on("mousemove",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#d38fad";
                        else if(d.properties.DIVID==20)
                            var color="#b56346";
                        else if(d.properties.DIVID==30)
                            var color="#d8bf97";
                        else if(d.properties.DIVID==40)
                            var color="#78a84a";
                        else if(d.properties.DIVID==50)
                            var color="#856e93";
                        else if(d.properties.DIVID==55)
                            var color="#d6cf7e";
                        else if(d.properties.DIVID==60)
                            var color="#c1b449";
                        else if(d.properties.DIVID==99)
                            var color="#16b3ce";
                        else if(d.properties.DIVID==30)
                            var color="#FF0000";

                        return color;
                    });
                })
                .on("mouseout",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#f39dc2";
                        else if(d.properties.DIVID==20)
                            var color="#c56340";
                        else if(d.properties.DIVID==30)
                            var color="#fcdba8";
                        else if(d.properties.DIVID==40)
                            var color="#81c242";
                        else if(d.properties.DIVID==50)
                            var color="#a085b2";
                        else if(d.properties.DIVID==55)
                            var color="#e1cf3d";
                        else if(d.properties.DIVID==60)
                            var color="#fef58e";
                        else if(d.properties.DIVID==99)
                            var color="#00daff";

                        return color;
                    });
                    hideTooltip();
                })
                .on("touchstart", function(d){
                    if(!tapped){
                        tapped=setTimeout(function(){
                            tapped=null
                        },300); //wait 300ms
                    } else {
                        clearTimeout(tapped);
                        tapped=null
                        clicked(d);
                    }
                });
        }
        else{
            //Create a path for each map feature in the data
            features.selectAll("path")
                .data(geodata.features)
                .enter()
                .append("path")
                .attr("d",path)
                .on("mouseover",showTooltip)
                .on("click",showFixedTooltip)
                .style("fill", function(d){
                    if(d.properties.DIVID==10)
                        var color="#f39dc2";
                    else if(d.properties.DIVID==20)
                        var color="#c56340";
                    else if(d.properties.DIVID==30)
                        var color="#fcdba8";
                    else if(d.properties.DIVID==40)
                        var color="#81c242";
                    else if(d.properties.DIVID==50)
                        var color="#a085b2";
                    else if(d.properties.DIVID==55)
                        var color="#e1cf3d";
                    else if(d.properties.DIVID==60)
                        var color="#fef58e";
                    else if(d.properties.DIVID==99)
                        var color="#00daff";
                    else if(d.properties.DIVID==30)
                            var color="#FF0000";

                    return color;
                })
                .on("mousemove",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#d38fad";
                        else if(d.properties.DIVID==20)
                            var color="#b56346";
                        else if(d.properties.DIVID==30)
                            var color="#d8bf97";
                        else if(d.properties.DIVID==40)
                            var color="#78a84a";
                        else if(d.properties.DIVID==50)
                            var color="#856e93";
                        else if(d.properties.DIVID==55)
                            var color="#d6cf7e";
                        else if(d.properties.DIVID==60)
                            var color="#c1b449";
                        else if(d.properties.DIVID==99)
                            var color="#16b3ce";
                        else if(d.properties.DIVID==30)
                            var color="#FF0000";

                        return color;
                    });
                })
                .on("mouseout",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#f39dc2";
                        else if(d.properties.DIVID==20)
                            var color="#c56340";
                        else if(d.properties.DIVID==30)
                            var color="#fcdba8";
                        else if(d.properties.DIVID==40)
                            var color="#81c242";
                        else if(d.properties.DIVID==50)
                            var color="#a085b2";
                        else if(d.properties.DIVID==55)
                            var color="#e1cf3d";
                        else if(d.properties.DIVID==60)
                            var color="#fef58e";
                        else if(d.properties.DIVID==99)
                            var color="#00daff";
                        else if(d.properties.DISTNAME=="Dhaka")
                            var color="#ff0000";
                        

                        return color;
                    });
                    hideTooltip();
                })
                //.on("dblclick",clicked);
        }


        features.selectAll("text")
            .data(geodata.features)
            .enter()
            .append("svg:text")
            .text(function(d){
                if(d.properties.OBJECTID=="6" || d.properties.OBJECTID=="15" || d.properties.OBJECTID=="26" || d.properties.OBJECTID=="47" || d.properties.OBJECTID=="61" || d.properties.OBJECTID=="81" || d.properties.OBJECTID=="85" || d.properties.OBJECTID=="91")
                    return d.properties.DISTNAME;
            })
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            })
            .attr("text-anchor","middle")
            .attr('font-size','15pt')
            .attr('font-weight','bold');

        features.selectAll(".mark")
            .data(geodata.features)
            .enter()
            .append("image")
            .attr('class','mark')
            .attr('width', 20)
            .attr('height', 20)
            //.attr("xlink:href",function(d){
              //  if(d.properties.OBJECTID=="36" || d.properties.OBJECTID=="42" || d.properties.OBJECTID=="51" || d.properties.OBJECTID=="75" || d.properties.OBJECTID=="93")
                //    return 'image/Implemented_Area.gif';
            //})
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            });

    });
}

var type_name = "dis";
var dist_id;
var dist_name;
var upz_id;
var upz_name;
load_dis();

//Create a tooltip, hidden at the start
var tooltip = d3.select("map").append("div").attr("class","tooltip");

// Add optional onClick events for features here
// d.properties contains the attributes (e.g. d.properties.name, d.properties.population)
function clicked(d) {
    if(d.properties.OBJECTID==36 || d.properties.OBJECTID==42 || d.properties.OBJECTID==51 || d.properties.OBJECTID==75 || d.properties.OBJECTID==93){
        $("svg").remove();
        type_name = "upz";
        dist_id = d.properties.OBJECTID;
        dist_name = d.properties.DISTNAME;
        load_upz(d.properties.DISTNAME, d.properties.OBJECTID);
    }
}

function clicked_upz(d) {
    if(dist_id==36 || dist_id==42 || dist_id==51 || dist_id==75 || dist_id==93){
        $("svg").remove();
        type_name = "union";
        upz_id = d.properties.MEAN_UZCOD;
        upz_name = d.properties.UPAZILA;
        load_union(d.properties.UPAZILA, d.properties.MEAN_DISTC, d.properties.MEAN_UZCOD);
    }
}

function get_data_zilla(key,zilla_name){
    var info = zilla_name;

    return info;
}


//Position of the tooltip relative to the cursor
var tooltipOffset = {x: 5, y: -25};

//Create a tooltip, hidden at the start
function showTooltip(d) {
    //moveTooltip();
    //alert(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD));
    if(type_name=="dis"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(d.properties.OBJECTID,d.properties.DISTNAME));

    }

    $(".chart_view").show();
}

//Create a tooltip, hidden at the start
function showFixedTooltip(d) {
    //moveTooltip();
    //alert(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD));
    if(type_name=="dis"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(d.properties.OBJECTID,d.properties.DISTNAME));

    }

    $(".chart_view").show();
}

//Move the tooltip to track the mouse
function moveTooltip() {
    $(".tooltip_view").style("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .style("left",(d3.event.pageX+tooltipOffset.x)+"px");

    $(".chart_view").show();
}

//Create a tooltip, hidden at the start
function hideTooltip(d) {
    $(".tooltip_view").hide();//style("display","none");
    if(type_name=="upz"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(dist_id, dist_name));
    }
    else if(type_name=="union"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_upz(dist_id + "_" + upz_id,upz_name));
    }
    $(".chart_view").hide();
}

function moveTooltip() {
    $(".tooltip_view").style("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .style("left",(d3.event.pageX+tooltipOffset.x)+"px");

    $(".chart_view").show();
}

function showTooltipFacility(d) {
    moveTooltipFacility();
    $(".mouse_hover").show()
        .text(d.Facility_Name);
}

function moveTooltipFacility() {
    $(".mouse_hover").css("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .css("left",(d3.event.pageX+tooltipOffset.x)+"px");
}

function hideTooltipFacility() {
    $(".mouse_hover").hide();
}
