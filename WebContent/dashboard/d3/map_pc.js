/**
 * Created by masum.billah on 10/18/2017.
 */

//$("body").disableSelection();


function load_dis(){
    $(".map_heading").text("Map of Bangladesh");
    hideTooltip();
    $(".zilla_legend").show();
    $(".union_legend").hide();
    //Map dimensions (in pixels)
	
	var map_color ='#a9aeb1';
	var map_hover ='#C0D965';
	var map_background_color = '#666';
	
    w = 426;
    var width = w,
        height = 600;//w*1.41;

    //Map projection
    var projection = d3.geo.mercator()
        .scale(123262.83313561068)
        .center([90.56198764383417,23.348253082500126]) //projection center
        .translate([width/2,height/2]) //translate to center the map in view

    //Generate paths based on projection
    var path = d3.geo.path()
		
        .projection(projection);

    //Create an SVG
    var svg = d3.select("map").append("svg")
        .attr("width", width)
        .attr("height", height);

    //Group for the map features
    var features = svg.append("g")
        .attr("class","features")
		;

    d3.json("d3/District_Map.geojson",function(error,geodata) {
        if (error) return console.log(error); 

        
        
            //Create a path for each map feature in the data
            features.selectAll("path")
                .data(geodata.features)
                .enter()
                .append("path")
                .attr("d",path)
                .on("mouseover",showTooltip)
                .on("click",showFixedTooltip)
				.style("stroke-width", 5)
                .style("fill", function(d){

					
					if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93 || d.properties.OBJECTID==51 || d.properties.OBJECTID==75 || d.properties.OBJECTID==42
					|| d.properties.OBJECTID==30 || d.properties.OBJECTID==56 || d.properties.OBJECTID==29 || d.properties.OBJECTID==13 || d.properties.OBJECTID==12)
                        return map_hover;
						
					return map_color;//set_fill_color(d.properties.OBJECTID);
                })
                .on("mousemove",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#d38fad";
                        else if(d.properties.DIVID==20)
                            var color="#b56346";
                        else if(d.properties.DIVID==30)
                            var color="#d8bf97";
                        else if(d.properties.DIVID==40)
                            var color="#78a84a";
                        else if(d.properties.DIVID==50)
                            var color="#856e93";
                        else if(d.properties.DIVID==55)
                            var color="#d6cf7e";
                        else if(d.properties.DIVID==60)
                            var color="#c1b449";
                        else if(d.properties.DIVID==99)
                            var color="#16b3ce";
                        else if(d.properties.DIVID==30)
                            var color="#FF0000";
						else if(d.properties.OBJECTID==91)
                            var color="#FF0000";

						if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93 || d.properties.OBJECTID==51 || d.properties.OBJECTID==75 || d.properties.OBJECTID==42
					|| d.properties.OBJECTID==30 || d.properties.OBJECTID==56 || d.properties.OBJECTID==29 || d.properties.OBJECTID==13 || d.properties.OBJECTID==12)
                        return '#4BB378';
						
						return map_color;

                    });
                })
                .on("mouseout",function(){
                    d3.select(this).style("fill", function(d){
                        if(d.properties.DIVID==10)
                            var color="#f39dc2";
                        else if(d.properties.DIVID==20)
                            var color="#c56340";
                        else if(d.properties.DIVID==30)
                            var color="#fcdba8";
                        else if(d.properties.DIVID==40)
                            var color="#81c242";
                        else if(d.properties.DIVID==50)
                            var color="#a085b2";
                        else if(d.properties.DIVID==55)
                            var color="#e1cf3d";
                        else if(d.properties.DIVID==60)
                            var color="#fef58e";
                        else if(d.properties.DIVID==99)
                            var color="#00daff";
                        else if(d.properties.DISTNAME=="Dhaka")
                            var color="#ff0000";
                        
						
						if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93 || d.properties.OBJECTID==51 || d.properties.OBJECTID==75 || d.properties.OBJECTID==42
					|| d.properties.OBJECTID==30 || d.properties.OBJECTID==56 || d.properties.OBJECTID==29 || d.properties.OBJECTID==13 || d.properties.OBJECTID==12)
                        return map_hover;

                        return map_color;
                    });
                    hideTooltip();
                })
                .on("click",function(d){
					bread_crumbs_zilla(d);
				});
        


        features.selectAll("text")
            .data(geodata.features)
            .enter()
            .append("svg:text")
            .text(function(d){
				if(d.properties.OBJECTID==36 || d.properties.OBJECTID==93 || d.properties.OBJECTID==51 || d.properties.OBJECTID==75 || d.properties.OBJECTID==42
					|| d.properties.OBJECTID==30 || d.properties.OBJECTID==56 || d.properties.OBJECTID==29 || d.properties.OBJECTID==13 || d.properties.OBJECTID==12)
                    return d.properties.DISTNAME;
            })
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            })
            .attr("text-anchor","middle")
            .attr('font-size','8pt')
            .attr('font-weight','bold')
			.style('fill', '#000')
			.style('cursor', 'pointer')
			.on("click",function(d){
					bread_crumbs_zilla(d);
				});
			;

        features.selectAll(".mark")
            .data(geodata.features)
            .enter()
            .append("image")
            .attr('class','mark')
            .attr('width', 20)
            .attr('height', 20)
            //.attr("xlink:href",function(d){
              //  if(d.properties.OBJECTID=="36" || d.properties.OBJECTID=="42" || d.properties.OBJECTID=="51" || d.properties.OBJECTID=="75" || d.properties.OBJECTID=="93")
                //    return 'image/Implemented_Area.gif';
            //})
            .attr("x", function(d){
                return path.centroid(d)[0];
            })
            .attr("y", function(d){
                return  path.centroid(d)[1];
            });

    });
}

var type_name = "dis";
var dist_id;
var dist_name;
var upz_id;
var upz_name;
load_dis();

//Create a tooltip, hidden at the start
var tooltip = d3.select("map").append("div").attr("class","tooltip");

// Add optional onClick events for features here
// d.properties contains the attributes (e.g. d.properties.name, d.properties.population)
function clicked(d) {
    if(d.properties.OBJECTID==36 || d.properties.OBJECTID==42 || d.properties.OBJECTID==51 || d.properties.OBJECTID==75 || d.properties.OBJECTID==93){
        $("svg").remove();
        type_name = "upz";
        dist_id = d.properties.OBJECTID;
        dist_name = d.properties.DISTNAME;
        load_upz(d.properties.DISTNAME, d.properties.OBJECTID);
    }
}

function clicked_upz(d) {
    if(dist_id==36 || dist_id==42 || dist_id==51 || dist_id==75 || dist_id==93){
        $("svg").remove();
        type_name = "union";
        upz_id = d.properties.MEAN_UZCOD;
        upz_name = d.properties.UPAZILA;
        load_union(d.properties.UPAZILA, d.properties.MEAN_DISTC, d.properties.MEAN_UZCOD);
    }
}

function get_data_zilla(key,zilla_name){
    //var info = zilla_name;
	var info = key + zilla_name;
    return info;
}


//Position of the tooltip relative to the cursor
var tooltipOffset = {x: 5, y: -25};

//Create a tooltip, hidden at the start
function showTooltip(d) {
    //moveTooltip();
    //alert(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD));
    if(type_name=="dis"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(d.properties.OBJECTID,d.properties.DISTNAME));

    }

    $(".chart_view").show();
}

//Create a tooltip, hidden at the start
function showFixedTooltip(d) {
    //moveTooltip();
    //alert(get_data_upz(d.properties.MEAN_DISTC + "_" +d.properties.MEAN_UZCOD));
    if(type_name=="dis"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(d.properties.OBJECTID,d.properties.DISTNAME));

    }

    $(".chart_view").show();
}

//Move the tooltip to track the mouse
function moveTooltip() {
    $(".tooltip_view").style("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .style("left",(d3.event.pageX+tooltipOffset.x)+"px");

    $(".chart_view").show();
}

//Create a tooltip, hidden at the start
function hideTooltip(d) {
    $(".tooltip_view").hide();//style("display","none");
    if(type_name=="upz"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_zilla(dist_id, dist_name));
    }
    else if(type_name=="union"){
        $(".tooltip_view").show()//style("display","block")
            .html(get_data_upz(dist_id + "_" + upz_id,upz_name));
    }
    $(".chart_view").hide();
}

function moveTooltip() {
    $(".tooltip_view").style("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .style("left",(d3.event.pageX+tooltipOffset.x)+"px");

    $(".chart_view").show();
}

function showTooltipFacility(d) {
    moveTooltipFacility();
    $(".mouse_hover").show()
        .text(d.Facility_Name);
}

function moveTooltipFacility() {
    $(".mouse_hover").css("top",(d3.event.pageY+tooltipOffset.y)+"px")
        .css("left",(d3.event.pageX+tooltipOffset.x)+"px");
}

function hideTooltipFacility() {
    $(".mouse_hover").hide();
}

function set_fill_color(id){
	return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
}


function bread_crumbs_zilla(d){
	$(".dist").remove();
	$("#breadcrumb").append('<li class="dist"><a href="#">'+d.properties.DISTNAME+'</a></li>');
}

function bread_crumbs_upazila(){
	
}