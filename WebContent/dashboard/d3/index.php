<!DOCTYPE html>
<html>
  <head>
    <title>GIS Map Bangladesh</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">	
	<script type="text/javascript" src="jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="d3.v3.min.js"></script>
    <link href="style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <map>
		<script src="d3/map_pc.js"></script>
	</map>
	<div class="tooltip_view"></div>
  </body>
</html>