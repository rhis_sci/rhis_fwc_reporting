# Author: Arjuda Anjum
SHELL := /bin/bash
#run with this: $ make db pass=rhis12#rhis
db:
	@ssh anjum@10.10.10.17 'PGPASSWORD=${pass} psql -U rhis_admin -h 10.12.1.21 -p 5432 RHIS_FACILITY_CENTRAL -t -c \
		"select zillaid,upazilaid,url,port,dbname from emis_facility_implemented_upazila order by zillaid,upazilaid" \
			| while read zillaid upazilaid; \
				do echo "USER: $$zillaid --> $$upazilaid";\
			done'
test:
	@ssh anjum@10.10.10.17 'PGPASSWORD=${pass} psql -U rhis_admin -h 10.12.1.21 -p 5432 RHIS_FACILITY_CENTRAL -t -c \
		"select url,port,dbname,username,password from emis_facility_implemented_upazila order by zillaid,upazilaid" \
			| while read dbname url port dbname username password ; do\
			    psql -U $$username -h $$url -p $$port -d $$dbname -t -c  "select count (*) from providerdb"\
			    done'
