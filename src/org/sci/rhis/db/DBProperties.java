package org.sci.rhis.db;
import java.util.Properties;

public class DBProperties {

    public Properties setProperties(){
        Properties prop = new Properties();
        try {
            //Set Table Name
            prop.setProperty("table_clientmap", "clientMap");
            prop.setProperty("table_member", "Member");
            prop.setProperty("table_gpservice", "gpService");
            prop.setProperty("table_providerdb", "ProviderDB");
            prop.setProperty("table_providertype", "ProviderType");
            prop.setProperty("table_node_details", "node_details");
            prop.setProperty("table_providerarea", "ProviderArea");
            prop.setProperty("table_zilla", "Zilla");
            prop.setProperty("table_upazila", "Upazila");
            prop.setProperty("table_unions", "Unions");
            prop.setProperty("table_village", "Village");
            prop.setProperty("table_household", "Household");
            prop.setProperty("table_last_login_detail", "last_login_detail");
            prop.setProperty("table_facility_provider", "facility_provider");
            prop.setProperty("table_facility_type", "facility_type");
            prop.setProperty("table_facility_registry", "facility_registry");
            prop.setProperty("table_mis3_report_submission", "mis3_report_submission");
            prop.setProperty("table_mis3_report_submission_data", "mis3_report_submission_data");
            prop.setProperty("table_ancservice", "ancService");
            prop.setProperty("table_pncservicemother", "pncServiceMother");
            prop.setProperty("table_pncservicechild", "pncServiceChild");
            prop.setProperty("table_delivery", "delivery");
            prop.setProperty("table_newborn", "newBorn");
            prop.setProperty("table_pregwomen", "pregWomen");
            prop.setProperty("table_death", "death");
            prop.setProperty("table_providertype", "ProviderType");
            prop.setProperty("table_pillcondomservice", "pillCondomService");
            prop.setProperty("table_iudservice", "iudService");
            prop.setProperty("table_iudfollowupservice", "iudFollowupService");
            prop.setProperty("table_implantservice", "implantService");
            prop.setProperty("table_implantfollowupservice", "implantFollowupService");
            prop.setProperty("table_womaninjectable", "womanInjectable");
            prop.setProperty("table_pacservice", "pacService");
            prop.setProperty("table_censusdata", "CensusData");
            prop.setProperty("table_satelite_planning_master", "satelite_planning_master");
            prop.setProperty("table_satelite_planning_details", "satelite_planning_details");
            prop.setProperty("table_immunizationhistory", "immunizationHistory");
            prop.setProperty("table_elco", "elco");
            prop.setProperty("table_regserial", "regSerial");
            prop.setProperty("table_deathreason", "deathReason");
            prop.setProperty("table_deliveryattendantdesignation", "delivery_attendant_designation");
            prop.setProperty("table_diseaselist", "diseaseList");
            prop.setProperty("table_disease_list_en", "disease_list_en");
            prop.setProperty("table_fwaunit", "FWAUnit");
            prop.setProperty("table_associated_provider_id", "associated_provider_id");
            prop.setProperty("table_fpexamination", "FPExamination");
            prop.setProperty("table_permanent_method_service", "permanent_method_service");
            prop.setProperty("table_permanent_method_followup_service", "permanent_method_followup_service");
            //Set Column Schema

            //zilla
            prop.setProperty("zilla_divid", "DIVID,integer," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_zillaid", "ZILLAID,integer," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_zillaname", "ZILLANAME,character varying," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_zillanameeng", "ZILLANAMEENG,character varying," + prop.getProperty("table_zilla"));

            //upazila
            prop.setProperty("upazila_upazilaid", "UPAZILAID,integer," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_upazilaname", "UPAZILANAME,character varying," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_upazilanameeng", "UPAZILANAMEENG,character varying," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_zillaid", "ZILLAID,integer," + prop.getProperty("table_upazila"));

            //unions
            prop.setProperty("unions_municipalityid", "MUNICIPALITYID,integer," + prop.getProperty("table_unions"));
            prop.setProperty("unions_unionid", "UNIONID,integer," + prop.getProperty("table_unions"));
            prop.setProperty("unions_unionname", "UNIONNAME,character varying," + prop.getProperty("table_unions"));
            prop.setProperty("unions_unionnameeng", "UNIONNAMEENG,character varying," + prop.getProperty("table_unions"));
            prop.setProperty("unions_upazilaid", "UPAZILAID,integer," + prop.getProperty("table_unions"));
            prop.setProperty("unions_zillaid", "ZILLAID,integer," + prop.getProperty("table_unions"));

            //village
            prop.setProperty("village_close", "close,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_crrvillagename", "CRRVILLAGENAME,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_fwaunit", "fwaunit,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_mouzaid", "MOUZAID,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_rmo", "RMO,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_unionid", "UNIONID,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_upazilaid", "UPAZILAID,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_villageid", "VILLAGEID,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_villagename", "VILLAGENAME,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_villagenameeng", "VILLAGENAMEENG,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_ward", "ward,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_zillaid", "ZILLAID,integer," + prop.getProperty("table_village"));

            //facility_provider
            prop.setProperty("facility_provider_end_date", "end_date,timestamp with time zone,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_facility_id", "facility_id,bigint,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_is_active", "is_active,integer,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_provider_id", "provider_id,integer,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_start_date", "start_date,timestamp with time zone,"+prop.getProperty("table_facility_provider"));


            //facility_type
            prop.setProperty("facility_type_description", "description,text," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_description_bangla", "description_bangla,text," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_short_code", "short_code,text," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_type", "type,integer," + prop.getProperty("table_facility_type"));

            //facility_registry
            prop.setProperty("facility_registry_facility_category", "facility_category,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facilityid", "facilityid,bigint," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facility_name", "facility_name,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facility_type", "facility_type,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facility_type_id", "facility_type_id,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_lat", "lat,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_lon", "lon,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_mouzaid", "mouzaid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_unionid", "unionid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_upazilaid", "upazilaid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_villageid", "villageid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_zillaid", "zillaid,integer," + prop.getProperty("table_facility_registry"));

            //mis3_report_submission
            prop.setProperty("mis3_report_submission_approval_date", "approval_date,timestamp without time zone," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_facility_id", "facility_id,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_report_month", "report_month,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_report_year", "report_year,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_submission_date", "submission_date,timestamp without time zone," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_submission_status", "submission_status,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_submitted_by", "submitted_by,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_supervise_comment", "supervise_comment,text," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_supervisor_comment", "supervisor_comment,text," + prop.getProperty("table_mis3_report_submission"));

            //mis3_report_submission_data
            prop.setProperty("mis3_report_submission_data_facility_id", "facility_id,integer,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_field_name", "field_name,character varying,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_field_value", "field_value,integer,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_report_month", "report_month,integer,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_report_year", "report_year,integer,"+prop.getProperty("table_mis3_report_submission_data"));

            //clientmap
            prop.setProperty("clientmap_gender", "gender,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_generatedid", "generatedId,bigint," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_healthid", "healthId,bigint," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_dob", "dob,date," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_name", "name,text," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_mobileno", "mobileNo,bigint," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_zillaid", "zillaId,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_upazilaid", "upazilaId,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_unionid", "unionId,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_villageid", "villageId,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_mouzaid", "mouzaId,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_systementrydate", "systemEntryDate,date," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_husbandname", "husbandName,text," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_hhno", "houseGRHoldingNo,text," + prop.getProperty("table_clientmap"));

            //member
            prop.setProperty("member_nameeng", "NameEng,character varying," + prop.getProperty("table_member"));
            prop.setProperty("member_healthid", "HealthID,bigint," + prop.getProperty("table_member"));

            //household
            prop.setProperty("household_dist", "Dist,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_div", "Div,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_hhno", "HHNo,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_lat", "Lat,character varying," + prop.getProperty("table_household"));
            prop.setProperty("household_lon", "Lon,character varying," + prop.getProperty("table_household"));
            prop.setProperty("household_mouza", "Mouza,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_provcode", "ProvCode,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_provtype", "ProvType,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_subblock", "subBlock,character varying," + prop.getProperty("table_household"));
            prop.setProperty("household_un", "UN,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_unit", "unit,character varying," + prop.getProperty("table_household"));
            prop.setProperty("household_upz", "Upz,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_vill", "Vill,integer," + prop.getProperty("table_household"));
            prop.setProperty("household_wardnew", "wardNew,character varying," + prop.getProperty("table_household"));
            prop.setProperty("household_wardold", "wardOld,character varying," + prop.getProperty("table_household"));


            //gpservice
            prop.setProperty("gpservice_advice", "advice,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_age", "age,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_anemia", "anemia,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_bpdiastolic", "bpDiastolic,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_bpsystolic", "bpSystolic,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_client", "client,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_complicationsign", "complicationSign,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_disease", "disease,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_edema", "edema,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_healthid", "healthId,bigint," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_jaundice", "jaundice,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_liver", "liver,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_lungs", "lungs,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_modifydate", "modifyDate,date," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_other", "other,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_providerid", "providerId,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_pulse", "pulse,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_refer", "refer,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_refercentername", "referCenterName,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_referreason", "referReason,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_serviceid", "serviceId,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_servicesource", "serviceSource,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_splin", "splin,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_symptom", "symptom,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_temperature", "temperature,real," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_tonsil", "tonsil,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_treatment", "treatment,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_upload", "upload,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_urinealbumin", "urineAlbumin,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_urinesugar", "urineSugar,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_visitdate", "visitDate,date," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_visitsource", "visitSource,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_weight", "weight,real," + prop.getProperty("table_gpservice"));

            //providerDB
            prop.setProperty("providerdb_active", "Active,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_areaupdate", "AreaUpdate,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_csba", "csba,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_devicesetting", "DeviceSetting,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_divid", "Divid,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_endate", "EnDate,timestamp with time zone," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_exdate", "ExDate,timestamp with time zone," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_facilityname", "FacilityName,text," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_healthidrequest", "HealthIDRequest,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_mobileno", "MobileNo,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provcode", "ProvCode,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provname", "ProvName,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provpass", "ProvPass,text," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provtype", "ProvType,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_supervisorcode", "supervisorCode,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_systemupdatedt", "SystemUpdateDT,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_tablestructurerequest", "TableStructureRequest,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_unionid", "unionid,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_upazilaid", "upazilaid,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_zillaid", "zillaid,integer," + prop.getProperty("table_providerdb"));

            //providertype
            prop.setProperty("providertype_provtype", "ProvType,integer," + prop.getProperty("table_providertype"));
            prop.setProperty("providertype_typename", "TypeName,character varying," + prop.getProperty("table_providertype"));

            //providerarea
            prop.setProperty("providerarea_areacode", "AreaCode,character varying," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_block", "Block,character varying," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_divid", "Divid,integer," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_fwa_round", "fwa_round,numeric," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_fwaunit", "FWAUnit,character varying," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_mouzaid", "mouzaid,integer," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_provcode", "provCode,integer," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_provtype", "provType,integer," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_starthhno", "StartHHno,character varying," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_unionid", "unionid,integer," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_upazilaid", "upazilaid,integer," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_villageid", "villageid,integer," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_ward", "Ward,character varying," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_wardnew", "WardNew,character varying," + prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_zillaid", "zillaid,integer," + prop.getProperty("table_providerarea"));

            //node_details
            prop.setProperty("node_details_base_url", "base_url,character varying," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_client", "client,integer," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_id", "id,character varying," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_provcode", "ProvCode,integer," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_provtype", "ProvType,integer," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_server", "server,character varying," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_sync_url", "sync_url,character varying," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_version", "version,character varying," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_upload", "upload,integer," + prop.getProperty("table_node_details"));
            prop.setProperty("node_details_dbdownloadstatus", "dbdownloadstatus,integer," + prop.getProperty("table_node_details"));

            //table_last_login_detail
            prop.setProperty("last_login_detail_login_request_detail", "login_request_detail,text," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_login_success", "login_success,boolean," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_login_time", "login_time,timestamp without time zone," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_monitoring_tool", "monitoring_tool,integer," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_provider_id", "provider_id,integer," + prop.getProperty("table_last_login_detail"));

            //ancservice
            prop.setProperty("ancservice_advice", "advice,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_anemia", "anemia,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_bpdiastolic", "bpDiastolic,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_bpsystolic", "bpSystolic,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_client", "client,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_complicationsign", "complicationSign,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_disease", "disease,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_edema", "edema,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_fetalpresentation", "fetalPresentation,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_fetusheartrate", "fetusHeartRate,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_healthid", "healthId,bigint," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_ironfolqty", "ironFolQty,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_ironfolstatus", "ironFolStatus,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_ironfolunit", "ironFolUnit,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_jaundice", "jaundice,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_misoqty", "misoQty,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_misostatus", "misoStatus,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_misounit", "misoUnit,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_modifydate", "modifyDate,date," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_pregno", "pregNo,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_providerid", "providerId,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_refer", "refer,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_refercentername", "referCenterName,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_referreason", "referReason,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_serviceid", "serviceId,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_servicesource", "serviceSource,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_symptom", "symptom,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_treatment", "treatment,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_upload", "upload,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_urinealbumin", "urineAlbumin,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_urinesugar", "urineSugar,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_uterusheight", "uterusHeight,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_visitdate", "visitDate,date," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_visitmonth", "visitMonth,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_visitsource", "visitSource,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_weight", "weight,real," + prop.getProperty("table_ancservice"));

            //pncservicemother
            prop.setProperty("pncservicemother_advice", "advice,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_anemia", "anemia,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_bpdiastolic", "bpDiastolic,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_bpsystolic", "bpSystolic,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_breastcondition", "breastCondition,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_client", "client,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_complicationsign", "complicationSign,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_disease", "disease,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_edema", "edema,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_fpmethod", "FPMethod,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_healthid", "healthId,bigint," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_hematuria", "hematuria,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_modifydate", "modifyDate,date," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_perineum", "perineum,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_pregno", "pregNo,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_providerid", "providerId,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_refer", "refer,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_refercentername", "referCenterName,character varying," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_referreason", "referReason,character varying," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_serviceid", "serviceId,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_servicesource", "serviceSource,character varying," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_symptom", "symptom,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_systementrydate", "systemEntryDate,date," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_temperature", "temperature,double precision," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_treatment", "treatment,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_upload", "upload,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_uterusinvolution", "uterusInvolution,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_visitdate", "visitDate,date," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_visitmonth", "visitMonth,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_visitsource", "visitSource,integer," + prop.getProperty("table_pncservicemother"));

            //pncservicechild
            prop.setProperty("pncservicechild_advice", "advice,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_breastfeedingonly", "breastFeedingOnly,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_breathingperminute", "breathingPerMinute,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_childhealthid", "childHealthId,bigint," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_childno", "childNo,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_client", "client,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_dangersign", "dangerSign,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_disease", "disease,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_healthid", "healthId,bigint," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_modifydate", "modifyDate,date," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_pregno", "pregNo,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_providerid", "providerId,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_refer", "refer,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_refercentername", "referCenterName,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_referreason", "referReason,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_serviceid", "serviceId,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_servicesource", "serviceSource,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_symptom", "symptom,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_systementrydate", "systemEntryDate,date," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_temperature", "temperature,real," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_treatment", "treatment,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_upload", "upload,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_visitdate", "visitDate,date," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_visitmonth", "visitMonth,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_visitsource", "visitSource,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_weight", "weight,real," + prop.getProperty("table_pncservicechild"));

            //delivery
            prop.setProperty("delivery_abortion", "abortion,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_admissiondate", "admissionDate,date," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_advice", "advice,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_applyoxytocin", "applyOxytocin,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_applytraction", "applyTraction,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_attendantdesignation", "attendantDesignation,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_attendantname", "attendantName,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_bed", "bed,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_blockeddelivery", "blockedDelivery,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_blurryvision", "blurryVision,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_client", "client,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_convulsion", "convulsion,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_deliverycentername", "deliveryCenterName,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_deliverydonebythisprovider", "deliveryDoneByThisProvider,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_episiotomy", "episiotomy,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_excessbloodloss", "excessBloodLoss,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_headache", "headache,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_healthid", "healthId,bigint," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_latedelivery", "lateDelivery,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_livebirth", "liveBirth,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_misoprostol", "misoprostol,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_modifydate", "modifyDate,date," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_nboy", "nBoy,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_ngirl", "nGirl,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_nunidentified", "nUnidentified,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_otherbodypart", "otherBodyPart,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_others", "others,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_othersnote", "othersNote,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcomedate", "outcomeDate,date," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcomeplace", "outcomePlace,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcometime", "outcomeTime,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcometype", "outcomeType,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_placenta", "placenta,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_pregno", "pregNo,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_providerid", "providerId,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_refer", "refer,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_refercentername", "referCenterName,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_referreason", "referReason,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_stillbirth", "stillBirth,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_stillbirthfresh", "stillBirthFresh,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_stillbirthmacerated", "stillBirthMacerated,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_systementrydate", "systemEntryDate,date," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_treatment", "treatment,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_upload", "upload,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_uterusmassage", "uterusMassage,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_ward", "ward,text," + prop.getProperty("table_delivery"));

            //newborn
            prop.setProperty("newborn_attendantdesignation", "attendantDesignation,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_bagnmask", "bagNMask,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_baththreedays", "bathThreeDays,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_birthstatus", "birthStatus,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_birthweight", "birthWeight,real," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_birthweightstatus", "birthWeightStatus,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_breastfeed", "breastFeed,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_childhealthid", "childHealthId,bigint," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_childno", "childNo,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_chlorehexidin", "chlorehexidin,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_client", "client,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_dryingafterbirth", "dryingAfterBirth,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_gender", "gender,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_healthid", "healthId,bigint," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_immaturebirth", "immatureBirth,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_livingstatus", "livingStatus,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_modifydate", "modifyDate,date," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcomedate", "outcomeDate,date," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcomeplace", "outcomePlace,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcometime", "outcomeTime,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcometype", "outcomeType,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_pregno", "pregNo,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_providerid", "providerId,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_refer", "refer,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_refercentername", "referCenterName,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_referreason", "referReason,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_resassitation", "resassitation,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_skintouch", "skinTouch,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_stimulation", "stimulation,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_systementrydate", "systemEntryDate,date," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_upload", "upload,integer," + prop.getProperty("table_newborn"));

            //pregwomen
            prop.setProperty("pregwomen_bloodgroup", "bloodGroup,text," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_client", "client,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_edd", "EDD,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_gravida", "gravida,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_healthid", "healthId,bigint," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_height", "height,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_housegrholdingno", "houseGRHoldingNo,character varying," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_lastchildage", "lastChildAge,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_lmp", "LMP,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_mobileno", "mobileNo,bigint," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_modifydate", "modifyDate,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_para", "para,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_pregno", "pregNo,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_providerid", "providerId,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_riskhistory", "riskHistory,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_riskhistorynote", "riskHistoryNote,text," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_statuslmp", "statusLMP,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_systementrydate", "systemEntryDate,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_templmp", "tempLMP,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_upload", "upload,integer," + prop.getProperty("table_pregwomen"));

            //death
            prop.setProperty("death_causeofdeath", "causeOfDeath,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_childno", "childNo,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_client", "client,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_deathdt", "deathDT,date," + prop.getProperty("table_death"));
            prop.setProperty("death_deathofpregwomen", "deathOfPregWomen,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_healthid", "healthId,bigint," + prop.getProperty("table_death"));
            prop.setProperty("death_modifydate", "modifyDate,date," + prop.getProperty("table_death"));
            prop.setProperty("death_placeofdeath", "placeOfDeath,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_pregno", "pregNo,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_providerid", "providerId,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_systementrydate", "systemEntryDate,date," + prop.getProperty("table_death"));
            prop.setProperty("death_upload", "upload,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_uploaddate", "uploadDate,date," + prop.getProperty("table_death"));

            //providertype
            prop.setProperty("providertype_provtype", "ProvType,integer," + prop.getProperty("table_providertype"));
            prop.setProperty("providertype_typename", "TypeName,character varying," + prop.getProperty("table_providertype"));

            //pillcondomservice
            prop.setProperty("pillcondomservice_amount", "amount,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_client", "client,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_healthid", "healthId,bigint," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_isnewclient", "isNewClient,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_methodtype", "methodType,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_modifydate", "modifyDate,date," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_providerid", "providerId,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_serviceid", "serviceId,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_transactionid", "transactionId,bigint," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_visitdate", "visitDate,date," + prop.getProperty("table_pillcondomservice"));

            //iudservice
            prop.setProperty("iudservice_attendantallowance", "attendantAllowance,real," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_attendantdesignation", "attendantDesignation,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_attendantname", "attendantName,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_client", "client,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_clientallowance", "clientAllowance,real," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_companionaddress", "companionAddress,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_companionallowance", "companionAllowance,real," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_companionname", "companionName,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_healthid", "healthId,bigint," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudafterdelivery", "iudAfterDelivery,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudcount", "iudCount,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudimplantdate", "iudImplantDate,date," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudtype", "iudType,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_lmp", "LMP,date," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_modifydate", "modifyDate,date," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_providerasattendant", "providerAsAttendant,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_providerid", "providerId,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_servicesource", "serviceSource,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_treatment", "treatment,text," + prop.getProperty("table_iudservice"));

            //iudfollwupservice
            prop.setProperty("iudfollowupservice_allowance", "allowance,real," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_attendantname", "attendantName,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_client", "client,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_comment", "comment,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_complication", "complication,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_followupdate", "followupDate,date," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_fpamount", "fpAmount,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_fpmethod", "fpMethod,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_healthid", "healthId,bigint," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_isfixedvisit", "isFixedVisit,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudcount", "iudCount,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremovedate", "iudRemoveDate,date," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremoverdesignation", "iudRemoverDesignation,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremovereason", "iudRemoveReason,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremovername", "iudRemoverName,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_management", "management,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_modifydate", "modifyDate,date," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_monitoringofficername", "monitoringOfficerName,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_providerasattendant", "providerAsAttendant,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_providerid", "providerId,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_refer", "refer,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_refercentername", "referCenterName,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_referreason", "referReason,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_serviceid", "serviceId,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_treatment", "treatment,text," + prop.getProperty("table_iudfollowupservice"));

            //implantservice
            prop.setProperty("implantservice_attendantallowance", "attendantAllowance,real," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_attendantdesignation", "attendantDesignation,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_attendantname", "attendantName,text," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_client", "client,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_clientallowance", "clientAllowance,real," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_healthid", "healthId,bigint," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implantafterdelivery", "implantAfterDelivery,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implantcount", "implantCount,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implantimplantdate", "implantImplantDate,date," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implanttype", "implantType,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_modifydate", "modifyDate,date," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_providerasattendant", "providerAsAttendant,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_providerid", "providerId,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_servicesource", "serviceSource,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_treatment", "treatment,text," + prop.getProperty("table_implantservice"));

            //implantfollowupservice
            prop.setProperty("implantfollowupservice_allowance", "allowance,real," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_attendantname", "attendantName,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_client", "client,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_comment", "comment,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_complication", "complication,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_followupdate", "followupDate,date," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_fpamount", "fpAmount,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_fpmethod", "fpMethod,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_healthid", "healthId,bigint," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantcount", "implantCount,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremovedate", "implantRemoveDate,date," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremoverdesignation", "implantRemoverDesignation,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremovereason", "implantRemoveReason,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremovername", "implantRemoverName,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_isfixedvisit", "isFixedVisit,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_management", "management,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_modifydate", "modifyDate,date," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_monitoringofficername", "monitoringOfficerName,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_providerasattendant", "providerAsAttendant,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_providerid", "providerId,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_refer", "refer,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_refercentername", "referCenterName,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_referreason", "referReason,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_serviceid", "serviceId,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_treatment", "treatment,text," + prop.getProperty("table_implantfollowupservice"));

            //womaninjectable
            prop.setProperty("womaninjectable_bpdiastolic", "bpDiastolic,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_bpsystolic", "bpSystolic,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_breastmass", "breastMass,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_client", "client,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_dosedate", "doseDate,date," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_doseid", "doseId,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_healthid", "healthId,bigint," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_injectionid", "injectionId,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_isnewclient", "isNewClient,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_lmp", "LMP,date," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_modifydate", "modifyDate,date," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_providerid", "providerId,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_pulse", "pulse,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_pvc", "PVC,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_sideeffect", "sideEffect,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_systementrydate", "systemEntryDate,date," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_upload", "upload,integer," + prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_weight", "weight,real," + prop.getProperty("table_womaninjectable"));

            //pacservice
            prop.setProperty("pacservice_abdomen", "abdomen,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_advice", "advice,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_anemia", "anemia,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_bpdiastolic", "bpDiastolic,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_bpsystolic", "bpSystolic,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_cervix", "cervix,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_client", "client,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_complicationsign", "complicationSign,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_disease", "disease,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_fpmethod", "FPMethod,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_healthid", "healthId,bigint," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_hematuria", "hematuria,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_modifydate", "modifyDate,date," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_perineum", "perineum,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_pregno", "pregNo,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_providerid", "providerId,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_refer", "refer,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_refercentername", "referCenterName,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_referreason", "referReason,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_satelitecentername", "sateliteCenterName,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_serviceid", "serviceId,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_servicesource", "serviceSource,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_symptom", "symptom,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_systementrydate", "systemEntryDate,date," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_temperature", "temperature,real," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_treatment", "treatment,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_upload", "upload,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_uterusinvolution", "uterusInvolution,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_visitdate", "visitDate,date," + prop.getProperty("table_pacservice"));

            //satelite_planning_master
            prop.setProperty("satelite_planning_master_approve_date", "approve_date,timestamp without time zone," + prop.getProperty("table_satelite_planning_master"));
            prop.setProperty("satelite_planning_master_comment", "comment,text," + prop.getProperty("table_satelite_planning_master"));
            prop.setProperty("satelite_planning_master_modify_date", "modify_date,timestamp without time zone," + prop.getProperty("table_satelite_planning_master"));
            prop.setProperty("satelite_planning_master_provider_id", "provider_id,integer," + prop.getProperty("table_satelite_planning_master"));
            prop.setProperty("satelite_planning_master_satelite_planning_id", "satelite_planning_id,integer," + prop.getProperty("table_satelite_planning_master"));
            prop.setProperty("satelite_planning_master_satelite_planning_year", "satelite_planning_year,integer," + prop.getProperty("table_satelite_planning_master"));
            prop.setProperty("satelite_planning_master_status", "status,integer," + prop.getProperty("table_satelite_planning_master"));
            prop.setProperty("satelite_planning_master_submit_date", "submit_date,timestamp without time zone," + prop.getProperty("table_satelite_planning_master"));

            //satelite_planning_details
            prop.setProperty("satelite_planning_details_modify_date", "modify_date,timestamp without time zone," + prop.getProperty("table_satelite_planning_details"));
            prop.setProperty("satelite_planning_details_satelite_actual_date", "satelite_actual_date,timestamp without time zone," + prop.getProperty("table_satelite_planning_details"));
            prop.setProperty("satelite_planning_details_satelite_id", "satelite_id,integer," + prop.getProperty("table_satelite_planning_details"));
            prop.setProperty("satelite_planning_details_satelite_name", "satelite_name,character varying," + prop.getProperty("table_satelite_planning_details"));
            prop.setProperty("satelite_planning_details_satelite_planning_id", "satelite_planning_id,integer," + prop.getProperty("table_satelite_planning_details"));
            prop.setProperty("satelite_planning_details_satelite_scheduled_date", "satelite_scheduled_date,date," + prop.getProperty("table_satelite_planning_details"));
            prop.setProperty("satelite_planning_details_system_entry_date", "system_entry_date,timestamp without time zone," + prop.getProperty("table_satelite_planning_details"));
            prop.setProperty("satelite_planning_details_unplaned_satelite", "unplaned_satelite,boolean," + prop.getProperty("table_satelite_planning_details"));
            //immunizationHistory
            prop.setProperty("immunizationhistory_healthid", "healthId,bigint," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imucard", "imuCard,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imucode", "imuCode,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imudate", "imuDate,date," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imudose", "imuDose,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imusource", "imuSource,character varying," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_modifydate", "modifyDate,date," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_providerid", "providerId,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_systementrydate", "systemEntryDate,date," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_upload", "upload,integer," + prop.getProperty("table_immunizationhistory"));

            //elco
            prop.setProperty("elco_dau", "dau,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_domsource", "domSource,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_elcono", "elcoNo,text," + prop.getProperty("table_elco"));
            prop.setProperty("elco_hahhno", "haHHNo,text," + prop.getProperty("table_elco"));
            prop.setProperty("elco_healthid", "healthId,bigint," + prop.getProperty("table_elco"));
            prop.setProperty("elco_hhstatus", "hhStatus,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_husbandage", "husbandAge,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_husbandname", "husbandName,text," + prop.getProperty("table_elco"));
            prop.setProperty("elco_marrage", "marrAge,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_marrdate", "marrDate,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_modifydate", "modifyDate,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_providerid", "providerId,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_regdt", "regDT,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_son", "son,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_systementrydate", "systemEntryDate,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_upload", "upload,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_uploaddate", "uploadDate,date," + prop.getProperty("table_elco"));

            //regSerial
            prop.setProperty("regserial_facilityid", "facilityId,integer," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_healthid", "healthId,bigint," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_modifydate", "modifyDate,date," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_providerid", "providerId,integer," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_serialno", "serialNo,bigint," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_servicecategory", "serviceCategory,integer," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_systementrydate", "systemEntryDate,date," + prop.getProperty("table_regserial"));

            //deathReason
            prop.setProperty("deathreason_code", "code,text," + prop.getProperty("table_deathreason"));
            prop.setProperty("deathreason_deathreasonid", "deathReasonId,integer," + prop.getProperty("table_deathreason"));
            prop.setProperty("deathreason_detail", "detail,text," + prop.getProperty("table_deathreason"));

            //diseaselist
            prop.setProperty("diseaselist_detail", "detail,text," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_id", "id,integer," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_serviceid", "serviceId,integer," + prop.getProperty("table_diseaselist"));

            //diseaselist en
            prop.setProperty("disease_list_en_detail", "detail,text," + prop.getProperty("table_disease_list_en"));
            prop.setProperty("disease_list_en_id", "id,integer," + prop.getProperty("table_disease_list_en"));
            prop.setProperty("disease_list_en_serviceid", "serviceId,integer," + prop.getProperty("table_disease_list_en"));

            //fwaunit
            prop.setProperty("fwaunit_ucode", "UCode,character varying," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_uname", "UName,character varying," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_unameban", "UNameBan,character varying," + prop.getProperty("table_fwaunit"));

            //associated_provider_id
            prop.setProperty("associated_provider_id_associated_id", "associated_id,integer,"+prop.getProperty("table_associated_provider_id"));
            prop.setProperty("associated_provider_id_provider_id", "provider_id,integer,"+prop.getProperty("table_associated_provider_id"));

            //FPExamination
            prop.setProperty("fpexamination_healthid", "healthId,bigint,"+prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_serviceid", "serviceId,integer,"+prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_fptype", "FPType,integer,"+prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_bpdiastolic", "bpDiastolic,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_bpsystolic", "bpSystolic,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_jaundice", "jaundice,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_diabetes", "diabetes,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_breastcondition", "breastCondition,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_lmp", "LMP,date," + prop.getProperty("table_fpexamination"));

            //PermanentMethodService
            prop.setProperty("permanent_method_service_advice", "advice,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_client", "client,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_client_allowance", "client_allowance,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_client_came_alone", "client_came_alone,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_cloth", "cloth,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_address", "companion_address,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_allowance", "companion_allowance,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_designation", "companion_designation,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_mobile", "companion_mobile,bigint,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_name", "companion_name,character varying,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_complication", "complication,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_doctor_designation", "doctor_designation,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_doctor_name", "doctor_name,character varying,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_healthid", "healthid,bigint,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_management", "management,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_operation_name", "operation_name,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_permanent_method_operation_date", "permanent_method_operation_date,date,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_permanent_method_operation_time", "permanent_method_operation_time,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_pmscount", "pmscount,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_providerid", "providerid,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_refer", "refer,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_refer_center_name", "refer_center_name,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_refer_reason", "refer_reason,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_treatment", "treatment,character varying,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_upload", "upload,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_permanent_method_service"));

            //PermanentMethodFollowupService
            prop.setProperty("permanent_method_followup_service_complication", "complication,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_doctor_designation", "doctor_designation,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_doctor_name", "doctor_name,character varying,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_examination", "examination,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_followup_date", "followup_date,date,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_followup_reason", "followup_reason,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_healthid", "healthid,bigint,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_management", "management,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_pmscount", "pmscount,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_providerid", "providerid,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_refer", "refer,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_refer_center_name", "refer_center_name,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_refer_reason", "refer_reason,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_serviceid", "serviceid,bigint,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_treatment", "treatment,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_upload", "upload,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_permanent_method_followup_service"));

        }
        catch (Exception e){
            e.printStackTrace();
        }

        return prop;
    }
}
