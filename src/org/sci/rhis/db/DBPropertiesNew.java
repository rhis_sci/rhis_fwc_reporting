package org.sci.rhis.db;
import java.util.Properties;

public class DBPropertiesNew {

    public Properties setProperties() {
        Properties prop = new Properties();

        try {
            //Set Table Name
            prop.setProperty("table_censusdata", "CensusData");
            prop.setProperty("table_clientmap", "clientmap");
            prop.setProperty("table_member", "member");
            prop.setProperty("table_gpservice", "gpservice");
            prop.setProperty("table_providerdb", "providerdb");
            prop.setProperty("table_providertype", "providertype");
            prop.setProperty("table_node_details", "node_details");
            prop.setProperty("table_providerarea", "providerarea");
            prop.setProperty("table_provider_leave_status", "provider_leave_status");
            prop.setProperty("table_zilla", "zilla");
            prop.setProperty("table_upazila", "upazila");
            prop.setProperty("table_unions", "unions");
            prop.setProperty("table_village", "village");
            prop.setProperty("table_last_login_detail", "last_login_detail");
            prop.setProperty("table_facility_provider", "facility_provider");
            prop.setProperty("table_facility_type", "facility_type");
            prop.setProperty("table_facility_registry", "facility_registry");
            prop.setProperty("table_mis3_report_submission", "mis3_report_submission");
            prop.setProperty("table_mis3_report_submission_data", "mis3_report_submission_data");
            prop.setProperty("table_mis3_approver", "mis3_approver");
            prop.setProperty("table_ancservice", "ancservice");
            prop.setProperty("table_pncservicemother", "pncservicemother");
            prop.setProperty("table_pncservicechild", "pncservicechild");
            prop.setProperty("table_delivery", "delivery");
            prop.setProperty("table_newborn", "newborn");
            prop.setProperty("table_pregwomen", "pregwomen");
            prop.setProperty("table_death", "death");
            prop.setProperty("table_providertype", "providertype");
            prop.setProperty("table_pillcondomservice", "pillcondomservice");
            prop.setProperty("table_iudservice", "iudservice");
            prop.setProperty("table_iudfollowupservice", "iudfollowupservice");
            prop.setProperty("table_implantservice", "implantservice");
            prop.setProperty("table_implantfollowupservice", "implantfollowupservice");
            prop.setProperty("table_womaninjectable", "womaninjectable");
            prop.setProperty("table_pacservice", "pacservice");
            prop.setProperty("table_satelite_session_planning", "satelite_session_planning");
            prop.setProperty("table_satelite_planning_detail", "satelite_planning_detail");
            prop.setProperty("table_users", "users");
            prop.setProperty("table_immunizationhistory", "immunizationhistory");
            prop.setProperty("table_elco", "elco");
            prop.setProperty("table_regserial", "regserial");
            prop.setProperty("table_deathreason", "deathreason");
            prop.setProperty("table_deliveryattendantdesignation", "delivery_attendant_designation");
            prop.setProperty("table_diseaselist", "diseaselist");
            prop.setProperty("table_disease_list_en", "disease_list_en");
            prop.setProperty("table_fwaunit", "fwaunit");
            prop.setProperty("table_associated_provider_id", "associated_provider_id");
            prop.setProperty("table_fpexamination", "fpexamination");
            prop.setProperty("table_permanent_method_service", "permanent_method_service");
            prop.setProperty("table_permanent_method_followup_service", "permanent_method_followup_service");
            prop.setProperty("table_child_care_service", "child_care_service");
            prop.setProperty("table_child_care_service_detail", "child_care_service_detail");

            //Set Column Schema

            //ancservice
            prop.setProperty("ancservice_advice", "advice,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_anemia", "anemia,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_bpdiastolic", "bpdiastolic,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_bpsystolic", "bpsystolic,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_client", "client,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_complicationsign", "complicationsign,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_disease", "disease,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_edema", "edema,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_fetalpresentation", "fetalpresentation,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_fetusheartrate", "fetusheartrate,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_healthid", "healthid,bigint," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_ironfolqty", "ironfolqty,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_ironfolstatus", "ironfolstatus,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_ironfolunit", "ironfolunit,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_jaundice", "jaundice,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_misoqty", "misoqty,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_misostatus", "misostatus,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_misounit", "misounit,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_pregno", "pregno,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_providerid", "providerid,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_refer", "refer,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_refercentername", "refercentername,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_referreason", "referreason,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_serviceid", "serviceid,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_servicesource", "servicesource,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_symptom", "symptom,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_treatment", "treatment,text," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_upload", "upload,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_urinealbumin", "urinealbumin,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_urinesugar", "urinesugar,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_uterusheight", "uterusheight,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_visitdate", "visitdate,date," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_visitmonth", "visitmonth,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_visitsource", "visitsource,integer," + prop.getProperty("table_ancservice"));
            prop.setProperty("ancservice_weight", "weight,real," + prop.getProperty("table_ancservice"));

            //clientmap
            prop.setProperty("clientmap_gender", "gender,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_generatedid", "generatedid,bigint," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_healthid", "healthid,bigint," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_dob", "dob,date," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_name", "name,text," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_mobileno", "mobileno,bigint," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_zillaid", "zillaid,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_upazilaid", "upazilaid,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_unionid", "unionid,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_villageid", "villageid,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_mouzaid", "mouzaid,integer," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_husbandname", "husbandname,text," + prop.getProperty("table_clientmap"));
            prop.setProperty("clientmap_hhno", "hhno,integer," + prop.getProperty("table_clientmap"));

            //member
            prop.setProperty("member_nameeng", "nameeng,character varying," + prop.getProperty("table_member"));
            prop.setProperty("member_healthid", "healthid,bigint," + prop.getProperty("table_member"));
            prop.setProperty("member_extype", "extype,integer," + prop.getProperty("table_member"));

            //death
            prop.setProperty("death_causeofdeath", "causeofdeath,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_childno", "childno,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_client", "client,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_deathdt", "deathdt,date," + prop.getProperty("table_death"));
            prop.setProperty("death_deathofpregwomen", "deathofpregwomen,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_healthid", "healthid,bigint," + prop.getProperty("table_death"));
            prop.setProperty("death_lat", "lat,real," + prop.getProperty("table_death"));
            prop.setProperty("death_lon", "lon,real," + prop.getProperty("table_death"));
            prop.setProperty("death_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_death"));
            prop.setProperty("death_placeofdeath", "placeofdeath,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_pregno", "pregno,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_providerid", "providerid,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_death"));
            prop.setProperty("death_upload", "upload,integer," + prop.getProperty("table_death"));
            prop.setProperty("death_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_death"));

            //delivery
            prop.setProperty("delivery_abortion", "abortion,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_admissiondate", "admissiondate,date," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_advice", "advice,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_applyoxytocin", "applyoxytocin,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_applytraction", "applytraction,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_attendantdesignation", "attendantdesignation,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_attendantname", "attendantname,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_bed", "bed,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_blockeddelivery", "blockeddelivery,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_blurryvision", "blurryvision,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_client", "client,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_convulsion", "convulsion,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_deliverycentername", "deliverycentername,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_deliverydonebythisprovider", "deliverydonebythisprovider,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_episiotomy", "episiotomy,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_excessbloodloss", "excessbloodloss,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_headache", "headache,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_healthid", "healthid,bigint," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_latedelivery", "latedelivery,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_livebirth", "livebirth,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_misoprostol", "misoprostol,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_nboy", "nboy,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_ngirl", "ngirl,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_nunidentified", "nunidentified,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_otherbodypart", "otherbodypart,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_others", "others,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_othersnote", "othersnote,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcomedate", "outcomedate,date," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcomeplace", "outcomeplace,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcometime", "outcometime,time without time zone," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_outcometype", "outcometype,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_placenta", "placenta,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_pregno", "pregno,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_providerid", "providerid,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_refer", "refer,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_refercentername", "refercentername,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_referreason", "referreason,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_satelitecentername", "satelitecentername,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_stillbirth", "stillbirth,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_stillbirthfresh", "stillbirthfresh,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_stillbirthmacerated", "stillbirthmacerated,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_treatment", "treatment,text," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_upload", "upload,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_uterusmassage", "uterusmassage,integer," + prop.getProperty("table_delivery"));
            prop.setProperty("delivery_ward", "ward,text," + prop.getProperty("table_delivery"));

            //facility_provider
            prop.setProperty("facility_provider_end_date", "end_date,timestamp with time zone,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_facility_id", "facility_id,bigint,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_is_active", "is_active,integer,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_provider_id", "providerid,integer,"+prop.getProperty("table_facility_provider"));
            prop.setProperty("facility_provider_start_date", "start_date,timestamp with time zone,"+prop.getProperty("table_facility_provider"));

            //facility_registry
            prop.setProperty("facility_registry_facility_category", "facility_category,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facilityid", "facilityid,bigint," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facility_name", "facility_name,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facility_type", "facility_type,text," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_facility_type_id", "facility_type_id,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_lat", "lat,real," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_lon", "lon,real," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_mouzaid", "mouzaid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_unionid", "unionid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_upazilaid", "upazilaid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_upload", "upload,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_villageid", "villageid,integer," + prop.getProperty("table_facility_registry"));
            prop.setProperty("facility_registry_zillaid", "zillaid,integer," + prop.getProperty("table_facility_registry"));

            //facility_type
            prop.setProperty("facility_type_description", "description,text," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_description_bangla", "description_bangla,text," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_short_code", "short_code,text," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_type", "type,integer," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_upload", "upload,integer," + prop.getProperty("table_facility_type"));
            prop.setProperty("facility_type_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_facility_type"));

            //gpservice
            prop.setProperty("gpservice_advice", "advice,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_age", "age,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_anemia", "anemia,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_bpdiastolic", "bpdiastolic,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_bpsystolic", "bpsystolic,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_client", "client,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_complicationsign", "complicationsign,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_disease", "disease,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_edema", "edema,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_healthid", "healthid,bigint," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_jaundice", "jaundice,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_liver", "liver,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_lungs", "lungs,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_other", "other,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_providerid", "providerid,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_pulse", "pulse,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_refer", "refer,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_refercentername", "refercentername,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_referreason", "referreason,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_serviceid", "serviceid,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_servicesource", "servicesource,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_splin", "splin,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_symptom", "symptom,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_temperature", "temperature,real," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_tonsil", "tonsil,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_treatment", "treatment,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_upload", "upload,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_urinealbumin", "urinealbumin,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_urinesugar", "urinesugar,text," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_visitdate", "visitdate,date," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_visitsource", "visitsource,integer," + prop.getProperty("table_gpservice"));
            prop.setProperty("gpservice_weight", "weight,real," + prop.getProperty("table_gpservice"));

            //implantfollowupservice
            prop.setProperty("implantfollowupservice_allowance", "allowance,real," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_attendantname", "attendantname,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_client", "client,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_comment", "comment,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_complication", "complication,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_followupdate", "followupdate,date," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_fpamount", "fpamount,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_fpmethod", "fpmethod,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_healthid", "healthid,bigint," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantcount", "implantcount,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremovedate", "implantremovedate,date," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremoverdesignation", "implantremoverdesignation,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremovereason", "implantremovereason,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_implantremovername", "implantremovername,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_isfixedvisit", "isfixedvisit,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_management", "management,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_monitoringofficername", "monitoringofficername,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_providerasattendant", "providerasattendant,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_providerid", "providerid,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_refer", "refer,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_refercentername", "refercentername,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_referreason", "referreason,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_serviceid", "serviceid,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_treatment", "treatment,text," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_upload", "upload,integer," + prop.getProperty("table_implantfollowupservice"));
            prop.setProperty("implantfollowupservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_implantfollowupservice"));

            //implantservice
            prop.setProperty("implantservice_attendantallowance", "attendantallowance,real," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_attendantdesignation", "attendantdesignation,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_attendantname", "attendantname,text," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_client", "client,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_clientallowance", "clientallowance,real," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_healthid", "healthid,bigint," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implantafterdelivery", "implantafterdelivery,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implantcount", "implantcount,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implantimplantdate", "implantimplantdate,date," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_implanttype", "implanttype,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_providerasattendant", "providerasattendant,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_providerid", "providerid,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_servicesource", "servicesource,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_treatment", "treatment,text," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_upload", "upload,integer," + prop.getProperty("table_implantservice"));
            prop.setProperty("implantservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_implantservice"));

            //iudfollowupservice
            prop.setProperty("iudfollowupservice_allowance", "allowance,real," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_attendantname", "attendantname,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_client", "client,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_comment", "comment,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_complication", "complication,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_followupdate", "followupdate,date," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_fpamount", "fpamount,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_fpmethod", "fpmethod,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_healthid", "healthid,bigint," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_isfixedvisit", "isfixedvisit,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudcount", "iudcount,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremovedate", "iudremovedate,date," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremoverdesignation", "iudremoverdesignation,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremovereason", "iudremovereason,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_iudremovername", "iudremovername,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_management", "management,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_monitoringofficername", "monitoringofficername,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_providerasattendant", "providerasattendant,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_providerid", "providerid,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_refer", "refer,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_refercentername", "refercentername,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_referreason", "referreason,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_serviceid", "serviceid,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_treatment", "treatment,text," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_upload", "upload,integer," + prop.getProperty("table_iudfollowupservice"));
            prop.setProperty("iudfollowupservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_iudfollowupservice"));

            //iudservice
            prop.setProperty("iudservice_attendantallowance", "attendantallowance,real," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_attendantdesignation", "attendantdesignation,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_attendantname", "attendantname,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_client", "client,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_clientallowance", "clientallowance,real," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_companionaddress", "companionaddress,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_companionallowance", "companionallowance,real," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_companionname", "companionname,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_healthid", "healthid,bigint," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudafterdelivery", "iudafterdelivery,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudcount", "iudcount,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudimplantdate", "iudimplantdate,date," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_iudtype", "iudtype,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_lmp", "lmp,date," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_providerasattendant", "providerasattendant,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_providerid", "providerid,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_servicesource", "servicesource,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_treatment", "treatment,text," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_upload", "upload,integer," + prop.getProperty("table_iudservice"));
            prop.setProperty("iudservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_iudservice"));

            //womaninjectable
            prop.setProperty("womaninjectable_bpdiastolic", "bpdiastolic,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_bpsystolic", "bpsystolic,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_breastmass", "breastmass,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_client", "client,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_dosedate", "dosedate,date,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_doseid", "doseid,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_healthid", "healthid,bigint,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_injectionid", "injectionid,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_isnewclient", "isnewclient,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_lmp", "lmp,date,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_providerid", "providerid,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_pulse", "pulse,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_pvc", "pvc,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_satelitecentername", "satelitecentername,text,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_sideeffect", "sideeffect,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_upload", "upload,integer,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_womaninjectable"));
            prop.setProperty("womaninjectable_weight", "weight,real,"+prop.getProperty("table_womaninjectable"));

            //last_login_detail
            prop.setProperty("last_login_detail_login_request_detail", "login_request_detail,text," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_login_success", "login_success,boolean," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_monitoring_tool", "monitoring_tool,integer," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_login_time", "login_time,timestamp without time zone," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_provider_id", "providerid,integer," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_upload", "upload,integer," + prop.getProperty("table_last_login_detail"));
            prop.setProperty("last_login_detail_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_last_login_detail"));

            //mis3_report_submission
            prop.setProperty("mis3_report_submission_approval_date", "approval_date,timestamp without time zone," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_facility_id", "facility_id,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_report_month", "report_month,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_report_year", "report_year,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_submission_date", "submission_date,timestamp without time zone," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_submission_status", "submission_status,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_submitted_by", "submitted_by,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_supervise_comment", "supervise_comment,text," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_supervisor_comment", "supervisor_comment,text," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_upload", "upload,integer," + prop.getProperty("table_mis3_report_submission"));
            prop.setProperty("mis3_report_submission_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_mis3_report_submission"));

            //mis3_report_submission_data
            prop.setProperty("mis3_report_submission_data_facility_id", "facility_id,integer,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_field_name", "field_name,character varying,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_field_value", "field_value,integer,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_report_month", "report_month,integer,"+prop.getProperty("table_mis3_report_submission_data"));
            prop.setProperty("mis3_report_submission_data_report_year", "report_year,integer,"+prop.getProperty("table_mis3_report_submission_data"));

            //mis3_approver
            prop.setProperty("mis3_approver_providerid", "providerid,integer,"+prop.getProperty("table_mis3_approver"));
            prop.setProperty("mis3_approver_active", "active,integer,"+prop.getProperty("table_mis3_approver"));
            prop.setProperty("mis3_approver_start_date", "start_date,timestamp without time zone,"+prop.getProperty("table_mis3_approver"));
            prop.setProperty("mis3_approver_end_date", "end_date,timestamp without time zone,"+prop.getProperty("table_mis3_approver"));
            prop.setProperty("mis3_approver_approval_zilla", "approval_zilla,integer,"+prop.getProperty("table_mis3_approver"));
            prop.setProperty("mis3_approver_approval_upazilas", "approval_upazilas,text,"+prop.getProperty("table_mis3_approver"));

            //newborn
            prop.setProperty("newborn_attendantdesignation", "attendantdesignation,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_bagnmask", "bagnmask,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_baththreedays", "baththreedays,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_birthstatus", "birthstatus,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_birthweight", "birthweight,real," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_birthweightstatus", "birthweightstatus,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_breastfeed", "breastfeed,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_childhealthid", "childhealthid,bigint," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_childno", "childno,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_chlorehexidin", "chlorehexidin,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_client", "client,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_dryingafterbirth", "dryingafterbirth,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_gender", "gender,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_healthid", "healthid,bigint," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_immaturebirth", "immaturebirth,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_livingstatus", "livingstatus,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcomedate", "outcomedate,date," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcomeplace", "outcomeplace,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcometime", "outcometime,time without time zone," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_outcometype", "outcometype,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_pregno", "pregno,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_providerid", "providerid,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_refer", "refer,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_refercentername", "refercentername,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_referreason", "referreason,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_resassitation", "resassitation,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_satelitecentername", "satelitecentername,text," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_skintouch", "skintouch,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_stimulation", "stimulation,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_upload", "upload,integer," + prop.getProperty("table_newborn"));
            prop.setProperty("newborn_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_newborn"));

            //node_details
            prop.setProperty("node_details_base_url", "base_url,character varying,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_changerequest", "changerequest,integer,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_client", "client,integer,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_dbdownloadstatus", "dbdownloadstatus,integer,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_dbsyncrequest", "dbsyncrequest,integer,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_id", "id,character varying,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_provcode", "providerid,integer,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_provtype", "provtype,integer,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_server", "server,character varying,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_sync_url", "sync_url,character varying,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_upload", "upload,integer,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_version", "version,character varying,"+prop.getProperty("table_node_details"));
            prop.setProperty("node_details_community_active", "community_active,integer,"+prop.getProperty("table_node_details"));

            //pacservice
            prop.setProperty("pacservice_abdomen", "abdomen,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_advice", "advice,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_anemia", "anemia,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_bpdiastolic", "bpdiastolic,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_bpsystolic", "bpsystolic,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_cervix", "cervix,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_client", "client,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_complicationsign", "complicationsign,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_disease", "disease,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_fpmethod", "fpmethod,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_healthid", "healthid,bigint," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_hematuria", "hematuria,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_perineum", "perineum,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_pregno", "pregno,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_providerid", "providerid,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_refer", "refer,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_refercentername", "refercentername,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_referreason", "referreason,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_serviceid", "serviceid,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_servicesource", "servicesource,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_symptom", "symptom,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_temperature", "temperature,real," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_treatment", "treatment,text," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_upload", "upload,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_uterusinvolution", "uterusinvolution,integer," + prop.getProperty("table_pacservice"));
            prop.setProperty("pacservice_visitdate", "visitdate,date," + prop.getProperty("table_pacservice"));

            //pillcondomservice
            prop.setProperty("pillcondomservice_amount", "amount,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_client", "client,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_healthid", "healthid,bigint," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_isnewclient", "isnewclient,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_methodtype", "methodtype,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_providerid", "providerid,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_satelitecentername", "satelitecentername,text," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_serviceid", "serviceid,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_transactionid", "transactionid,bigint," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_upload", "upload,integer," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_pillcondomservice"));
            prop.setProperty("pillcondomservice_visitdate", "visitdate,date," + prop.getProperty("table_pillcondomservice"));

            //pncservicechild
            prop.setProperty("pncservicechild_advice", "advice,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_breastfeedingonly", "breastfeedingonly,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_breathingperminute", "breathingperminute,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_childhealthid", "childhealthid,bigint," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_childno", "childno,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_client", "client,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_dangersign", "dangersign,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_disease", "disease,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_healthid", "healthid,bigint," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_pregno", "pregno,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_providerid", "providerid,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_refer", "refer,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_refercentername", "refercentername,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_referreason", "referreason,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_satelitecentername", "satelitecentername,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_serviceid", "serviceid,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_servicesource", "servicesource,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_symptom", "symptom,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_temperature", "temperature,real," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_treatment", "treatment,text," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_upload", "upload,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_visitdate", "visitdate,date," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_visitmonth", "visitmonth,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_visitsource", "visitsource,integer," + prop.getProperty("table_pncservicechild"));
            prop.setProperty("pncservicechild_weight", "weight,real," + prop.getProperty("table_pncservicechild"));

            //pncservicemother
            prop.setProperty("pncservicemother_advice", "advice,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_anemia", "anemia,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_bpdiastolic", "bpdiastolic,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_bpsystolic", "bpsystolic,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_breastcondition", "breastcondition,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_client", "client,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_complicationsign", "complicationsign,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_disease", "disease,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_edema", "edema,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_fpmethod", "fpmethod,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_healthid", "healthid,bigint," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_hematuria", "hematuria,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_hemoglobin", "hemoglobin,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_perineum", "perineum,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_pregno", "pregno,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_providerid", "providerid,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_refer", "refer,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_refercentername", "refercentername,character varying," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_referreason", "referreason,character varying," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_satelitecentername", "satelitecentername,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_serviceid", "serviceid,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_servicesource", "servicesource,character varying," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_symptom", "symptom,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_temperature", "temperature,double precision," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_treatment", "treatment,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_upload", "upload,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_uterusinvolution", "uterusinvolution,text," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_visitdate", "visitdate,date," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_visitmonth", "visitmonth,integer," + prop.getProperty("table_pncservicemother"));
            prop.setProperty("pncservicemother_visitsource", "visitsource,integer," + prop.getProperty("table_pncservicemother"));

            //pregwomen
            prop.setProperty("pregwomen_bloodgroup", "bloodgroup,text," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_client", "client,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_edd", "edd,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_gravida", "gravida,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_healthid", "healthid,bigint," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_height", "height,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_housegrholdingno", "housegrholdingno,character varying," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_lastchildage", "lastchildage,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_lmp", "lmp,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_mobileno", "mobileno,bigint," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_para", "para,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_pregno", "pregno,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_providerid", "providerid,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_riskhistory", "riskhistory,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_riskhistorynote", "riskhistorynote,text," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_satelitecentername", "satelitecentername,text," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_statuslmp", "statuslmp,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_templmp", "templmp,date," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_upload", "upload,integer," + prop.getProperty("table_pregwomen"));
            prop.setProperty("pregwomen_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_pregwomen"));

            //providerdb
            prop.setProperty("providerdb_active", "active,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_areaupdate", "areaupdate,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_csba", "csba,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_devicesetting", "devicesetting,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_divid", "divid,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_email", "email,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_endate", "endate,timestamp with time zone," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_exdate", "exdate,timestamp with time zone," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_facilityname", "facilityname,text," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_healthidrequest", "healthidrequest,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_mobileno", "mobileno,bigint," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provcode", "providerid,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provname", "provname,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provpass", "provpass,text," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_provtype", "provtype,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_supervisorcode", "supervisorcode,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_systemupdatedt", "systemupdatedt,character varying," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_tablestructurerequest", "tablestructurerequest,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_unionid", "unionid,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_upazilaid", "upazilaid,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_upload", "upload,integer," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_providerdb"));
            prop.setProperty("providerdb_zillaid", "zillaid,integer," + prop.getProperty("table_providerdb"));

            //providertype
            prop.setProperty("providertype_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_providertype"));
            prop.setProperty("providertype_provtype", "provtype,integer," + prop.getProperty("table_providertype"));
            prop.setProperty("providertype_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_providertype"));
            prop.setProperty("providertype_typename", "typename,character varying," + prop.getProperty("table_providertype"));
            prop.setProperty("providertype_upload", "upload,integer," + prop.getProperty("table_providertype"));
            prop.setProperty("providertype_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_providertype"));

            //providerarea
            prop.setProperty("providerarea_areacode", "areacode,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_block", "block,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_divid", "divid,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_fwa_round", "fwa_round,numeric,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_fwaunit", "fwaunit,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_mouzaid", "mouzaid,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_provcode", "providerid,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_provtype", "provtype,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_starthhno", "starthhno,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_unionid", "unionid,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_upazilaid", "upazilaid,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_upload", "upload,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_villageid", "villageid,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_ward", "ward,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_wardnew", "wardnew,integer,"+prop.getProperty("table_providerarea"));
            prop.setProperty("providerarea_zillaid", "zillaid,integer,"+prop.getProperty("table_providerarea"));

            //provider_leave_status
            prop.setProperty("provider_leave_status_providerid", "providerid,integer," + prop.getProperty("table_provider_leave_status"));
            prop.setProperty("provider_leave_status_leave_type", "leave_type,integer," + prop.getProperty("table_provider_leave_status"));
            prop.setProperty("provider_leave_status_leave_start_date", "leave_start_date,date," + prop.getProperty("table_provider_leave_status"));
            prop.setProperty("provider_leave_status_leave_end_date", "leave_end_date,date," + prop.getProperty("table_provider_leave_status"));
            prop.setProperty("provider_leave_status_leave_comment", "leave_comment,text," + prop.getProperty("table_provider_leave_status"));

            //zilla
            prop.setProperty("zilla_divid", "divid,integer," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_upload", "upload,integer," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_zillaid", "zillaid,integer," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_zillaname", "zillaname,character varying," + prop.getProperty("table_zilla"));
            prop.setProperty("zilla_zillanameeng", "zillanameeng,character varying," + prop.getProperty("table_zilla"));

            //unions
            prop.setProperty("unions_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_unions"));
            prop.setProperty("unions_municipalityid", "municipalityid,integer," + prop.getProperty("table_unions"));
            prop.setProperty("unions_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_unions"));
            prop.setProperty("unions_unionid", "unionid,integer," + prop.getProperty("table_unions"));
            prop.setProperty("unions_unionname", "unionname,character varying," + prop.getProperty("table_unions"));
            prop.setProperty("unions_unionnameeng", "unionnameeng,character varying," + prop.getProperty("table_unions"));
            prop.setProperty("unions_upazilaid", "upazilaid,integer," + prop.getProperty("table_unions"));
            prop.setProperty("unions_upload", "upload,integer," + prop.getProperty("table_unions"));
            prop.setProperty("unions_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_unions"));
            prop.setProperty("unions_zillaid", "zillaid,integer," + prop.getProperty("table_unions"));

            //upazila
            prop.setProperty("upazila_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_upazilaid", "upazilaid,integer," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_upazilaname", "upazilaname,character varying," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_upazilanameeng", "upazilanameeng,character varying," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_upload", "upload,integer," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_upazila"));
            prop.setProperty("upazila_zillaid", "zillaid,integer," + prop.getProperty("table_upazila"));

            //village
            prop.setProperty("village_close", "close,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_crrvillagename", "crrvillagename,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_fwaunit", "fwaunit,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_mouzaid", "mouzaid,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_rmo", "rmo,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_unionid", "unionid,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_upazilaid", "upazilaid,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_villageid", "villageid,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_villagename", "villagename,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_villagenameeng", "villagenameeng,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_ward", "ward,character varying," + prop.getProperty("table_village"));
            prop.setProperty("village_zillaid", "zillaid,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_village"));
            prop.setProperty("village_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_village"));
            prop.setProperty("village_upload", "upload,integer," + prop.getProperty("table_village"));
            prop.setProperty("village_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_village"));


            //satelite_planning_master
            prop.setProperty("satelite_session_planning_approve_date", "approve_date,timestamp without time zone,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_comments", "comments,text,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_modify_date", "modify_date,timestamp without time zone,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_providerid", "providerid,bigint,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_planning_id", "planning_id,bigint,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_facility_id", "facility_id,bigint,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_year", "year,integer,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_month", "month,integer,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_status", "status,integer,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_zillaid", "zillaid,integer,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_upazilaid", "upazilaid,integer,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_unionid", "unionid,integer,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_submit_date", "submit_date,timestamp without time zone,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_system_entry_date", "system_entry_date,timestamp without time zone,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_upload", "upload,integer,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_other1", "other1,text,"+prop.getProperty("table_satelite_session_planning"));
            prop.setProperty("satelite_session_planning_other2", "other2,text,"+prop.getProperty("table_satelite_session_planning"));

            //satelite_planning_details(old)
//            prop.setProperty("satelite_planning_details_fwa_name", "fwa_name,character varying,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_modify_date", "modify_date,timestamp without time zone,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_satelite_actual_date", "satelite_actual_date,timestamp without time zone,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_satelite_id", "satelite_id,integer,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_satelite_name", "satelite_name,character varying,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_satelite_planning_id", "satelite_planning_id,integer,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_satelite_scheduled_date", "satelite_scheduled_date,date,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_system_entry_date", "system_entry_date,timestamp without time zone,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_unplaned_satelite_date", "unplaned_satelite_date,timestamp without time zone,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_upload", "upload,integer,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_satelite_planning_details"));
//            prop.setProperty("satelite_planning_details_village_name", "village_name,character varying,"+prop.getProperty("table_satelite_planning_details"));
            


            //satelite_planning_details(new)
            prop.setProperty("satelite_planning_detail_fwa_name", "fwa_name,character varying,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_fwa_id", "fwa_id,integer,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_mouzaid", "mouzaid,integer,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_villageid", "villageid,integer,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_planning_detail_id", "planning_detail_id,integer,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_planning_id", "planning_id,bigint,"+prop.getProperty("table_satelite_planning_detail"));

            prop.setProperty("satelite_planning_detail_satelite_name", "satelite_name,character varying,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_schedule_date", "schedule_date,date,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_system_entry_date", "system_entry_date,timestamp without time zone,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_modify_date", "modify_date,timestamp without time zone,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_upload", "upload,integer,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_other1", "other1,text,"+prop.getProperty("table_satelite_planning_detail"));
            prop.setProperty("satelite_planning_detail_other2", "other2,text,"+prop.getProperty("table_satelite_planning_detail"));

            //users
            prop.setProperty("users_provcode", "providerid,text," + prop.getProperty("table_users"));
            prop.setProperty("users_provname", "provname,character varying," + prop.getProperty("table_users"));
            prop.setProperty("users_provtype", "provtype,integer," + prop.getProperty("table_users"));
            prop.setProperty("users_provpass", "provpass,text," + prop.getProperty("table_users"));

            //immunizationHistory
            prop.setProperty("immunizationhistory_healthid", "healthid,bigint," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imucard", "imucard,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imucode", "imucode,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imudate", "imudate,date," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imudose", "imudose,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_imusource", "imusource,character varying," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_modifydate", "modifydate,date," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_providerid", "providerid,integer," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_systementrydate", "systementrydate,date," + prop.getProperty("table_immunizationhistory"));
            prop.setProperty("immunizationhistory_upload", "upload,integer," + prop.getProperty("table_immunizationhistory"));

            //elco
            prop.setProperty("elco_dau", "dau,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_domsource", "domsource,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_elcono", "elcono,text," + prop.getProperty("table_elco"));
            prop.setProperty("elco_hahhno", "hahhno,text," + prop.getProperty("table_elco"));
            prop.setProperty("elco_healthid", "healthid,bigint," + prop.getProperty("table_elco"));
            prop.setProperty("elco_hhstatus", "hhstatus,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_husbandage", "husbandage,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_husbandname", "husbandname,text," + prop.getProperty("table_elco"));
            prop.setProperty("elco_marrage", "marrage,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_marrdate", "marrdate,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_modifydate", "modifydate,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_providerid", "providerid,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_regdt", "regdt,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_son", "son,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_systementrydate", "systementrydate,date," + prop.getProperty("table_elco"));
            prop.setProperty("elco_upload", "upload,integer," + prop.getProperty("table_elco"));
            prop.setProperty("elco_uploaddate", "uploaddate,date," + prop.getProperty("table_elco"));

            //regSerial
            prop.setProperty("regserial_facilityid", "facilityid,integer," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_healthid", "healthid,bigint," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_modifydate", "modifydate,date," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_providerid", "providerid,integer," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_serialno", "serialno,bigint," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_servicecategory", "servicecategory,integer," + prop.getProperty("table_regserial"));
            prop.setProperty("regserial_systementrydate", "systementrydate,date," + prop.getProperty("table_regserial"));

            //deathReason
            prop.setProperty("deathreason_code", "code,text," + prop.getProperty("table_deathreason"));
            prop.setProperty("deathreason_deathreasonid", "deathreasonid,integer," + prop.getProperty("table_deathreason"));
            prop.setProperty("deathreason_detail", "detail,text," + prop.getProperty("table_deathreason"));

            //diseaselist
            prop.setProperty("diseaselist_detail", "detail,text," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_id", "id,integer," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_serviceid", "serviceid,bigint," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_upload", "upload,integer," + prop.getProperty("table_diseaselist"));
            prop.setProperty("diseaselist_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_diseaselist"));

            //diseaselist en
            prop.setProperty("disease_list_en_detail", "detail,text," + prop.getProperty("table_disease_list_en"));
            prop.setProperty("disease_list_en_id", "id,integer," + prop.getProperty("table_disease_list_en"));
            prop.setProperty("disease_list_en_serviceid", "serviceId,integer," + prop.getProperty("table_disease_list_en"));

            //fwaunit
            prop.setProperty("fwaunit_modifydate", "modifydate,timestamp without time zone," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_systementrydate", "systementrydate,timestamp without time zone," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_ucode", "ucode,integer," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_uname", "uname,character varying," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_unameban", "unameban,character varying," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_upload", "upload,integer," + prop.getProperty("table_fwaunit"));
            prop.setProperty("fwaunit_uploaddate", "uploaddate,timestamp without time zone," + prop.getProperty("table_fwaunit"));

            //associated_provider_id
            prop.setProperty("associated_provider_id_associated_id", "associated_id,integer,"+prop.getProperty("table_associated_provider_id"));
            prop.setProperty("associated_provider_id_provider_id", "provider_id,integer,"+prop.getProperty("table_associated_provider_id"));

            //FPExamination
            prop.setProperty("fpexamination_healthid", "healthid,bigint,"+prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_serviceid", "serviceid,integer,"+prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_fptype", "fptype,integer,"+prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_bpdiastolic", "bpdiastolic,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_bpsystolic", "bpsystolic,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_jaundice", "jaundice,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_diabetes", "diabetes,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_breastcondition", "breastcondition,integer," + prop.getProperty("table_fpexamination"));
            prop.setProperty("fpexamination_lmp", "lmp,date," + prop.getProperty("table_fpexamination"));

            //PermanentMethodService
            prop.setProperty("permanent_method_service_advice", "advice,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_client", "client,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_client_allowance", "client_allowance,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_client_came_alone", "client_came_alone,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_cloth", "cloth,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_address", "companion_address,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_allowance", "companion_allowance,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_designation", "companion_designation,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_mobile", "companion_mobile,bigint,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_companion_name", "companion_name,character varying,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_complication", "complication,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_doctor_designation", "doctor_designation,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_doctor_name", "doctor_name,character varying,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_healthid", "healthid,bigint,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_management", "management,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_operation_name", "operation_name,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_permanent_method_operation_date", "permanent_method_operation_date,date,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_permanent_method_operation_time", "permanent_method_operation_time,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_pmscount", "pmscount,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_providerid", "providerid,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_refer", "refer,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_refer_center_name", "refer_center_name,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_refer_reason", "refer_reason,text,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_treatment", "treatment,character varying,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_upload", "upload,integer,"+prop.getProperty("table_permanent_method_service"));
            prop.setProperty("permanent_method_service_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_permanent_method_service"));

            //PermanentMethodFollowupService
            prop.setProperty("permanent_method_followup_service_complication", "complication,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_doctor_designation", "doctor_designation,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_doctor_name", "doctor_name,character varying,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_examination", "examination,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_followup_date", "followup_date,date,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_followup_reason", "followup_reason,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_healthid", "healthid,bigint,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_management", "management,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_pmscount", "pmscount,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_providerid", "providerid,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_refer", "refer,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_refer_center_name", "refer_center_name,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_refer_reason", "refer_reason,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_serviceid", "serviceid,bigint,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_treatment", "treatment,text,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_upload", "upload,integer,"+prop.getProperty("table_permanent_method_followup_service"));
            prop.setProperty("permanent_method_followup_service_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_permanent_method_followup_service"));

            //child_care_service
            prop.setProperty("child_care_service_healthid", "healthid,bigint,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_providerid", "providerid,bigint,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_visitdate", "visitdate,date,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_indicationstartdate", "indicationstartdate,date,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_isfollowup", "isfollowup,integer,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_refer", "refer,integer,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_satelitecentername", "satelitecentername,text,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_client", "client,integer,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_comment", "comment,text,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_other1", "other1,text,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_other2", "other2,text,"+prop.getProperty("table_child_care_service"));
            prop.setProperty("child_care_service_advice", "advice,text,"+prop.getProperty("table_child_care_service"));

            //child_care_service_detail
            prop.setProperty("child_care_service_detail_healthid", "healthid,bigint,"+prop.getProperty("table_child_care_service_detail"));
            prop.setProperty("child_care_service_detail_entrydate", "entrydate,timestamp without time zone,"+prop.getProperty("table_child_care_service_detail"));
            prop.setProperty("child_care_service_detail_inputid", "inputid,integer,"+prop.getProperty("table_child_care_service_detail"));
            prop.setProperty("child_care_service_detail_inputvalue", "inputvalue,text,"+prop.getProperty("table_child_care_service_detail"));
            prop.setProperty("child_care_service_detail_comment", "comment,text,"+prop.getProperty("table_child_care_service_detail"));
            prop.setProperty("child_care_service_detail_systementrydate", "systementrydate,timestamp without time zone,"+prop.getProperty("table_child_care_service_detail"));
            prop.setProperty("child_care_service_detail_modifydate", "modifydate,timestamp without time zone,"+prop.getProperty("table_child_care_service_detail"));
            prop.setProperty("child_care_service_detail_uploaddate", "uploaddate,timestamp without time zone,"+prop.getProperty("table_child_care_service_detail"));


        }
        catch (Exception e){
            e.printStackTrace();
        }

        return prop;
    }
}
