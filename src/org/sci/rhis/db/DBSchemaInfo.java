package org.sci.rhis.db;

import java.util.Properties;

public class DBSchemaInfo {
    private Properties prop = new Properties();

    public DBSchemaInfo(String zilla){
        prop = null;
        try {
            DBProperties setProperties = new DBProperties();
            DBPropertiesNew setPropertiesNew = new DBPropertiesNew();

//            if (zilla.equals("69") || zilla.equals("36") || zilla.equals("42") || zilla.equals("51") || zilla.equals("75") || zilla.equals("93"))
                prop = setPropertiesNew.setProperties();
//            else
//                prop = setProperties.setProperties();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public DBSchemaInfo(){
        prop = null;
        try {
            DBPropertiesNew setPropertiesNew = new DBPropertiesNew();

            prop = setPropertiesNew.setProperties();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public String getTable(String field){
        String tableName = "\"" + prop.getProperty(field) + "\"";
        return tableName;
    }

    public String getColumn(String alias, String field){
        String columnName = "";
        try {
            String[] fieldDetail = prop.getProperty(field).split(",");
            switch (alias) {
                case "table":
                    columnName = "\"" + fieldDetail[2] + "\"" + ".\"" + fieldDetail[0] + "\"";
                    break;
                case "":
                    columnName = "\"" + fieldDetail[0] + "\"";
                    break;
                default:
                    columnName = alias + ".\"" + fieldDetail[0] + "\"";
                    break;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return columnName;
    }

    public String getColumn(String alias, String field, String[] value, String condition){
        String columnName = "";
        try {
            String[] fieldDetail = prop.getProperty(field).split(",");
            switch(condition){
                case "=":
                    columnName = getColumn(alias, field) + " = " + getValue(fieldDetail[1],value[0]);
                    break;
                case "!=":
                    columnName = getColumn(alias, field) + " != " + getValue(fieldDetail[1],value[0]);
                    break;
                case ">":
                    columnName = getColumn(alias, field) + " > " + getValue(fieldDetail[1],value[0]);
                    break;
                case ">=":
                    columnName = getColumn(alias, field) + " >= " + getValue(fieldDetail[1],value[0]);
                    break;
                case "<":
                    columnName = getColumn(alias, field) + " < " + getValue(fieldDetail[1],value[0]);
                    break;
                case "<=":
                    columnName = getColumn(alias, field) + " <= " + getValue(fieldDetail[1],value[0]);
                    break;
                case "<>":
                    columnName = getColumn(alias, field) + " <> " + getValue(fieldDetail[1],value[0]);
                    break;
                case "isnull":
                    columnName = getColumn(alias, field) + " IS NULL";
                    break;
                case "isnotnull":
                    columnName = getColumn(alias, field) + " IS NOT NULL";
                    break;
                case "between":
                    columnName = getColumn(alias, field) + " BETWEEN " + getValue(fieldDetail[1],value[0]) + " AND " + getValue(fieldDetail[1],value[1]);
                    break;
                case "in":
                    columnName = getColumn(alias, field) + " IN (";
                    for(String eachVal : value){
                        columnName = columnName + getValue(fieldDetail[1],eachVal) + ",";
                    }
                    columnName = columnName.substring(0, columnName.length()-1) + ")";
                    break;
                case "notin":
                    columnName = getColumn(alias, field) + " NOT IN (";
                    for(String eachVal : value){
                        columnName = columnName + getValue(fieldDetail[1],eachVal) + ",";
                    }
                    columnName = columnName.substring(0, columnName.length()-1) + ")";
                    break;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return columnName;
    }

    public String getValue(String type, String value){
        String val = "";
        try {
            val = (type.equals("string") || type.equals("character varying") || type.equals("text") || type.equals("timestamp with time zone") || type.equals("timestamp without time zone") || type.equals("date")) ? ("'" + value + "'") : value;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return val;
    }
}
