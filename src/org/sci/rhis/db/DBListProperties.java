package org.sci.rhis.db;

import java.util.Properties;

public class DBListProperties {
    public static Properties prop = new Properties();

    public Properties setProperties(){
        try {
            //zilla list
            prop.setProperty("zilla", "12,13,27,29,30,36,42,44,46,49,50,51,54,56,58,69,75,85,91,93,94");
            //prop.setProperty("zilla", "12,13,29,30,36,42,50,51,56,75,91");
            //prop.setProperty("zilla", "12,13,29,30,36,42,50,51,56,75");
            prop.setProperty("zilla_all", "3,4,9,12,13,15,19,22,27,29,30,32,35,36,42,44,46,47,48,49,50,51,52,54,56,58,65,69,73,75,77,84,85,90,91,93,94");
            //prop.setProperty("zilla_all", "12,13,19,27,29,30,36,42,44,49,50,51,54,56,58,69,75,85");
            //prop.setProperty("zilla_all", "12,13,19,27,29,30,36,42,44,49,50,51,56,69,75,85");

            //div list
            prop.setProperty("div", "10,20,30,40,50,55,60,70");

            //div-zilla mapping
            prop.setProperty("div_10", "4,9,42");
            prop.setProperty("div_20", "3,12,13,15,19,22,30,46,51,75,84,15");
            prop.setProperty("div_30", "29,35,48,54,56,93");
            prop.setProperty("div_40", "44,47,50,65");
            prop.setProperty("div_50", "69");
            prop.setProperty("div_55", "27,32,49,52,85,94,73,77");
            prop.setProperty("div_60", "36,58,90,91");

            //zilla-upazila mapping
            prop.setProperty("3","4,14,51,73,89,91,95"); //bandarban
            prop.setProperty("4","9,19,28,47,85"); //barguna
            prop.setProperty("9","18,21,25,29,54,65,91");
            prop.setProperty("12","2,4,7,13,33,63,85,90,94");
            prop.setProperty("13","22,45,47,49,58,76,79,95");
            prop.setProperty("15","4,6,8,10,12,18,19,20,28,33,35,37,39,41,43,47,53,55,57,61,65,70,74,78,82,86");
            prop.setProperty("19","9,15,18,27,31,33,36,40,54,67,72,74,75,81,87,94");
            prop.setProperty("22","16,24,45,49,56,66,90,94");
            prop.setProperty("27","10,12,17,21,30,38,43,47,56,60,64,69,77");
            prop.setProperty("29","3,10,18,21,47,56,62,84,90");
            prop.setProperty("30","14,25,29,41,51,94");
            prop.setProperty("32","21,24,30,67,82,88,91"); //gaibanda
            prop.setProperty("35","32,43,51,58,91"); // gopalganj
            prop.setProperty("36","2,5,11,26,44,68,71,77");
            prop.setProperty("42","40,43,73,84");
            prop.setProperty("44","14,19,33,42,71,80");
            prop.setProperty("46","43,49,61,65,67,70,77,80");
            prop.setProperty("47","12,17,21,30,40,45,48,51,53,64,69,75,85,94");
            prop.setProperty("48","2,6,11,27,33,42,45,49,54,59,76,79,92");
            prop.setProperty("49","6,8,9,18,52,61,77,79,94");
            prop.setProperty("50","15,39,63,71,79,94");
            prop.setProperty("51","33,43,58,65,73");
            prop.setProperty("52","2,33,39,55,70");
            prop.setProperty("54","40,54,80,87");
            prop.setProperty("56","10,22,28,46,70,78,82");
            prop.setProperty("58","14,35,56,65,74,80,83");
            prop.setProperty("65","28,52,76");
            prop.setProperty("69","9,15,41,44,55,63,91");
            prop.setProperty("73","12,15,36,45,64,85");
            prop.setProperty("75","7,10,21,36,47,80,83,85,87");
            prop.setProperty("77","4,25,34,73,90");
            prop.setProperty("84","47,7,21,25,29,36,58,75,78,87"); //Rangamati
            prop.setProperty("85","3,27,42,49,58,73,76,92");
            prop.setProperty("90","18,23,27,29,32,33,47,50,86,89,92");
            prop.setProperty("91","8,17,20,27,31,35,38,41,53,59,62,94");
            prop.setProperty("93","9,19,23,25,28,38,47,57,66,76,85,95");
            prop.setProperty("94","8,51,82,86,94");
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return prop;
    }
}
