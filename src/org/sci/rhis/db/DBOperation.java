package org.sci.rhis.db;

//import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.Enumeration;

/**
 * @author sabah.mugab
 * @created June, 2015
 */
public class DBOperation {
	
	public DBInfoHandler dbConnect(DBInfoHandler getDBInfo){
		
		try {
			Class.forName(getDBInfo.getConnCls());
			getDBInfo.setConnection( DriverManager.getConnection(
			            getDBInfo.getHost()+getDBInfo.getDBName(),
			            getDBInfo.getUserName(),
			            getDBInfo.getUserPassword()));
		}
		catch (ClassNotFoundException | SQLException e) {
			System.out.println("connection error");
			e.printStackTrace();
		}
		return getDBInfo;
	}	
	
	public DBInfoHandler dbCreateStatement(DBInfoHandler getDBInfo){
		
		try {
			getDBInfo.setStatement(dbConnect(getDBInfo).getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY,ResultSet.HOLD_CURSORS_OVER_COMMIT));
		} 
		catch (SQLException e) {
			System.out.println("Create statement error");
			e.printStackTrace();
		}
		return getDBInfo;
	}
		
	public DBInfoHandler dbCreatePreparedStatement(DBInfoHandler getDBInfo, String sql){
		
		try {
			getDBInfo.setPreparedStatement(dbConnect(getDBInfo).getConnection().prepareStatement(sql));
		} 
		catch (SQLException e) {
			System.out.println("Prepared statement error");
			e.printStackTrace();
		}
		return getDBInfo;
	}
	
	public DBInfoHandler dbExecute(DBInfoHandler getDBInfo){
		
		try {
			getDBInfo.setResultSet(getDBInfo.getPreparedStatement().executeQuery());					
		} 
		catch (SQLException e) {
			System.out.println("Resultset error");
			e.printStackTrace();
		}
		return getDBInfo;
	}
	
	public DBInfoHandler dbExecute(DBInfoHandler getDBInfo, String sql){
	
		try {
			if(getDBInfo.getStatement()!=null){
				getDBInfo.setResultSet(getDBInfo.getStatement().executeQuery(sql));
			}
			else{
				this.dbCreateStatement(getDBInfo);
				getDBInfo.setResultSet(getDBInfo.getStatement().executeQuery(sql));
			}			
		} 
		catch (SQLException e) {
			System.out.println("Statement error");
			e.printStackTrace();
		}
		return getDBInfo;
	}
	
	public boolean dbStatementExecute(DBInfoHandler getDBInfo, String sql){
		boolean status = false;	
		try {
			if(getDBInfo.getStatement()!=null){
				status = getDBInfo.getStatement().execute(sql);
			}
			else{
				this.dbCreateStatement(getDBInfo);
				status = getDBInfo.getStatement().execute(sql);				
			}			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public Integer dbExecuteUpdate(DBInfoHandler getDBInfo, String sql){
		int status = 0;	
		try {
			if(getDBInfo.getStatement()!=null){
				status = getDBInfo.getStatement().executeUpdate(sql);
			}
			else{
				this.dbCreateStatement(getDBInfo);
				status = getDBInfo.getStatement().executeUpdate(sql);
			}			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public void closeConn(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getConnection() != null){
				if(!getDBInfo.getConnection().isClosed()){
					getDBInfo.getConnection().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		/*finally{
			// This manually deregisters JDBC driver, which prevents Tomcat 7 and upward from complaining about memory leaks
	        Enumeration<Driver> drivers = DriverManager.getDrivers();
	        while (drivers.hasMoreElements()){
	            Driver driver = drivers.nextElement();
	            try{
	                DriverManager.deregisterDriver(driver);	                
	            } 
	            catch (SQLException e){
	            	e.printStackTrace();	                
	            }
	        }
		}*/
	}
	
	public void closeStatement(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getStatement() != null){
				if(!getDBInfo.getStatement().isClosed()){
					getDBInfo.getStatement().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void closePreparedStatement(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getPreparedStatement() != null){
				if(!getDBInfo.getPreparedStatement().isClosed()){
					getDBInfo.getPreparedStatement().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void closeResultSet(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getResultSet() != null){
				if(!getDBInfo.getResultSet().isClosed()){
					getDBInfo.getResultSet().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void closeDBInfoHandler(DBOperation dbOperation,DBInfoHandler dbInfoHandler){
		dbOperation.closeResultSet(dbInfoHandler);
		dbOperation.closePreparedStatement(dbInfoHandler);
		dbOperation.closeConn(dbInfoHandler);
	}
}
