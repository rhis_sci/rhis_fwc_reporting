package org.sci.rhis.db;

import org.sci.rhis.util.AES;

import java.util.Properties;

/**
 * @author sabah.mugab
 * @created June, 2015
 */
public class FacilityDB{

	DBInfoHandler detailFacility = new DBInfoHandler();

	public DBInfoHandler facilityDBInfo(){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = AES.decrypt(prop.getProperty("FACILITY"), "rhis_db_info").split(",");

		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(dbDetails[1]);
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}

	public DBInfoHandler facilityDBInfo(Integer zilla){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = null;
//		if(zilla==36 || zilla==51 || zilla==75)
//			dbDetails = prop.getProperty("FACILITY_DIST_"+zilla).split(",");
//		else


		dbDetails = AES.decrypt(prop.getProperty("FACILITY_"+zilla), "rhis_db_info").split(",");
//		String db_name = "RHIS_"+String.format("%02d", zilla); //add 0 for single value zilla(ex: bandarban 3 to 03)

		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(dbDetails[1]);
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}

	public DBInfoHandler facilityDistDBInfo(String zilla){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = null;
//		if(zilla.equals("42") || zilla.equals("93"))
		dbDetails = AES.decrypt(prop.getProperty("FACILITY_DIST_"+zilla), "rhis_db_info").split(",");
//		else
//			dbDetails = prop.getProperty("FACILITY_DIST_"+zilla).split(",");

		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(dbDetails[1]);
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}

	public DBInfoHandler facilityDBInfo(String db){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = AES.decrypt(prop.getProperty("FACILITY_"+db), "rhis_db_info").split(",");

		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(dbDetails[1]);
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}

	public DBInfoHandler dynamicDBInfo(String zilla, String upazila){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = null;
		//convert each upazilaDB to zillaDB
		dbDetails = AES.decrypt(prop.getProperty("FACILITY_"+zilla), "rhis_db_info").split(",");
//		String db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
		//for special district which has no upz database
//		if(zilla.equals("48") || zilla.equals("9") || zilla.equals("13")|| zilla.equals("65") || zilla.equals("81") || zilla.equals("26") || zilla.equals("6") || zilla.equals("47")){
			String db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla));

//		}


		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(db_name);
//		detailFacility.setDBName(dbDetails[1]);//enable this for test system
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}

	//Shahed
	public DBInfoHandler mHealthDBInfo(){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = AES.decrypt(prop.getProperty("MHEALTH"), "rhis_db_info").split(",");

		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(dbDetails[1]);
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}

	public DBInfoHandler centralMonitorngInfo(){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = AES.decrypt(prop.getProperty("FACILITY_CENTRAL_MONITORING"), "rhis_db_info").split(",");

		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(dbDetails[1]);
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}

	public DBInfoHandler GEODBInfo(){
		Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
		String[] dbDetails = AES.decrypt(prop.getProperty("GEO_BD"), "rhis_db_info").split(",");

		detailFacility.setHost(dbDetails[0]);
		detailFacility.setDBName(dbDetails[1]);
		detailFacility.setUserName(dbDetails[2]);
		detailFacility.setUserPassword(dbDetails[3]);

		return detailFacility;
	}
}