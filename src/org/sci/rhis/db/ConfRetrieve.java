package org.sci.rhis.db;

import java.util.Properties;
import java.io.IOException;

/**
 * @author sabah.mugab
 * @created June, 2015
 */
public class ConfRetrieve {

	public static Properties readingConf(){
				
		Properties properties = new Properties();
		try{
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("resource/db.properties"));
		}
		catch(IOException e){
			e.printStackTrace();
		}
	    return properties;
	}
}
