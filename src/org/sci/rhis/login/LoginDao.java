package org.sci.rhis.login;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.util.CalendarDate;

/**
 * @author sabah.mugab
 * @created June, 2015
 */
public class LoginDao {

	public static int pID;
	public static String providerID;
	public static String pName;
	public static String facilityName;
	public static int pType;
	public static String id;
	public static int client;
	public static String server;
	public static int mis3_approver;
	
	public static boolean validate(JSONObject loginCred){
		DBSchemaInfo table_schema = new DBSchemaInfo();
		boolean status=false;
		boolean login_success=false;
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.facilityDBInfo();
				
		try{
		
			String sql = "select "+table_schema.getColumn("table", "providerdb_provcode")+" as \"ProvCode\", "+table_schema.getColumn("table", "providerdb_provname")+" as \"ProvName\", "
						+ ""+table_schema.getColumn("table", "providerdb_facilityname")+" as \"FacilityName\", "+table_schema.getColumn("table", "providerdb_provtype")+" as \"ProvType\" "
					    + "FROM "+ table_schema.getTable("table_providerdb") +" "
						+ "WHERE "+table_schema.getColumn("","providerdb_active",new String[]{"1"},"=")+" and "+table_schema.getColumn("table", "providerdb_provcode")+"=? AND "+table_schema.getColumn("table", "providerdb_provpass")+"=?";

			dbObject = dbOp.dbCreatePreparedStatement(dbObject, sql);
			int num = 0;
			try{
				  num = loginCred.getInt("uid");
				  dbObject.getPreparedStatement().setInt(1,num);
			}
			catch (NumberFormatException e) {
				dbObject.getPreparedStatement().setInt(1,num);
			}
			dbObject.getPreparedStatement().setString(2,loginCred.getString("upass"));
			
			dbObject = dbOp.dbExecute(dbObject);
			ResultSet rs = dbObject.getResultSet();
			status=rs.next();
			
			if(status){
				login_success=true;
				pID = rs.getInt("ProvCode");
				pName = rs.getString("ProvName");
				facilityName = rs.getString("FacilityName");
				pType = rs.getInt("ProvType");
						
			}

			sql = "INSERT INTO "+ table_schema.getTable("table_last_login_detail") +"("+table_schema.getColumn("", "last_login_detail_provider_id")+","
					+ table_schema.getColumn("", "last_login_detail_login_time")+","+table_schema.getColumn("", "last_login_detail_login_success")+","
					+ table_schema.getColumn("", "last_login_detail_monitoring_tool")+") VALUES ("
					+ loginCred.getInt("uid") + ","
					+ "'" + CalendarDate.getCurrentDateTime() + "',"
					+ login_success + ", 1)";


//			System.out.println("Last Login Detail query="+sql);

			dbOp.dbStatementExecute(dbObject, sql);

			mis3_approver = 0;
			if(pType==14 || pType==15 || pType==18 || pType==20 || pType==24 || pType==25) {
				String sql_mis3_approver = "select * from "+table_schema.getTable("table_mis3_approver")+" " +
						"where "+table_schema.getColumn("", "mis3_approver_providerid",new String[]{Integer.toString(pID)},"=")+" and "+table_schema.getColumn("", "mis3_approver_active",new String[]{"1"},"=");

				dbObject = dbOp.dbExecute(dbObject, sql_mis3_approver);
				rs = dbObject.getResultSet();
				boolean check_approver = rs.next();
				if(check_approver)
					mis3_approver = 1;
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		return status;
	}

	public static boolean validateUnified(JSONObject loginCred){
		DBSchemaInfo table_schema = new DBSchemaInfo("69");
		boolean status=false;
		boolean login_success=false;
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.facilityDBInfo("CENTRAL");

		try{

			String sql = "select "+table_schema.getColumn("", "users_provcode")+" as \"ProvCode\", "+table_schema.getColumn("", "users_provname")+" as \"ProvName\", "
					+ ""+table_schema.getColumn("", "users_provtype")+" as \"ProvType\" "
					+ "FROM "+ table_schema.getTable("table_users") +" "
					+ "WHERE "+table_schema.getColumn("", "users_provcode")+"=? AND "+table_schema.getColumn("", "users_provpass")+"=?";
			System.out.println(sql);
			dbObject = dbOp.dbCreatePreparedStatement(dbObject, sql);
//			try{
//				dbObject.getPreparedStatement().setString(1,loginCred.getString("uid"));
//			}
//			catch (NumberFormatException e) {
//				dbObject.getPreparedStatement().setString(1,loginCred.getString("uid"));
//			}
			dbObject.getPreparedStatement().setString(1,loginCred.getString("uid"));
			dbObject.getPreparedStatement().setString(2,loginCred.getString("upass"));


			dbObject = dbOp.dbExecute(dbObject);
			ResultSet rs = dbObject.getResultSet();
			status=rs.next();

			if(status){
				login_success=true;
				providerID = rs.getString("ProvCode");
				pName = rs.getString("ProvName");
				pType = rs.getInt("ProvType");

			}

//			sql = "INSERT INTO "+ table_schema.getTable("table_last_login_detail") +"("+table_schema.getColumn("", "last_login_detail_provider_id")+","
//					+ table_schema.getColumn("", "last_login_detail_login_time")+","+table_schema.getColumn("", "last_login_detail_login_success")+","
//					+ table_schema.getColumn("", "last_login_detail_monitoring_tool")+") VALUES ("
//					+ loginCred.getInt("uid") + ","
//					+ "'" + CalendarDate.getCurrentDateTime() + "',"
//					+ login_success + ", 1)";
//			dbOp.dbStatementExecute(dbObject, sql);

			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		return status;
	}

//	public static String getCommunityUserPass(JSONObject loginCred){
//        DBSchemaInfo table_schema = new DBSchemaInfo();
//        boolean status=false;
//        String  upass="";
//        FacilityDB dbFacility = new FacilityDB();
//        DBOperation dbOp = new DBOperation();
//        DBInfoHandler dbObject = dbFacility.facilityDBInfo("CENTRAL");
//
//        try{
//
//            String sql = "select "+table_schema.getColumn("", "users_provpass")+" as \"ProvPass\" "
//                    + "FROM "+ table_schema.getTable("table_users") +" "
//                    + "WHERE "+table_schema.getColumn("", "users_provcode")+"=?";
//            System.out.println(sql);
//            dbObject = dbOp.dbCreatePreparedStatement(dbObject, sql);//			}
//            dbObject.getPreparedStatement().setString(1,loginCred.getString("uid"));
//
//            dbObject = dbOp.dbExecute(dbObject);
//            ResultSet rs = dbObject.getResultSet();
//            status=rs.next();
//
//            if(status){
//                upass = rs.getString("ProvPass");
//
//            }
//
//            if(!rs.isClosed()){
//                rs.close();
//            }
//        }
//        catch(Exception e){
//            System.out.println(e);
//        }
//        finally{
//            dbOp.closeResultSet(dbObject);
//            dbOp.closePreparedStatement(dbObject);
//            dbOp.closeConn(dbObject);
//            dbObject = null;
//        }
//        return upass;
//    }
}