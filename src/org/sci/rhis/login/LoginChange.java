package org.sci.rhis.login;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.util.CalendarDate;

/**
 * @author evana.yasmin
 * @created 30th January, 2019
 */
public class LoginChange {

    public static String CurrentPassword;
    public static String NewPassword;
    public static String ConPassword;
    public static String sql_updateProvider;
    public static String sql_updateProviderUpazila;
    public static Integer providerCode;

    public static boolean updateUserPassword(JSONObject formValue) {

        DBSchemaInfo table_schema = new DBSchemaInfo();
        boolean status = false;

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        try{
            //CurrPass = Integer.parseInt(formValue.getString("CurrentPassword"));
            //NewPassword = Integer.parseInt(formValue.getString("NewPassword"));
            //ConPassword = Integer.parseInt(formValue.getString("ConPassword"));

            CurrentPassword = formValue.getString("currentpass");
            NewPassword = formValue.getString("newpass");
            ConPassword = formValue.getString("oldpass");
            providerCode = Integer.parseInt(formValue.getString("uid"));

            sql_updateProvider = "update "+table_schema.getTable("table_providerdb")+" set "+table_schema.getColumn("", "providerdb_provpass",new String[]{NewPassword},"=")+" where "+table_schema.getColumn("", "providerdb_provcode",new String[]{Integer.toString(providerCode)},"=")+" and "+table_schema.getColumn("", "providerdb_provpass",new String[]{CurrentPassword},"=")+" " ;

            //sql_updateProvider = "update "+table_schema.getTable("table_providerdb")+" set "+table_schema.getColumn("", "providerdb_exdate",new String[]{retireddate},"=")+", "+table_schema.getColumn("", "providerdb_active",new String[]{Character.toString('2')},"=")+" where "+table_schema.getColumn("", "providerdb_provcode",new String[]{Integer.toString(providerCode)},"=")+" and "+table_schema.getColumn("", "providerdb_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("", "providerdb_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and "+table_schema.getColumn("", "providerdb_unionid",new String[]{Integer.toString(union)},"=");

            //System.out.println(sql_assignAppVersion);

            Integer sql_status = dbOp.dbExecuteUpdate(dbObject,sql_updateProvider);

            if(sql_status == 0)
            {
                return status = false;
            }
            else
            {
                return status = true;
            }


        }
        catch(Exception e){
            System.out.println(e);
            status = false;
        }
        finally{
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return status;


    }

    /*
     * Provider Information Edit
     * @Author: evana.yasmin
     * @Date: 19/02/2019
     * */

    public static boolean updateProviderInfo(JSONObject formValue) {

        boolean status = false;

        try{
            String provname = formValue.getString("provname");
            String mobileno = formValue.getString("mobileno");
            String providerPass = formValue.getString("provpass");
            providerCode = Integer.parseInt(formValue.getString("providerid"));
            Integer zillaid = Integer.parseInt(formValue.getString("zillaid"));
            Integer upazilaid = Integer.parseInt(formValue.getString("upazilaid"));
            Integer providerType = Integer.parseInt(formValue.getString("providertype"));
            String approval_upazilas = "";
            if(formValue.has("approval_upazilas"))
                approval_upazilas = formValue.getString("approval_upazilas");

            if (providerType != 2 && providerType != 3 && providerType != 4 && providerType != 5 && providerType != 6 && providerType != 17 && providerType != 101)
            {

                DBSchemaInfo table_schema = new DBSchemaInfo("36");
                FacilityDB dbFacility = new FacilityDB();
                DBOperation dbOp = new DBOperation();
//                DBInfoHandler dbObject = dbFacility.facilityDBInfo("36");
                DBInfoHandler dbObject = dbFacility.facilityDBInfo(); //moved to rhis_facility_central db instead of rhis_36


                sql_updateProvider = "update "+table_schema.getTable("table_providerdb")+" set "+table_schema.getColumn("", "providerdb_provname",new String[]{provname},"=")+","
                        +table_schema.getColumn("", "providerdb_mobileno",new String[]{mobileno},"=")+","
                        +table_schema.getColumn("", "providerdb_provpass",new String[]{providerPass},"=")
                        +" where "+table_schema.getColumn("", "providerdb_provcode",new String[]{Integer.toString(providerCode)},"=")
                        +" and "+table_schema.getColumn("", "providerdb_zillaid",new String[]{Integer.toString(zillaid)},"=")+" " ;


                System.out.println(sql_updateProvider);

                Integer sql_status = dbOp.dbExecuteUpdate(dbObject,sql_updateProvider);

                if(providerType==14 || providerType==15 || providerType==18 || providerType==20 || providerType==24 || providerType==25) {
                    String mis3_approval_string = "";
                    if (!formValue.getString("active").equals("") || !formValue.getString("inactive").equals("")) {
                        if(formValue.getString("active").equals("1")) {
                            mis3_approval_string = "insert into " +table_schema.getTable("table_mis3_approver")+ " (" +table_schema.getColumn("","mis3_approver_providerid")+ ", " +table_schema.getColumn("","mis3_approver_active")+ ", " +table_schema.getColumn("","mis3_approver_start_date")+ ", "+table_schema.getColumn("","mis3_approver_approval_zilla")+", "+table_schema.getColumn("","mis3_approver_approval_upazilas")+") "+
                                    "values(" + providerCode + ", 1, '" + (formValue.getString("active_date").equals("")?formValue.getString("start_date"):formValue.getString("active_date")) + "', "+zillaid+", '"+approval_upazilas+"') " +
                                    "on conflict ("+table_schema.getColumn("","mis3_approver_providerid")+", "+table_schema.getColumn("","mis3_approver_active")+", "+table_schema.getColumn("","mis3_approver_start_date")+") " +
                                    "do " +
                                    "update set "+table_schema.getColumn("","mis3_approver_approval_upazilas")+"='"+approval_upazilas+"'";

                            System.out.println(mis3_approval_string);
                            dbOp.dbStatementExecute(dbObject,mis3_approval_string);
                        }
                        else if(formValue.getString("inactive").equals("2")){
                            mis3_approval_string = "update " +table_schema.getTable("table_mis3_approver")+ " set " +table_schema.getColumn("","mis3_approver_active")+ "=2, " +table_schema.getColumn("","mis3_approver_end_date")+ "='" + formValue.getString("active_date") + "' " +
                                    "where "+table_schema.getColumn("", "mis3_approver_providerid",new String[]{Integer.toString(providerCode)},"=")+" and "+table_schema.getColumn("", "mis3_approver_active",new String[]{"1"},"=");

                            dbOp.dbExecuteUpdate(dbObject,mis3_approval_string);
                        }
                    }
                }

                if(sql_status == 0)
                {
                    return status = false;
                }
                else
                {
                    return status = true;
                }
            }
            else
            {

                DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zillaid));
                FacilityDB dbFacility = new FacilityDB();
                DBOperation dbOp = new DBOperation();
                DBInfoHandler dbObject = dbFacility.facilityDBInfo(Integer.toString(zillaid));

                System.out.println("Only Upazilaid=" + upazilaid);

                /****  Update Zilla Db****/
                sql_updateProvider = "update "+table_schema.getTable("table_providerdb")+" set "+table_schema.getColumn("", "providerdb_provname",new String[]{provname},"=")+","
                        +table_schema.getColumn("", "providerdb_mobileno",new String[]{mobileno},"=")+","
                        +table_schema.getColumn("", "providerdb_provpass",new String[]{providerPass},"=")
                        +" where "+table_schema.getColumn("", "providerdb_provcode",new String[]{Integer.toString(providerCode)},"=")
                        +" and "+table_schema.getColumn("", "providerdb_zillaid",new String[]{Integer.toString(zillaid)},"=")+" " ;

                System.out.println(sql_updateProvider);

                Integer sql_status = dbOp.dbExecuteUpdate(dbObject,sql_updateProvider);

                if(sql_status == 0)
                {
                    System.out.println("District level upazila not updated");
                }
                else
                {
                    System.out.println("District level upazilla updated");
                }

                /*** Update zilla db end****/



                /****  Update upaZilla Db of that zilla ****/

                sql_updateProviderUpazila = "update "+table_schema.getTable("table_providerdb")+" set "+table_schema.getColumn("", "providerdb_provname",new String[]{provname},"=")+","
                        +table_schema.getColumn("", "providerdb_mobileno",new String[]{mobileno},"=")+","
                        +table_schema.getColumn("", "providerdb_provpass",new String[]{providerPass},"=")
                        +" where "+table_schema.getColumn("", "providerdb_provcode",new String[]{Integer.toString(providerCode)},"=")
                        +" and "+table_schema.getColumn("", "providerdb_zillaid",new String[]{Integer.toString(zillaid)},"=")
                        +" and "+table_schema.getColumn("", "providerdb_upazilaid",new String[]{Integer.toString(upazilaid)},"=") ;

                System.out.println(sql_updateProviderUpazila);

                DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zillaid), Integer.toString(upazilaid));

                Integer sql_status_upazila = dbOp.dbExecuteUpdate(dbObject_dynamic,sql_updateProviderUpazila);
                dbOp.closeStatement(dbObject_dynamic);
                dbOp.closeConn(dbObject_dynamic);

                /*** Update Upazilla db end****/

                if(sql_status_upazila == 0)
                {
                    System.out.println("Upazilla database did not update");
                }
                else
                {
                    System.out.println("Upazila database updated");
                }

                if(sql_status == 0)
                {
                    return status = false;
                }
                else
                {
                    return status = true;
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
            status = false;
        }

        return status;


    }

}
