package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class CommunityInfo {
    public static Integer zilla;
    public static Integer upazila;
    public static Integer union;

    public static LinkedHashMap LoadVillage(JSONObject hhInfo){
        zilla = hhInfo.getInt("zillaid");
        upazila = hhInfo.getInt("upazilaid");
        union = hhInfo.getInt("unionid");

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        LinkedHashMap<String, LinkedHashMap<String, String>> hhinfo = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

        try {

            String sql = "select "+table_schema.getColumn("","village_villagenameeng")+" as village_name, "+
                    "(case when "+table_schema.getColumn("","household_wardold")+" is not null then "+table_schema.getColumn("","household_wardold")+" else '0' end) as ward_id, " +
                    table_schema.getColumn("","household_dist")+" as zilla_id, " +
                    table_schema.getColumn("","household_upz")+" as upazila_id,"+table_schema.getColumn("","household_un")+" as union_id, "+table_schema.getColumn("","household_vill")+" as village_id, " +
                    table_schema.getColumn("","household_mouza")+" as mouza_id, "+
                    "min("+table_schema.getColumn("","household_hhno")+") as min_hhno, max("+table_schema.getColumn("","household_hhno")+") as max_hhno " +
                    "from "+ table_schema.getTable("table_village") +" v " +
                    "join "+ table_schema.getTable("table_household") +" hh " +
                    "on hh."+table_schema.getColumn("","household_dist")+"=v."+table_schema.getColumn("","village_zillaid")+" and " +
                    "hh."+table_schema.getColumn("","household_upz")+"=v."+table_schema.getColumn("","village_upazilaid")+" and " +
                    "hh."+table_schema.getColumn("","household_un")+"=v."+table_schema.getColumn("","village_unionid")+" and " +
                    "hh."+table_schema.getColumn("","household_mouza")+"=v."+table_schema.getColumn("","village_mouzaid")+" and " +
                    "hh."+table_schema.getColumn("","household_vill")+"=v."+table_schema.getColumn("","village_villageid")+" " +
                    "where " + table_schema.getColumn("", "village_zillaid",new String[]{Integer.toString(zilla)},"=") + " and " +
                    table_schema.getColumn("", "village_upazilaid",new String[]{Integer.toString(upazila)},"=") + " and " +
                    table_schema.getColumn("", "village_unionid",new String[]{Integer.toString(union)},"=") + " " +
                    "GROUP BY "+table_schema.getColumn("","household_dist")+","+table_schema.getColumn("","household_upz")+"," +
                    table_schema.getColumn("","household_un")+","+table_schema.getColumn("","household_mouza")+"," +
                    table_schema.getColumn("","household_vill")+","+table_schema.getColumn("","village_villagenameeng")+","+
                    table_schema.getColumn("","household_wardold")+" order by "+table_schema.getColumn("","village_villagenameeng");
            System.out.println(sql);
            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();


            int j =0;
            while(rs.next()){
                hhinfo.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)), new LinkedHashMap<String, String>());
                for (int i = 1; i <= columnCount; i++) {
                    hhinfo.get(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2))).put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));
                }
            }
            if(!rs.isClosed()){
                rs.close();
            }


        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return hhinfo;
    }

    public static boolean updateHHWardInfo(JSONObject hhInfo){
        boolean status = false;
        zilla = hhInfo.getInt("zilla");
        upazila = hhInfo.getInt("upazila");
        union = hhInfo.getInt("union");
        Integer mouza = hhInfo.getInt("mouza");
        Integer village = hhInfo.getInt("village");
        String ward = hhInfo.getString("ward");
        String hhStart = hhInfo.getString("hhStart");
        String hhEnd = hhInfo.getString("hhEnd");

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

        try {
            String update_sql = "UPDATE "+ table_schema.getTable("table_household") +" SET " + table_schema.getColumn("", "household_wardold",new String[]{ward},"=") + " " +
                    "WHERE " + table_schema.getColumn("", "household_dist",new String[]{Integer.toString(zilla)},"=") + " AND " + table_schema.getColumn("", "household_upz",new String[]{Integer.toString(upazila)},"=") + " " +
                    "AND " + table_schema.getColumn("", "household_un",new String[]{Integer.toString(union)},"=") + " AND " + table_schema.getColumn("", "household_mouza",new String[]{Integer.toString(mouza)},"=") + " " +
                    "AND " + table_schema.getColumn("", "household_vill",new String[]{Integer.toString(village)},"=") + " AND "+ table_schema.getColumn("", "household_hhno",new String[]{hhStart,hhEnd},"between");

            System.out.println(update_sql);

            dbOp.dbExecuteUpdate(dbObject,update_sql);
            status = true;
        }
        catch (Exception e){
            status = false;
            e.printStackTrace();
        }
        finally {
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return status;
    }
}
