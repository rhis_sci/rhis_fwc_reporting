package org.sci.rhis.report;

//import java.time.YearMonth;

import org.json.JSONObject;
import org.sci.rhis.util.CalendarDate;

public class ProviderWiseReport_old {

	public static String methodType;
	public static String zilla;
	public static String upazila;
	public static String resultString;
	public static String graphString;
	public static String sql;
	public static String date_type;
	public static String monthSelect;
	public static String start_date;
	public static String end_date;
	public static String db_name;
	
	public static String getResultSet(JSONObject reportCred){
	
		try{
			methodType = reportCred.getString("methodType");
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			date_type = reportCred.getString("report1_dateType");
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
			//System.out.println(date_type);
			
			/*sql = "select (anc.\"providerId\") as \"ProviderId\", anc.\"ProvName\", \"ANC\", \"PNC\", \"PNC-N\", \"Delivery\" ,"
					+ "sum(\"ANC\" + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\" " 
					+ " from " 

					+ "(select \"providerId\", \"ProvName\", count (*) as \"ANC\" "
					+ "FROM \"ancService\", \"ProviderDB\" "
					+ "where \"providerId\" in "
					+ "(select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "AND \"ancService\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "AND \"ancService\".\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "group by \"providerId\", \"ProvName\" "
					+ ") AS anc "
					+ "Left join "
					+ "(select \"providerId\", \"ProvName\", count (*) as \"PNC\" "
					+ "FROM \"pncServiceMother\", \"ProviderDB\" "
					+ "where \"providerId\" in "
					+ "      (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "AND \"pncServiceMother\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "AND \"pncServiceMother\".\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "group by \"providerId\", \"ProvName\" "
					+ ") AS pnc "
					+ "on anc.\"providerId\" = pnc.\"providerId\" "
					+ "Left join "
					+ "(select \"providerId\", \"ProvName\", count (*) as \"PNC-N\" "
					+ "FROM \"pncServiceChild\", \"ProviderDB\" "
					+ "where \"providerId\" in "
					+ "       (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "  AND \"pncServiceChild\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "  AND \"pncServiceChild\".\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "  group by \"providerId\", \"ProvName\" "
					+ "  ) AS pncc "
					+ "  on anc.\"providerId\" = pncc.\"providerId\" "
					+ "  left join "
					+ "  ( select \"providerId\", \"ProvName\", count (*) as \"Delivery\" "
					+ "  FROM \"delivery\", \"ProviderDB\" "
					+ "  where \"providerId\" in "
					+ "       (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "  AND \"delivery\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "  AND \"delivery\".\"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "  group by \"providerId\", \"ProvName\" "
					+ ") as delivery "
					+ "on anc.\"providerId\" = delivery.\"providerId\" "
					+ "group by anc.\"providerId\", anc.\"ProvName\", \"ANC\", \"PNC\", \"PNC-N\",\"Delivery\" "
					+ "order by anc.\"providerId\", anc.\"ProvName\" ";*/
			
			if(methodType.equals("1"))
			{
				sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\", "
						//+ "(CASE WHEN anc.\"ANC\" IS NULL THEN 0 ELSE anc.\"ANC\" END) AS \"ANC\", "
						+ "(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\", "
						+ "(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\", "
						+ "(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\", "
						+ "(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\", "
						//+ "(CASE WHEN pnc.\"PNC\" IS NULL THEN 0 ELSE pnc.\"PNC\" END) AS \"PNC\", "
						+ "(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\", "
						+ "(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\", "
						+ "(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\", "
						+ "(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\", "
						//+ "(CASE WHEN pncc.\"PNC-N\" IS NULL THEN 0 ELSE pncc.\"PNC-N\" END) AS \"PNC-N\", "
						+ "(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\", "
						+ "(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\", "
						+ "(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\", "
						+ "(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\", "
						+ "(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END),"
						+ " ((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\" "
						+ " from \"ProviderDB\" "
						+ " left join"
						+ " (select \"providerId\",count (*) as \"ANC\","
						+ " count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\","
						+ " count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\","
						+ " count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\","
						+ " count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\""
						+ " from \"ancService\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') group by \"providerId\") as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " left join "
						+ " (select \"providerId\",count (*) as \"PNC\","
						+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\","
						+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\","
						+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\","
						+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\""
						+ " from \"pncServiceMother\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') group by \"providerId\") as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " left join "
						+ " (select \"providerId\",count (*) as \"PNC-N\","
						+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\","
						+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\","
						+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\","
						+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\""
						+ " from \"pncServiceChild\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') group by \"providerId\") as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " left join "
						+ " (select \"providerId\",count (*) as \"Delivery\" from \"delivery\" where \"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' and \"deliveryDoneByThisProvider\"='1' group by \"providerId\") as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " join "
						+ " \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" "
						+ " join "
						+ " (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid "
						+ " where \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
						//+ " AND ((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END))>0 ";
			}
			else if(methodType.equals("2"))
			{
				sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\", "
						+ "(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", "
						+ "(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", "
						+ "(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  "
						+ "(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  "
						+ "(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\",  "
						+ "((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\"   "
						+ "from \"ProviderDB\"   "
						+ "left join  "
						+ "(select \"providerId\",COUNT(CASE WHEN (\"methodType\"=1 OR \"methodType\"=10) THEN 1 ELSE NULL END) as \"Pill\", COUNT(CASE WHEN \"methodType\"=2 THEN 1 ELSE NULL END) as \"Condom\" from \"pillCondomService\" where \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as pillcondom on pillcondom.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"providerId\",count (*) as \"IUD\" from \"iudService\" where \"iudImplantDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as iud on iud.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"providerId\",count (*) as \"IUD Followup\" from \"iudFollowupService\" where \"followupDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as iudfollowup on iudfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"providerId\",count (*) as \"Injectable\" from \"womanInjectable\" where \"doseDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as injectable on injectable.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"Dist\", \"Upz\", \"UN\", count(*) as \"ELCO\" from \"Member\" where date_part('year', age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) < '49' AND date_part('year',  age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) > '15' AND \"Sex\" = '2' group by \"Dist\", \"Upz\", \"UN\") as member on member.\"Dist\"=\"ProviderDB\".zillaid AND member.\"Upz\"=\"ProviderDB\".upazilaid AND member.\"UN\"=\"ProviderDB\".unionid   "
						+ "join "
						+ "\"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" "
						+ "join   "
						+ "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   "
						+ "where \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			}
			else
			{
				sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\", "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END) AS \"Male\", "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END) AS \"Female\", "
						+ "(COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END)+COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END)) AS \"TOTAL\" "
						+ "from \"ProviderDB\"   "
						+ "left join  "
						+ "\"gpService\" on \"gpService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" and \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
						+ "left join "
						+ "\"clientMap\" on \"clientMap\".\"generatedId\"=\"gpService\".\"healthId\" "
						+ "join "
						+ "\"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" "
						+ "join "
						+ "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   "
						+ "where \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) "
						+ "GROUP BY \"ProviderDB\".\"ProvCode\", \"ProviderDB\".\"ProvName\", \"FacilityName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", unions.\"UnionName\"";
			}	


			
			System.out.println(sql);
			
			resultString = GetResultSet.getFinalResult(sql,zilla, upazila);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return resultString;
	}
	
	public static JSONObject getGraphSet(JSONObject reportCred){
		
		JSONObject graphInfo = null;
		
		try{
			methodType = reportCred.getString("methodType");
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			date_type = reportCred.getString("report1_dateType");
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
			if(methodType.equals("1"))
			{
				sql = "select \"ProviderDB\".\"ProvName\", "
						//+ "(CASE WHEN anc.\"ANC\" IS NULL THEN 0 ELSE anc.\"ANC\" END) AS \"ANC\", "
						+ "(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\", "
						+ "(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\", "
						+ "(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\", "
						+ "(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\", "
						//+ "(CASE WHEN pnc.\"PNC\" IS NULL THEN 0 ELSE pnc.\"PNC\" END) AS \"PNC\", "
						+ "(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\", "
						+ "(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\", "
						+ "(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\", "
						+ "(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\", "
						//+ "(CASE WHEN pncc.\"PNC-N\" IS NULL THEN 0 ELSE pncc.\"PNC-N\" END) AS \"PNC-N\", "
						+ "(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\", "
						+ "(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\", "
						+ "(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\", "
						+ "(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\", "
						+ "(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END),"
						+ "(CASE WHEN member.\"ELCO\" IS NULL THEN 0 ELSE member.\"ELCO\" END) "
						//+ " ((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\" "
						+ " from \"ProviderDB\" "
						+ " left join"
						+ " (select \"providerId\",count (*) as \"ANC\","
						+ " count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\","
						+ " count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\","
						+ " count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\","
						+ " count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\""
						+ " from \"ancService\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') group by \"providerId\") as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " left join "
						+ " (select \"providerId\",count (*) as \"PNC\","
						+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\","
						+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\","
						+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\","
						+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\""
						+ " from \"pncServiceMother\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') group by \"providerId\") as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " left join "
						+ " (select \"providerId\",count (*) as \"PNC-N\","
						+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\","
						+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\","
						+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\","
						+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\""
						+ " from \"pncServiceChild\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') group by \"providerId\") as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " left join "
						+ " (select \"providerId\",count (*) as \"Delivery\" from \"delivery\" where \"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' and \"deliveryDoneByThisProvider\"='1' group by \"providerId\") as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
						+ " left join "
						+ " (select \"Dist\", \"Upz\", \"UN\", count(*) as \"ELCO\" from \"Member\" where date_part('year', age('"+end_date+"',to_date(\"DOB\", 'YYYY-MM-DD'))) < '49' AND date_part('year',  age('"+end_date+"',to_date(\"DOB\", 'YYYY-MM-DD'))) > '15' AND \"Sex\" = '2' group by \"Dist\", \"Upz\", \"UN\") as member on member.\"Dist\"=\"ProviderDB\".zillaid AND member.\"Upz\"=\"ProviderDB\".upazilaid AND member.\"UN\"=\"ProviderDB\".unionid "
						+ " join "
						+ " (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid "
						+ " where \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			}
			else if(methodType.equals("2"))
			{
				sql = "select \"ProviderDB\".\"ProvName\", "
						+ "(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", "
						+ "(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", "
						+ "(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  "
						+ "(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  "
						+ "(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\",  "
						+ "(CASE WHEN member.\"ELCO\" IS NULL THEN 0 ELSE member.\"ELCO\" END) "
						+ "from \"ProviderDB\"   "
						+ "left join  "
						+ "(select \"providerId\",COUNT(CASE WHEN (\"methodType\"=1 OR \"methodType\"=10) THEN 1 ELSE NULL END) as \"Pill\", COUNT(CASE WHEN \"methodType\"=2 THEN 1 ELSE NULL END) as \"Condom\" from \"pillCondomService\" where \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as pillcondom on pillcondom.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"providerId\",count (*) as \"IUD\" from \"iudService\" where \"iudImplantDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as iud on iud.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"providerId\",count (*) as \"IUD Followup\" from \"iudFollowupService\" where \"followupDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as iudfollowup on iudfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"providerId\",count (*) as \"Injectable\" from \"womanInjectable\" where \"doseDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' group by \"providerId\") as injectable on injectable.\"providerId\"=\"ProviderDB\".\"ProvCode\"   "
						+ "left join   "
						+ "(select \"Dist\", \"Upz\", \"UN\", count(*) as \"ELCO\" from \"Member\" where date_part('year', age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) < '49' AND date_part('year',  age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) > '15' AND \"Sex\" = '2' group by \"Dist\", \"Upz\", \"UN\") as member on member.\"Dist\"=\"ProviderDB\".zillaid AND member.\"Upz\"=\"ProviderDB\".upazilaid AND member.\"UN\"=\"ProviderDB\".unionid   "
						+ "join   "
						+ "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   "
						+ "where \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			}
			else
			{
				sql = "select \"ProviderDB\".\"ProvName\", "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END) AS \"Male\", "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END) AS \"Female\" "
						+ "from \"ProviderDB\"   "
						+ "left join  "
						+ "\"gpService\" on \"gpService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" and \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
						+ "left join "
						+ "\"clientMap\" on \"clientMap\".\"generatedId\"=\"gpService\".\"healthId\" "
						+ "join   "
						+ "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   "
						+ "where \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) "
						+ "GROUP BY \"ProviderDB\".\"ProvName\"";
			}	
				


			
			//System.out.println(sql);
			
			//graphInfo = GetResultSet.getFinalGraphResult(sql, zilla, upazila);
			
			//System.out.println(graphString);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return graphInfo;
	}

}
