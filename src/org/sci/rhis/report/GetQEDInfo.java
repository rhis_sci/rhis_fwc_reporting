
package org.sci.rhis.report;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.model.GetResult;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

public  class GetQEDInfo {

	public static String getResultSet(JSONObject reportCred){

		String resultString = "";

		JSONObject resultJSON = null;

		JSONObject tempResult = null;
		try {
//			if(reportCred.getString("reportViewType").equals("4") && reportCred.getString("report1_upazila").isEmpty()){
//				String zilla = reportCred.getString("report1_zilla");
//				DBListProperties setZillaList = new DBListProperties();
//				Properties db_prop = setZillaList.setProperties();
//				String[] upazila_list = db_prop.getProperty(zilla).split(",");
//
//				for (int i=0; i<upazila_list.length;i++) {
//					reportCred.put("report1_upazila", upazila_list[i]);
//
//					if(i==0)
//						resultJSON = new JSONObject(ServiceStatistics.getResultSet(reportCred).split(" table/graph/map ")[1].replaceAll("[\\[\\]]", "").replace("}},{", "},"));
//					else {
//						tempResult = new JSONObject(ServiceStatistics.getResultSet(reportCred).split(" table/graph/map ")[1].replaceAll("[\\[\\]]", "").replace("}},{", "},"));
//
//						Iterator<String> iteratorResult = tempResult.keys();
//						while (iteratorResult.hasNext()){
//							String key = iteratorResult.next();
//							JSONObject getResult = new JSONObject("{"+resultJSON.getString(key).substring(1, resultJSON.getString(key).length()-1) + "," + tempResult.getString(key).substring(1, tempResult.getString(key).length()-1)+"}");
//							System.out.println(getResult);
//							resultJSON.put(key, getResult);
//						}
//					}
//				}
//			}
//			else

			resultJSON = new JSONObject(ServiceStatistics.getResultSet(reportCred).split(" table/graph/map ")[1].replaceAll("[\\[\\]]", "").replace("}},{", "},"));

			System.out.println(resultJSON);

			String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
			String table_header = "";
			if(reportCred.getString("reportViewType").equals("1"))
				table_header += "<thead><tr><th>Zilla Name</th>";
			else if(reportCred.getString("reportViewType").equals("2"))
				table_header += "<thead><tr><th>Upazila Name</th>";
			else if(reportCred.getString("reportViewType").equals("3"))
				table_header += "<thead><tr><th>Union Name</th>";
			else
				table_header += "<thead><tr><th>Facility Name</th><th>Upazila Name</th><th>Union Name</th>";
			table_header += "<th>Maternal Deaths</th>" +
					"<th>Maternal Deaths-<br />Haemorrhage</th>" +
					"<th>Maternal Deaths-<br />Preeclampsia/Eclampsia</th>" +
					"<th>Maternal Deaths-<br />Sepsis</th>" +
					"<th>Maternal Deaths-<br />Other</th>" +
					"<th>Neonatal Deaths-<br />Prematurity</th>" +
					"<th>Neonatal Deaths-<br />Birth Asphyxia</th>" +
					"<th>Neonatal Deaths-<br />Sepsis</th>" +
					"<th>Neonatal Deaths-<br />Other</th>" +
					"<th>Stillbirth Rate<br />(Per 1000)</th>" +
					"<th>Stillbirth Rate-<br />Fresh(Per 1000)</th>" +
					"<th>Stillbirth Rate-<br />Macerated(Per 1000)</th>" +
					"<th>Neonatal Mortality<br /> Rate(Per 1000)</th>" +
					"<th>Obstetric Case<br />Fatality Rate(%)</th>" +
					"<th>Breastfed(%)</th>" +
					"<th>Oxytocin(%)</th>" +
					"<th>Birthweight(%)</th>" +
					"<th>Weighing<br />≤2,000g(%)</th>";
			table_header += "</tr></thead>";
			JSONObject getGEOName = new JSONObject(resultJSON.getString("Maternal Deaths"));
			Iterator<String> iterator = getGEOName.keys();

			ObjectMapper mapper = new ObjectMapper();
			Map<String, Map<String, String>> resultMap = mapper.readValue(resultJSON.toString(), Map.class);
			Integer c1,c2,c3,c4,c5,c6,c7,c8,c9;
			Double c10,c11,c12,c13,c14,c15,c16,c17,c18,c19;
			c1=c2=c3=c4=c5=c6=c7=c8=c9=0;
			c10=c11=c12=c13=c14=c15=c16=c17=c18=c19=0.00;
			DecimalFormat df = new DecimalFormat("0.00");

			String table_body = "<tbody>";
			while (iterator.hasNext()) {
				String key = iterator.next();

				c1+=Integer.parseInt(resultMap.get("Maternal Deaths").get(key));
				c2+=Integer.parseInt(resultMap.get("Maternal Deaths-Haemorrhage").get(key));
				c3+=Integer.parseInt(resultMap.get("Maternal Deaths-Preeclampsia/Eclampsia").get(key));
				c4+=Integer.parseInt(resultMap.get("Maternal Deaths-Sepsis").get(key));
				c5+=Integer.parseInt(resultMap.get("Maternal Deaths-Other").get(key));
				c6+=Integer.parseInt(resultMap.get("Neonatal Deaths-Prematurity").get(key));
				c7+=Integer.parseInt(resultMap.get("Neonatal Deaths-Birth Asphyxia").get(key));
				c8+=Integer.parseInt(resultMap.get("Neonatal Deaths-Sepsis").get(key));
				c9+=Integer.parseInt(resultMap.get("Neonatal Deaths-Other").get(key));
				c10+=Double.parseDouble(resultMap.get("Total Newborn").get(key));
				c11+=Double.parseDouble(resultMap.get("Stillbirth").get(key));
				c12+=Double.parseDouble(resultMap.get("Stillbirth-Fresh").get(key));
				c13+=Double.parseDouble(resultMap.get("Stillbirth-Macerated").get(key));
				c14+=Double.parseDouble(resultMap.get("Breastfed").get(key));
				c15+=Double.parseDouble(resultMap.get("Oxytocin").get(key));
				c16+=Double.parseDouble(resultMap.get("Birthweight").get(key));
				c17+=Double.parseDouble(resultMap.get("Weighing≤2,000g").get(key));
				c18+=Double.parseDouble(resultMap.get("Total Delivery").get(key));
				c19+=Double.parseDouble(resultMap.get("Neonatal Deaths").get(key));

				table_body += "<tr>";

				if(reportCred.getString("reportViewType").equals("1") || reportCred.getString("reportViewType").equals("2") || reportCred.getString("reportViewType").equals("3"))
					table_body += "<td>"+key+"</td>";
				else
					table_body += "<td>"+key.split("_")[0]+"</td><td align='center'>"+key.split("_")[1]+"</td><td align='center'>"+key.split("_")[2]+"</td>";

				table_body += "<td align='center'>"+resultMap.get("Maternal Deaths").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Maternal Deaths-Haemorrhage").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Maternal Deaths-Preeclampsia/Eclampsia").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Maternal Deaths-Sepsis").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Maternal Deaths-Other").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Neonatal Deaths-Prematurity").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Neonatal Deaths-Birth Asphyxia").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Neonatal Deaths-Sepsis").get(key)+"</td>" +
						"<td align='center'>"+resultMap.get("Neonatal Deaths-Other").get(key)+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Newborn").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Stillbirth").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*1000))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Newborn").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Stillbirth-Fresh").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*1000))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Newborn").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Stillbirth-Macerated").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*1000))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Newborn").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Neonatal Deaths").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*1000))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Delivery").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Maternal Deaths").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*100))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Newborn").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Breastfed").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*100))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Delivery").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Oxytocin").get(key))/Double.parseDouble(resultMap.get("Total Delivery").get(key)))*100))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Newborn").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Birthweight").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*100))+"</td>" +
						"<td align='center'>"+(resultMap.get("Total Newborn").get(key).equals("0")?0.00:df.format((Double.parseDouble(resultMap.get("Weighing≤2,000g").get(key))/Double.parseDouble(resultMap.get("Total Newborn").get(key)))*100))+"</td>";

				table_body += "</tr>";
			}
			table_body += "</tbody>";

			String calspan = "";
			if(reportCred.getString("reportViewType").equals("4"))
				calspan = "colspan=3";

			String table_footer = "<tfoot><th "+calspan+" style='text-align: right;'>Total:</th>" +
					"<th style='text-align: center;'>"+c1+"</th>" +
					"<th style='text-align: center;'>"+c2+"</th>" +
					"<th style='text-align: center;'>"+c3+"</th>" +
					"<th style='text-align: center;'>"+c4+"</th>" +
					"<th style='text-align: center;'>"+c5+"</th>" +
					"<th style='text-align: center;'>"+c6+"</th>" +
					"<th style='text-align: center;'>"+c7+"</th>" +
					"<th style='text-align: center;'>"+c8+"</th>" +
					"<th style='text-align: center;'>"+c9+"</th>" +
					"<th style='text-align: center;'>"+(c10==0?0.00:df.format((c11/c10)*1000))+"</th>" +
					"<th style='text-align: center;'>"+(c10==0?0.00:df.format((c12/c10)*1000))+"</th>" +
					"<th style='text-align: center;'>"+(c10==0?0.00:df.format((c13/c10)*1000))+"</th>" +
					"<th style='text-align: center;'>"+(c10==0?0.00:df.format((c19/c10)*1000))+"</th>" +
					"<th style='text-align: center;'>"+(c18==0?0.00:df.format((c1/c10)*100))+"</th>" +
					"<th style='text-align: center;'>"+(c10==0?0.00:df.format((c14/c10)*100))+"</th>" +
					"<th style='text-align: center;'>"+(c18==0?0.00:df.format((c15/c18)*100))+"</th>" +
					"<th style='text-align: center;'>"+(c10==0?0.00:df.format((c16/c10)*100))+"</th>" +
					"<th style='text-align: center;'>"+(c10==0?0.00:df.format((c17/c10)*100))+"</th>" +
					"</tfoot>";

			resultString = table+table_header+table_body+table_footer+"</table>";
			System.out.println(resultString);
//			System.out.println(resultString.split(" table/graph/map ")[1].replaceAll("[\\[\\]]", ""));
		}
		catch (Exception e) {
			e.printStackTrace();
		}

//		resultString = resultString.split(" table/graph/map ")[0];

		return resultString;
	}
}
