package org.sci.rhis.report;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;

public class SymChannelCreate {
	
	public static Integer zilla;
	public static Integer upazila;
	public static String tablename;
	public static Integer synctype;
	public static String formtype;
	public static String servername;
	public static String nodeid;
	public static String sql_servername;
	public static String sql_sym1;
	public static String sql_sym2;
	public static String sql_sym3;
	public static String sql_sym4;
	
	public static String dropdown;
	public static String sql_dropdown;
	
	public static String DB_name;
	
	public static boolean createChannel(JSONObject formValue){
		
//		DBOperation dbOp = new DBOperation();
//		DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();
		
		boolean status = false;
		int serverToNodeSync = 1;
		int nodeToServerSync = 2;
		int bothWaySync = 3;

		zilla = Integer.parseInt(formValue.getString("zilla"));

		DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
		
		try{
			upazila = Integer.parseInt(formValue.getString("upazila"));
			formtype = formValue.getString("formtype");
			if(formValue.has("tablename")) {
				tablename = formValue.getString("tablename");
			}
			if(!(formValue.get("synctype").equals(""))) {
				synctype = Integer.parseInt(formValue.getString("synctype"));
			}
			
			DBOperation dbOp = new DBOperation();
			DBInfoHandler dbObject = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
			
			//DB_name = "RHIS_"+zilla+"_"+String.format("%02d", upazila);
			//Connection c = DriverManager.getConnection("jdbc:postgresql://10.10.10.17:5432/"+DB_name, "postgres", "postgres");
			//c.setAutoCommit(false);
			//Statement statement = c.createStatement();
			
			sql_servername = "SELECT lower("+table_schema.getColumn("","zilla_zillanameeng")+") as zillaname from " + table_schema.getTable("table_zilla") + " where "+table_schema.getColumn("","zilla_zillaid",new String[]{Integer.toString(zilla)},"=");
			
			//ResultSet rs = statement.executeQuery(sql_servername);
			ResultSet rs = dbOp.dbExecute(dbObject,sql_servername).getResultSet();
			
			
			if(rs.next()){
					servername = rs.getString("zillaname")+"-"+String.format("%02d", upazila);
					//space removed as coxs bazar exist in zilla table. we need coxsbazar
					servername = servername.replaceAll("\\s+","");

			}
			System.out.println(servername);
			
			nodeid = zilla+"-"+String.format("%02d", upazila);

			//option:1->create sym chanel onetime
			if(formtype.equals("1"))
			{
				sql_sym1 = "SELECT fn_create_sym_channel_onetime('"+servername+"','"+nodeid+"')";
				dbOp.dbExecute(dbObject,sql_sym1);

				sql_sym2 = "SELECT fn_create_sym_channel('ancservice,clientmap,clientmap_extension,death,delivery,elco,fpinfo,immunizationhistory,newborn,pacservice,pncservicechild,pncservicemother,pregwomen,regserial,fpmethods,fpmapping,fpexamination,gpservice,iudservice,iudfollowupservice,implantservice,implantfollowupservice,pillcondomservice,womaninjectable,permanent_method_service,permanent_method_followup_service,child_care_service,child_care_service_detail','"+servername+"'," +bothWaySync+")";
				System.out.println(sql_sym2);
				//statement.executeQuery(sql_sym2);
				dbOp.dbExecute(dbObject,sql_sym2);

				sql_sym3 = "SELECT fn_create_sym_channel('member,treatmentlist,diseaselist,symptomlist','"+servername+"'," +serverToNodeSync+")";
				System.out.println(sql_sym3);
				dbOp.dbExecute(dbObject,sql_sym3);

				sql_sym4 = "SELECT fn_create_sym_channel('login_location_tracking','"+servername+"'," +nodeToServerSync+")";
				System.out.println(sql_sym4);
				dbOp.dbExecute(dbObject,sql_sym4);

			}else{

				// both way
				if(synctype == bothWaySync) {
                	sql_sym3 = "SELECT fn_create_sym_channel('"+tablename+"','"+servername+"',"+bothWaySync+")";
					dbOp.dbExecute(dbObject, sql_sym3);
				// node to server
				}else if(synctype == nodeToServerSync) {
					sql_sym2 = "SELECT fn_create_sym_channel('"+tablename+"','"+servername+"',"+nodeToServerSync+")";
					dbOp.dbExecute(dbObject,sql_sym2);
				// server to node
				}else{
					sql_sym1 = "SELECT fn_create_sym_channel('"+tablename+"','"+servername+"',"+serverToNodeSync+")";
					System.out.println(sql_sym3);
					dbOp.dbExecute(dbObject, sql_sym1);
				}
			}
			
			status = true;
			
//			rs.close();
//			statement.close();
//	        c.close();
			
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
			
		}
		catch(Exception e){
			System.out.println(e);
			status = false;
		}
		
		return status;
	}

	public static String getDropdown(JSONObject geoInfo) {
		
//		DBOperation dbOp = new DBOperation();
//		DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();

		try{
			zilla = Integer.parseInt(geoInfo.getString("zilla"));
			upazila = Integer.parseInt(geoInfo.getString("upazila"));
			
//			DB_name = "RHIS_"+zilla+"_"+String.format("%02d", upazila);
//			
//			Connection c = DriverManager.getConnection("jdbc:postgresql://10.10.10.17:5432/"+DB_name, "postgres", "postgres");
//			c.setAutoCommit(false);
//			Statement statement = c.createStatement();
			
			DBOperation dbOp = new DBOperation();
			DBInfoHandler dbObject = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
			
			sql_dropdown = "select table_name from information_schema.tables where table_schema='public' and table_type='BASE TABLE' and table_name NOT LIKE 'sym_%' and table_name NOT IN (select channel_id from sym_channel) order by table_name asc";
			
			dropdown = "<select class=\"form-control select2\" id=\"TableName\" style=\"width: 100%;\"><option value=\"\">--Select Table Name--</option>";
			
			//ResultSet rs_dropdown = statement.executeQuery(sql_dropdown);
			
			ResultSet rs_dropdown = dbOp.dbExecute(dbObject,sql_dropdown).getResultSet();
			
			while(rs_dropdown.next()){
				dropdown += "<option value=\""+rs_dropdown.getString("table_name")+"\">"+rs_dropdown.getString("table_name")+"</option>";
			}
			
			dropdown += "</select>";
			
//			rs_dropdown.close();
//			statement.close();
//	        c.close();
			
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return dropdown;
	}

}
