package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.db.DBSchemaInfo;

public class ServiceStatistics {

    public static String type;
    public static String reportViewType;
    public static String methodType;
    public static String zilla;
    public static String upazila;
    public static String resultString;
    public static String sql;
    public static String date_type;
    public static String monthSelect;
    public static String yearSelect;
    public static String start_date;
    public static String end_date;
    public static String db_name;
    public static String reportViewOption;

    public static String getResultSet(JSONObject reportCred) {
        resultString = "";
        try {
            if (reportCred.has("reportViewType")) {
                reportViewType = reportCred.getString("reportViewType");
            } else {
                reportViewType = "";
            }

            if (reportCred.has("reportViewOption")) {
                reportViewOption = reportCred.getString("reportViewOption");
            } else {
                reportViewOption = "";
            }
            methodType = reportCred.getString("methodType");
            zilla = reportCred.getString("report1_zilla");
            upazila = reportCred.getString("report1_upazila");
            date_type = reportCred.getString("report1_dateType");
            type = reportCred.getString("type");

            if (date_type.equals("1")) {
                monthSelect = reportCred.getString("report1_month");
                String[] date = monthSelect.split("-");
                int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                start_date = monthSelect + "-01";
                end_date = monthSelect + "-" + daysInMonth;
            } else if (date_type.equals("3")) {
                yearSelect = reportCred.getString("report1_year");
                start_date = yearSelect + "-01-01";
                end_date = yearSelect + "-12-31";
            } else {
                start_date = reportCred.getString("report1_start_date");
                end_date = reportCred.getString("report1_end_date");
            }

            /**
             * reportViewOption = "" for flu like symptoms
             * reportViewOption = 1 serevice statistics aggregate data
             * reportViewOption = 2 serevice statistics monthwise data
             */
            if (reportViewType.equals("1")) {
                if (reportViewOption.equals("1") || reportViewOption.equals("")) {
                    resultString = GetResultSet.getFinalResultZillaWise(reportViewType, methodType, type, start_date, end_date);
                } else if (reportViewOption.equals("2")) {
                    resultString = GetResultTimeSeries.getFinalResultZillaWise_timeseries(reportViewType, methodType, type, start_date, end_date);
                }
            } else if (reportViewType.equals("2")) {
                if (reportViewOption.equals("1")|| reportViewOption.equals("")) {
                    resultString = GetResultSet.getFinalResultUpazilaWise(reportViewType, methodType, type, zilla, start_date, end_date);
                } else if (reportViewOption.equals("2")) {
                    resultString = GetResultTimeSeries.getFinalResultUpazilaWise_timeseries(reportViewType, methodType, type, zilla, start_date, end_date);
                }
            } else if (reportViewType.equals("3")) {
                if (reportViewOption.equals("1")|| reportViewOption.equals("")) {
                    resultString = GetResultSet.getResultUnionWise(reportViewType, methodType, type, zilla, upazila, start_date, end_date);
                } else if (reportViewOption.equals("2")) {
                    resultString = GetResultTimeSeries.getResultUnionWise_timeseries(reportViewType, methodType, type, zilla, upazila, start_date, end_date);
                }
            } else if (reportViewType.equals("4")) {
                if (reportViewOption.equals("1")|| reportViewOption.equals("")) {
                    resultString = GetResultSet.getResultUnionWise(reportViewType, methodType, type, zilla, upazila, start_date, end_date);
                } else if (reportViewOption.equals("2")) {
                    resultString = GetResultTimeSeries.getResultUnionWise_timeseries(reportViewType, methodType, type, zilla, upazila, start_date, end_date);
                }
            } else if (type.equals("1") || type.equals("11"))
                resultString = GetResultSet.getFinalResultProviderWise(reportViewType, methodType, type, zilla, upazila, start_date, end_date);

            //System.out.println(sql);


        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }

        return resultString;
    }

    public static String getSql(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date) {
        String create_sql = "";
        DBSchemaInfo table_schema = null;
        if (zilla.equals("36") && upazila.equals("71"))
            table_schema = new DBSchemaInfo();
        else
            table_schema = new DBSchemaInfo(zilla);

        try {
            String sql_condition = "";

            if (sql_view_type.equals("1"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype", new String[]{"4", "5", "6", "101", "17"}, "in") + " ";
            else if (sql_view_type.equals("2"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype", new String[]{"4", "5", "6", "101", "17"}, "in") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " ";
            else if (sql_view_type.equals("3") || form_type.equals("1"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype", new String[]{"4", "5", "6", "101", "17"}, "in") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=") + " and " + table_schema.getColumn("", "providerdb_endate", new String[]{end_date}, "<=") + " ";
            else if (form_type.equals("11"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype", new String[]{"2", "3"}, "in") + " and " + table_schema.getColumn("table", "providerdb_csba", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=") + " and " + table_schema.getColumn("", "providerdb_endate", new String[]{end_date}, "<=") + " ";

//			if(sql_view_type.equals("1"))
//				sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17","2","3"},"in") + " ";
//			else if(sql_view_type.equals("2"))
//				sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17","2","3"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " ";
//			else if(sql_view_type.equals("3") || form_type.equals("1"))
//				sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17","2","3"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " and " + table_schema.getColumn("", "providerdb_endate",new String[]{end_date},"<=") + " ";
//			else if(form_type.equals("11"))
//				sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"2","3"},"in") + " and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +" and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " and " + table_schema.getColumn("", "providerdb_endate",new String[]{end_date},"<=") + " ";

            String select_element = "";
            if (sql_view_type.equals("3"))
                select_element = table_schema.getColumn("unions", "unions_unionid") + ", unions.\"UnionName\", ";
            else if (form_type.equals("1") || form_type.equals("11")) {
                select_element = table_schema.getColumn("table", "providerdb_provname") + " \"Provider Name\", " + table_schema.getColumn("table", "providerdb_provcode") + " as \"Provider ID\", " +
                        table_schema.getColumn("table", "providertype_typename") + ", " + table_schema.getColumn("table", "providerdb_mobileno") + ", " + table_schema.getColumn("table", "providerdb_facilityname") + ", unions.\"UnionName\", " +
                        "(CASE WHEN ct_prov.count_provider>1 THEN (" + table_schema.getColumn("table", "providerdb_provname") + " || '(' || unions.\"UnionName\" || ')') ELSE " + table_schema.getColumn("table", "providerdb_provname") + " END) AS \"ProvName\", ";
            } else
                select_element = "";

            String sum = "";
            if (form_type.equals("1") || form_type.equals("11"))
                sum = "";
            else
                sum = "SUM";

            if (sql_method_type.equals("1") || form_type.equals("11")) {
                create_sql = "select " + select_element +
                        sum + "(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " +
                        sum + "(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " +
                        sum + "(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " +
                        sum + "(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " +
                        sum + "(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) as \"Delivery\",  " +
//						sum+"(CASE WHEN delivery.livebirth IS NULL THEN 0 ELSE delivery.livebirth END) as \"Live Birth\",  " +
//						sum+"(CASE WHEN delivery.stillbirth IS NULL THEN 0 ELSE delivery.stillbirth END) as \"Still Birth\",  " +
//						sum+"(CASE WHEN delivery.amtsl IS NULL THEN 0 ELSE delivery.amtsl END) as \"AMTSL\",  " +
                        sum + "(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " +
                        sum + "(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " +
                        sum + "(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " +
                        sum + "(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " +
                        sum + "(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " +
                        sum + "(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " +
                        sum + "(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " +
                        sum + "(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\"  " +
                        //sum+"((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " +
                        "from " + table_schema.getTable("table_providerdb") + " " +
                        "left join  " +
                        "(select " + table_schema.getColumn("table", "ancservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"ANC\",  " +
                        "count(case when " + table_schema.getColumn("", "ancservice_serviceid", new String[]{"1"}, "=") + " then 1 else null end) as \"ANC1\",  " +
                        "count(case when " + table_schema.getColumn("", "ancservice_serviceid", new String[]{"2"}, "=") + " then 1 else null end) as \"ANC2\",  " +
                        "count(case when " + table_schema.getColumn("", "ancservice_serviceid", new String[]{"3"}, "=") + " then 1 else null end) as \"ANC3\",  " +
                        "count(case when " + table_schema.getColumn("", "ancservice_serviceid", new String[]{"4"}, "=") + " then 1 else null end) as \"ANC4\"  " +
                        "from  " + table_schema.getTable("table_ancservice") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "ancservice_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "ancservice_providerid") + " " +
                        "where ";

                if (form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "ancservice_client", new String[]{"2", "22"}, "in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "ancservice_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "ancservice_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end)  " +
                        "else " + table_schema.getColumn("", "ancservice_visitdate", new String[]{start_date, end_date}, "between") + " end) " +
                        "AND (" + table_schema.getColumn("", "ancservice_servicesource", new String[]{}, "isnull") + " OR " + table_schema.getColumn("", "ancservice_serviceid", new String[]{"0"}, "=") + ") AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "ancservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as anc on anc." + table_schema.getColumn("", "ancservice_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and anc." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND anc." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND anc." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + "    " +
                        "left join   " +
                        "(select " + table_schema.getColumn("table", "pncservicemother_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"PNC\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid", new String[]{"1"}, "=") + " then 1 else null end) as \"PNC1\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid", new String[]{"2"}, "=") + " then 1 else null end) as \"PNC2\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid", new String[]{"3"}, "=") + " then 1 else null end) as \"PNC3\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid", new String[]{"4"}, "=") + " then 1 else null end) as \"PNC4\"  " +
                        "from  " + table_schema.getTable("table_pncservicemother") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "pncservicemother_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "pncservicemother_providerid") + " " +
                        "where ";

                if (form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "pncservicemother_client", new String[]{"2", "22"}, "in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "pncservicemother_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "pncservicemother_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end)  " +
                        "else " + table_schema.getColumn("", "pncservicemother_visitdate", new String[]{start_date, end_date}, "between") + " end) " +
                        "AND (" + table_schema.getColumn("", "pncservicemother_servicesource", new String[]{}, "isnull") + " OR " + table_schema.getColumn("", "pncservicemother_servicesource", new String[]{"0"}, "=") + ") AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "pncservicemother_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as pnc on pnc." + table_schema.getColumn("", "pncservicemother_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and pnc." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND pnc." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND pnc." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + "    " +
                        "left join   " +
                        "(select " + table_schema.getColumn("table", "pncservicechild_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"PNC-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid", new String[]{"1"}, "=") + " then 1 else null end) as \"PNC1-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid", new String[]{"2"}, "=") + " then 1 else null end) as \"PNC2-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid", new String[]{"3"}, "=") + " then 1 else null end) as \"PNC3-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid", new String[]{"4"}, "=") + " then 1 else null end) as \"PNC4-N\"  " +
                        "from  " + table_schema.getTable("table_pncservicechild") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "pncservicechild_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "pncservicechild_providerid") + " " +
                        "where ";

                if (form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "pncservicechild_client", new String[]{"2", "22"}, "in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "pncservicechild_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "pncservicechild_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end)  " +
                        "else " + table_schema.getColumn("", "pncservicechild_visitdate", new String[]{start_date, end_date}, "between") + " end) " +
                        "AND (" + table_schema.getColumn("", "pncservicechild_servicesource", new String[]{}, "isnull") + " OR " + table_schema.getColumn("", "pncservicechild_servicesource", new String[]{"0"}, "=") + ") AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "pncservicechild_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as pncc on pncc." + table_schema.getColumn("", "pncservicechild_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and pncc." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND pncc." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND pncc." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + "    " +
                        "left join   " +
                        "(select " + table_schema.getColumn("table", "delivery_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"Delivery\", " +
                        "COUNT(CASE WHEN (" + table_schema.getColumn("table", "delivery_applyoxytocin", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("table", "delivery_applytraction", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("table", "delivery_uterusmassage", new String[]{"1"}, "=") + ") THEN 1 ELSE NULL END) AS amtsl, " +
                        "SUM(" + table_schema.getColumn("table", "delivery_livebirth") + ") as livebirth, " +
                        "SUM(" + table_schema.getColumn("table", "delivery_stillbirth") + ") as stillbirth " +
                        "from  " + table_schema.getTable("table_delivery") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "delivery_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "delivery_providerid") + " " +
                        "where ";

                if (form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "delivery_client", new String[]{"2", "22"}, "in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "delivery_outcomedate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "delivery_outcomedate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end)  " +
                        "else " + table_schema.getColumn("", "delivery_outcomedate", new String[]{start_date, end_date}, "between") + " end) ";

                if (!form_type.equals("11"))
                    create_sql += "AND " + table_schema.getColumn("", "delivery_deliverydonebythisprovider", new String[]{"1"}, "=") + " ";

                create_sql += "AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "delivery_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as delivery on delivery." + table_schema.getColumn("", "delivery_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and delivery." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND delivery." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND delivery." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + "    " +
                        "join  " + table_schema.getTable("table_providertype") + " on " + table_schema.getColumn("table", "providerdb_provtype") + "=" + table_schema.getColumn("table", "providertype_provtype") + "   ";

                if (form_type.equals("1") || form_type.equals("11")) {
                    create_sql += "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                            "and " + sql_condition + " " +
                            "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " ";
                }

                if (sql_view_type.equals("3") || form_type.equals("1") || form_type.equals("11"))
                    create_sql += "join  (select " + table_schema.getColumn("", "unions_zillaid") + "," + table_schema.getColumn("", "unions_upazilaid") + "," + table_schema.getColumn("", "unions_unionid") + "," + table_schema.getColumn("", "unions_unionnameeng") + " as \"UnionName\" from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid") + "=" + zilla + " and " + table_schema.getColumn("", "unions_upazilaid") + "=" + upazila + ") as unions on unions." + table_schema.getColumn("", "unions_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND unions." + table_schema.getColumn("", "unions_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND unions." + table_schema.getColumn("", "unions_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + "   ";

                create_sql += "where  " + sql_condition + " " +
                        "AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") ";

                if (sql_view_type.equals("3"))
                    create_sql += "GROUP BY unions.\"UnionName\", " + table_schema.getColumn("unions", "unions_unionid") + " ";
            } else if (sql_method_type.equals("2")) {

                create_sql = "select " + select_element +
                        sum + "(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", " +
                        sum + "(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", " +
                        sum + "(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  " +
                        sum + "(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  " +
                        sum + "(CASE WHEN implant.\"IMPLANT\" IS NULL THEN 0 ELSE implant.\"IMPLANT\" END) AS \"IMPLANT\",  " +
                        sum + "(CASE WHEN implantfollowup.\"IMPLANT Followup\" IS NULL THEN 0 ELSE implantfollowup.\"IMPLANT Followup\" END) AS \"IMPLANT Followup\",  " +
                        sum + "(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\"  " +
                        //sum+"((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"IMPLANT\" is NULL then 0 else \"IMPLANT\" END) + (case when \"IMPLANT Followup\" is NULL then 0 else \"IMPLANT Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\" " +
                        "from " + table_schema.getTable("table_providerdb") + "   " +
                        "left join  " +
                        "(select " + table_schema.getColumn("table", "pillcondomservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + "," +
                        "COUNT(CASE WHEN (" + table_schema.getColumn("", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") THEN 1 ELSE NULL END) as \"Pill\", " +
                        "COUNT(CASE WHEN " + table_schema.getColumn("", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " THEN 1 ELSE NULL END) as \"Condom\" " +
                        "from " + table_schema.getTable("table_pillcondomservice") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "pillcondomservice_providerid") + "" +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "pillcondomservice_providerid") + " " +
                        "where (case when count_provider>1 then " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "pillcondomservice_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "pillcondomservice_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
                        "else " + table_schema.getColumn("", "pillcondomservice_visitdate", new String[]{start_date, end_date}, "between") + " end) AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "pillcondomservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as pillcondom on pillcondom." + table_schema.getColumn("", "pillcondomservice_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and pillcondom." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND pillcondom." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND pillcondom." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + " " +
                        "left join   " +
                        "(select " + table_schema.getColumn("table", "iudservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"IUD\" " +
                        "from " + table_schema.getTable("table_iudservice") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "iudservice_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "iudservice_providerid") + " " +
                        "where (case when count_provider>1 then " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "iudservice_iudimplantdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "iudservice_iudimplantdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
                        "else " + table_schema.getColumn("", "iudservice_iudimplantdate", new String[]{start_date, end_date}, "between") + " end) AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "iudservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as iud on iud." + table_schema.getColumn("", "iudservice_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and iud." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND iud." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND iud." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + " " +
                        "left join " +
                        "(select " + table_schema.getColumn("table", "iudfollowupservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"IUD Followup\" " +
                        "from " + table_schema.getTable("table_iudfollowupservice") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "iudfollowupservice_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "iudfollowupservice_providerid") + " " +
                        "where (case when count_provider>1 then " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "iudfollowupservice_followupdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "iudfollowupservice_followupdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
                        "else " + table_schema.getColumn("", "iudfollowupservice_followupdate", new String[]{start_date, end_date}, "between") + " end) AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "iudfollowupservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as iudfollowup on iudfollowup." + table_schema.getColumn("", "iudfollowupservice_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and iudfollowup." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND iudfollowup." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND iudfollowup." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + " " +
                        "left join   " +
                        "(select " + table_schema.getColumn("table", "implantservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"IMPLANT\" " +
                        "from " + table_schema.getTable("table_implantservice") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "implantservice_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "implantservice_providerid") + " " +
                        "where (case when count_provider>1 then " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "implantservice_implantimplantdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "implantservice_implantimplantdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
                        "else " + table_schema.getColumn("", "implantservice_implantimplantdate", new String[]{start_date, end_date}, "between") + " end) AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "implantservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as implant on implant." + table_schema.getColumn("", "implantservice_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and implant." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND implant." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND implant." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + " " +
                        "left join " +
                        "(select " + table_schema.getColumn("table", "implantfollowupservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"IMPLANT Followup\" " +
                        "from " + table_schema.getTable("table_implantfollowupservice") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "implantfollowupservice_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "implantfollowupservice_providerid") + " " +
                        "where (case when count_provider>1 then " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "implantfollowupservice_followupdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "implantfollowupservice_followupdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
                        "else " + table_schema.getColumn("", "implantfollowupservice_followupdate", new String[]{start_date, end_date}, "between") + " end) AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "implantfollowupservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as implantfollowup on implantfollowup." + table_schema.getColumn("", "implantfollowupservice_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and implantfollowup." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND implantfollowup." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND implantfollowup." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + " " +
                        "left join   " +
                        "(select " + table_schema.getColumn("table", "womaninjectable_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ",count (*) as \"Injectable\" " +
                        "from " + table_schema.getTable("table_womaninjectable") + " " +
                        "join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "womaninjectable_providerid") + " " +
                        "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                        "and " + sql_condition + " " +
                        "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "womaninjectable_providerid") + " " +
                        "where (case when count_provider>1 then " +
                        "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "womaninjectable_dosedate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "womaninjectable_dosedate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
                        "else " + table_schema.getColumn("", "womaninjectable_dosedate", new String[]{start_date, end_date}, "between") + " end) AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") group by " + table_schema.getColumn("table", "womaninjectable_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + ") as injectable on injectable." + table_schema.getColumn("", "womaninjectable_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and injectable." + table_schema.getColumn("", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND injectable." + table_schema.getColumn("", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND injectable." + table_schema.getColumn("", "providerdb_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + " ";

                if (form_type.equals("1")) {
                    create_sql += "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                            "and " + sql_condition + " " +
                            "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " ";
                }
                if (form_type.equals("1"))
                    create_sql += "join  " + table_schema.getTable("table_providertype") + " on " + table_schema.getColumn("table", "providerdb_provtype") + "=" + table_schema.getColumn("table", "providertype_provtype") + "   ";

                if (sql_view_type.equals("3") || form_type.equals("1"))
                    create_sql += "join  (select " + table_schema.getColumn("", "unions_zillaid") + "," + table_schema.getColumn("", "unions_upazilaid") + "," + table_schema.getColumn("", "unions_unionid") + "," + table_schema.getColumn("", "unions_unionnameeng") + " as \"UnionName\" from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid") + "=" + zilla + " and " + table_schema.getColumn("", "unions_upazilaid") + "=" + upazila + ") as unions on unions." + table_schema.getColumn("", "unions_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND unions." + table_schema.getColumn("", "unions_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND unions." + table_schema.getColumn("", "unions_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + "   ";

                create_sql += "where " + sql_condition + " AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") ";

                if (sql_view_type.equals("3"))
                    create_sql += "GROUP BY unions.\"UnionName\", " + table_schema.getColumn("unions", "unions_unionid") + " ";

                //System.out.println(create_sql);

            } else {
                create_sql = "select " + select_element
                        + "COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + ") THEN 1 ELSE NULL END) AS \"Male\", "
                        + "COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + ") THEN 1 ELSE NULL END) AS \"Female\" "
                        //+ "(COUNT(CASE WHEN ("+ table_schema.getColumn("table", "clientmap_gender",new String[]{"1"},"=") +") THEN 1 ELSE NULL END)+COUNT(CASE WHEN ("+ table_schema.getColumn("table", "clientmap_gender",new String[]{"2"},"=") +") THEN 1 ELSE NULL END)) AS \"TOTAL\" "
                        + "from " + table_schema.getTable("table_providerdb") + "   "
                        + "left join  "
                        + "" + table_schema.getTable("table_gpservice") + " on " + table_schema.getColumn("table", "gpservice_providerid") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " and " + table_schema.getColumn("", "gpservice_visitdate", new String[]{start_date, end_date}, "between") + " "
                        + "left join "
                        + "" + table_schema.getTable("table_clientmap") + " on " + table_schema.getColumn("table", "clientmap_generatedid") + "=" + table_schema.getColumn("table", "gpservice_healthid") + " "
                        + "join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") "
                        + "and " + sql_condition + " "
                        + "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " ";

                if (form_type.equals("1"))
                    create_sql += "join  " + table_schema.getTable("table_providertype") + " on " + table_schema.getColumn("table", "providerdb_provtype") + "=" + table_schema.getColumn("table", "providertype_provtype") + "   ";

                if (sql_view_type.equals("3") || form_type.equals("1"))
                    create_sql += "join  (select " + table_schema.getColumn("", "unions_zillaid") + "," + table_schema.getColumn("", "unions_upazilaid") + "," + table_schema.getColumn("", "unions_unionid") + "," + table_schema.getColumn("", "unions_unionnameeng") + " as \"UnionName\" from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid") + "=" + zilla + " and " + table_schema.getColumn("", "unions_upazilaid") + "=" + upazila + ") as unions on unions." + table_schema.getColumn("", "unions_zillaid") + "=" + table_schema.getColumn("table", "providerdb_zillaid") + " AND unions." + table_schema.getColumn("", "unions_upazilaid") + "=" + table_schema.getColumn("table", "providerdb_upazilaid") + " AND unions." + table_schema.getColumn("", "unions_unionid") + "=" + table_schema.getColumn("table", "providerdb_unionid") + "   ";

                create_sql += "where (case when " + table_schema.getColumn("", "gpservice_visitdate") + " is not null then (case when count_provider>1 then "
                        + "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "gpservice_visitdate") + " between '" + start_date + " ' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "gpservice_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + " ' end) "
                        + "else " + table_schema.getColumn("", "gpservice_visitdate", new String[]{start_date, end_date}, "between") + " end) else " + table_schema.getColumn("", "gpservice_visitdate") + " is null end) "
                        + " AND " + sql_condition + " AND (" + table_schema.getColumn("table", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("table", "providerdb_exdate", new String[]{}, "isnull") + ") ";
                if (sql_view_type.equals("3"))
                    create_sql += "GROUP BY unions.\"UnionName\", " + table_schema.getColumn("unions", "unions_unionid") + " ";
                else if (form_type.equals("1"))
                    create_sql += "GROUP BY " + table_schema.getColumn("table", "providerdb_provcode") + ", " + table_schema.getColumn("table", "providerdb_provname") + ", " + table_schema.getColumn("table", "providertype_typename") + ", " + table_schema.getColumn("table", "providerdb_mobileno") + ", " + table_schema.getColumn("table", "providerdb_facilityname") + ", ct_prov.count_provider, unions.\"UnionName\" ";

                //create_sql += "order by \"TOTAL\" desc";
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(create_sql);
        return create_sql;
    }

    public static String getResultSetRHS(JSONObject reportCred) {

        try {
            reportViewType = reportCred.getString("reportViewType");
            methodType = reportCred.getString("methodType");
            zilla = reportCred.getString("report1_zilla");
            upazila = reportCred.getString("report1_upazila");
            date_type = reportCred.getString("report1_dateType");

            //db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));

            if (date_type.equals("1")) {
                monthSelect = reportCred.getString("report1_month");
                String[] date = monthSelect.split("-");
                //YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                //int daysInMonth = yearMonthObject.lengthOfMonth();
                int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                start_date = monthSelect + "-01";
                end_date = monthSelect + "-" + daysInMonth;
            } else {
                start_date = reportCred.getString("report1_start_date");
                end_date = reportCred.getString("report1_end_date");
            }

            if (methodType.equals("1")) {
                if (reportViewType.equals("1")) {
                    sql = "select "
                            + "anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
                            + "delivery.\"Normal\", delivery.\"C-Section\", delivery.\"AMTSL\", delivery.\"Live brith\", delivery.\"Still brith\", "
                            + "pnc.\"PNC1-M\", pnc.\"PNC2-M\", pnc.\"PNC3-M\", pnc.\"PNC4-M\", pnc.\"Postpartum FP\", "
                            + "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\", "
                            + "(anc.\"ANCRef\" + delivery.\"DeliveryRef\" + pnc.\"PNCRef\") as \"Refer ANC, Delivery & PNC\", anc.\"Eclamsia patient by MgS04 Injection\", refer.\"New Born Refer\" "
                            + "from \"ProviderDB\" "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, count(\"ancService\".*) as \"Total ANC\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric < 5 then 1 else NULL end) as \"ANC1\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 5 and 6 then 1 else NULL end) as \"ANC2\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 7 and 8 then 1 else NULL end) as \"ANC3\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric > 8 then 1 else NULL end) as \"ANC4\", "
                            + "count(case when \"ancService\".\"refer\"=1 then 1 else NULL end) AS \"ANCRef\", "
                            + "count(case when (\"ancService\".treatment like '%\"2\"%' and (\"ancService\".client=2 OR \"ancService\".client=22)) then 1 else NULL end) AS \"Eclamsia patient by MgS04 Injection\" "
                            + "from \"ProviderDB\" "
                            + "left join \"ancService\" on \"ancService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join \"pregWomen\" on \"pregWomen\".\"healthId\"=\"ancService\".\"healthId\" and \"pregWomen\".\"pregNo\"=\"ancService\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and ((\"ancService\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') ) "
                            + "group by \"ProviderDB\".zillaid) as anc on \"ProviderDB\".zillaid=anc.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, "
                            + "count(case when (delivery.\"outcomeType\"=1) THEN 1 ELSE NULL END) AS \"Normal\", "
                            + "count(case when (delivery.\"outcomeType\"=2) THEN 1 ELSE NULL END) AS \"C-Section\", "
                            + "count(case when (delivery.\"applyOxytocin\"=1 and delivery.\"applyTraction\"=1 and delivery.\"uterusMassage\"=1) then 1 else NULL end) AS \"AMTSL\", "
                            + "(case when (sum(delivery.\"liveBirth\") is not NULL) then sum(delivery.\"liveBirth\") else 0 end) AS \"Live brith\", "
                            + "(case when (sum(delivery.\"stillBirth\") is not NULL) then sum(delivery.\"stillBirth\") else 0 end) AS \"Still brith\", "
                            + "count(case when delivery.\"refer\"=1 then 1 else NULL end) AS \"DeliveryRef\" "
                            + "from \"ProviderDB\" "
                            + "left join delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "and ((delivery.\"outcomeDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (delivery.\"deliveryDoneByThisProvider\" = '1') ) "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "group by \"ProviderDB\".zillaid) as delivery on \"ProviderDB\".zillaid=delivery.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, count(\"pncServiceMother\".*) as \"Total PNC\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric > 41 then 1 else NULL end) as \"PNC4-M\", "
                            + "count(case when (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1') then 1 else NULL end) as \"Postpartum FP\", "
                            + "count(case when \"pncServiceMother\".\"refer\"=1 then 1 else NULL end) AS \"PNCRef\" "
                            + "from \"ProviderDB\" "
                            + "left join \"pncServiceMother\" on \"pncServiceMother\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join delivery on delivery.\"healthId\"=\"pncServiceMother\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceMother\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and ((\"pncServiceMother\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') ) "
                            + "group by \"ProviderDB\".zillaid) as pnc on \"ProviderDB\".zillaid=pnc.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, count(\"pncServiceChild\".*) as \"Total PNC-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric > 41 then 1 else NULL end) as \"PNC4-N\" "
                            + "from \"ProviderDB\" "
                            + "left join \"pncServiceChild\" on \"pncServiceChild\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join delivery on delivery.\"healthId\"=\"pncServiceChild\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and ((\"pncServiceChild\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') ) "
                            + "group by \"ProviderDB\".zillaid) as pncn on \"ProviderDB\".zillaid=pncn.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, "
                            + "(count(case when \"newBorn\".\"refer\" = 1 then 1 else NULL end) + count(case when (\"pncServiceChild\".\"refer\" = 1 AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= 28) then 1 else NULL end)) as \"New Born Refer\" "
                            + "from \"ProviderDB\" "
                            + "join \"newBorn\" on \"newBorn\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "join \"pncServiceChild\" on \"newBorn\".\"healthId\"=\"pncServiceChild\".\"healthId\" and \"newBorn\".\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + " ' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and (\"newBorn\".\"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') and (\"pncServiceChild\".\"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') "
                            + "group by \"ProviderDB\".zillaid) as refer on \"ProviderDB\".zillaid=refer.zillaid "
                            + "where \"ProviderDB\".zillaid=" + zilla + " "
                            + "group by anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
                            + "delivery.\"Normal\", delivery.\"C-Section\", delivery.\"AMTSL\", delivery.\"Live brith\", delivery.\"Still brith\", "
                            + "pnc.\"PNC1-M\", pnc.\"PNC2-M\", pnc.\"PNC3-M\", pnc.\"PNC4-M\", pnc.\"Postpartum FP\", "
                            + "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\", anc.\"ANCRef\", delivery.\"DeliveryRef\", pnc.\"PNCRef\", anc.\"Eclamsia patient by MgS04 Injection\", refer.\"New Born Refer\"";


                    resultString = FWCRHSResultSet.getFinalResultZillaWise(sql, zilla);
                } else if (reportViewType.equals("2")) {
                    sql = "select "
                            + "anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
                            + "delivery.\"Normal\", delivery.\"C-Section\", delivery.\"AMTSL\", delivery.\"Live brith\", delivery.\"Still brith\", "
                            + "pnc.\"PNC1-M\", pnc.\"PNC2-M\", pnc.\"PNC3-M\", pnc.\"PNC4-M\", pnc.\"Postpartum FP\", "
                            + "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\", "
                            + "(anc.\"ANCRef\" + delivery.\"DeliveryRef\" + pnc.\"PNCRef\") as \"Refer ANC, Delivery & PNC\", anc.\"Eclamsia patient by MgS04 Injection\", refer.\"New Born Refer\" "
                            + "from \"ProviderDB\" "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, count(\"ancService\".*) as \"Total ANC\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric < 5 then 1 else NULL end) as \"ANC1\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 5 and 6 then 1 else NULL end) as \"ANC2\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 7 and 8 then 1 else NULL end) as \"ANC3\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric > 8 then 1 else NULL end) as \"ANC4\", "
                            + "count(case when \"ancService\".\"refer\"=1 then 1 else NULL end) AS \"ANCRef\", "
                            + "count(case when (\"ancService\".treatment like '%\"2\"%' and (\"ancService\".client=2 OR \"ancService\".client=22)) then 1 else NULL end) AS \"Eclamsia patient by MgS04 Injection\" "
                            + "from \"ProviderDB\" "
                            + "left join \"ancService\" on \"ancService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join \"pregWomen\" on \"pregWomen\".\"healthId\"=\"ancService\".\"healthId\" and \"pregWomen\".\"pregNo\"=\"ancService\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and ((\"ancService\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') ) "
                            + "group by \"ProviderDB\".zillaid) as anc on \"ProviderDB\".zillaid=anc.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, "
                            + "count(case when (delivery.\"outcomeType\"=1) THEN 1 ELSE NULL END) AS \"Normal\", "
                            + "count(case when (delivery.\"outcomeType\"=2) THEN 1 ELSE NULL END) AS \"C-Section\", "
                            + "count(case when (delivery.\"applyOxytocin\"=1 and delivery.\"applyTraction\"=1 and delivery.\"uterusMassage\"=1) then 1 else NULL end) AS \"AMTSL\", "
                            + "(case when (sum(delivery.\"liveBirth\") is not NULL) then sum(delivery.\"liveBirth\") else 0 end) AS \"Live brith\", "
                            + "(case when (sum(delivery.\"stillBirth\") is not NULL) then sum(delivery.\"stillBirth\") else 0 end) AS \"Still brith\", "
                            + "count(case when delivery.\"refer\"=1 then 1 else NULL end) AS \"DeliveryRef\" "
                            + "from \"ProviderDB\" "
                            + "left join delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "and ((delivery.\"outcomeDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (delivery.\"deliveryDoneByThisProvider\" = '1') ) "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "group by \"ProviderDB\".zillaid) as delivery on \"ProviderDB\".zillaid=delivery.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, count(\"pncServiceMother\".*) as \"Total PNC\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric > 41 then 1 else NULL end) as \"PNC4-M\", "
                            + "count(case when (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1') then 1 else NULL end) as \"Postpartum FP\", "
                            + "count(case when \"pncServiceMother\".\"refer\"=1 then 1 else NULL end) AS \"PNCRef\" "
                            + "from \"ProviderDB\" "
                            + "left join \"pncServiceMother\" on \"pncServiceMother\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join delivery on delivery.\"healthId\"=\"pncServiceMother\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceMother\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and ((\"pncServiceMother\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') ) "
                            + "group by \"ProviderDB\".zillaid) as pnc on \"ProviderDB\".zillaid=pnc.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, count(\"pncServiceChild\".*) as \"Total PNC-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric > 41 then 1 else NULL end) as \"PNC4-N\" "
                            + "from \"ProviderDB\" "
                            + "left join \"pncServiceChild\" on \"pncServiceChild\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join delivery on delivery.\"healthId\"=\"pncServiceChild\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and ((\"pncServiceChild\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') ) "
                            + "group by \"ProviderDB\".zillaid) as pncn on \"ProviderDB\".zillaid=pncn.zillaid "
                            + "left join "
                            + "(select \"ProviderDB\".zillaid, "
                            + "(count(case when \"newBorn\".\"refer\" = 1 then 1 else NULL end) + count(case when (\"pncServiceChild\".\"refer\" = 1 AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= 28) then 1 else NULL end)) as \"New Born Refer\" "
                            + "from \"ProviderDB\" "
                            + "join \"newBorn\" on \"newBorn\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "join \"pncServiceChild\" on \"newBorn\".\"healthId\"=\"pncServiceChild\".\"healthId\" and \"newBorn\".\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + " ' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
                            + "and (\"newBorn\".\"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') and (\"pncServiceChild\".\"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') "
                            + "group by \"ProviderDB\".zillaid) as refer on \"ProviderDB\".zillaid=refer.zillaid "
                            + "where \"ProviderDB\".zillaid=" + zilla + " "
                            + "group by anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
                            + "delivery.\"Normal\", delivery.\"C-Section\", delivery.\"AMTSL\", delivery.\"Live brith\", delivery.\"Still brith\", "
                            + "pnc.\"PNC1-M\", pnc.\"PNC2-M\", pnc.\"PNC3-M\", pnc.\"PNC4-M\", pnc.\"Postpartum FP\", "
                            + "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\", anc.\"ANCRef\", delivery.\"DeliveryRef\", pnc.\"PNCRef\", anc.\"Eclamsia patient by MgS04 Injection\", refer.\"New Born Refer\"";


                    resultString = FWCRHSResultSet.getFinalResultUpazilaWise(sql, zilla);
                } else {
                    sql = "select \"Unions\".\"UNIONNAMEENG\" as \"Union Name\", "
                            + "anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
                            + "delivery.\"Normal\", delivery.\"C-Section\", delivery.\"AMTSL\", delivery.\"Live brith\", delivery.\"Still brith\", "
                            + "pnc.\"PNC1-M\", pnc.\"PNC2-M\", pnc.\"PNC3-M\", pnc.\"PNC4-M\", pnc.\"Postpartum FP\", "
                            + "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\", "
                            + "(anc.\"ANCRef\" + delivery.\"DeliveryRef\" + pnc.\"PNCRef\") as \"Refer ANC, Delivery & PNC\", anc.\"Eclamsia patient by MgS04 Injection\", refer.\"New Born Refer\" "
                            + "from \"Unions\" "
                            + "left join "
                            + "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", count(\"ancService\".*) as \"Total ANC\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric < 5 then 1 else NULL end) as \"ANC1\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 5 and 6 then 1 else NULL end) as \"ANC2\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 7 and 8 then 1 else NULL end) as \"ANC3\", "
                            + "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric > 8 then 1 else NULL end) as \"ANC4\", "
                            + "count(case when \"ancService\".\"refer\"=1 then 1 else NULL end) AS \"ANCRef\", "
                            + "count(case when (\"ancService\".treatment like '%\"2\"%' and (\"ancService\".client=2 OR \"ancService\".client=22)) then 1 else NULL end) AS \"Eclamsia patient by MgS04 Injection\" "
                            + "from \"ProviderDB\" "
                            + "left join \"ancService\" on \"ancService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join \"pregWomen\" on \"pregWomen\".\"healthId\"=\"ancService\".\"healthId\" and \"pregWomen\".\"pregNo\"=\"ancService\".\"pregNo\" "
                            + "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + " and \"ProviderDB\".upazilaid = " + upazila + ") "
                            + "and ((\"ancService\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') ) "
                            + "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as anc on \"Unions\".\"ZILLAID\"=anc.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=anc.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=anc.\"UNIONID\" "
                            + "left join "
                            + "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", "
                            + "count(case when (delivery.\"outcomeType\"=1) THEN 1 ELSE NULL END) AS \"Normal\", "
                            + "count(case when (delivery.\"outcomeType\"=2) THEN 1 ELSE NULL END) AS \"C-Section\", "
                            + "count(case when (delivery.\"applyOxytocin\"=1 and delivery.\"applyTraction\"=1 and delivery.\"uterusMassage\"=1) then 1 else NULL end) AS \"AMTSL\", "
                            + "(case when (sum(delivery.\"liveBirth\") is not NULL) then sum(delivery.\"liveBirth\") else 0 end) AS \"Live brith\", "
                            + "(case when (sum(delivery.\"stillBirth\") is not NULL) then sum(delivery.\"stillBirth\") else 0 end) AS \"Still brith\", "
                            + "count(case when delivery.\"refer\"=1 then 1 else NULL end) AS \"DeliveryRef\" "
                            + "from \"ProviderDB\" "
                            + "left join delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
                            + "and ((delivery.\"outcomeDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (delivery.\"deliveryDoneByThisProvider\" = '1') ) "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + " and \"ProviderDB\".upazilaid = " + upazila + ") "
                            + "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as delivery on \"Unions\".\"ZILLAID\"=delivery.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=delivery.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=delivery.\"UNIONID\" "
                            + "left join "
                            + "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", count(\"pncServiceMother\".*) as \"Total PNC\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-M\", "
                            + "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric > 41 then 1 else NULL end) as \"PNC4-M\", "
                            + "count(case when (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1') then 1 else NULL end) as \"Postpartum FP\", "
                            + "count(case when \"pncServiceMother\".\"refer\"=1 then 1 else NULL end) AS \"PNCRef\" "
                            + "from \"ProviderDB\" "
                            + "left join \"pncServiceMother\" on \"pncServiceMother\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join delivery on delivery.\"healthId\"=\"pncServiceMother\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceMother\".\"pregNo\" "
                            + "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + " and \"ProviderDB\".upazilaid = " + upazila + ") "
                            + "and ((\"pncServiceMother\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') ) "
                            + "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as pnc on \"Unions\".\"ZILLAID\"=pnc.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=pnc.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=pnc.\"UNIONID\" "
                            + "left join "
                            + "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", count(\"pncServiceChild\".*) as \"Total PNC-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-N\", "
                            + "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric > 41 then 1 else NULL end) as \"PNC4-N\" "
                            + "from \"ProviderDB\" "
                            + "left join \"pncServiceChild\" on \"pncServiceChild\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "left join delivery on delivery.\"healthId\"=\"pncServiceChild\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
                            + "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + " and \"ProviderDB\".upazilaid = " + upazila + ") "
                            + "and ((\"pncServiceChild\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') ) "
                            + "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as pncn on \"Unions\".\"ZILLAID\"=pncn.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=pncn.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=pncn.\"UNIONID\" "
                            + "left join "
                            + "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", "
                            + "(count(case when \"newBorn\".\"refer\" = 1 then 1 else NULL end) + count(case when (\"pncServiceChild\".\"refer\" = 1 AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= 28) then 1 else NULL end)) as \"New Born Refer\" "
                            + "from \"ProviderDB\" "
                            + "join \"newBorn\" on \"newBorn\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
                            + "join \"pncServiceChild\" on \"newBorn\".\"healthId\"=\"pncServiceChild\".\"healthId\" and \"newBorn\".\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
                            + "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
                            + "where (\"ProviderDB\".\"ProvType\" in (4,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + " ' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + " and \"ProviderDB\".upazilaid = " + upazila + ") "
                            + "and (\"newBorn\".\"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') and (\"pncServiceChild\".\"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') "
                            + "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as refer on \"Unions\".\"ZILLAID\"=refer.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=refer.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=refer.\"UNIONID\" "
                            + "join \"ProviderDB\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
                            + "where \"Unions\".\"ZILLAID\"=" + zilla + " and \"Unions\".\"UPAZILAID\"=" + upazila + " "
                            + "and \"ProviderDB\".\"ProvType\" in (4,101,17) "
                            + "group by \"Unions\".\"UNIONNAMEENG\", anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
                            + "delivery.\"Normal\", delivery.\"C-Section\", delivery.\"AMTSL\", delivery.\"Live brith\", delivery.\"Still brith\", "
                            + "pnc.\"PNC1-M\", pnc.\"PNC2-M\", pnc.\"PNC3-M\", pnc.\"PNC4-M\", pnc.\"Postpartum FP\", "
                            + "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\", anc.\"ANCRef\", delivery.\"DeliveryRef\", pnc.\"PNCRef\", anc.\"Eclamsia patient by MgS04 Injection\", refer.\"New Born Refer\"";


                    resultString = FWCRHSResultSet.getFinalResult(sql, zilla, upazila);
                }
            } else {
                getResultSet(reportCred);
            }


            System.out.println(sql);


        } catch (Exception e) {
            System.out.println(e);
        }

        return resultString;
    }

    public static JSONObject getGraphSet(JSONObject reportCred) {

        JSONObject graphInfo = null;

        try {
            reportViewType = reportCred.getString("reportViewType");
            methodType = reportCred.getString("methodType");
            zilla = reportCred.getString("report1_zilla");
            upazila = reportCred.getString("report1_upazila");
            date_type = reportCred.getString("report1_dateType");

            //db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));

            if (date_type.equals("1")) {
                monthSelect = reportCred.getString("report1_month");
                String[] date = monthSelect.split("-");
                //YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                //int daysInMonth = yearMonthObject.lengthOfMonth();
                int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                start_date = monthSelect + "-01";
                end_date = monthSelect + "-" + daysInMonth;
            } else {
                start_date = reportCred.getString("report1_start_date");
                end_date = reportCred.getString("report1_end_date");
            }

            if (reportViewType.equals("1")) {
                if (methodType.equals("1")) {

                    sql = "select  " +
                            "SUM(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " +
                            "SUM(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " +
                            "SUM(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " +
                            "SUM(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " +
                            "SUM(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) as \"Delivery\",  " +
                            "SUM(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " +
                            "SUM(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\",  " +
                            "SUM((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " +
                            "from \"ProviderDB\"   " +
                            "left join  " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"ANC\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\"  " +
                            "from \"ancService\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"ancService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ancService\".\"providerId\" " +
                            "where (case when count_provider>1  then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\"> '" + start_date + "' or \"ProviderDB\".\"ExDate\" is NULL) group by \"providerId\",zillaid,upazilaid,unionid) as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and anc.zillaid=\"ProviderDB\".zillaid AND anc.upazilaid=\"ProviderDB\".upazilaid AND anc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\"  " +
                            "from \"pncServiceMother\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceMother\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceMother\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\"> '" + start_date + "' or \"ProviderDB\".\"ExDate\" is NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pnc.zillaid=\"ProviderDB\".zillaid AND pnc.upazilaid=\"ProviderDB\".upazilaid AND pnc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC-N\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\"  " +
                            "from \"pncServiceChild\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceChild\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceChild\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\"> '" + start_date + "' or \"ProviderDB\".\"ExDate\" is NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pncc.zillaid=\"ProviderDB\".zillaid AND pncc.upazilaid=\"ProviderDB\".upazilaid AND pncc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"Delivery\"  " +
                            "from \"delivery\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"delivery\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"delivery\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"outcomeDate\" between '" + start_date + "' AND \"ExDate\"::date else \"outcomeDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND \"delivery\".\"deliveryDoneByThisProvider\"=1 AND (\"ProviderDB\".\"ExDate\"> '" + start_date + "' or \"ProviderDB\".\"ExDate\" is NULL) group by \"providerId\",zillaid,upazilaid,unionid) as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" and delivery.zillaid=\"ProviderDB\".zillaid AND delivery.upazilaid=\"ProviderDB\".upazilaid AND delivery.unionid=\"ProviderDB\".unionid    " +
                            "join  \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\"   " +
                            "where (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6)  " +
                            "AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL)";
                } else if (methodType.equals("2")) {

                    sql = "select " +
                            "SUM(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", " +
                            "SUM(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", " +
                            "SUM(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  " +
                            "SUM(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  " +
                            "SUM(CASE WHEN implant.\"IMPLANT\" IS NULL THEN 0 ELSE implant.\"IMPLANT\" END) AS \"IMPLANT\",  " +
                            "SUM(CASE WHEN implantfollowup.\"IMPLANT Followup\" IS NULL THEN 0 ELSE implantfollowup.\"IMPLANT Followup\" END) AS \"IMPLANT Followup\",  " +
                            "SUM(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\",  " +
                            "SUM((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"IMPLANT\" is NULL then 0 else \"IMPLANT\" END) + (case when \"IMPLANT Followup\" is NULL then 0 else \"IMPLANT Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\" " +
                            "from \"ProviderDB\"   " +
                            "left join  " +
                            "(select \"providerId\",zillaid,upazilaid,unionid," +
                            "COUNT(CASE WHEN (\"methodType\"=1 OR \"methodType\"=10) THEN 1 ELSE NULL END) as \"Pill\", " +
                            "COUNT(CASE WHEN \"methodType\"=2 THEN 1 ELSE NULL END) as \"Condom\" " +
                            "from \"pillCondomService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pillCondomService\".\"providerId\"" +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pillCondomService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pillcondom on pillcondom.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pillcondom.zillaid=\"ProviderDB\".zillaid AND pillcondom.upazilaid=\"ProviderDB\".upazilaid AND pillcondom.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"IUD\",zillaid,upazilaid,unionid " +
                            "from \"iudService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"iudImplantDate\" between '" + start_date + "' AND \"ExDate\"::date else \"iudImplantDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"iudImplantDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as iud on iud.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iud.zillaid=\"ProviderDB\".zillaid AND iud.upazilaid=\"ProviderDB\".upazilaid AND iud.unionid=\"ProviderDB\".unionid " +
                            "left join " +
                            "(select \"providerId\",count (*) as \"IUD Followup\",zillaid,upazilaid,unionid " +
                            "from \"iudFollowupService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudFollowupService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudFollowupService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"followupDate\" between '" + start_date + "' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"followupDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as iudfollowup on iudfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iudfollowup.zillaid=\"ProviderDB\".zillaid AND iudfollowup.upazilaid=\"ProviderDB\".upazilaid AND iudfollowup.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"IMPLANT\",zillaid,upazilaid,unionid " +
                            "from \"implantService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"implantImplantDate\" between '" + start_date + "' AND \"ExDate\"::date else \"implantImplantDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"implantImplantDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implant on implant.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implant.zillaid=\"ProviderDB\".zillaid AND implant.upazilaid=\"ProviderDB\".upazilaid AND implant.unionid=\"ProviderDB\".unionid " +
                            "left join " +
                            "(select \"providerId\",count (*) as \"IMPLANT Followup\",zillaid,upazilaid,unionid " +
                            "from \"implantFollowupService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"followupDate\" between '" + start_date + "' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"followupDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implantfollowup on implantfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implantfollowup.zillaid=\"ProviderDB\".zillaid AND implantfollowup.upazilaid=\"ProviderDB\".upazilaid AND implantfollowup.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"Injectable\",zillaid,upazilaid,unionid " +
                            "from \"womanInjectable\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"womanInjectable\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"womanInjectable\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"doseDate\" between '" + start_date + "' AND \"ExDate\"::date else \"doseDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"doseDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as injectable on injectable.\"providerId\"=\"ProviderDB\".\"ProvCode\" and injectable.zillaid=\"ProviderDB\".zillaid AND injectable.upazilaid=\"ProviderDB\".upazilaid AND injectable.unionid=\"ProviderDB\".unionid " +
                            "where (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL)";

                } else {

                    sql = "select "
                            + "COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END) AS \"Male\", "
                            + "COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END) AS \"Female\", "
                            + "(COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END)+COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END)) AS \"TOTAL\" "
                            + "from \"ProviderDB\"   "
                            + "left join  "
                            + "\"gpService\" on \"gpService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" and \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' "
                            + "left join "
                            + "\"clientMap\" on \"clientMap\".\"generatedId\"=\"gpService\".\"healthId\" "
                            + "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) "
                            + "and \"ProvType\" in (4,101,17,5,6) "
                            + "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"gpService\".\"providerId\" "
                            + "where (case when count_provider>1 then "
                            + "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + " ' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + " ' end) "
                            + "else \"visitDate\" BETWEEN '" + start_date + " ' AND '" + end_date + " ' end) "
                            + " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) "
                            + "order by \"TOTAL\" desc";
                }

                System.out.println(sql);
                //graphInfo = GetResultSet.getFinalGraphResultZillaWise(sql);
            } else if (reportViewType.equals("2")) {
                if (methodType.equals("1")) {
                    sql = "select  " +
                            "SUM(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " +
                            "SUM(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " +
                            "SUM(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " +
                            "SUM(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " +
                            "SUM(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) as \"Delivery\",  " +
                            "SUM(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " +
                            "SUM(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\",  " +
                            "SUM((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " +
                            "from \"ProviderDB\"   " +
                            "left join  " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"ANC\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\"  " +
                            "from \"ancService\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"ancService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ancService\".\"providerId\" " +
                            "where (case when count_provider>1  then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and anc.zillaid=\"ProviderDB\".zillaid AND anc.upazilaid=\"ProviderDB\".upazilaid AND anc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\"  " +
                            "from \"pncServiceMother\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceMother\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceMother\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pnc.zillaid=\"ProviderDB\".zillaid AND pnc.upazilaid=\"ProviderDB\".upazilaid AND pnc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC-N\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\"  " +
                            "from \"pncServiceChild\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceChild\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceChild\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pncc.zillaid=\"ProviderDB\".zillaid AND pncc.upazilaid=\"ProviderDB\".upazilaid AND pncc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"Delivery\"  " +
                            "from \"delivery\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"delivery\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"delivery\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"outcomeDate\" between '" + start_date + "' AND \"ExDate\"::date else \"outcomeDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND \"delivery\".\"deliveryDoneByThisProvider\"=1 AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" and delivery.zillaid=\"ProviderDB\".zillaid AND delivery.upazilaid=\"ProviderDB\".upazilaid AND delivery.unionid=\"ProviderDB\".unionid    " +
                            "join  \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\"   " +
                            "where \"ProviderDB\".zillaid = " + zilla + " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6)  " +
                            "AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL)";
                } else if (methodType.equals("2")) {
                    sql = "select " +
                            "SUM(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", " +
                            "SUM(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", " +
                            "SUM(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  " +
                            "SUM(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  " +
                            "SUM(CASE WHEN implant.\"IMPLANT\" IS NULL THEN 0 ELSE implant.\"IMPLANT\" END) AS \"IMPLANT\",  " +
                            "SUM(CASE WHEN implantfollowup.\"IMPLANT Followup\" IS NULL THEN 0 ELSE implantfollowup.\"IMPLANT Followup\" END) AS \"IMPLANT Followup\",  " +
                            "SUM(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\",  " +
                            "SUM((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"IMPLANT\" is NULL then 0 else \"IMPLANT\" END) + (case when \"IMPLANT Followup\" is NULL then 0 else \"IMPLANT Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\" " +
                            "from \"ProviderDB\"   " +
                            "left join  " +
                            "(select \"providerId\",zillaid,upazilaid,unionid," +
                            "COUNT(CASE WHEN (\"methodType\"=1 OR \"methodType\"=10) THEN 1 ELSE NULL END) as \"Pill\", " +
                            "COUNT(CASE WHEN \"methodType\"=2 THEN 1 ELSE NULL END) as \"Condom\" " +
                            "from \"pillCondomService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pillCondomService\".\"providerId\"" +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pillCondomService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pillcondom on pillcondom.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pillcondom.zillaid=\"ProviderDB\".zillaid AND pillcondom.upazilaid=\"ProviderDB\".upazilaid AND pillcondom.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"IUD\",zillaid,upazilaid,unionid " +
                            "from \"iudService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"iudImplantDate\" between '" + start_date + "' AND \"ExDate\"::date else \"iudImplantDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"iudImplantDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as iud on iud.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iud.zillaid=\"ProviderDB\".zillaid AND iud.upazilaid=\"ProviderDB\".upazilaid AND iud.unionid=\"ProviderDB\".unionid " +
                            "left join " +
                            "(select \"providerId\",count (*) as \"IUD Followup\",zillaid,upazilaid,unionid " +
                            "from \"iudFollowupService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudFollowupService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudFollowupService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"followupDate\" between '" + start_date + "' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"followupDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as iudfollowup on iudfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iudfollowup.zillaid=\"ProviderDB\".zillaid AND iudfollowup.upazilaid=\"ProviderDB\".upazilaid AND iudfollowup.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"IMPLANT\",zillaid,upazilaid,unionid " +
                            "from \"implantService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"implantImplantDate\" between '" + start_date + "' AND \"ExDate\"::date else \"implantImplantDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"implantImplantDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implant on implant.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implant.zillaid=\"ProviderDB\".zillaid AND implant.upazilaid=\"ProviderDB\".upazilaid AND implant.unionid=\"ProviderDB\".unionid " +
                            "left join " +
                            "(select \"providerId\",count (*) as \"IMPLANT Followup\",zillaid,upazilaid,unionid " +
                            "from \"implantFollowupService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"followupDate\" between '" + start_date + "' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"followupDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implantfollowup on implantfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implantfollowup.zillaid=\"ProviderDB\".zillaid AND implantfollowup.upazilaid=\"ProviderDB\".upazilaid AND implantfollowup.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"Injectable\",zillaid,upazilaid,unionid " +
                            "from \"womanInjectable\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"womanInjectable\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"womanInjectable\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"doseDate\" between '" + start_date + "' AND \"ExDate\"::date else \"doseDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"doseDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as injectable on injectable.\"providerId\"=\"ProviderDB\".\"ProvCode\" and injectable.zillaid=\"ProviderDB\".zillaid AND injectable.upazilaid=\"ProviderDB\".upazilaid AND injectable.unionid=\"ProviderDB\".unionid " +
                            "where \"ProviderDB\".zillaid = " + zilla + " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL)";
                } else {
                    sql = "select "
                            + "COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END) AS \"Male\", "
                            + "COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END) AS \"Female\", "
                            + "(COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END)+COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END)) AS \"TOTAL\" "
                            + "from \"ProviderDB\"   "
                            + "left join  "
                            + "\"gpService\" on \"gpService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" and \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' "
                            + "left join "
                            + "\"clientMap\" on \"clientMap\".\"generatedId\"=\"gpService\".\"healthId\" "
                            + "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and "
                            + "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) "
                            + "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"gpService\".\"providerId\" "
                            + "where (case when count_provider>1 then "
                            + "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + " ' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + " ' end) "
                            + "else \"visitDate\" BETWEEN '" + start_date + " ' AND '" + end_date + " ' end) "
                            + "AND \"ProviderDB\".zillaid = " + zilla
                            + " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) "
                            + "order by \"TOTAL\" desc";
                }

                //graphInfo = GetResultSet.getFinalGraphResultUpazilaWise(sql, zilla);
            } else {
                if (methodType.equals("1")) {
                    sql = "select unions.\"UnionName\",  " +
                            "SUM(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " +
                            "SUM(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " +
                            "SUM(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " +
                            "SUM(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " +
                            "SUM(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) as \"Delivery\",  " +
                            "SUM(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " +
                            "SUM(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " +
                            "SUM(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " +
                            "SUM(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\",  " +
                            "SUM((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " +
                            "from \"ProviderDB\"   " +
                            "left join  " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"ANC\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\"  " +
                            "from \"ancService\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"ancService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ancService\".\"providerId\" " +
                            "where (case when count_provider>1  then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0')AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL)  group by \"providerId\",zillaid,upazilaid,unionid) as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and anc.zillaid=\"ProviderDB\".zillaid AND anc.upazilaid=\"ProviderDB\".upazilaid AND anc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\"  " +
                            "from \"pncServiceMother\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceMother\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceMother\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pnc.zillaid=\"ProviderDB\".zillaid AND pnc.upazilaid=\"ProviderDB\".upazilaid AND pnc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC-N\",  " +
                            "count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\",  " +
                            "count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\",  " +
                            "count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\",  " +
                            "count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\"  " +
                            "from \"pncServiceChild\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceChild\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceChild\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + "' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pncc.zillaid=\"ProviderDB\".zillaid AND pncc.upazilaid=\"ProviderDB\".upazilaid AND pncc.unionid=\"ProviderDB\".unionid    " +
                            "left join   " +
                            "(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"Delivery\"  " +
                            "from \"delivery\"  " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"delivery\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"delivery\".\"providerId\" " +
                            "where (case when count_provider>1 then  " +
                            "(case when \"ExDate\" is NOT NULL then \"outcomeDate\" between '" + start_date + "' AND \"ExDate\"::date else \"outcomeDate\" between \"EnDate\"::date and '" + end_date + "' end)  " +
                            "else \"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) " +
                            "AND \"delivery\".\"deliveryDoneByThisProvider\"=1 AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" and delivery.zillaid=\"ProviderDB\".zillaid AND delivery.upazilaid=\"ProviderDB\".upazilaid AND delivery.unionid=\"ProviderDB\".unionid    " +
                            "join  \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\"   " +
                            "join  (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"=" + zilla + " and \"UPAZILAID\"=" + upazila + ") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   " +
                            "where \"ProviderDB\".zillaid = " + zilla + " and \"ProviderDB\".upazilaid = " + upazila + " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6)  " +
                            "AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by unions.\"UnionName\" order by \"TOTAL\" desc";
                } else if (methodType.equals("2")) {
                    sql = "select unions.\"UnionName\", " +
                            "SUM(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", " +
                            "SUM(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", " +
                            "SUM(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  " +
                            "SUM(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  " +
                            "SUM(CASE WHEN implant.\"IMPLANT\" IS NULL THEN 0 ELSE implant.\"IMPLANT\" END) AS \"IMPLANT\",  " +
                            "SUM(CASE WHEN implantfollowup.\"IMPLANT Followup\" IS NULL THEN 0 ELSE implantfollowup.\"IMPLANT Followup\" END) AS \"IMPLANT Followup\",  " +
                            "SUM(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\",  " +
                            "SUM((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"IMPLANT\" is NULL then 0 else \"IMPLANT\" END) + (case when \"IMPLANT Followup\" is NULL then 0 else \"IMPLANT Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\" " +
                            "from \"ProviderDB\"   " +
                            "left join  " +
                            "(select \"providerId\",zillaid,upazilaid,unionid," +
                            "COUNT(CASE WHEN (\"methodType\"=1 OR \"methodType\"=10) THEN 1 ELSE NULL END) as \"Pill\", " +
                            "COUNT(CASE WHEN \"methodType\"=2 THEN 1 ELSE NULL END) as \"Condom\" " +
                            "from \"pillCondomService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pillCondomService\".\"providerId\"" +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pillCondomService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + " ' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + " ' end) " +
                            "else \"visitDate\" BETWEEN '" + start_date + " ' AND '" + end_date + " ' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + " ' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as pillcondom on pillcondom.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pillcondom.zillaid=\"ProviderDB\".zillaid AND pillcondom.upazilaid=\"ProviderDB\".upazilaid AND pillcondom.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"IUD\",zillaid,upazilaid,unionid " +
                            "from \"iudService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"iudImplantDate\" between '" + start_date + " ' AND \"ExDate\"::date else \"iudImplantDate\" between \"EnDate\"::date and '" + end_date + " ' end) " +
                            "else \"iudImplantDate\" BETWEEN '" + start_date + " ' AND '" + end_date + " ' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + " ' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as iud on iud.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iud.zillaid=\"ProviderDB\".zillaid AND iud.upazilaid=\"ProviderDB\".upazilaid AND iud.unionid=\"ProviderDB\".unionid " +
                            "left join " +
                            "(select \"providerId\",count (*) as \"IUD Followup\",zillaid,upazilaid,unionid " +
                            "from \"iudFollowupService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudFollowupService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudFollowupService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"followupDate\" between '" + start_date + " ' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '" + end_date + " ' end) " +
                            "else \"followupDate\" BETWEEN '" + start_date + " ' AND '" + end_date + " ' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + " ' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as iudfollowup on iudfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iudfollowup.zillaid=\"ProviderDB\".zillaid AND iudfollowup.upazilaid=\"ProviderDB\".upazilaid AND iudfollowup.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"IMPLANT\",zillaid,upazilaid,unionid " +
                            "from \"implantService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"implantImplantDate\" between '" + start_date + "' AND \"ExDate\"::date else \"implantImplantDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"implantImplantDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implant on implant.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implant.zillaid=\"ProviderDB\".zillaid AND implant.upazilaid=\"ProviderDB\".upazilaid AND implant.unionid=\"ProviderDB\".unionid " +
                            "left join " +
                            "(select \"providerId\",count (*) as \"IMPLANT Followup\",zillaid,upazilaid,unionid " +
                            "from \"implantFollowupService\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) " +
                            "and \"ProvType\" in (4,101,17,5,6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"followupDate\" between '" + start_date + "' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '" + end_date + "' end) " +
                            "else \"followupDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implantfollowup on implantfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implantfollowup.zillaid=\"ProviderDB\".zillaid AND implantfollowup.upazilaid=\"ProviderDB\".upazilaid AND implantfollowup.unionid=\"ProviderDB\".unionid " +
                            "left join   " +
                            "(select \"providerId\",count (*) as \"Injectable\",zillaid,upazilaid,unionid " +
                            "from \"womanInjectable\" " +
                            "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"womanInjectable\".\"providerId\" " +
                            "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and " +
                            "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) " +
                            "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"womanInjectable\".\"providerId\" " +
                            "where (case when count_provider>1 then " +
                            "(case when \"ExDate\" is NOT NULL then \"doseDate\" between '" + start_date + " ' AND \"ExDate\"::date else \"doseDate\" between \"EnDate\"::date and '" + end_date + " ' end) " +
                            "else \"doseDate\" BETWEEN '" + start_date + " ' AND '" + end_date + " ' end) AND (\"ProviderDB\".\"ExDate\">='" + start_date + " ' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as injectable on injectable.\"providerId\"=\"ProviderDB\".\"ProvCode\" and injectable.zillaid=\"ProviderDB\".zillaid AND injectable.upazilaid=\"ProviderDB\".upazilaid AND injectable.unionid=\"ProviderDB\".unionid " + "join   " +
                            "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"=" + zilla + " and \"UPAZILAID\"=" + upazila + ") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   " +
                            "where \"ProviderDB\".zillaid = " + zilla + " AND \"ProviderDB\".upazilaid = " + upazila + " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='" + start_date + " ' OR \"ProviderDB\".\"ExDate\" IS NULL) " +
                            "group by unions.\"UnionName\"";
                } else {
                    sql = "select unions.\"UnionName\", "
                            + "COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END) AS \"Male\", "
                            + "COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END) AS \"Female\", "
                            + "(COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END)+COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END)) AS \"TOTAL\" "
                            + "from \"ProviderDB\"   "
                            + "left join  "
                            + "\"gpService\" on \"gpService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" and \"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "' "
                            + "left join "
                            + "\"clientMap\" on \"clientMap\".\"generatedId\"=\"gpService\".\"healthId\" "
                            + "join "
                            + "\"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" "
                            + "join "
                            + "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"=" + zilla + " and \"UPAZILAID\"=" + upazila + ") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   "
                            + "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid=" + zilla + " and upazilaid = " + upazila + " and (\"ExDate\"> '" + start_date + "' or \"ExDate\" is NULL) and "
                            + "(\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) "
                            + "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"gpService\".\"providerId\" "
                            + "where (case when count_provider>1 then "
                            + "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '" + start_date + " ' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '" + end_date + " ' end) "
                            + "else \"visitDate\" BETWEEN '" + start_date + " ' AND '" + end_date + " ' end) "
                            + "AND \"ProviderDB\".zillaid = " + zilla + " AND \"ProviderDB\".upazilaid = " + upazila
                            + " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='" + start_date + "' OR \"ProviderDB\".\"ExDate\" IS NULL) "
                            + "GROUP BY unions.\"UnionName\"";
                }

                //graphInfo = GetResultSet.getFinalGraphResult(sql, zilla, upazila);
            }


            System.out.println(sql);


            //System.out.println(graphString);

        } catch (Exception e) {
            System.out.println(e);
        }

        return graphInfo;
    }

}
