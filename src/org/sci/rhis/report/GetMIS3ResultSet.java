package org.sci.rhis.report;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONObject;
import org.sci.rhis.db.*;

public class GetMIS3ResultSet {
	

	public static Map<String, String> getFinalResult(String sql, String type, String zilla, String upazila){
		
		Map<String, String> service = new HashMap<String, String>();
		Map<String, String> temp = new HashMap<String, String>();
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
		System.out.println("Db Object="+ dbObject.getDBName());
		
		try{
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
		    //service.clear();
		    String dbValue;
		    String tempValue;
		    int j = 0;
			while(rs.next()){
				j++;
				for (int i = 1; i <= columnCount; i++) {
					dbValue = rs.getString(metadata.getColumnName(i));
					
					
					if (j>1) {
						tempValue = temp.get(metadata.getColumnName(i));
						if(type.equals("notString"))
							dbValue = Integer.toString(Integer.parseInt(dbValue)+Integer.parseInt(tempValue));
						//System.out.println(dbValue + ", ");
					}
					
					
					//System.out.println(dbValue);
					service.put(metadata.getColumnName(i), dbValue);
					temp.put(metadata.getColumnName(i), dbValue);
			    	//System.out.println(rs.getString(metadata.getColumnName(i)) + ", ");      
			    }
					
			}
			//System.out.println((HashMap<String, String>) service);
			
			for (String key : service.keySet()) {
			    //System.out.println(key + " " + service.get(key));
			}
			
			//System.out.println(resultString);
			//System.out.println(resultList[2][1]);
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return service;
		
	}

	public static Map<String, String> getFinalResult(String sql, String type, String zilla, String upazila, String csba){

		Map<String, String> service = new HashMap<String, String>();
		Map<String, String> temp = new HashMap<String, String>();

		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = null;
		if(csba.equals("1") && zilla.equals("69"))
			dbObject = dbFacility.facilityDBInfo(Integer.parseInt(zilla));
		else
			dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

		try{

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();

			ResultSetMetaData metadata = rs.getMetaData();
			int columnCount = metadata.getColumnCount();

			//service.clear();
			String dbValue;
			String tempValue;
			int j = 0;
			while(rs.next()){
				j++;
				for (int i = 1; i <= columnCount; i++) {
					dbValue = rs.getString(metadata.getColumnName(i));


					if (j>1) {
						tempValue = temp.get(metadata.getColumnName(i));
						if(type.equals("notString"))
							dbValue = Integer.toString(Integer.parseInt(dbValue)+Integer.parseInt(tempValue));
						//System.out.println(dbValue + ", ");
					}


					//System.out.println(dbValue);
					service.put(metadata.getColumnName(i), dbValue);
					temp.put(metadata.getColumnName(i), dbValue);
					//System.out.println(rs.getString(metadata.getColumnName(i)) + ", ");
				}

			}
			//System.out.println((HashMap<String, String>) service);

			for (String key : service.keySet()) {
				//System.out.println(key + " " + service.get(key));
			}

			//System.out.println(resultString);
			//System.out.println(resultList[2][1]);
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return service;

	}
	
	public static LinkedHashMap<String, LinkedHashMap<String, String>> getLMISResult(String zilla, String upazila, String union, String startDate, String endDate)
	{
		String logistic;
		
		LinkedHashMap<String, LinkedHashMap<String, String>> logisticData = new LinkedHashMap<String, LinkedHashMap<String, String>>();
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
		
		JSONObject jsonObj = null;
		JSONObject tempJsonObj = null;
		Iterator jsonKey;
		
		try{
			String sql_provider = "select \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid="+upazila+" and unionid="+union+" and \"ProvType\" in (4, 101) and (\"ExDate\">='"+ startDate +"' OR \"ExDate\" IS NULL)";
			
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql_provider);
			ResultSet rs = dbObject.getResultSet();
			
			ResultSetMetaData metadata = rs.getMetaData();
			
			int j=0;
			int temp;
			
			while(rs.next()){
				j++;
				String jsonString ="{\"pId\":\""+rs.getString(metadata.getColumnName(1))+"\",\"startDate\":\""+startDate+"\",\"endDate\": \""+endDate+"\",\"requestType\":\"mis3\"}";
				//String jsonString ="{\"pId\":\"364001\",\"startDate\":\"2017-01-01\",\"endDate\": \"2017-01-30\",\"requestType\":\"mis3\"}";
				//System.out.println(jsonString);
				logistic = LiveSynchronization.synchronize("jsonString=" + jsonString, "MISGenerateServlet");
				
				jsonObj = new JSONObject(logistic);
				
//				if(j>1)
//				{
					int count_obj = Integer.parseInt(jsonObj.getString("count"));
					
					for(int k=1; k<=count_obj; k++)
					{
						tempJsonObj = jsonObj.getJSONObject("MIS-3").getJSONObject("MIS3Items").getJSONObject(Integer.toString(k));
						
						if(Integer.parseInt(tempJsonObj.getString("misColumnNo"))<30){
							if (!logisticData.containsKey(tempJsonObj.getString("misColumnNo")))
								logisticData.put(tempJsonObj.getString("misColumnNo"), (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
							
							jsonKey = tempJsonObj.keys();
							
							while(jsonKey.hasNext()) {
								String jsonParameter = (String) jsonKey.next();
								if(tempJsonObj.has(jsonParameter) && !jsonParameter.equals("misColumnNo") && !jsonParameter.equals("itemCode"))
								{
									if(j>1)
									{
										temp = Integer.parseInt(tempJsonObj.getString(jsonParameter)) + Integer.parseInt(logisticData.get(tempJsonObj.getString("misColumnNo")).get(jsonParameter));
										logisticData.get(tempJsonObj.getString("misColumnNo")).put(jsonParameter, Integer.toString(temp));
									}
									else
										logisticData.get(tempJsonObj.getString("misColumnNo")).put(jsonParameter, tempJsonObj.getString(jsonParameter));
								}
								else
									logisticData.get(tempJsonObj.getString("misColumnNo")).put(jsonParameter, tempJsonObj.getString(jsonParameter));
							}
						}
					}
					
//				}
//				else
//					jsonObj = new JSONObject(logistic);
				
				//System.out.println(jsonObj.getJSONObject("MIS-3").getJSONObject("MIS3Items").getJSONObject("11").getString("misColumnNo"));
					
			}
			
			//System.out.println(logisticData);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		
		
		
		return logisticData;
	}

}
