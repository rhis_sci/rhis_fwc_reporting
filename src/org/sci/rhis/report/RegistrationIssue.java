package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBSchemaInfo;

public class RegistrationIssue {

    public static String CheckProviderSymRegistration(JSONObject reportCred){

        String zilla = reportCred.getString("report1_zilla");
        String upazila = reportCred.getString("report1_upazila");
        String resultString = "";

        DBSchemaInfo table_schema = null;
        if(zilla.equals("36") && upazila.equals("71"))
            table_schema = new DBSchemaInfo();
        else
            table_schema = new DBSchemaInfo(zilla);

        try{
            String providertype = reportCred.getString("providertype");

            String providertype_condition = "";

            if(!providertype.equals("") && !providertype.equals("null") && (providertype.equals("2") || providertype.equals("3")))
                providertype_condition = "and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{providertype},"=")+" and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +"";
            else if(!providertype.equals("") && !providertype.equals("null"))
                providertype_condition = "and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{providertype},"=");
            else
                providertype_condition = "and "+ table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") +" ";
//                providertype_condition = "and ("+ table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") +" or ("+ table_schema.getColumn("table", "providerdb_provtype",new String[]{"2","3"},"in") +" and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +"))";

            String sql = "select "+table_schema.getColumn("","providerdb_provname")+" as \"Provider Name\", (case when "+ table_schema.getColumn("table", "providertype_provtype",new String[]{"2","3"},"in") +" and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +" then "+table_schema.getColumn("table","providertype_typename")+" || ' (CSBA)' else "+table_schema.getColumn("table","providertype_typename")+" end) as \"Provider Type\", "+table_schema.getColumn("","providerdb_mobileno")+" as \"Mobile No\", "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider Code\", "
                    + "(case when registration_enabled=0 then 'Registered' else (case when registration_enabled is null then 'Registration Not Open' else 'Yet Not Registered' end) end) as \"Registration Status\", "
                    + table_schema.getColumn("table","zilla_zillanameeng")+" as \"Zilla Name\", "+table_schema.getColumn("table","upazila_upazilanameeng")+" as \"Upazila Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Assign Area\", "+table_schema.getColumn("","providerdb_facilityname")+" as \"Facility Name\" "
                    + "from "+ table_schema.getTable("table_providerdb") +" join "+ table_schema.getTable("table_unions") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","unions_zillaid")+" and "+table_schema.getColumn("table","providerdb_upazilaid")+"="+table_schema.getColumn("table","unions_upazilaid")+" and "+table_schema.getColumn("table","providerdb_unionid")+"="+table_schema.getColumn("table","unions_unionid")+" "
                    + "left join " + table_schema.getTable("table_node_details") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "node_details_provcode") + " "
                    + "left join "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providertype_provtype")+"="+table_schema.getColumn("table","providerdb_provtype")+" "
                    + "left join "+ table_schema.getTable("table_upazila") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","upazila_zillaid")+" and "+table_schema.getColumn("table","providerdb_upazilaid")+"="+table_schema.getColumn("table","upazila_upazilaid")+" "
                    + "left join "+ table_schema.getTable("table_zilla") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","zilla_zillaid")+" "
                    + "left join sym_node_security on sym_node_security.node_id=" + table_schema.getColumn("table", "node_details_id") + " "
                    + "where "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+" and "+table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=")+" and "+table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=")+" "+providertype_condition+" "
                    + "group by "+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("","providerdb_provname")+", "+table_schema.getColumn("table","providertype_provtype")+", "+ table_schema.getColumn("table", "providerdb_csba") +", "+table_schema.getColumn("table","providertype_typename")+", "+table_schema.getColumn("","providerdb_mobileno")+", "+table_schema.getColumn("table","zilla_zillanameeng")+", "+table_schema.getColumn("table","upazila_upazilanameeng")+", "+table_schema.getColumn("table","unions_unionnameeng")+", "+table_schema.getColumn("","providerdb_facilityname")+", registration_enabled";



            System.out.println(sql);

            resultString = GetResultSet.getFinalResult(sql, zilla, upazila);

        }
        catch(Exception e){
            System.out.println(e);
        }

        return resultString;
    }
}
