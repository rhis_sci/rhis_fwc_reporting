package org.sci.rhis.report;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.model.GetResult;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetResultTimeSeries {
    public static String upazilaSql;

    public static String getFinalResultZillaWise_timeseries(String sql_view_type, String sql_method_type, String form_type, String start_date, String end_date) {
        String graphString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        DBSchemaInfo table_schema = new DBSchemaInfo("36");

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject graphInfoTemp = null;
        String geoSql = "";
        String resultset = new String();

        try {
            LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();
            LinkedHashMap<String, String> zilla_name = new LinkedHashMap<String, String>();
            int tempValue;
            ArrayList<String> zilla_array = new ArrayList<String>();
            ArrayList<String> div_array = new ArrayList<String>();
            LinkedHashMap<String, LinkedHashMap<Integer, String>> upazila_array = new LinkedHashMap<String, LinkedHashMap<Integer, String>>();
            String temp_zilla;

            DBListProperties setZillaList = new DBListProperties();
            Properties db_prop = setZillaList.setProperties();
            String[] zilla_list = db_prop.getProperty("zilla_all").split(",");

            geoSql = "select " + table_schema.getColumn("", "upazila_upazilanameeng") + ", " + table_schema.getColumn("table", "upazila_upazilaid") + ", " + table_schema.getColumn("table", "zilla_zillaid") + ", " + table_schema.getColumn("table", "zilla_zillanameeng") + ", " + table_schema.getColumn("table", "zilla_divid") + "  from " + table_schema.getTable("table_upazila") + " join " + table_schema.getTable("table_zilla") + " on " + table_schema.getColumn("table", "zilla_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " where " + table_schema.getColumn("table", "upazila_zillaid", zilla_list, "in") + " order by " + table_schema.getColumn("table", "upazila_zillaid") + " asc";

            System.out.println("GeoSQL query=" + geoSql);

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, geoSql);
            ResultSet rs = dbObject.getResultSet();

            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();

            ResultSetMetaData metadata = rs.getMetaData();

            long startTime = System.currentTimeMillis();

            int l = 0;
            boolean active_db;
//			GetSSSql getSSSql = new GetSSSql();
            while (rs.next()) {
                active_db = GetResultSet.check_active_db(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)));
                if (active_db) {
//					String sql = getSSSql.getSql(sql_view_type, sql_method_type, form_type, rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)), start_date, end_date);
//
//					threadResult.add(pool.submit(new GetThreadResult(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)), sql)));

                    if (!zilla_array.contains(rs.getString(metadata.getColumnName(3)))) {
                        l = 0;
                        zilla_array.add(rs.getString(metadata.getColumnName(3)));
                        div_array.add(rs.getString(metadata.getColumnName(3)) + "-" + rs.getString(metadata.getColumnName(5)) + "-" + rs.getString(metadata.getColumnName(4)));
                        zilla_name.put(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(4)));
                        upazila_array.put(rs.getString(metadata.getColumnName(3)), new LinkedHashMap<Integer, String>());
                    }

                    upazila_array.get(rs.getString(metadata.getColumnName(3))).put(l, rs.getString(metadata.getColumnName(2)));
                    l++;
                }

            }

            GetResult getResult = new GetResult(sql_view_type, sql_method_type, form_type, "0", "0", start_date, end_date);
            finalResult = getResult.getSSAggrResultMap_timeseries();
            getResult = null;

            System.out.println("Final Result=" + finalResult);

            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;
            System.out.println(elapsedTime);

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr>";
            //add year month
            table_header += "<th>Year</th><th>Month</th>";
            table_header += "<th>Zilla Name</th>";
            String table_body = "<tbody>";
            for (String key : finalResult.keySet()) {
                String[] key_val = key.split("_");

                temp = new JSONObject();

                table_body += "<tr><td>" + finalResult.get(key).get("Year") + "</td>";
                table_body += "<td>" + finalResult.get(key).get("month") + "</td>";
                table_body += "<td>" + zilla_name.get(key_val[0]) + "</td>";
                for (String key2 : finalResult.get(key).keySet()) {
                    if (!key2.equals("Year") && !key2.equals("month")) {
                        if (!graphMap.containsKey(key2)) {
                            graphMap.put(key2, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                            table_header += "<th>" + key2 + "</th>";
                        }
                        temp.put(key2, finalResult.get(key).get(key2));
                        graphMap.get(key2).put(zilla_name.get(key_val[0]), finalResult.get(key).get(key2));

                        table_body += "<td>" + finalResult.get(key).get(key2) + "</td>";
                    }
                }
                table_body += "</tr>";
                mapInfo.put(key_val[0], temp);
            }

            /***
             * @Author: evana
             * @Date: 03/07/2019
             * @Desc: Division seperation
             * *****/
            JSONObject divInfo = new JSONObject();
            String temp1 = "";

            for (int m = 0; m < div_array.size(); m++) {
                temp1 = div_array.get(m);
                divInfo.put(temp1, temp1);
                temp1 = "";
            }


            System.out.println("Mapinfo=" + mapInfo);
            System.out.println("Divinfo=" + divInfo);

            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";

            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    if (!key2.equals("Year") && !key2.equals("month")) {
                        temp.put(key2, graphMap.get(key).get(key2));
                        graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                    }
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }

            System.out.println(graphInfo);
            //resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString();
            resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString() + " table/graph/map " + divInfo.toString();

            System.out.println(resultset);
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultset;

    }

    public static String getFinalResultUpazilaWise_timeseries(String sql_view_type, String sql_method_type, String form_type, String zilla, String start_date, String end_date) {

        String graphString = "";

        String[] monthArray = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(Integer.parseInt(zilla));

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> getGraphResult = new LinkedHashMap<String, String>();

        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject graphInfoTemp = null;
        String resultset = new String();
        JSONObject dashboard = new JSONObject();

        try {

            GetResult getResult = new GetResult(sql_view_type, sql_method_type, form_type, zilla, "0", start_date, end_date);
            finalResult = getResult.getSSAggrResultMap_timeseries();
            getResult = null;

            upazilaSql = "select " + table_schema.getColumn("", "upazila_upazilanameeng") + ", " + table_schema.getColumn("", "upazila_upazilaid") + " from " + table_schema.getTable("table_upazila") + " where " + table_schema.getColumn("", "upazila_zillaid") + "=" + zilla;

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, upazilaSql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();

            String colVal = "";

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            //add year month
            String table_header = "<thead><tr>";
            table_header += "<th>Year</th><th>Month</th>";
            table_header += "<th>Upazila Name</th>";
            String table_body = "<tbody>";
            graphString = "";
            boolean active_db;

//            for (String finalKey: finalResult.keySet()) {
            List<String> yearList = getYearList(start_date, end_date);

            for (String year : yearList) {
                for (String month : monthArray) {
                    while (rs.next()) {

                        active_db = GetResultSet.check_active_db(zilla, rs.getString(metadata.getColumnName(2)));
                        if (active_db) {
                            colVal = rs.getString(metadata.getColumnName(1));
                            getGraphResult = finalResult.get(zilla + "_" + rs.getString(metadata.getColumnName(2)) + "_" + month + "_" + year);

                            //for newly implemented upazila where we dont get data(ex for 12_3 no data exists in finalresult)
                            if (!(getGraphResult == null)) {
                                temp = new JSONObject();
                                LinkedHashMap<String, String> dashboard_temp = new LinkedHashMap<String, String>();

                                table_body += "<tr><td>" + getGraphResult.get("Year") + "</td>";
                                table_body += "<td>" + getGraphResult.get("month") + "</td>";
                                table_body += "<td>" + rs.getString(metadata.getColumnName(1)) + "</td>";

                                for (String key : getGraphResult.keySet()) {

                                    if (!graphMap.containsKey(key) && !key.equals("Year") && !key.equals("month")) {
                                        table_header += "<th>" + key + "</th>";
                                        graphMap.put(key, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                                    }

                                    if (!key.equals("Year") && !key.equals("month")) {
                                        dashboard_temp.put('"' + key + '"', '"' + getGraphResult.get(key) + '"');

                                        temp.put(key, getGraphResult.get(key));
                                        graphMap.get(key).put(rs.getString(metadata.getColumnName(1)), getGraphResult.get(key));
                                        table_body += "<td>" + getGraphResult.get(key) + "</td>";
                                    }

                                }

                                table_body += "</tr>";
                                mapInfo.put(zilla + "_" + rs.getString(metadata.getColumnName(2)), temp);
                                dashboard.put(colVal, dashboard_temp);
                            }
                        }
                    }
                    rs.beforeFirst();
                }
            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";

            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    temp.put(key2, graphMap.get(key).get(key2));
                    graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }

            resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString() + " table/graph/map " + dashboard.toString();

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return resultset;

    }

    public static String getYear(String date) {
        String year = null;
        Matcher matcher = Pattern.compile("(\\d+).*").matcher(date);
        if (matcher.matches()) {
            year = matcher.group(1);

        }
        return year;

    }

    public static List<String> getYearList(String sYear, String eYear) {
        sYear = getYear(sYear);
        eYear = getYear(eYear);
        List<String> yearList = new ArrayList<>();

        if (sYear.equals(eYear)) {
            yearList.add(sYear);

        } else {
            int i = 1;
            Integer endYear = new Integer(eYear);
            Integer startYear = new Integer(sYear);
            yearList.add(sYear);
            int addyear = startYear + 1;
            while (addyear <= endYear) {

                yearList.add(String.valueOf(addyear));
                addyear += 1;

            }

        }
        return yearList;


    }

    public static String getResultUnionWise_timeseries(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date) {
        String graphString = "";
        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject temp_item = null;
        JSONObject graphInfoTemp = null;
        String resultset = new String();
        JSONObject dashboard = new JSONObject();


        try {
            String colVal = "";
            String colVal2 = "";
            String colVal3 = "";
            String tempVal = "";
            int j = 1;

            GetResult getResult = new GetResult(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date);
            finalResult = getResult.getSSAggrResultMap_timeseries();
            getResult = null;

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr>";
            //add year month
            table_header += "<th>Year</th><th>Month</th>";

            if (sql_view_type.equals("4"))
                table_header += "<th>Facility Name</th><th>Upazila Name</th>";
            table_header += "<th>Union Name</th>";
            String table_body = "<tbody>";
            graphString = "";
            for (String key : finalResult.keySet()) {
                String[] key_val = key.split("_");
                if (sql_view_type.equals("3"))
                    colVal = finalResult.get(key).get("unionnameeng");
                else if (sql_view_type.equals("4")) {
                    colVal = finalResult.get(key).get("facility_name");
                    colVal2 = finalResult.get(key).get("upazilanameeng");
                    colVal3 = finalResult.get(key).get("unionnameeng");
                }

                if (tempVal.equals(colVal)) {
                    colVal = sql_view_type.equals("3") ? finalResult.get(key).get("unionnameeng") : finalResult.get(key).get("facility_name") + "-" + j;
                    j++;
                } else
                    tempVal = sql_view_type.equals("3") ? finalResult.get(key).get("unionnameeng") : finalResult.get(key).get("facility_name");

                temp = new JSONObject();
                LinkedHashMap<String, String> dashboard_temp = new LinkedHashMap<String, String>();
                table_body += "<tr><td>" + finalResult.get(key).get("Year") + "</td>";
                table_body += "<td>" + finalResult.get(key).get("month") + "</td>";
                if (sql_view_type.equals("4")) {
//                    table_body += "<tr><td>" + finalResult.get(key).get("facility_name") + "</td>";
                    table_body += "<td>" + finalResult.get(key).get("facility_name") + "</td>";
                    table_body += "<td>" + finalResult.get(key).get("upazilanameeng") + "</td>";
                    table_body += "<td>" + finalResult.get(key).get("unionnameeng") + "</td>";
                } else
//                    table_body += "<tr><td>" + finalResult.get(key).get("unionnameeng") + "</td>";
                    table_body += "<td>" + finalResult.get(key).get("unionnameeng") + "</td>";
                for (String key2 : finalResult.get(key).keySet()) {
                    if (!key2.equals("unionnameeng") && !key2.equals("upazilanameeng") && !key2.equals("facility_name") && !key2.equals("Year") && !key2.equals("month")) {
                        temp_item = new JSONObject();
                        if (!graphMap.containsKey(key2)) {
                            graphMap.put(key2, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                            table_header += "<th>" + key2 + "</th>";
                        }

                        dashboard_temp.put('"' + key2 + '"', '"' + finalResult.get(key).get(key2) + '"');

                        temp.put(key2, finalResult.get(key).get(key2));

                        if (sql_view_type.equals("4"))
                            graphMap.get(key2).put(colVal + "_" + colVal2 + "_" + colVal3, finalResult.get(key).get(key2));
                        else
                            graphMap.get(key2).put(colVal, finalResult.get(key).get(key2));

                        table_body += "<td>" + finalResult.get(key).get(key2) + "</td>";

                    }
                }
                table_body += "</tr>";
                if (sql_view_type.equals("4"))
                    mapInfo.put(zilla + "_" + upazila + "_" + key_val[2] + "_" + colVal3, temp);
                else
                    mapInfo.put(zilla + "_" + upazila + "_" + key_val[2], temp);
                //System.out.println(dashboard_temp);
                dashboard.put(colVal, dashboard_temp);
            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";
            //System.out.println(dashboard);
            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    temp.put(key2, graphMap.get(key).get(key2));
                    graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }
            //System.out.println(dashboard);
            resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString() + " table/graph/map " + dashboard.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
        //return searchList;
        return resultset;

    }
}
