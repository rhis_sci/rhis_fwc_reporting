package org.sci.rhis.report;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;

import com.sun.org.apache.xpath.internal.objects.XBooleanStatic;
import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.servlet.DbdownloadRequest;
import org.sci.rhis.util.CalendarDate;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;


public class ProviderInfo {
    public static Integer zilla;
    public static Integer user_zilla;
    public static Integer zillaId;
    public static String zillaname;
    public static Integer upazila;
    public static Integer union;
    public static String unionname;
    public static Integer providerType;
    public static Integer providerCode;
    public static String providerPass;
    public static String providerName;
    public static String providerMob;
    public static String facilityId;
    public static String facilityName;
    public static String facilityType;
    public static String retiredType;
    public static String retireddate;
    public static String base_url;
    public static String sync_url;
    public static String sql_providerdb;
    public static String sql_facility_provider;
    public static String sql_nodeDetails;
    public static String sql_sym_node;
    public static String sql_providerdb_upz;
    public static String sql_nodeDetails_upz;
    public static String DB_name;
    public static String servername;
    public static String nodename;
    public static String changetype;
    public static String aav_providerType;
    public static String version;
    public static String sql_assignAppVersion;
    public static String sql_updateProvider;
    public static Integer formType;


    private static int READ_TIMEOUT = 15000;
    private static int CONNECTION_TIMEOUT = 15000;

    public static boolean saveProvider(JSONObject formValue) {


        //System.out.println("formValue="+ formValue);

        zilla = Integer.parseInt(formValue.getString("zillaid"));
        providerType = Integer.parseInt(formValue.getString("providertype"));


        //formType = Integer.parseInt(formValue.getString("type"));

        if (formValue.has("type")) {
            formType = Integer.parseInt(formValue.getString("type"));
        } else {
            formType = 0;
        }

        System.out.println("Zilla =" + zilla);

        if (formValue.has("version")) {
            version = formValue.getString("version");
        } else {
            version = "";
        }

//		if(formType == 1)
//		{
//			if(formValue.has("version"))
//			{
//				version = formValue.getString("version");
//			}
//			else
//			{
//				version = "";
//			}
//
//			//System.out.println("version="+ version);
//		}
//		else
//		{
//			version = "";
//		}


        if ((providerType == 2) || (providerType == 3) || (providerType == 4) || (providerType == 101) || (providerType == 5) || (providerType == 6) || (providerType == 17)) {
            zilla = Integer.parseInt(formValue.getString("zillaid"));
            //version = formValue.getString("version");
            //System.out.println("version="+ version);
        } else {
            user_zilla = Integer.parseInt(formValue.getString("zillaid"));
            zilla = 36;
        }

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        DBOperation dbOp = new DBOperation();

        boolean status = false;

        DBInfoHandler detailDomain = new DBInfoHandler();
        Properties prop = ConfInfoRetrieve.readingConf(detailDomain.getDomainLoc());
        String[] DomainDetails = prop.getProperty("DOMAININFO_" + zilla).split(",");

        String host = DomainDetails[0].trim();
//	    String user=DomainDetails[1];
//	    String password=DomainDetails[2];
//	    System.out.println("Domain Details="+DomainDetails[3]);
//	    Integer portNum=Integer.parseInt(DomainDetails[3]);

        System.out.println("shahed - provider type 1");

        try {
            zillaId = Integer.parseInt(formValue.getString("zillaid"));
            zillaname = formValue.getString("zillaname");
            providerType = Integer.parseInt(formValue.getString("providertype"));
            providerCode = Integer.parseInt(formValue.getString("providercode"));
            providerPass = formValue.getString("providerpass");
            providerName = formValue.getString("providername");
            providerMob = formValue.getString("providermobile");
            //union = Integer.parseInt(formValue.getString("unionid"));  /* @Author: Evana, @Date: 21/07/2019*/


            /**************** Provider type
             FWV = 4				SACMO FP = 5
             SACMO HS = 6		PARAMEDIC	101
             MIDWIFE	= 17		HA (CSBA) = 2
             FWA (CSBA) = 3

             */

            //System.out.println(providerType);

            String providerTransfer = "";
            if (formValue.has("providerTransfer"))
                providerTransfer = formValue.getString("providerTransfer");
            else
                providerTransfer = "0";
            String EnDate = "";
            if (formValue.has("EnDate") && providerTransfer.equals("1"))
                EnDate = "'" + formValue.getString("EnDate") + "'";
            else
                EnDate = "CURRENT_TIMESTAMP";

            Boolean providerIdExist = true;
            String query = "SELECT count(*) FROM " + table_schema.getTable("table_providerdb") + " WHERE " + table_schema.getColumn("", "providerdb_provcode") + "=" + providerCode;
            DBInfoHandler dbObject_conn = new FacilityDB().facilityDBInfo(zilla);
            ResultSet providerResult = dbOp.dbExecute(dbObject_conn, query).getResultSet();
            while (providerResult.next()) {
                if (Integer.parseInt(providerResult.getString(1)) == 0) {
                    providerIdExist = false;
                } else {
                    providerIdExist = true;
                }
            }
            System.out.println(providerIdExist);

            if (providerIdExist && !providerTransfer.equals("1")) {
                return false;
            } else {
                if (providerType != 2 && providerType != 3 && providerType != 4 && providerType != 5 && providerType != 6 && providerType != 17 && providerType != 101) {
                    String center_name = "";
                    String upazilaId = "";
                    if (providerType == 999 || providerType == 998 || providerType == 994 || providerType == 997)
                        center_name = "মা-মনি কার্যালয়";
                    else if (providerType == 0)
                        center_name = "অতিথি অ্যাকাউন্ট";
                    else
                        center_name = "পরিবার পরিকল্পনা অধিদপ্তর";

                    if (providerType == 14 || providerType == 15 || providerType == 19 || providerType == 16 || (providerType == 22 && (!formValue.getString("upazilaid").equals("") && formValue.getString("upazilaid") != null)))
                        upazilaId = formValue.getString("upazilaid");
                    else
                        upazilaId = "99";

                    sql_providerdb = "insert into " + table_schema.getTable("table_providerdb") + " (" + table_schema.getColumn("", "providerdb_divid") + ", " + table_schema.getColumn("", "providerdb_zillaid") + ", " + table_schema.getColumn("", "providerdb_upazilaid") + ", " + table_schema.getColumn("", "providerdb_unionid") + ", "
                            + "" + table_schema.getColumn("", "providerdb_provtype") + ", " + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "providerdb_mobileno") + ", " + table_schema.getColumn("", "providerdb_endate") + ", "
                            + "" + table_schema.getColumn("", "providerdb_active") + ", " + table_schema.getColumn("", "providerdb_devicesetting") + ", " + table_schema.getColumn("", "providerdb_healthidrequest") + ", " + table_schema.getColumn("", "providerdb_tablestructurerequest") + ", "
                            + "" + table_schema.getColumn("", "providerdb_areaupdate") + ", " + table_schema.getColumn("", "providerdb_provpass") + ", " + table_schema.getColumn("", "providerdb_facilityname") + ")"
                            + "VALUES("
                            + "(SELECT " + table_schema.getColumn("", "zilla_divid") + " FROM " + table_schema.getTable("table_zilla") + " WHERE " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(user_zilla)}, "=") + "),"
                            + user_zilla + "," + upazilaId + ",99," + providerType + "," + providerCode + ", '" + providerName + "', "
                            + providerMob + ", " + EnDate + ", '1', '2', "
                            + "'2', '2', '2', '" + providerPass + "', ";
                    if (providerType == 14 || providerType == 15 || providerType == 19 || providerType == 16 || (providerType == 22 && (!formValue.getString("upazilaid").equals("") && formValue.getString("upazilaid") != null)))
                        sql_providerdb += "(SELECT '" + center_name + ", ' || " + table_schema.getColumn("", "upazila_upazilaname") + " || ', ' || " + table_schema.getColumn("", "zilla_zillaname") + " from " + table_schema.getTable("table_zilla") + " join " + table_schema.getTable("table_upazila") + " on " + table_schema.getColumn("table", "zilla_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " where " + table_schema.getColumn("table", "zilla_zillaid", new String[]{Integer.toString(user_zilla)}, "=") + " and " + table_schema.getColumn("table", "upazila_upazilaid", new String[]{upazilaId}, "=") + "))";
                    else
                        sql_providerdb += "(SELECT '" + center_name + ", ' || " + table_schema.getColumn("", "zilla_zillaname") + " from " + table_schema.getTable("table_zilla") + " where " + table_schema.getColumn("table", "zilla_zillaid", new String[]{Integer.toString(user_zilla)}, "=") + "))";

                    sql_providerdb += "RETURNING " + table_schema.getColumn("", "providerdb_provcode") + "";

                    System.out.println(sql_providerdb);

                    DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();
                    dbOp.dbExecute(dbObject, sql_providerdb);
                    //dbOp.dbStatementExecute(dbObject,sql_facility_provider);

                    dbOp.closeStatement(dbObject);
                    dbOp.closeConn(dbObject);
                    dbObject = null;

                    status = true;


                } else {


                    upazila = Integer.parseInt(formValue.getString("upazilaid"));
                    union = Integer.parseInt(formValue.getString("unionid"));
                    unionname = formValue.getString("unionname");

                    //if(zilla.equals(36) && upazila.equals(71))
//				if(zilla==36)
//					base_url = "http://"+ host +":8080/RHIS_FWC/";
//				else if(zilla==69)
//					base_url = "http://"+ host +":8080/eMIS_69/";
//				else if(zilla==93)
//					base_url = "http://mchdrhis.icddrb.org:8080/eMIS/";
//				else
                    base_url = "http://" + host + ":8080/eMIS_" + String.format("%02d", zilla) + "/";
                    //base_url = "http://mamoni.net:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";

				/*else if(zilla.equals(93) && upazila.equals(9))
					base_url = "http://mchdrhis.icddrb.org:8080/CcahWebservice_facility/";
				else if(zilla.equals(93))
					base_url = "http://mchdrhis.icddrb.org:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";
				else
					base_url = "http://mamoni.net:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";*/

                    if (zilla == 36 && upazila == 71)
                        sync_url = "http://" + host + ":31415/sync/";
//				else if(zilla==93 && upazila==71)
//					sync_url = "http://mchdrhis.icddrb.org:31415/sync/";
                    else {
                        Integer zilla_port;
                        if (zilla < 10) {
                            zilla_port = zilla + 10;
//						if(zilla.equals(93))
//							sync_url = "http://mchdrhis.icddrb.org:"+zilla_port+String.format("%02d", upazila)+"4/sync/";
//						else
                            sync_url = "http://" + host + ":" + zilla_port + String.format("%02d", upazila) + "4/sync/";
                        } else {
                            if (zilla > 64) {
                                zilla_port = zilla - 55;
//							if(zilla.equals(93))
//								sync_url = "http://mchdrhis.icddrb.org:"+zilla_port+String.format("%02d", upazila)+"2/sync/";
//							else
                                sync_url = "http://" + host + ":" + zilla_port + String.format("%02d", upazila) + "2/sync/";
                            } else {
//							if(zilla.equals(93))
//								sync_url = "http://mchdrhis.icddrb.org:"+zilla+String.format("%02d", upazila)+"0/sync/";
//							else
                                sync_url = "http://" + host + ":" + zilla + String.format("%02d", upazila) + "0/sync/";
                            }
                        }
                    }
                    //facilityName = formValue.get("facilityname");

                    /**
                     * set dbdownloadstatus = 1 for all non beta version
                     */
                    String dbdownloadStatus = table_schema.getColumn("", "node_details_dbdownloadstatus");
                    int dbdownloadStatusValue = 1;
                    if(zilla == 48 || zilla ==9 || zilla ==65 || zilla ==81 || zilla ==26 || zilla ==6 || zilla ==47){
                        dbdownloadStatusValue = 0;
                    }
                    if (version.equals("")) {
                        System.out.println("version is empty=" + version);

                        sql_nodeDetails = "insert into " + table_schema.getTable("table_node_details") + " (" + table_schema.getColumn("", "node_details_provcode") + ", " + table_schema.getColumn("", "node_details_provtype") + ", " + table_schema.getColumn("", "node_details_client") + ", " + table_schema.getColumn("", "node_details_id") + "," + table_schema.getColumn("", "node_details_server") + ", " + table_schema.getColumn("", "node_details_base_url") + ", " + table_schema.getColumn("", "node_details_sync_url") + ", " + dbdownloadStatus + ")"
                                + "VALUES(" + providerCode + ", " + providerType + ", 2, "
                                + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + "), "
                                + "(SELECT replace(format('%s-" + String.format("%02d", zilla) + "-" + String.format("%02d", upazila) + "', lower(" + table_schema.getColumn("", "zilla_zillanameeng") + ")),' ','') from " + table_schema.getTable("table_zilla") + " where " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "), "
                                + "'" + base_url + "', '" + sync_url + "', '" + dbdownloadStatusValue + "')"
                                + "RETURNING " + table_schema.getColumn("", "node_details_provcode") + "";
                    } else {
                        sql_nodeDetails = "insert into " + table_schema.getTable("table_node_details") + " (" + table_schema.getColumn("", "node_details_provcode") + ", " + table_schema.getColumn("", "node_details_provtype") + ", " + table_schema.getColumn("", "node_details_client") + ", " + table_schema.getColumn("", "node_details_id") + "," + table_schema.getColumn("", "node_details_version") + ", " + table_schema.getColumn("", "node_details_server") + ", " + table_schema.getColumn("", "node_details_base_url") + ", " + table_schema.getColumn("", "node_details_sync_url") + ", " + dbdownloadStatus + ")"
                                + "VALUES(" + providerCode + ", " + providerType + ", 2, "
                                + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + "), "
                                + version + ","
                                + "(SELECT replace(format('%s-" + String.format("%02d", zilla) + "-" + String.format("%02d", upazila) + "', lower(" + table_schema.getColumn("", "zilla_zillanameeng") + ")),' ','') from " + table_schema.getTable("table_zilla") + " where " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "), "
                                + "'" + base_url + "', '" + sync_url + "', '" + dbdownloadStatusValue + "')"
                                + "RETURNING " + table_schema.getColumn("", "node_details_provcode") + "";

                    }
                    //System.out.println(sql_nodeDetails);
                    DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
                    ResultSet rs = dbOp.dbExecute(dbObject, sql_nodeDetails).getResultSet();

                    sql_providerdb = "insert into " + table_schema.getTable("table_providerdb") + " (" + table_schema.getColumn("", "providerdb_divid") + ", " + table_schema.getColumn("", "providerdb_zillaid") + ", " + table_schema.getColumn("", "providerdb_upazilaid") + ", " + table_schema.getColumn("", "providerdb_unionid") + ", "
                            + "" + table_schema.getColumn("", "providerdb_provtype") + ", " + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "providerdb_mobileno") + ", " + table_schema.getColumn("", "providerdb_endate") + ", "
                            + "" + table_schema.getColumn("", "providerdb_active") + ", " + table_schema.getColumn("", "providerdb_devicesetting") + ", " + table_schema.getColumn("", "providerdb_healthidrequest") + ", " + table_schema.getColumn("", "providerdb_tablestructurerequest") + ", "
                            + "" + table_schema.getColumn("", "providerdb_areaupdate") + ", " + table_schema.getColumn("", "providerdb_provpass") + ", " + table_schema.getColumn("", "providerdb_facilityname") + ")"
                            + "VALUES("
                            + "(SELECT " + table_schema.getColumn("", "zilla_divid") + " FROM " + table_schema.getTable("table_zilla") + " WHERE " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "),"
                            + zilla + "," + upazila + "," + union + "," + providerType + "," + providerCode + ", '" + providerName + "', "
                            + providerMob + ", " + EnDate + ", '1', '2', "
                            + "'2', '2', '2', '" + providerPass + "', '" + formValue.get("facilityname") + "') "
                            + "RETURNING " + table_schema.getColumn("", "providerdb_provcode") + "";

                    System.out.println(sql_providerdb);

                    sql_facility_provider = "insert into " + table_schema.getTable("table_facility_provider") + " (" + table_schema.getColumn("", "facility_provider_facility_id") + ", " + table_schema.getColumn("", "facility_provider_provider_id") + ", " + table_schema.getColumn("", "facility_provider_is_active") + ", " + table_schema.getColumn("", "facility_provider_start_date") + ") values(" + formValue.get("facilityid") + ", " + providerCode + ", 1, " + EnDate + ")";

                    sql_sym_node = "insert into sym_node (node_id,node_group_id,external_id,sync_enabled,sync_url,schema_version,"
                            + "symmetric_version,database_type,database_version,heartbeat_time,timezone_offset,batch_to_send_count,"
                            + "batch_in_error_count,created_at_node_id) "
                            + "values ("
                            + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + "), "
                            + "'FWV',"
                            + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + "), "
                            + "1,null,null,null,null,null,CURRENT_TIMESTAMP,null,0,0,"
                            + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + ")) ";

                    if (rs.next()) {
                        status = true;
                        dbOp.dbExecute(dbObject, sql_providerdb);
                        dbOp.dbStatementExecute(dbObject, sql_facility_provider);
                        //dbOp.dbExecute(dbObject,sql_sym_node);

//					DB_name = "RHIS_"+zilla+"_"+String.format("%02d", upazila);
//					Connection c = DriverManager.getConnection("jdbc:postgresql://10.10.10.17:5432/"+DB_name, "postgres", "postgres");
//					//c.setAutoCommit(false);
//					Statement statement = c.createStatement();

                        if (version.equals("")) {
                            sql_nodeDetails_upz = "insert into " + table_schema.getTable("table_node_details") + " (" + table_schema.getColumn("", "node_details_provcode") + ", " + table_schema.getColumn("", "node_details_provtype") + ", " + table_schema.getColumn("", "node_details_client") + ", " + table_schema.getColumn("", "node_details_id") + "," + table_schema.getColumn("", "node_details_server") + ", " + table_schema.getColumn("", "node_details_base_url") + ", " + table_schema.getColumn("", "node_details_sync_url") + ")"
                                    + "VALUES(" + providerCode + ", " + providerType + ", 2, "
                                    + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + "), "
                                    + "(SELECT format('%s-" + String.format("%02d", zilla) + "-" + String.format("%02d", upazila) + "', lower(" + table_schema.getColumn("", "zilla_zillanameeng") + ")) from " + table_schema.getTable("table_zilla") + " where " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "), "
                                    + "'" + base_url + "', '" + sync_url + "')";

                        } else {
                            sql_nodeDetails_upz = "insert into " + table_schema.getTable("table_node_details") + " (" + table_schema.getColumn("", "node_details_provcode") + ", " + table_schema.getColumn("", "node_details_provtype") + ", " + table_schema.getColumn("", "node_details_client") + ", " + table_schema.getColumn("", "node_details_id") + "," + table_schema.getColumn("", "node_details_version") + ", " + table_schema.getColumn("", "node_details_server") + ", " + table_schema.getColumn("", "node_details_base_url") + ", " + table_schema.getColumn("", "node_details_sync_url") + ")"
                                    + "VALUES(" + providerCode + ", " + providerType + ", 2, "
                                    + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + "), "
                                    + version + ","
                                    + "(SELECT format('%s-" + String.format("%02d", zilla) + "-" + String.format("%02d", upazila) + "', lower(" + table_schema.getColumn("", "zilla_zillanameeng") + ")) from " + table_schema.getTable("table_zilla") + " where " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "), "
                                    + "'" + base_url + "', '" + sync_url + "')";
                        }
                        sql_providerdb_upz = "insert into " + table_schema.getTable("table_providerdb") + " (" + table_schema.getColumn("", "providerdb_divid") + ", " + table_schema.getColumn("", "providerdb_zillaid") + ", " + table_schema.getColumn("", "providerdb_upazilaid") + ", " + table_schema.getColumn("", "providerdb_unionid") + ", "
                                + "" + table_schema.getColumn("", "providerdb_provtype") + ", " + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "providerdb_mobileno") + ", " + table_schema.getColumn("", "providerdb_endate") + ", "
                                + "" + table_schema.getColumn("", "providerdb_active") + ", " + table_schema.getColumn("", "providerdb_devicesetting") + ", " + table_schema.getColumn("", "providerdb_healthidrequest") + ", " + table_schema.getColumn("", "providerdb_tablestructurerequest") + ", "
                                + "" + table_schema.getColumn("", "providerdb_areaupdate") + ", " + table_schema.getColumn("", "providerdb_provpass") + ", " + table_schema.getColumn("", "providerdb_facilityname") + ")"
                                + "VALUES("
                                + "(SELECT " + table_schema.getColumn("", "zilla_divid") + " FROM " + table_schema.getTable("table_zilla") + " WHERE " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "),"
                                + zilla + "," + upazila + "," + union + "," + providerType + "," + providerCode + ", '" + providerName + "', "
                                + providerMob + ", " + EnDate + ", '1', '1', "
                                + "'2', '2', '2', '" + providerPass + "', '" + formValue.get("facilityname") + "') ";

                        DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
                        /**
                         * this line is commented as providerdb and node_details are remote table so in upz db this is not needed
                         */
//					dbOp.dbStatementExecute(dbObject_dynamic,sql_nodeDetails_upz);
//					dbOp.dbStatementExecute(dbObject_dynamic,sql_providerdb_upz);

                        dbOp.dbStatementExecute(dbObject_dynamic, sql_sym_node);


                        // Facility Assessment User Creation
                        // This section is developed by Shahed May 30 2019
                        // This will create facility user while creating provider form monitoring tool
//
//					String user_id = String.valueOf(providerCode);
//					String pass = "$2y$10$MQ3d8LB4.CMtk4B6IvDEEe/h3aQNL/Ge/V1IrnRu3ceKljk6BuQdm";
//					String name =providerName;
//					//String designation ="designation";
//					String phone = providerMob;
//					String user_type = "FWV-SACMO";
//					String zilla = String.valueOf(zillaId);
//					String upazilaId =String.valueOf(upazila);
//					String unionId =String.valueOf(union);
//					String create_date = null;
//					String update_date = null;
//					String remember_token = "cxWAf5c3O7fUREvpLVmPajq8ZvIjJXisdMnkOJ8Qs9hQcM8ytvdWoG02QBhj";
//					String facility_id = String.valueOf(formValue.get("facilityid"));
//					String token ="123456";
//
//					System.out.println("Shahed");
//					System.out.println("facility_id: "+facility_id + "Provider ID : " + user_id + "Provider Name: " + name + "phone: " + phone + "zilla : "+ zilla);
//					System.out.println("upazilaId: "+upazilaId + "unionId ID : " + unionId + "Provider Name: " + name + "phone: " + phone + "zilla : "+ zilla);
//
//
//					//Connecting to Facility DB
//					DBOperation facilityDBOp = new DBOperation();
//					DBInfoHandler facilityDBHandler = new FacilityDB().facilityDBInfo("ASSESSMENT"); // Get DB parameter form db.properties
//
//					String facilityInsertSql = "INSERT INTO users (user_id, password, name, phone, user_type, zilla_id, upazilla_id, union_id, remember_token, facility_id, token) values( ";
//					facilityInsertSql += user_id +",";
//					facilityInsertSql += "\'"+ pass +"\',";
//					facilityInsertSql += "\'"+name +"\',";
//					facilityInsertSql += phone +",";
//					facilityInsertSql += "\'"+user_type +"\',";
//					facilityInsertSql += zilla +",";
//					facilityInsertSql += upazilaId +",";
//					facilityInsertSql += unionId +",";
//					facilityInsertSql += "\'"+remember_token +"\',";
//					facilityInsertSql += facility_id +",";
//					facilityInsertSql += token +")";
//
//					System.out.println(facilityInsertSql);
//
//
//					facilityDBOp.dbExecute(facilityDBHandler,facilityInsertSql).getResultSet();
//
//					dbOp.closeStatement(facilityDBHandler);
//					dbOp.closeConn(facilityDBHandler);
//
//					facilityDBOp= null;
//
//					System.out.println("facility assessment db info" + facilityDBHandler);

                        // End of Facility Assessment User Creation

//					statement.execute(sql_nodeDetails);
//					statement.execute(sql_providerdb);
//					statement.execute(sql_sym_node);

//					statement.close();
//			        c.close();

                        dbOp.closeStatement(dbObject_dynamic);
                        dbOp.closeConn(dbObject_dynamic);

//					if(zilla==93 && upazila==9)
//						servername = "tangail-93-9";
//					else
                        servername = zillaname + "-" + String.format("%02d", zilla) + "-" + String.format("%02d", upazila);
                        nodename = unionname.replaceAll("\\s+", "") + "_" + providerCode + "_TAB";

                        JSONObject jsonStr = new JSONObject();
                        jsonStr.put("providerid", providerCode);
                        jsonStr.put("zillaid", String.format("%02d", zilla));
                        jsonStr.put("upazilaid", String.format("%02d", upazila));
                        jsonStr.put("symserver", servername);
                        jsonStr.put("symnode", nodename);

                        String URL = "";

//					if(zilla==69)
//						URL = "http://"+host+":8080/eMIS_69/";
//					else

                        URL = "http://" + host + ":8080/eMIS_" + String.format("%02d", zilla) + "/";

                        /**
                         * off this function as per req as we set dbdownloadstatus = 1 during add provider
                         */
//                        serverComm(URL + "updownstatus", "info=" + jsonStr);

				    /*String command1="bash /var/lib/symmetricds/symmetric-server-3.7.27/bin/symadmin --engine "+servername+" open-registration FWV "+nodename;

					ArrayList<String> result = new ArrayList<String>();

				    java.util.Properties config = new java.util.Properties();
			    	config.put("StrictHostKeyChecking", "no");
			    	JSch jsch = new JSch();
			    	Session session=jsch.getSession(user, host, portNum);
			    	session.setPassword(password);
			    	session.setConfig(config);
			    	session.connect();
			    	System.out.println("Connected");


			    	Channel channel=session.openChannel("exec");
			        ((ChannelExec)channel).setCommand(command1);
			        channel.setInputStream(null);
			        ((ChannelExec)channel).setErrStream(System.err);

			        InputStream in=channel.getInputStream();
			        channel.connect();

			     // Read the output from the input stream we set above
			        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			        String line;

			        //Read each line from the buffered reader and add it to result list
			        // You can also simple print the result here
			        while ((line = reader.readLine()) != null)
			        {
			        	result.add(line);
			        }
			        System.out.println(Arrays.asList(result));
			        //retrieve the exit status of the remote command corresponding to this channel
			        int exitStatus = channel.getExitStatus();

			        channel.disconnect();
			        session.disconnect();
			        System.out.println("DONE");*/
                    } else {
                        status = false;
                    }

                    dbOp.closeStatement(dbObject);
                    dbOp.closeConn(dbObject);
                    dbObject = null;

                }

                //System.out.println(status);

            }
        } catch (Exception e) {
            status = false;
            e.printStackTrace();
        } finally {

        }

        return status;
    }

    private static void serverComm(String uri, String jsonString) {

        try {
            URL url = new URL(uri);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer =
                    new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
            writer.write(jsonString);

            writer.flush();
            writer.close();
            os.close();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("No response!");
            } else {
                InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String bufferedStrChunk;
                while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                    stringBuilder.append(bufferedStrChunk);
                }
                String testString = stringBuilder.toString();
                System.out.println(testString);
            }
        } catch (MalformedURLException mul) {
            mul.getStackTrace();
        } catch (ProtocolException pe) {
            pe.getStackTrace();
        } catch (IOException io) {
            io.getStackTrace();

        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static boolean assignHealthID(JSONObject formValue) {

        boolean status = false;
        String sql_assignHealthID;

        try {


            zilla = Integer.parseInt(formValue.getString("zillaid"));
            upazila = Integer.parseInt(formValue.getString("upazilaid"));
            union = Integer.parseInt(formValue.getString("unionid"));
            providerType = Integer.parseInt(formValue.getString("providertype"));
            providerCode = Integer.parseInt(formValue.getString("providercode"));

            //DB_name = "RHIS_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila);

            Connection c = DriverManager.getConnection("jdbc:postgresql://10.10.10.17:5432/PRS2", "postgres", "postgres");
            //c.setAutoCommit(false);
            Statement statement = c.createStatement();


            sql_assignHealthID = "SELECT sp_generatehealthid('" + zilla + "','" + upazila + "','" + union + "','" + providerType + "','" + providerCode + "',5000)";
            System.out.println(sql_assignHealthID);
            statement.executeQuery(sql_assignHealthID);

            status = true;

            statement.close();
            c.close();

        } catch (Exception e) {
            System.out.println(e);
            status = false;
        }

        return status;
    }

    public static boolean assignAppVersion(JSONObject formValue) {

        boolean status = false;
        String providerType_condition = "";
        zilla = Integer.parseInt(formValue.getString("zillaid"));

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

        try {
            changetype = formValue.getString("changetype");
            if (!changetype.equals("1"))
                upazila = Integer.parseInt(formValue.getString("upazilaid"));
            if (changetype.equals("3"))
                union = Integer.parseInt(formValue.getString("unionid"));
            if (changetype.equals("4"))
                providerCode = Integer.parseInt(formValue.getString("providercode"));
            version = formValue.getString("version");

            aav_providerType = formValue.getString("aav_providerType");

            if (!aav_providerType.equals("") && !aav_providerType.equals("null"))
                providerType_condition = "and " + table_schema.getColumn("", "providerdb_provtype", new String[]{aav_providerType}, "=");
            else
                providerType_condition = "";


            if (changetype.equals("1"))
                sql_assignAppVersion = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_version", new String[]{version}, "=") + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("2"))
                sql_assignAppVersion = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_version", new String[]{version}, "=") + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("3"))
                sql_assignAppVersion = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_version", new String[]{version}, "=") + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("4"))
                sql_assignAppVersion = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_version", new String[]{version}, "=") + " where " + table_schema.getColumn("", "node_details_provcode") + "=" + providerCode;


            dbOp.dbExecuteUpdate(dbObject, sql_assignAppVersion);

            if (changetype.equals("1")) {
                String upazilaSql = "select " + table_schema.getColumn("", "upazila_upazilanameeng") + ", " + table_schema.getColumn("", "upazila_upazilaid") + " from " + table_schema.getTable("table_upazila") + " where " + table_schema.getColumn("", "upazila_zillaid", new String[]{Integer.toString(zilla)}, "=");

                FacilityDB dbFacilityUpz = new FacilityDB();
                DBInfoHandler dbObjectUpz = dbFacilityUpz.facilityDBInfo(zilla);
                dbObjectUpz = dbOp.dbCreateStatement(dbObjectUpz);
                dbObjectUpz = dbOp.dbExecute(dbObjectUpz, upazilaSql);
                ResultSet rs_upz = dbObjectUpz.getResultSet();
                ResultSetMetaData metadataUpz = rs_upz.getMetaData();

                while (rs_upz.next()) {
                    DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), rs_upz.getString(metadataUpz.getColumnName(2)));
                    dbOp.dbExecuteUpdate(dbObject_dynamic, sql_assignAppVersion);

                    dbOp.closeStatement(dbObject_dynamic);
                    dbOp.closeConn(dbObject_dynamic);
                }

                dbOp.closeResultSet(dbObjectUpz);
                dbOp.closePreparedStatement(dbObjectUpz);
                dbOp.closeConn(dbObjectUpz);
                dbObjectUpz = null;
            } else {
                DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_assignAppVersion);

                dbOp.closeStatement(dbObject_dynamic);
                dbOp.closeConn(dbObject_dynamic);
            }

            status = true;

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return status;
    }

    public static boolean updateProviderRetiredDate(JSONObject formValue) {

        boolean status = false;

        zilla = Integer.parseInt(formValue.getString("zillaid"));

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
        DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));

        try {
            retiredType = formValue.getString("retiredType");
            upazila = Integer.parseInt(formValue.getString("upazilaid"));
            retireddate = formValue.getString("retireddate");
            providerCode = Integer.parseInt(formValue.getString("providerID"));

            if (retiredType.equals("1") || retiredType.equals("2")) {
                String sql_updateFacilityProvider = "update " + table_schema.getTable("table_facility_provider") + " set " + table_schema.getColumn("", "facility_provider_end_date", new String[]{retireddate}, "=") + ", " + table_schema.getColumn("", "facility_provider_is_active", new String[]{Character.toString('2')}, "=") + " " +
                        "where (" + table_schema.getColumn("", "facility_provider_provider_id", new String[]{Integer.toString(providerCode)}, "=") + " " +
                        "or " + table_schema.getColumn("", "facility_provider_provider_id") + " in (select " + table_schema.getColumn("", "associated_provider_id_associated_id") + " from " + table_schema.getTable("table_associated_provider_id") + " where " + table_schema.getColumn("", "associated_provider_id_provider_id", new String[]{Integer.toString(providerCode)}, "=") + ")) " +
                        "and " + table_schema.getColumn("", "facility_provider_is_active", new String[]{"1"}, "=");

                dbOp.dbExecuteUpdate(dbObject, sql_updateFacilityProvider);
            } else if (retiredType.equals("3")) {
                String sql_insertLeaveInfo = "insert into " + table_schema.getTable("table_provider_leave_status") + "(" + table_schema.getColumn("", "provider_leave_status_providerid") + ", " + table_schema.getColumn("", "provider_leave_status_leave_type") + "," +
                        table_schema.getColumn("", "provider_leave_status_leave_start_date") + ", " + table_schema.getColumn("", "provider_leave_status_leave_comment") + ") " +
                        "values(" + providerCode + ", " + formValue.getString("leaveType") + ", '" + retireddate + "', '" + formValue.getString("leaveComment") + "')";

                dbOp.dbStatementExecute(dbObject, sql_insertLeaveInfo);

                String sql_change_db_status = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_dbdownloadstatus", new String[]{"1"}, "=") + "," + table_schema.getColumn("", "node_details_dbsyncrequest", new String[]{"1"}, "=") + " where " + table_schema.getColumn("", "node_details_provcode") + "=" + providerCode;
                dbOp.dbExecuteUpdate(dbObject, sql_change_db_status);
            } else if (retiredType.equals("4")) {
                String sql_updateLeaveInfo = "update " + table_schema.getTable("table_provider_leave_status") + " set " + table_schema.getColumn("", "provider_leave_status_leave_end_date", new String[]{retireddate}, "=") + " where " + table_schema.getColumn("", "provider_leave_status_providerid", new String[]{providerCode.toString()}, "=") + " and " + table_schema.getColumn("", "provider_leave_status_leave_end_date", new String[]{}, "isnull");
                dbOp.dbExecuteUpdate(dbObject, sql_updateLeaveInfo);

                String sql_nodename = "select " + table_schema.getColumn("", "node_details_id") + " as nodename from " + table_schema.getTable("table_node_details") + " where " + table_schema.getColumn("", "node_details_provcode", new String[]{providerCode.toString()}, "=");
                dbObject = dbOp.dbExecute(dbObject, sql_nodename);
                ResultSet rs = dbObject.getResultSet();

                ResultSetMetaData metadata = rs.getMetaData();

                while (rs.next()) {
                    nodename = rs.getString("nodename");
                }

                DBInfoHandler detailDomain = new DBInfoHandler();
                Properties prop = ConfInfoRetrieve.readingConf(detailDomain.getDomainLoc());
                String[] DomainDetails = prop.getProperty("DOMAININFO_" + zilla).split(",");

                String host = DomainDetails[0];

                zillaname = formValue.getString("zillaname");
                servername = zillaname + "-" + String.format("%02d", zilla) + "-" + String.format("%02d", upazila);

                JSONObject jsonStr = new JSONObject();
                jsonStr.put("providerid", providerCode);
                jsonStr.put("zillaid", zilla);
                jsonStr.put("upazilaid", upazila);
                jsonStr.put("symserver", servername);
                jsonStr.put("symnode", nodename);

                String URL = "http://" + host + ":8080/eMIS_" + zilla + "/";

                serverComm(URL + "updownstatus", "info=" + jsonStr);
            }

            if (retiredType.equals("1") || retiredType.equals("3")) {
                if (retiredType.equals("1")) {
                    sql_updateProvider = "update " + table_schema.getTable("table_providerdb") + " set " + table_schema.getColumn("", "providerdb_exdate", new String[]{retireddate}, "=") + ", " + table_schema.getColumn("", "providerdb_active", new String[]{Character.toString('2')}, "=") + " where " + table_schema.getColumn("", "providerdb_provcode", new String[]{Integer.toString(providerCode)}, "=") + " and " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "="); //+" and "+table_schema.getColumn("", "providerdb_unionid",new String[]{Integer.toString(union)},"=");

                    dbOp.dbExecuteUpdate(dbObject, sql_updateProvider);


                    dbOp.dbExecuteUpdate(dbObject_dynamic, sql_updateProvider);
                }


                String sql_sym_node_security = "delete from sym_node_security where node_id in(select id from node_details where providerid=" + providerCode + ")";
                String sql_sym_node = "delete from sym_node where node_id in(select id from node_details where providerid=" + providerCode + ")";
                String sql_sym_node_host = "delete from sym_node_host where node_id in(select id from node_details where providerid=" + providerCode + ")";
                String sql_sym_registration_request = "delete from sym_registration_request where external_id in(select id from node_details where providerid=" + providerCode + ")";
                String sql_sym_node_channel_ctl = "delete from sym_node_channel_ctl where node_id in(select id from node_details where providerid=" + providerCode + ")";
                String sql_sym_incoming_batch = "delete from sym_incoming_batch where node_id in(select id from node_details where providerid=" + providerCode + ")";
                String sql_sym_outgoing_batch = "delete from sym_outgoing_batch where node_id in(select id from node_details where providerid=" + providerCode + ")";

                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_sym_node_security);
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_sym_node);
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_sym_node_host);
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_sym_registration_request);
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_sym_node_channel_ctl);
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_sym_incoming_batch);
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_sym_outgoing_batch);
            }

            dbOp.closeStatement(dbObject_dynamic);
            dbOp.closeConn(dbObject_dynamic);
            dbObject_dynamic = null;

            status = true;

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return status;
    }

    public static String getProviderDetails(JSONObject reportCred) {
        String resultString = "";
        zilla = Integer.parseInt(reportCred.getString("report1_zilla"));

        DBSchemaInfo table_schema = new DBSchemaInfo();
        try {
            providerType = Integer.parseInt(reportCred.getString("providertype"));

            if (providerType == 2 || providerType == 3) {

                String sql = "select " + table_schema.getColumn("table", "providerdb_provname") + " as \"Provider Name\", " + table_schema.getColumn("table", "providerdb_provcode") + " as \"Provider Code\", " + table_schema.getColumn("", "providerdb_mobileno") + " as \"Mobile No\"," + table_schema.getColumn("table", "zilla_zillanameeng") + " as \"Zilla Name\", " + table_schema.getColumn("table", "upazila_upazilanameeng") + " as \"Upazila Name\", " + table_schema.getColumn("", "unions_unionnameeng") + " as \"Assign Area\", " + table_schema.getColumn("", "node_details_version") + " as \"Version\", to_char(date(" + table_schema.getColumn("table", "providerdb_endate") + "),'dd-MM-yyyy') as \"Create Date\" "
                        + "from " + table_schema.getTable("table_providerdb")
                        + " left join " + table_schema.getTable("table_node_details") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "node_details_provcode") + " and " + table_schema.getColumn("table", "providerdb_provtype") + "=" + table_schema.getColumn("table", "node_details_provtype") + " "
                        + " left join " + table_schema.getTable("table_unions") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "unions_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "unions_upazilaid") + " and " + table_schema.getColumn("table", "providerdb_unionid") + "=" + table_schema.getColumn("table", "unions_unionid") + " "
                        + "left join " + table_schema.getTable("table_upazila") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "upazila_upazilaid") + " "
                        + "left join " + table_schema.getTable("table_zilla") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "zilla_zillaid") + " "
                        + "where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("table", "providerdb_provtype", new String[]{Integer.toString(providerType)}, "=") + " and " + table_schema.getColumn("", "providerdb_active", new String[]{Integer.toString(1)}, "=") + " and " + table_schema.getColumn("", "providerdb_csba", new String[]{Integer.toString(1)}, "=");

//				String sql = "select " + table_schema.getColumn("", "providerdb_provname") + " as \"Provider Name\", " + table_schema.getColumn("", "providerdb_provcode") + " as \"Provider Code\", " + table_schema.getColumn("", "providerdb_mobileno") + " as \"Mobile No\", " + table_schema.getColumn("table", "zilla_zillanameeng") + " as \"Zilla Name\", " + table_schema.getColumn("table", "upazila_upazilanameeng") + " as \"Upazila Name\", " + table_schema.getColumn("", "unions_unionnameeng") + " as \"Assign Area\", " + table_schema.getColumn("", "providerdb_facilityname") + " as \"Facility Name\", date("+table_schema.getColumn("", "providerdb_endate")+") as \"Create Date\" "
//						+ "from " + table_schema.getTable("table_providerdb") + " left join " + table_schema.getTable("table_unions") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "unions_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "unions_upazilaid") + " and " + table_schema.getColumn("table", "providerdb_unionid") + "=" + table_schema.getColumn("table", "unions_unionid") + " "
//						+ "left join " + table_schema.getTable("table_upazila") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "upazila_upazilaid") + " "
//						+ "left join " + table_schema.getTable("table_zilla") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "zilla_zillaid") + " "
//						+ "where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_provtype", new String[]{Integer.toString(providerType)}, "=") + " and " + table_schema.getColumn("", "providerdb_active", new String[]{Integer.toString(1)}, "=")+ " and " + table_schema.getColumn("", "providerdb_csba", new String[]{Integer.toString(1)}, "=");

                System.out.println("Provider Details CSB=" + sql);
                resultString = GetResultSet.getProviderFinalResult(sql, zilla, providerType);
            } else{

//				String sql = "select " + table_schema.getColumn("", "providerdb_provname") + " as \"Provider Name\", " + table_schema.getColumn("", "providerdb_provcode") + " as \"Provider Code\", " + table_schema.getColumn("", "providerdb_mobileno") + " as \"Mobile No\", " + table_schema.getColumn("table", "zilla_zillanameeng") + " as \"Zilla Name\", " + table_schema.getColumn("table", "upazila_upazilanameeng") + " as \"Upazila Name\", " + table_schema.getColumn("", "unions_unionnameeng") + " as \"Assign Area\", " + table_schema.getColumn("", "providerdb_facilityname") + " as \"Facility Name\", date("+table_schema.getColumn("", "providerdb_endate")+") as \"Create Date\" "
//						+ "from " + table_schema.getTable("table_providerdb") + " left join " + table_schema.getTable("table_unions") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "unions_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "unions_upazilaid") + " and " + table_schema.getColumn("table", "providerdb_unionid") + "=" + table_schema.getColumn("table", "unions_unionid") + " "
//						+ "left join " + table_schema.getTable("table_upazila") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "upazila_upazilaid") + " "
//						+ "left join " + table_schema.getTable("table_zilla") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "zilla_zillaid") + " "
//						+ "where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_provtype", new String[]{Integer.toString(providerType)}, "=") + " and " + table_schema.getColumn("", "providerdb_active", new String[]{Integer.toString(1)}, "=");
//
                String sql = "select " + table_schema.getColumn("table", "providerdb_provname") + " as \"Provider Name\", " + table_schema.getColumn("table", "providerdb_provcode") + " as \"Provider Code\", " + table_schema.getColumn("", "providerdb_mobileno") + " as \"Mobile No\"," + table_schema.getColumn("table", "zilla_zillanameeng") + " as \"Zilla Name\", " + table_schema.getColumn("table", "upazila_upazilanameeng") + " as \"Upazila Name\", " + table_schema.getColumn("", "unions_unionnameeng") + " as \"Assign Area\", " + table_schema.getColumn("", "providerdb_facilityname") + " as \"Facility Name\"," + table_schema.getColumn("", "node_details_version") + " as \"Version\", to_char(date(" + table_schema.getColumn("table", "providerdb_endate") + "),'dd-MM-yyyy') as \"Create Date\", "
                        + "(case when " + table_schema.getColumn("table", "provider_leave_status_leave_type", new String[]{"1"}, "=") + " then 'Maternal Leave<br>('||" + table_schema.getColumn("table", "provider_leave_status_leave_comment") + "||')' "
                        + "when " + table_schema.getColumn("table", "provider_leave_status_leave_type", new String[]{"2"}, "=") + " then 'Training Leave' "
                        + "when " + table_schema.getColumn("table", "provider_leave_status_leave_type", new String[]{"3"}, "=") + " then 'Long Term Sick Leave' "
                        + "when " + table_schema.getColumn("table", "provider_leave_status_leave_type", new String[]{"4"}, "=") + " then 'Other' "
                        + "when " + table_schema.getColumn("table", "provider_leave_status_leave_type", new String[]{}, "isnull") + " then 'Active' else null end) as \"Status\" "
                        + "from " + table_schema.getTable("table_providerdb") + " "
                        + "left join " + table_schema.getTable("table_node_details") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "node_details_provcode") + " and " + table_schema.getColumn("table", "providerdb_provtype") + "=" + table_schema.getColumn("table", "node_details_provtype") + " "
                        + "left join " + table_schema.getTable("table_provider_leave_status") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "provider_leave_status_providerid") + " and " + table_schema.getColumn("table", "provider_leave_status_leave_end_date", new String[]{}, "isnull") + " "
                        + "left join " + table_schema.getTable("table_unions") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "unions_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "unions_upazilaid") + " and " + table_schema.getColumn("table", "providerdb_unionid") + "=" + table_schema.getColumn("table", "unions_unionid") + " "
                        + "left join " + table_schema.getTable("table_upazila") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "upazila_upazilaid") + " "
                        + "left join " + table_schema.getTable("table_zilla") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "zilla_zillaid") + " "
                        + "where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and "+ table_schema.getColumn("", "providerdb_active", new String[]{Integer.toString(1)}, "=");

                //for 'All' providertype
                if(!(providerType == 0)){
                    sql+=  " and " + table_schema.getColumn("table", "providerdb_provtype", new String[]{Integer.toString(providerType)}, "=") ;

                }


                System.out.println("Provider Details=" + sql);
                resultString = GetResultSet.getProviderFinalResult(sql, zilla, providerType);
            }


            //resultString = GetResultSet.getProviderResult(sql,zilla,providerType);

            /*
             * For updating Provider Information
             * @Author: Evana
             * Date:-  18/02/2019
             * */


        } catch (Exception e) {
            System.out.println(e);
        }

        return resultString;
    }

    /**
     * @Author: Evana
     * @Date: 30/06/2019
     * @Description: User Details Information
     ***/

    public static String getUserDetails(JSONObject reportCred) {
        String resultString = "";
        zilla = Integer.parseInt(reportCred.getString("report1_zilla"));

        DBSchemaInfo table_schema = new DBSchemaInfo();
        try {
            providerType = Integer.parseInt(reportCred.getString("providertype"));

            String sql = "select " + table_schema.getColumn("", "providerdb_provname") + " as \"User Name\", " + table_schema.getColumn("", "providerdb_provcode") + " as \"User Code\", " + table_schema.getColumn("", "providerdb_mobileno") + " as \"Mobile No\", " + table_schema.getColumn("table", "zilla_zillanameeng") + " as \"Zilla Name\", " + table_schema.getColumn("table", "upazila_upazilanameeng") + " as \"Upazila Name\"," + table_schema.getColumn("", "providerdb_facilityname") + " as \"Facility Name\", to_char(date(" + table_schema.getColumn("table", "providerdb_endate") + "),'dd-MM-yyyy') as \"Create Date\" "
                    + "from " + table_schema.getTable("table_providerdb") + " left join " + table_schema.getTable("table_unions") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "unions_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "unions_upazilaid") + " and " + table_schema.getColumn("table", "providerdb_unionid") + "=" + table_schema.getColumn("table", "unions_unionid") + " "
                    + "left join " + table_schema.getTable("table_upazila") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " and " + table_schema.getColumn("table", "providerdb_upazilaid") + "=" + table_schema.getColumn("table", "upazila_upazilaid") + " "
                    + "left join " + table_schema.getTable("table_zilla") + " on " + table_schema.getColumn("table", "providerdb_zillaid") + "=" + table_schema.getColumn("table", "zilla_zillaid") + " "
                    + "where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_provtype", new String[]{Integer.toString(providerType)}, "=") + " and " + table_schema.getColumn("", "providerdb_active", new String[]{Integer.toString(1)}, "=");


            System.out.println("Provider Details=" + sql);

            //resultString = GetResultSet.getProviderResult(sql,zilla,providerType);

            /*
             * For updating Provider Information
             * @Author: Evana
             * Date:-  18/02/2019
             * */
            resultString = GetResultSet.getProviderFinalResult(sql, zilla, providerType);

        } catch (Exception e) {
            System.out.println(e);
        }

        return resultString;
    }


    /*
     * Provider Information for edit
     * @ Author: Evana
     * @Date: 19/02/2019
     * */

    public static JSONObject getProviderEditDetails(Integer providercode, Integer zillacode, Integer providerType) throws NumberFormatException, SQLException {

        Integer zillaidnew = 0;

        if (providerType != 2 && providerType != 3 && providerType != 4 && providerType != 5 && providerType != 6 && providerType != 17 && providerType != 101) {
            zillaidnew = 36;
        } else {
            zillaidnew = zillacode;
        }
        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zillaidnew));
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject;
        if (zillaidnew == 36)
            dbObject = dbFacility.facilityDBInfo();//moved to rhis_facility_central db instead of rhis_36
        else
            dbObject = dbFacility.facilityDBInfo(Integer.toString(zillaidnew));


        String sql = "";

        sql = "select " + table_schema.getColumn("table", "providerdb_provcode") + ", "
                + table_schema.getColumn("", "providerdb_provname") + ","
                + table_schema.getColumn("", "providerdb_mobileno") + ","
                + table_schema.getColumn("", "providerdb_zillaid") + ","
                + table_schema.getColumn("", "providerdb_provtype") + ","
                + table_schema.getColumn("", "providerdb_provpass") + ","
                + table_schema.getColumn("", "providerdb_upazilaid") + "";
        if (providerType == 14 || providerType == 15 || providerType == 18 || providerType == 20 || providerType == 24 || providerType == 25) {
            sql += "," + table_schema.getColumn("table", "mis3_approver_active") + "" +
                    "," + table_schema.getColumn("table", "mis3_approver_approval_zilla") + "" +
                    "," + table_schema.getColumn("table", "mis3_approver_approval_upazilas") + "" +
                    "," + table_schema.getColumn("table", "mis3_approver_start_date") + "";
        }
        sql += " from " + table_schema.getTable("table_providerdb") + " ";
        if (providerType == 14 || providerType == 15 || providerType == 18 || providerType == 20 || providerType == 24 || providerType == 25)
            sql += "left join " + table_schema.getTable("table_mis3_approver") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "mis3_approver_providerid") + " and " + table_schema.getColumn("table", "mis3_approver_active", new String[]{"1"}, "=");
        sql += " where " + table_schema.getColumn("table", "providerdb_active", new String[]{"1"}, "=")
                + " and " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zillacode)}, "=") + " and " + table_schema.getColumn("table", "providerdb_provcode", new String[]{Integer.toString(providercode)}, "=");

        System.out.println(sql);

        JSONObject provider_info = new JSONObject();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            ResultSetMetaData metadata = rs.getMetaData();

            while (rs.next()) {
                Integer providerCodedb = Integer.parseInt(rs.getString(metadata.getColumnName(1)));
                String provname = rs.getString(metadata.getColumnName(2));
                String mobileno = rs.getString(metadata.getColumnName(3));
                Integer zillaid = Integer.parseInt(rs.getString(metadata.getColumnName(4)));
                providerType = Integer.parseInt(rs.getString(metadata.getColumnName(5)));
                String providerPass = rs.getString(metadata.getColumnName(6));
                Integer upazilaid = Integer.parseInt(rs.getString(metadata.getColumnName(7)));

                provider_info.put("providercode", providerCodedb);
                provider_info.put("zillaid", zillaid);
                provider_info.put("providerName", provname);
                provider_info.put("mobileno", mobileno);
                provider_info.put("providerType", providerType);
                provider_info.put("providerPass", providerPass);
                provider_info.put("upazilaid", upazilaid);
                if ((providerType == 14 || providerType == 15 || providerType == 18 || providerType == 20 || providerType == 24 || providerType == 25) && rs.getString(metadata.getColumnName(8)) != null) {
                    provider_info.put("mis3_approver_active", rs.getString(metadata.getColumnName(8)));
                    provider_info.put("approval_zilla", rs.getString(metadata.getColumnName(9)));
                    provider_info.put("approval_upazilas", rs.getString(metadata.getColumnName(10)));
                    provider_info.put("start_date", rs.getString(metadata.getColumnName(11)));
                } else
                    provider_info.put("mis3_approver_active", "");
            }

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return provider_info;

    }

    /*
     * @Author: Evana
     * @Date: 07/07/2019
     * @Title: get provider Code of a provider
     * */

    public static JSONObject getProviderCode(JSONObject formValue) {
        String provider_code = "";
        Integer pid = 0;

        zilla = Integer.parseInt(formValue.getString("zillaid"));

        String providerType = formValue.getString("providerType");

        JSONObject provider_info = new JSONObject();

        if (providerType.equals("6"))
            provider_info.put("code", "");
        else {

            DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

            FacilityDB dbFacility = new FacilityDB();
            DBOperation dbOp = new DBOperation();
            DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

            String matchedProvid = zilla + providerType + "%";

            /**
             * need to change this sql
             * 	select * from providerdb where providerid::text like '75TT%'
             * 	e.g. '754%' for FWV
             */
//			sql = "select MAX("+table_schema.getColumn("","providerdb_provcode")+") as provcode from "+table_schema.getTable("table_providerdb")+" where "+table_schema.getColumn("","providerdb_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","providerdb_provtype",new String[]{providerType},"=");
            String sql = "select MAX(" + table_schema.getColumn("", "providerdb_provcode") + ") as provcode from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and providerid::text like (\'" + matchedProvid + "\')";
            System.out.println("Provider code sql=" + sql);

            try {

                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject, sql);
                ResultSet rs = dbObject.getResultSet();
//                rs.beforeFirst();


                // removed previous code, rowcount == 0 condition as its never return 0 row..if we dont get any provcode it returns row with null value
                while (rs.next()) {
                    String idValue = rs.getString("provcode");
                    if (idValue == null) {
                        if (providerType.equals("101")) {
                            /**
                             * -changed the "10" to "90"
                             * as FPI also use 10.so for separating FPI and paramedic we used diff code "90"
                             * -still have some bug as if previous id with 101 exist then it will never enter here.
                             */
//                        provider_code = String.format("%02d", zilla.toString()) + "10" + "00";
                            provider_code = String.format("%02d", zilla) + "90" + "00 ";
                        } else {
                            provider_code = String.format("%02d", zilla) + providerType + "100";
                        }
                    } else {
                        pid = Integer.parseInt(idValue);
                        pid = pid + 1;

                        provider_code = pid.toString();

                    }
                }


                String providCheckSql = "select providerid from providerdb where providerid= " + provider_code;
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject, providCheckSql);
                rs = dbObject.getResultSet();
                rs.last();
                int rowCount = rs.getRow();


                /**
                 * - check whether suggested providerid exist or not
                 * - if exist, find a providerid until its not exist in providerdb
                 */

                if (rowCount != 0) {
                    do {
                        pid = Integer.parseInt(provider_code);
                        pid += 1;
                        provider_code = pid.toString();
                        providCheckSql = "select providerid from providerdb where providerid= " + provider_code;
                        dbObject = dbOp.dbCreateStatement(dbObject);
                        dbObject = dbOp.dbExecute(dbObject, providCheckSql);
                        rs = dbObject.getResultSet();
                        rs.last();
                        rowCount = rs.getRow();
                    } while (rowCount != 0);
                }
                provider_info.put("code", provider_code);

            } catch (Exception e) {
                System.out.println(e);
            } finally {
                dbOp.closeResultSet(dbObject);
                dbOp.closePreparedStatement(dbObject);
                dbOp.closeConn(dbObject);
                dbObject = null;
            }
        }

        System.out.println(provider_info);
        return provider_info;

    }


    public static JSONObject getProviderInfo(JSONObject formValue) throws NumberFormatException, SQLException {
        if (formValue.has("zillaid")) {
            if (!formValue.getString("zillaid").equals("") && !formValue.getString("zillaid").equals("null"))
                zilla = Integer.parseInt(formValue.getString("zillaid"));
            else
                zilla = 0;
        } else
            zilla = 0;

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        if (formValue.has("upazilaid")) {
            if (!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))
                upazila = Integer.parseInt(formValue.getString("upazilaid"));
            else
                upazila = 0;
        } else
            upazila = 0;

        if (formValue.has("unionid")) {
            if (!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))
                union = Integer.parseInt(formValue.getString("unionid"));
            else
                union = 0;
        } else
            union = 0;

        String providerType = formValue.getString("providerType");
        String providerType_condition = "";

        if (providerType.equals("All"))
            providerType_condition = " and " + table_schema.getColumn("", "providerdb_provtype", new String[]{"4", "5", "6", "17", "101"}, "in");
        else
            providerType_condition = " and " + table_schema.getColumn("", "providerdb_provtype", new String[]{providerType}, "=");

        String providerListType = "0";
        if (formValue.has("providerListType"))
            providerListType = formValue.getString("providerListType");
        String providerLeaveInfo = "";
        if (providerListType.equals("4"))
            providerLeaveInfo = "join " + table_schema.getTable("table_provider_leave_status") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "provider_leave_status_providerid") + " and " + table_schema.getColumn("table", "provider_leave_status_leave_end_date", new String[]{}, "isnull");

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        String sql = "";

        if (union != 0)
            sql = "select " + table_schema.getColumn("table", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + " from " + table_schema.getTable("table_providerdb") + " " + providerLeaveInfo + " where " + table_schema.getColumn("", "providerdb_active", new String[]{"1"}, "=") + " and " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + providerType_condition;
        else if (upazila != 0 && union == 0)
            sql = "select " + table_schema.getColumn("table", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + " from " + table_schema.getTable("table_providerdb") + " " + providerLeaveInfo + " where " + table_schema.getColumn("", "providerdb_active", new String[]{"1"}, "=") + " and " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + providerType_condition;
        else if (upazila == 0 && union == 0)
            sql = "select " + table_schema.getColumn("table", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + " from " + table_schema.getTable("table_providerdb") + " " + providerLeaveInfo + " where " + table_schema.getColumn("", "providerdb_active", new String[]{"1"}, "=") + " and " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + providerType_condition;
        else
            sql = "";

        System.out.println(sql);


        JSONObject provider_info = new JSONObject();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            ResultSetMetaData metadata = rs.getMetaData();

            while (rs.next()) {
                provider_info.put(rs.getString(metadata.getColumnName(1)), rs.getString(metadata.getColumnName(2)));
            }

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        System.out.println(provider_info);
        return provider_info;

    }

    public static JSONObject getProviderInfoASDP(JSONObject formValue) throws NumberFormatException, SQLException {
        zilla = Integer.parseInt(formValue.getString("zillaid"));
        upazila = Integer.parseInt(formValue.getString("upazilaid"));

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        String sql = "select " + table_schema.getColumn("", "associated_provider_id_associated_id") + ", " + table_schema.getColumn("table", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "facility_registry_facility_name") + " " +
                "from " + table_schema.getTable("table_providerdb") + " " +
                "join " + table_schema.getTable("table_associated_provider_id") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "associated_provider_id_provider_id") + " " +
                "join " + table_schema.getTable("table_facility_provider") + " on " + table_schema.getColumn("table", "facility_provider_provider_id") + "=" + table_schema.getColumn("table", "associated_provider_id_associated_id") + " " +
                "join " + table_schema.getTable("table_facility_registry") + " on " + table_schema.getColumn("table", "facility_registry_facilityid") + "=" + table_schema.getColumn("table", "facility_provider_facility_id") + " " +
                "where " + table_schema.getColumn("", "providerdb_active", new String[]{"1"}, "=") + " and " + table_schema.getColumn("", "facility_provider_is_active", new String[]{"1"}, "=") + " " +
                "and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=");

        JSONObject provider_info = new JSONObject();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            ResultSetMetaData metadata = rs.getMetaData();

            while (rs.next()) {
                provider_info.put(rs.getString(metadata.getColumnName(1)), rs.getString(metadata.getColumnName(2)) + " - " + rs.getString(metadata.getColumnName(3)) + ", " + rs.getString(metadata.getColumnName(4)));
            }

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        System.out.println(provider_info);
        return provider_info;

    }

    public static boolean providerTransfer(JSONObject formValue) throws ParseException {
        boolean check_status = false;
        Integer curr_zillaid = formValue.getInt("curr_zillaid");
        Integer curr_upazilaid = 99;
        if (formValue.has("curr_upazilaid") && !formValue.getString("curr_upazilaid").equals(""))
            curr_upazilaid = formValue.getInt("curr_upazilaid");
        else
            curr_upazilaid = 99;
        Integer curr_unionid = 99;
        if (formValue.has("curr_unionid") && !formValue.getString("curr_unionid").equals(""))
            curr_unionid = formValue.getInt("curr_unionid");
        else
            curr_unionid = 99;
        String providerID = formValue.getString("providerID");
        String transfer_date = formValue.getString("transfer_date");
        zilla = Integer.parseInt(formValue.getString("zillaid"));
        if (formValue.has("upazilaid") && !formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))
            upazila = Integer.parseInt(formValue.getString("upazilaid"));
        else
            upazila = 99;
        if (formValue.has("unionid") && !formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))
            union = Integer.parseInt(formValue.getString("unionid"));
        else
            union = 99;
        providerType = Integer.parseInt(formValue.getString("providertype"));
        String facilityid = formValue.getString("facilityid");
        String facilityname = "";
        if (!formValue.getString("facilityid").equals(""))
            facilityname = formValue.getString("facilityname");

        String EnDate = transfer_date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(EnDate));
        c.add(Calendar.DATE, 1);  // number of days to add
        EnDate = sdf.format(c.getTime());  // dt is now the new date

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(curr_zillaid));

        DBOperation dbOp = new DBOperation();

        DBInfoHandler dbObject_curr = new FacilityDB().facilityDBInfo(curr_zillaid);
        DBInfoHandler dbObject_curr_upz = new FacilityDB().dynamicDBInfo(Integer.toString(curr_zillaid), Integer.toString(curr_upazilaid));

        DBInfoHandler dbObject = null;
        DBInfoHandler dbObject_upz = null;
        if (zilla != 99) {
            dbObject = new FacilityDB().facilityDBInfo(zilla);
            dbObject_upz = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
        }

        String sql_facility_provider_update = "";

        try {
            if (zilla == 99) {
                sql_updateProvider = "update " + table_schema.getTable("table_providerdb") + " set " + table_schema.getColumn("", "providerdb_exdate", new String[]{transfer_date}, "=") + ", " + table_schema.getColumn("", "providerdb_active", new String[]{"2"}, "=") + " where " + table_schema.getColumn("", "providerdb_provcode", new String[]{providerID}, "=");

                if (providerType != 4 && providerType != 5 && providerType != 6 && providerType != 17 && providerType != 101)
                    dbOp.dbExecuteUpdate(dbObject_curr, sql_updateProvider);
                else {
                    dbOp.dbExecuteUpdate(dbObject_curr, sql_updateProvider);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_updateProvider);
                }
                check_status = true;
            } else {
                if (providerType != 4 && providerType != 5 && providerType != 6 && providerType != 17 && providerType != 101) {

                    Integer upazilaId;
                    if (providerType == 14 || providerType == 15)
                        upazilaId = upazila;
                    else
                        upazilaId = 99;

                    sql_updateProvider = "update " + table_schema.getTable("table_providerdb") + " set " + table_schema.getColumn("", "providerdb_exdate", new String[]{transfer_date}, "=") + ", " + table_schema.getColumn("", "providerdb_active", new String[]{"2"}, "=") + " where " + table_schema.getColumn("", "providerdb_provcode", new String[]{providerID}, "=") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(curr_zillaid)}, "=")
                            + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{Integer.toString(curr_upazilaid)}, "=") + " and " + table_schema.getColumn("table", "providerdb_unionid", new String[]{Integer.toString(curr_unionid)}, "=");
                    dbOp.dbExecuteUpdate(dbObject, sql_updateProvider);

                    sql_providerdb = "insert into " + table_schema.getTable("table_providerdb") + " (" + table_schema.getColumn("", "providerdb_divid") + ", " + table_schema.getColumn("", "providerdb_zillaid") + ", " + table_schema.getColumn("", "providerdb_upazilaid") + ", " + table_schema.getColumn("", "providerdb_unionid") + ", "
                            + table_schema.getColumn("", "providerdb_provtype") + ", " + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "providerdb_mobileno") + ", " + table_schema.getColumn("", "providerdb_endate") + ", "
                            + table_schema.getColumn("", "providerdb_active") + ", " + table_schema.getColumn("", "providerdb_devicesetting") + ", " + table_schema.getColumn("", "providerdb_systemupdatedt") + ", " + table_schema.getColumn("", "providerdb_healthidrequest") + ", " + table_schema.getColumn("", "providerdb_tablestructurerequest") + ", "
                            + table_schema.getColumn("", "providerdb_areaupdate") + ", " + table_schema.getColumn("", "providerdb_provpass") + ", " + table_schema.getColumn("", "providerdb_facilityname") + ") "
                            + "select"
                            + "(SELECT " + table_schema.getColumn("", "zilla_divid") + " FROM " + table_schema.getTable("table_zilla") + " WHERE " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "),"
                            + zilla + "," + upazilaId + ",99,"
                            + table_schema.getColumn("", "providerdb_provtype") + ", " + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "providerdb_mobileno") + ", '" + EnDate + "', "
                            + "1, " + table_schema.getColumn("", "providerdb_devicesetting") + ", " + table_schema.getColumn("", "providerdb_systemupdatedt") + ", " + table_schema.getColumn("", "providerdb_healthidrequest") + ", " + table_schema.getColumn("", "providerdb_tablestructurerequest") + ", "
                            + table_schema.getColumn("", "providerdb_areaupdate") + ", " + table_schema.getColumn("", "providerdb_provpass") + ", '" + facilityname + "'"
                            + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_provcode", new String[]{providerID}, "=") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(curr_zillaid)}, "=")
                            + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{Integer.toString(curr_upazilaid)}, "=") + " and " + table_schema.getColumn("table", "providerdb_unionid", new String[]{Integer.toString(curr_unionid)}, "=");

                    System.out.println(sql_providerdb);
                    dbOp.dbStatementExecute(dbObject, sql_providerdb);

                    check_status = true;
                } else {
                    sql_updateProvider = "update " + table_schema.getTable("table_providerdb") + " set " + table_schema.getColumn("", "providerdb_exdate", new String[]{transfer_date}, "=") + ", " + table_schema.getColumn("", "providerdb_active", new String[]{"2"}, "=") + " where " + table_schema.getColumn("", "providerdb_provcode", new String[]{providerID}, "=") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(curr_zillaid)}, "=")
                            + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{Integer.toString(curr_upazilaid)}, "=") + " and " + table_schema.getColumn("table", "providerdb_unionid", new String[]{Integer.toString(curr_unionid)}, "=");
                    dbOp.dbExecuteUpdate(dbObject_curr, sql_updateProvider);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_updateProvider);


                    if (curr_zillaid == zilla && curr_upazilaid == upazila) {

                        sql_providerdb = "insert into " + table_schema.getTable("table_providerdb") + " (" + table_schema.getColumn("", "providerdb_divid") + ", " + table_schema.getColumn("", "providerdb_zillaid") + ", " + table_schema.getColumn("", "providerdb_upazilaid") + ", " + table_schema.getColumn("", "providerdb_unionid") + ", "
                                + table_schema.getColumn("", "providerdb_provtype") + ", " + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "providerdb_mobileno") + ", " + table_schema.getColumn("", "providerdb_endate") + ", "
                                + table_schema.getColumn("", "providerdb_active") + ", " + table_schema.getColumn("", "providerdb_devicesetting") + ", " + table_schema.getColumn("", "providerdb_systemupdatedt") + ", " + table_schema.getColumn("", "providerdb_healthidrequest") + ", " + table_schema.getColumn("", "providerdb_tablestructurerequest") + ", "
                                + table_schema.getColumn("", "providerdb_areaupdate") + ", " + table_schema.getColumn("", "providerdb_provpass") + ", " + table_schema.getColumn("", "providerdb_facilityname") + ") "
                                + "select"
                                + "(SELECT " + table_schema.getColumn("", "zilla_divid") + " FROM " + table_schema.getTable("table_zilla") + " WHERE " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + "),"
                                + zilla + "," + upazila + "," + union + ","
                                + table_schema.getColumn("", "providerdb_provtype") + ", " + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provname") + ", " + table_schema.getColumn("", "providerdb_mobileno") + ", '" + EnDate + "', "
                                + "1, " + table_schema.getColumn("", "providerdb_devicesetting") + ", " + table_schema.getColumn("", "providerdb_systemupdatedt") + ", " + table_schema.getColumn("", "providerdb_healthidrequest") + ", " + table_schema.getColumn("", "providerdb_tablestructurerequest") + ", "
                                + table_schema.getColumn("", "providerdb_areaupdate") + ", " + table_schema.getColumn("", "providerdb_provpass") + ", '" + facilityname + "'"
                                + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_provcode", new String[]{providerID}, "=") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(curr_zillaid)}, "=")
                                + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{Integer.toString(curr_upazilaid)}, "=") + " and " + table_schema.getColumn("table", "providerdb_unionid", new String[]{Integer.toString(curr_unionid)}, "=");
                        System.out.println(sql_providerdb);
                        dbOp.dbStatementExecute(dbObject, sql_providerdb);
                        dbOp.dbStatementExecute(dbObject_upz, sql_providerdb);

                        sql_facility_provider_update = "update " + table_schema.getTable("table_facility_provider") + " set " + table_schema.getColumn("", "facility_provider_is_active", new String[]{"2"}, "=") + ", " + table_schema.getColumn("", "facility_provider_end_date", new String[]{transfer_date}, "=") + " where " + table_schema.getColumn("", "facility_provider_provider_id", new String[]{providerID}, "=") + " and " + table_schema.getColumn("", "facility_provider_is_active", new String[]{"1"}, "=") + "";
                        dbOp.dbStatementExecute(dbObject_curr, sql_facility_provider_update);

                        sql_facility_provider = "insert into " + table_schema.getTable("table_facility_provider") + " (" + table_schema.getColumn("", "facility_provider_facility_id") + ", " + table_schema.getColumn("", "facility_provider_provider_id") + ", " + table_schema.getColumn("", "facility_provider_is_active") + ", " + table_schema.getColumn("", "facility_provider_start_date") + ") values(" + facilityid + ", " + providerID + ", 1, '" + EnDate + "')";
                        dbOp.dbStatementExecute(dbObject, sql_facility_provider);

                        check_status = true;
                    } else {

                        String node_id = "";
                        JSONObject provider_info = new JSONObject();
                        String sql_providerInfo = "select " + table_schema.getColumn("", "providerdb_provpass") + " as prov_pass, " + table_schema.getColumn("", "providerdb_provname") + " as prov_name, " + table_schema.getColumn("", "providerdb_mobileno") + " as prov_mobile, " + table_schema.getColumn("", "node_details_id") + " as node_id, " + table_schema.getColumn("", "node_details_version") + " as version " +
                                "from " + table_schema.getTable("table_providerdb") + " " +
                                "inner join " + table_schema.getColumn("", "table_node_details") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "node_details_provcode") + " " +
                                " where " + table_schema.getColumn("table", "providerdb_provcode", new String[]{providerID}, "=") + " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{Integer.toString(curr_zillaid)}, "=")
                                + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{Integer.toString(curr_upazilaid)}, "=") + " and " + table_schema.getColumn("table", "providerdb_unionid", new String[]{Integer.toString(curr_unionid)}, "=");
                        //"where " + table_schema.getColumn("table", "providerdb_provcode", new String[]{providerID}, "=") + " and " + table_schema.getColumn("table", "providerdb_active", new String[]{"1"}, "=");

                        dbObject_curr = dbOp.dbCreateStatement(dbObject_curr);
                        dbObject_curr = dbOp.dbExecute(dbObject_curr, sql_providerInfo);
                        ResultSet rs = dbObject_curr.getResultSet();

                        while (rs.next()) {
                            provider_info.put("zillaid", zilla);
                            provider_info.put("zillaname", formValue.getString("zillaname"));
                            provider_info.put("upazilaid", upazila);
                            provider_info.put("unionid", union);
                            provider_info.put("unionname", formValue.getString("unionname"));
                            provider_info.put("providertype", providerType);
                            provider_info.put("providercode", providerID);
                            provider_info.put("providerpass", rs.getString("prov_pass"));
                            provider_info.put("providername", rs.getString("prov_name"));
                            provider_info.put("providermobile", rs.getString("prov_mobile"));
                            provider_info.put("facilityid", facilityid);
                            provider_info.put("facilityname", facilityname);
                            provider_info.put("version", rs.getString("version"));
                            provider_info.put("EnDate", EnDate);
                            node_id = rs.getString("node_id");
                            provider_info.put("providerTransfer", "1");
                        }

                        if (!rs.isClosed()) {
                            rs.close();
                        }

                        if (curr_zillaid == zilla) {
                            String delete_node_details = "delete from " + table_schema.getTable("table_node_details") + " where " + table_schema.getColumn("", "node_details_provcode", new String[]{providerID}, "=");
                            dbOp.dbExecuteUpdate(dbObject, delete_node_details);
                        }

                        sql_facility_provider_update = "update " + table_schema.getTable("table_facility_provider") + " set " + table_schema.getColumn("", "facility_provider_is_active", new String[]{"2"}, "=") + ", " + table_schema.getColumn("", "facility_provider_end_date", new String[]{transfer_date}, "=") + " where " + table_schema.getColumn("", "facility_provider_provider_id", new String[]{providerID}, "=") + " and " + table_schema.getColumn("", "facility_provider_is_active", new String[]{"1"}, "=") + "";
                        dbOp.dbStatementExecute(dbObject_curr, sql_facility_provider_update);

                        check_status = saveProvider(provider_info);

                        if (check_status == true) {
                            String sql_sym_node_security = "delete from sym_node_security where node_id='" + node_id + "'";

                            String sql_sym_node = "delete from sym_node where node_id='" + node_id + "'";
                            String sql_sym_node_host = "delete from sym_node_host where node_id='" + node_id + "'";
                            String sql_sym_registration_request = "delete from sym_registration_request where node_id='" + node_id + "'";
                            String sql_sym_node_channel_ctl = "delete from sym_node_channel_ctl where node_id='" + node_id + "'";
                            String sql_sym_incoming_batch = "delete from sym_incoming_batch where node_id='" + node_id + "'";
                            String sql_sym_outgoing_batch = "delete from sym_outgoing_batch where node_id='" + node_id + "'";

                            dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_security);
                            dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node);
                            dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_host);
                            dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_registration_request);
                            dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_channel_ctl);
                            dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_incoming_batch);
                            dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_outgoing_batch);

                            check_status = true;
                        } else {
                            check_status = false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            check_status = false;
            e.printStackTrace();
        } finally {
            if (zilla != 99) {
                dbOp.closeStatement(dbObject);
                dbOp.closeConn(dbObject);
                dbObject = null;

                dbOp.closeStatement(dbObject_upz);
                dbOp.closeConn(dbObject_upz);
                dbObject_upz = null;
            }

            dbOp.closeStatement(dbObject_curr);
            dbOp.closeConn(dbObject_curr);
            dbObject_curr = null;

            dbOp.closeStatement(dbObject_curr_upz);
            dbOp.closeConn(dbObject_curr_upz);
            dbObject_curr_upz = null;
        }
        return check_status;
    }

    public static boolean downloadUploadStatus(JSONObject formValue) {

        boolean status = false;

        String providerType_condition = "";

        zilla = Integer.parseInt(formValue.getString("zillaid"));

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

        try {
            changetype = formValue.getString("changetype");
            if (!changetype.equals("1"))
                upazila = Integer.parseInt(formValue.getString("upazilaid"));
            if (!changetype.equals("1") && !changetype.equals("2") && !changetype.equals("4"))
                union = Integer.parseInt(formValue.getString("unionid"));
            if (changetype.equals("4"))
                providerCode = Integer.parseInt(formValue.getString("providercode"));
            String auds_providerType = "";
            String upload_download_status = formValue.getString("upload_download_status");

            if (formValue.has("aav_providerType")) {
                auds_providerType = formValue.getString("aav_providerType");
            }

            if (!auds_providerType.equals("") && !auds_providerType.equals("null")) {
                providerType_condition = "and " + table_schema.getColumn("", "providerdb_provtype", new String[]{auds_providerType}, "=");
            } else {
                providerType_condition = "";
            }

            String update_set_sql = "";
            if (upload_download_status.equals("1")) {
                update_set_sql = table_schema.getColumn("", "node_details_dbdownloadstatus", new String[]{"1"}, "=") + "," + table_schema.getColumn("", "node_details_dbsyncrequest", new String[]{"1"}, "=");
            } else if (upload_download_status.equals("2")) {
                update_set_sql = table_schema.getColumn("", "node_details_dbsyncrequest", new String[]{"1"}, "=");
            } else if (upload_download_status.equals("3")) {
                update_set_sql = table_schema.getColumn("", "node_details_dbsyncrequest", new String[]{"2"}, "=");
            }

            //System.out.println(zilla+"="+upazila+"="+union+"="+changetype+"="+upload_download_status+"="+providerCode);
            String sql_change_db_status = "";

            if (changetype.equals("1"))
                sql_change_db_status = "update " + table_schema.getTable("table_node_details") + " set " + update_set_sql + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("2"))
                sql_change_db_status = "update " + table_schema.getTable("table_node_details") + " set " + update_set_sql + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("3"))
                sql_change_db_status = "update " + table_schema.getTable("table_node_details") + " set " + update_set_sql + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("4"))
                sql_change_db_status = "update " + table_schema.getTable("table_node_details") + " set " + update_set_sql + " where " + table_schema.getColumn("", "node_details_provcode") + "=" + providerCode;


            System.out.println(sql_change_db_status);
            dbOp.dbExecuteUpdate(dbObject, sql_change_db_status);

            DBListProperties setZillaList = new DBListProperties();
            Properties db_prop = setZillaList.setProperties();

            String[] upazila_list = db_prop.getProperty(zilla.toString()).split(",");

            if (changetype.equals("1") && upload_download_status.equals("1")) {
                for (int i = 0; i < upazila_list.length; i++) {
                    DBInfoHandler dbObject_curr_upz = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), upazila_list[i]);

                    String sql_sym_node_security = "delete from sym_node_security where node_id in(select id from node_details)";
                    String sql_sym_node = "delete from sym_node where node_id in(select id from node_details)";
                    String sql_sym_node_host = "delete from sym_node_host where node_id in(select id from node_details)";
                    String sql_sym_registration_request = "delete from sym_registration_request where external_id in(select id from node_details)";
                    String sql_sym_node_channel_ctl = "delete from sym_node_channel_ctl where node_id in(select id from node_details)";
                    String sql_sym_incoming_batch = "delete from sym_incoming_batch where node_id in(select id from node_details)";
                    String sql_sym_outgoing_batch = "delete from sym_outgoing_batch where node_id in(select id from node_details)";

                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_security);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_host);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_registration_request);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_channel_ctl);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_incoming_batch);
                    dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_outgoing_batch);

                    dbOp.closeStatement(dbObject_curr_upz);
                    dbOp.closeConn(dbObject_curr_upz);
                    dbObject_curr_upz = null;
                }
            } else if (changetype.equals("2") && upload_download_status.equals("1")) {
                DBInfoHandler dbObject_curr_upz = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));

                String sql_sym_node_security = "delete from sym_node_security where node_id in(select id from node_details)";
                String sql_sym_node = "delete from sym_node where node_id in(select id from node_details)";
                String sql_sym_node_host = "delete from sym_node_host where node_id in(select id from node_details)";
                String sql_sym_registration_request = "delete from sym_registration_request where external_id in(select id from node_details)";
                String sql_sym_node_channel_ctl = "delete from sym_node_channel_ctl where node_id in(select id from node_details)";
                String sql_sym_incoming_batch = "delete from sym_incoming_batch where node_id in(select id from node_details)";
                String sql_sym_outgoing_batch = "delete from sym_outgoing_batch where node_id in(select id from node_details)";

                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_security);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_host);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_registration_request);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_channel_ctl);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_incoming_batch);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_outgoing_batch);

                dbOp.closeStatement(dbObject_curr_upz);
                dbOp.closeConn(dbObject_curr_upz);
                dbObject_curr_upz = null;
            } else if (changetype.equals("3") && upload_download_status.equals("1")) {
                DBInfoHandler dbObject_curr_upz = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));

                String sql_sym_node_security = "delete from sym_node_security where node_id in(select id from node_details join " + table_schema.getTable("table_providerdb") + " using(" + table_schema.getColumn("", "providerdb_provcode") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + ")";
                String sql_sym_node = "delete from sym_node where node_id in(select id from node_details join " + table_schema.getTable("table_providerdb") + " using(" + table_schema.getColumn("", "providerdb_provcode") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + ")";
                String sql_sym_node_host = "delete from sym_node_host where node_id in(select id from node_details join " + table_schema.getTable("table_providerdb") + " using(" + table_schema.getColumn("", "providerdb_provcode") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + ")";
                String sql_sym_registration_request = "delete from sym_registration_request where external_id in(select id from node_details join " + table_schema.getTable("table_providerdb") + " using(" + table_schema.getColumn("", "providerdb_provcode") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + ")";
                String sql_sym_node_channel_ctl = "delete from sym_node_channel_ctl where node_id in(select id from node_details join " + table_schema.getTable("table_providerdb") + " using(" + table_schema.getColumn("", "providerdb_provcode") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + ")";
                String sql_sym_incoming_batch = "delete from sym_incoming_batch where node_id in(select id from node_details join " + table_schema.getTable("table_providerdb") + " using(" + table_schema.getColumn("", "providerdb_provcode") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + ")";
                String sql_sym_outgoing_batch = "delete from sym_outgoing_batch where node_id in(select id from node_details join " + table_schema.getTable("table_providerdb") + " using(" + table_schema.getColumn("", "providerdb_provcode") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + ")";

                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_security);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_host);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_registration_request);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_channel_ctl);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_incoming_batch);
                dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_outgoing_batch);

                dbOp.closeStatement(dbObject_curr_upz);
                dbOp.closeConn(dbObject_curr_upz);
                dbObject_curr_upz = null;
            } else if (changetype.equals("4") && upload_download_status.equals("1")) { //status 1 = dbdownload,changetype 4 = providerwise
                String sProviderCode = Integer.toString(providerCode);
                String sZilla = Integer.toString(zilla);
                String sUpz = Integer.toString(upazila);

                String provname_sql = "select provname from providerdb where providerid = " + providerCode;
                DBInfoHandler dbObject_curr_upz = new FacilityDB().dynamicDBInfo(sZilla, sUpz);
                ResultSet rs = dbOp.dbExecute(dbObject_curr_upz, provname_sql).getResultSet();

                String provName = null;
                while (rs.next()) {
                    provName = rs.getString("provname");
                }


                deleteSymTables_providerwise(dbOp, sZilla, sUpz, sProviderCode); //delete all sym table reference
                DbdownloadRequest.insertDataintoDBtable(sZilla, sUpz, sProviderCode, provName, 1);

            }

            status = true;

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return status;
    }

    public static void deleteSymTables_providerwise(DBOperation dbOp, String zilla, String upazila, String providerCode) {
        DBInfoHandler dbObject_curr_upz = new FacilityDB().dynamicDBInfo(zilla, upazila);
        String sql_sym_node_security = "delete from sym_node_security where node_id in(select id from node_details where providerid=" + providerCode + ")";
        String sql_sym_node = "delete from sym_node where node_id in(select id from node_details where providerid=" + providerCode + ")";
        String sql_sym_node_host = "delete from sym_node_host where node_id in(select id from node_details where providerid=" + providerCode + ")";
        String sql_sym_registration_request = "delete from sym_registration_request where external_id in(select id from node_details where providerid=" + providerCode + ")";
        String sql_sym_node_channel_ctl = "delete from sym_node_channel_ctl where node_id in(select id from node_details where providerid=" + providerCode + ")";
        String sql_sym_incoming_batch = "delete from sym_incoming_batch where node_id in(select id from node_details where providerid=" + providerCode + ")";
        String sql_sym_outgoing_batch = "delete from sym_outgoing_batch where node_id in(select id from node_details where providerid=" + providerCode + ")";

        dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_security);
        dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node);
        dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_host);
        dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_registration_request);
        dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_node_channel_ctl);
        dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_incoming_batch);
        dbOp.dbExecuteUpdate(dbObject_curr_upz, sql_sym_outgoing_batch);

        dbOp.closeStatement(dbObject_curr_upz);
        dbOp.closeConn(dbObject_curr_upz);
        dbObject_curr_upz = null;

    }

    public static boolean additionalSDP(JSONObject formValue) {
        System.out.println(formValue.toString());
        zilla = Integer.parseInt(formValue.getString("zillaid"));
        int facilityid = Integer.parseInt(formValue.getString("facilityid"));
        String transferDate = formValue.getString("transfer_date");
        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
        DBOperation dbOp = new DBOperation();
        boolean status = false;
        int newProviderId = 0;
        int oldProviderId = 0;

        try {
            zillaname = formValue.getString("zillaname");
            DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
            ResultSet rs = dbOp.dbExecute(dbObject, "Select count(" + table_schema.getColumn("", "associated_provider_id_provider_id") + ") from " + table_schema.getTable("table_associated_provider_id") + "").getResultSet();
            int totalProvider = 0;
            while (rs.next()) {
                totalProvider = Integer.parseInt(rs.getString(1));
            }
            newProviderId = zilla * 10000 + totalProvider + 1;
            oldProviderId = Integer.parseInt(formValue.getString("providerID"));

            String associdCheckSql = "select " + table_schema.getColumn("", "associated_provider_id_associated_id") +
                    " from " + table_schema.getTable("table_associated_provider_id") + "where " +
                    table_schema.getColumn("", "associated_provider_id_associated_id") + "=" + newProviderId;
            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, associdCheckSql);
            rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();


            /**
             * - check whether suggested associated_id exist or not
             * - if exist, find a associated_id until its not exist in associated_provider_id
             */

            if (rowCount != 0) {
                do {

                    newProviderId += 1;
                    associdCheckSql = "select " + table_schema.getColumn("", "associated_provider_id_associated_id") + " from " + table_schema.getTable("table_associated_provider_id") + "where " + table_schema.getColumn("", "associated_provider_id_associated_id") + "=" + newProviderId;
                    dbObject = dbOp.dbCreateStatement(dbObject);
                    dbObject = dbOp.dbExecute(dbObject, associdCheckSql);
                    rs = dbObject.getResultSet();
                    rs.last();
                    rowCount = rs.getRow();
                } while (rowCount != 0);
            }

            /**
             * check whether one provider assigns into same facility multiple time with active status or not.
             * if exist we will not reassign it, -return false without db operation
             */

            String isProviderAssigned = "select providerid from "+table_schema.getTable("table_facility_provider")
                                      +" f inner join "+table_schema.getTable("table_associated_provider_id")
                                      + "a on " +table_schema.getColumn("a", "associated_provider_id_associated_id") +" = " +table_schema.getColumn("f", "facility_provider_provider_id")
                                      + " where "+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")
                                      +" and f.facility_id = "+facilityid+" and a.provider_id = "+oldProviderId;
            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, isProviderAssigned);
            rs = dbObject.getResultSet();
            rs.last();
            rowCount = rs.getRow();
            if(rowCount!=0){
                    return false;
            }else {

                String sqlAss = "INSERT INTO " + table_schema.getTable("table_associated_provider_id") + "(" + table_schema.getColumn("", "associated_provider_id_provider_id") + ", " + table_schema.getColumn("", "associated_provider_id_associated_id") + ") VALUES (" + oldProviderId + "," + newProviderId + ");";
                dbOp.dbStatementExecute(dbObject, sqlAss);

                String sqlFaci = "INSERT INTO " + table_schema.getTable("table_facility_provider") + "(" + table_schema.getColumn("", "facility_provider_facility_id") + ", " + table_schema.getColumn("", "facility_provider_provider_id") + ", " + table_schema.getColumn("", "facility_provider_is_active") + "," + table_schema.getColumn("", "facility_provider_start_date") + ") " +
                        "VALUES (" + facilityid + "," + newProviderId + ",1,'" + transferDate + "');";
                System.out.println(sqlFaci);
                dbOp.dbStatementExecute(dbObject, sqlFaci);
                dbOp.closeStatement(dbObject);
                dbOp.closeConn(dbObject);
                status = true;

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return status;
    }

    public static boolean setManageProviderInfo(JSONObject formValue) {
        String sql_UpdateChangeRequest = "";
        boolean status = false;
        zilla = Integer.parseInt(formValue.getString("zillaid"));
        String providerType_condition = "";
        //Added three info for Manage provider
        JSONObject jsonObject = new JSONObject();
        String notice = formValue.getString("notice");
        String sql = formValue.getString("sql");
        String helpline = formValue.getString("helpline");
        if (!(notice.isEmpty()))
            jsonObject.put("notice", formValue.getString("notice"));
        if (!(sql.isEmpty()))
            jsonObject.put("sql", formValue.getString("sql"));
        if (!(helpline.isEmpty())) {
            jsonObject.put("helpline", formValue.getString("helpline"));
        }
        String changeReqColumnVal = jsonObject.toString().replace("(?m)[^$]", "'");


        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

        try {
            changetype = formValue.getString("changetype");
            if (!changetype.equals("1"))
                upazila = Integer.parseInt(formValue.getString("upazilaid"));
            if (changetype.equals("3"))
                union = Integer.parseInt(formValue.getString("unionid"));
            if (changetype.equals("4"))
                providerCode = Integer.parseInt(formValue.getString("providercode"));


            aav_providerType = formValue.getString("aav_providerType");

            if (!aav_providerType.equals("") && !aav_providerType.equals("null"))
                providerType_condition = "and " + table_schema.getColumn("", "providerdb_provtype", new String[]{aav_providerType}, "=");
            else
                providerType_condition = "";


            if (changetype.equals("1"))
                sql_UpdateChangeRequest = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_changerequest") + " = '" + changeReqColumnVal + "'" + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("2"))
                sql_UpdateChangeRequest = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_changerequest") + " = '" + changeReqColumnVal + "'" + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("3"))
                sql_UpdateChangeRequest = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_changerequest") + " = '" + changeReqColumnVal + "'" + " where " + table_schema.getColumn("", "node_details_provcode") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "providerdb_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "providerdb_unionid", new String[]{Integer.toString(union)}, "=") + " " + providerType_condition + ")";
            else if (changetype.equals("4"))
                sql_UpdateChangeRequest = "update " + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_changerequest") + " = '" + changeReqColumnVal + "'" + " where " + table_schema.getColumn("", "node_details_provcode") + "=" + providerCode;


            dbOp.dbExecuteUpdate(dbObject, sql_UpdateChangeRequest);

            if (changetype.equals("1")) {
                String upazilaSql = "select " + table_schema.getColumn("", "upazila_upazilanameeng") + ", " + table_schema.getColumn("", "upazila_upazilaid") + " from " + table_schema.getTable("table_upazila") + " where " + table_schema.getColumn("", "upazila_zillaid", new String[]{Integer.toString(zilla)}, "=");

                FacilityDB dbFacilityUpz = new FacilityDB();
                DBInfoHandler dbObjectUpz = dbFacilityUpz.facilityDBInfo(zilla);
                dbObjectUpz = dbOp.dbCreateStatement(dbObjectUpz);
                dbObjectUpz = dbOp.dbExecute(dbObjectUpz, upazilaSql);
                ResultSet rs_upz = dbObjectUpz.getResultSet();
                ResultSetMetaData metadataUpz = rs_upz.getMetaData();

                while (rs_upz.next()) {
                    DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), rs_upz.getString(metadataUpz.getColumnName(2)));
                    dbOp.dbExecuteUpdate(dbObject_dynamic, sql_UpdateChangeRequest);

                    dbOp.closeStatement(dbObject_dynamic);
                    dbOp.closeConn(dbObject_dynamic);
                }

                dbOp.closeResultSet(dbObjectUpz);
                dbOp.closePreparedStatement(dbObjectUpz);
                dbOp.closeConn(dbObjectUpz);
                dbObjectUpz = null;
            } else {
                DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
                dbOp.dbExecuteUpdate(dbObject_dynamic, sql_UpdateChangeRequest);

                dbOp.closeStatement(dbObject_dynamic);
                dbOp.closeConn(dbObject_dynamic);
            }

            status = true;

        } catch (Exception e) {
            e.printStackTrace();
            status = false;
        } finally {
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return status;
    }

    public static String getFWAList(JSONObject formValue){
        boolean status = false;
        zilla = Integer.parseInt(formValue.getString("report1_zilla"));
        upazila = Integer.parseInt(formValue.getString("report1_upazila"));
        String version_no = formValue.getString("versionNo");
        int communityActive = Integer.parseInt(formValue.getString("communityActive"));

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        String sql = "select z.zillanameeng,up.upazilanameeng,p.providerid,p.provname,p.unionid from "
                + table_schema.getTable("table_providerdb")+ " p left join "+table_schema.getTable("table_node_details")+ " n on p.providerid = n.providerid " +
                " inner join "+table_schema.getTable("table_zilla")+" z on z.zillaid = p.zillaid" +
                " inner join "+table_schema.getTable("table_upazila")+" up on up.zillaid = p.zillaid and up.upazilaid = p.upazilaid" +
                " inner join "+table_schema.getTable("table_unions")+" un on un.zillaid = p.zillaid and un.upazilaid = p.upazilaid and un.unionid = p.unionid " +
                " where n.providerid is null and p.provtype = 3  and p.active = 1 and p.zillaid = "+zilla+" and p.upazilaid = "+upazila;

        String resultSet= GetResultSet.getFWAResult(sql,zilla,upazila,version_no,communityActive,true);

        return resultSet;
    }

    public static boolean addCSV(JSONObject info){
        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);


        boolean status = true;
        providerCode = Integer.parseInt(info.getString("providercode"));
        zilla = Integer.parseInt(info.getString("zillaid"));
        upazila = Integer.parseInt(info.getString("upazilaid"));
        union =  Integer.parseInt(info.getString("unionid"));
        version = info.getString("version");
        int communityActive  = info.getInt("comunityactive");

        try {
            sql_providerdb = "update " + table_schema.getTable("table_providerdb") + " set " + table_schema.getColumn("", "providerdb_csba") + " = 1"
                    + " where " + table_schema.getColumn("", "providerdb_provcode") + " =" + providerCode
                    + " and " + table_schema.getColumn("", "providerdb_zillaid") + " = " + zilla
                    + " and " + table_schema.getColumn("", "providerdb_upazilaid") + " = " + upazila;

            sql_nodeDetails = "insert into " + table_schema.getTable("table_node_details") + " (" + table_schema.getColumn("", "node_details_provcode") + ", " + table_schema.getColumn("", "node_details_provtype") + ", " + table_schema.getColumn("", "node_details_client") + ", " + table_schema.getColumn("", "node_details_id") + ", " + table_schema.getColumn("", "node_details_version")+" ,"+ table_schema.getColumn("", "node_details_server")+")"

                    + "VALUES(" + providerCode + ", " + 3 + ", 2, "
                    + "(SELECT format('%s_" + providerCode + "_TAB', replace(" + table_schema.getColumn("", "unions_unionnameeng") + ", ' ', '')) from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{Integer.toString(zilla)}, "=") + " AND " + table_schema.getColumn("", "unions_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " AND " + table_schema.getColumn("", "unions_unionid", new String[]{Integer.toString(union)}, "=") + "), "
                    + version+","
                    + "(SELECT format('%s-" + String.format("%02d", zilla) + "-" + String.format("%02d", upazila) + "', lower(" + table_schema.getColumn("", "zilla_zillanameeng") + ")) from " + table_schema.getTable("table_zilla") + " where " + table_schema.getColumn("", "zilla_zillaid", new String[]{Integer.toString(zilla)}, "=") + ")) "
                    + "RETURNING " + table_schema.getColumn("", "node_details_provcode") + "";

            dbOp.dbExecuteUpdate(dbObject, sql_providerdb);
            dbOp.dbExecute(dbObject, sql_nodeDetails);
            if(communityActive==1) {
                String sql_update_nodedetails = "update" + table_schema.getTable("table_node_details") + " set " + table_schema.getColumn("", "node_details_community_active", new String[]{"1"}, "=")
                        + " where " + table_schema.getColumn("", "node_details_provcode", new String[]{providerCode.toString()}, "=");
                dbOp.dbExecute(dbObject, sql_update_nodedetails);
            }

        }catch (Exception e){
            e.printStackTrace();
            status = false;
        } finally {
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return status;
    }
}
