package org.sci.rhis.report;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.EnglishtoBangla;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

//import java.time.YearMonth;

public class MNCReport {
	
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String dateType;
    public static String yearSelect;
    public static String monthSelect;
	public static String startDate;
	public static String endDate;
    public static String resultSet;

	public static String getResultSet(JSONObject reportCred){
        String tableHtml = "";
        try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			union = reportCred.getString("report1_union");
			dateType = reportCred.getString("report1_dateType");
            String facility_id =  reportCred.getString("facilityid");
            startDate = LocalDate.now().withDayOfMonth(1).toString();
            endDate =LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).toString();

            try {
                if(dateType.equals("1")) {
                    monthSelect = reportCred.getString("report1_month");
                    String[] date = monthSelect.split("-");
                    int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                    startDate = monthSelect + "-01";
                    endDate = monthSelect + "-" + daysInMonth;
//                    startDate = reportCred.getString("report1_month")+"-01";
//                    LocalDate date= LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
//                    endDate = date.withDayOfMonth(date.getMonth().maxLength()).toString();
                }
                else if(dateType.equals("3"))
                {
                    yearSelect = reportCred.getString("report1_year");
                    startDate = yearSelect + "-01-01";
                    endDate = yearSelect + "-12-31";
                }else {
                    startDate = reportCred.getString("report1_start_date");
                    endDate = reportCred.getString("report1_end_date");
                    if(startDate.equals("")){
                        LocalDate date= LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
                        startDate = date.withDayOfMonth(1).toString();
                    }
                }
                DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
                FacilityDB dbFacility2nd = new FacilityDB();
                DBInfoHandler dbObject2nd = dbFacility2nd.facilityDBInfo(Integer.parseInt(zilla));
                DBOperation dbOp2nd = new DBOperation();
                dbObject2nd = dbOp2nd.dbCreateStatement(dbObject2nd);
                dbObject2nd = dbOp2nd.dbExecute(dbObject2nd, "SELECT "+table_schema.getColumn("","facility_provider_provider_id")+" from facility_provider where facility_id = "+facility_id);
                ResultSet provider_obj = dbObject2nd.getResultSet();
                String providerList = "";
                while (provider_obj.next()) {
                    String id = provider_obj.getString("providerid");
                    providerList += id +",";
                }
                providerList = providerList.substring(0, providerList.length() - 1);
                String sql = "SELECT " +
                        "CONCAT(cm."+table_schema.getColumn("","clientmap_name")+",'</br>(',cm."+table_schema.getColumn("","clientmap_healthid")+",')') as client_name, "+
                        "rs."+table_schema.getColumn("","regserial_serialno")+" as serial, "+
                        "to_char(rs."+table_schema.getColumn("","regserial_systementrydate")+",'DD MM YYYY') as serial_date, "+
                        "age(pw."+table_schema.getColumn("","pregwomen_systementrydate")+"::date,cm."+table_schema.getColumn("","clientmap_dob")+") as client_age, "+
                        "cm."+table_schema.getColumn("","clientmap_husbandname")+" as client_husband, "+
                        "cm."+table_schema.getColumn("","clientmap_hhno")+" as client_house_holding, "+
                        "cm."+table_schema.getColumn("","clientmap_mobileno")+" as client_mobile, "+
                        "vil."+table_schema.getColumn("","village_villagename")+" as client_village, "+
                        "to_char(pw."+table_schema.getColumn("","pregwomen_edd")+",'DD MM YYYY') as client_edd, "+
                        "to_char(pw."+table_schema.getColumn("","pregwomen_lmp")+",'DD MM YYYY') as client_lmp, "+
                        "pw."+table_schema.getColumn("","pregwomen_para")+" as client_para, "+
                        "pw."+table_schema.getColumn("","pregwomen_gravida")+" as client_gravida, "+
                        "CONCAT(pw."+table_schema.getColumn("","pregwomen_height")+"/12,' ফুট ',pw."+table_schema.getColumn("","pregwomen_height")+"%12,' ইঞ্চি') as client_height, "+
                        "CASE pw."+table_schema.getColumn("","pregwomen_bloodgroup")+" WHEN '1' THEN 'A+' WHEN '2' THEN 'A-' WHEN '3' THEN 'B+' WHEN '4' THEN 'B-' WHEN '5' THEN 'AB+' WHEN '6' THEN 'AB-'  WHEN '7' THEN 'O+' WHEN '8' THEN 'O-' ELSE '' END as client_blood_group, "+
                        "CASE WHEN elc."+table_schema.getColumn("","elco_son")+" IS NULL THEN 0 ELSE elc."+table_schema.getColumn("","elco_son")+"::integer END as client_son, "+
                        "CASE WHEN elc."+table_schema.getColumn("","elco_dau")+" IS NULL THEN 0 ELSE elc."+table_schema.getColumn("","elco_dau")+"::integer END as client_dau, "+
                        "elc."+table_schema.getColumn("","elco_dau")+"::integer + elc."+table_schema.getColumn("","elco_son")+"::integer as total_child, "+
                        "CASE WHEN pw."+table_schema.getColumn("","pregwomen_lastchildage")+"/12::integer != 0 or pw."+table_schema.getColumn("","pregwomen_lastchildage")+"%12::integer!=0 then CONCAT(pw."+table_schema.getColumn("","pregwomen_lastchildage")+"/12,' বছর ',pw."+table_schema.getColumn("","pregwomen_lastchildage")+"%12,' মাস') ELSE '' END  as client_last_child_age, "+
                        "pw."+table_schema.getColumn("","pregwomen_riskhistorynote")+" as client_risk_his_note, "+

                        "CASE tt1."+table_schema.getColumn("","immunizationhistory_imudose")+"::integer WHEN 1 then 'checked' ELSE '' END as is_tt1, "+
                        "CASE tt2."+table_schema.getColumn("","immunizationhistory_imudose")+"::integer WHEN 2 then 'checked' ELSE '' END as is_tt2, "+
                        "CASE tt3."+table_schema.getColumn("","immunizationhistory_imudose")+"::integer WHEN 3 then 'checked' ELSE '' END as is_tt3, "+
                        "CASE tt4."+table_schema.getColumn("","immunizationhistory_imudose")+"::integer WHEN 4 then 'checked' ELSE '' END as is_tt4, "+
                        "CASE tt5."+table_schema.getColumn("","immunizationhistory_imudose")+"::integer WHEN 5 then 'checked' ELSE '' END as is_tt5, "+

                        "to_char(anc1."+table_schema.getColumn("","ancservice_visitdate")+",'DD MM YYYY') as anc1_service_date, "+
                        "to_char(anc2."+table_schema.getColumn("","ancservice_visitdate")+",'DD MM YYYY') as anc2_service_date, "+
                        "to_char(anc3."+table_schema.getColumn("","ancservice_visitdate")+",'DD MM YYYY') as anc3_service_date, "+
                        "to_char(anc4."+table_schema.getColumn("","ancservice_visitdate")+",'DD MM YYYY') as anc4_service_date, "+

                        "CASE WHEN anc1."+table_schema.getColumn("","ancservice_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(anc1."+table_schema.getColumn("","ancservice_bpsystolic")+",'/',anc1."+table_schema.getColumn("","ancservice_bpdiastolic")+") END as anc1_bp, "+
                        "CASE WHEN anc2."+table_schema.getColumn("","ancservice_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(anc2."+table_schema.getColumn("","ancservice_bpsystolic")+",'/',anc2."+table_schema.getColumn("","ancservice_bpdiastolic")+") END  as anc2_bp, "+
                        "CASE WHEN anc3."+table_schema.getColumn("","ancservice_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(anc3."+table_schema.getColumn("","ancservice_bpsystolic")+",'/',anc3."+table_schema.getColumn("","ancservice_bpdiastolic")+") END  as anc3_bp, "+
                        "CASE WHEN anc4."+table_schema.getColumn("","ancservice_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(anc4."+table_schema.getColumn("","ancservice_bpsystolic")+",'/',anc4."+table_schema.getColumn("","ancservice_bpdiastolic")+") END  as anc4_bp, "+

                        "anc1."+table_schema.getColumn("","ancservice_weight")+"::integer as anc1_weight, "+
                        "anc2."+table_schema.getColumn("","ancservice_weight")+"::integer as anc2_weight, "+
                        "anc3."+table_schema.getColumn("","ancservice_weight")+"::integer as anc3_weight, "+
                        "anc4."+table_schema.getColumn("","ancservice_weight")+"::integer as anc4_weight, "+

                        "CASE anc1."+table_schema.getColumn("","ancservice_edema")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc1_edema, "+
                        "CASE anc2."+table_schema.getColumn("","ancservice_edema")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc2_edema, "+
                        "CASE anc3."+table_schema.getColumn("","ancservice_edema")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc3_edema, "+
                        "CASE anc4."+table_schema.getColumn("","ancservice_edema")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc4_edema, "+

                        "anc1."+table_schema.getColumn("","ancservice_uterusheight")+" as anc1_uterus_height, "+
                        "anc2."+table_schema.getColumn("","ancservice_uterusheight")+" as anc2_uterus_height, "+
                        "anc3."+table_schema.getColumn("","ancservice_uterusheight")+" as anc3_uterus_height, "+
                        "anc4."+table_schema.getColumn("","ancservice_uterusheight")+" as anc4_uterus_height, "+

                        "anc1."+table_schema.getColumn("","ancservice_fetusheartrate")+" as anc1_fetus_heart_rate, "+
                        "anc2."+table_schema.getColumn("","ancservice_fetusheartrate")+" as anc2_fetus_heart_rate, "+
                        "anc3."+table_schema.getColumn("","ancservice_fetusheartrate")+" as anc3_fetus_heart_rate, "+
                        "anc4."+table_schema.getColumn("","ancservice_fetusheartrate")+" as anc4_fetus_heart_rate, "+

                        "CASE anc1."+table_schema.getColumn("","ancservice_fetalpresentation")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়' else '' end  as anc1_fetal_presentation, "+
                        "CASE anc2."+table_schema.getColumn("","ancservice_fetalpresentation")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়' else '' end  as anc2_fetal_presentation, "+
                        "CASE anc3."+table_schema.getColumn("","ancservice_fetalpresentation")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়' else '' end  as anc3_fetal_presentation, "+
                        "CASE anc4."+table_schema.getColumn("","ancservice_fetalpresentation")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়' else '' end  as anc4_fetal_presentation, "+

                        "CASE anc1."+table_schema.getColumn("","ancservice_jaundice")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc1_jaundice, "+
                        "CASE anc2."+table_schema.getColumn("","ancservice_jaundice")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc2_jaundice, "+
                        "CASE anc3."+table_schema.getColumn("","ancservice_jaundice")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc3_jaundice, "+
                        "CASE anc4."+table_schema.getColumn("","ancservice_jaundice")+"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as anc4_jaundice, "+


                        "CASE anc1."+table_schema.getColumn("","ancservice_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as anc1_anemia, "+
                        "CASE anc2."+table_schema.getColumn("","ancservice_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as anc2_anemia, "+
                        "CASE anc3."+table_schema.getColumn("","ancservice_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as anc3_anemia, "+
                        "CASE anc4."+table_schema.getColumn("","ancservice_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as anc4_anemia, "+

                        "CASE anc1."+table_schema.getColumn("","ancservice_urinesugar")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc1_urine_sugar, "+
                        "CASE anc2."+table_schema.getColumn("","ancservice_urinesugar")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc2_urine_sugar, "+
                        "CASE anc3."+table_schema.getColumn("","ancservice_urinesugar")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc3_urine_sugar, "+
                        "CASE anc4."+table_schema.getColumn("","ancservice_urinesugar")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc4_urine_sugar, "+

                        "CASE anc1."+table_schema.getColumn("","ancservice_urinealbumin")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc1_urine_albumin, "+
                        "CASE anc2."+table_schema.getColumn("","ancservice_urinealbumin")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc2_urine_albumin, "+
                        "CASE anc3."+table_schema.getColumn("","ancservice_urinealbumin")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc3_urine_albumin, "+
                        "CASE anc4."+table_schema.getColumn("","ancservice_urinealbumin")+"::integer when 1 then 'নাই' when 2 then '+' when 3 then '++' when 4 then '+++' when 5 then '++++' else ' ' END  as anc4_urine_albumin, "+



                        "anc1."+table_schema.getColumn("","ancservice_hemoglobin")+" as anc1_hemoglobin, "+
                        "anc2."+table_schema.getColumn("","ancservice_hemoglobin")+" as anc2_hemoglobin, "+
                        "anc3."+table_schema.getColumn("","ancservice_hemoglobin")+" as anc3_hemoglobin, "+
                        "anc4."+table_schema.getColumn("","ancservice_hemoglobin")+" as anc4_hemoglobin, "+

                        "anc1."+table_schema.getColumn("","ancservice_complicationsign")+" as anc1_complication_sign, "+
                        "anc2."+table_schema.getColumn("","ancservice_complicationsign")+" as anc2_complication_sign, "+
                        "anc3."+table_schema.getColumn("","ancservice_complicationsign")+" as anc3_complication_sign, "+
                        "anc4."+table_schema.getColumn("","ancservice_complicationsign")+" as anc4_complication_sign, "+

                        "anc1."+table_schema.getColumn("","ancservice_symptom")+" as anc1_symptom, "+
                        "anc2."+table_schema.getColumn("","ancservice_symptom")+" as anc2_symptom, "+
                        "anc3."+table_schema.getColumn("","ancservice_symptom")+" as anc3_symptom, "+
                        "anc4."+table_schema.getColumn("","ancservice_symptom")+" as anc4_symptom, "+

                        "anc1."+table_schema.getColumn("","ancservice_disease")+" as anc1_disease, "+
                        "anc2."+table_schema.getColumn("","ancservice_disease")+" as anc2_disease, "+
                        "anc3."+table_schema.getColumn("","ancservice_disease")+" as anc3_disease, "+
                        "anc4."+table_schema.getColumn("","ancservice_disease")+" as anc4_disease, "+

                        "anc1."+table_schema.getColumn("","ancservice_treatment")+" as anc1_treatment, "+
                        "anc2."+table_schema.getColumn("","ancservice_treatment")+" as anc2_treatment, "+
                        "anc3."+table_schema.getColumn("","ancservice_treatment")+" as anc3_treatment, "+
                        "anc4."+table_schema.getColumn("","ancservice_treatment")+" as anc4_treatment, "+

                        "anc1."+table_schema.getColumn("","ancservice_advice")+" as anc1_advice, "+
                        "anc2."+table_schema.getColumn("","ancservice_advice")+" as anc2_advice, "+
                        "anc3."+table_schema.getColumn("","ancservice_advice")+" as anc3_advice, "+
                        "anc4."+table_schema.getColumn("","ancservice_advice")+" as anc4_advice, "+


                        "d."+table_schema.getColumn("","delivery_deliverycentername")+" as delivery_place, "+
                        "to_char(d."+table_schema.getColumn("","delivery_admissiondate")+",'DD MM YYYY') as admission_date, "+
                        "to_char(d."+table_schema.getColumn("","delivery_outcomedate")+",'DD MM YYYY') as delivery_date, "+
                        "d."+table_schema.getColumn("","delivery_outcometime")+" as outcome_time, "+
                        "CASE d."+table_schema.getColumn("","delivery_outcometype")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'সিজারিয়ান' else '' END as delivery_type, "+
                        "d."+table_schema.getColumn("","delivery_livebirth")+" as live_birth, "+
                        "d."+table_schema.getColumn("","delivery_stillbirth")+" as still_birth, "+
                        "d."+table_schema.getColumn("","delivery_stillbirthfresh")+" as still_birth_fresh, "+
                        "d."+table_schema.getColumn("","delivery_stillbirthmacerated")+" as still_birth_macerated, "+
                        "CASE d."+table_schema.getColumn("","delivery_applyoxytocin")+"::integer when 1 then '<input type=\"checkbox\" disabled checked> হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' " +
                                                   " when 2 then '<input type=\"checkbox\" disabled> হ্যাঁ &nbsp;<input type=\"checkbox\" checked disabled> না' " +
                                                   " ELSE '<input type=\"checkbox\" disabled > হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' END as apply_oxytocin, "+
                        "CASE d."+table_schema.getColumn("","delivery_applytraction")+"::integer when 1 then '<input type=\"checkbox\" disabled checked> হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' " +
                                                    " when 2 then '<input type=\"checkbox\" disabled> হ্যাঁ &nbsp;<input type=\"checkbox\" checked disabled> না' " +
                                                   " ELSE '<input type=\"checkbox\" disabled > হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' END as \"applyTraction\", "+
                        "CASE d."+table_schema.getColumn("","delivery_uterusmassage")+"::integer when 1 then '<input type=\"checkbox\" disabled checked> হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' " +
                                                    " when 2 then '<input type=\"checkbox\" disabled> হ্যাঁ &nbsp;<input type=\"checkbox\" checked disabled> না' " +
                                                    " ELSE '<input type=\"checkbox\" disabled > হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' END as \"uterusMassage\", "+
                        "d."+table_schema.getColumn("","delivery_attendantname")+" as attendant_name, "+
                        "dad.\"attendant_designation_ban\" as attendant_designation, "+
                        "d."+table_schema.getColumn("","delivery_nboy")+" as \"nBoy\", "+
                        "d."+table_schema.getColumn("","delivery_ngirl")+" as \"nGirl\", "+
                        "CASE d."+table_schema.getColumn("","delivery_excessbloodloss")+"::integer when 1 then 'checked' ELSE '' END as \"excessBloodLoss\", "+
                        "CASE d."+table_schema.getColumn("","delivery_latedelivery")+"::integer when 1 then 'checked' ELSE '' END as \"lateDelivery\", "+
                        "CASE d."+table_schema.getColumn("","delivery_blockeddelivery")+"::integer when 1 then 'checked' ELSE '' END as \"blockedDelivery\", "+
                        "CASE d."+table_schema.getColumn("","delivery_placenta")+"::integer when 1 then 'checked' ELSE '' END as placenta, "+
                        "CASE d."+table_schema.getColumn("","delivery_headache")+"::integer when 1 then 'checked' ELSE '' END as headache, "+
                        "CASE d."+table_schema.getColumn("","delivery_blurryvision")+"::integer when 1 then 'checked' ELSE '' END as \"blurryVision\", "+
                        "CASE d."+table_schema.getColumn("","delivery_otherbodypart")+"::integer when 1 then 'checked' ELSE '' END as \"otherBodyPart\", "+
                        "CASE d."+table_schema.getColumn("","delivery_convulsion")+"::integer when 1 then 'checked' ELSE '' END as convulsion, "+
                        "CASE d."+table_schema.getColumn("","delivery_others")+"::integer when 1 then 'checked' ELSE '' END as others, "+
                        "d."+table_schema.getColumn("","delivery_othersnote")+" as \"othersNote\", "+
                        "CASE d."+table_schema.getColumn("","delivery_episiotomy")+"::integer when 1 then 'checked' ELSE '' END as episiotomy, "+
                        "d."+table_schema.getColumn("","delivery_refer")+" as refer, "+
                        "d."+table_schema.getColumn("","delivery_referreason")+" as \"referReason\", "+
                        "d."+table_schema.getColumn("","delivery_refercentername")+" as \"referCenterName\", "+
                        "CASE d."+table_schema.getColumn("","delivery_misoprostol")+"::integer " +
                            "when 1 then '<input type=\"checkbox\" disabled checked> হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' " +
                            "when 2 then '<input type=\"checkbox\" disabled> হ্যাঁ &nbsp;<input type=\"checkbox\" checked disabled> না' " +
                            "ELSE '<input type=\"checkbox\" disabled > হ্যাঁ &nbsp;<input type=\"checkbox\" disabled> না' END  as misoprostol, "+

                        "round(nb."+table_schema.getColumn("","newborn_birthweight")+"::numeric,2) as \"birthWeight\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_immaturebirth")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"immatureBirth\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_dryingafterbirth")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"dryingAfterBirth\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_skintouch")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"skinTouch\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_chlorehexidin")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"chlorehexidin\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_breastfeed")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"breastFeed\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_resassitation")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"resassitation\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_stimulation")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"stimulation\", "+
                        "CASE nb."+table_schema.getColumn("","newborn_bagnmask")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"bagNMask\", "+
                        "nb."+table_schema.getColumn("","newborn_referreason")+" as \"nb_referReason\", "+
                        "nb."+table_schema.getColumn("","newborn_refercentername")+" as \"nb_referCenterName\", "+

                        "to_char(pncm1."+table_schema.getColumn("","pncservicemother_visitdate")+",'DD MM YYYY') as pncm1_service_date, "+
                        "to_char(pncm2."+table_schema.getColumn("","pncservicemother_visitdate")+",'DD MM YYYY') as pncm2_service_date, "+
                        "to_char(pncm3."+table_schema.getColumn("","pncservicemother_visitdate")+",'DD MM YYYY') as pncm3_service_date, "+
                        "to_char(pncm4."+table_schema.getColumn("","pncservicemother_visitdate")+",'DD MM YYYY') as pncm4_service_date, "+

                        "CASE WHEN pncm1."+table_schema.getColumn("","pncservicemother_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(pncm1."+table_schema.getColumn("","pncservicemother_bpsystolic")+",'/',pncm1."+table_schema.getColumn("","pncservicemother_bpdiastolic")+") END as pncm1_bp, "+
                        "CASE WHEN pncm2."+table_schema.getColumn("","pncservicemother_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(pncm2."+table_schema.getColumn("","pncservicemother_bpsystolic")+",'/',pncm2."+table_schema.getColumn("","pncservicemother_bpdiastolic")+") END  as pncm2_bp, "+
                        "CASE WHEN pncm3."+table_schema.getColumn("","pncservicemother_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(pncm3."+table_schema.getColumn("","pncservicemother_bpsystolic")+",'/',pncm3."+table_schema.getColumn("","pncservicemother_bpdiastolic")+") END  as pncm3_bp, "+
                        "CASE WHEN pncm4."+table_schema.getColumn("","pncservicemother_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(pncm4."+table_schema.getColumn("","pncservicemother_bpsystolic")+",'/',pncm4."+table_schema.getColumn("","pncservicemother_bpdiastolic")+") END  as pncm4_bp, "+

                        "pncm1."+table_schema.getColumn("","pncservicemother_temperature")+"::integer as pncm1_temp, "+
                        "pncm2."+table_schema.getColumn("","pncservicemother_temperature")+"::integer as pncm2_temp, "+
                        "pncm3."+table_schema.getColumn("","pncservicemother_temperature")+"::integer as pncm3_temp, "+
                        "pncm4."+table_schema.getColumn("","pncservicemother_temperature")+"::integer as pncm4_temp, "+

                        "CASE pncm1."+table_schema.getColumn("","pncservicemother_breastcondition")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ফোলা' when 3 then 'ব্যথাযুক্ত' else '' END as pncm1_breast_condition, "+
                        "CASE pncm2."+table_schema.getColumn("","pncservicemother_breastcondition")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ফোলা' when 3 then 'ব্যথাযুক্ত' else '' END as pncm2_breast_condition, "+
                        "CASE pncm3."+table_schema.getColumn("","pncservicemother_breastcondition")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ফোলা' when 3 then 'ব্যথাযুক্ত' else '' END as pncm3_breast_condition, "+
                        "CASE pncm4."+table_schema.getColumn("","pncservicemother_breastcondition")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ফোলা' when 3 then 'ব্যথাযুক্ত' else '' END as pncm4_breast_condition, "+

                       "CASE pncm1."+table_schema.getColumn("","pncservicemother_uterusinvolution")+"::integer when 1 then 'নাভি বরাবর' when 2 then 'নাভি হতে ২-৩ ইঞ্চি নিচে' when 3 then 'অনুভব করা যাচ্ছে না' else '' END as pncm1_uterus_involution, "+
                        "CASE pncm2."+table_schema.getColumn("","pncservicemother_uterusinvolution")+"::integer when 1 then 'নাভি বরাবর' when 2 then 'নাভি হতে ২-৩ ইঞ্চি নিচে' when 3 then 'অনুভব করা যাচ্ছে না' else '' END as pncm2_uterus_involution, "+
                        "CASE pncm3."+table_schema.getColumn("","pncservicemother_uterusinvolution")+"::integer when 1 then 'নাভি বরাবর' when 2 then 'নাভি হতে ২-৩ ইঞ্চি নিচে' when 3 then 'অনুভব করা যাচ্ছে না' else '' END as pncm3_uterus_involution, "+
                        "CASE pncm4."+table_schema.getColumn("","pncservicemother_uterusinvolution")+"::integer when 1 then 'নাভি বরাবর' when 2 then 'নাভি হতে ২-৩ ইঞ্চি নিচে' when 3 then 'অনুভব করা যাচ্ছে না' else '' END as pncm4_uterus_involution, "+

                        "CASE pncm1."+table_schema.getColumn("","pncservicemother_hematuria")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'দূর্গন্ধযুক্ত' when 3 then 'অতিরিক্ত' else '' END as pncm1_hematuria, "+
                        "CASE pncm2."+table_schema.getColumn("","pncservicemother_hematuria")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'দূর্গন্ধযুক্ত' when 3 then 'অতিরিক্ত' else '' END as pncm2_hematuria, "+
                        "CASE pncm3."+table_schema.getColumn("","pncservicemother_hematuria")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'দূর্গন্ধযুক্ত' when 3 then 'অতিরিক্ত' else '' END as pncm3_hematuria, "+
                        "CASE pncm4."+table_schema.getColumn("","pncservicemother_hematuria")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'দূর্গন্ধযুক্ত' when 3 then 'অতিরিক্ত' else '' END as pncm4_hematuria, "+

                        "CASE pncm1."+table_schema.getColumn("","pncservicemother_perineum")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ছিঁড়ে গিয়েছে' when 3 then 'অন্যান্য জটিলতা' else '' END as pncm1_perineum, "+
                        "CASE pncm2."+table_schema.getColumn("","pncservicemother_perineum")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ছিঁড়ে গিয়েছে' when 3 then 'অন্যান্য জটিলতা' else '' END as pncm2_perineum, "+
                        "CASE pncm3."+table_schema.getColumn("","pncservicemother_perineum")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ছিঁড়ে গিয়েছে' when 3 then 'অন্যান্য জটিলতা' else '' END as pncm3_perineum, "+
                        "CASE pncm4."+table_schema.getColumn("","pncservicemother_perineum")+"::integer when 1 then 'স্বাভাবিক' when 2 then 'ছিঁড়ে গিয়েছে' when 3 then 'অন্যান্য জটিলতা' else '' END as pncm4_perineum, "+

                        "CASE pncm1."+table_schema.getColumn("","pncservicemother_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as pncm1_anemia, "+
                        "CASE pncm2."+table_schema.getColumn("","pncservicemother_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as pncm2_anemia, "+
                        "CASE pncm3."+table_schema.getColumn("","pncservicemother_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as pncm3_anemia, "+
                        "CASE pncm4."+table_schema.getColumn("","pncservicemother_anemia")+"::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as pncm4_anemia, "+

                        "pncm1."+table_schema.getColumn("","pncservicemother_hemoglobin")+" as pncm1_hemoglobin, "+
                        "pncm2."+table_schema.getColumn("","pncservicemother_hemoglobin")+" as pncm2_hemoglobin, "+
                        "pncm3."+table_schema.getColumn("","pncservicemother_hemoglobin")+" as pncm3_hemoglobin, "+
                        "pncm4."+table_schema.getColumn("","pncservicemother_hemoglobin")+" as pncm4_hemoglobin, "+

                        "pncm1."+table_schema.getColumn("","pncservicemother_symptom")+" as pncm1_symptom, "+
                        "pncm2."+table_schema.getColumn("","pncservicemother_symptom")+" as pncm2_symptom, "+
                        "pncm3."+table_schema.getColumn("","pncservicemother_symptom")+" as pncm3_symptom, "+
                        "pncm4."+table_schema.getColumn("","pncservicemother_symptom")+" as pncm4_symptom, "+

                        "pncm1."+table_schema.getColumn("","pncservicemother_disease")+" as pncm1_disease, "+
                        "pncm2."+table_schema.getColumn("","pncservicemother_disease")+" as pncm2_disease, "+
                        "pncm3."+table_schema.getColumn("","pncservicemother_disease")+" as pncm3_disease, "+
                        "pncm4."+table_schema.getColumn("","pncservicemother_disease")+" as pncm4_disease, "+

                        "pncm1."+table_schema.getColumn("","pncservicemother_treatment")+" as pncm1_treatment, "+
                        "pncm2."+table_schema.getColumn("","pncservicemother_treatment")+" as pncm2_treatment, "+
                        "pncm3."+table_schema.getColumn("","pncservicemother_treatment")+" as pncm3_treatment, "+
                        "pncm4."+table_schema.getColumn("","pncservicemother_treatment")+" as pncm4_treatment, "+

                        "pncm1."+table_schema.getColumn("","pncservicemother_advice")+" as pncm1_advice, "+
                        "pncm2."+table_schema.getColumn("","pncservicemother_advice")+" as pncm2_advice, "+
                        "pncm3."+table_schema.getColumn("","pncservicemother_advice")+" as pncm3_advice, "+
                        "pncm4."+table_schema.getColumn("","pncservicemother_advice")+" as pncm4_advice, "+

                        "to_char(pncc1."+table_schema.getColumn("","pncservicechild_visitdate")+",'DD MM YYYY') as pncc1_service_date, "+
                        "to_char(pncc2."+table_schema.getColumn("","pncservicechild_visitdate")+",'DD MM YYYY') as pncc2_service_date, "+
                        "to_char(pncc3."+table_schema.getColumn("","pncservicechild_visitdate")+",'DD MM YYYY') as pncc3_service_date, "+
                        "to_char(pncc4."+table_schema.getColumn("","pncservicechild_visitdate")+",'DD MM YYYY') as pncc4_service_date, "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_temperature")+"::integer as pncc1_temp, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_temperature")+"::integer as pncc2_temp, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_temperature")+"::integer as pncc3_temp, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_temperature")+"::integer as pncc4_temp, "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_weight")+"::integer as pncc1_weight, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_weight")+"::integer as pncc2_weight, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_weight")+"::integer as pncc3_weight, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_weight")+"::integer as pncc4_weight, "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_breathingperminute")+"::integer as pncc1_breathing_per_minute, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_breathingperminute")+"::integer as pncc2_breathing_per_minute, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_breathingperminute")+"::integer as pncc3_breathing_per_minute, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_breathingperminute")+"::integer as pncc4_breathing_per_minute, "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_dangersign")+" as pncc1_danger_sign, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_dangersign")+" as pncc2_danger_sign, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_dangersign")+" as pncc3_danger_sign, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_dangersign")+" as pncc4_danger_sign, "+

                        "CASE pncc1."+table_schema.getColumn("","pncservicechild_breastfeedingonly")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"pncc1_breastFeedingOnly\", "+
                        "CASE pncc2."+table_schema.getColumn("","pncservicechild_breastfeedingonly")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"pncc2_breastFeedingOnly\", "+
                        "CASE pncc3."+table_schema.getColumn("","pncservicechild_breastfeedingonly")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"pncc3_breastFeedingOnly\", "+
                        "CASE pncc4."+table_schema.getColumn("","pncservicechild_breastfeedingonly")+"::integer when 1 then 'হ্যাঁ' when 2 then 'না' ELSE '' END  as \"pncc4_breastFeedingOnly\", "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_symptom")+" as pncc1_symptom, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_symptom")+" as pncc2_symptom, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_symptom")+" as pncc3_symptom, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_symptom")+" as pncc4_symptom, "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_disease")+" as pncc1_disease, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_disease")+" as pncc2_disease, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_disease")+" as pncc3_disease, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_disease")+" as pncc4_disease, "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_treatment")+" as pncc1_treatment, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_treatment")+" as pncc2_treatment, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_treatment")+" as pncc3_treatment, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_treatment")+" as pncc4_treatment, "+

                        "pncc1."+table_schema.getColumn("","pncservicechild_advice")+" as pncc1_advice, "+
                        "pncc2."+table_schema.getColumn("","pncservicechild_advice")+" as pncc2_advice, "+
                        "pncc3."+table_schema.getColumn("","pncservicechild_advice")+" as pncc3_advice, "+
                        "pncc4."+table_schema.getColumn("","pncservicechild_advice")+" as pncc4_advice, "+

                        "dth."+table_schema.getColumn("","death_deathofpregwomen")+" as \"deathOfPregWomen\", "+
                        "dthr."+table_schema.getColumn("","deathreason_detail")+" as \"causeOfDeath\", "+
                        "CASE dth."+table_schema.getColumn("","death_placeofdeath")+"::integer when 1 then 'বাড়ি' when 2 then  'সেবাকেন্দ্র' else 'অন্যন্য' END as \"placeOfDeath\", "+
                        "dth."+table_schema.getColumn("","death_deathdt")+" as \"deathDT\", "+

                        "dthrc."+table_schema.getColumn("","deathreason_detail")+" as \"causeOfDeath_child\", "+
                        "CASE dthc."+table_schema.getColumn("","death_placeofdeath")+"::integer when 1 then 'বাড়ি' when 2 then 'সেবাকেন্দ্র' else 'অন্যন্য' END as \"placeOfDeath_child\", "+
                        "dthc."+table_schema.getColumn("","death_deathdt")+" as \"deathDT_child\" "+

                        "FROM       " + table_schema.getTable("table_pregwomen") + " as pw " +
                        "INNER JOIN " + table_schema.getTable("table_clientmap") + " as cm ON cm." + table_schema.getColumn("","clientmap_generatedid") + " = pw." + table_schema.getColumn("","pregwomen_healthid") +" \n" +

                        "LEFT JOIN " + table_schema.getTable("table_immunizationhistory") +" as tt1 ON tt1."+table_schema.getColumn("","immunizationhistory_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND tt1."+table_schema.getColumn("","immunizationhistory_imudose")+"=1 " +
                        "LEFT JOIN " + table_schema.getTable("table_immunizationhistory") +" as tt2 ON tt2."+table_schema.getColumn("","immunizationhistory_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND tt2."+table_schema.getColumn("","immunizationhistory_imudose")+"=2 " +
                        "LEFT JOIN " + table_schema.getTable("table_immunizationhistory") +" as tt3 ON tt3."+table_schema.getColumn("","immunizationhistory_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND tt3."+table_schema.getColumn("","immunizationhistory_imudose")+"=3 " +
                        "LEFT JOIN " + table_schema.getTable("table_immunizationhistory") +" as tt4 ON tt4."+table_schema.getColumn("","immunizationhistory_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND tt4."+table_schema.getColumn("","immunizationhistory_imudose")+"=4 " +
                        "LEFT JOIN " + table_schema.getTable("table_immunizationhistory") +" as tt5 ON tt5."+table_schema.getColumn("","immunizationhistory_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND tt5."+table_schema.getColumn("","immunizationhistory_imudose")+"=5 " +

                        "LEFT JOIN " + table_schema.getTable("table_ancservice")  + " as anc1 ON anc1." + table_schema.getColumn("","ancservice_healthid")+ " = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND anc1."+table_schema.getColumn("","ancservice_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND anc1."+table_schema.getColumn("","ancservice_serviceid")+"=1 AND anc1."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+")"+
                        "LEFT JOIN " + table_schema.getTable("table_ancservice")  + " as anc2 ON anc2." + table_schema.getColumn("","ancservice_healthid")+ " = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND anc2."+table_schema.getColumn("","ancservice_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND anc2."+table_schema.getColumn("","ancservice_serviceid")+"=2 AND anc2."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+")"+
                        "LEFT JOIN " + table_schema.getTable("table_ancservice")  + " as anc3 ON anc3." + table_schema.getColumn("","ancservice_healthid")+ " = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND anc3."+table_schema.getColumn("","ancservice_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND anc3."+table_schema.getColumn("","ancservice_serviceid")+"=3 AND anc3."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+")"+
                        "LEFT JOIN " + table_schema.getTable("table_ancservice")  + " as anc4 ON anc4." + table_schema.getColumn("","ancservice_healthid")+ " = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND anc4."+table_schema.getColumn("","ancservice_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND anc4."+table_schema.getColumn("","ancservice_serviceid")+"=4 AND anc4."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+")"+

                        "LEFT JOIN " + table_schema.getTable("table_delivery")  + " as d ON d."+table_schema.getColumn("","delivery_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid")+" AND d."+table_schema.getColumn("","delivery_pregno")+" = pw."+table_schema.getColumn("","pregwomen_pregno")+" AND d."+table_schema.getColumn("","delivery_providerid")+" IN ("+providerList+")"+
                        "LEFT JOIN " + table_schema.getTable("table_newborn")   + " as nb ON nb."+ table_schema.getColumn("","newborn_healthid") +" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND nb."+ table_schema.getColumn("","newborn_pregno") +" = pw."+ table_schema.getColumn("","pregwomen_pregno") +" AND nb."+ table_schema.getColumn("","newborn_providerid") +" IN ("+providerList+") "+

                        "LEFT JOIN " + table_schema.getTable("table_pncservicemother")  + " as pncm1 ON pncm1."+ table_schema.getColumn("","pncservicemother_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND pncm1."+table_schema.getColumn("","pncservicemother_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND pncm1."+table_schema.getColumn("","pncservicemother_serviceid")+"=1 AND pncm1."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") "+
                        "LEFT JOIN " + table_schema.getTable("table_pncservicemother")  + " as pncm2 ON pncm2."+ table_schema.getColumn("","pncservicemother_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND pncm2."+table_schema.getColumn("","pncservicemother_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND pncm1."+table_schema.getColumn("","pncservicemother_serviceid")+"=2 AND pncm2."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") "+
                        "LEFT JOIN " + table_schema.getTable("table_pncservicemother")  + " as pncm3 ON pncm3."+ table_schema.getColumn("","pncservicemother_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND pncm3."+table_schema.getColumn("","pncservicemother_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND pncm1."+table_schema.getColumn("","pncservicemother_serviceid")+"=3 AND pncm3."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") "+
                        "LEFT JOIN " + table_schema.getTable("table_pncservicemother")  + " as pncm4 ON pncm4."+ table_schema.getColumn("","pncservicemother_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid") +" AND pncm4."+table_schema.getColumn("","pncservicemother_pregno")+"=pw."+table_schema.getColumn("","pregwomen_pregno")+" AND pncm1."+table_schema.getColumn("","pncservicemother_serviceid")+"=4 AND pncm4."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") "+

                        "LEFT JOIN " + table_schema.getTable("table_pncservicechild")   +  " as pncc1 ON pncc1."+ table_schema.getColumn("","pncservicechild_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid")+" AND pncc1."+ table_schema.getColumn("","pncservicechild_pregno")+"=pw."+ table_schema.getColumn("","pregwomen_pregno")+" AND pncc1."+ table_schema.getColumn("","pncservicechild_serviceid")+"=1 AND pncc1."+ table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") "+
                        "LEFT JOIN " + table_schema.getTable("table_pncservicechild")   +  " as pncc2 ON pncc2."+ table_schema.getColumn("","pncservicechild_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid")+" AND pncc2."+ table_schema.getColumn("","pncservicechild_pregno")+"=pw."+ table_schema.getColumn("","pregwomen_pregno")+" AND pncc2."+ table_schema.getColumn("","pncservicechild_serviceid")+"=2 AND pncc2."+ table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") "+
                        "LEFT JOIN " + table_schema.getTable("table_pncservicechild")   +  " as pncc3 ON pncc3."+ table_schema.getColumn("","pncservicechild_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid")+" AND pncc3."+ table_schema.getColumn("","pncservicechild_pregno")+"=pw."+ table_schema.getColumn("","pregwomen_pregno")+" AND pncc3."+ table_schema.getColumn("","pncservicechild_serviceid")+"=3 AND pncc3."+ table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") "+
                        "LEFT JOIN " + table_schema.getTable("table_pncservicechild")   +  " as pncc4 ON pncc4."+ table_schema.getColumn("","pncservicechild_healthid")+" = pw."+ table_schema.getColumn("","pregwomen_healthid")+" AND pncc4."+ table_schema.getColumn("","pncservicechild_pregno")+"=pw."+ table_schema.getColumn("","pregwomen_pregno")+" AND pncc4."+ table_schema.getColumn("","pncservicechild_serviceid")+"=4 AND pncc4."+ table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") "+

                        "LEFT JOIN " + table_schema.getTable("table_village") + " as vil ON cm."+table_schema.getColumn("","clientmap_villageid")+"=vil."+table_schema.getColumn("","village_villageid")+" and cm."+table_schema.getColumn("","clientmap_mouzaid")+"=vil."+table_schema.getColumn("","village_mouzaid")+" and cm."+table_schema.getColumn("","clientmap_upazilaid")+"=vil."+table_schema.getColumn("","village_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_unionid")+"=vil."+table_schema.getColumn("","village_unionid")+"  and cm."+table_schema.getColumn("","clientmap_zillaid")+"=vil."+table_schema.getColumn("","village_zillaid")+
                        "LEFT JOIN " + table_schema.getTable("table_elco")+ "  as elc ON elc."+table_schema.getColumn("","elco_healthid")+"=pw."+table_schema.getColumn("","pregwomen_healthid")+"  " +
                        "LEFT JOIN " + table_schema.getTable("table_regserial")+ " as rs ON rs."+table_schema.getColumn("","regserial_healthid")+"=cm."+table_schema.getColumn("","clientmap_healthid")+" and rs."+table_schema.getColumn("","regserial_servicecategory")+" =1 and rs."+table_schema.getColumn("","regserial_providerid")+" IN ("+providerList+")  " +
                        "LEFT JOIN " + table_schema.getTable("table_death") + " as dth ON dth."+table_schema.getColumn("","death_healthid")+"=cm."+table_schema.getColumn("","clientmap_healthid")+" and dth."+table_schema.getColumn("","death_deathofpregwomen")+"=1 and  dth."+table_schema.getColumn("","death_providerid")+" IN ("+providerList+")  " +
                        "LEFT JOIN " + table_schema.getTable("table_death") + " as dthc ON dthc."+table_schema.getColumn("","death_healthid")+"=cm."+table_schema.getColumn("","clientmap_healthid")+" and dthc."+table_schema.getColumn("","death_deathofpregwomen")+"!=1  and dthc."+table_schema.getColumn("","death_pregno")+" !=0 and dthc."+table_schema.getColumn("","death_childno")+"!=0 and dthc."+table_schema.getColumn("","death_providerid")+" IN ("+providerList+")  " +
                        "LEFT JOIN " + table_schema.getTable("table_deathreason") + " as dthr ON dthr."+table_schema.getColumn("","deathreason_deathreasonid")+"=dth."+table_schema.getColumn("","death_causeofdeath")+"  " +
                        "LEFT JOIN " + table_schema.getTable("table_deliveryattendantdesignation") + " as dad ON dad.attendant_code = d."+table_schema.getColumn("","delivery_attendantdesignation")+" "+
                        "LEFT JOIN " + table_schema.getTable("table_deathreason") + " as dthrc ON dthrc."+table_schema.getColumn("","deathreason_deathreasonid")+"=dthc."+table_schema.getColumn("","death_causeofdeath")+"  " +
                        "WHERE  " +
                        "(anc1."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+") AND anc1."+table_schema.getColumn("","ancservice_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(anc2."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+") AND anc2."+table_schema.getColumn("","ancservice_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(anc3."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+") AND anc3."+table_schema.getColumn("","ancservice_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(anc4."+table_schema.getColumn("","ancservice_providerid")+" IN ("+providerList+") AND anc4."+table_schema.getColumn("","ancservice_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+

                        "(pncm1."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") AND pncm1."+table_schema.getColumn("","pncservicemother_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(pncm2."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") AND pncm2."+table_schema.getColumn("","pncservicemother_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(pncm3."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") AND pncm3."+table_schema.getColumn("","pncservicemother_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(pncm4."+table_schema.getColumn("","pncservicemother_providerid")+" IN ("+providerList+") AND pncm4."+table_schema.getColumn("","pncservicemother_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
//
                        "(pncc1."+table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") AND pncc1."+table_schema.getColumn("","pncservicechild_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(pncc2."+table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") AND pncc2."+table_schema.getColumn("","pncservicechild_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(pncc3."+table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") AND pncc3."+table_schema.getColumn("","pncservicechild_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(pncc4."+table_schema.getColumn("","pncservicechild_providerid")+" IN ("+providerList+") AND pncc4."+table_schema.getColumn("","pncservicechild_visitdate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
//
                        "(d."+table_schema.getColumn("","delivery_providerid")+" IN ("+providerList+") AND d."+table_schema.getColumn("","delivery_outcomedate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) or"+
                        "(nb."+table_schema.getColumn("","newborn_providerid")+" IN ("+providerList+") AND nb."+table_schema.getColumn("","newborn_outcomedate")+" BETWEEN ('"+startDate+"') AND ('"+endDate+"'))";

//                       "anc1.\"providerId\" IN ("+providerList+")";// and (" +// and cm."healthId"=655545
//                        "(\"LMP\" BETWEEN ('"+startDate+"') AND ('"+endDate+"')) " +
//                        "OR " +
//                        "(\"EDD\"::date + interval '48 days' BETWEEN ('"+startDate+"') AND ('"+endDate+"')))";
                System.out.println(sql);
                FacilityDB dbFacility = new FacilityDB();
                DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla,upazila);

                DBOperation dbOp = new DBOperation();
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject, sql);
                ResultSet rs = dbObject.getResultSet();
                //*******************************************//

                List<JSONObject> jsonObj = new ArrayList<JSONObject>();
                ResultSetMetaData meta = rs.getMetaData();
                while (rs.next()) {
                    HashMap<String, String> data = new HashMap<String, String>();
                    for (int i = 1; i <= meta.getColumnCount(); i++) {
                        String key = meta.getColumnName(i);
                        String value = rs.getString(key);
                        if(value == null){
                            value = "";
                        }
                        data.put(key, value);
                    }
                    jsonObj.add(new JSONObject(data));
                }
                return jsonObj.toString();

                //*******************************************//
            }catch (Exception e){
                System.out.println(e);
            }

//			System.out.println(startDate+"   "+endDate);
		}
		catch(Exception e){
			System.out.println(e);
            resultSet = "Bad Url Request";
		}
		return resultSet;
	}

}
