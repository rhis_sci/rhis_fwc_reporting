
package org.sci.rhis.report;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;
import java.util.concurrent.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.model.GetResult;
import org.sci.rhis.model.GetThreadResult;
import org.sci.rhis.util.CalendarDate;

public  class GetMapInfo {
	
	public static LinkedHashMap<String, LinkedHashMap<String, String>> resultSetThread = new LinkedHashMap<String, LinkedHashMap<String, String>>();
	public static JSONObject searchInfo = null;
	public static JSONObject getInfo(JSONObject geoInfo) {
		searchInfo = geoInfo;
		JSONObject info = new JSONObject();
		int zilla = Integer.parseInt(geoInfo.getString("zilla"));
		DBSchemaInfo table_schema = new DBSchemaInfo();

		DBListProperties setZillaList = new DBListProperties();
		Properties db_prop = setZillaList.setProperties();
		String[] zilla_list = db_prop.getProperty("zilla_all").split(",");

		try{
			String geoSql = "";
			int upazila = Integer.parseInt(geoInfo.getString("upazila"));
			String type = geoInfo.getString("type");
			String method_type = "";
			if (geoInfo.has("methodType"))
				method_type = geoInfo.getString("methodType");

			boolean active_db;

			Calendar c = Calendar.getInstance();
//			int year = c.get(Calendar.YEAR);
//			int month = c.get(Calendar.MONTH)+1;
			int year = geoInfo.has("year")?Integer.parseInt(geoInfo.getString("year")):c.get(Calendar.YEAR);
			int month = geoInfo.has("month")?Integer.parseInt(geoInfo.getString("month")):c.get(Calendar.MONTH)+1;
			String days = "01";
			int daysInMonth = CalendarDate.daysInMonth(year, month);
			String start_date = year + "-" + month + "-" + days;
			String end_date = year + "-" + month + "-" + daysInMonth;

			// 0 for get population information
			if(type.equals("0")){
				DBOperation dbOp = new DBOperation();
//				DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
                DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();//moved to rhis_facility_central db instead of rhis_36
				
				JSONObject zilla_info = new JSONObject();
				JSONObject upazila_info = new JSONObject();
				
				double total_population;
				double male;
				double female;
				
				double temp_total_population;
				double temp_male;
				double temp_female;
				
				double db_male;
				double db_female;
				double db_rate;

				int total_year = year-2017;

				String population_sql = "select * from "+ table_schema.getTable("table_censusdata") ;
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,population_sql);
				ResultSet rs = dbObject.getResultSet();
				ResultSetMetaData metadata = rs.getMetaData();
				
				while(rs.next()){
					
					db_male = Double.parseDouble(rs.getString(metadata.getColumnName(6)));
					db_female = Double.parseDouble(rs.getString(metadata.getColumnName(7)));
					db_rate = Double.parseDouble(rs.getString(metadata.getColumnName(4)));

					// Ex: male = 2011 census male x (1+growth_rate/100)total year after 2011
					male = Math.pow((1+(db_rate/100)),total_year)*db_male;
					female = Math.pow((1+(db_rate/100)),total_year)*db_female;
					total_population = male + female;

					// calculate district wise total population
					if(!info.has(rs.getString(metadata.getColumnName(1)))){
						JSONObject population_info = new JSONObject();
						population_info.put("Total", total_population);
						population_info.put("Male", male);
						population_info.put("Female", female);
						info.put(rs.getString(metadata.getColumnName(1)), population_info);
					}
					else{
						JSONObject population_info = new JSONObject();
						zilla_info = info.getJSONObject(rs.getString(metadata.getColumnName(1)));
						temp_total_population = Double.parseDouble(zilla_info.getString("Total")) + total_population;
						temp_male = Double.parseDouble(zilla_info.getString("Male")) + male;
						temp_female = Double.parseDouble(zilla_info.getString("Female")) + female;

						population_info.put("Total", temp_total_population);
						population_info.put("Male", temp_male);
						population_info.put("Female", temp_female);
						info.put(rs.getString(metadata.getColumnName(1)), population_info);
						
					}

					// calculate upazila wise total population
					if(!info.has(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)))){
						JSONObject population_info = new JSONObject();
						population_info.put("Total", total_population);
						population_info.put("Male", male);
						population_info.put("Female", female);
						info.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)), population_info);
					}
					else{
						JSONObject population_info = new JSONObject();
						upazila_info = info.getJSONObject(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)));
						temp_total_population = Double.parseDouble(upazila_info.getString("Total")) + total_population;
						temp_male = Double.parseDouble(upazila_info.getString("Male")) + male;
						temp_female = Double.parseDouble(upazila_info.getString("Female")) + female;
						
						population_info.put("Total", temp_total_population);
						population_info.put("Male", temp_male);
						population_info.put("Female", temp_female);
						info.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)), population_info);
					}

					// calculate union wise total population
					JSONObject population_info = new JSONObject();
					population_info.put("Total", total_population);
					population_info.put("Male", male);
					population_info.put("Female", female);
					info.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2))+"_"+rs.getString(metadata.getColumnName(3)), population_info);
				
				}

				for(int z=0;z<zilla_list.length;z++){
					if(!info.has(zilla_list[z])){
						JSONObject population_info = new JSONObject();
						population_info.put("Total", "");
						population_info.put("Male", "");
						population_info.put("Female", "");
						info.put(zilla_list[z], population_info);
					}
				}
				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
				dbObject = null;
			}
			// District wise result given
			else if(type.equals("1")){
				
				LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();

				long startTime = System.currentTimeMillis();

				GetResult getResult = new GetResult("1",method_type,"0","0","0",start_date,end_date);
				info = getResult.getSSAggrResult(); // get the result zilla wise

//				JSONObject jsonResultSet = getResult.getSSAggrResult(); // get the result upazila wise
//				System.out.println(jsonResultSet);

//				int tempValue;
//				// calculate the result district wise
//				for(int m=0;m<zilla_list.length;m++){
//					finalResult.put(zilla_list[m], new LinkedHashMap<String, String>());
//					String[] upazila_list = db_prop.getProperty(zilla_list[m]).split(",");
//					for (int l = 0; l < upazila_list.length; l++) {
//						if(jsonResultSet.has(zilla_list[m]+"_"+upazila_list[l])) {
//							JSONObject upazilaResultSet = new JSONObject(jsonResultSet.getString(zilla_list[m] + "_" + upazila_list[l]));
//							Iterator<String> iterator = upazilaResultSet.keys();
//							while (iterator.hasNext()) {
//								String key = iterator.next();
//								if (l>0)
//									tempValue = Integer.parseInt(finalResult.get(zilla_list[m]).get(key)) + Integer.parseInt(upazilaResultSet.getString(key));
//
//								else
//									tempValue = Integer.parseInt(upazilaResultSet.getString(key));
//								finalResult.get(zilla_list[m]).put(key, Integer.toString(tempValue));
//							}
//						}
//					}
//				}

				long stopTime = System.currentTimeMillis();
		        long elapsedTime = stopTime - startTime;
		        System.out.println(elapsedTime);

				getResult = null;

		        // import upozila count and union count into info json
//				String zilla_info = "select count(distinct("+table_schema.getColumn("","unions_upazilaid")+")) as upz_count, count("+table_schema.getColumn("","unions_unionid")+") as union_count, "+table_schema.getColumn("","unions_zillaid")+" as zillaid from "+ table_schema.getTable("table_unions") +" where "+table_schema.getColumn("","unions_zillaid",zilla_list,"in")+" and "+table_schema.getColumn("", "unions_municipalityid",new String[]{},"isnull")+" group by "+table_schema.getColumn("","unions_zillaid");
//                DBOperation dbOp = new DBOperation();
//                DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
//				dbObject = dbOp.dbCreateStatement(dbObject);
//				dbObject = dbOp.dbExecute(dbObject,zilla_info);
//				ResultSet rs = dbObject.getResultSet();
//
//				while(rs.next()){
//					finalResult.get(rs.getString("zillaid")).put("Upazila_count", rs.getString("upz_count"));
//					finalResult.get(rs.getString("zillaid")).put("Union_count", rs.getString("union_count"));
//
//					JSONObject temp_info = new JSONObject(finalResult.get(rs.getString("zillaid")));
//					info.put(rs.getString("zillaid"), temp_info);
//				}


//				System.out.println(info);
//				dbOp.closeStatement(dbObject);
//				dbOp.closeConn(dbObject);
//				dbObject = null;
				
			}
			// Upazila wise result given
			else if(type.equals("2")){
			
				DBOperation dbOp = new DBOperation();
                DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();//moved to rhis_facility_central db instead of rhis_36
//				DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

				GetResult getResult = new GetResult("2",method_type,"0",Integer.toString(zilla),"0",start_date,end_date);
				info = getResult.getSSAggrResult(); // get upazila wise result of selected district

//				JSONObject jsonResultSet = getResult.getThreadResult(); // get upazila wise result of selected district
//
//				geoSql = "select "+table_schema.getColumn("","upazila_upazilanameeng")+", "+table_schema.getColumn("","upazila_upazilaid")+", "+table_schema.getColumn("","upazila_zillaid")+", count("+table_schema.getColumn("","unions_unionid")+") as union_count " +
//						"from "+ table_schema.getTable("table_upazila") +" join "+ table_schema.getTable("table_unions") +" using ("+table_schema.getColumn("","upazila_zillaid")+", "+table_schema.getColumn("","upazila_upazilaid")+") " +
//						"where "+table_schema.getColumn("", "upazila_zillaid",new String[]{Integer.toString(zilla)},"=") + "group by " + table_schema.getColumn("","upazila_upazilanameeng") + ", " +table_schema.getColumn("","upazila_upazilaid")+ ", "+table_schema.getColumn("","upazila_zillaid");
//
//				dbObject = dbOp.dbExecute(dbObject,geoSql);
//				ResultSet rs = dbObject.getResultSet();
//				ResultSetMetaData metadata = rs.getMetaData();
//
//				JSONObject geo_name = new JSONObject();
//				JSONObject geo_key = new JSONObject();
//				JSONObject upazilaInfo = new JSONObject();
//				while(rs.next()){
//					active_db = GetResultSet.check_active_db(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)));
//					if(active_db) {
//                        geo_name.put("Upazila Name", rs.getString(metadata.getColumnName(1)));
//                        info.put(zilla + "_" + rs.getString(metadata.getColumnName(2)), geo_name); // import selected upazila name
//                        info.put(zilla + "_" + rs.getString(metadata.getColumnName(2)), jsonResultSet.get(zilla+"_"+rs.getString(metadata.getColumnName(2)))); // import selected upazila result
//                        geo_key = info.getJSONObject(zilla + "_" + rs.getString(metadata.getColumnName(2)));
//                        geo_key.put("Upazila Name", rs.getString(metadata.getColumnName(1)));
//						upazilaInfo.put(zilla+"_"+rs.getString(metadata.getColumnName(2)), rs.getString("union_count")); // import union count of selected upazila
//					}
//				}
//				info.put("Upazila_info", upazilaInfo); // import full union count result
//
//				getResult = null;
//
//				dbOp.closeStatement(dbObject);
//				dbOp.closeConn(dbObject);
//				dbObject = null;
				
			}
			// Union wise result given
			else if(type.equals("3")){
				GetResult getResult = new GetResult("3",method_type,"0",Integer.toString(zilla),Integer.toString(upazila),start_date,end_date);
				info = getResult.getSSAggrResult();
				getResult = null;
				
			}
			// Facility information given
			else if(type.equals("4")) {
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject;

				String facility_info_sql = "";
				if(db_prop.getProperty("zilla").contains(Integer.toString(zilla))) { // check if requested zilla is in implemented zilla list
					// get all facility information of implemented district
				    if (zilla == 0) {
						String[] upazila_list = null;
						for (int z = 0; z < zilla_list.length; z++) {
							upazila_list = db_prop.getProperty(zilla_list[z]).split(",");
							facility_info_sql = "select " + table_schema.getColumn("", "facility_registry_facilityid") + " as facilityid, " + table_schema.getColumn("", "facility_registry_facility_type") + " as facility_type, " +
									"" + table_schema.getColumn("", "facility_registry_lat") + " as lat, " + table_schema.getColumn("", "facility_registry_lon") + " as lon, " +
									"" + table_schema.getColumn("", "facility_registry_facility_name") + " || ' ' || " + table_schema.getColumn("", "facility_registry_facility_type") + " as \"Facility_Name\", " +
									"category as facility_category, last_update_date from " + table_schema.getTable("table_facility_registry") + " " +
									"left join dblink('host=103.48.19.7 port=5412  dbname=facility_assessment user=rhis_admin password=rhis12#rhis', $$ " +
									"select facility_id, (case when last>199 then 'A' when last between 100 and 199 then 'B' else 'C' end) category, last_created_at::date as last_update_date " +
									"from facility_registry join " +
									"(with CTE as (select * " +
									",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at asc ) ascrnk " +
									",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at desc ) desrnk " +
									"from (SELECT facility_id, created_at, sum(value) AS max_value " +
									"FROM questions_options_mapping GROUP BY facility_id, created_at) TestTable) " +
									"select t1.facility_id, t2.max_value as last, t2.created_at as last_created_at from( select * from cte where ascrnk = 1) t1 " +
									"left join ( select * from cte where desrnk = 1) t2 on t1.facility_id = t2.facility_id ) s on s.facility_id=facility_registry.facilityid " +
									"where zillaid::integer=" + zilla_list[z] + " $$) as facility_info (facility_id integer, category text, last_update_date date) on facility_info.facility_id=" + table_schema.getColumn("table", "facility_registry_facilityid") + " " +
									"where (" + table_schema.getColumn("", "facility_registry_lat") + " != '0.0' or " + table_schema.getColumn("", "facility_registry_lat", new String[]{}, "isnotnull") + ") " +
									"and (" + table_schema.getColumn("", "facility_registry_lon") + " != '0.0' or " + table_schema.getColumn("", "facility_registry_lon", new String[]{}, "isnotnull") + ") " +
									"and " + table_schema.getColumn("", "facility_registry_zillaid", new String[]{zilla_list[z]}, "=");

							System.out.println(facility_info_sql);


//							if(zilla_list[z].equals("36")){
								 dbObject = new FacilityDB().centralMonitorngInfo();

//							}else {
//								 dbObject = new FacilityDB().facilityDBInfo(Integer.parseInt(zilla_list[z]));
//							}
							dbObject = dbOp.dbCreateStatement(dbObject);
							dbObject = dbOp.dbExecute(dbObject, facility_info_sql);
							ResultSet rs = dbObject.getResultSet();
							ResultSetMetaData metadata = rs.getMetaData();

							JSONObject facilityInfo = new JSONObject();
							JSONArray facility_array = new JSONArray();
							while (rs.next()) {
								facilityInfo = new JSONObject();
								facilityInfo.put(metadata.getColumnName(1), rs.getString("facilityid"));
								facilityInfo.put(metadata.getColumnName(2), rs.getString("facility_type"));
								facilityInfo.put(metadata.getColumnName(3), rs.getString("lat"));
								facilityInfo.put(metadata.getColumnName(4), rs.getString("lon"));
								facilityInfo.put(metadata.getColumnName(5), rs.getString("Facility_Name"));
								facilityInfo.put(metadata.getColumnName(6), rs.getString("facility_category"));
								facilityInfo.put(metadata.getColumnName(7), rs.getString("last_update_date"));

								facility_array.put(facilityInfo);
							}

							info.put(zilla_list[z], facility_array);

							dbOp.closeStatement(dbObject);
							dbOp.closeConn(dbObject);
							dbObject = null;
						}

						System.out.println(info);
					} else {
				        // Get facility information of selected upazila
						if (upazila > 0) {
							facility_info_sql = "select " + table_schema.getColumn("", "facility_registry_facilityid") + " as facilityid, " + table_schema.getColumn("", "facility_registry_facility_type") + " as facility_type, " +
									"" + table_schema.getColumn("", "facility_registry_lat") + " as lat, " + table_schema.getColumn("", "facility_registry_lon") + " as lon, " +
									"" + table_schema.getColumn("", "facility_registry_facility_name") + " || ' ' || " + table_schema.getColumn("", "facility_registry_facility_type") + " as \"Facility_Name\", " +
									"category as facility_category, last_update_date from " + table_schema.getTable("table_facility_registry") + " " +
									"left join dblink('host=103.48.19.7 port=5412 dbname=facility_assessment user=rhis_admin password=rhis12#rhis', $$ " +
									"select facility_id, (case when last>199 then 'A' when last between 100 and 199 then 'B' else 'C' end) category, last_created_at::date as last_update_date " +
									"from facility_registry join " +
									"(with CTE as (select * " +
									",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at asc ) ascrnk " +
									",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at desc ) desrnk " +
									"from (SELECT facility_id, created_at, sum(value) AS max_value " +
									"FROM questions_options_mapping GROUP BY facility_id, created_at) TestTable) " +
									"select t1.facility_id, t2.max_value as last, t2.created_at as last_created_at from( select * from cte where ascrnk = 1) t1 " +
									"left join ( select * from cte where desrnk = 1) t2 on t1.facility_id = t2.facility_id ) s on s.facility_id=facility_registry.facilityid " +
									"where zillaid::integer=" + zilla + " and upazilaid::integer=" + upazila + " $$) as facility_info (facility_id integer, category text, last_update_date date) on facility_info.facility_id=" + table_schema.getColumn("table", "facility_registry_facilityid") + " " +
									"where (" + table_schema.getColumn("", "facility_registry_lat") + " != '0.0' or " + table_schema.getColumn("", "facility_registry_lat", new String[]{}, "isnotnull") + ") and " +
									"(" + table_schema.getColumn("", "facility_registry_lon") + " != '0.0' or " + table_schema.getColumn("", "facility_registry_lon", new String[]{}, "isnotnull") + ") and " +
									"" + table_schema.getColumn("", "facility_registry_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "facility_registry_upazilaid", new String[]{Integer.toString(upazila)}, "=");
						}
						// Get facility information of selected zilla
						else {
							facility_info_sql = "select " + table_schema.getColumn("", "facility_registry_facilityid") + " as facilityid, " + table_schema.getColumn("", "facility_registry_facility_type") + " as facility_type, " +
									"" + table_schema.getColumn("", "facility_registry_lat") + " as lat, " + table_schema.getColumn("", "facility_registry_lon") + " as lon, " +
									"" + table_schema.getColumn("", "facility_registry_facility_name") + " || ' ' || " + table_schema.getColumn("", "facility_registry_facility_type") + " as \"Facility_Name\", " +
									"category as facility_category, last_update_date from " + table_schema.getTable("table_facility_registry") + " " +
									"left join dblink('host=103.48.19.7 port=5412 dbname=facility_assessment user=rhis_admin password=rhis12#rhis', $$ " +
									"select facility_id, (case when last>199 then 'A' when last between 100 and 199 then 'B' else 'C' end) category, last_created_at::date as last_update_date " +
									"from facility_registry join " +
									"(with CTE as (select * " +
									",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at asc ) ascrnk " +
									",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at desc ) desrnk " +
									"from (SELECT facility_id, created_at, sum(value) AS max_value " +
									"FROM questions_options_mapping GROUP BY facility_id, created_at) TestTable) " +
									"select t1.facility_id, t2.max_value as last, t2.created_at as last_created_at from( select * from cte where ascrnk = 1) t1 " +
									"left join ( select * from cte where desrnk = 1) t2 on t1.facility_id = t2.facility_id ) s on s.facility_id=facility_registry.facilityid " +
									"where zillaid::integer=" + zilla + " $$) as facility_info (facility_id integer, category text, last_update_date date) on facility_info.facility_id=" + table_schema.getColumn("table", "facility_registry_facilityid") + " " +
									"where (" + table_schema.getColumn("", "facility_registry_lat") + " != '0.0' or " + table_schema.getColumn("", "facility_registry_lat", new String[]{}, "isnotnull") + ") and " +
									"(" + table_schema.getColumn("", "facility_registry_lon") + " != '0.0' or " + table_schema.getColumn("", "facility_registry_lon", new String[]{}, "isnotnull") + ") and " +
									"" + table_schema.getColumn("", "facility_registry_zillaid", new String[]{Integer.toString(zilla)}, "=");
						}

						System.out.println(facility_info_sql);
//						if(zilla==36){
							dbObject = new FacilityDB().centralMonitorngInfo();
//						}else {
//							dbObject = new FacilityDB().facilityDBInfo(zilla);
//						}

//						DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
						dbObject = dbOp.dbCreateStatement(dbObject);
						dbObject = dbOp.dbExecute(dbObject, facility_info_sql);
						ResultSet rs = dbObject.getResultSet();
						ResultSetMetaData metadata = rs.getMetaData();

						JSONObject facilityInfo = new JSONObject();
						JSONArray facility_array = new JSONArray();
						while (rs.next()) {
							facilityInfo = new JSONObject();
							facilityInfo.put(metadata.getColumnName(1), rs.getString("facilityid"));
							facilityInfo.put(metadata.getColumnName(2), rs.getString("facility_type"));
							facilityInfo.put(metadata.getColumnName(3), rs.getString("lat"));
							facilityInfo.put(metadata.getColumnName(4), rs.getString("lon"));
							facilityInfo.put(metadata.getColumnName(5), rs.getString("Facility_Name"));
							facilityInfo.put(metadata.getColumnName(6), rs.getString("facility_category"));
							facilityInfo.put(metadata.getColumnName(7), rs.getString("last_update_date"));

							facility_array.put(facilityInfo);
						}

						info.put("Facility_info", facility_array);

						System.out.println(facility_array);

						dbOp.closeStatement(dbObject);
						dbOp.closeConn(dbObject);
						dbObject = null;
					}
				}
				// Get facility information of non implemented zilla
				else {
					if (upazila > 0) {
						facility_info_sql = "select * from " +
								"dblink('host=103.48.19.7 port=5412  dbname=facility_assessment user=rhis_admin password=rhis12#rhis', $$ " +
								"select facility_id as facilityid, facility_type, lat, lon, facility_name as \"Facility_Name\", (case when last>199 then 'A' when last between 100 and 199 then 'B' else 'C' end) facility_category, last_created_at::date as last_update_date " +
								"from facility_registry join " +
								"(with CTE as (select * " +
								",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at asc ) ascrnk " +
								",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at desc ) desrnk " +
								"from (SELECT facility_id, created_at, sum(value) AS max_value " +
								"FROM questions_options_mapping GROUP BY facility_id, created_at) TestTable) " +
								"select t1.facility_id, t2.max_value as last, t2.created_at as last_created_at from( select * from cte where ascrnk = 1) t1 " +
								"left join ( select * from cte where desrnk = 1) t2 on t1.facility_id = t2.facility_id ) s on s.facility_id=facility_registry.facilityid " +
								"where zillaid::integer=" + zilla + " and upazilaid::integer=" + upazila + " and lat is not null and lon is not null $$) as facility_info " +
								"(facilityid integer, facility_type text, lat text, lon text, \"Facility_Name\" text, facility_category text, last_update_date date)";
					} else {
						facility_info_sql = "select * from " +
								"dblink('host=103.48.19.7 port=5412 dbname=facility_assessment user=rhis_admin password=rhis12#rhis', $$ " +
								"select facility_id as facilityid, facility_type, lat, lon, facility_name as \"Facility_Name\", (case when last>199 then 'A' when last between 100 and 199 then 'B' else 'C' end) facility_category, last_created_at::date as last_update_date " +
								"from facility_registry join " +
								"(with CTE as (select * " +
								",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at asc ) ascrnk " +
								",ROW_NUMBER() over (partition by facility_id order by facility_id,created_at desc ) desrnk " +
								"from (SELECT facility_id, created_at, sum(value) AS max_value " +
								"FROM questions_options_mapping GROUP BY facility_id, created_at) TestTable) " +
								"select t1.facility_id, t2.max_value as last, t2.created_at as last_created_at from( select * from cte where ascrnk = 1) t1 " +
								"left join ( select * from cte where desrnk = 1) t2 on t1.facility_id = t2.facility_id ) s on s.facility_id=facility_registry.facilityid " +
								"where zillaid::integer=" + zilla + " and lat is not null and lon is not null $$) as facility_info " +
								"(facilityid integer, facility_type text, lat text, lon text, \"Facility_Name\" text, facility_category text, last_update_date date)";
					}
					System.out.println(facility_info_sql);


					dbObject = new FacilityDB().centralMonitorngInfo();
					dbObject = dbOp.dbCreateStatement(dbObject);
					dbObject = dbOp.dbExecute(dbObject, facility_info_sql);
					ResultSet rs = dbObject.getResultSet();
					ResultSetMetaData metadata = rs.getMetaData();

					JSONObject facilityInfo = new JSONObject();
					JSONArray facility_array = new JSONArray();
					while (rs.next()) {
						facilityInfo = new JSONObject();
						facilityInfo.put(metadata.getColumnName(1), rs.getString("facilityid"));
						facilityInfo.put(metadata.getColumnName(2), rs.getString("facility_type"));
						facilityInfo.put(metadata.getColumnName(3), rs.getString("lat"));
						facilityInfo.put(metadata.getColumnName(4), rs.getString("lon"));
						facilityInfo.put(metadata.getColumnName(5), rs.getString("Facility_Name"));
						facilityInfo.put(metadata.getColumnName(6), rs.getString("facility_category"));
						facilityInfo.put(metadata.getColumnName(7), rs.getString("last_update_date"));

						facility_array.put(facilityInfo);
					}

					info.put("Facility_info", facility_array);

					System.out.println(facility_array);

					dbOp.closeStatement(dbObject);
					dbOp.closeConn(dbObject);
					dbObject = null;
					
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println(e);
		}
		finally {
            table_schema = null;
            setZillaList = null;
		}
		
		return info;
	}
}
