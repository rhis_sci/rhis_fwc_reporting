package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.EnglishtoBangla;

import java.sql.ResultSet;
//import java.time.YearMonth;
import java.sql.SQLException;
import java.util.*;

public class MIS3Report {



	//Coded By Evana: 23/01/2019

	//Query for GP child
	public static List <Object> getChildQuery(String start_date, String end_date) {
		String query_child = "";
		boolean query_from_gpservice = false;
		List ret_array= new ArrayList<>();

		DBSchemaInfo table_schema = new DBSchemaInfo();
		String[] date = start_date.split("-");
		int month = Integer.parseInt(date[1]);
		int year = Integer.parseInt(date[0]);
		if(month <=9 && year <=2020) {
			query_child += "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer < 1 )" + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_0_1_center , "
					+ "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer < 1 )" + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_0_1_satelite , "
					+ "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer >= 1 and date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer <= 5 )" + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_1_5_center , "
					+ "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer >= 1 and date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer <= 5 )" + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_1_5_satelite , ";
            query_from_gpservice = true;
		}else{
			query_child += "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer < 1 )" + " AND " + table_schema.getColumn("table", "child_care_service_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_0_1_center , "
					+ "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer < 1 )" + " AND " + table_schema.getColumn("table", "child_care_service_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_0_1_satelite , "
					+ "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer >= 1 and date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer <= 5 )" + " AND " + table_schema.getColumn("table", "child_care_service_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_1_5_center , "
					+ "COUNT(CASE WHEN((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer >= 1 and date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer <= 5 )" + " AND " + table_schema.getColumn("table", "child_care_service_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_outdoor_child_1_5_satelite , ";


		}

		ret_array.add(query_from_gpservice);
		ret_array.add(query_child);
		return (ret_array);
	}

	public static String getQuery(String end_date)
	{
		String query_adolescence = "";

		DBSchemaInfo table_schema = new DBSchemaInfo();

		query_adolescence += "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"6\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_change_council_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"6\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_change_council_satelite, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"7\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_child_marraige_council_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"7\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_child_marraige_council_satelite, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"5\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_desease_council_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"5\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_desease_council_satelite, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"8\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_food_council_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"8\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_food_council_satelite, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"9\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_fighting_council_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"9\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_fighting_council_satelite, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"10\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_mental_council_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"10\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_mental_council_satelite, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"11\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_sanitary_council_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_advice") + " like '%\"11\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_adolescent_sanitary_council_satelite, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_treatment") + " like '%\"53\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_adolescence_folicacide_supply_center, "
				+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_treatment") + " like '%\"53\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_adolescence_folicacide_supply_satelite, ";

		return query_adolescence;
	}

	public static String getResultSet(JSONObject reportCred){

		String zilla;
		String upazila;
		String union;
		String providerId;
		String supervisorId;
		String facilityId;
		String facilityName = null;
		String resultString;
		String sql_zilla;
		String sql_upazila;
		String sql_union;
		String sql_provider;
		String sql_injectable;
		String sql_iud;
		String sql_iud_followup;
		String sql_implant;
		String sql_implant_followup;
		String sql_pill_comdom;
		String sql_anc;
		String sql_new_anc;
		String sql_pac;
		String sql_newborn_refer;
		String sql_newborn;
		String sql_child;
		String sql_mother;
		String sql_delivery;
		String sql_maternalDeath;
		String sql_gp;
		String sql_child_care_service;
		String sql_child_care_service_detail;
		String date_type;
		String monthSelect = null;
		String yearSelect;
		String start_date;
		String end_date;
		String bangla_month = null;
		String bangla_year = null;
		String db_name;
		String query_condition;
		String csba;
		String mis3ViewType;
		String mis3Submit;
		String jsonRequest;
		String dhis2;
		String uType;
		String uCode;
		String mis3MonitoringTool;
		String query_adolescence;
		String query_child;
		String query_pac_condition;
		String query_pac_condition_satelite;

		String sql_permanent_method;
		String sql_permanent_method_followup;

		int referYes = 1;
		int newborn = 28;

		int normalDelivery = 1;
		int csectionDelivery = 2;
		int applyOxytocinYes = 1;
		int applyTractionYes = 1;
		int applyUterusMassageYes = 1;
		int deliveryDoneByThisProviderYes = 1;

		Map<String, String> zilla_info = new HashMap<String, String>();
		Map<String, String> upazila_info = new HashMap<String, String>();
		Map<String, String> union_info = new HashMap<String, String>();
		Map<String, String> provider_info = new HashMap<String, String>();
		Map<String, String> injectable = new HashMap<String, String>();
		Map<String, String> iud = new HashMap<String, String>();
		Map<String, String> iud_followup = new HashMap<String, String>();
		Map<String, String> implant = new HashMap<String, String>();
		Map<String, String> implant_followup = new HashMap<String, String>();
		Map<String, String> pill_condom = new HashMap<String, String>();
		Map<String, String> anc = new HashMap<String, String>();
		Map<String, String> pms = new HashMap<String, String>();
		Map<String, String> pms_followup = new HashMap<String, String>();
		Map<String, String> pac = new HashMap<String, String>();
		Map<String, String> pnc_newborn_refer = new HashMap<String, String>();
		Map<String, String> newborn_info = new HashMap<String, String>();
		Map<String, String> pnc_child = new HashMap<String, String>();
		Map<String, String> pnc_mother = new HashMap<String, String>();
		Map<String, String> delivery = new HashMap<String, String>();
		Map<String, String> maternalDeath = new HashMap<String, String>();
		Map<String, String> gp = new HashMap<String, String>();
		Map<String, String> ccs = new HashMap<String, String>();
		Map<String, String> ccsd = new HashMap<String, String>();
		LinkedHashMap<String, LinkedHashMap<String, String>> lmis = new LinkedHashMap<String, LinkedHashMap<String, String>>();

		Integer DateOption = 0;

		JSONObject mis3_json = new JSONObject();
		JSONObject new_mis3_json = new JSONObject();
		JSONObject mis3_submit_data = new JSONObject();
		int rowCount_submission = 0;
		resultString = "";
		try{
			if(reportCred.has("mis3MonitoringTool"))
				mis3MonitoringTool = reportCred.getString("mis3MonitoringTool");
			else
				mis3MonitoringTool = "0";

			if(reportCred.has("report1_zilla"))
				zilla = reportCred.getString("report1_zilla");
			else
				zilla = "";

			if(reportCred.has("report1_upazila"))
				upazila = reportCred.getString("report1_upazila");
			else
				upazila = "";
			//upazila = reportCred.getString("report1_upazila");

			DBSchemaInfo table_schema = null;
			if(zilla.equals("36") && upazila.equals("71"))
				table_schema = new DBSchemaInfo();
			else
				table_schema = new DBSchemaInfo(zilla);

			if(reportCred.has("report1_union"))
				union = reportCred.getString("report1_union");
			else
				union = "";
			//union = reportCred.getString("report1_union");
            if(reportCred.has("report1_dateType")) {
                date_type = reportCred.getString("report1_dateType");
            }else{
                date_type = "0";
            }
			if(reportCred.has("providerId"))
				providerId = reportCred.getString("providerId");
			else
				providerId = "";
			if(reportCred.has("providerId"))
				providerId = reportCred.getString("providerId");
			else
				providerId = "";
			//providerId = reportCred.getString("providerId");
			if(reportCred.has("supervisorId"))
				supervisorId = reportCred.getString("supervisorId");
			else
				supervisorId = "";
			if(reportCred.has("csba"))
				csba = reportCred.getString("csba");
			else
				csba = "2";
			//csba = reportCred.getString("csba");
			mis3ViewType = reportCred.getString("mis3ViewType");

			if(reportCred.has("mis3Submit"))
				mis3Submit = reportCred.getString("mis3Submit");
			else
				mis3Submit = "0";

			if(reportCred.has("jsonRequest"))
				jsonRequest = reportCred.getString("jsonRequest");
			else
				jsonRequest = "0";


			if(reportCred.has("utype"))
				uType = reportCred.getString("utype");
			else
				uType = "";

			if(reportCred.has("ucode"))
				uCode = reportCred.getString("ucode");
			else
				uCode = "";

			if(reportCred.has("dhis2"))
				dhis2 = reportCred.getString("dhis2");
			else
				dhis2 = "";

			//facilityName = reportCred.getString("facilityName");

			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			//System.out.println(db_name);
			String dhis2_month = "";
			String dhis2_year = "";

			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");

				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();

				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-" + daysInMonth;

				bangla_month = EnglishtoBangla.ConvertMonth(date[1]);
				bangla_year = EnglishtoBangla.ConvertNumberToBangla(date[0]);

				dhis2_month = date[1];
				dhis2_year = date[0];
				DateOption = Integer.parseInt(dhis2_year);
			}
			else if(date_type.equals("3"))
			{
				yearSelect = reportCred.getString("report1_year");
				start_date = yearSelect + "-01-01";
				end_date = yearSelect + "-12-31";

				String[] edate = end_date.split("-");
				System.out.println("Edate for Testing = " + edate[0]);
				DateOption = Integer.parseInt(edate[0]);
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");

				String[] date = end_date.split("-");
				bangla_month = EnglishtoBangla.ConvertMonth(date[1]);
				bangla_year = EnglishtoBangla.ConvertNumberToBangla(date[0]);

				DateOption = Integer.parseInt(date[0]);
			}

			System.out.println("Before DateOption=" + DateOption);

			String providerType = "";
			String db_csba = "2";
			Integer provider_count = 0;

			//if(!providerId.equals("") && (zilla.equals("") || zilla.equals("null") || upazila.equals("") || upazila.equals("null") || union.equals("") || union.equals("null")))
			if(!supervisorId.equals("") && !uType.equals("999") && !uType.equals("998") && !uType.equals("994"))
			{
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo();

				sql_provider = "select * from "+ table_schema.getTable("table_providerdb") +" where "+table_schema.getColumn("", "providerdb_provcode",new String[]{supervisorId},"=");
				//System.out.println(sql_provider);
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,sql_provider);
				ResultSet rs = dbObject.getResultSet();

				while(rs.next()){
					zilla = rs.getString("zillaid");
				}

				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
				dbObject = null;

				//System.out.println(zilla);
			}
			else if(!providerId.equals("") && mis3Submit.equals("0"))
			{
				provider_count++;

				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));
				sql_provider = "select * from "+ table_schema.getTable("table_providerdb") +" where "+table_schema.getColumn("", "providerdb_active",new String[]{"1"},"=")+" and "+table_schema.getColumn("", "providerdb_provcode",new String[]{providerId},"=");
				System.out.println(sql_provider);
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,sql_provider);
				ResultSet rs = dbObject.getResultSet();

				while(rs.next()){
					zilla = rs.getString("zillaid");
					upazila = rs.getString("upazilaid");
					union = rs.getString("unionid");
					providerType = rs.getString("provtype");
					db_csba = rs.getString("csba");
					System.out.println("Provider Type: "+rs.getString("provtype"));
					//
//					if(rs.getString("csba") != null && !rs.getString("csba").isEmpty())
//						csba = rs.getString("csba");
//					else
//						csba = "2";

					if(rs.getString("csba") != null && !rs.getString("csba").isEmpty())
						db_csba = rs.getString("csba");
					else
						db_csba = "2";
					provider_count++;
				}

				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
				dbObject = null;

				//System.out.println(zilla);
			}

			String associated_facility_id = "";

			if((mis3Submit.equals("1") && (reportCred.getString("facilityId").equals("") || reportCred.getString("facilityId").equals("null"))) || provider_count==1){
				String facility_info = "select "+table_schema.getColumn("","facility_provider_facility_id")+" as facility_id from "+table_schema.getTable("table_facility_provider")+" where "+table_schema.getColumn("","facility_provider_provider_id",new String[]{providerId},"=")+" and "+table_schema.getColumn("", "facility_provider_is_active",new String[]{"1"},"=");
				System.out.println(facility_info);
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,facility_info);
				ResultSet rs = dbObject.getResultSet();

				while(rs.next()){
					if(provider_count==1)
						associated_facility_id = rs.getString("facility_id");
					else
						reportCred.put("facilityId", rs.getString("facility_id"));
				}
			}

			if(reportCred.has("facilityId") || mis3Submit.equals("1") || !associated_facility_id.equals("")){
				String facility_info = "";
				if(!associated_facility_id.equals(""))
					facility_info = "select "+table_schema.getColumn("","facility_registry_facility_name")+" as facility_name from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_facilityid",new String[]{associated_facility_id},"=");
				else
					facility_info = "select "+table_schema.getColumn("","facility_registry_facility_name")+" as facility_name from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_facilityid",new String[]{reportCred.getString("facilityId")},"=");
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,facility_info);
				ResultSet rs = dbObject.getResultSet();

				while(rs.next()){
					facilityName = rs.getString("facility_name");
				}
			}
			else{
				String facility_info = "select "+table_schema.getColumn("","providerdb_facilityname")+" as facility_name from "+table_schema.getTable("table_providerdb")+" where " + table_schema.getColumn("","providerdb_provcode",new String[]{providerId},"=") + " and " + table_schema.getColumn("","providerdb_active",new String[]{"1"},"=");
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,facility_info);
				ResultSet rs = dbObject.getResultSet();

				while(rs.next()){
					facilityName = rs.getString("facility_name");
				}
			}

			if(reportCred.has("facilityId"))
				facilityId = reportCred.getString("facilityId");
			else
				facilityId = "";

			String associated_id = "";
			if(providerId.equals("") || mis3Submit.equals("1")){
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));

				sql_provider = "select "+table_schema.getColumn("table", "facility_provider_provider_id")+" as provider_id,"+table_schema.getColumn("table", "providerdb_zillaid")+","+table_schema.getColumn("table", "providerdb_upazilaid")+","+table_schema.getColumn("table", "providerdb_unionid")+", " +
						table_schema.getColumn("table", "facility_registry_zillaid")+" as fr_zillaid,"+table_schema.getColumn("table", "facility_registry_upazilaid")+" as fr_upazilaid,"+table_schema.getColumn("table", "providerdb_unionid")+" as fr_unionid " +
						"from "+ table_schema.getTable("table_facility_provider") +" " +
						"inner join " + table_schema.getTable("table_facility_registry") +" on "+table_schema.getColumn("table", "facility_provider_facility_id")+ "="+table_schema.getColumn("table", "facility_registry_facilityid")+ " " +
						"left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "facility_provider_provider_id")+"  and ("+table_schema.getColumn("", "providerdb_endate",new String[]{start_date},">=")+" or "+table_schema.getColumn("", "providerdb_active",new String[]{"1"},"=")+") " +
						"where "+table_schema.getColumn("", "facility_provider_facility_id",new String[]{facilityId},"=")+" " +
						"and ("+ table_schema.getColumn("", "facility_provider_end_date",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "facility_provider_is_active",new String[]{"1"},"=")+")";
				System.out.println(sql_provider);
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,sql_provider);
				ResultSet rs = dbObject.getResultSet();
				rs.last();
				int rowCount = rs.getRow();
				rs.beforeFirst();
				String facility_provider = "";
				int r = 0;
				while(rs.next()){
					if(rs.getString("zillaid")!=null && rs.getString("upazilaid")!=null)
						facility_provider += rs.getString("provider_id")+",";
					else
						associated_id += rs.getString("provider_id")+",";


					if(r<1){
						zilla = rs.getString("fr_zillaid");
						upazila = rs.getString("fr_upazilaid");
						union = rs.getString("fr_unionid");
						r++;
					}
				}

				System.out.println(facility_provider + " "+associated_id);

				if(facility_provider.endsWith(","))
					facility_provider = facility_provider.substring(0,facility_provider.length() - 1);

				if(associated_id.endsWith(","))
					associated_id = associated_id.substring(0,associated_id.length() - 1);

				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
				dbObject = null;

				if(facility_provider.isEmpty())
					query_condition = ""+table_schema.getColumn("table", "providerdb_provcode")+" in (0)"+" AND ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_active",new String[]{"1"},"=")+")";
				else
					query_condition = ""+table_schema.getColumn("table", "providerdb_provcode")+" in ("+facility_provider+")"+" AND ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_active",new String[]{"1"},"=")+")";
				//System.out.println(query_condition);
			}
			else
				query_condition = ""+table_schema.getColumn("table", "providerdb_provcode",new String[]{providerId},"=")+" AND ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_active",new String[]{"1"},"=")+")";

			/***
			 * @Author: Evana
			 * @Date: 13/06/2019
			 * @Description: Query Condition for satelitecentername
			 * **/


			if(associated_id.isEmpty() && provider_count == 1)
				associated_id = reportCred.getString("providerId");

			int count_report_submit = 0;
			String submission_status = "";
			String submission_date = "";
			String comment = "";
			if(!supervisorId.equals("") || uType.equals("999") || uType.equals("998") || uType.equals("994") || mis3Submit.equals("1")) {
				String[] date = start_date.split("-");
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));

				String sql_check_mis3_submission = "select "+table_schema.getColumn("", "mis3_report_submission_submission_status")+" as submission_status, "+table_schema.getColumn("", "mis3_report_submission_submission_date")+" as submission_date, "+table_schema.getColumn("", "mis3_report_submission_submission_status")+" as submission_status, "+table_schema.getColumn("", "mis3_report_submission_supervisor_comment")+" as supervisor_comment from "+ table_schema.getTable("table_mis3_report_submission") +" where "+table_schema.getColumn("", "mis3_report_submission_facility_id",new String[]{facilityId},"=")+" " +
						"and "+table_schema.getColumn("", "mis3_report_submission_report_year",new String[]{date[0]},"=")+" and "+table_schema.getColumn("", "mis3_report_submission_report_month",new String[]{date[1]},"=");
				System.out.println(sql_check_mis3_submission);
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,sql_check_mis3_submission);
				ResultSet rs = dbObject.getResultSet();
				rs.last();
				count_report_submit = rs.getRow();
				rs.beforeFirst();
				while(rs.next()) {
					comment = ((rs.getString("supervisor_comment")!=null)?rs.getString("supervisor_comment"):"");
					submission_status = rs.getString("submission_status");
					submission_date = rs.getString("submission_date");
				}
				//comment = (!comment.equals("null")?comment:"");
				System.out.println(comment);
				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
				dbObject = null;

				//query_condition += " and \"systemEntryDate\"<'"+submission_date+"' ";
			}

			if(!csba.equals("1")) {
				sql_zilla = "select "+table_schema.getColumn("", "zilla_zillaname")+" as zilla_name from " + table_schema.getTable("table_zilla") + " where " + table_schema.getColumn("", "zilla_zillaid", new String[]{reportCred.getString("report1_zilla")}, "=");
				System.out.println(sql_zilla);
				zilla_info = GetMIS3ResultSet.getFinalResult(sql_zilla, "getString", zilla, upazila);

				sql_upazila = "select "+table_schema.getColumn("", "upazila_upazilaname")+" as upazila_name from " + table_schema.getTable("table_upazila") + " where " + table_schema.getColumn("", "upazila_zillaid", new String[]{reportCred.getString("report1_zilla")}, "=") + " and " + table_schema.getColumn("", "upazila_upazilaid", new String[]{reportCred.has("report1_upazila")?reportCred.getString("report1_upazila"):upazila}, "=");
				upazila_info = GetMIS3ResultSet.getFinalResult(sql_upazila, "getString", zilla, upazila);

				sql_union = "select "+table_schema.getColumn("", "unions_unionname")+" as union_name from " + table_schema.getTable("table_unions") + " where " + table_schema.getColumn("", "unions_zillaid", new String[]{reportCred.getString("report1_zilla")}, "=") + " and " + table_schema.getColumn("", "unions_upazilaid", new String[]{reportCred.has("report1_upazila")?reportCred.getString("report1_upazila"):upazila}, "=") + " and " + table_schema.getColumn("", "unions_unionid", new String[]{reportCred.has("report1_union")?reportCred.getString("report1_union"):union}, "=");
				union_info = GetMIS3ResultSet.getFinalResult(sql_union, "getString", zilla, upazila);
				System.out.println(sql_union);
			}

			if((!providerType.equals("6")) || providerId.equals("")) {
				if(!csba.equals("1")) {
					sql_pill_comdom = "SELECT ";
					if(DateOption>2018){
						sql_pill_comdom += "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)  AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as pill_new_center, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)  AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as pill_new_satelite, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)  AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as pill_old_center, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)   AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as pill_old_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)   AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as condom_new_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)   AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as condom_new_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)   AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as condom_old_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)   AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as condom_old_satelite, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as pill_ppfp_new_center, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as pill_ppfp_new_satelite, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as pill_ppfp_old_center, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as pill_ppfp_old_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as condom_ppfp_new_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as condom_ppfp_new_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as condom_ppfp_old_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" OR fpinfo.lastdeliverydate is not null) AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as condom_ppfp_old_satelite, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as pill_ppfp_expire_center, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as pill_ppfp_expire_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as condom_ppfp_expire_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs2", "pillcondomservice_visitdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as condom_ppfp_expire_satelite, ";
					}
					else {
						sql_pill_comdom += "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as pill_new_center, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as pill_new_satelite, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as pill_old_center, "
								+ "COUNT(CASE WHEN ((" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " OR " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + ") AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as pill_old_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as condom_new_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as condom_new_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as condom_old_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_isnewclient", new String[]{"1"}, "!=") + " AND " + table_schema.getColumn("pcs1", "pillcondomservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as condom_old_satelite, ";
					}
					sql_pill_comdom += "SUM(CASE WHEN " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"1"}, "=") + " THEN " + table_schema.getColumn("pcs1", "pillcondomservice_amount") + " ELSE 0 END) as pill_shukhi_total, "
							+ "SUM(CASE WHEN " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"10"}, "=") + " THEN " + table_schema.getColumn("pcs1", "pillcondomservice_amount") + " ELSE 0 END) as pill_apon_total, "
							+ "SUM(CASE WHEN " + table_schema.getColumn("pcs1", "pillcondomservice_methodtype", new String[]{"2"}, "=") + " THEN " + table_schema.getColumn("pcs1", "pillcondomservice_amount") + " ELSE 0 END) as condom_total "
							+ "FROM " + table_schema.getTable("table_pillcondomservice") + " pcs1 ";

					//Added new condition for ppfp if data is exist in fpinfo but not in delivery
					sql_pill_comdom += "left join fpinfo on fpinfo.healthid = pcs1.healthid and fpinfo.lastdeliverydate + interval '1 years' > pcs1.visitdate ";

					if(DateOption>2018) {
						//added for max outcome date in delivery
						String sqlCondition_max_outcomedate = " and " + table_schema.getColumn("table", "delivery_outcomedate") + "= (select max(outcomedate) from" + table_schema.getTable("table_delivery")+ "where healthid = pcs1.healthid) ";
						sql_pill_comdom += "left join " + table_schema.getTable("table_delivery") + " on " + table_schema.getColumn("table", "delivery_healthid") + "=" + table_schema.getColumn("pcs1", "pillcondomservice_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'>" + table_schema.getColumn("pcs1", "pillcondomservice_visitdate")+" " +
								sqlCondition_max_outcomedate +
								"left join (select " + table_schema.getColumn("", "pillcondomservice_methodtype") + ", " + table_schema.getColumn("", "pillcondomservice_visitdate") + ", " + table_schema.getColumn("", "pillcondomservice_healthid") + " FROM " + table_schema.getTable("table_pillcondomservice") + " " +
								"where " + table_schema.getColumn("", "pillcondomservice_visitdate") + "< '" + start_date + "' limit 1) pcs2 on "+ table_schema.getColumn("pcs2", "pillcondomservice_healthid") + "=" + table_schema.getColumn("pcs1", "pillcondomservice_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'>" + table_schema.getColumn("pcs2", "pillcondomservice_visitdate")+" and "+table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'<" + table_schema.getColumn("pcs1", "pillcondomservice_visitdate")+ " ";
					}


					sql_pill_comdom += "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("pcs1", "pillcondomservice_providerid") + " "
							+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
							+ "where " + query_condition + " "
							+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("pcs1", "pillcondomservice_providerid") + " "
							+ "where (case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("pcs1", "pillcondomservice_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("pcs1", "pillcondomservice_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("pcs1", "pillcondomservice_visitdate", new String[]{start_date, end_date}, "between") + " end) "
							+ "and ";
					if(!associated_id.isEmpty())
						sql_pill_comdom += "((" + query_condition +") or " + table_schema.getColumn("pcs1", "pillcondomservice_providerid", new String[]{associated_id}, "in") + ") ";
					else
						sql_pill_comdom += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_pill_comdom += "and " + table_schema.getColumn("pcs1", "pillcondomservice_systementrydate", new String[]{submission_date}, "<") + " ";
					sql_pill_comdom += "GROUP BY " + table_schema.getColumn("pcs1", "pillcondomservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + " ";
					System.out.println(sql_pill_comdom);
					pill_condom = GetMIS3ResultSet.getFinalResult(sql_pill_comdom, "notString", zilla, upazila);
					//System.out.println(pill_condom);

					//			sql_injectable = "SELECT COUNT(CASE WHEN (\"doseId\"=1 AND \"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) as new_inject_center, "
					//					+ "COUNT(CASE WHEN (\"doseId\"=1 AND \"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) as new_inject_satelite, "
					//					+ "COUNT(CASE WHEN (\"doseId\">1 AND \"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) as old_inject_center, "
					//					+ "COUNT(CASE WHEN (\"doseId\">1 AND \"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) as old_inject_satelite, "
					//					+ "COUNT(CASE WHEN (\"sideEffect\" IS NOT NULL AND \"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) as \"sideEffect_inj_center\", "
					//					+ "COUNT(CASE WHEN (\"sideEffect\" IS NOT NULL AND \"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) as \"sideEffect_inj_satelite\" "
					//					+ "FROM \"womanInjectable\""
					//					+ "WHERE \"providerId\" IN "
					//					+ "       (select "+table_schema.getColumn("","providerdb_provcode")+" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+" AND unionid = "+union+" AND \"ProvType\"=4) "
					//					+ "AND \"doseDate\" BETWEEN ('" + start_date + "') AND ('" + end_date +"') "
					//					+ "GROUP BY \"providerId\"";

					sql_injectable = "SELECT ";
					if(DateOption>2018) {
						sql_injectable += "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + " AND fpinfo.lastdeliverydate is null)  AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as new_inject_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + "  AND fpinfo.lastdeliverydate is null)  AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as new_inject_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + "  AND fpinfo.lastdeliverydate is null)  AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as old_inject_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnull") + "  AND fpinfo.lastdeliverydate is null)  AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as old_inject_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") + " OR fpinfo.lastdeliverydate is not null ) AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as new_inject_ppfp_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") + " OR fpinfo.lastdeliverydate is not null ) AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as new_inject_ppfp_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") + " OR fpinfo.lastdeliverydate is not null ) AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as old_inject_ppfp_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_isnewclient", new String[]{"1"}, "!=") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") + " OR fpinfo.lastdeliverydate is not null ) AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as old_inject_ppfp_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("winj", "womaninjectable_dosedate", new String[]{}, "isnotnull") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") + " OR fpinfo.lastdeliverydate is not null ) AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as inject_ppfp_expire_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("winj", "womaninjectable_dosedate", new String[]{}, "isnotnull") + " AND (" + table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") + " OR fpinfo.lastdeliverydate is not null ) AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as inject_ppfp_expire_satelite, ";
					}
					else{
						sql_injectable += "COUNT(CASE WHEN (" + table_schema.getColumn("", "womaninjectable_isnewclient", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as new_inject_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "womaninjectable_isnewclient", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as new_inject_satelite, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "womaninjectable_isnewclient", new String[]{"1"}, "!=") + " AND " + table_schema.getColumn("", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as old_inject_center, "
								+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "womaninjectable_isnewclient", new String[]{"1"}, "!=") + " AND " + table_schema.getColumn("", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as old_inject_satelite, ";
					}
					sql_injectable += "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_sideeffect", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as \"sideEffect_inj_center\", "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "womaninjectable_sideeffect", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("table", "womaninjectable_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as \"sideEffect_inj_satelite\" "
							+ "FROM " + table_schema.getTable("table_womaninjectable") + " ";

                    //Added new condition for ppfp if data is exist in fpinfo but not in delivery
                    sql_injectable += "left join fpinfo on fpinfo.healthid = womaninjectable.healthid and fpinfo.lastdeliverydate + interval '1 years' > womaninjectable.dosedate ";

                    if(DateOption>2018) {
						sql_injectable += "left join " + table_schema.getTable("table_delivery") + " on " + table_schema.getColumn("table", "delivery_healthid") + "=" + table_schema.getColumn("table", "womaninjectable_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'>" + table_schema.getColumn("table", "womaninjectable_dosedate")+" "+
								"left join (select " + table_schema.getColumn("", "womaninjectable_dosedate") + ", " + table_schema.getColumn("", "womaninjectable_healthid") + " FROM " + table_schema.getTable("table_womaninjectable") + " " +
								"where " + table_schema.getColumn("", "womaninjectable_dosedate") + "< '" + start_date + "' limit 1) winj on "+ table_schema.getColumn("winj", "womaninjectable_healthid") + "=" + table_schema.getColumn("winj", "womaninjectable_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'<" + table_schema.getColumn("winj", "womaninjectable_dosedate")+" and "+table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'<" + table_schema.getColumn("table", "womaninjectable_dosedate")+ " ";
					}



                    sql_injectable += "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "womaninjectable_providerid") + " "
							+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
							+ "where " + query_condition + " "
							+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "womaninjectable_providerid") + " "
							+ "where (case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("table", "womaninjectable_dosedate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("table", "womaninjectable_dosedate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("table", "womaninjectable_dosedate", new String[]{start_date, end_date}, "between") + " end) "
							+ "and ";
					if(!associated_id.isEmpty())
						sql_injectable += "((" + query_condition +") or " + table_schema.getColumn("table", "womaninjectable_providerid", new String[]{associated_id}, "in") + ") ";
					else
						sql_injectable += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_injectable += "and " + table_schema.getColumn("table", "womaninjectable_systementrydate", new String[]{submission_date}, "<") + " ";
					sql_injectable += "GROUP BY " + table_schema.getColumn("table", "womaninjectable_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + " ";

					System.out.println(sql_injectable);
					injectable = GetMIS3ResultSet.getFinalResult(sql_injectable, "notString", zilla, upazila);

					sql_iud = "SELECT COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudafterdelivery", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("iuds", "iudservice_iudimplantdate", new String[]{start_date, end_date}, "between") + " AND " + table_schema.getColumn("iuds", "iudservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as iud_after_delivery_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudafterdelivery", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("iuds", "iudservice_iudimplantdate", new String[]{start_date, end_date}, "between") + " AND " + table_schema.getColumn("iuds", "iudservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as iud_after_delivery_satelite, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudafterdelivery", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("iuds", "iudservice_iudimplantdate", new String[]{start_date, end_date}, "between") + " AND " + table_schema.getColumn("iuds", "iudservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as iud_normal_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudafterdelivery", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("iuds", "iudservice_iudimplantdate", new String[]{start_date, end_date}, "between") + " AND " + table_schema.getColumn("iuds", "iudservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as iud_normal_satelite, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudimplantdate") + "+interval '5 years'>" + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate") + " AND " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("ifs", "iudservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as before_time_finish_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudimplantdate") + "+interval '5 years'>" + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate") + " AND " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("ifs", "iudservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as before_time_finish_satelite, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudimplantdate") + "+interval '5 years'<=" + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate") + " AND " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("ifs", "iudservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as after_time_finish_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("iuds", "iudservice_iudimplantdate") + "+interval '5 years'<=" + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate") + " AND " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("ifs", "iudservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as after_time_finish_satelite ";
					if(DateOption>2018){
						sql_iud += ",COUNT(CASE WHEN (" + table_schema.getColumn("iud2", "iudservice_iudimplantdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("iuds", "iudservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as iud_ppfp_expire_center, " +
								"COUNT(CASE WHEN (" + table_schema.getColumn("iud2", "iudservice_iudimplantdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("iuds", "iudservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as iud_ppfp_expire_satelite ";
					}
					sql_iud += "FROM " + table_schema.getTable("table_iudservice") + " as iuds left join " + table_schema.getTable("table_iudfollowupservice") + " as ifs on " + table_schema.getColumn("iuds", "iudservice_healthid") + "=" + table_schema.getColumn("ifs", "iudfollowupservice_healthid") + " and " + table_schema.getColumn("iuds", "iudservice_iudcount") + "=" + table_schema.getColumn("ifs", "iudservice_iudcount") + " and " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate", new String[]{}, "isnotnull") + " ";

                    //Added new condition for ppfp if data is exist in fpinfo but not in delivery
                    sql_iud += "left join fpinfo on fpinfo.healthid = iuds.healthid and fpinfo.lastdeliverydate + interval '1 years' > iuds.iudimplantdate ";

                    if(DateOption>2018) {
						sql_iud += "left join " + table_schema.getTable("table_delivery") + " on " + table_schema.getColumn("table", "delivery_healthid") + "=" + table_schema.getColumn("iuds", "iudservice_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'>" + table_schema.getColumn("iuds", "iudservice_iudimplantdate")+" "+
								"left join (select " + table_schema.getColumn("", "iudservice_iudimplantdate") + ", " + table_schema.getColumn("", "iudservice_healthid") + " FROM " + table_schema.getTable("table_iudservice") + " " +
								"where " + table_schema.getColumn("", "iudservice_iudimplantdate") + "< '" + start_date + "' limit 1) iud2 on "+ table_schema.getColumn("iud2", "iudservice_healthid") + "=" + table_schema.getColumn("iud2", "iudservice_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'<" + table_schema.getColumn("iud2", "iudservice_iudimplantdate")+" and "+table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'<" + table_schema.getColumn("iuds", "iudservice_iudimplantdate")+ " ";
					}
					sql_iud += "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("iuds", "iudservice_providerid") + " or " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("ifs", "iudfollowupservice_providerid") + " "
							+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
							+ "where " + query_condition
							+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " " //+ table_schema.getColumn("iuds", "iudservice_providerid") + " or " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("ifs", "iudfollowupservice_providerid") + " "
							+ "where ((case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("iuds", "iudservice_iudimplantdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("iuds", "iudservice_iudimplantdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("iuds", "iudservice_iudimplantdate", new String[]{start_date, end_date}, "between") + " end) or "
							+ "(case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("ifs", "iudfollowupservice_iudremovedate", new String[]{start_date, end_date}, "between") + " end)) "
							+ "and ";
					if(!associated_id.isEmpty())
						sql_iud += "((" + query_condition +") or " + table_schema.getColumn("iuds", "iudservice_providerid", new String[]{associated_id}, "in") + ") ";
					else
						sql_iud += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_iud += "and " + table_schema.getColumn("iuds", "iudservice_systementrydate", new String[]{submission_date}, "<") + " ";
					sql_iud += "GROUP BY " + table_schema.getColumn("iuds", "iudservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + " ";

					System.out.println(sql_iud);
					iud = GetMIS3ResultSet.getFinalResult(sql_iud, "notString", zilla, upazila);

					sql_iud_followup = "SELECT COUNT(CASE WHEN (" + table_schema.getColumn("", "iudfollowupservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as iud_followup_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "iudfollowupservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as iud_followup_satelite, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "iudfollowupservice_complication", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("", "iudfollowupservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as iud_sideeffect_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "iudfollowupservice_complication", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("", "iudfollowupservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as iud_sideeffect_satelite "
							+ "FROM " + table_schema.getTable("table_iudfollowupservice") + " "
							+ "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "iudfollowupservice_providerid") + " "
							+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
							+ "where " + query_condition
							+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "iudfollowupservice_providerid") + " "
							+ "where (case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "iudfollowupservice_followupdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "iudfollowupservice_followupdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("", "iudfollowupservice_followupdate", new String[]{start_date, end_date}, "between") + " end) "
							+ "and ";
					if(!associated_id.isEmpty())
						sql_iud_followup += "((" + query_condition +") or " + table_schema.getColumn("table", "iudfollowupservice_providerid", new String[]{associated_id}, "in") + ") ";
					else
						sql_iud_followup += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_iud_followup += "and " + table_schema.getColumn("", "iudfollowupservice_systementrydate", new String[]{submission_date}, "<") + " ";
					sql_iud_followup += "GROUP BY " + table_schema.getColumn("table", "iudfollowupservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + " ";

					//System.out.println(sql_iud_followup);
					iud_followup = GetMIS3ResultSet.getFinalResult(sql_iud_followup, "notString", zilla, upazila);

					sql_implant = "SELECT COUNT(CASE WHEN (" + table_schema.getColumn("implants", "implantservice_implantafterdelivery", new String[]{"1"}, "=") + " and "+table_schema.getColumn("implants", "implantservice_implantimplantdate", new String[]{start_date, end_date}, "between")+") THEN 1 ELSE NULL END) as implant_after_delivery, "
							//+ "COUNT(CASE WHEN " + table_schema.getColumn("implants", "implantservice_implantimplantdate", new String[]{start_date, end_date}, "between") + " THEN 1 ELSE NULL END) as implant_new, "
							+ "COUNT(CASE WHEN " + table_schema.getColumn("implants", "implantservice_implantafterdelivery", new String[]{"2"}, "=") + " and "+table_schema.getColumn("implants", "implantservice_implantimplantdate", new String[]{start_date, end_date}, "between")+" THEN 1 ELSE NULL END) as implant_new, "
							+ "COUNT(CASE WHEN (((" + table_schema.getColumn("implants", "implantservice_implanttype", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("implants", "implantservice_implantimplantdate") + "+interval '3 years'>" + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate") + ") OR (" + table_schema.getColumn("implants", "implantservice_implanttype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("implants", "implantservice_implantimplantdate") + "+interval '5 years'>" + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate") + ")) AND " + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as implant_before_time_finish, "
							+ "COUNT(CASE WHEN (((" + table_schema.getColumn("implants", "implantservice_implanttype", new String[]{"1"}, "=") + " AND " + table_schema.getColumn("implants", "implantservice_implantimplantdate") + "+interval '3 years'<=" + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate") + ") OR (" + table_schema.getColumn("implants", "implantservice_implanttype", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("implants", "implantservice_implantimplantdate") + "+interval '5 years'<=" + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate") + ")) AND " + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as implant_after_time_finish, "
							+ "COUNT(CASE WHEN " + table_schema.getColumn("implants", "implantservice_implanttype", new String[]{"1"}, "=") + " and " + table_schema.getColumn("implants", "implantservice_implantimplantdate", new String[]{start_date, end_date}, "between") + " THEN 1 ELSE NULL END) as implant_implanon, "
							+ "COUNT(CASE WHEN " + table_schema.getColumn("implants", "implantservice_implanttype", new String[]{"2"}, "=") + " and " + table_schema.getColumn("implants", "implantservice_implantimplantdate", new String[]{start_date, end_date}, "between") + " THEN 1 ELSE NULL END) as implant_jadelle ";
					if(DateOption>2018){
						sql_implant += ",COUNT(CASE WHEN (" + table_schema.getColumn("implant2", "implantservice_implantimplantdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("implants", "implantservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as implant_ppfp_expire_center, " +
								"COUNT(CASE WHEN (" + table_schema.getColumn("implant2", "implantservice_implantimplantdate", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("implants", "implantservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as implant_ppfp_expire_satelite ";
					}
					sql_implant += "FROM " + table_schema.getTable("table_implantservice") + " as implants left join " + table_schema.getTable("table_implantfollowupservice") + " as ifs on " + table_schema.getColumn("implants", "implantservice_healthid") + "=" + table_schema.getColumn("ifs", "implantfollowupservice_healthid") + " and " + table_schema.getColumn("implants", "implantservice_implantcount") + "=" + table_schema.getColumn("ifs", "implantservice_implantcount") + " and " + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate", new String[]{}, "isnotnull") + " ";
                    //Added new condition for ppfp if data is exist in fpinfo but not in delivery
                    sql_implant += "left join fpinfo on fpinfo.healthid = implants.healthid and fpinfo.lastdeliverydate + interval '1 years' > implants.implantimplantdate ";

                    if(DateOption>2018) {
						sql_implant += "left join " + table_schema.getTable("table_delivery") + " on " + table_schema.getColumn("table", "delivery_healthid") + "=" + table_schema.getColumn("implants", "implantservice_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'>" + table_schema.getColumn("implants", "implantservice_implantimplantdate")+" "+
								"left join (select " + table_schema.getColumn("", "implantservice_implantimplantdate") + ", " + table_schema.getColumn("", "implantservice_healthid") + " FROM " + table_schema.getTable("table_implantservice") + " " +
								"where " + table_schema.getColumn("", "implantservice_implantimplantdate") + "< '" + start_date + "' limit 1) implant2 on "+ table_schema.getColumn("implant2", "implantservice_healthid") + "=" + table_schema.getColumn("implant2", "implantservice_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'<" + table_schema.getColumn("implant2", "implantservice_implantimplantdate")+" and "+table_schema.getColumn("table", "delivery_outcomedate") + "+interval '1 years'<" + table_schema.getColumn("implants", "implantservice_implantimplantdate")+ " ";
					}
					sql_implant += "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("implants", "implantservice_providerid") + " or " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("ifs", "implantfollowupservice_providerid") + " "
							+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
							+ "where " + query_condition
							+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " "//+ table_schema.getColumn("implants", "implantservice_providerid") + " or " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("ifs", "implantfollowupservice_providerid") + " "
							+ "where ((case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("implants", "implantservice_implantimplantdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("implants", "implantservice_implantimplantdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("implants", "implantservice_implantimplantdate", new String[]{start_date, end_date}, "between") + " end) or "
							+ "(case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("ifs", "implantfollowupservice_implantremovedate", new String[]{start_date, end_date}, "between") + " end)) "
							+ "and ";
					if(!associated_id.isEmpty())
						sql_implant += "((" + query_condition +") or " + table_schema.getColumn("implants", "implantservice_providerid", new String[]{associated_id}, "in") + ") ";
					else
						sql_implant += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_implant += "and " + table_schema.getColumn("implants", "implantservice_systementrydate", new String[]{submission_date}, "<") + " ";
					sql_implant += "GROUP BY " + table_schema.getColumn("implants", "implantservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + " ";

					System.out.println(sql_implant);
					implant = GetMIS3ResultSet.getFinalResult(sql_implant, "notString", zilla, upazila);


					sql_implant_followup = "SELECT COUNT(CASE WHEN (" + table_schema.getColumn("", "implantfollowupservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as implant_followup_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "implantfollowupservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as implant_followup_satelite, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "implantfollowupservice_complication", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("", "implantfollowupservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as implant_sideeffect_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("", "implantfollowupservice_complication", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("", "implantfollowupservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as implant_sideeffect_satelite "
							+ "FROM " + table_schema.getTable("table_implantfollowupservice") + " "
							+ "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "implantfollowupservice_providerid") + " "
							+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
							+ "where " + query_condition
							+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "implantfollowupservice_providerid") + " "
							+ "where (case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("", "implantfollowupservice_followupdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("", "implantfollowupservice_followupdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("", "implantfollowupservice_followupdate", new String[]{start_date, end_date}, "between") + " end) "
							+ "and ";
					if(!associated_id.isEmpty())
						sql_implant_followup += "((" + query_condition +") or " + table_schema.getColumn("table", "implantfollowupservice_providerid", new String[]{associated_id}, "in") + ") ";
					else
						sql_implant_followup += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_implant_followup += "and " + table_schema.getColumn("table", "implantfollowupservice_systementrydate", new String[]{submission_date}, "<") + " ";
					sql_implant_followup += "GROUP BY " + table_schema.getColumn("table", "implantfollowupservice_providerid") + "," + table_schema.getColumn("", "providerdb_zillaid") + "," + table_schema.getColumn("", "providerdb_upazilaid") + "," + table_schema.getColumn("", "providerdb_unionid") + " ";

					System.out.println(sql_implant_followup);
					implant_followup = GetMIS3ResultSet.getFinalResult(sql_implant_followup, "notString", zilla, upazila);
				}

				/* Permanent Method
				 * @Author: Evana
				 * @ Date: 22/05/2019
				 * */

				sql_permanent_method = "SELECT COUNT(CASE WHEN (" + table_schema.getColumn("permanent", "permanent_method_service_permanent_method_operation_time", new String[]{"2"}, "!=") + " AND " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_normal_male, "
						+ "COUNT(CASE WHEN (" + table_schema.getColumn("permanent", "permanent_method_service_permanent_method_operation_time", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_after_delivery_male, "
						+ "COUNT(CASE WHEN (" + table_schema.getColumn("permanent", "permanent_method_service_permanent_method_operation_time", new String[]{"2"}, "!=") + " AND " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_normal_female, "
						+ "COUNT(CASE WHEN (" + table_schema.getColumn("permanent", "permanent_method_service_permanent_method_operation_time", new String[]{"2"}, "=") + " AND " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_after_delivery_female "
						+ "FROM " + table_schema.getTable("table_permanent_method_service") + " as permanent "
						+ "join " + table_schema.getTable("table_clientmap") + " on " + table_schema.getColumn("table", "clientmap_generatedid") + "=" + table_schema.getColumn("permanent", "permanent_method_service_healthid") + " "
						+ "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("permanent", "permanent_method_service_providerid") + " "
						+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
						+ "where " + query_condition
						+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " "
						+ "where (case when count_provider>1 then "
						+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("permanent", "permanent_method_service_permanent_method_operation_date") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("permanent", "permanent_method_service_permanent_method_operation_date") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
						+ "else " + table_schema.getColumn("permanent", "permanent_method_service_permanent_method_operation_date", new String[]{start_date, end_date}, "between") + " end) "
						+ "and ";

				if(!associated_id.isEmpty())

					sql_permanent_method += "((" + query_condition +") or " + table_schema.getColumn("permanent", "permanent_method_service_providerid", new String[]{associated_id}, "in") + ") ";

				else

					sql_permanent_method += query_condition + " ";

				sql_permanent_method += "GROUP BY " + table_schema.getColumn("permanent", "permanent_method_service_providerid") + "," + table_schema.getColumn("table", "providerdb_zillaid") + "," + table_schema.getColumn("table", "providerdb_upazilaid") + "," + table_schema.getColumn("table", "providerdb_unionid") + " ";

				System.out.println("permanent method="+sql_permanent_method);
				pms = GetMIS3ResultSet.getFinalResult(sql_permanent_method, "notString", zilla, upazila);

				System.out.println("PMS Result="+pms);

				//System.out.println("Permanent Method="+ sql_permanent_method);


				/*
				 * Permanent Method Followup Query
				 * @Author: evana
				 * */


				sql_permanent_method_followup = "SELECT COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_followup_male_center, "
						+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_followup_female_center, "
						+ "COUNT(CASE WHEN (" + table_schema.getColumn("permanent", "permanent_method_followup_service_complication", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_complication_male, "
						+ "COUNT(CASE WHEN (" + table_schema.getColumn("permanent", "permanent_method_followup_service_complication", new String[]{}, "isnotnull") + " AND " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_complication_female "
						+ "FROM " + table_schema.getTable("table_permanent_method_followup_service") + " as permanent "
						+ "join " + table_schema.getTable("table_clientmap") + " on " + table_schema.getColumn("table", "clientmap_generatedid") + "=" + table_schema.getColumn("permanent", "permanent_method_followup_service_healthid") + " "
						+ "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("permanent", "permanent_method_followup_service_providerid") + " "
						+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
						+ "where " + query_condition
						+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "providerdb_provcode") + " "
						+ "where (case when count_provider>1 then "
						+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("permanent", "permanent_method_followup_service_followup_date") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("permanent", "permanent_method_followup_service_followup_date") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
						+ "else " + table_schema.getColumn("permanent", "permanent_method_followup_service_followup_date", new String[]{start_date, end_date}, "between") + " end) "
						+ "and ";


				if(!associated_id.isEmpty())

					sql_permanent_method_followup += "((" + query_condition +") or " + table_schema.getColumn("permanent", "permanent_method_followup_service_providerid", new String[]{associated_id}, "in") + ") ";

				else

					sql_permanent_method_followup += query_condition + " ";

				sql_permanent_method_followup += "GROUP BY " + table_schema.getColumn("permanent", "permanent_method_followup_service_providerid") + "," + table_schema.getColumn("table", "providerdb_zillaid") + "," + table_schema.getColumn("table", "providerdb_upazilaid") + "," + table_schema.getColumn("table", "providerdb_unionid") + " ";

				System.out.println("permanent method Followup="+sql_permanent_method_followup);
				pms_followup = GetMIS3ResultSet.getFinalResult(sql_permanent_method_followup, "notString", zilla, upazila);

				System.out.println("PMS Followup Result="+pms_followup);


				sql_anc = "SELECT ";


				if(mis3ViewType.equals("2")) {
					sql_anc += "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric < 5 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as anc1center, "
							+ "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric < 5 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as anc1satelite, "
							+ "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric  between 5 and 6 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as anc2center, "
							+ "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric  between 5 and 6 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as anc2satelite, "
							+ "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric  between 7 and 8 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as anc3center, "
							+ "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric  between 7 and 8 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as anc3satelite, "
							+ "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric > 8 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as anc4center, "
							+ "count(case when (extract('month' from  age( "+table_schema.getColumn("table", "ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric > 8 AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as anc4satelite, ";
				}
				else {
					sql_anc += "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS anc1center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS anc1satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"2"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS anc2center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"2"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS anc2satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"3"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS anc3center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"3"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS anc3satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"4"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS anc4center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_serviceid",new String[]{"4"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS anc4satelite,";
				}

				sql_anc += "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_refer",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS ancRefercenter,"
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_refer",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS ancRefersatelite, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"2\"%'  AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS ancMgSO4center, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"2\"%'  AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS ancMgSO4satelite, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"23\"%' AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS ancMisoprostolcenter, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"23\"%' AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS ancMisoprostolsatelite, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"25\"%' AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS ancAntinatalKorticenter, " //@Author: Evana : 21/05/2019
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"25\"%' AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS ancAntinatalKortisatelite, " // @Author: Evana : 21/05/2019
						+ "COUNT(CASE WHEN (("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"4\"%' AND "+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"12\"%')) OR "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+" THEN 1 ELSE NULL END) as ancprgwomanayronfoliccenter, "		// @Author: evana, @Date: 29-05-2019
						+ "COUNT(CASE WHEN (("+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"4\"%' AND "+table_schema.getColumn("table", "ancservice_treatment")+" like '%\"12\"%')) OR "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+" THEN 1 ELSE NULL END) as ancprgwomanayronfolicsatelite, "	// @Author: evana, @Date: 29-05-2019
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_weight",new String[]{},"isnotnull")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS ancmotherweightcenter, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_weight",new String[]{},"isnotnull")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS ancmotherweightsatelite, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_disease")+" like '%\"16\"%' AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS ancAPHcenter, " //@Author: Evana : 21/05/2019
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_disease")+" like '%\"16\"%'  AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS ancAPHsatelite, " // @Author: Evana : 21/05/2019
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_advice")+" like '%\"16\"%'  AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS ancAdvicecenter, " // Code By Evana: 20/01/2019
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_advice")+" like '%\"16\"%'  AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS ancAdvicesatelite, " // Coded By Evana: 20/01/2019
						+ "COUNT(CASE WHEN (("+table_schema.getColumn("table", "ancservice_symptom")+" like '%\"3\"%' OR "+table_schema.getColumn("table", "ancservice_symptom")+" like '%\"5\"%')) AND "+table_schema.getColumn("table", "ancservice_urinealbumin")+"::integer > 1 AND "+table_schema.getColumn("table", "ancservice_bpdiastolic",new String[]{"90"},">=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnull")+" THEN 1 ELSE NULL END) as anchighpreeclampsiacenter, "
						+ "COUNT(CASE WHEN (("+table_schema.getColumn("table", "ancservice_symptom")+" like '%\"3\"%' OR "+table_schema.getColumn("table", "ancservice_symptom")+" like '%\"5\"%')) AND "+table_schema.getColumn("table", "ancservice_urinealbumin")+"::integer > 1 AND "+table_schema.getColumn("table", "ancservice_bpdiastolic",new String[]{"90"},">=")+" AND "+table_schema.getColumn("table", "ancservice_satelitecentername",new String[]{},"isnotnull")+" THEN 1 ELSE NULL END) as anchighpreeclampsiasatelite "
						+ "FROM " + table_schema.getTable("table_ancservice") + " "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "ancservice_providerid")+" "
						+ "left join "+ table_schema.getTable("table_pregwomen") +" on "+table_schema.getColumn("table", "pregwomen_healthid")+"="+table_schema.getColumn("table", "ancservice_healthid")+" and "+table_schema.getColumn("table", "pregwomen_pregno")+"="+table_schema.getColumn("table", "ancservice_pregno")+" "
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "ancservice_providerid")+" "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","ancservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","ancservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("", "ancservice_visitdate",new String[]{start_date,end_date},"between") +" end) "
						+ "AND ("+table_schema.getColumn("table", "ancservice_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("table", "ancservice_servicesource",new String[]{"0"},"=")+") "
						+ "and ";


				if(!associated_id.isEmpty())
					sql_anc += "((" + query_condition +") or " + table_schema.getColumn("table", "ancservice_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_anc += query_condition + " ";

				//System.out.println(sql_anc);
//				if(!submission_date.equals(""))
//					sql_anc +=  "and "+ table_schema.getColumn("table", "ancservice_systementrydate",new String[]{submission_date},"<") +" ";
				sql_anc +=  "GROUP BY "+table_schema.getColumn("table","ancservice_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+" ";

				System.out.println(sql_anc);

				anc = GetMIS3ResultSet.getFinalResult(sql_anc, "notString", zilla, upazila, csba);

				System.out.println("Anc Result="+anc);

				//SELECT COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + ") THEN 1 ELSE NULL END) as permanent_method_followup_male_center, "

		/*	sql_pac = "SELECT COUNT ("+table_schema.getColumn("table", "pacservice_serviceid")+") AS serviceCount "
					+ "FROM "+ table_schema.getTable("table_pacservice") +" "
					+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "pacservice_providerid")+" "
					+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
					+ "where "+query_condition+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "pacservice_providerid")+" "						+ "where (case when count_provider>1 then "
					+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pacservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pacservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
					+ "else "+ table_schema.getColumn("", "pacservice_visitdate",new String[]{start_date,end_date},"between") +" end) "
					+ "and ";*/

				sql_pac = "SELECT COUNT(CASE WHEN ("+table_schema.getColumn("table", "pacservice_serviceid",new String[]{},"isnotnull")+" AND "+table_schema.getColumn("table", "pacservice_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS serviceCount, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pacservice_serviceid",new String[]{},"isnotnull")+" AND "+table_schema.getColumn("table", "pacservice_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pacservicecountsatelite "
						+ "FROM "+ table_schema.getTable("table_pacservice") +" "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "pacservice_providerid")+" "
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "pacservice_providerid")+" "						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pacservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pacservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("", "pacservice_visitdate",new String[]{start_date,end_date},"between") +" end) "
						+ "and ";


				if(!associated_id.isEmpty())
					sql_pac += "((" + query_condition +") or " + table_schema.getColumn("table", "pacservice_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_pac += query_condition + " ";
//				if(!submission_date.equals(""))
//					sql_pac +=  "and "+ table_schema.getColumn("table", "pacservice_systementrydate",new String[]{submission_date},"<") +" ";
				sql_pac +=  "GROUP BY "+table_schema.getColumn("table","pacservice_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+" ";
				System.out.println("PAC query="+sql_pac);

				pac = GetMIS3ResultSet.getFinalResult(sql_pac, "notString", zilla, upazila, csba);


				sql_newborn_refer = "SELECT \"newbornReferTable\".\"newbornRefer\","
						+ "\"newbornReferTablePNC\".\"pncnewbornReferCenter\",\"newbornReferTablePNC\".\"pncnewbornReferSatelite\" "
						+ "FROM "+ table_schema.getTable("table_providerdb") +" "
						+ "join "
						+ "(SELECT "+table_schema.getColumn("table", "pncservicechild_providerid")+", "+table_schema.getColumn("table", "providerdb_zillaid")+", "+table_schema.getColumn("table", "providerdb_upazilaid")+", "+table_schema.getColumn("table", "providerdb_unionid")+", "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_refer",new String[]{Integer.toString(referYes)},"=")+" "
						+ "AND ("+table_schema.getColumn("table", "pncservicechild_visitdate")+" - "+table_schema.getColumn("table", "newborn_outcomedate")+") <= " + newborn + " "
						+ "AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS \"pncnewbornReferCenter\","
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_refer",new String[]{Integer.toString(referYes)},"=")+" "
						+ "AND ("+table_schema.getColumn("table", "pncservicechild_visitdate")+" - "+table_schema.getColumn("table", "newborn_outcomedate")+") <= " + newborn + " "
						+ "AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS \"pncnewbornReferSatelite\" "
						+ "FROM "+ table_schema.getTable("table_pncservicechild") +" "
						+ "JOIN "+ table_schema.getTable("table_newborn") +" using ("+table_schema.getColumn("", "pncservicechild_healthid")+","+table_schema.getColumn("", "pncservicechild_pregno")+","+table_schema.getColumn("", "pncservicechild_childno")+") "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "pncservicechild_providerid")+" "
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "pncservicechild_providerid")+" "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","pncservicechild_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","pncservicechild_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("table", "pncservicechild_visitdate",new String[]{start_date,end_date},"between") +" end) "
						+ "and ";
				if(!associated_id.isEmpty())
					sql_newborn_refer += "((" + query_condition +") or " + table_schema.getColumn("table", "pncservicechild_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_newborn_refer += query_condition + " ";
//				if(!submission_date.equals(""))
//					sql_newborn_refer +=  "and "+ table_schema.getColumn("table", "pncservicechild_systementrydate",new String[]{submission_date},"<") +" ";
				sql_newborn_refer += "GROUP BY "+table_schema.getColumn("table","pncservicechild_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+") AS \"newbornReferTablePNC\" "
						+ "on  \"newbornReferTablePNC\"."+table_schema.getColumn("", "pncservicechild_providerid")+"="+table_schema.getColumn("table", "providerdb_provcode")+" and \"newbornReferTablePNC\"."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND \"newbornReferTablePNC\"."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND \"newbornReferTablePNC\"."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+" "
						+ "join "
						+ "(SELECT "+table_schema.getColumn("table", "newborn_providerid")+", "+table_schema.getColumn("table", "providerdb_zillaid")+", "+table_schema.getColumn("table", "providerdb_upazilaid")+", "+table_schema.getColumn("table", "providerdb_unionid")+", COUNT("+table_schema.getColumn("table", "newborn_refer")+") AS \"newbornRefer\" "
						+ "FROM "+ table_schema.getTable("table_newborn") +" "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "newborn_providerid")+""
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "newborn_providerid")+" "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","newborn_outcomedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","newborn_outcomedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("table", "newborn_outcomedate",new String[]{start_date,end_date},"between") +" end) "
						+ "AND "+table_schema.getColumn("table", "newborn_refer",new String[]{Integer.toString(referYes)},"=")+" "
						+ "and ";
				if(!associated_id.isEmpty())
					sql_newborn_refer += "((" + query_condition +") or " + table_schema.getColumn("table", "newborn_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_newborn_refer += query_condition + " ";
//				if(!submission_date.equals(""))
//					sql_newborn_refer +=  "and "+ table_schema.getColumn("table", "newborn_systementrydate",new String[]{submission_date},"<") +" ";
				sql_newborn_refer += "GROUP BY "+table_schema.getColumn("table","newborn_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+")  AS \"newbornReferTable\" on  "
						+ "\"newbornReferTable\"."+table_schema.getColumn("","newborn_providerid")+"="+table_schema.getColumn("table", "providerdb_provcode")+" and \"newbornReferTable\"."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND \"newbornReferTable\"."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND \"newbornReferTable\"."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+" "
						//+ "WHERE \"newbornReferTablePNC\".\"providerId\" = \"newbornReferTable\".\"providerId\" "
						+ "ORDER BY \"newbornReferTablePNC\"."+table_schema.getColumn("","pncservicechild_providerid")+"";
				System.out.println(sql_newborn_refer);
				pnc_newborn_refer = GetMIS3ResultSet.getFinalResult(sql_newborn_refer, "notString", zilla, upazila, csba);

				sql_child = "SELECT ";

				if(mis3ViewType.equals("2")) {
					sql_child += "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric < 2 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncchild1center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric < 2 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncchild1satelite, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 2 and 3 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncchild2center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 2 and 3 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncchild2satelite, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 7 and 14 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncchild3center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 7 and 14 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncchild3satelite, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 42 and 48 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncchild4center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicechild_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 42 and 48 AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncchild4satelite ";
				}
				else {
					sql_child += "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncchild1center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncchild1satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"2"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncchild2center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"2"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncchild2satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"3"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncchild3center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"3"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncchild3satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"4"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncchild4center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicechild_serviceid",new String[]{"4"},"=")+" AND "+table_schema.getColumn("table", "pncservicechild_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncchild4satelite ";
				}

				sql_child += "FROM "+ table_schema.getTable("table_pncservicechild") +" "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "pncservicechild_providerid")+" "
						+ "left join "+ table_schema.getTable("table_delivery") +" on "+table_schema.getColumn("table", "delivery_healthid")+"="+table_schema.getColumn("table", "pncservicechild_healthid")+" and "+table_schema.getColumn("table", "delivery_pregno")+"="+table_schema.getColumn("table", "pncservicechild_pregno")+" "
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "pncservicechild_providerid")+" "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pncservicechild_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pncservicechild_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("", "pncservicechild_visitdate",new String[]{start_date,end_date},"between") +" end) "
						+ "AND ("+table_schema.getColumn("table", "pncservicechild_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("table", "pncservicechild_servicesource",new String[]{"0"},"=")+") "
						+ "and ";
				if(!associated_id.isEmpty())
					sql_child += "((" + query_condition +") or " + table_schema.getColumn("table", "pncservicechild_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_child += query_condition + " ";
//				if(!submission_date.equals(""))
//					sql_child +=  "and "+ table_schema.getColumn("table", "pncservicechild_systementrydate",new String[]{submission_date},"<") +" ";
				sql_child +=  "GROUP BY "+table_schema.getColumn("table","pncservicechild_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+" ";
				System.out.println(sql_child);
				pnc_child = GetMIS3ResultSet.getFinalResult(sql_child, "notString", zilla, upazila, csba);

				sql_mother = "SELECT ";

				if(mis3ViewType.equals("2")) {
					sql_mother += "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric < 2 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncmother1center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric < 2 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncmother1satelite, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 2 and 3 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncmother2center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 2 and 3 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncmother2satelite, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 7 and 14 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncmother3center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 7 and 14 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncmother3satelite, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 42 and 48 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") then 1 else NULL end) as pncmother4center, "
							+ "count(case when (extract('day' from  age( "+table_schema.getColumn("table", "pncservicemother_visitdate")+", "+table_schema.getColumn("", "delivery_outcomedate")+"))::numeric between 42 and 48 AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") then 1 else NULL end) as pncmother4satelite, ";
				}
				else {
					sql_mother += "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncmother1center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncmother1satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"2"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncmother2center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"2"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncmother2satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"3"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncmother3center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"3"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncmother3satelite,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"4"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncmother4center,"
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "pncservicemother_serviceid",new String[]{"4"},"=")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncmother4satelite,";
				}

				sql_mother += "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "pncservicemother_fpmethod",new String[]{"0"},"<>") +" AND "+ table_schema.getColumn("table", "pncservicemother_fpmethod",new String[]{"0"},"<>") +" AND "+table_schema.getColumn("table", "pncservicemother_fpmethod",new String[]{},"isnotnull")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncmotherFPcenter,"
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "pncservicemother_fpmethod",new String[]{"0"},"<>") +" AND "+ table_schema.getColumn("table", "pncservicemother_fpmethod",new String[]{"0"},"<>") +" AND "+table_schema.getColumn("table", "pncservicemother_fpmethod",new String[]{},"isnotnull")+" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncmotherFPsatelite,"
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "pncservicemother_refer",new String[]{"1"},"=") +" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncmotherRefercenter,"
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "pncservicemother_refer",new String[]{"1"},"=") +" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncmotherRefersatelite, "
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "pncservicemother_disease",new String[]{"3"},"=") +" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS pncmotherPPHcenter,"
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "pncservicemother_disease",new String[]{"3"},"=") +" AND "+table_schema.getColumn("table", "pncservicemother_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS pncmotherPPHsatelite "
						+ "FROM " + table_schema.getTable("table_pncservicemother") + " "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+ table_schema.getColumn("table", "pncservicemother_providerid") +" "
						+ "left join "+ table_schema.getTable("table_delivery") +" on "+table_schema.getColumn("table", "delivery_healthid")+"="+table_schema.getColumn("table", "pncservicemother_healthid")+" and "+table_schema.getColumn("table", "delivery_pregno")+"="+table_schema.getColumn("table", "pncservicemother_pregno")+" "
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+ table_schema.getColumn("table", "pncservicemother_providerid") +" "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pncservicemother_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pncservicemother_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("", "pncservicemother_visitdate",new String[]{start_date,end_date},"between") +" end) "
						+ "AND ("+table_schema.getColumn("table", "pncservicemother_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("table", "pncservicemother_servicesource",new String[]{"0"},"=")+") "
						+ "and ";
				if(!associated_id.isEmpty())
					sql_mother += "((" + query_condition +") or " + table_schema.getColumn("table", "pncservicemother_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_mother += query_condition + " ";
//				if(!submission_date.equals(""))
//					sql_mother +=  "and "+ table_schema.getColumn("table", "pncservicemother_systementrydate",new String[]{submission_date},"<") +" ";
				sql_mother +=  "GROUP BY "+table_schema.getColumn("table","pncservicemother_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+" ";
				System.out.println(sql_mother);
				pnc_mother = GetMIS3ResultSet.getFinalResult(sql_mother, "notString", zilla, upazila, csba);

				sql_delivery = "SELECT COUNT(CASE WHEN ("+table_schema.getColumn("table", "delivery_outcometype",new String[]{Integer.toString(normalDelivery)},"=")+") THEN 1 ELSE NULL END) AS normaldelivery, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "delivery_outcometype",new String[]{Integer.toString(csectionDelivery)},"=")+") THEN 1 ELSE NULL END) AS csectiondelivery, "
						+ "COUNT(CASE WHEN ("+table_schema.getColumn("table", "delivery_applyoxytocin",new String[]{Integer.toString(applyOxytocinYes)},"=")+" "
						+ "AND "+table_schema.getColumn("table", "delivery_applytraction",new String[]{Integer.toString(applyTractionYes)},"=")+" "
						+ "AND "+table_schema.getColumn("table", "delivery_uterusmassage",new String[]{Integer.toString(applyUterusMassageYes)},"=")+") THEN 1 ELSE NULL END) AS amtsl, "
						+ "(CASE WHEN (SUM("+table_schema.getColumn("table", "delivery_livebirth")+") IS NOT NULL) THEN SUM("+table_schema.getColumn("table", "delivery_livebirth")+") ELSE 0 END) AS livebirth, "
						+ "SUM(CASE WHEN "+table_schema.getColumn("table", "delivery_livebirth")+" IS NOT NULL and "+table_schema.getColumn("table", "delivery_satelitecentername",new String[]{},"isnull")+" THEN "+table_schema.getColumn("table", "delivery_livebirth")+" ELSE 0 END) AS livebirth_center, "
						+ "SUM(CASE WHEN "+table_schema.getColumn("table", "delivery_livebirth")+" IS NOT NULL and "+table_schema.getColumn("table", "delivery_satelitecentername",new String[]{},"isnotnull")+" THEN "+table_schema.getColumn("table", "delivery_livebirth")+" ELSE 0 END) AS livebirth_satelite, "
						+ "(CASE WHEN (SUM("+table_schema.getColumn("table", "delivery_stillbirth")+") IS NOT NULL) THEN SUM("+table_schema.getColumn("table", "delivery_stillbirth")+") ELSE 0 END) AS stillbirth,"
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "delivery_refer",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "delivery_satelitecentername",new String[]{},"isnull")+") THEN 1 ELSE NULL END) AS deliveryRefercenter,"
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "delivery_refer",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "delivery_satelitecentername",new String[]{},"isnotnull")+") THEN 1 ELSE NULL END) AS deliveryRefersatelite, "
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "delivery_misoprostol",new String[]{"1"},"=")+") THEN 1 ELSE NULL END) AS deliverymisoprostol, "
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "delivery_applyoxytocin",new String[]{"1"},"=")+") THEN 1 ELSE NULL END) AS deliverycxytocin, "
						+ "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "delivery_excessbloodloss",new String[]{"1"},"=")+") THEN 1 ELSE NULL END) AS deliveryIPHcenter " // @Author: Evana, Date: 21/05/2019
						+ "FROM "+ table_schema.getTable("table_delivery") +" "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "delivery_providerid")+" "
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "delivery_providerid")+"  "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","delivery_outcomedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","delivery_outcomedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("", "delivery_outcomedate",new String[]{start_date,end_date},"between") +" end) "
						+ "AND "+table_schema.getColumn("table", "delivery_deliverydonebythisprovider",new String[]{Integer.toString(deliveryDoneByThisProviderYes)},"=")+" "
						+ "and ";
				if(!associated_id.isEmpty())
					sql_delivery += "((" + query_condition +") or " + table_schema.getColumn("table", "delivery_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_delivery += query_condition + " ";
//				if(!submission_date.equals(""))
//					sql_delivery +=  "and "+ table_schema.getColumn("table", "delivery_systementrydate",new String[]{submission_date},"<") +" ";
				sql_delivery +=  "GROUP BY "+table_schema.getColumn("table","delivery_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+" ";
				System.out.println(sql_delivery);
				delivery = GetMIS3ResultSet.getFinalResult(sql_delivery, "notString", zilla, upazila, csba);

				sql_newborn = "SELECT COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_dryingafterbirth",new String[]{"1"},"=")+" THEN 1 ELSE NULL END) as dryingafterbirth, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_chlorehexidin",new String[]{"1"},"=")+" THEN 1 ELSE NULL END) as chlorehexidin, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_skintouch",new String[]{"1"},"=")+" THEN 1 ELSE NULL END) as skintouch, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_breastfeed",new String[]{"1"},"=")+" THEN 1 ELSE NULL END) as breastfeed, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_bagnmask",new String[]{"1"},"=")+" THEN 1 ELSE NULL END) as bagnmask, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthweight")+"<2.5 THEN 1 ELSE NULL END) as lowbirthweight, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthweight")+"<2.5 and "+ table_schema.getColumn("", "newborn_birthweight") +">=2 and "+table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnull")+" THEN 1 ELSE NULL END) as less_weight_2500_child_center, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthweight")+"<2.5 and "+ table_schema.getColumn("", "newborn_birthweight") +">=2 and "+table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnotnull")+" THEN 1 ELSE NULL END) as less_weight_2500_child_satelite, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthweight") +"<2 and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnull") +" THEN 1 ELSE NULL END) as less_weight_2000_child_center, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthweight") +"<2 and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnotnull") +" THEN 1 ELSE NULL END) as less_weight_2000_child_satelite, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_immaturebirth",new String[]{"1"},"=")+" THEN 1 ELSE NULL END) as immaturebirth, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_immaturebirth",new String[]{"1"},"=")+" and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnull") +" THEN 1 ELSE NULL END) as immaturebirth_center, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_immaturebirth",new String[]{"1"},"=")+" and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnotnull") +" THEN 1 ELSE NULL END) as immaturebirth_satelite, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthstatus",new String[]{"2"},"=")+" and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnull") +" THEN 1 ELSE NULL END) as stillbirth_fresh_center, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthstatus",new String[]{"2"},"=")+" and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnotnull") +" THEN 1 ELSE NULL END) as stillbirth_fresh_satelite, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthstatus",new String[]{"3"},"=")+" and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnull") +" THEN 1 ELSE NULL END) as stillbirth_macerated_center, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("", "newborn_birthstatus",new String[]{"3"},"=")+" and "+ table_schema.getColumn("table", "newborn_satelitecentername", new String[]{}, "isnotnull") +" THEN 1 ELSE NULL END) as stillbirth_macerated_satelite "
						+ "FROM "+ table_schema.getTable("table_newborn") +" "
						+ "join "+ table_schema.getTable("table_delivery") +" on "+table_schema.getColumn("table", "delivery_healthid")+"="+ table_schema.getColumn("table", "newborn_healthid")+" and "+table_schema.getColumn("table", "delivery_pregno")+"="+table_schema.getColumn("table", "newborn_pregno")+" "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "newborn_providerid")+""
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "newborn_providerid")+" "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","newborn_outcomedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","newborn_outcomedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("table", "newborn_outcomedate",new String[]{start_date,end_date},"between") +" end) "
						+ "AND "+table_schema.getColumn("table", "delivery_deliverydonebythisprovider",new String[]{Integer.toString(deliveryDoneByThisProviderYes)},"=")+" "
						+ "and ";
				if(!associated_id.isEmpty())
					sql_newborn += "((" + query_condition +") or " + table_schema.getColumn("table", "newborn_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_newborn += query_condition + " ";
//				if(!submission_date.equals(""))
//					sql_newborn +=  "and "+ table_schema.getColumn("table", "newborn_systementrydate",new String[]{submission_date},"<") +" ";
				sql_newborn +=  "GROUP BY "+table_schema.getColumn("table","newborn_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+" ";
				System.out.println(sql_newborn);
				newborn_info = GetMIS3ResultSet.getFinalResult(sql_newborn, "notString", zilla, upazila, csba);

				sql_maternalDeath = "SELECT COUNT(*) as maternal_death, "
						+ "COUNT(CASE WHEN "+ table_schema.getColumn("table", "delivery_outcomedate", new String[]{}, "isnotnull") +" THEN 1 ELSE NULL END) AS newborn_death "
						+ "FROM "+ table_schema.getTable("table_death") +" "
						+ "left join " + table_schema.getTable("table_delivery") + " on " + table_schema.getColumn("table", "delivery_healthid") + "=" + table_schema.getColumn("table", "death_healthid") + " and " + table_schema.getColumn("table", "delivery_outcomedate") + "+interval '29 days'>" + table_schema.getColumn("table", "death_deathdt")+" "
						+ "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "death_providerid")+" "
						+ "left join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+query_condition
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "death_providerid")+"  "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","death_deathdt")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","death_deathdt")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("", "death_deathdt",new String[]{start_date,end_date},"between") +" end) "
						+ "AND "+ table_schema.getColumn("", "death_deathofpregwomen",new String[]{"1"},"=")+" "
						+ "and ";
				if(!associated_id.isEmpty())
					sql_maternalDeath += "((" + query_condition +") or " + table_schema.getColumn("table", "death_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_maternalDeath += query_condition + " ";
//					if(!submission_date.equals(""))
//						sql_maternalDeath +=  "and "+ table_schema.getColumn("table", "death_systementrydate",new String[]{submission_date},"<") +" ";
				sql_maternalDeath +=  "GROUP BY "+table_schema.getColumn("table","death_providerid")+","+table_schema.getColumn("","providerdb_zillaid")+","+table_schema.getColumn("","providerdb_upazilaid")+","+table_schema.getColumn("","providerdb_unionid")+" ";

				System.out.println(sql_maternalDeath);
				maternalDeath = GetMIS3ResultSet.getFinalResult(sql_maternalDeath, "notString", zilla, upazila, csba);

				query_adolescence = getQuery(end_date);

				/**
				 * if startdate is before september then query_child use gp_service otherwise it uses child_care_service
				 */
                List <Object> query_child_array = getChildQuery(start_date,end_date);
				boolean query_from_gpservice = Boolean.valueOf(query_child_array.get(0).toString());
				query_child = query_child_array.get(1).toString();

				if(!csba.equals("1")) {
					sql_gp = "SELECT COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " AND (date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer > 5) " + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_male_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=")+ "AND (date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer > 5 )" + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_male_satelite, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ "AND (date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer > 5 )" + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_female_center, "
							+ "COUNT(CASE WHEN (" + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + "AND (date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer > 5 )" + " AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_female_satelite, "
							+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND " + table_schema.getColumn("", "gpservice_treatment") + " like '%\"53\"%' AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_ifa_center, "
							+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND " + table_schema.getColumn("", "gpservice_treatment") + " like '%\"53\"%' AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_ifa_satelite, "
							+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_disease") + " like '%\"14\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"15\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"25\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"26\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_rta_sta_center, "
							+ "COUNT(CASE WHEN ((date_part('year', age('" + end_date + "'," + table_schema.getColumn("", "clientmap_dob") + "))::integer between 10 and 19) AND (" + table_schema.getColumn("", "gpservice_disease") + " like '%\"14\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"15\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"25\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"26\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_rta_sta_satelite, "
							+ query_adolescence;

					if(query_from_gpservice) {
						sql_gp += query_child;
					}
					sql_gp	+=  "COUNT(CASE WHEN ((" + table_schema.getColumn("", "gpservice_disease") + " like '%\"14\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"15\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"25\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"26\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnull") + ") THEN 1 ELSE NULL END) as gp_rta_sta_all_center, "
							+ "COUNT(CASE WHEN ((" + table_schema.getColumn("", "gpservice_disease") + " like '%\"14\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"15\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"25\"%' or " + table_schema.getColumn("", "gpservice_disease") + " like '%\"26\"%') AND " + table_schema.getColumn("table", "gpservice_satelitecentername", new String[]{}, "isnotnull") + ") THEN 1 ELSE NULL END) as gp_rta_sta_all_satelite "
							+ "FROM " + table_schema.getTable("table_gpservice") + " "
							+ "JOIN " + table_schema.getTable("table_clientmap") + " on " + table_schema.getColumn("table", "clientmap_generatedid") + "=" + table_schema.getColumn("table", "gpservice_healthid") + " "
							+ "left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "gpservice_providerid") + " "
							+ "left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " "
							+ "where " + query_condition
							+ "group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "gpservice_providerid") + "  "
							+ "where (case when count_provider>1 then "
							+ "(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("table", "gpservice_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("table", "gpservice_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) "
							+ "else " + table_schema.getColumn("table", "gpservice_visitdate", new String[]{start_date, end_date}, "between") + " end) "
							+ "and ";
					if(!associated_id.isEmpty())
						sql_gp += "((" + query_condition +") or " + table_schema.getColumn("table", "gpservice_providerid", new String[]{associated_id}, "in") + ") ";
					else
						sql_gp += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_gp += "and " + table_schema.getColumn("table", "gpservice_systementrydate", new String[]{submission_date}, "<") + " ";
					sql_gp += "GROUP BY " + table_schema.getColumn("table", "gpservice_providerid") + "," + table_schema.getColumn("table", "providerdb_zillaid") + "," + table_schema.getColumn("table", "providerdb_upazilaid") + "," + table_schema.getColumn("table", "providerdb_unionid") + " ";

					System.out.println(sql_gp);
					gp = GetMIS3ResultSet.getFinalResult(sql_gp, "notString", zilla, upazila);

					//lmis = GetMIS3ResultSet.getLMISResult(zilla, upazila, union, start_date, end_date);
				}

				sql_child_care_service = "select  ";
				if(!query_from_gpservice) {
					sql_child_care_service += query_child ;
				}
				sql_child_care_service += "count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '1'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '2'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_infected_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '3'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_neumonia_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '3'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_neumonia_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '3'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_neumonia_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '3'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"very_serious_disease_neumonia_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '16'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"pneumonia_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '16'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"pneumonia_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '16'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"pneumonia_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '16'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"pneumonia_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '17'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"influenja_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '17'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"influenja_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '17'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"influenja_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '17'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"influenja_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('18'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '35'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '37'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"diarrhea_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('18'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '35'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '37'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"diarrhea_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('18'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '35'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '37'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"diarrhea_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('18'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '35'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '37'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"diarrhea_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '21'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"malaria_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '21'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"malaria_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '21'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"malaria_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '21'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"malaria_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '22'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"fever_not_malaria_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '22'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"fever_not_malaria_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '22'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"fever_not_malaria_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '22'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"fever_not_malaria_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('28'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '29'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"measles_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('28'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '29'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"measles_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('28'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '29'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"measles_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('28'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '29'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"measles_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('19'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '26'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"ear_problem_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('19'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '26'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"ear_problem_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('19'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '26'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"ear_problem_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('19'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '26'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')))then 1 else null end) as \"ear_problem_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('24'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '30'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '31'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"malnutrition_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('24'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '30'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '31'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"malnutrition_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and ('24'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '30'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '31'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"malnutrition_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and ('24'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '30'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) or '31'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ','))) then 1 else null end) as \"malnutrition_1-5year_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + "))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_2month-1year_female\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_1-5year_male\", " +
						"count(case when date_part('year', age(" + table_schema.getColumn("", "child_care_service_visitdate") + ", " + table_schema.getColumn("", "clientmap_dob") + ")) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=")+ " and '25'=ANY(string_to_array(" + table_schema.getColumn("", "child_care_service_other2") + ", ',')) then 1 else null end) as \"other_disease_1-5year_female\" " +
						"FROM " + table_schema.getTable("table_child_care_service") + " " +
						"JOIN " + table_schema.getTable("table_clientmap") + " on " + table_schema.getColumn("table", "clientmap_generatedid") + "=" + table_schema.getColumn("table", "child_care_service_healthid") + " " +
						"left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "child_care_service_providerid") + " " +
						"left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " " +
						"where " + query_condition + " " +
						"group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "child_care_service_providerid") + "  " +
						"where (case when count_provider>1 then " +
						"(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("table", "child_care_service_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("table", "child_care_service_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
						"else " + table_schema.getColumn("table", "child_care_service_visitdate", new String[]{start_date, end_date}, "between") + " end) " +
						"and ";
				if(!associated_id.isEmpty())
					sql_child_care_service += "((" + query_condition +") or " + table_schema.getColumn("table", "child_care_service_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_child_care_service += query_condition + " ";
//					if (!submission_date.equals(""))
//						sql_gp += "and " + table_schema.getColumn("table", "gpservice_systementrydate", new String[]{submission_date}, "<") + " ";

				sql_child_care_service += "GROUP BY " + table_schema.getColumn("table", "child_care_service_providerid") + "," + table_schema.getColumn("table", "providerdb_zillaid") + "," + table_schema.getColumn("table", "providerdb_upazilaid") + "," + table_schema.getColumn("table", "providerdb_unionid") + " ";

				System.out.println(sql_child_care_service);

				ccs = GetMIS3ResultSet.getFinalResult(sql_child_care_service, "notString", zilla, upazila);
				System.out.println(ccs);

				sql_child_care_service_detail = "select  " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"92"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"refer_accepted_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"92"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"refer_accepted_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"92"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"refer_accepted_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"92"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"refer_accepted_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"95"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"fst_dose_injection_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"95"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"fst_dose_injection_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"95"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"fst_dose_injection_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"95"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"fst_dose_injection_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"98"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"second_dose_injection_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"98"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"second_dose_injection_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"98"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"second_dose_injection_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"98"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"second_dose_injection_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_0-28days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 0 and 28 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_0-28days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_29-59days_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + " between 29 and 59 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_29-59days_female\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(visitdate, dob))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_2month-1year_male\", " +
						"count(case when " + table_schema.getColumn("", "child_care_service_visitdate") + "-" + table_schema.getColumn("", "clientmap_dob") + ">59 and date_part('year', age(visitdate, dob))<1 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_2month-1year_female\", " +
						"count(case when date_part('year', age(visitdate, dob)) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"1"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_1-5year_male\", " +
						"count(case when date_part('year', age(visitdate, dob)) between 1 and 5 and " + table_schema.getColumn("table", "clientmap_gender", new String[]{"2"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputid", new String[]{"91"}, "=") + " and " + table_schema.getColumn("table", "child_care_service_detail_inputvalue", new String[]{"1"}, "=") + " then 1 else null end) as \"imci_referred_1-5year_female\" " +
						"FROM " + table_schema.getTable("table_child_care_service") + " " +
						"JOIN " + table_schema.getTable("table_child_care_service_detail") + " on " + table_schema.getColumn("table", "child_care_service_healthid") + "=" + table_schema.getColumn("table", "child_care_service_detail_healthid") + " and " + table_schema.getColumn("table", "child_care_service_systementrydate") + "=" + table_schema.getColumn("table", "child_care_service_detail_entrydate") + " " +
						"JOIN " + table_schema.getTable("table_clientmap") + " on " + table_schema.getColumn("table", "clientmap_generatedid") + "=" + table_schema.getColumn("table", "child_care_service_healthid") + " " +
						"left join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "child_care_service_providerid") + " " +
						"left join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " " +
						"where " + query_condition + " " +
						"group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "child_care_service_providerid") + "  " +
						"where (case when count_provider>1 then " +
						"(case when " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnotnull") + " then " + table_schema.getColumn("table", "child_care_service_visitdate") + " between '" + start_date + "' AND " + table_schema.getColumn("", "providerdb_exdate") + "::date else " + table_schema.getColumn("table", "child_care_service_visitdate") + " between " + table_schema.getColumn("", "providerdb_endate") + "::date and '" + end_date + "' end) " +
						"else " + table_schema.getColumn("table", "child_care_service_visitdate", new String[]{start_date, end_date}, "between") + " end) " +
						"and ";
				if(!associated_id.isEmpty())
					sql_child_care_service_detail += "((" + query_condition +") or " + table_schema.getColumn("table", "child_care_service_providerid", new String[]{associated_id}, "in") + ") ";
				else
					sql_child_care_service_detail += query_condition + " ";

				System.out.println(sql_child_care_service_detail);

				ccsd = GetMIS3ResultSet.getFinalResult(sql_child_care_service_detail, "notString", zilla, upazila);
				System.out.println(ccsd);

				//System.out.println(lmis);

				/*System.out.println(anc);
				System.out.println(pac);
				System.out.println(pnc_mother);
				System.out.println(pnc_child);
				System.out.println(pnc_newborn_refer);
				System.out.println(delivery);*/

				String pill_new_center="0", pill_new_satelite="0", pill_old_center="0", pill_old_satelite="0", condom_new_center="0", condom_new_satelite="0", condom_old_center="0", condom_old_satelite="0", pill_shukhi_total="0", pill_apon_total="0", condom_total="0";
				String pill_ppfp_new_center="0", pill_ppfp_new_satelite="0", pill_ppfp_old_center="0", pill_ppfp_old_satelite="0", condom_ppfp_new_center="0", condom_ppfp_new_satelite="0", condom_ppfp_old_center="0", condom_ppfp_old_satelite="0";
				String pill_ppfp_expire_center="0",pill_ppfp_expire_satelite="0",condom_ppfp_expire_center="0",condom_ppfp_expire_satelite="0";

				String new_inject_center="0", new_inject_satelite="0", old_inject_center="0", old_inject_satelite="0", sideEffect_inj_center="0", sideEffect_inj_satelite="0";
				String new_inject_ppfp_center="0", new_inject_ppfp_satelite="0", old_inject_ppfp_center="0", old_inject_ppfp_satelite="0";
				String inject_ppfp_expire_center="0",inject_ppfp_expire_satelite="0";

				String iud_after_delivery_center="0", iud_after_delivery_satelite="0", iud_normal_center="0", iud_normal_satelite="0", before_time_finish_center="0", before_time_finish_satelite="0", after_time_finish_center="0", after_time_finish_satelite="0";
				String iud_ppfp_expire_center="0",iud_ppfp_expire_satelite="0";

				String iud_followup_center="0", iud_followup_satelite="0", iud_sideeffect_center="0", iud_sideeffect_satelite="0";

				String implant_after_delivery="0", implant_new="0", implant_before_time_finish="0", implant_after_time_finish="0";
				String implant_ppfp_expire_center="0",implant_ppfp_expire_satelite="0";

				String implant_followup_center="0", implant_followup_satelite="0", implant_sideeffect_center="0", implant_sideeffect_satelite="0";

				String anc1center="0", anc2center="0", anc3center="0", anc4center="0", ancrefercenter="0";
				String anc1satelite="0", anc2satelite="0", anc3satelite="0", anc4satelite="0", ancrefersatelite="0";
				String ancmgso4center="0", ancmgso4satelite="0",ancmisoprostolcenter="0", ancmisoprostolsatelite="0", anchighpreeclampsiacenter="0", anchighpreeclampsiasatelite;

				String normaldelivery="0", csectiondelivery="0", amtsl="0", livebirth="0", stillbirth="0", deliveryrefercenter="0", deliveryrefersatelite="0", deliverymisoprostol = "0", deliverycxytocin = "0";

				String pacservicecount = "0";
				String pacservicecountsatelite = "0";

				String pncmother1center="0", pncmother2center="0", pncmother3center="0", pncmother4center="0", pncmotherfpcenter="0", pncmotherrefercenter="0";
				String pncmother1satelite="0", pncmother2satelite="0", pncmother3satelite="0", pncmother4satelite="0", pncmotherfpsatelite="0", pncmotherrefersatelite="0";

				String pncchild1center="0", pncchild2center="0", pncchild3center="0", pncchild4center="0";
				String pncchild1satelite="0", pncchild2satelite="0", pncchild3satelite="0", pncchild4satelite="0";

				String newbornreferalcenter = "0", newbornreferalsatelite="0";

				String maternal_death = "0";

				String dryingafterbirth = "0", chlorehexidin = "0", skintouch = "0", breastfeed = "0", bagnmask = "0", lowbirthweight = "0", immaturebirth = "0";

				String gp_male_center="0", gp_female_center="0", gp_female_satelite="0", gp_ifa_center="0", gp_ifa_satelite="0", gp_rta_sta_center="0", gp_rta_sta_satelite="0", gp_rta_sta_all_center="0", gp_rta_sta_all_satelite="0";

				String anc1text="", anc2text="", anc3text="", anc4text="", pncm1text="", pncm2text="", pncm3text="", pncm4text="", pncn1text="", pncn2text="", pncn3text="", pncn4text="";

				String less_weight_2500_child_center="0",less_weight_2500_child_satelite="0",less_weight_2000_child_center="0",less_weight_2000_child_satelite="0",immaturebirth_center="0",immaturebirth_satelite="0";

				// Coded By Evana: 20/01/2019
				String ancadvicecenter="0", ancadvicesatelite="0";

				if(mis3ViewType.equals("2")) {
					anc1text = "পরিদর্শন - ১ (৪ মাসের মধ্যে)";
					anc2text = "পরিদর্শন - ২ (৬ মাসের মধ্যে)";
					anc3text = "পরিদর্শন - ৩ (৮ মাসের মধ্যে)";
					anc4text = "পরিদর্শন - ৪ (৯ মাসে)";

					pncm1text = "পরিদর্শন - ১ (২৪ ঘন্টার মধ্যে)";
					pncm2text = "পরিদর্শন - ২ (২-৩ দিনের মধ্যে)";
					pncm3text = "পরিদর্শন - ৩ (৭-১৪ দিনের মধ্যে)";
					pncm4text = "পরিদর্শন - ৪ (৪২-৪৮ দিনের মধ্যে)";

					pncn1text = "পরিদর্শন - ১ (২৪ ঘন্টার মধ্যে)";
					pncn2text = "পরিদর্শন - ২ (২-৩ দিনের মধ্যে)";
					pncn3text = "পরিদর্শন - ৩ (৭-১৪ দিনের মধ্যে)";
					pncn4text = "পরিদর্শন - ৪ (৪২-৪৮ দিনের মধ্যে)";
				}
				else {
					anc1text = "পরিদর্শন - ১";
					anc2text = "পরিদর্শন - ২";
					anc3text = "পরিদর্শন - ৩";
					anc4text = "পরিদর্শন - ৪";

					pncm1text = "পরিদর্শন - ১";
					pncm2text = "পরিদর্শন - ২";
					pncm3text = "পরিদর্শন - ৩";
					pncm4text = "পরিদর্শন - ৪";

					pncn1text = "পরিদর্শন - ১";
					pncn2text = "পরিদর্শন - ২";
					pncn3text = "পরিদর্শন - ৩";
					pncn4text = "পরিদর্শন - ৪";
				}

				if(!csba.equals("1")) {
					if (!pill_condom.isEmpty()) {
						pill_new_center = pill_condom.get("pill_new_center");
						pill_new_satelite = pill_condom.get("pill_new_satelite");
						pill_old_center = pill_condom.get("pill_old_center");
						pill_old_satelite = pill_condom.get("pill_old_satelite");
						condom_new_center = pill_condom.get("condom_new_center");
						condom_new_satelite = pill_condom.get("condom_new_satelite");
						condom_old_center = pill_condom.get("condom_old_center");
						condom_old_satelite = pill_condom.get("condom_old_satelite");

						if(DateOption>2018) {
							pill_ppfp_new_center = pill_condom.get("pill_ppfp_new_center");
							pill_ppfp_new_satelite = pill_condom.get("pill_ppfp_new_satelite");
							pill_ppfp_old_center = pill_condom.get("pill_ppfp_old_center");
							pill_ppfp_old_satelite = pill_condom.get("pill_ppfp_old_satelite");
							condom_ppfp_new_center = pill_condom.get("condom_ppfp_new_center");
							condom_ppfp_new_satelite = pill_condom.get("condom_ppfp_new_satelite");
							condom_ppfp_old_center = pill_condom.get("condom_ppfp_old_center");
							condom_ppfp_old_satelite = pill_condom.get("condom_ppfp_old_satelite");

							pill_ppfp_expire_center = pill_condom.get("pill_ppfp_expire_center");
							pill_ppfp_expire_satelite = pill_condom.get("pill_ppfp_expire_satelite");
							condom_ppfp_expire_center = pill_condom.get("condom_ppfp_expire_center");
							condom_ppfp_expire_satelite = pill_condom.get("condom_ppfp_expire_satelite");
						}

						pill_shukhi_total = pill_condom.get("pill_shukhi_total");
						pill_apon_total = pill_condom.get("pill_apon_total");
						condom_total = pill_condom.get("condom_total");

						mis3_json.put("pill_new_center", pill_condom.get("pill_new_center"));
						mis3_json.put("pill_new_satelite", pill_condom.get("pill_new_satelite"));
						mis3_json.put("pill_old_center", pill_condom.get("pill_old_center"));
						mis3_json.put("pill_old_satelite", pill_condom.get("pill_old_satelite"));
						mis3_json.put("condom_new_center", pill_condom.get("condom_new_center"));
						mis3_json.put("condom_new_satelite", pill_condom.get("condom_new_satelite"));
						mis3_json.put("condom_old_center", pill_condom.get("condom_old_center"));
						mis3_json.put("condom_old_satelite", pill_condom.get("condom_old_satelite"));

						if(DateOption>2018) {
							mis3_json.put("pill_ppfp_new_center", pill_condom.get("pill_ppfp_new_center"));
							mis3_json.put("pill_ppfp_new_satelite", pill_condom.get("pill_ppfp_new_satelite"));
							mis3_json.put("pill_ppfp_old_center", pill_condom.get("pill_ppfp_old_center"));
							mis3_json.put("pill_ppfp_old_satelite", pill_condom.get("pill_ppfp_old_satelite"));
							mis3_json.put("condom_ppfp_new_center", pill_condom.get("condom_ppfp_new_center"));
							mis3_json.put("condom_ppfp_new_satelite", pill_condom.get("condom_ppfp_new_satelite"));
							mis3_json.put("condom_ppfp_old_center", pill_condom.get("condom_ppfp_old_center"));
							mis3_json.put("condom_ppfp_old_satelite", pill_condom.get("condom_ppfp_old_satelite"));

							mis3_json.put("pill_ppfp_expire_center", pill_condom.get("pill_ppfp_expire_center"));
							mis3_json.put("pill_ppfp_expire_satelite", pill_condom.get("pill_ppfp_expire_satelite"));
							mis3_json.put("condom_ppfp_expire_center", pill_condom.get("condom_ppfp_expire_center"));
							mis3_json.put("condom_ppfp_expire_satelite", pill_condom.get("condom_ppfp_expire_satelite"));
						}

						mis3_json.put("pill_shukhi_total", pill_condom.get("pill_shukhi_total"));
						mis3_json.put("pill_apon_total", pill_condom.get("pill_apon_total"));
						mis3_json.put("condom_total", pill_condom.get("condom_total"));
					}
					else{
						mis3_json.put("pill_new_center", "0");
						mis3_json.put("pill_new_satelite", "0");
						mis3_json.put("pill_old_center", "0");
						mis3_json.put("pill_old_satelite", "0");
						mis3_json.put("condom_new_center", "0");
						mis3_json.put("condom_new_satelite", "0");
						mis3_json.put("condom_old_center", "0");
						mis3_json.put("condom_old_satelite", "0");

						if(DateOption>2018) {
							mis3_json.put("pill_ppfp_new_center", "0");
							mis3_json.put("pill_ppfp_new_satelite", "0");
							mis3_json.put("pill_ppfp_old_center", "0");
							mis3_json.put("pill_ppfp_old_satelite", "0");
							mis3_json.put("condom_ppfp_new_center", "0");
							mis3_json.put("condom_ppfp_new_satelite", "0");
							mis3_json.put("condom_ppfp_old_center", "0");
							mis3_json.put("condom_ppfp_old_satelite", "0");

							mis3_json.put("pill_ppfp_expire_center", "0");
							mis3_json.put("pill_ppfp_expire_satelite", "0");
							mis3_json.put("condom_ppfp_expire_center", "0");
							mis3_json.put("condom_ppfp_expire_satelite", "0");
						}

						mis3_json.put("pill_shukhi_total", "0");
						mis3_json.put("pill_apon_total", "0");
						mis3_json.put("condom_total", "0");
					}

					if (!injectable.isEmpty()) {
						new_inject_center = injectable.get("new_inject_center");
						new_inject_satelite = injectable.get("new_inject_satelite");
						old_inject_center = injectable.get("old_inject_center");
						old_inject_satelite = injectable.get("old_inject_satelite");
						sideEffect_inj_center = injectable.get("sideEffect_inj_center");
						sideEffect_inj_satelite = injectable.get("sideEffect_inj_satelite");

						if(DateOption>2018) {
							new_inject_ppfp_center = injectable.get("new_inject_ppfp_center");
							new_inject_ppfp_satelite = injectable.get("new_inject_ppfp_satelite");
							old_inject_ppfp_center = injectable.get("old_inject_ppfp_center");
							old_inject_ppfp_satelite = injectable.get("old_inject_ppfp_satelite");

							inject_ppfp_expire_center = injectable.get("inject_ppfp_expire_center");
							inject_ppfp_expire_satelite = injectable.get("inject_ppfp_expire_satelite");
						}

						mis3_json.put("new_inject_center", injectable.get("new_inject_center"));
						mis3_json.put("new_inject_satelite", injectable.get("new_inject_satelite"));
						mis3_json.put("old_inject_center", injectable.get("old_inject_center"));
						mis3_json.put("old_inject_satelite", injectable.get("old_inject_satelite"));

						if(DateOption>2018) {
							mis3_json.put("new_inject_ppfp_center", injectable.get("new_inject_ppfp_center"));
							mis3_json.put("new_inject_ppfp_satelite", injectable.get("new_inject_ppfp_satelite"));
							mis3_json.put("old_inject_ppfp_center", injectable.get("old_inject_ppfp_center"));
							mis3_json.put("old_inject_ppfp_satelite", injectable.get("old_inject_ppfp_satelite"));

							mis3_json.put("inject_ppfp_expire_center", injectable.get("inject_ppfp_expire_center"));
							mis3_json.put("inject_ppfp_expire_satelite", injectable.get("inject_ppfp_expire_satelite"));
						}

						mis3_json.put("sideEffect_inj_center", injectable.get("sideEffect_inj_center"));
						mis3_json.put("sideEffect_inj_satelite", injectable.get("sideEffect_inj_satelite"));
					}
					else{
						mis3_json.put("new_inject_center", "0");
						mis3_json.put("new_inject_satelite", "0");
						mis3_json.put("old_inject_center", "0");
						mis3_json.put("old_inject_satelite", "0");

						if(DateOption>2018) {
							mis3_json.put("new_inject_ppfp_center", "0");
							mis3_json.put("new_inject_ppfp_satelite", "0");
							mis3_json.put("old_inject_ppfp_center", "0");
							mis3_json.put("old_inject_ppfp_satelite", "0");

							mis3_json.put("inject_ppfp_expire_center", "0");
							mis3_json.put("inject_ppfp_expire_satelite", "0");
						}

						mis3_json.put("sideEffect_inj_center", "0");
						mis3_json.put("sideEffect_inj_satelite", "0");
					}

					if (!iud.isEmpty()) {
						iud_after_delivery_center = iud.get("iud_after_delivery_center");
						iud_after_delivery_satelite = iud.get("iud_after_delivery_satelite");
						iud_normal_center = iud.get("iud_normal_center");
						iud_normal_satelite = iud.get("iud_normal_satelite");
						before_time_finish_center = iud.get("before_time_finish_center");
						before_time_finish_satelite = iud.get("before_time_finish_satelite");
						after_time_finish_center = iud.get("after_time_finish_center");
						after_time_finish_satelite = iud.get("after_time_finish_satelite");

						if(DateOption>2018) {
							iud_ppfp_expire_center = iud.get("iud_ppfp_expire_center");
							iud_ppfp_expire_satelite = iud.get("iud_ppfp_expire_satelite");
						}

						mis3_json.put("iud_after_delivery_center", iud.get("iud_after_delivery_center"));
						mis3_json.put("iud_after_delivery_satelite", iud.get("iud_after_delivery_satelite"));
						mis3_json.put("iud_normal_center", iud.get("iud_normal_center"));
						mis3_json.put("iud_normal_satelite", iud.get("iud_normal_satelite"));
						mis3_json.put("before_time_finish_center", iud.get("before_time_finish_center"));
						mis3_json.put("before_time_finish_satelite", iud.get("before_time_finish_satelite"));
						mis3_json.put("after_time_finish_center", iud.get("after_time_finish_center"));
						mis3_json.put("after_time_finish_satelite", iud.get("after_time_finish_satelite"));

						if(DateOption>2018) {
							mis3_json.put("iud_ppfp_expire_center", iud.get("iud_ppfp_expire_center"));
							mis3_json.put("iud_ppfp_expire_satelite", iud.get("iud_ppfp_expire_satelite"));
						}
					}
					else{
						mis3_json.put("iud_after_delivery_center", "0");
						mis3_json.put("iud_after_delivery_satelite", "0");
						mis3_json.put("iud_normal_center", "0");
						mis3_json.put("iud_normal_satelite", "0");
						mis3_json.put("before_time_finish_center", "0");
						mis3_json.put("before_time_finish_satelite", "0");
						mis3_json.put("after_time_finish_center", "0");
						mis3_json.put("after_time_finish_satelite", "0");

						if(DateOption>2018) {
							mis3_json.put("iud_ppfp_expire_center", "0");
							mis3_json.put("iud_ppfp_expire_satelite", "0");
						}
					}

					if (!iud_followup.isEmpty()) {
						iud_followup_center = iud_followup.get("iud_followup_center");
						iud_followup_satelite = iud_followup.get("iud_followup_satelite");
						iud_sideeffect_center = iud_followup.get("iud_sideeffect_center");
						iud_sideeffect_satelite = iud_followup.get("iud_sideeffect_satelite");

						mis3_json.put("iud_followup_center", iud_followup.get("iud_followup_center"));
						mis3_json.put("iud_followup_satelite", iud_followup.get("iud_followup_satelite"));
						mis3_json.put("iud_sideeffect_center", iud_followup.get("iud_sideeffect_center"));
						mis3_json.put("iud_sideeffect_satelite", iud_followup.get("iud_sideeffect_satelite"));
					}
					else{
						mis3_json.put("iud_followup_center", "0");
						mis3_json.put("iud_followup_satelite", "0");
						mis3_json.put("iud_sideeffect_center", "0");
						mis3_json.put("iud_sideeffect_satelite", "0");
					}

					if (!implant.isEmpty()) {
						implant_after_delivery = implant.get("implant_after_delivery");
						implant_new = implant.get("implant_new");
						implant_before_time_finish = implant.get("implant_before_time_finish");
						implant_after_time_finish = implant.get("implant_after_time_finish");

						if(DateOption>2018) {
							implant_ppfp_expire_center = implant.get("implant_ppfp_expire_center");
							implant_ppfp_expire_satelite = implant.get("implant_ppfp_expire_satelite");
						}


						mis3_json.put("implant_after_delivery", implant.get("implant_after_delivery"));
						mis3_json.put("implant_new", implant.get("implant_new"));
						mis3_json.put("implant_before_time_finish", implant.get("implant_before_time_finish"));
						mis3_json.put("implant_after_time_finish", implant.get("implant_after_time_finish"));
						mis3_json.put("implant_implanon", implant.get("implant_implanon"));
						mis3_json.put("implant_jadelle", implant.get("implant_jadelle"));

						if(DateOption>2018) {
							mis3_json.put("implant_ppfp_expire_center", implant.get("implant_ppfp_expire_center"));
							mis3_json.put("implant_ppfp_expire_satelite", implant.get("implant_ppfp_expire_satelite"));
						}
					}
					else{
						mis3_json.put("implant_after_delivery", "0");
						mis3_json.put("implant_new", "0");
						mis3_json.put("implant_before_time_finish", "0");
						mis3_json.put("implant_after_time_finish", "0");
						mis3_json.put("implant_implanon", "0");
						mis3_json.put("implant_jadelle", "0");

						if(DateOption>2018) {
							mis3_json.put("implant_ppfp_expire_center", "0");
							mis3_json.put("implant_ppfp_expire_satelite", "0");
						}
					}

					if (!implant_followup.isEmpty()) {
						implant_followup_center = implant_followup.get("implant_followup_center");
						implant_followup_satelite = implant_followup.get("implant_followup_satelite");
						implant_sideeffect_center = implant_followup.get("implant_sideeffect_center");
						implant_sideeffect_satelite = implant_followup.get("implant_sideeffect_satelite");

						mis3_json.put("implant_followup_center", implant_followup.get("implant_followup_center"));
						mis3_json.put("implant_followup_satelite", implant_followup.get("implant_followup_satelite"));
						mis3_json.put("implant_sideeffect_center", implant_followup.get("implant_sideeffect_center"));
						mis3_json.put("implant_sideeffect_satelite", implant_followup.get("implant_sideeffect_satelite"));
					}
					else{
						mis3_json.put("implant_followup_center", "0");
						mis3_json.put("implant_followup_satelite", "0");
						mis3_json.put("implant_sideeffect_center", "0");
						mis3_json.put("implant_sideeffect_satelite", "0");
					}
				}

				if (!anc.isEmpty()) {
					anc1center = anc.get("anc1center");
					anc2center = anc.get("anc2center");
					anc3center = anc.get("anc3center");
					anc4center = anc.get("anc4center");

					anc1satelite = anc.get("anc1satelite");
					anc2satelite = anc.get("anc2satelite");
					anc3satelite = anc.get("anc3satelite");
					anc4satelite = anc.get("anc4satelite");

					ancrefercenter = anc.get("ancrefercenter");
					ancrefersatelite = anc.get("ancrefersatelite");

					ancmgso4center = anc.get("ancmgso4center");
					ancmgso4satelite = anc.get("ancmgso4satelite");

					ancmisoprostolcenter = anc.get("ancmisoprostolcenter");
					ancmisoprostolsatelite = anc.get("ancmisoprostolsatelite");

					anchighpreeclampsiacenter = anc.get("anchighpreeclampsiacenter");
					anchighpreeclampsiasatelite = anc.get("anchighpreeclampsiasatelite");

					mis3_json.put("anc1center", anc.get("anc1center"));
					mis3_json.put("anc2center", anc.get("anc2center"));
					mis3_json.put("anc3center", anc.get("anc3center"));
					mis3_json.put("anc4center", anc.get("anc4center"));
					mis3_json.put("anc1satelite", anc.get("anc1satelite"));
					mis3_json.put("anc2satelite", anc.get("anc2satelite"));
					mis3_json.put("anc3satelite", anc.get("anc3satelite"));
					mis3_json.put("anc4satelite", anc.get("anc4satelite"));
					mis3_json.put("ancrefercenter", anc.get("ancrefercenter"));
					mis3_json.put("ancrefersatelite", anc.get("ancrefersatelite"));
					mis3_json.put("ancmgso4center", anc.get("ancmgso4center"));
					mis3_json.put("ancmgso4satelite", anc.get("ancmgso4satelite"));
					mis3_json.put("ancmisoprostolcenter", anc.get("ancmisoprostolcenter"));
					mis3_json.put("ancmisoprostolsatelite", anc.get("ancmisoprostolsatelite"));
					mis3_json.put("ancAntinatalKorticenter",anc.get("ancAntinatalKorticenter"));
					mis3_json.put("ancAntinatalKortisatelite",anc.get("ancAntinatalKortisatelite"));
					mis3_json.put("anchighpreeclampsiacenter", anc.get("anchighpreeclampsiacenter"));
					mis3_json.put("anchighpreeclampsiasatelite", anc.get("anchighpreeclampsiasatelite"));



					//@Author: Evana
					// @Dated: 21/05/2019

					mis3_json.put("ancAPHcenter", anc.get("ancAPHcenter"));
					mis3_json.put("ancAPHsatelite", anc.get("ancAPHsatelite"));

					// Coded By Evana: 20/01/2019
					mis3_json.put("ancadvicecenter",anc.get("ancadvicecenter"));
					mis3_json.put("ancadvicesatelite", anc.get("ancadvicesatelite"));

					// @Author: Evana: Date: 29/05/2019

					mis3_json.put("ancprgwomanayronfoliccenter", anc.get("ancprgwomanayronfoliccenter"));
					mis3_json.put("ancprgwomanayronfolicsatelite", anc.get("ancprgwomanayronfolicsatelite"));
					mis3_json.put("ancmotherweightcenter", anc.get("ancmotherweightcenter"));
					mis3_json.put("ancmotherweightsatelite", anc.get("ancmotherweightsatelite"));

					System.out.println("ancmotherweightCenter="+anc.get("ancmotherweightcenter"));
				}
				else{
					mis3_json.put("anc1center", "0");
					mis3_json.put("anc2center", "0");
					mis3_json.put("anc3center", "0");
					mis3_json.put("anc4center", "0");
					mis3_json.put("anc1satelite", "0");
					mis3_json.put("anc2satelite", "0");
					mis3_json.put("anc3satelite", "0");
					mis3_json.put("anc4satelite", "0");
					mis3_json.put("ancrefercenter", "0");
					mis3_json.put("ancrefersatelite", "0");
					mis3_json.put("ancmgso4center", "0");
					mis3_json.put("ancmgso4satelite", "0");
					mis3_json.put("ancmisoprostolcenter", "0");
					mis3_json.put("ancmisoprostolsatelite", "0");
					mis3_json.put("ancAntinatalKorticenter", "0");
					mis3_json.put("ancAntinatalKortisatelite", "0");
					mis3_json.put("anchighpreeclampsiacenter", "0");
					mis3_json.put("anchighpreeclampsiasatelite", "0");

					//Coded By Evana: 20/01/2019

					mis3_json.put("ancadvicecenter","0");
					mis3_json.put("ancadvicesatelite", "0");

					//	@Author: Evana
					//  @Dated: 29/05/2019
					mis3_json.put("ancprgwomanayronfoliccenter", "0");
					mis3_json.put("ancprgwomanayronfolicsatelite", "0");
					mis3_json.put("ancmotherweightcenter", "0");
					mis3_json.put("ancmotherweightsatelite", "0");

				}

				/****
				 * @author Evana
				 * @Date: 22/05/2019
				 * Permanent Method
				 * */

				if(!pms.isEmpty())
				{
					mis3_json.put("permanent_method_normal_male", pms.get("permanent_method_normal_male"));
					mis3_json.put("permanent_method_after_delivery_male", pms.get("permanent_method_after_delivery_male"));
					mis3_json.put("permanent_method_normal_female", pms.get("permanent_method_normal_female"));
					mis3_json.put("permanent_method_after_delivery_female", pms.get("permanent_method_after_delivery_female"));
				}
				else
				{
					mis3_json.put("permanent_method_normal_male", "0");
					mis3_json.put("permanent_method_after_delivery_male", "0");
					mis3_json.put("permanent_method_normal_female", "0");
					mis3_json.put("permanent_method_after_delivery_female", "0");

				}

				if(!pms_followup.isEmpty())
				{
					mis3_json.put("permanent_method_followup_male_center", pnc_mother.get("permanent_method_followup_male_center"));
					mis3_json.put("permanent_method_followup_female_center", pnc_mother.get("permanent_method_followup_female_center"));
					mis3_json.put("permanent_method_complication_male", pnc_mother.get("permanent_method_complication_male"));
					mis3_json.put("permanent_method_complication_female", pnc_mother.get("permanent_method_complication_female"));
				}
				else
				{
					mis3_json.put("permanent_method_followup_male_center", "0");
					mis3_json.put("permanent_method_followup_female_center", "0");
					mis3_json.put("permanent_method_complication_male", "0");
					mis3_json.put("permanent_method_complication_female", "0");

				}

				if (!pnc_mother.isEmpty()) {
					pncmother1center = pnc_mother.get("pncmother1center");
					pncmother2center = pnc_mother.get("pncmother2center");
					pncmother3center = pnc_mother.get("pncmother3center");
					pncmother4center = pnc_mother.get("pncmother4center");

					pncmother1satelite = pnc_mother.get("pncmother1satelite");
					pncmother2satelite = pnc_mother.get("pncmother2satelite");
					pncmother3satelite = pnc_mother.get("pncmother3satelite");
					pncmother4satelite = pnc_mother.get("pncmother4satelite");

					pncmotherfpcenter = pnc_mother.get("pncmotherfpcenter");
					pncmotherfpsatelite = pnc_mother.get("pncmotherfpsatelite");

					pncmotherrefercenter = pnc_mother.get("pncmotherrefercenter");
					pncmotherrefersatelite = pnc_mother.get("pncmotherrefersatelite");

					mis3_json.put("pncmother1center", pnc_mother.get("pncmother1center"));
					mis3_json.put("pncmother2center", pnc_mother.get("pncmother2center"));
					mis3_json.put("pncmother3center", pnc_mother.get("pncmother3center"));
					mis3_json.put("pncmother4center", pnc_mother.get("pncmother4center"));
					mis3_json.put("pncmother1satelite", pnc_mother.get("pncmother1satelite"));
					mis3_json.put("pncmother2satelite", pnc_mother.get("pncmother2satelite"));
					mis3_json.put("pncmother3satelite", pnc_mother.get("pncmother3satelite"));
					mis3_json.put("pncmother4satelite", pnc_mother.get("pncmother4satelite"));
					mis3_json.put("pncmotherfpcenter", pnc_mother.get("pncmotherfpcenter"));
					mis3_json.put("pncmotherfpsatelite", pnc_mother.get("pncmotherfpsatelite"));
					mis3_json.put("pncmotherrefercenter", pnc_mother.get("pncmotherrefercenter"));
					mis3_json.put("pncmotherrefersatelite", pnc_mother.get("pncmotherrefersatelite"));

					mis3_json.put("pncmotherPPHcenter", pnc_mother.get("pncmotherPPHcenter"));
					mis3_json.put("pncmotherPPHsatelite", pnc_mother.get("pncmotherPPHcenter"));

				}
				else{
					mis3_json.put("pncmother1center", "0");
					mis3_json.put("pncmother2center", "0");
					mis3_json.put("pncmother3center", "0");
					mis3_json.put("pncmother4center", "0");
					mis3_json.put("pncmother1satelite", "0");
					mis3_json.put("pncmother2satelite", "0");
					mis3_json.put("pncmother3satelite", "0");
					mis3_json.put("pncmother4satelite", "0");
					mis3_json.put("pncmotherfpcenter", "0");
					mis3_json.put("pncmotherfpsatelite", "0");
					mis3_json.put("pncmotherrefercenter", "0");
					mis3_json.put("pncmotherrefersatelite", "0");
					mis3_json.put("pncmotherPPHcenter", "0");
					mis3_json.put("pncmotherPPHsatelite", "0");
				}

				if (!pnc_child.isEmpty()) {
					pncchild1center = pnc_child.get("pncchild1center");
					pncchild2center = pnc_child.get("pncchild2center");
					pncchild3center = pnc_child.get("pncchild3center");
					pncchild4center = pnc_child.get("pncchild4center");

					pncchild1satelite = pnc_child.get("pncchild1satelite");
					pncchild2satelite = pnc_child.get("pncchild2satelite");
					pncchild3satelite = pnc_child.get("pncchild3satelite");
					pncchild4satelite = pnc_child.get("pncchild4satelite");

					mis3_json.put("pncchild1center", pnc_child.get("pncchild1center"));
					mis3_json.put("pncchild2center", pnc_child.get("pncchild2center"));
					mis3_json.put("pncchild3center", pnc_child.get("pncchild3center"));
					mis3_json.put("pncchild4center", pnc_child.get("pncchild4center"));
					mis3_json.put("pncchild1satelite", pnc_child.get("pncchild1satelite"));
					mis3_json.put("pncchild2satelite", pnc_child.get("pncchild2satelite"));
					mis3_json.put("pncchild3satelite", pnc_child.get("pncchild3satelite"));
					mis3_json.put("pncchild4satelite", pnc_child.get("pncchild4satelite"));
				}
				else{
					mis3_json.put("pncchild1center", "0");
					mis3_json.put("pncchild2center", "0");
					mis3_json.put("pncchild3center", "0");
					mis3_json.put("pncchild4center", "0");
					mis3_json.put("pncchild1satelite", "0");
					mis3_json.put("pncchild2satelite", "0");
					mis3_json.put("pncchild3satelite", "0");
					mis3_json.put("pncchild4satelite", "0");
				}

				if (!delivery.isEmpty()) {
					normaldelivery=delivery.get("normaldelivery");
					csectiondelivery=delivery.get("csectiondelivery");
					amtsl=delivery.get("amtsl");
					livebirth=delivery.get("livebirth");
					stillbirth=delivery.get("stillbirth");

					deliveryrefercenter=delivery.get("deliveryrefercenter");
					deliveryrefersatelite=delivery.get("deliveryrefersatelite");

					deliverymisoprostol = delivery.get("deliverymisoprostol");
					deliverycxytocin = "0";//delivery.get("deliverycxytocin");

					mis3_json.put("normaldelivery", delivery.get("normaldelivery"));
					mis3_json.put("csectiondelivery", delivery.get("csectiondelivery"));
					mis3_json.put("amtsl", delivery.get("amtsl"));
					mis3_json.put("livebirth", delivery.get("livebirth"));
					mis3_json.put("stillbirth", delivery.get("stillbirth"));
					mis3_json.put("deliveryrefercenter", delivery.get("deliveryrefercenter"));
					mis3_json.put("deliveryrefersatelite", delivery.get("deliveryrefersatelite"));
					mis3_json.put("deliverymisoprostol", delivery.get("deliverymisoprostol"));
					mis3_json.put("deliverycxytocin", "0");
					mis3_json.put("deliveryIPHcenter", delivery.get("deliveryIPHcenter"));

					if(DateOption>2018) {
						mis3_json.put("livebirth_center", delivery.get("livebirth_center"));
						mis3_json.put("livebirth_satelite", delivery.get("livebirth_satelite"));
					}
				}
				else{
					mis3_json.put("normaldelivery", "0");
					mis3_json.put("csectiondelivery", "0");
					mis3_json.put("amtsl", "0");
					mis3_json.put("livebirth", "0");
					mis3_json.put("stillbirth", "0");
					mis3_json.put("deliveryrefercenter", "0");
					mis3_json.put("deliveryrefersatelite", "0");
					mis3_json.put("deliverymisoprostol", "0");
					mis3_json.put("deliverycxytocin", "0");
					mis3_json.put("deliveryIPHcenter", "0");

					if(DateOption>2018) {
						mis3_json.put("livebirth_center", "0");
						mis3_json.put("livebirth_satelite", "0");
					}
				}

				if (!pac.isEmpty()) {
					pacservicecount=pac.get("servicecount");

					mis3_json.put("pacservicecount", pac.get("servicecount"));
					mis3_json.put("pacservicecountsatelite", pac.get("pacservicecountsatelite"));

				}
				else{
					mis3_json.put("pacservicecount", "0");
					mis3_json.put("pacservicecountsatelite", "0");
				}

				if (!pnc_newborn_refer.isEmpty()) {
					newbornreferalcenter=pnc_newborn_refer.get("pncnewbornReferCenter");
					newbornreferalsatelite=pnc_newborn_refer.get("pncnewbornReferSatelite");

					mis3_json.put("newbornreferalcenter", pnc_newborn_refer.get("pncnewbornReferCenter"));
					mis3_json.put("newbornreferalsatelite", pnc_newborn_refer.get("pncnewbornReferSatelite"));
				}
				else{
					mis3_json.put("newbornreferalcenter", "0");
					mis3_json.put("newbornreferalsatelite", "0");
				}

				if (!maternalDeath.isEmpty()) {
					maternal_death=maternalDeath.get("maternal_death");
					mis3_json.put("maternal_death", maternalDeath.get("maternal_death"));
					mis3_json.put("newborn_death", maternalDeath.get("newborn_death"));
				}
				else{
					mis3_json.put("maternal_death", "0");
					mis3_json.put("newborn_death", "0");
				}

				if (!newborn_info.isEmpty()) {
					dryingafterbirth=newborn_info.get("dryingafterbirth");
					chlorehexidin=newborn_info.get("chlorehexidin");
					skintouch=newborn_info.get("skintouch");
					breastfeed=newborn_info.get("breastfeed");
					bagnmask=newborn_info.get("bagnmask");
					lowbirthweight=newborn_info.get("lowbirthweight");
					immaturebirth=newborn_info.get("immaturebirth");

					mis3_json.put("dryingafterbirth", newborn_info.get("dryingafterbirth"));
					mis3_json.put("chlorehexidin", newborn_info.get("chlorehexidin"));
					mis3_json.put("skintouch", newborn_info.get("skintouch"));
					mis3_json.put("breastfeed", newborn_info.get("breastfeed"));
					mis3_json.put("bagnmask", newborn_info.get("bagnmask"));
					mis3_json.put("lowbirthweight", newborn_info.get("lowbirthweight"));
					mis3_json.put("immaturebirth", newborn_info.get("immaturebirth"));

					if(DateOption>2018) {
						mis3_json.put("less_weight_2500_child_center", newborn_info.get("less_weight_2500_child_center"));
						mis3_json.put("less_weight_2500_child_satelite", newborn_info.get("less_weight_2500_child_satelite"));
						mis3_json.put("less_weight_2000_child_center", newborn_info.get("less_weight_2000_child_center"));
						mis3_json.put("less_weight_2000_child_satelite", newborn_info.get("less_weight_2000_child_satelite"));
						mis3_json.put("immaturebirth_center", newborn_info.get("immaturebirth_center"));
						mis3_json.put("immaturebirth_satelite", newborn_info.get("immaturebirth_satelite"));
						mis3_json.put("stillbirth_fresh_center", newborn_info.get("stillbirth_fresh_center"));
						mis3_json.put("stillbirth_fresh_satelite", newborn_info.get("stillbirth_fresh_satelite"));
						mis3_json.put("stillbirth_macerated_center", newborn_info.get("stillbirth_macerated_center"));
						mis3_json.put("stillbirth_macerated_satelite", newborn_info.get("stillbirth_macerated_satelite"));
					}
				}
				else{
					mis3_json.put("dryingafterbirth", "0");
					mis3_json.put("chlorehexidin", "0");
					mis3_json.put("skintouch", "0");
					mis3_json.put("breastfeed", "0");
					mis3_json.put("bagnmask", "0");
					mis3_json.put("lowbirthweight", "0");
					mis3_json.put("immaturebirth", "0");

					if(DateOption>2018) {
						mis3_json.put("less_weight_2500_child_center", "0");
						mis3_json.put("less_weight_2500_child_satelite", "0");
						mis3_json.put("less_weight_2000_child_center", "0");
						mis3_json.put("less_weight_2000_child_satelite", "0");
						mis3_json.put("immaturebirth_center", "0");
						mis3_json.put("immaturebirth_satelite", "0");
						mis3_json.put("stillbirth_fresh_center", "0");
						mis3_json.put("stillbirth_fresh_satelite", "0");
						mis3_json.put("stillbirth_macerated_center", "0");
						mis3_json.put("stillbirth_macerated_satelite", "0");
					}
				}

				if(!ccs.isEmpty()){
					if(!query_from_gpservice){
						mis3_json.put("gp_outdoor_child_0_1_center ", ccs.get("gp_outdoor_child_0_1_center"));
						mis3_json.put("gp_outdoor_child_0_1_satelite ", ccs.get("gp_outdoor_child_0_1_satelite"));

						mis3_json.put("gp_outdoor_child_1_5_center", ccs.get("gp_outdoor_child_1_5_center"));
						mis3_json.put("gp_outdoor_child_1_5_satelite", ccs.get("gp_outdoor_child_1_5_satelite"));

					}
					mis3_json.put("very_serious_disease_0-28days_male", ccs.get("very_serious_disease_0-28days_male"));
					mis3_json.put("very_serious_disease_0-28days_female", ccs.get("very_serious_disease_0-28days_female"));
					mis3_json.put("very_serious_disease_29-59days_male", ccs.get("very_serious_disease_29-59days_male"));
					mis3_json.put("very_serious_disease_29-59days_female", ccs.get("very_serious_disease_29-59days_female"));
					mis3_json.put("very_serious_disease_2month-1year_male", ccs.get("very_serious_disease_2month-1year_male"));
					mis3_json.put("very_serious_disease_2month-1year_female", ccs.get("very_serious_disease_2month-1year_female"));
					mis3_json.put("very_serious_disease_1-5year_male", ccs.get("very_serious_disease_1-5year_male"));
					mis3_json.put("very_serious_disease_1-5year_female", ccs.get("very_serious_disease_1-5year_female"));
					mis3_json.put("very_serious_disease_infected_0-28days_male", ccs.get("very_serious_disease_infected_0-28days_male"));
					mis3_json.put("very_serious_disease_infected_0-28days_female", ccs.get("very_serious_disease_infected_0-28days_female"));
					mis3_json.put("very_serious_disease_infected_29-59days_male", ccs.get("very_serious_disease_infected_29-59days_male"));
					mis3_json.put("very_serious_disease_infected_29-59days_female", ccs.get("very_serious_disease_infected_29-59days_female"));
					mis3_json.put("very_serious_disease_infected_2month-1year_male", ccs.get("very_serious_disease_infected_2month-1year_male"));
					mis3_json.put("very_serious_disease_infected_2month-1year_female", ccs.get("very_serious_disease_infected_2month-1year_female"));
					mis3_json.put("very_serious_disease_infected_1-5year_male", ccs.get("very_serious_disease_infected_1-5year_male"));
					mis3_json.put("very_serious_disease_infected_1-5year_female", ccs.get("very_serious_disease_infected_1-5year_female"));
					mis3_json.put("very_serious_disease_neumonia_0-28days_male", ccs.get("very_serious_disease_neumonia_0-28days_male"));
					mis3_json.put("very_serious_disease_neumonia_0-28days_female", ccs.get("very_serious_disease_neumonia_0-28days_female"));
					mis3_json.put("very_serious_disease_neumonia_29-59days_male", ccs.get("very_serious_disease_neumonia_29-59days_male"));
					mis3_json.put("very_serious_disease_neumonia_29-59days_female", ccs.get("very_serious_disease_neumonia_29-59days_female"));
					mis3_json.put("pneumonia_2month-1year_male", ccs.get("pneumonia_2month-1year_male"));
					mis3_json.put("pneumonia_2month-1year_female", ccs.get("pneumonia_2month-1year_female"));
					mis3_json.put("pneumonia_1-5year_male", ccs.get("pneumonia_1-5year_male"));
					mis3_json.put("pneumonia_1-5year_female", ccs.get("pneumonia_1-5year_female"));
					mis3_json.put("influenja_2month-1year_male", ccs.get("influenja_2month-1year_male"));
					mis3_json.put("influenja_2month-1year_female", ccs.get("influenja_2month-1year_female"));
					mis3_json.put("influenja_1-5year_male", ccs.get("influenja_1-5year_male"));
					mis3_json.put("influenja_1-5year_female", ccs.get("influenja_1-5year_female"));
					mis3_json.put("diarrhea_2month-1year_male", ccs.get("diarrhea_2month-1year_male"));
					mis3_json.put("diarrhea_2month-1year_female", ccs.get("diarrhea_2month-1year_female"));
					mis3_json.put("diarrhea_1-5year_male", ccs.get("diarrhea_1-5year_male"));
					mis3_json.put("diarrhea_1-5year_female", ccs.get("diarrhea_1-5year_female"));
					mis3_json.put("malaria_2month-1year_male", ccs.get("malaria_2month-1year_male"));
					mis3_json.put("malaria_2month-1year_female", ccs.get("malaria_2month-1year_female"));
					mis3_json.put("malaria_1-5year_male", ccs.get("malaria_1-5year_male"));
					mis3_json.put("malaria_1-5year_female", ccs.get("malaria_1-5year_female"));
					mis3_json.put("fever_not_malaria_2month-1year_male", ccs.get("fever_not_malaria_2month-1year_male"));
					mis3_json.put("fever_not_malaria_2month-1year_female", ccs.get("fever_not_malaria_2month-1year_female"));
					mis3_json.put("fever_not_malaria_1-5year_male", ccs.get("fever_not_malaria_1-5year_male"));
					mis3_json.put("fever_not_malaria_1-5year_female", ccs.get("fever_not_malaria_1-5year_female"));
					mis3_json.put("measles_2month-1year_male", ccs.get("measles_2month-1year_male"));
					mis3_json.put("measles_2month-1year_female", ccs.get("measles_2month-1year_female"));
					mis3_json.put("measles_1-5year_male", ccs.get("measles_1-5year_male"));
					mis3_json.put("measles_1-5year_female", ccs.get("measles_1-5year_female"));
					mis3_json.put("ear_problem_2month-1year_male", ccs.get("ear_problem_2month-1year_male"));
					mis3_json.put("ear_problem_2month-1year_female", ccs.get("ear_problem_2month-1year_female"));
					mis3_json.put("ear_problem_1-5year_male", ccs.get("ear_problem_1-5year_male"));
					mis3_json.put("ear_problem_1-5year_female", ccs.get("ear_problem_1-5year_female"));
					mis3_json.put("malnutrition_2month-1year_male", ccs.get("malnutrition_2month-1year_male"));
					mis3_json.put("malnutrition_2month-1year_female", ccs.get("malnutrition_2month-1year_female"));
					mis3_json.put("malnutrition_1-5year_male", ccs.get("malnutrition_1-5year_male"));
					mis3_json.put("malnutrition_1-5year_female", ccs.get("malnutrition_1-5year_female"));
					mis3_json.put("other_disease_0-28days_male", ccs.get("other_disease_0-28days_male"));
					mis3_json.put("other_disease_0-28days_female", ccs.get("other_disease_0-28days_female"));
					mis3_json.put("other_disease_29-59days_male", ccs.get("other_disease_29-59days_male"));
					mis3_json.put("other_disease_29-59days_female", ccs.get("other_disease_29-59days_female"));
					mis3_json.put("other_disease_2month-1year_male", ccs.get("other_disease_2month-1year_male"));
					mis3_json.put("other_disease_2month-1year_female", ccs.get("other_disease_2month-1year_female"));
					mis3_json.put("other_disease_1-5year_male", ccs.get("other_disease_1-5year_male"));
					mis3_json.put("other_disease_1-5year_female", ccs.get("other_disease_1-5year_female"));

					mis3_submit_data.put("very_serious_disease_0-28days_male", ccs.get("very_serious_disease_0-28days_male"));
					mis3_submit_data.put("very_serious_disease_0-28days_female", ccs.get("very_serious_disease_0-28days_female"));
					mis3_submit_data.put("very_serious_disease_29-59days_male", ccs.get("very_serious_disease_29-59days_male"));
					mis3_submit_data.put("very_serious_disease_29-59days_female", ccs.get("very_serious_disease_29-59days_female"));
					mis3_submit_data.put("very_serious_disease_2month-1year_male", ccs.get("very_serious_disease_2month-1year_male"));
					mis3_submit_data.put("very_serious_disease_2month-1year_female", ccs.get("very_serious_disease_2month-1year_female"));
					mis3_submit_data.put("very_serious_disease_1-5year_male", ccs.get("very_serious_disease_1-5year_male"));
					mis3_submit_data.put("very_serious_disease_1-5year_female", ccs.get("very_serious_disease_1-5year_female"));
					mis3_submit_data.put("very_serious_disease_infected_0-28days_male", ccs.get("very_serious_disease_infected_0-28days_male"));
					mis3_submit_data.put("very_serious_disease_infected_0-28days_female", ccs.get("very_serious_disease_infected_0-28days_female"));
					mis3_submit_data.put("very_serious_disease_infected_29-59days_male", ccs.get("very_serious_disease_infected_29-59days_male"));
					mis3_submit_data.put("very_serious_disease_infected_29-59days_female", ccs.get("very_serious_disease_infected_29-59days_female"));
					mis3_submit_data.put("very_serious_disease_infected_2month-1year_male", ccs.get("very_serious_disease_infected_2month-1year_male"));
					mis3_submit_data.put("very_serious_disease_infected_2month-1year_female", ccs.get("very_serious_disease_infected_2month-1year_female"));
					mis3_submit_data.put("very_serious_disease_infected_1-5year_male", ccs.get("very_serious_disease_infected_1-5year_male"));
					mis3_submit_data.put("very_serious_disease_infected_1-5year_female", ccs.get("very_serious_disease_infected_1-5year_female"));
					mis3_submit_data.put("very_serious_disease_neumonia_0-28days_male", ccs.get("very_serious_disease_neumonia_0-28days_male"));
					mis3_submit_data.put("very_serious_disease_neumonia_0-28days_female", ccs.get("very_serious_disease_neumonia_0-28days_female"));
					mis3_submit_data.put("very_serious_disease_neumonia_29-59days_male", ccs.get("very_serious_disease_neumonia_29-59days_male"));
					mis3_submit_data.put("very_serious_disease_neumonia_29-59days_female", ccs.get("very_serious_disease_neumonia_29-59days_female"));
					mis3_submit_data.put("pneumonia_2month-1year_male", ccs.get("pneumonia_2month-1year_male"));
					mis3_submit_data.put("pneumonia_2month-1year_female", ccs.get("pneumonia_2month-1year_female"));
					mis3_submit_data.put("pneumonia_1-5year_male", ccs.get("pneumonia_1-5year_male"));
					mis3_submit_data.put("pneumonia_1-5year_female", ccs.get("pneumonia_1-5year_female"));
					mis3_submit_data.put("influenja_2month-1year_male", ccs.get("influenja_2month-1year_male"));
					mis3_submit_data.put("influenja_2month-1year_female", ccs.get("influenja_2month-1year_female"));
					mis3_submit_data.put("influenja_1-5year_male", ccs.get("influenja_1-5year_male"));
					mis3_submit_data.put("influenja_1-5year_female", ccs.get("influenja_1-5year_female"));
					mis3_submit_data.put("diarrhea_2month-1year_male", ccs.get("diarrhea_2month-1year_male"));
					mis3_submit_data.put("diarrhea_2month-1year_female", ccs.get("diarrhea_2month-1year_female"));
					mis3_submit_data.put("diarrhea_1-5year_male", ccs.get("diarrhea_1-5year_male"));
					mis3_submit_data.put("diarrhea_1-5year_female", ccs.get("diarrhea_1-5year_female"));
					mis3_submit_data.put("malaria_2month-1year_male", ccs.get("malaria_2month-1year_male"));
					mis3_submit_data.put("malaria_2month-1year_female", ccs.get("malaria_2month-1year_female"));
					mis3_submit_data.put("malaria_1-5year_male", ccs.get("malaria_1-5year_male"));
					mis3_submit_data.put("malaria_1-5year_female", ccs.get("malaria_1-5year_female"));
					mis3_submit_data.put("fever_not_malaria_2month-1year_male", ccs.get("fever_not_malaria_2month-1year_male"));
					mis3_submit_data.put("fever_not_malaria_2month-1year_female", ccs.get("fever_not_malaria_2month-1year_female"));
					mis3_submit_data.put("fever_not_malaria_1-5year_male", ccs.get("fever_not_malaria_1-5year_male"));
					mis3_submit_data.put("fever_not_malaria_1-5year_female", ccs.get("fever_not_malaria_1-5year_female"));
					mis3_submit_data.put("measles_2month-1year_male", ccs.get("measles_2month-1year_male"));
					mis3_submit_data.put("measles_2month-1year_female", ccs.get("measles_2month-1year_female"));
					mis3_submit_data.put("measles_1-5year_male", ccs.get("measles_1-5year_male"));
					mis3_submit_data.put("measles_1-5year_female", ccs.get("measles_1-5year_female"));
					mis3_submit_data.put("ear_problem_2month-1year_male", ccs.get("ear_problem_2month-1year_male"));
					mis3_submit_data.put("ear_problem_2month-1year_female", ccs.get("ear_problem_2month-1year_female"));
					mis3_submit_data.put("ear_problem_1-5year_male", ccs.get("ear_problem_1-5year_male"));
					mis3_submit_data.put("ear_problem_1-5year_female", ccs.get("ear_problem_1-5year_female"));
					mis3_submit_data.put("malnutrition_2month-1year_male", ccs.get("malnutrition_2month-1year_male"));
					mis3_submit_data.put("malnutrition_2month-1year_female", ccs.get("malnutrition_2month-1year_female"));
					mis3_submit_data.put("malnutrition_1-5year_male", ccs.get("malnutrition_1-5year_male"));
					mis3_submit_data.put("malnutrition_1-5year_female", ccs.get("malnutrition_1-5year_female"));
					mis3_submit_data.put("other_disease_0-28days_male", ccs.get("other_disease_0-28days_male"));
					mis3_submit_data.put("other_disease_0-28days_female", ccs.get("other_disease_0-28days_female"));
					mis3_submit_data.put("other_disease_29-59days_male", ccs.get("other_disease_29-59days_male"));
					mis3_submit_data.put("other_disease_29-59days_female", ccs.get("other_disease_29-59days_female"));
					mis3_submit_data.put("other_disease_2month-1year_male", ccs.get("other_disease_2month-1year_male"));
					mis3_submit_data.put("other_disease_2month-1year_female", ccs.get("other_disease_2month-1year_female"));
					mis3_submit_data.put("other_disease_1-5year_male", ccs.get("other_disease_1-5year_male"));
					mis3_submit_data.put("other_disease_1-5year_female", ccs.get("other_disease_1-5year_female"));
				}

				else{
					mis3_json.put("very_serious_disease_0-28days_male", "0");
					mis3_json.put("very_serious_disease_0-28days_female", "0");
					mis3_json.put("very_serious_disease_29-59days_male", "0");
					mis3_json.put("very_serious_disease_29-59days_female", "0");
					mis3_json.put("very_serious_disease_2month-1year_male", "0");
					mis3_json.put("very_serious_disease_2month-1year_female", "0");
					mis3_json.put("very_serious_disease_1-5year_male", "0");
					mis3_json.put("very_serious_disease_1-5year_female", "0");
					mis3_json.put("very_serious_disease_infected_0-28days_male", "0");
					mis3_json.put("very_serious_disease_infected_0-28days_female", "0");
					mis3_json.put("very_serious_disease_infected_29-59days_male", "0");
					mis3_json.put("very_serious_disease_infected_29-59days_female", "0");
					mis3_json.put("very_serious_disease_infected_2month-1year_male", "0");
					mis3_json.put("very_serious_disease_infected_2month-1year_female", "0");
					mis3_json.put("very_serious_disease_infected_1-5year_male", "0");
					mis3_json.put("very_serious_disease_infected_1-5year_female", "0");
					mis3_json.put("very_serious_disease_neumonia_0-28days_male", "0");
					mis3_json.put("very_serious_disease_neumonia_0-28days_female", "0");
					mis3_json.put("very_serious_disease_neumonia_29-59days_male", "0");
					mis3_json.put("very_serious_disease_neumonia_29-59days_female", "0");
					mis3_json.put("pneumonia_2month-1year_male", "0");
					mis3_json.put("pneumonia_2month-1year_female", "0");
					mis3_json.put("pneumonia_1-5year_male", "0");
					mis3_json.put("pneumonia_1-5year_female", "0");
					mis3_json.put("influenja_2month-1year_male", "0");
					mis3_json.put("influenja_2month-1year_female", "0");
					mis3_json.put("influenja_1-5year_male", "0");
					mis3_json.put("influenja_1-5year_female", "0");
					mis3_json.put("diarrhea_2month-1year_male", "0");
					mis3_json.put("diarrhea_2month-1year_female", "0");
					mis3_json.put("diarrhea_1-5year_male", "0");
					mis3_json.put("diarrhea_1-5year_female", "0");
					mis3_json.put("malaria_2month-1year_male", "0");
					mis3_json.put("malaria_2month-1year_female", "0");
					mis3_json.put("malaria_1-5year_male", "0");
					mis3_json.put("malaria_1-5year_female", "0");
					mis3_json.put("fever_not_malaria_2month-1year_male", "0");
					mis3_json.put("fever_not_malaria_2month-1year_female", "0");
					mis3_json.put("fever_not_malaria_1-5year_male", "0");
					mis3_json.put("fever_not_malaria_1-5year_female", "0");
					mis3_json.put("measles_2month-1year_male", "0");
					mis3_json.put("measles_2month-1year_female", "0");
					mis3_json.put("measles_1-5year_male", "0");
					mis3_json.put("measles_1-5year_female", "0");
					mis3_json.put("ear_problem_2month-1year_male", "0");
					mis3_json.put("ear_problem_2month-1year_female", "0");
					mis3_json.put("ear_problem_1-5year_male", "0");
					mis3_json.put("ear_problem_1-5year_female", "0");
					mis3_json.put("malnutrition_2month-1year_male", "0");
					mis3_json.put("malnutrition_2month-1year_female", "0");
					mis3_json.put("malnutrition_1-5year_male", "0");
					mis3_json.put("malnutrition_1-5year_female", "0");
					mis3_json.put("other_disease_0-28days_male", "0");
					mis3_json.put("other_disease_0-28days_female", "0");
					mis3_json.put("other_disease_29-59days_male", "0");
					mis3_json.put("other_disease_29-59days_female", "0");
					mis3_json.put("other_disease_2month-1year_male", "0");
					mis3_json.put("other_disease_2month-1year_female", "0");
					mis3_json.put("other_disease_1-5year_male", "0");
					mis3_json.put("other_disease_1-5year_female", "0");
				}

				if(!ccsd.isEmpty()){
					mis3_json.put("refer_accepted_0-28days_male", ccsd.get("refer_accepted_0-28days_male"));
					mis3_json.put("refer_accepted_0-28days_female", ccsd.get("refer_accepted_0-28days_female"));
					mis3_json.put("refer_accepted_29-59days_male", ccsd.get("refer_accepted_29-59days_male"));
					mis3_json.put("refer_accepted_29-59days_female", ccsd.get("refer_accepted_29-59days_female"));
					mis3_json.put("fst_dose_injection_0-28days_male", ccsd.get("fst_dose_injection_0-28days_male"));
					mis3_json.put("fst_dose_injection_0-28days_female", ccsd.get("fst_dose_injection_0-28days_female"));
					mis3_json.put("fst_dose_injection_29-59days_male", ccsd.get("fst_dose_injection_29-59days_male"));
					mis3_json.put("fst_dose_injection_29-59days_female", ccsd.get("fst_dose_injection_29-59days_female"));
					mis3_json.put("second_dose_injection_0-28days_male", ccsd.get("second_dose_injection_0-28days_male"));
					mis3_json.put("second_dose_injection_0-28days_female", ccsd.get("second_dose_injection_0-28days_female"));
					mis3_json.put("second_dose_injection_29-59days_male", ccsd.get("second_dose_injection_29-59days_male"));
					mis3_json.put("second_dose_injection_29-59days_female", ccsd.get("second_dose_injection_29-59days_female"));
					mis3_json.put("imci_referred_0-28days_male", ccsd.get("imci_referred_0-28days_male"));
					mis3_json.put("imci_referred_0-28days_female", ccsd.get("imci_referred_0-28days_female"));
					mis3_json.put("imci_referred_29-59days_male", ccsd.get("imci_referred_29-59days_male"));
					mis3_json.put("imci_referred_29-59days_female", ccsd.get("imci_referred_29-59days_female"));
					mis3_json.put("imci_referred_2month-1year_male", ccsd.get("imci_referred_2month-1year_male"));
					mis3_json.put("imci_referred_2month-1year_female", ccsd.get("imci_referred_2month-1year_female"));
					mis3_json.put("imci_referred_1-5year_male", ccsd.get("imci_referred_1-5year_male"));
					mis3_json.put("imci_referred_1-5year_female", ccsd.get("imci_referred_1-5year_female"));

					mis3_submit_data.put("refer_accepted_0-28days_male", ccsd.get("refer_accepted_0-28days_male"));
					mis3_submit_data.put("refer_accepted_0-28days_female", ccsd.get("refer_accepted_0-28days_female"));
					mis3_submit_data.put("refer_accepted_29-59days_male", ccsd.get("refer_accepted_29-59days_male"));
					mis3_submit_data.put("refer_accepted_29-59days_female", ccsd.get("refer_accepted_29-59days_female"));
					mis3_submit_data.put("fst_dose_injection_0-28days_male", ccsd.get("fst_dose_injection_0-28days_male"));
					mis3_submit_data.put("fst_dose_injection_0-28days_female", ccsd.get("fst_dose_injection_0-28days_female"));
					mis3_submit_data.put("fst_dose_injection_29-59days_male", ccsd.get("fst_dose_injection_29-59days_male"));
					mis3_submit_data.put("fst_dose_injection_29-59days_female", ccsd.get("fst_dose_injection_29-59days_female"));
					mis3_submit_data.put("second_dose_injection_0-28days_male", ccsd.get("second_dose_injection_0-28days_male"));
					mis3_submit_data.put("second_dose_injection_0-28days_female", ccsd.get("second_dose_injection_0-28days_female"));
					mis3_submit_data.put("second_dose_injection_29-59days_male", ccsd.get("second_dose_injection_29-59days_male"));
					mis3_submit_data.put("second_dose_injection_29-59days_female", ccsd.get("second_dose_injection_29-59days_female"));
					mis3_submit_data.put("imci_referred_0-28days_male", ccsd.get("imci_referred_0-28days_male"));
					mis3_submit_data.put("imci_referred_0-28days_female", ccsd.get("imci_referred_0-28days_female"));
					mis3_submit_data.put("imci_referred_29-59days_male", ccsd.get("imci_referred_29-59days_male"));
					mis3_submit_data.put("imci_referred_29-59days_female", ccsd.get("imci_referred_29-59days_female"));
					mis3_submit_data.put("imci_referred_2month-1year_male", ccsd.get("imci_referred_2month-1year_male"));
					mis3_submit_data.put("imci_referred_2month-1year_female", ccsd.get("imci_referred_2month-1year_female"));
					mis3_submit_data.put("imci_referred_1-5year_male", ccsd.get("imci_referred_1-5year_male"));
					mis3_submit_data.put("imci_referred_1-5year_female", ccsd.get("imci_referred_1-5year_female"));
				}
				else{
					mis3_json.put("refer_accepted_0-28days_male", "0");
					mis3_json.put("refer_accepted_0-28days_female", "0");
					mis3_json.put("refer_accepted_29-59days_male", "0");
					mis3_json.put("refer_accepted_29-59days_female", "0");
					mis3_json.put("fst_dose_injection_0-28days_male", "0");
					mis3_json.put("fst_dose_injection_0-28days_female", "0");
					mis3_json.put("fst_dose_injection_29-59days_male", "0");
					mis3_json.put("fst_dose_injection_29-59days_female", "0");
					mis3_json.put("second_dose_injection_0-28days_male", "0");
					mis3_json.put("second_dose_injection_0-28days_female", "0");
					mis3_json.put("second_dose_injection_29-59days_male", "0");
					mis3_json.put("second_dose_injection_29-59days_female", "0");
					mis3_json.put("imci_referred_0-28days_male", "0");
					mis3_json.put("imci_referred_0-28days_female", "0");
					mis3_json.put("imci_referred_29-59days_male", "0");
					mis3_json.put("imci_referred_29-59days_female", "0");
					mis3_json.put("imci_referred_2month-1year_male", "0");
					mis3_json.put("imci_referred_2month-1year_female", "0");
					mis3_json.put("imci_referred_1-5year_male", "0");
					mis3_json.put("imci_referred_1-5year_female", "0");
				}

				if(!csba.equals("1")) {
					if (!gp.isEmpty()) {
						gp_male_center = gp.get("gp_male_center");

						gp_female_center = gp.get("gp_female_center");
						gp_female_satelite = gp.get("gp_female_satelite");
						gp_ifa_center = gp.get("gp_ifa_center");
						gp_ifa_satelite = gp.get("gp_ifa_satelite");
						gp_rta_sta_center = gp.get("gp_rta_sta_center");
						gp_rta_sta_satelite = gp.get("gp_rta_sta_satelite");
						gp_rta_sta_all_center = gp.get("gp_rta_sta_all_center");
						gp_rta_sta_all_satelite = gp.get("gp_rta_sta_all_satelite");



						mis3_json.put("gp_male_center", gp.get("gp_male_center"));
						mis3_json.put("gp_male_satelite", gp.get("gp_male_satelite"));
						mis3_json.put("gp_female_center", gp.get("gp_female_center"));
						mis3_json.put("gp_female_satelite", gp.get("gp_female_satelite"));
						mis3_json.put("gp_ifa_center", gp.get("gp_ifa_center"));
						mis3_json.put("gp_ifa_satelite", gp.get("gp_ifa_satelite"));
						mis3_json.put("gp_rta_sta_center", gp.get("gp_rta_sta_center"));
						mis3_json.put("gp_rta_sta_satelite", gp.get("gp_rta_sta_satelite"));
						mis3_json.put("gp_rta_sta_all_center", gp.get("gp_rta_sta_all_center"));
						mis3_json.put("gp_rta_sta_all_satelite", gp.get("gp_rta_sta_all_satelite"));

						if(query_from_gpservice) {
							mis3_json.put("gp_outdoor_child_0_1_center ", gp.get("gp_outdoor_child_0_1_center"));
							mis3_json.put("gp_outdoor_child_0_1_satelite ", gp.get("gp_outdoor_child_0_1_satelite"));

							mis3_json.put("gp_outdoor_child_1_5_center", gp.get("gp_outdoor_child_1_5_center"));
							mis3_json.put("gp_outdoor_child_1_5_satelite", gp.get("gp_outdoor_child_1_5_satelite"));
						}
						//****Coded By Evana : 23/01/2019*//
						mis3_json.put("adolescent_change_counceling_center", gp.get("gp_adolescent_change_council_center"));
						mis3_json.put("adolescent_change_counceling_satelite", gp.get("gp_adolescent_change_council_satelite"));

						mis3_json.put("adolescence_marraige_counceling_center", gp.get("gp_child_marraige_council_center"));
						mis3_json.put("adolescence_marraige_counceling_satelite", gp.get("gp_child_marraige_council_satelite"));

						mis3_json.put("adolescence_sexual_disease_counceling_center", gp.get("gp_adolescent_desease_council_center"));
						mis3_json.put("adolescence_sexual_disease_counceling_satelite", gp.get("gp_adolescent_desease_council_satelite"));

						mis3_json.put("adolescence_food_counceling_center", gp.get("gp_adolescent_food_council_center"));
						mis3_json.put("adolescence_food_counceling_satelite", gp.get("gp_adolescent_food_council_satelite"));

						mis3_json.put("adolescence_fighting_counceling_center", gp.get("gp_adolescent_fighting_council_center"));
						mis3_json.put("adolescence_fighting_counceling_satelite", gp.get("gp_adolescent_fighting_council_satelite"));

						mis3_json.put("adolescence_mentalhealth_counceling_center", gp.get("gp_adolescent_mental_council_center"));
						mis3_json.put("adolescence_mentalhealth_counceling_satelite", gp.get("gp_adolescent_mental_council_satelite"));

						mis3_json.put("adolescence_sanitary_counceling_center", gp.get("gp_adolescent_sanitary_council_center"));
						mis3_json.put("adolescence_sanitary_counceling_satelite", gp.get("gp_adolescent_sanitary_council_satelite"));

						mis3_json.put("adolescence_folicacide_supply_center", gp.get("gp_adolescence_folicacide_supply_center"));
						mis3_json.put("adolescence_folicacide_supply_satelite", gp.get("gp_adolescence_folicacide_supply_satelite"));

					}
					else{
						mis3_json.put("gp_male_center", "0");
						mis3_json.put("gp_female_center", "0");
						mis3_json.put("gp_female_satelite", "0");
						mis3_json.put("gp_ifa_center", "0");
						mis3_json.put("gp_ifa_satelite", "0");
						mis3_json.put("gp_rta_sta_center", "0");
						mis3_json.put("gp_rta_sta_satelite", "0");
						mis3_json.put("gp_rta_sta_all_center", "0");
						mis3_json.put("gp_rta_sta_all_satelite", "0");

						mis3_json.put("adolescent_change_counceling_center", "0");
						mis3_json.put("adolescent_change_counceling_satelite", "0");

						mis3_json.put("adolescence_marraige_counceling_center", "0");
						mis3_json.put("adolescence_marraige_counceling_satelite", "0");

						mis3_json.put("adolescence_sexual_disease_counceling_center", "0");
						mis3_json.put("adolescence_sexual_disease_counceling_satelite", "0");

						mis3_json.put("adolescence_food_counceling_center", "0");
						mis3_json.put("adolescence_food_counceling_satelite", "0");

						mis3_json.put("adolescence_fighting_counceling_center", "0");
						mis3_json.put("adolescence_fighting_counceling_satelite", "0");

						mis3_json.put("adolescence_mentalhealth_counceling_center", "0");
						mis3_json.put("adolescence_mentalhealth_counceling_satelite", "0");

						mis3_json.put("adolescence_sanitary_counceling_center", "0");
						mis3_json.put("adolescence_sanitary_counceling_satelite", "0");


					}

					if((!facilityId.equals("") && mis3MonitoringTool.equals("0")) || mis3ViewType.equals("3") || dhis2.equals("1")) {
						System.out.println("M");
						String[] report_date_month = (start_date).split("-");
						DBOperation dbOp_submission = new DBOperation();
						DBInfoHandler dbObject_submission = new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));
						String sql_submit_data = "select " + table_schema.getColumn("", "mis3_report_submission_data_field_name") + " as field_name, " + table_schema.getColumn("", "mis3_report_submission_data_field_value") + " as field_value from " + table_schema.getTable("table_mis3_report_submission_data") + " where " + table_schema.getColumn("table", "mis3_report_submission_data_facility_id", new String[]{facilityId}, "=") + " and " + table_schema.getColumn("table", "mis3_report_submission_data_report_month", new String[]{report_date_month[1]}, "=") + " and " + table_schema.getColumn("table", "mis3_report_submission_data_report_year", new String[]{report_date_month[0]}, "=");

						dbObject_submission = dbOp_submission.dbCreateStatement(dbObject_submission);
						dbObject_submission = dbOp_submission.dbExecute(dbObject_submission, sql_submit_data);
						ResultSet rs_submission = dbObject_submission.getResultSet();
						rs_submission.last();
						rowCount_submission = rs_submission.getRow();
						rs_submission.beforeFirst();
						while (rs_submission.next()) {
							if(submission_status.equals("2")) {
								if(!mis3_json.has(rs_submission.getString("field_name"))) {
									mis3_json.put(rs_submission.getString("field_name"), rs_submission.getString("field_value"));
									mis3_submit_data.put(rs_submission.getString("field_name"), rs_submission.getString("field_value"));
								}
							}
							else{
								if(!mis3_json.has(rs_submission.getString("field_name"))) {//changed because its taking data from mis3_report_submission_data table which data is diff from service query
									mis3_json.put(rs_submission.getString("field_name"), rs_submission.getString("field_value"));
									mis3_submit_data.put(rs_submission.getString("field_name"), rs_submission.getString("field_value"));
								}
							}
						}

						System.out.println("Submitted MIS3: "+mis3_json);

						//calculation for previous month data
						String prev_month = String.valueOf(Integer.parseInt(report_date_month[1].toString())-1);
						String sql_submit_data_prev_month = "select " + table_schema.getColumn("", "mis3_report_submission_data_field_name") + " as field_name, " + table_schema.getColumn("", "mis3_report_submission_data_field_value") + " as field_value from " + table_schema.getTable("table_mis3_report_submission_data") + " where " + table_schema.getColumn("table", "mis3_report_submission_data_facility_id", new String[]{facilityId}, "=") + " and " + table_schema.getColumn("table", "mis3_report_submission_data_report_month", new String[]{prev_month}, "=") + " and " + table_schema.getColumn("table", "mis3_report_submission_data_report_year", new String[]{report_date_month[0]}, "=");
						mis3_json = getMIS3DataFromPreviousMonth(dbObject_submission,sql_submit_data_prev_month,submission_status,mis3_json,mis3_submit_data);

						dbOp_submission.closeStatement(dbObject_submission);
						dbOp_submission.closeConn(dbObject_submission);
						dbObject_submission = null;

					}

//					if (!lmis.containsKey("1"))
//						lmis.put("1", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("1").put("distribution", pill_shukhi_total);
//
//					if (!lmis.containsKey("2"))
//						lmis.put("2", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("2").put("distribution", pill_apon_total);
//
//					if (!lmis.containsKey("3"))
//						lmis.put("3", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("3").put("distribution", condom_total);
//
//					if (!lmis.containsKey("4"))
//						lmis.put("4", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("4").put("distribution", Integer.toString(Integer.parseInt(new_inject_center) + Integer.parseInt(old_inject_center) + Integer.parseInt(new_inject_satelite) + Integer.parseInt(old_inject_satelite)));
//
//					if (!lmis.containsKey("5"))
//						lmis.put("5", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("5").put("distribution", Integer.toString(Integer.parseInt(new_inject_center) + Integer.parseInt(old_inject_center) + Integer.parseInt(new_inject_satelite) + Integer.parseInt(old_inject_satelite)));
//
//					if (!lmis.containsKey("6"))
//						lmis.put("6", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("6").put("distribution", Integer.toString(Integer.parseInt(iud_normal_center) + Integer.parseInt(iud_normal_satelite) + Integer.parseInt(iud_after_delivery_center) + Integer.parseInt(iud_after_delivery_satelite)));
//
//					if (!lmis.containsKey("10"))
//						lmis.put("10", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("10").put("distribution", deliverymisoprostol);
//
//					if (!lmis.containsKey("12"))
//						lmis.put("12", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("12").put("distribution", chlorehexidin);
//
//					if (!lmis.containsKey("13"))
//						lmis.put("13", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("13").put("distribution", Integer.toString(Integer.parseInt(ancmgso4center) + Integer.parseInt(ancmgso4satelite)));
//
//					if (!lmis.containsKey("14"))
//						lmis.put("14", (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
//
//					lmis.get("14").put("distribution", deliverycxytocin);
				}

				int l;

				if(!csba.equals("1") && !db_csba.equals("1") && !jsonRequest.equals("1") && !dhis2.equals("1"))
				{
					resultString = "";
					if((facilityId.equals("3600001") || facilityId.equals("757001") || facilityId.equals("9366131")) && uCode.equals("269119"))
						resultString += "<input type='button' class='btn btn-info dhis2_export' value='Export to DHIS2'><input type='hidden' class='dhis2_month' value='"+dhis2_month+"'><input type='hidden' class='dhis2_year' value='"+dhis2_year+"'><br /> <br />";

					resultString += "<div style='float: right;'><button type='button' class='btn btn-default pre_button' value='0' disabled><i class='fa fa-chevron-left' aria-hidden='true'></i></button><span class='showing_page'>&nbsp&nbsp 1 of 3 &nbsp&nbsp</span><button type='button' class='btn btn-default next_button' value='2'><i class='fa fa-chevron-right' aria-hidden='true'></i></button></div>";

					resultString += "<table class=\"table\" id=\"table_header_1\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
							+ "<td><div style='border: 1px solid black;text-align: center;'>ছেলে হোক, মেয়ে হোক,<br>দুটি সন্তানই যথেষ্ট।</div></td>"
							+ "<td style='text-align: center;'><b>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>পরিবার পরিকল্পনা অধিদপ্তর<br>পরিবার পরিকল্পনা, মা ও শিশু স্বাস্থ্য কার্যক্রমের মাসিক অগ্রগতির প্রতিবেদন</b>"
							+ "<br>(UH&FWC এবং অন্যান্য প্রতিষ্ঠানের জন্য প্রযোজ্য)<br>"
							+ "<b>মাসঃ</b> "+bangla_month+" <b>সালঃ</b> "+bangla_year+"</td>"
							+ "<td style='text-align: right;' class='page_no'>এমআইএস ফরম-৩<br>পৃষ্ঠা-১</td>"
							+ "</tr><tr><td colspan='3'><img width='60' src='image/dgfp_logo.png'></td></tr><tr></table>";


					resultString += "<table class=\"table\" id=\"table_header_2\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
							+ "<td style='width: 38%'><b>কেন্দ্রের নামঃ  </b> "+facilityName+"</td>"
							+ "<td><b>ইউনিয়নঃ</b> "+union_info.get("union_name")+"</td>"
							+ "<td style='text-align: center;'><b>উপজেলা/থানাঃ </b> "+upazila_info.get("upazila_name")+"</td>"
							+ "<td style='text-align: right;'><b>জেলাঃ</b> "+zilla_info.get("zilla_name")+"</td>"
							+"</tr></table>";

					resultString += MIS3ReportTable.getFacilityTable(mis3_json, mis3ViewType);



					resultString += "<div style='float: right;'><button type='button' class='btn btn-default pre_button' value='0' disabled><i class='fa fa-chevron-left' aria-hidden='true'></i></button><span class='showing_page'>&nbsp&nbsp 1 of 3 &nbsp&nbsp</span><button type='button' class='btn btn-default next_button' value='2'><i class='fa fa-chevron-right' aria-hidden='true'></i></button></div>";
				}
				else if(db_csba.equals("1") && !csba.equals("1") && (providerType.equals("2") || providerType.equals("3")))
				{

					DBOperation dbOp = new DBOperation();
					DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo();
					sql_provider = "select "+table_schema.getColumn("", "providerarea_provcode")+" , "+table_schema.getColumn("", "providerarea_provtype")+" , "
							//"string_agg("+table_schema.getColumn("", "village_villagename")+" , ', ') as village, "
							+ "string_agg(distinct("+table_schema.getColumn("", "fwaunit_unameban")+") , ', ') as fwaunit, string_agg(distinct("+table_schema.getColumn("", "providerarea_ward")+")::text , ', ') as ward "
							//"string_agg("+table_schema.getColumn("", "providerarea_block")+" , ', ') as block "
							+ "from "+ table_schema.getTable("table_providerarea") +" "
							+ "left join "+ table_schema.getTable("table_fwaunit") +" on "+table_schema.getColumn("table", "fwaunit_ucode")+" ="+table_schema.getColumn("table", "providerarea_fwaunit")+" "
							//+ "join "+ table_schema.getTable("table_village") +" on "+table_schema.getColumn("table", "village_zillaid")+" ="+table_schema.getColumn("table", "providerarea_zillaid")+"  and "+table_schema.getColumn("table", "village_upazilaid")+" ="+table_schema.getColumn("table", "providerarea_upazilaid")+"  "
							//+ "and "+table_schema.getColumn("table", "village_unionid")+" ="+table_schema.getColumn("table", "providerarea_unionid")+"  and "+table_schema.getColumn("table", "village_mouzaid")+" ="+table_schema.getColumn("table", "providerarea_mouzaid")+"  and "+table_schema.getColumn("table", "village_villageid")+" ="+table_schema.getColumn("table", "providerarea_villageid")+"  "
							+ "where "+table_schema.getColumn("", "providerarea_provcode",new String[]{providerId},"=")+" group by "+table_schema.getColumn("", "providerarea_provcode")+" , "+table_schema.getColumn("", "providerarea_provtype")+" ";
					System.out.println(sql_provider);
					dbObject = dbOp.dbCreateStatement(dbObject);
					dbObject = dbOp.dbExecute(dbObject,sql_provider);
					ResultSet rs = dbObject.getResultSet();

					//String providerType = null;
					//String village = null;
					String ward_unit = "";
					while(rs.next()){
						//providerType = rs.getString("ProvType");
						//village = rs.getString("village");
						if(providerType.equals("3"))
							ward_unit = "<b>ইউনিট:</b> " + rs.getString("fwaunit");
						else
							ward_unit = "<b>ওয়ার্ড:</b> " + rs.getString("ward");

						//ward_unit = rs.getString("ward");
					}

					dbOp.closeStatement(dbObject);
					dbOp.closeConn(dbObject);
					dbObject = null;

					resultString = "<table class=\"table\" id=\"table_header_1\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
							+ "<td><div style='border: 1px solid black;text-align: center;'>ছেলে হোক, মেয়ে হোক,<br>দুটি সন্তানই যথেষ্ট।</div></td>"
							+ "<td style='text-align: center;'><b>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>স্বাস্থ্য ও পরিবার কল্যাণ মন্ত্রনালয়<br>সিএসবিএ মাসিক অগ্রগতির প্রতিবেদন</b><br>"
							+ "<b>মাসঃ</b> "+bangla_month+" <b>সালঃ</b> "+bangla_year+"</td>"
							+ "<td style='text-align: right;' class='page_no'>সিএসবিএ</td>"
							+ "</tr>";

					if(providerType.equals("3"))
						resultString += "<tr><td colspan='3'><img width='60' src='image/dgfp_logo.png'></td></tr><tr>";

					resultString += "</table>";

					resultString += "<table class=\"table\" id=\"table_header_2\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
							+ "<td style='width: 38%'>"+ward_unit+"</td>"
							+ "<td><b>ইউনিয়নঃ</b> "+union_info.get("union_name")+"</td>"
							+ "<td style='text-align: center;'><b>উপজেলা/থানাঃ </b> "+upazila_info.get("upazila_name")+"</td>"
							+ "<td style='text-align: right;'><b>জেলাঃ</b> "+zilla_info.get("zilla_name")+"</td>"
							+"</tr></table>";

					resultString += MIS3ReportTable.getCSBATable(mis3_json, mis3ViewType);

					//System.out.println(resultString);
				}
				else if(csba.equals("1")){

					resultString = "tt_women_1st_csba=0,tt_women_2nd_csba=0,tt_women_3rd_csba=0,tt_women_4th_csba=0,tt_women_5th_csba=0";
					resultString = resultString + ",preg_anc_service_visit1_csba="+(Integer.parseInt(anc1center) + Integer.parseInt(anc1satelite));
					resultString = resultString + ",preg_anc_service_visit2_csba="+(Integer.parseInt(anc2center) + Integer.parseInt(anc2satelite));
					resultString = resultString + ",preg_anc_service_visit3_csba="+(Integer.parseInt(anc3center) + Integer.parseInt(anc3satelite));
					resultString = resultString + ",preg_anc_service_visit4_csba="+(Integer.parseInt(anc4center) + Integer.parseInt(anc4satelite));
					//resultString = resultString + ",cesarean="+csectiondelivery+",delivary_service_3rd_amts1_csba="+amtsl+",livebirth="+livebirth+",stillbirth="+stillbirth+",pacservice="+pacservicecount;
					resultString = resultString + ",delivary_service_delivery_done_csba="+(Integer.parseInt(csectiondelivery) + Integer.parseInt(normaldelivery));
					resultString = resultString + ",delivary_service_3rd_amts1_csba="+amtsl+",delivary_service_misoprostol_taken_csba="+deliverymisoprostol;
					resultString = resultString + ",pnc_mother_family_planning_csba="+(Integer.parseInt(pncmotherfpcenter) + Integer.parseInt(pncmotherfpsatelite));
					resultString = resultString + ",pnc_mother_visit1_csba="+(Integer.parseInt(pncmother1center) + Integer.parseInt(pncmother1satelite));
					resultString = resultString + ",pnc_mother_visit2_csba="+(Integer.parseInt(pncmother2center) + Integer.parseInt(pncmother2satelite));
					resultString = resultString + ",pnc_mother_visit3_csba="+(Integer.parseInt(pncmother3center) + Integer.parseInt(pncmother3satelite));
					resultString = resultString + ",pnc_mother_visit4_csba="+(Integer.parseInt(pncmother4center) + Integer.parseInt(pncmother4satelite));
					resultString = resultString + ",pnc_child_visit1_csba="+(Integer.parseInt(pncchild1center) + Integer.parseInt(pncchild1satelite));
					resultString = resultString + ",pnc_child_visit2_csba="+(Integer.parseInt(pncchild2center) + Integer.parseInt(pncchild2satelite));
					resultString = resultString + ",pnc_child_visit3_csba="+(Integer.parseInt(pncchild3center) + Integer.parseInt(pncchild3satelite));
					resultString = resultString + ",pnc_child_visit4_csba="+(Integer.parseInt(pncchild4center) + Integer.parseInt(pncchild4satelite));
					//resultString = resultString + ",mncrefer="+(Integer.parseInt(ancrefercenter) + Integer.parseInt(deliveryrefercenter) + Integer.parseInt(pncmotherrefercenter) + Integer.parseInt(ancrefersatelite) + Integer.parseInt(deliveryrefersatelite) + Integer.parseInt(pncmotherrefersatelite));
					resultString = resultString + ",ref_preg_delivery_pnc_diff_refer_csba="+(Integer.parseInt(pncmotherrefercenter) + Integer.parseInt(pncmotherrefersatelite));
					resultString = resultString + ",ref_eclampsia_mgso4_inj_refer_csba=0";
					resultString = resultString + ",ref_newborn_difficulty_csba="+(Integer.parseInt(newbornreferalcenter) + Integer.parseInt(newbornreferalsatelite));
				}
				else if(jsonRequest.equals("1")){
					resultString = mis3_json.toString();
					System.out.println(mis3_json);
				}
				else if(dhis2.equals("1")){
					Integer inject_total = Integer.parseInt(mis3_json.getString("new_inject_center"))+Integer.parseInt(mis3_json.getString("old_inject_center"))+Integer.parseInt(mis3_json.getString("new_inject_satelite"))
							+Integer.parseInt(mis3_json.getString("old_inject_satelite"))+Integer.parseInt(mis3_json.getString("new_inject_ppfp_center"))+Integer.parseInt(mis3_json.getString("old_inject_ppfp_center"))
							+Integer.parseInt(mis3_json.getString("new_inject_ppfp_satelite"))+Integer.parseInt(mis3_json.getString("old_inject_ppfp_satelite"));
					mis3_json.put("inject_vial_total", inject_total);
					mis3_json.put("inject_syringes_total", inject_total);
					Integer iud_total = Integer.parseInt(mis3_json.getString("iud_normal_center")) + Integer.parseInt(mis3_json.getString("iud_normal_satelite")) + Integer.parseInt(mis3_json.getString("iud_after_delivery_center")) + Integer.parseInt(mis3_json.getString("iud_after_delivery_satelite"));
					mis3_json.put("iud_total", iud_total);
					Integer misoprostolcenter = Integer.parseInt(mis3_json.getString("ancmisoprostolcenter"))+Integer.parseInt(mis3_json.getString("ancmisoprostolsatelite"))+Integer.parseInt(mis3_json.getString("deliverymisoprostol"));
					mis3_json.put("misoprostolcenter", misoprostolcenter);
					Integer mgso4 = Integer.parseInt(mis3_json.getString("ancmgso4center"))+Integer.parseInt(mis3_json.getString("ancmgso4satelite"));
					mis3_json.put("mgso4", mgso4);

					resultString = mis3_json.toString();
					System.out.println(mis3_json);
				}
			}
			else {
				sql_gp = "SELECT COUNT(CASE WHEN "+table_schema.getColumn("table", "clientmap_gender",new String[]{"1"},"=")+" THEN 1 ELSE NULL END) as gp_male, "
						+ "COUNT(CASE WHEN "+table_schema.getColumn("table", "clientmap_gender",new String[]{"2"},"=")+" THEN 1 ELSE NULL END) as gp_female "
						+ "FROM "+ table_schema.getTable("table_gpservice") +" "
						+ "JOIN "+ table_schema.getTable("table_clientmap") +" on "+table_schema.getColumn("table", "clientmap_generatedid")+"="+table_schema.getColumn("table", "gpservice_healthid")+" "
						+ "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "gpservice_providerid")+" "
						+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" "
						+ "where "+table_schema.getColumn("", "providerdb_zillaid",new String[]{zilla},"=")+" and "+table_schema.getColumn("", "providerdb_upazilaid",new String[]{upazila},"=")+" and "+table_schema.getColumn("", "providerdb_unionid",new String[]{union},"=")+" "
						+ "and ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_active",new String[]{"1"},"=")+") "
						+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table", "gpservice_providerid")+"  "
						+ "where (case when count_provider>1 then "
						+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","gpservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","gpservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
						+ "else "+ table_schema.getColumn("table", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" end) "
						+ "and "+query_condition+" GROUP BY "+table_schema.getColumn("table","gpservice_providerid")+","+table_schema.getColumn("table","providerdb_zillaid")+","+table_schema.getColumn("table","providerdb_upazilaid")+","+table_schema.getColumn("table","providerdb_unionid")+" ";

				System.out.println(sql_gp);
				gp = GetMIS3ResultSet.getFinalResult(sql_gp, "notString", zilla, upazila);

				String gp_male="0", gp_female="0";

				if (!gp.isEmpty()) {
					gp_male = gp.get("gp_male");
					gp_female = gp.get("gp_female");
				}

				resultString = "<div style='float: right;'><button type='button' class='btn btn-default pre_button' value='0' disabled><i class='fa fa-chevron-left' aria-hidden='true'></i></button><span class='showing_page'>&nbsp&nbsp 1 of 3 &nbsp&nbsp</span><button type='button' class='btn btn-default next_button' value='2'><i class='fa fa-chevron-right' aria-hidden='true'></i></button></div>";

				resultString += "<table class=\"table\" id=\"table_header_1\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
						+ "<td><div style='border: 1px solid black;text-align: center;'>ছেলে হোক, মেয়ে হোক,<br>দুটি সন্তানই যথেষ্ট।</div></td>"
						+ "<td style='text-align: center;'><b>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>পরিবার স্বাস্থ্য অধিদপ্তর</b><br>"
						+ "<b>মাসঃ</b> "+bangla_month+" <b>সালঃ</b> "+bangla_year+"</td>"
						+ "<td style='text-align: right;' class='page_no'></td>"
						+ "</tr><tr><td colspan='3'><img width='60' src='image/dghs_logo.png'></td></tr><tr></table>";


				resultString += "<table class=\"table\" id=\"table_header_2\" width=\"700\" cellspacing=\"0\" style='font-size:10px;'><tr>"
						+ "<td style='width: 38%'><b>কেন্দ্রের নামঃ  </b> "+facilityName+"</td>"
						+ "<td><b>ইউনিয়নঃ</b> "+union_info.get("union_name")+"</td>"
						+ "<td style='text-align: center;'><b>উপজেলা/থানাঃ </b> "+upazila_info.get("upazila_name")+"</td>"
						+ "<td style='text-align: right;'><b>জেলাঃ</b> "+zilla_info.get("zilla_name")+"</td>"
						+"</tr></table>";

				resultString += "<table class=\"table page_1 table_hide\" id=\"table_1\" width=\"700\" border=\"1\" cellspacing=\"0\" style='font-size:11px;'><tr class='danger'>"
						+ "<td style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>সেবার ধরণ</td>"
						+ "<td style='text-align: center;border-top: 1px solid black;'>রোগীর লিঙ্গ</td>"
						+ "<td style='text-align: center;border-top: 1px solid black;'>সংখ্যা</td>"
						+ "</tr>";

				resultString +=  "<tr class='success'>"
						+ "<td rowspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সাধারণ রোগী</td>"
						+ "<td style='border-top: 1px solid black;'>পুরুষ</td>"
						+ "<td align='center' style='border-top: 1px solid black;'>"+gp_male+"</td>"
						+ "</tr>";

				resultString +=  "<tr>"
						+ "<td>মহিলা </td>"
						+ "<td align='center'>"+gp_female+"</td>"
						+ "</tr>";

				resultString += "</table>";
			}

			if(mis3Submit.equals("1") && supervisorId.equals("")){
				String[] date = start_date.split("-");
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));
				String sql_check_mis3_submission = "select * from "+ table_schema.getTable("table_mis3_report_submission") +" where "+table_schema.getColumn("", "mis3_report_submission_facility_id",new String[]{facilityId},"=")+" " +
						"and "+table_schema.getColumn("", "mis3_report_submission_report_year",new String[]{date[0]},"=")+" and "+table_schema.getColumn("", "mis3_report_submission_report_month",new String[]{date[1]},"=")+" and "+ table_schema.getColumn("", "mis3_report_submission_submission_status",new String[]{"0","1","3"},"in") +"";
				System.out.println(sql_check_mis3_submission);
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,sql_check_mis3_submission);
				ResultSet rs = dbObject.getResultSet();
				rs.last();
				int rowCount = rs.getRow();
				String submissionStatus= "";
				if(rowCount>0) {
					submissionStatus = rs.getString("submission_status");
				}
				rs.beforeFirst();
				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
				dbObject = null;

				if(rowCount>0) {
					if(submissionStatus.equals("1")) {
						resultString = "<div class='alert alert-success' style='text-align: center;'><strong>Your MIS3 report is approved</strong></div><br><br>" + resultString;
					}else{
						resultString = "<div class='alert alert-success' style='text-align: center;'><strong>You have submitted your MIS3 Report</strong></div><br><br>" + resultString;
					}
				}else {
					String submit_button = "";
					System.out.println("Submit Status="+submission_status);
					System.out.println("MIS3 Json="+mis3_json);

					submit_button = "<input type='hidden' id='jsonRequest' value='" + reportCred + "'><input type='button' id='submitMIS3' class='btn btn-success' value='Submit MIS3'><input type='hidden' id='total_mis3_data' value='" + mis3_json + "'>";
					if(rowCount_submission != 0)
						submit_button += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='submitted_data' value='" + mis3_submit_data + "'><input type='button' id='re_entry' class='btn btn-success' value='Edit Entry Data'>";
					else
						submit_button += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='submitted_data' value=''>";

					resultString = submit_button + "<br><br>" + (!comment.equals("")?"<b style='color:red'>Superviser Comment:"+comment:"") + "</b><br><br>" + resultString;
					System.out.println(submission_status);
					if(submission_status.equals("2"))
						resultString += "<input type='hidden' id='submissionStatus' value='"+submission_status+"'>";
				}
			}
			else if(!supervisorId.equals("") && !uType.equals("999") && !uType.equals("998") && !uType.equals("994")) {
				if(count_report_submit>0) {
					if(submission_status.equals("1"))
						resultString = "<div class='alert alert-success' style='text-align: center;'><strong>You have approved this MIS3 Report</strong></div><br>"+resultString;
					else if(submission_status.equals("2"))
						resultString = "<div class='alert alert-danger' style='text-align: center;'><strong>You reject this MIS3 Report and send for correction</strong></div><br>"+resultString;
					else if(submission_status.equals("0") || submission_status.equals("3"))
						resultString = "<strong>Give your feedback (If Any):</strong><br><textarea class='form-control' rows='5' id='supervisor_comment'>"+comment+"</textarea><br><input type='hidden' id='report1_zilla' value='"+zilla+"'><input type='hidden' id='facilityName' value='"+facilityId+"'><input type='hidden' id='mis3ReportMonth' value='"+monthSelect+"'><input type='button' id='approveMIS3' class='btn btn-success' value='Approve MIS3'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id='rejectMIS3' class='btn btn-danger' value='Reject MIS3'><br>"+resultString;
				}
				else
					resultString = "<div class='alert alert-info' style='text-align: center;'><strong>MIS3 yet not submitted.</strong></div>";
			}
//			else if(uType.equals("999") || uType.equals("998")) {
//				if(count_report_submit>0) {
//					if(submission_status.equals("1"))
//						resultString = "<div class='alert alert-success' style='text-align: center;'><strong>This MIS3 Report has been approved</strong></div><br>"+resultString;
//					else if(submission_status.equals("2"))
//						resultString = "<div class='alert alert-danger' style='text-align: center;'><strong>This MIS3 has been rejected and send for correction</strong></div><br>"+resultString;
//					else if(submission_status.equals("0"))
//						resultString = "<div class='alert alert-success' style='text-align: center;'><strong>This MIS3 has been submitted and wating for supervisor approval</strong></div><br>"+resultString;
//				}
//				else
//					resultString = "<div class='alert alert-info' style='text-align: center;'><strong>MIS3 yet not submitted.</strong></div>";
//			}

			//System.out.println(resultString);

			if(DateOption > 2018 && !providerType.equals("6") && !csba.equals("1") && !(db_csba.equals("1") && !csba.equals("1") && (providerType.equals("2") || providerType.equals("3"))))
			{
				System.out.println("Dateoption New =" + DateOption);

				new_mis3_json = mis3_json;

				//mis3_json.put("pill_new_center", pill_condom.get("pill_new_center"));

//			facilityName = reportCred.getString("facilityName");
//				facilityName = "";
				System.out.println("Facility Name: "+facilityName);
				new_mis3_json.put("union",union_info.get("union_name"));
				new_mis3_json.put("upazilla",upazila_info.get("upazila_name"));
				new_mis3_json.put("facility", facilityName);
				new_mis3_json.put("zilla", zilla_info.get("zilla_name"));
				new_mis3_json.put("bangla_month",bangla_month);
				new_mis3_json.put("bangla_year",bangla_year);

				if(mis3Submit.equals("1") && supervisorId.equals("")){
					String[] date = start_date.split("-");
					DBOperation dbOp = new DBOperation();
					DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(Integer.parseInt(zilla));
					String sql_check_mis3_submission = "select * from "+ table_schema.getTable("table_mis3_report_submission") +" where "+table_schema.getColumn("", "mis3_report_submission_facility_id",new String[]{facilityId},"=")+" " +
							"and "+table_schema.getColumn("", "mis3_report_submission_report_year",new String[]{date[0]},"=")+" and "+table_schema.getColumn("", "mis3_report_submission_report_month",new String[]{date[1]},"=")+" and "+ table_schema.getColumn("", "mis3_report_submission_submission_status",new String[]{"0","1","3"},"in") +"";
					System.out.println(sql_check_mis3_submission);
					dbObject = dbOp.dbCreateStatement(dbObject);
					dbObject = dbOp.dbExecute(dbObject,sql_check_mis3_submission);
					ResultSet rs = dbObject.getResultSet();
					rs.last();
					String submissionStatus = "";
					int rowCount = rs.getRow();
					if(rowCount>0) {
						submissionStatus = rs.getString("submission_status");
					}
					rs.beforeFirst();
					dbOp.closeStatement(dbObject);
					dbOp.closeConn(dbObject);
					dbObject = null;

					if(rowCount>0)
						//new_mis3_json.put("html_string", "<div class='alert alert-success' style='text-align: center; '><strong>You have submitted your MIS3 Report.</strong></div><br><br>");
						if(submissionStatus.equals("1")) {
							new_mis3_json.put("html_string", "<style>#mis3_entry_form{display: none;}</style><div class='alert alert-success' style='text-align: center; '><strong>Your MIS3 Report is approved.</strong></div><br><br>");
						}else {
							new_mis3_json.put("html_string", "<style>#mis3_entry_form{display: none;}</style><div class='alert alert-success' style='text-align: center; '><strong>You have submitted your MIS3 Report.</strong></div><br><br>");
						}
					else {
						String submit_button = "";
						submit_button = "<input type='hidden' id='jsonRequest' value='" + reportCred + "'><input type='button' id='submitMIS3' class='btn btn-success' value='Submit MIS3'><input type='hidden' id='total_mis3_data' value='" + mis3_json + "'>";
						if(rowCount_submission != 0)
							submit_button += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='submitted_data' value='" + mis3_submit_data + "'><input type='button' id='re_entry' class='btn btn-success' value='Edit Entry Data'>";
						else
							submit_button += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='submitted_data' value=''>";

						new_mis3_json.put("html_string", submit_button + "<br><br>" + (!comment.equals("")?"<b style='color:red'>Supervisor Comment:"+comment:"") + "</b><br><br>");
						System.out.println(submission_status);
						if(submission_status.equals("2"))
							new_mis3_json.put("html_submissionStatus", "<input type='hidden' id='submissionStatus' value='"+submission_status+"'>");
					}
				}
				else if(!supervisorId.equals("") && !uType.equals("999") && !uType.equals("998") && !uType.equals("994")) {
					if(count_report_submit>0) {
						if(submission_status.equals("1"))
							new_mis3_json.put("html_string", "<div class='alert alert-success' style='text-align: center;'><strong>You already approve this MIS3 Report</strong></div><br>");
						else if(submission_status.equals("2"))
							new_mis3_json.put("html_string", "<div class='alert alert-danger' style='text-align: center;'><strong>You reject this MIS3 Report and send for correction</strong></div><br>");
						else if(submission_status.equals("0") || submission_status.equals("3"))
							new_mis3_json.put("html_string", "<strong>Give your feedback (If Any):</strong><br><textarea class='form-control' rows='5' id='supervisor_comment'>"+comment+"</textarea><br><input type='hidden' id='report1_zilla' value='"+zilla+"'><input type='hidden' id='facilityName' value='"+facilityId+"'><input type='hidden' id='mis3ReportMonth' value='"+monthSelect+"'><input type='button' id='approveMIS3' class='btn btn-success' value='Approve MIS3'>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id='rejectMIS3' class='btn btn-danger' value='Reject MIS3'><br>");
					}
					else
						new_mis3_json.put("html_string", "<div class='alert alert-info' style='text-align: center;'><strong>MIS3 yet not submitted.</strong></div>");
				}

				System.out.println("New MIS3:"+new_mis3_json);
				resultString = new_mis3_json.toString();
				//return resultString = mis3_json.toString();

			}
		}
		catch(Exception e){
			e.printStackTrace();
			//System.out.println(e);
			resultString = "Bad Url Request";
		}


//        String MIS3Reporttype = "";
//        if(reportCred.has("mis3NewMonitoringTool")) {
//             MIS3Reporttype = reportCred.getString("mis3NewMonitoringTool");
//        }

		// Coded By Evana: 08/01/2019
		//if((pickYear > 2018)&&(MIS3Reporttype.equals("3")))
//		if((DateOption > 2018)&&(MIS3Reporttype.equals("3")))
		System.out.println(resultString);
		return resultString;

	}


	public static JSONObject getMIS3DataFromPreviousMonth(DBInfoHandler dbObject,String sql,String submission_status,JSONObject mis3_json,JSONObject mis3_submit_data) throws SQLException {
		DBOperation dbOp = new DBOperation();
        JSONObject temp_json = new JSONObject();

		dbObject = dbOp.dbCreateStatement(dbObject);
		dbObject = dbOp.dbExecute(dbObject, sql);
		ResultSet rs_submission = dbObject.getResultSet();
		rs_submission.last();
		rs_submission.beforeFirst();
		while (rs_submission.next()) {
					temp_json.put(rs_submission.getString("field_name"), rs_submission.getString("field_value"));
		}

		temp_json.put("pill_shukhi_total",(temp_json.has("pill_shukhi_total")?temp_json.getString("pill_shukhi_total"):"0"));
		temp_json.put("pill_apon_total",(temp_json.has("pill_apon_total")?temp_json.getString("pill_apon_total"):"0"));
		temp_json.put("condom_total",temp_json.has("condom_total")?temp_json.getString("condom_total"):"0");
		temp_json.put("new_inject_center",temp_json.has("new_inject_center")?temp_json.getString("new_inject_center"):"0");
		temp_json.put("new_inject_satelite",temp_json.has("new_inject_satelite")?temp_json.getString("new_inject_satelite"):"0");
		temp_json.put("old_inject_center",temp_json.has("old_inject_center")?temp_json.getString("old_inject_center"):"0");
		temp_json.put("old_inject_satelite",temp_json.has("old_inject_satelite")?temp_json.getString("old_inject_satelite"):"0");
		temp_json.put("iud_normal_center",temp_json.has("iud_normal_center")?temp_json.getString("iud_normal_center"):"0");
		temp_json.put("iud_normal_satelite",temp_json.has("iud_normal_satelite")?temp_json.getString("iud_normal_satelite"):"0");
		temp_json.put("iud_after_delivery_center",temp_json.has("iud_after_delivery_center")?temp_json.getString("iud_after_delivery_center"):"0");
		temp_json.put("iud_after_delivery_satelite",temp_json.has("iud_after_delivery_satelite")?temp_json.getString("iud_after_delivery_satelite"):"0");
		temp_json.put("implant_implanon",temp_json.has("implant_implanon")?temp_json.getString("implant_implanon"):"0");
		temp_json.put("implant_jadelle",temp_json.has("implant_jadelle")?temp_json.getString("implant_jadelle"):"0");
		temp_json.put("ecp_taken_center",temp_json.has("ecp_taken_center")?temp_json.getString("ecp_taken_center"):"0");
		temp_json.put("ecp_taken_satelite",temp_json.has("ecp_taken_satelite")?temp_json.getString("ecp_taken_satelite"):"0");
		temp_json.put("ancmisoprostolcenter",temp_json.has("ancmisoprostolcenter")?temp_json.getString("ancmisoprostolcenter"):"0");
		temp_json.put("deliverymisoprostol",temp_json.has("deliverymisoprostol")?temp_json.getString("deliverymisoprostol"):"0");
		temp_json.put("ancmisoprostolsatelite",temp_json.has("ancmisoprostolsatelite")?temp_json.getString("ancmisoprostolsatelite"):"0");
		temp_json.put("chlorehexidin",temp_json.has("chlorehexidin")?temp_json.getString("chlorehexidin"):"0");
		temp_json.put("ancmgso4center",temp_json.has("ancmgso4center")?temp_json.getString("ancmgso4center"):"0");
		temp_json.put("ancmgso4satelite",temp_json.has("ancmgso4satelite")?temp_json.getString("ancmgso4satelite"):"0");

		dbOp.closeStatement(dbObject);
		dbOp.closeConn(dbObject);
		dbObject = null;

		mis3_json.put("remaining_stock_shukhi",Integer.parseInt((temp_json.has("previous_stock_shukhi")?temp_json.getString("previous_stock_shukhi"):"0")) + Integer.parseInt((temp_json.has("stock_in_shukhi")?temp_json.getString("stock_in_shukhi"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_shukhi")?temp_json.getString("adjustment_stock_plus_shukhi"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_shukhi")?temp_json.getString("adjustment_stock_minus_shukhi"):"0")) + Integer.parseInt("-" + temp_json.getString("pill_shukhi_total")));
		mis3_json.put("remaining_stock_apon",Integer.parseInt((temp_json.has("previous_stock_apon")?temp_json.getString("previous_stock_apon"):"0")) + Integer.parseInt((temp_json.has("stock_in_apon")?temp_json.getString("stock_in_apon"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_apon")?temp_json.getString("adjustment_stock_plus_apon"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_apon")?temp_json.getString("adjustment_stock_minus_apon"):"0")) + Integer.parseInt("-" + temp_json.getString("pill_apon_total")));
		mis3_json.put("remaining_stock_condom",Integer.parseInt((temp_json.has("previous_stock_condom")?temp_json.getString("previous_stock_condom"):"0")) + Integer.parseInt((temp_json.has("stock_in_condom")?temp_json.getString("stock_in_condom"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_condom")?temp_json.getString("adjustment_stock_plus_condom"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_condom")?temp_json.getString("adjustment_stock_minus_condom"):"0")) + Integer.parseInt("-" + temp_json.getString("condom_total")));
		mis3_json.put("remaining_stock_viale",Integer.parseInt((temp_json.has("previous_stock_viale")?temp_json.getString("previous_stock_viale"):"0")) + Integer.parseInt((temp_json.has("stock_in_viale")?temp_json.getString("stock_in_viale"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_viale")?temp_json.getString("adjustment_stock_plus_viale"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_viale")?temp_json.getString("adjustment_stock_minus_viale"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(temp_json.getString("new_inject_center")) + Integer.parseInt(temp_json.getString("old_inject_center")) + Integer.parseInt(temp_json.getString("new_inject_satelite")) + Integer.parseInt(temp_json.getString("old_inject_satelite")))));
		mis3_json.put("remaining_stock_syringe",Integer.parseInt((temp_json.has("previous_stock_syringe")?temp_json.getString("previous_stock_syringe"):"0")) + Integer.parseInt((temp_json.has("stock_in_syringe")?temp_json.getString("stock_in_syringe"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_syringe")?temp_json.getString("adjustment_stock_plus_syringe"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_syringe")?temp_json.getString("adjustment_stock_minus_syringe"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(temp_json.getString("new_inject_center")) + Integer.parseInt(temp_json.getString("old_inject_center")) + Integer.parseInt(temp_json.getString("new_inject_satelite")) + Integer.parseInt(temp_json.getString("old_inject_satelite")))));
		mis3_json.put("remaining_stock_iud",Integer.parseInt((temp_json.has("previous_stock_iud")?temp_json.getString("previous_stock_iud"):"0")) + Integer.parseInt((temp_json.has("stock_in_iud")?temp_json.getString("stock_in_iud"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_iud")?temp_json.getString("adjustment_stock_plus_iud"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_iud")?temp_json.getString("adjustment_stock_minus_iud"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(temp_json.getString("iud_normal_center")) + Integer.parseInt(temp_json.getString("iud_normal_satelite")) + Integer.parseInt(temp_json.getString("iud_after_delivery_center")) + Integer.parseInt(temp_json.getString("iud_after_delivery_satelite")))));
		mis3_json.put("remaining_stock_implanon",Integer.parseInt((temp_json.has("previous_stock_implanon")?temp_json.getString("previous_stock_implanon"):"0")) + Integer.parseInt((temp_json.has("stock_in_implanon")?temp_json.getString("stock_in_implanon"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_implanon")?temp_json.getString("adjustment_stock_plus_implanon"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_implanon")?temp_json.getString("adjustment_stock_minus_implanon"):"0")) + Integer.parseInt("-" + temp_json.getString("implant_implanon")));
		mis3_json.put("remaining_stock_jadelle",Integer.parseInt((temp_json.has("previous_stock_jadelle")?temp_json.getString("previous_stock_jadelle"):"0")) + Integer.parseInt((temp_json.has("stock_in_jadelle")?temp_json.getString("stock_in_jadelle"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_jadelle")?temp_json.getString("adjustment_stock_plus_jadelle"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_jadelle")?temp_json.getString("adjustment_stock_minus_jadelle"):"0")) + Integer.parseInt("-" + temp_json.getString("implant_jadelle")));
		mis3_json.put("remaining_stock_ecp",Integer.parseInt((temp_json.has("previous_stock_ecp")?temp_json.getString("previous_stock_ecp"):"0")) + Integer.parseInt((temp_json.has("stock_in_ecp")?temp_json.getString("stock_in_ecp"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_ecp")?temp_json.getString("adjustment_stock_plus_ecp"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_ecp")?temp_json.getString("adjustment_stock_minus_ecp"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt((temp_json.has("ecp_taken_center")?temp_json.getString("ecp_taken_center"):"0")) + Integer.parseInt((temp_json.has("ecp_taken_satelite")?temp_json.getString("ecp_taken_satelite"):"0")))));
		mis3_json.put("remaining_stock_misoprostol",Integer.parseInt((temp_json.has("previous_stock_misoprostol")?temp_json.getString("previous_stock_misoprostol"):"0")) + Integer.parseInt((temp_json.has("stock_in_misoprostol")?temp_json.getString("stock_in_misoprostol"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_misoprostol")?temp_json.getString("adjustment_stock_plus_misoprostol"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_misoprostol")?temp_json.getString("adjustment_stock_minus_misoprostol"):"0")) + Integer.parseInt("-" + temp_json.getString("ancmisoprostolcenter")) + Integer.parseInt("-" + temp_json.getString("ancmisoprostolsatelite")) + Integer.parseInt("-" + temp_json.getString("deliverymisoprostol")));
		mis3_json.put("remaining_stock_mrm_pac",Integer.parseInt((temp_json.has("previous_stock_mrm_pac")?temp_json.getString("previous_stock_mrm_pac"):"0")) + Integer.parseInt((temp_json.has("stock_in_mrm_pac")?temp_json.getString("stock_in_mrm_pac"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_mrm_pac")?temp_json.getString("adjustment_stock_plus_mrm_pac"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_mrm_pac")?temp_json.getString("adjustment_stock_minus_mrm_pac"):"0")) + Integer.parseInt("-" + (temp_json.has("mrm_pac")?temp_json.getString("mrm_pac"):"0")));
		mis3_json.put("remaining_stock_chlorehexidin",Integer.parseInt((temp_json.has("previous_stock_chlorehexidin")?temp_json.getString("previous_stock_chlorehexidin"):"0")) + Integer.parseInt((temp_json.has("stock_in_chlorehexidin")?temp_json.getString("stock_in_chlorehexidin"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_chlorehexidin")?temp_json.getString("adjustment_stock_plus_chlorehexidin"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_chlorehexidin")?temp_json.getString("adjustment_stock_minus_chlorehexidin"):"0")) + Integer.parseInt("-" + temp_json.getString("chlorehexidin")));
		mis3_json.put("remaining_stock_mgso4",Integer.parseInt((temp_json.has("previous_stock_mgso4")?temp_json.getString("previous_stock_mgso4"):"0")) + Integer.parseInt((temp_json.has("stock_in_mgso4")?temp_json.getString("stock_in_mgso4"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_mgso4")?temp_json.getString("adjustment_stock_plus_mgso4"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_mgso4")?temp_json.getString("adjustment_stock_minus_mgso4"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(temp_json.getString("ancmgso4center")) + Integer.parseInt(temp_json.getString("ancmgso4satelite")))));
		mis3_json.put("remaining_stock_oxytocin",Integer.parseInt((temp_json.has("previous_stock_oxytocin")?temp_json.getString("previous_stock_oxytocin"):"0")) + Integer.parseInt((temp_json.has("stock_in_oxytocin")?temp_json.getString("stock_in_oxytocin"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_oxytocin")?temp_json.getString("adjustment_stock_plus_oxytocin"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_oxytocin")?temp_json.getString("adjustment_stock_minus_oxytocin"):"0")) + Integer.parseInt("-" + (temp_json.has("deliverycxytocin")?temp_json.getString("deliverycxytocin"):"0")));
		mis3_json.put("remaining_stock_mnp",Integer.parseInt((temp_json.has("previous_stock_mnp")?temp_json.getString("previous_stock_mnp"):"0")) + Integer.parseInt((temp_json.has("stock_in_mnp")?temp_json.getString("stock_in_mnp"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_mnp")?temp_json.getString("adjustment_stock_plus_mnp"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_mnp")?temp_json.getString("adjustment_stock_minus_mnp"):"0")) + Integer.parseInt("-" + (temp_json.has("mnp")?temp_json.getString("mnp"):"0")));
		mis3_json.put("remaining_stock_sanitary_pad",Integer.parseInt((temp_json.has("previous_stock_sanitary_pad")?temp_json.getString("previous_stock_sanitary_pad"):"0")) + Integer.parseInt((temp_json.has("stock_in_sanitary_pad")?temp_json.getString("stock_in_sanitary_pad"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_sanitary_pad")?temp_json.getString("adjustment_stock_plus_sanitary_pad"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_sanitary_pad")?temp_json.getString("adjustment_stock_minus_sanitary_pad"):"0")) + Integer.parseInt("-" + (temp_json.has("sanitary_pad")?temp_json.getString("sanitary_pad"):"0")));
		mis3_json.put("remaining_stock_mr_kit",Integer.parseInt((temp_json.has("previous_stock_mr_kit")?temp_json.getString("previous_stock_mr_kit"):"0")) + Integer.parseInt((temp_json.has("stock_in_mr_kit")?temp_json.getString("stock_in_mr_kit"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_mr_kit")?temp_json.getString("adjustment_stock_plus_mr_kit"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_mr_kit")?temp_json.getString("adjustment_stock_minus_mr_kit"):"0")) + Integer.parseInt("-" + (temp_json.has("mr_kit")?temp_json.getString("mr_kit"):"0")));
		mis3_json.put("remaining_stock_delivery_kit",Integer.parseInt((temp_json.has("previous_stock_delivery_kit")?temp_json.getString("previous_stock_delivery_kit"):"0")) + Integer.parseInt((temp_json.has("stock_in_delivery_kit")?temp_json.getString("stock_in_delivery_kit"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_delivery_kit")?temp_json.getString("adjustment_stock_plus_delivery_kit"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_delivery_kit")?temp_json.getString("adjustment_stock_minus_delivery_kit"):"0")) + Integer.parseInt("-" + (temp_json.has("delivery_kit")?temp_json.getString("delivery_kit"):"0")));
		mis3_json.put("remaining_stock_dds_kit",Integer.parseInt((temp_json.has("previous_stock_dds_kit")?temp_json.getString("previous_stock_dds_kit"):"0")) + Integer.parseInt((temp_json.has("stock_in_dds_kit")?temp_json.getString("stock_in_dds_kit"):"0")) + Integer.parseInt((temp_json.has("adjustment_stock_plus_dds_kit")?temp_json.getString("adjustment_stock_plus_dds_kit"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_stock_minus_dds_kit")?temp_json.getString("adjustment_stock_minus_dds_kit"):"0")) + Integer.parseInt("-" + (temp_json.has("dds_kit")?temp_json.getString("dds_kit"):"0")));
		mis3_json.put("remaining_injection_antinatal",Integer.parseInt((temp_json.has("previous_injection_antinatal")?temp_json.getString("previous_injection_antinatal"):"0")) + Integer.parseInt((temp_json.has("stock_injection_antinatal")?temp_json.getString("stock_injection_antinatal"):"0")) + Integer.parseInt((temp_json.has("adjust_stock_injection_plus_antinatal")?temp_json.getString("adjust_stock_injection_plus_antinatal"):"0")) + Integer.parseInt("-" + (temp_json.has("adjust_stock_injection_minus_antinatal")?temp_json.getString("adjust_stock_injection_minus_antinatal"):"0")));
		mis3_json.put("remaining_injection_jentamysin",Integer.parseInt((temp_json.has("previous_injection_jentamysin")?temp_json.getString("previous_injection_jentamysin"):"0")) + Integer.parseInt((temp_json.has("stock_injection_jentamysin")?temp_json.getString("stock_injection_jentamysin"):"0")) + Integer.parseInt((temp_json.has("adjustment_injection_plus_jentamysin")?temp_json.getString("adjustment_injection_plus_jentamysin"):"0")) + Integer.parseInt("-" + (temp_json.has("adjustment_injection_minus_jentamysin")?temp_json.getString("adjustment_injection_minus_jentamysin"):"0")) + Integer.parseInt("-" + (temp_json.has("cmonth_distribution_injection_jentamysin")?temp_json.getString("cmonth_distribution_injection_jentamysin"):"0")));

		return mis3_json;

	}

}
