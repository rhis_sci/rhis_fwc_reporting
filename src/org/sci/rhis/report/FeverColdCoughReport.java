package org.sci.rhis.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

public class FeverColdCoughReport {
    public static String getResultSet(JSONObject reportCred){

        String resultString = "";

        JSONObject resultJSON = null;
        try {
            resultJSON = new JSONObject(ServiceStatistics.getResultSet(reportCred).split(" table/graph/map ")[1].replaceAll("[\\[\\]]", "").replace("}},{", "},"));
            System.out.println(resultJSON);
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "";
            if(reportCred.getString("reportViewType").equals("1"))
                table_header += "<thead><tr><th rowspan=2>Zilla Name</th>";
            else if(reportCred.getString("reportViewType").equals("2"))
                table_header += "<thead><tr><th rowspan=2>Upazila Name</th>";
            else if(reportCred.getString("reportViewType").equals("3"))
                table_header += "<thead><tr><th rowspan=2>Union Name</th>";
            else
                table_header += "<thead><tr><th rowspan=2>Facility Name</th><th rowspan=2>Upazila Name</th><th rowspan=2>Union Name</th>";

            table_header += "<th colspan=4 style='text-align: center;background: #e5a8a8;'>Fever</th>" +
                    "<th colspan=3 style='text-align: center;background: #a4e7a4;'>Cold</th>" +
                    "<th colspan=3 style='text-align: center;background: #9194e1;'>Cough</th>" +
                    "<th colspan=4 style='text-align: center;background: #eda7ed;'>Shortness of Breath</th>" +
                    "<th colspan=3 style='text-align: center;background: #e7cd9d;'>Headache</th>" +
                    "<th colspan=3 style='text-align: center;background: skyblue;'>Sore Throat</th>" +
                    "</tr>";

            table_header += "<tr><th style='background: #e5a8a8;'>0-59 Year</th>" +
                    "<th style='background: #e5a8a8;'>60+ Year</th>" +
                    "<th style='background: #e5a8a8;'>Pregnant<br />Women</th>" +
                    "<th style='background: #e5a8a8;'>Total</th>" +
                    "<th style='background: #a4e7a4;'>0-59 Year</th>" +
                    "<th style='background: #a4e7a4;'>60+ Year</th>" +
                    "<th style='background: #a4e7a4;'>Total</th>" +
                    "<th style='background: #9194e1;'>0-59 Year</th>" +
                    "<th style='background: #9194e1;'>60+ Year</th>" +
                    "<th style='background: #9194e1;'>Total</th>" +
                    "<th style='background: #eda7ed;'>0-59 Year</th>" +
                    "<th style='background: #eda7ed;'>60+ Year</th>" +
                    "<th style='background: #eda7ed;'>Pregnant<br />Women</th>" +
                    "<th style='background: #eda7ed;'>Total</th>" +
                    "<th style='background: #e7cd9d;'>0-59 Year</th>" +
                    "<th style='background: #e7cd9d;'>60+ Year</th>" +
                    "<th style='background: #e7cd9d;'>Total</th>" +
                    "<th style='background: skyblue;'>0-59 Year</th>" +
                    "<th style='background: skyblue;'>60+ Year</th>" +
                    "<th style='background: skyblue;'>Total</th>";
            table_header += "</tr></thead>";

            JSONObject getGEOName = new JSONObject(resultJSON.getString("Fever_0-59"));
            Iterator<String> iterator = getGEOName.keys();

            ObjectMapper mapper = new ObjectMapper();
            Map<String, Map<String, String>> resultMap = mapper.readValue(resultJSON.toString(), Map.class);

            Integer c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14;
            c1=c2=c3=c4=c5=c6=c7=c8=c9=c10=c11=c12=c13=c14=0;

            String table_body = "<tbody>";
            while (iterator.hasNext()) {
                String key = iterator.next();

                c1+=Integer.parseInt(resultMap.get("Fever_0-59").get(key));
                c2+=Integer.parseInt(resultMap.get("Fever_60+").get(key));
                c3+=Integer.parseInt(resultMap.get("Fever_PW").get(key));
                c4+=Integer.parseInt(resultMap.get("Cold_0-59").get(key));
                c5+=Integer.parseInt(resultMap.get("Cold_60+").get(key));
                c6+=Integer.parseInt(resultMap.get("Cough_0-59").get(key));
                c7+=Integer.parseInt(resultMap.get("Cough_60+").get(key));
                c8+=Integer.parseInt(resultMap.get("Shortness_Breath_0-59").get(key));
                c9+=Integer.parseInt(resultMap.get("Shortness_Breath_60+").get(key));
                c10+=Integer.parseInt(resultMap.get("Shortness_Breath_PW").get(key));
                c11+=Integer.parseInt(resultMap.get("Headache_0-59").get(key));
                c12+=Integer.parseInt(resultMap.get("Headache_60+").get(key));
                c13+=Integer.parseInt(resultMap.get("Sore_Throat_0-59").get(key));
                c14+=Integer.parseInt(resultMap.get("Sore_Throat_60+").get(key));

                table_body += "<tr>";

                if(reportCred.getString("reportViewType").equals("1") || reportCred.getString("reportViewType").equals("2") || reportCred.getString("reportViewType").equals("3"))
                    table_body += "<td>"+key+"</td>";
                else
                    table_body += "<td>"+key.split("_")[0]+"</td><td align='center'>"+key.split("_")[1]+"</td><td align='center'>"+key.split("_")[2]+"</td>";

                table_body += "<td align='center' style='background: #e5a8a8;'>"+resultMap.get("Fever_0-59").get(key)+"</td>" +
                        "<td align='center' style='background: #e5a8a8;'>"+resultMap.get("Fever_60+").get(key)+"</td>" +
                        "<td align='center' style='background: #e5a8a8;'>"+resultMap.get("Fever_PW").get(key)+"</td>" +
                        "<td align='center' style='background: #e5a8a8;'>"+(Integer.parseInt(resultMap.get("Fever_0-59").get(key))+Integer.parseInt(resultMap.get("Fever_60+").get(key))+Integer.parseInt(resultMap.get("Fever_PW").get(key)))+"</td>" +
                        "<td align='center' style='background: #a4e7a4;'>"+resultMap.get("Cold_0-59").get(key)+"</td>" +
                        "<td align='center' style='background: #a4e7a4;'>"+resultMap.get("Cold_60+").get(key)+"</td>" +
                        "<td align='center' style='background: #a4e7a4;'>"+(Integer.parseInt(resultMap.get("Cold_0-59").get(key))+Integer.parseInt(resultMap.get("Cold_60+").get(key)))+"</td>" +
                        "<td align='center' style='background: #9194e1;'>"+resultMap.get("Cough_0-59").get(key)+"</td>" +
                        "<td align='center' style='background: #9194e1;'>"+resultMap.get("Cough_60+").get(key)+"</td>" +
                        "<td align='center' style='background: #9194e1;'>"+(Integer.parseInt(resultMap.get("Cough_0-59").get(key))+Integer.parseInt(resultMap.get("Cough_60+").get(key)))+"</td>" +
                        "<td align='center' style='background: #eda7ed;'>"+resultMap.get("Shortness_Breath_0-59").get(key)+"</td>" +
                        "<td align='center' style='background: #eda7ed;'>"+resultMap.get("Shortness_Breath_60+").get(key)+"</td>" +
                        "<td align='center' style='background: #eda7ed;'>"+resultMap.get("Shortness_Breath_PW").get(key)+"</td>" +
                        "<td align='center' style='background: #eda7ed;'>"+(Integer.parseInt(resultMap.get("Shortness_Breath_0-59").get(key))+Integer.parseInt(resultMap.get("Shortness_Breath_60+").get(key))+Integer.parseInt(resultMap.get("Shortness_Breath_PW").get(key)))+"</td>" +
                        "<td align='center' style='background: #e7cd9d;'>"+resultMap.get("Headache_0-59").get(key)+"</td>" +
                        "<td align='center' style='background: #e7cd9d;'>"+resultMap.get("Headache_60+").get(key)+"</td>" +
                        "<td align='center' style='background: #e7cd9d;'>"+(Integer.parseInt(resultMap.get("Headache_0-59").get(key))+Integer.parseInt(resultMap.get("Headache_60+").get(key)))+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_0-59").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_60+").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+(Integer.parseInt(resultMap.get("Sore_Throat_0-59").get(key))+Integer.parseInt(resultMap.get("Sore_Throat_60+").get(key)))+"</td>";

                table_body += "</tr>";
            }
            table_body += "</tbody>";

            String calspan = "";
            if(reportCred.getString("reportViewType").equals("4"))
                calspan = "colspan=3";

            String table_footer = "<tfoot><tr><th "+calspan+" style='text-align: right;'>Total:</th>" +
                    "<th style='text-align: center;background: #e5a8a8;'>"+c1+"</th>" +
                    "<th style='text-align: center;background: #e5a8a8;'>"+c2+"</th>" +
                    "<th style='text-align: center;background: #e5a8a8;'>"+c3+"</th>" +
                    "<th style='text-align: center;background: #e5a8a8;'>"+(c1+c2+c3)+"</th>" +
                    "<th style='text-align: center;background: #a4e7a4;'>"+c4+"</th>" +
                    "<th style='text-align: center;background: #a4e7a4;'>"+c5+"</th>" +
                    "<th style='text-align: center;background: #a4e7a4;'>"+(c4+c5)+"</th>" +
                    "<th style='text-align: center;background: #9194e1;'>"+c6+"</th>" +
                    "<th style='text-align: center;background: #9194e1;'>"+c7+"</th>" +
                    "<th style='text-align: center;background: #9194e1;'>"+(c6+c7)+"</th>" +
                    "<th style='text-align: center;background: #eda7ed;'>"+c8+"</th>" +
                    "<th style='text-align: center;background: #eda7ed;'>"+c9+"</th>" +
                    "<th style='text-align: center;background: #eda7ed;'>"+c10+"</th>" +
                    "<th style='text-align: center;background: #eda7ed;'>"+(c8+c9+c10)+"</th>" +
                    "<th style='text-align: center;background: #e7cd9d;'>"+c11+"</th>" +
                    "<th style='text-align: center;background: #e7cd9d;'>"+c12+"</th>" +
                    "<th style='text-align: center;background: #e7cd9d;'>"+(c11+c12)+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c13+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c14+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+(c13+c14)+"</th></tr>" +
                    "</tfoot>";

            resultString = table+table_header+table_body+table_footer+"</table>";
            System.out.println(resultString);
//			System.out.println(resultString.split(" table/graph/map ")[1].replaceAll("[\\[\\]]", ""));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

//		resultString = resultString.split(" table/graph/map ")[0];

        return resultString;
    }

    public static String resultTrend(JSONObject reportCred){
        String resultString = "";
        try{
            String zilla = reportCred.getString("report1_zilla");
            String upazila = reportCred.getString("report1_upazila");
            String union = reportCred.getString("report1_union");
            String facilityId = reportCred.getString("facilityId");
            String report1_graphViewType = reportCred.getString("report1_graphViewType");

            String start_date = "";
            String end_date = "";

            if(report1_graphViewType.equals("1"))
            {
                String yearSelect = reportCred.getString("report1_year");
                start_date = yearSelect + "-01-01";
                if(Integer.parseInt(yearSelect)== Calendar.getInstance().get(Calendar.YEAR)){
                    Calendar cal = Calendar.getInstance();
                    end_date = yearSelect + "-"+new SimpleDateFormat("MM").format(cal.getTime())+"-"+CalendarDate.daysInMonth(Integer.parseInt(yearSelect), Integer.parseInt(new SimpleDateFormat("MM").format(cal.getTime())));
                }
                else
                    end_date = yearSelect + "-12-31";
            }
            else if(report1_graphViewType.equals("2"))
            {
                String monthSelect = reportCred.getString("report1_month");
                String[] date = monthSelect.split("-");
                int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                start_date = monthSelect + "-01";
                end_date = monthSelect + "-"+daysInMonth;
            }
            else
            {
                start_date = reportCred.getString("report1_start_date");
                end_date = reportCred.getString("report1_end_date");
            }

            String sql = "select ";
            if(report1_graphViewType.equals("1")) {
                sql += "date_part('year', report_date) as \"Year\", " +
                        "to_char(report_date,'Mon') as \"Month\", ";
            }
            else if(report1_graphViewType.equals("2")) {
                sql += "date_part('year', report_date) as \"Year\", " +
                        "to_char(report_date,'Mon') as \"Month\", " +
                        "(date_trunc('week', report_date + 1)::date - 2 || ' - ' || date_trunc('week', report_date + 1)::date + 4) AS \"Week\", ";
            }
            else
                sql += "report_date as \"Date\", ";

            sql += "sum((gp_data->>'Male')::int) + sum((gp_data->>'Female')::int) as \"Total General Patient\", " +
                    "sum((gp_data->>'Fever_60+')::int) + sum((gp_data->>'Fever_0-59')::int) as \"Fever\", " +
                    "sum((gp_data->>'Cold_60+')::int) + sum((gp_data->>'Cold_0-59')::int) as \"Cold\", " +
                    "sum((gp_data->>'Cough_60+')::int) + sum((gp_data->>'Cough_0-59')::int) as \"Cough\", " +
                    "sum((gp_data->>'Shortness_Breath_60+')::int) + sum((gp_data->>'Shortness_Breath_0-59')::int) as \"Shortness_Breath\", " +
                    "sum((gp_data->>'Headache_60+')::int) + sum((gp_data->>'Headache_0-59')::int) as \"Headache\", " +
                    "sum((gp_data->>'Sore_Throat_60+')::int) + sum((gp_data->>'Sore_Throat_0-59')::int) as \"Sore_Throat\" " +
                    "from emis_facility_aggregate_data ad " +
                    "inner join emis_facility_all_facility_provider fp using(providerid) " +
                    "inner join emis_facility_all_facility_registry fr on fr.facilityid=fp.facility_id and fr.zillaid=ad.zillaid and fr.upazilaid=ad.upazilaid ";

            sql += "where ";

            if(zilla!="null" && (upazila=="null" || upazila.isEmpty()))
                sql += "fr.zillaid="+zilla+" and ";
            else if (upazila!="null" && (union=="null" || union.isEmpty()))
                sql += "fr.zillaid="+zilla+" and fr.upazilaid="+upazila+" and ";
            else if (union!="null" && (facilityId=="null" || facilityId.isEmpty()))
                sql += "fr.zillaid="+zilla+" and fr.upazilaid="+upazila+" and fr.unionid="+union+" and ";
            else if(facilityId!="null")
                sql += "fr.facilityid = "+facilityId+" and ";

            sql += "(end_date::date>='"+start_date+"' or end_date is null) and " +
                    "(case when is_active=2 and end_date::date>='"+start_date+"' and end_date::date<='"+end_date+"' then report_date between '"+start_date+"' and end_date::date " +
                    "when is_active=1 and start_date::date>='"+start_date+"' then report_date between start_date::date and '"+end_date+"' " +
                    "else report_date between '"+start_date+"' and '"+end_date+"' end) ";

            if(report1_graphViewType.equals("1")) {
                sql += "group by 1,2";
            }
            else if(report1_graphViewType.equals("2")) {
                sql += "group by 1,2,3";
            }
            else {
                sql += "and extract('dow' from report_date) not in (5) " +
                        "group by 1";
            }

            System.out.println(sql);

            resultString = GetResultSet.getFeverColdCoughTrend(sql, report1_graphViewType);

        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e);
        }

        return resultString;
    }

    public static JSONObject resultForDHIS2(JSONObject reportCred){
        JSONObject data = new JSONObject();
        JSONArray data_array = new JSONArray();

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        try {
            String start_date;
            String end_date;
            String zilla = reportCred.getString("zillaid");
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");

            if(reportCred.has("startDate") && reportCred.has("endDate")) {
                start_date = reportCred.getString("startDate");
                end_date = reportCred.getString("endDate");

            }else {

                cal.add(Calendar.DATE, -1);
                end_date = date_format.format(cal.getTime());

                cal.add(Calendar.DATE, -30);
                start_date = "2015-01-01"; //date_format.format(cal.getTime());
            }



            String sql = "select replace(report_date::text, '-', '') as period, fr.facilityid as facility_id, fr.facility_name as facility_name, fr.zillaid as district_id, fr.upazilaid as upazila_id, fr.unionid as union_id " +
                    ",sum((gp_data->>'Fever_60+')::int) as \"Fever_60+\" " +
                    ",sum((gp_data->>'Fever_0-59')::int) as \"Fever_0-59\" " +
                    ",sum((mnc_data->>'Fever_PW')::int) as \"Fever_PW\" " +
                    ",sum((gp_data->>'Cold_60+')::int) as \"Cold_60+\" " +
                    ",sum((gp_data->>'Cold_0-59')::int) as \"Cold_0-59\" " +
                    ",sum((gp_data->>'Cough_60+')::int) as \"Cough_60+\" " +
                    ",sum((gp_data->>'Cough_0-59')::int) as \"Cough_0-59\" " +
                    ",sum((gp_data->>'Shortness_Breath_60+')::int) as \"Shortness_Breath_60+\" " +
                    ",sum((gp_data->>'Shortness_Breath_0-59')::int) as \"Shortness_Breath_0-59\" " +
                    ",sum((mnc_data->>'Shortness_Breath_PW')::int) as \"Shortness_Breath_PW\" " +
                    ",sum((gp_data->>'Headache_60+')::int) as \"Headache_60+\" " +
                    ",sum((gp_data->>'Headache_0-59')::int) as \"Headache_0-59\" " +
                    ",sum((gp_data->>'Sore_Throat_60+')::int) as \"Sore_Throat_60+\" " +
                    ",sum((gp_data->>'Sore_Throat_0-59')::int) as \"Sore_Throat_0-59\" " +
                    "from emis_facility_aggregate_data ad " +
                    "inner join emis_facility_all_facility_provider fp using(providerid) " +
                    "inner join emis_facility_all_facility_registry fr on fr.facilityid=fp.facility_id and fr.zillaid=ad.zillaid and fr.upazilaid=ad.upazilaid " +
                    "where " +
                    "fr.zillaid="+zilla+" and " +
                    "(end_date::date>='"+start_date+"' or end_date is null) and " +
                    "(case when is_active=2 and end_date::date>='"+start_date+"' and end_date::date<='"+end_date+"' then report_date between '"+start_date+"' and end_date::date " +
                    "when is_active=1 and start_date::date>='"+start_date+"' then report_date between start_date::date and '"+end_date+"' " +
                    "else report_date between '"+start_date+"' and '"+end_date+"' end) " +
                    "group by 1,2,3,4,5,6";

            System.out.println(sql);

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject,sql);
            ResultSet rs = dbObject.getResultSet();

            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            while (rs.next()){
                JSONObject flu_data_info = new JSONObject();
                JSONArray flu_data = new JSONArray();

                for (int i =7; i<=columnCount; i++){
                    JSONObject row_data = new JSONObject();
                    String value = "0";
                    if(rs.getString(metadata.getColumnName(i))!=null)
                        value = rs.getString(metadata.getColumnName(i));
                    row_data.put(metadata.getColumnName(i), value);
                    flu_data.put(row_data);
                }

                for (int i =1; i<=6; i++){
                    flu_data_info.put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));
                }
                flu_data_info.put("dataValues", flu_data);

                data_array.put(flu_data_info);
            }

            data.put("payload", data_array);
            System.out.println(data);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return data;
    }
}
