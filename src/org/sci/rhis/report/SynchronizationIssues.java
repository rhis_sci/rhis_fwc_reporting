package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.ConfInfoRetrieve;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.util.AES;

import java.util.Properties;

public class SynchronizationIssues {

    public static String sql;
    public static String zilla;
    public static String upazila;
    public static String type;

    public static String getResultSet(JSONObject reportCred) {

        String resultString = "";

        try {
            zilla = reportCred.getString("report1_zilla");
            upazila = reportCred.getString("report1_upazila");
            type = reportCred.getString("type");
            sql = getSql(zilla);

            System.out.println(sql);

            resultString = GetResultSet.getFinalResult(sql, zilla, upazila, type);

        } catch (Exception e) {
            System.out.println(e);
        }

        return resultString;
    }

    public static String getSql(String zilla){
        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);

        DBInfoHandler detailFacility = new DBInfoHandler();
        Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
        String[] dbDetails = AES.decrypt(prop.getProperty("FACILITY_" + zilla), "rhis_db_info").split(",");

        String[] host_name = (dbDetails[0].split("/"))[2].split(":");

        sql = "SELECT format('%s (%s)'::text, " + table_schema.getColumn("table", "providerdb_provname") + "," +
                "        CASE" +
//                "            WHEN " + table_schema.getColumn("table", "node_details_provtype") + " = 2 THEN 'HA-CSBA'::text" +
//                "            WHEN " + table_schema.getColumn("table", "node_details_provtype") + " = 3 THEN 'FWA-CSBA'::text" +
                "            WHEN " + table_schema.getColumn("table", "node_details_provtype") + " = 4 THEN 'FWV'::text" +
                "            WHEN " + table_schema.getColumn("table", "node_details_provtype") + " = 5 THEN 'SACMO-DGFP'::text" +
                "            WHEN " + table_schema.getColumn("table", "node_details_provtype") + " = 6 THEN 'SACMO-DGHS'::text" +
                "            WHEN " + table_schema.getColumn("table", "node_details_provtype") + " = 17 THEN 'Midwife'::text" +
                "            WHEN " + table_schema.getColumn("table", "node_details_provtype") + " = 101 THEN 'PARAMEDIC'::text" +
                "            ELSE NULL::text" +
                "        END) AS format," +
                "    " + table_schema.getColumn("table", "node_details_provcode") + "," +
                "        CASE " +
                "            WHEN sym_node_host.heartbeat_time is null THEN 'NO HEARTBAT'::text" +
                "            ELSE sym_node_host.heartbeat_time::text end AS \"HEARTBEAT\","  +
                "        CASE" +
                "            WHEN " + table_schema.getColumn("login", "last_login_detail_login_time") + " IS NULL THEN 'NO RECENT LOGIN'::text" +
                "            ELSE " + table_schema.getColumn("login", "last_login_detail_login_time") + "::text" +
                "        END AS \"LAST LOGIN\"," +
                "    " + table_schema.getColumn("login", "last_login_detail_login_success") + "::text AS \"LOGIN STATUS\"," +
                "    q1.unsync AS \"UNSYNC DATA\"," +
                "    q1.node_id" +
                "   FROM ( SELECT sym_outgoing_batch.node_id," +
                "            count(*) AS unsync" +
                "           FROM sym_outgoing_batch" +
                "          WHERE sym_outgoing_batch.status <> 'OK'::bpchar" +
                "          GROUP BY sym_outgoing_batch.node_id" +
                "          ORDER BY (count(*)) DESC) q1" +
                "     JOIN " + table_schema.getTable("table_node_details") + " ON q1.node_id::text = " + table_schema.getColumn("table", "node_details_id") + "::text" +
                "     LEFT JOIN sym_node_host USING (node_id)" +
                "     JOIN " + table_schema.getTable("table_providerdb") + " USING (" + table_schema.getColumn("", "providerdb_provcode") + ", " + table_schema.getColumn("", "providerdb_provtype") + ")" +
                "     LEFT JOIN ( SELECT " + table_schema.getColumn("tt", "last_login_detail_provider_id") + "," +
                "            " + table_schema.getColumn("tt", "last_login_detail_login_time") + "," +
                "            " + table_schema.getColumn("tt", "last_login_detail_login_success") + "" +
                "           FROM dblink('host=" + host_name[0] + " port=" + host_name[1]+ " dbname=" + dbDetails[1] + " user=" + dbDetails[2] + " password=" + dbDetails[3] + "'::text, 'select " + table_schema.getColumn("", "last_login_detail_provider_id") + ", " + table_schema.getColumn("", "last_login_detail_login_time") + ", " + table_schema.getColumn("", "last_login_detail_login_success") +
                " from " + table_schema.getTable("table_last_login_detail") + " l1" + " inner join " +
                "(select " + table_schema.getColumn("", "last_login_detail_provider_id") + ", max(" + table_schema.getColumn("", "last_login_detail_login_time") +
                ") as login_time from " + table_schema.getTable("table_last_login_detail") + " group by " + table_schema.getColumn("", "last_login_detail_provider_id") +
                " ) l2 using (" + table_schema.getColumn("", "last_login_detail_provider_id") + ", " + table_schema.getColumn("", "last_login_detail_login_time") + ")'::text) tt(" + table_schema.getColumn("", "last_login_detail_provider_id") + " integer, " + table_schema.getColumn("", "last_login_detail_login_time") + " timestamp without time zone, " + table_schema.getColumn("", "last_login_detail_login_success") + " boolean)) login ON " + table_schema.getColumn("table", "node_details_provcode") + " = " + table_schema.getColumn("login", "last_login_detail_provider_id") + "" +
                "  where " + table_schema.getColumn("table", "providerdb_active", new String[]{Integer.toString(1)}, "=")+
                " and "+ table_schema.getColumn("table","providerdb_provtype", new String[]{Integer.toString(2),Integer.toString(3)},"notin")+ " ORDER BY q1.unsync DESC";


        return sql ;

    }

}
