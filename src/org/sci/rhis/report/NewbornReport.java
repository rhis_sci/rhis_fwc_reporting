package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

public class NewbornReport {

    public static String div;
    public static String zilla;
    public static String upazila;
    public static String union;
    public static String sql;

    public static String date_type;
    public static String monthSelect;
    public static String yearSelect;
    public static String start_date;
    public static String end_date;
    public static String tableName;
    public static String condition;


    public static List<JSONObject> getResultSet(JSONObject reportCred) {

        List<JSONObject> resultString = new ArrayList<JSONObject>();
        zilla = reportCred.getString("report1_zilla");
        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);

        try {
            upazila = reportCred.getString("report1_upazila");
            if (reportCred.has("report1_union")) {
                union = reportCred.getString("report1_union");
            }
            date_type = reportCred.getString("report1_dateType");

            if (reportCred.has("report1_division")) {
                div = reportCred.getString("report1_division");
            }

            //System.out.println(union);

            //db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));


            if (date_type.equals("1")) {
                monthSelect = reportCred.getString("report1_month");
                String[] date = monthSelect.split("-");
                //YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                //int daysInMonth = yearMonthObject.lengthOfMonth();
                int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                start_date = monthSelect + "-01";
                end_date = monthSelect + "-" + daysInMonth;
            } else if (date_type.equals("3")) {
                yearSelect = reportCred.getString("report1_year");
                start_date = yearSelect + "-01-01";
                end_date = yearSelect + "-12-31";
            } else {
                start_date = reportCred.getString("report1_start_date");
                end_date = reportCred.getString("report1_end_date");
            }

            String geo_join_sql = null;
            String geo_sql = null;
            String condition_geo_sql = null;
            String newborn_sql = null;
            String inner_sql = null;
            String newborn_part;
//                geo_sql = "select date_part('year'::text, report_date) AS \"Year\", to_char(report_date::timestamp with time zone, 'MONTH'::text) AS month,z.zillanameeng,";


                newborn_part = ",sum((newborn_data->>'Livebirth')::int) as \"Livebirth\","
                        + "sum((newborn_data->>'chlorehexidin')::int) as \"Chlorehexidin\","
                        + "sum((newborn_data->>'breastfeed')::int) as \"Breastfeed\","
                        + "sum((newborn_data->>'skintouch')::int) as \"Skintouch\" ";

                geo_sql = "select z.zillanameeng";
                String division_part = "select d.divisioneng,z.zillanameeng" + newborn_part+" from division d inner join"+table_schema.getTable("table_zilla")+" z on d.id  = z.divid";
                String zilla_part = newborn_part+" from zilla z" ;

                geo_join_sql = " inner join " + table_schema.getTable("table_upazila") + " u using (" + table_schema.getColumn("", "zilla_zillaid")+
                        ") inner join " + table_schema.getTable("table_unions") + " un using(" + table_schema.getColumn("", "zilla_zillaid") + "," + table_schema.getColumn("", "upazila_upazilaid") +
                        ") left join newborn_aggregate_data n on z.zillaid = n.zillaid and u.upazilaid = n.upazilaid and un.unionid = n.unionid" +
                        " and report_date between '" + start_date + "' and '" + end_date + "'";

                condition_geo_sql =  " where z.zillaid =" + zilla ;

                //division All
                if(div.equals("0"))
                    newborn_sql = division_part+geo_join_sql+" group by  1,2 order by 1,2";

                //zilla All
                if(zilla.equals("0")){
                    newborn_sql = division_part+geo_join_sql+" where z.divid = "+div + " group by  1,2 order by 1,2";

                //upazilla All
                }else if (upazila.equals("0")){
                    newborn_sql = geo_sql + ",u.upazilanameeng" +zilla_part+geo_join_sql+ condition_geo_sql + " group by  1,2 order by 1,2";
                // Union All
                } else if(union.equals("0")){
                    condition_geo_sql += " and u.upazilaid ="+ upazila;
                    newborn_sql = geo_sql + ",u.upazilanameeng,un.unionnameeng " +zilla_part+geo_join_sql+ condition_geo_sql + " group by  1,2,3 order by 1,2";

                //upazila has speific value
                } else if(!upazila.equals("null") && !upazila.equals("0") && union.equals("null")){
                    condition_geo_sql += " and u.upazilaid ="+ upazila;
                    newborn_sql = geo_sql + ",u.upazilanameeng" +zilla_part+geo_join_sql+ condition_geo_sql + " group by  1,2 order by 1,2";
                //Union has specific value
                }else if(!union.equals("null") && !union.equals("0")){
                    condition_geo_sql += " and u.upazilaid ="+ upazila + " and un.unionid ="+ union;
                    newborn_sql = geo_sql + ",u.upazilanameeng,un.unionnameeng " +zilla_part+geo_join_sql+ condition_geo_sql + " group by  1,2,3 order by 1,2";

                }


                resultString = getJson(newborn_sql);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultString;
    }


    public static List<JSONObject> getJson(String sql) {

        List<JSONObject> resList = new ArrayList<JSONObject>();

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            // get column names
            ResultSetMetaData rsMeta = rs.getMetaData();
            int columnCnt = rsMeta.getColumnCount();
            List<String> columnNames = new ArrayList<String>();
            for (int i = 1; i <= columnCnt; i++) {
                columnNames.add(rsMeta.getColumnName(i));
            }

            while (rs.next()) { // convert each object to an human readable JSON object
                JSONObject obj = new JSONObject();
                for (int i = 1; i <= columnCnt; i++) {
                    String key = columnNames.get(i - 1);
                    String value = rs.getString(i);
                    if(value == null)
                        value = "0";
                    obj.put(key, value);
                }
                resList.add(obj);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(resList);
        return resList;


    }
}
