package org.sci.rhis.report;

import org.json.JSONObject;

public class ANC7Days {
	public static String zilla;
	public static String upazila;
	public static String resultString;
	public static String sql;
	public static String date_type;
	public static String monthSelect;
	public static String start_date;
	public static String end_date;
	public static String db_name;
	
	public static String getResultSet(JSONObject reportCred){
		resultString = "";
		try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			/*date_type = reportCred.getString("report1_dateType");
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}*/
			
			sql = "select * from view_ancdetails7days_by_madhabpur_fwvs";


			
			//System.out.println(sql);
			
			resultString = GetResultSet.getFinalResult(sql, zilla, upazila);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return resultString;
	}
}
