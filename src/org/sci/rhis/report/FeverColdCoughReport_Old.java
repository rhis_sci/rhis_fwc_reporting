package org.sci.rhis.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;

public class FeverColdCoughReport_Old {
    public static String getResultSet(JSONObject reportCred){

        String resultString = "";

        JSONObject resultJSON = null;
        try {
            resultJSON = new JSONObject(ServiceStatistics.getResultSet(reportCred).split(" table/graph/map ")[1].replaceAll("[\\[\\]]", "").replace("}},{", "},"));
            System.out.println(resultJSON);
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "";
            if(reportCred.getString("reportViewType").equals("1"))
                table_header += "<thead><tr><th rowspan=2>Zilla Name</th>";
            else if(reportCred.getString("reportViewType").equals("2"))
                table_header += "<thead><tr><th rowspan=2>Upazila Name</th>";
            else if(reportCred.getString("reportViewType").equals("3"))
                table_header += "<thead><tr><th rowspan=2>Union Name</th>";
            else
                table_header += "<thead><tr><th rowspan=2>Facility Name</th><th rowspan=2>Union Name</th>";

            table_header += "<th colspan=6 style='text-align: center;background: red;'>Fever</th>" +
                    "<th colspan=7 style='text-align: center;background: green;'>Cold</th>" +
                    "<th colspan=7 style='text-align: center;background: blue;'>Cough</th>" +
                    "<th colspan=7 style='text-align: center;background: purple;'>Shortness of Breath</th>" +
                    "<th colspan=7 style='text-align: center;background: orange;'>Asthma</th>" +
                    "<th colspan=7 style='text-align: center;background: yellow;'>Headache</th>" +
                    "<th colspan=7 style='text-align: center;background: skyblue;'>Sore Throat</th>" +
                    "</tr>";

            table_header += "<tr><th style='background: red;'>0-10 Year</th>" +
                    "<th style='background: red;'>11-30 Year</th>" +
                    "<th style='background: red;'>31-50 Year</th>" +
                    "<th style='background: red;'>51-60 Year</th>" +
                    "<th style='background: red;'>61-70 Year</th>" +
                    "<th style='background: red;'>70+ Year</th>" +
                    "<th style='background: red;'>Total</th>" +
                    "<th style='background: green;'>0-10 Year</th>" +
                    "<th style='background: green;'>11-30 Year</th>" +
                    "<th style='background: green;'>31-50 Year</th>" +
                    "<th style='background: green;'>51-60 Year</th>" +
                    "<th style='background: green;'>61-70 Year</th>" +
                    "<th style='background: green;'>70+ Year</th>" +
                    "<th style='background: green;'>Total</th>" +
                    "<th style='background: blue;'>0-10 Year</th>" +
                    "<th style='background: blue;'>11-30 Year</th>" +
                    "<th style='background: blue;'>31-50 Year</th>" +
                    "<th style='background: blue;'>51-60 Year</th>" +
                    "<th style='background: blue;'>61-70 Year</th>" +
                    "<th style='background: blue;'>70+ Year</th>" +
                    "<th style='background: blue;'>Total</th>" +
                    "<th style='background: purple;'>0-10 Year</th>" +
                    "<th style='background: purple;'>11-30 Year</th>" +
                    "<th style='background: purple;'>31-50 Year</th>" +
                    "<th style='background: purple;'>51-60 Year</th>" +
                    "<th style='background: purple;'>61-70 Year</th>" +
                    "<th style='background: purple;'>70+ Year</th>" +
                    "<th style='background: purple;'>Total</th>" +
                    "<th style='background: orange;'>0-10 Year</th>" +
                    "<th style='background: orange;'>11-30 Year</th>" +
                    "<th style='background: orange;'>31-50 Year</th>" +
                    "<th style='background: orange;'>51-60 Year</th>" +
                    "<th style='background: orange;'>61-70 Year</th>" +
                    "<th style='background: orange;'>70+ Year</th>" +
                    "<th style='background: orange;'>Total</th>" +
                    "<th style='background: yellow;'>0-10 Year</th>" +
                    "<th style='background: yellow;'>11-30 Year</th>" +
                    "<th style='background: yellow;'>31-50 Year</th>" +
                    "<th style='background: yellow;'>51-60 Year</th>" +
                    "<th style='background: yellow;'>61-70 Year</th>" +
                    "<th style='background: yellow;'>70+ Year</th>" +
                    "<th style='background: yellow;'>Total</th>" +
                    "<th style='background: skyblue;'>0-10 Year</th>" +
                    "<th style='background: skyblue;'>11-30 Year</th>" +
                    "<th style='background: skyblue;'>31-50 Year</th>" +
                    "<th style='background: skyblue;'>51-60 Year</th>" +
                    "<th style='background: skyblue;'>61-70 Year</th>" +
                    "<th style='background: skyblue;'>70+ Year</th>" +
                    "<th style='background: skyblue;'>Total</th>";
            table_header += "</tr></thead>";

            JSONObject getGEOName = new JSONObject(resultJSON.getString("Fever_0-10"));
            Iterator<String> iterator = getGEOName.keys();

            ObjectMapper mapper = new ObjectMapper();
            Map<String, Map<String, String>> resultMap = mapper.readValue(resultJSON.toString(), Map.class);

            Integer c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28,c29,c30,c31,c32,c33,c34,c35,c36,c37,c38,c39,c40,c41,c42;
            c1=c2=c3=c4=c5=c6=c7=c8=c9=c10=c11=c12=c13=c14=c15=c16=c17=c18=c19=c20=c21=c22=c23=c24=c25=c26=c27=c28=c29=c30=c31=c32=c33=c34=c35=c36=c37=c38=c39=c40=c41=c42=0;

            String table_body = "<tbody>";
            while (iterator.hasNext()) {
                String key = iterator.next();

                c1+=Integer.parseInt(resultMap.get("Fever_0-10").get(key));
                c2+=Integer.parseInt(resultMap.get("Fever_11-30").get(key));
                c3+=Integer.parseInt(resultMap.get("Fever_31-50").get(key));
                c4+=Integer.parseInt(resultMap.get("Fever_51-60").get(key));
                c5+=Integer.parseInt(resultMap.get("Fever_61-70").get(key));
                c6+=Integer.parseInt(resultMap.get("Fever_70+").get(key));
                c7+=Integer.parseInt(resultMap.get("Cold_0-10").get(key));
                c8+=Integer.parseInt(resultMap.get("Cold_11-30").get(key));
                c9+=Integer.parseInt(resultMap.get("Cold_31-50").get(key));
                c10+=Integer.parseInt(resultMap.get("Cold_51-60").get(key));
                c11+=Integer.parseInt(resultMap.get("Cold_61-70").get(key));
                c12+=Integer.parseInt(resultMap.get("Cold_70+").get(key));
                c13+=Integer.parseInt(resultMap.get("Cough_0-10").get(key));
                c14+=Integer.parseInt(resultMap.get("Cough_11-30").get(key));
                c15+=Integer.parseInt(resultMap.get("Cough_31-50").get(key));
                c16+=Integer.parseInt(resultMap.get("Cough_51-60").get(key));
                c17+=Integer.parseInt(resultMap.get("Cough_61-70").get(key));
                c18+=Integer.parseInt(resultMap.get("Cough_70+").get(key));
                c19+=Integer.parseInt(resultMap.get("Shortness_Breath_0-10").get(key));
                c20+=Integer.parseInt(resultMap.get("Shortness_Breath_11-30").get(key));
                c21+=Integer.parseInt(resultMap.get("Shortness_Breath_31-50").get(key));
                c22+=Integer.parseInt(resultMap.get("Shortness_Breath_51-60").get(key));
                c23+=Integer.parseInt(resultMap.get("Shortness_Breath_61-70").get(key));
                c24+=Integer.parseInt(resultMap.get("Shortness_Breath_70+").get(key));
                c25+=Integer.parseInt(resultMap.get("Asthma_0-10").get(key));
                c26+=Integer.parseInt(resultMap.get("Asthma_11-30").get(key));
                c27+=Integer.parseInt(resultMap.get("Asthma_31-50").get(key));
                c28+=Integer.parseInt(resultMap.get("Asthma_51-60").get(key));
                c29+=Integer.parseInt(resultMap.get("Asthma_61-70").get(key));
                c30+=Integer.parseInt(resultMap.get("Asthma_70+").get(key));
                c31+=Integer.parseInt(resultMap.get("Headache_0-10").get(key));
                c32+=Integer.parseInt(resultMap.get("Headache_11-30").get(key));
                c33+=Integer.parseInt(resultMap.get("Headache_31-50").get(key));
                c34+=Integer.parseInt(resultMap.get("Headache_51-60").get(key));
                c35+=Integer.parseInt(resultMap.get("Headache_61-70").get(key));
                c36+=Integer.parseInt(resultMap.get("Headache_70+").get(key));
                c37+=Integer.parseInt(resultMap.get("Sore_Throat_0-10").get(key));
                c38+=Integer.parseInt(resultMap.get("Sore_Throat_11-30").get(key));
                c39+=Integer.parseInt(resultMap.get("Sore_Throat_31-50").get(key));
                c40+=Integer.parseInt(resultMap.get("Sore_Throat_51-60").get(key));
                c41+=Integer.parseInt(resultMap.get("Sore_Throat_61-70").get(key));
                c42+=Integer.parseInt(resultMap.get("Sore_Throat_70+").get(key));

                table_body += "<tr>";

                if(reportCred.getString("reportViewType").equals("1") || reportCred.getString("reportViewType").equals("2") || reportCred.getString("reportViewType").equals("3"))
                    table_body += "<td>"+key+"</td>";
                else
                    table_body += "<td>"+key.split("_")[0]+"</td><td align='center'>"+key.split("_")[1]+"</td>";

                table_body += "<td align='center' style='background: red;'>"+resultMap.get("Fever_0-10").get(key)+"</td>" +
                        "<td align='center' style='background: red;'>"+resultMap.get("Fever_11-30").get(key)+"</td>" +
                        "<td align='center' style='background: red;'>"+resultMap.get("Fever_31-50").get(key)+"</td>" +
                        "<td align='center' style='background: red;'>"+resultMap.get("Fever_51-60").get(key)+"</td>" +
                        "<td align='center' style='background: red;'>"+resultMap.get("Fever_61-70").get(key)+"</td>" +
                        "<td align='center' style='background: red;'>"+resultMap.get("Fever_70+").get(key)+"</td>" +
                        "<td align='center' style='background: red;'>"+(Integer.parseInt(resultMap.get("Fever_0-10").get(key))+Integer.parseInt(resultMap.get("Fever_11-30").get(key))+Integer.parseInt(resultMap.get("Fever_31-50").get(key))+Integer.parseInt(resultMap.get("Fever_51-60").get(key))+Integer.parseInt(resultMap.get("Fever_61-70").get(key))+Integer.parseInt(resultMap.get("Fever_70+").get(key)))+"</td>" +
                        "<td align='center' style='background: green;'>"+resultMap.get("Cold_0-10").get(key)+"</td>" +
                        "<td align='center' style='background: green;'>"+resultMap.get("Cold_11-30").get(key)+"</td>" +
                        "<td align='center' style='background: green;'>"+resultMap.get("Cold_31-50").get(key)+"</td>" +
                        "<td align='center' style='background: green;'>"+resultMap.get("Cold_51-60").get(key)+"</td>" +
                        "<td align='center' style='background: green;'>"+resultMap.get("Cold_61-70").get(key)+"</td>" +
                        "<td align='center' style='background: green;'>"+resultMap.get("Cold_70+").get(key)+"</td>" +
                        "<td align='center' style='background: red;'>"+(Integer.parseInt(resultMap.get("Cold_0-10").get(key))+Integer.parseInt(resultMap.get("Cold_11-30").get(key))+Integer.parseInt(resultMap.get("Cold_31-50").get(key))+Integer.parseInt(resultMap.get("Cold_51-60").get(key))+Integer.parseInt(resultMap.get("Cold_61-70").get(key))+Integer.parseInt(resultMap.get("Cold_70+").get(key)))+"</td>" +
                        "<td align='center' style='background: blue;'>"+resultMap.get("Cough_0-10").get(key)+"</td>" +
                        "<td align='center' style='background: blue;'>"+resultMap.get("Cough_11-30").get(key)+"</td>" +
                        "<td align='center' style='background: blue;'>"+resultMap.get("Cough_31-50").get(key)+"</td>" +
                        "<td align='center' style='background: blue;'>"+resultMap.get("Cough_51-60").get(key)+"</td>" +
                        "<td align='center' style='background: blue;'>"+resultMap.get("Cough_61-70").get(key)+"</td>" +
                        "<td align='center' style='background: blue;'>"+resultMap.get("Cough_70+").get(key)+"</td>" +
                        "<td align='center' style='background: purple;'>"+resultMap.get("Shortness_Breath_0-10").get(key)+"</td>" +
                        "<td align='center' style='background: purple;'>"+resultMap.get("Shortness_Breath_11-30").get(key)+"</td>" +
                        "<td align='center' style='background: purple;'>"+resultMap.get("Shortness_Breath_31-50").get(key)+"</td>" +
                        "<td align='center' style='background: purple;'>"+resultMap.get("Shortness_Breath_51-60").get(key)+"</td>" +
                        "<td align='center' style='background: purple;'>"+resultMap.get("Shortness_Breath_61-70").get(key)+"</td>" +
                        "<td align='center' style='background: purple;'>"+resultMap.get("Shortness_Breath_70+").get(key)+"</td>" +
                        "<td align='center' style='background: orange;'>"+resultMap.get("Asthma_0-10").get(key)+"</td>" +
                        "<td align='center' style='background: orange;'>"+resultMap.get("Asthma_11-30").get(key)+"</td>" +
                        "<td align='center' style='background: orange;'>"+resultMap.get("Asthma_31-50").get(key)+"</td>" +
                        "<td align='center' style='background: orange;'>"+resultMap.get("Asthma_51-60").get(key)+"</td>" +
                        "<td align='center' style='background: orange;'>"+resultMap.get("Asthma_61-70").get(key)+"</td>" +
                        "<td align='center' style='background: orange;'>"+resultMap.get("Asthma_70+").get(key)+"</td>" +
                        "<td align='center' style='background: yellow;'>"+resultMap.get("Headache_0-10").get(key)+"</td>" +
                        "<td align='center' style='background: yellow;'>"+resultMap.get("Headache_11-30").get(key)+"</td>" +
                        "<td align='center' style='background: yellow;'>"+resultMap.get("Headache_31-50").get(key)+"</td>" +
                        "<td align='center' style='background: yellow;'>"+resultMap.get("Headache_51-60").get(key)+"</td>" +
                        "<td align='center' style='background: yellow;'>"+resultMap.get("Headache_61-70").get(key)+"</td>" +
                        "<td align='center' style='background: yellow;'>"+resultMap.get("Headache_70+").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_0-10").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_11-30").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_31-50").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_51-60").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_61-70").get(key)+"</td>" +
                        "<td align='center' style='background: skyblue;'>"+resultMap.get("Sore_Throat_70+").get(key)+"</td>";

                table_body += "</tr>";
            }
            table_body += "</tbody>";

            String calspan = "";
            if(reportCred.getString("reportViewType").equals("4"))
                calspan = "colspan=2";

            String table_footer = "<tfoot><tr><th "+calspan+" rowspan=2 style='text-align: right;'>Total:</th>" +
                    "<th style='text-align: center;background: red;'>"+c1+"</th>" +
                    "<th style='text-align: center;background: red;'>"+c2+"</th>" +
                    "<th style='text-align: center;background: red;'>"+c3+"</th>" +
                    "<th style='text-align: center;background: red;'>"+c4+"</th>" +
                    "<th style='text-align: center;background: red;'>"+c5+"</th>" +
                    "<th style='text-align: center;background: red;'>"+c6+"</th>" +
                    "<th style='text-align: center;background: green;'>"+c7+"</th>" +
                    "<th style='text-align: center;background: green;'>"+c8+"</th>" +
                    "<th style='text-align: center;background: green;'>"+c9+"</th>" +
                    "<th style='text-align: center;background: green;'>"+c10+"</th>" +
                    "<th style='text-align: center;background: green;'>"+c11+"</th>" +
                    "<th style='text-align: center;background: green;'>"+c12+"</th>" +
                    "<th style='text-align: center;background: blue;'>"+c13+"</th>" +
                    "<th style='text-align: center;background: blue;'>"+c14+"</th>" +
                    "<th style='text-align: center;background: blue;'>"+c15+"</th>" +
                    "<th style='text-align: center;background: blue;'>"+c16+"</th>" +
                    "<th style='text-align: center;background: blue;'>"+c17+"</th>" +
                    "<th style='text-align: center;background: blue;'>"+c18+"</th>" +
                    "<th style='text-align: center;background: purple;'>"+c19+"</th>" +
                    "<th style='text-align: center;background: purple;'>"+c20+"</th>" +
                    "<th style='text-align: center;background: purple;'>"+c21+"</th>" +
                    "<th style='text-align: center;background: purple;'>"+c22+"</th>" +
                    "<th style='text-align: center;background: purple;'>"+c23+"</th>" +
                    "<th style='text-align: center;background: purple;'>"+c24+"</th>" +
                    "<th style='text-align: center;background: orange;'>"+c25+"</th>" +
                    "<th style='text-align: center;background: orange;'>"+c26+"</th>" +
                    "<th style='text-align: center;background: orange;'>"+c27+"</th>" +
                    "<th style='text-align: center;background: orange;'>"+c28+"</th>" +
                    "<th style='text-align: center;background: orange;'>"+c29+"</th>" +
                    "<th style='text-align: center;background: orange;'>"+c30+"</th>" +
                    "<th style='text-align: center;background: yellow;'>"+c31+"</th>" +
                    "<th style='text-align: center;background: yellow;'>"+c32+"</th>" +
                    "<th style='text-align: center;background: yellow;'>"+c33+"</th>" +
                    "<th style='text-align: center;background: yellow;'>"+c34+"</th>" +
                    "<th style='text-align: center;background: yellow;'>"+c35+"</th>" +
                    "<th style='text-align: center;background: yellow;'>"+c36+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c37+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c38+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c39+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c40+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c41+"</th>" +
                    "<th style='text-align: center;background: skyblue;'>"+c42+"</th></tr>" +
                    "<tr><th style='text-align: center;background: red;' colspan=6>"+(c1+c2+c3+c4+c5+c6)+"</th>" +
                    "<th style='text-align: center;background: green;' colspan=6>"+(c7+c8+c9+c10+c11+c12)+"</th>" +
                    "<th style='text-align: center;background: blue;' colspan=6>"+(c13+c14+c15+c16+c17+c18)+"</th>" +
                    "<th style='text-align: center;background: purple;' colspan=6>"+(c19+c20+c21+c22+c23+c24)+"</th>" +
                    "<th style='text-align: center;background: orange;' colspan=6>"+(c25+c26+c27+c28+c29+c30)+"</th>" +
                    "<th style='text-align: center;background: yellow;' colspan=6>"+(c31+c32+c33+c34+c35+c36)+"</th>" +
                    "<th style='text-align: center;background: skyblue;' colspan=6>"+(c37+c38+c39+c40+c41+c42)+"</th></tr>" +
                    "</tfoot>";

            resultString = table+table_header+table_body+table_footer+"</table>";
            System.out.println(resultString);
//			System.out.println(resultString.split(" table/graph/map ")[1].replaceAll("[\\[\\]]", ""));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

//		resultString = resultString.split(" table/graph/map ")[0];

        return resultString;
    }
}
