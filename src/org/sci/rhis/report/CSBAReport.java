package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.util.CalendarDate;

public class CSBAReport {
	
	public static String methodType;
	public static String zilla;
	public static String upazila;
	public static String resultString;
	public static String graphString;
	public static String sql;
	public static String date_type;
	public static String monthSelect;
	public static String yearSelect;
	public static String start_date;
	public static String end_date;
	public static String db_name;
	
	public static String getResultSet(JSONObject reportCred){
		resultString = "";
		try{
			methodType = reportCred.getString("methodType");
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			date_type = reportCred.getString("report1_dateType");
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else if(date_type.equals("3"))
			{
				yearSelect = reportCred.getString("report1_year");
				start_date = yearSelect + "-01-01";
				end_date = yearSelect + "-12-31";
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
			sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\",  " + 
					"(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " + 
					"(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " + 
					"(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " + 
					"(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " + 
					"(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) AS \"Delivery\",  " + 
					"(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " + 
					"(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " + 
					"(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " + 
					"(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " + 
					"(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " + 
					"(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " + 
					"(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " + 
					"(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\",  " + 
					"((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " + 
					"from \"ProviderDB\"   " + 
					"left join  " + 
					"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"ANC\",  " + 
					"count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\",  " + 
					"count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\",  " + 
					"count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\",  " + 
					"count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\"  " + 
					"from \"ancService\"  " + 
					"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"ancService\".\"providerId\" " + 
					"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid="+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ancService\".\"providerId\" " + 
					"where (case when count_provider>1  then  " + 
					"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
					"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
					"AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and anc.zillaid=\"ProviderDB\".zillaid AND anc.upazilaid=\"ProviderDB\".upazilaid AND anc.unionid=\"ProviderDB\".unionid    " + 
					"left join   " + 
					"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC\",  " + 
					"count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\",  " + 
					"count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\",  " + 
					"count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\",  " + 
					"count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\"  " + 
					"from \"pncServiceMother\"  " + 
					"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceMother\".\"providerId\" " + 
					"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid="+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceMother\".\"providerId\" " + 
					"where (case when count_provider>1 then  " + 
					"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
					"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
					"AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pnc.zillaid=\"ProviderDB\".zillaid AND pnc.upazilaid=\"ProviderDB\".upazilaid AND pnc.unionid=\"ProviderDB\".unionid    " + 
					"left join   " + 
					"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC-N\",  " + 
					"count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\",  " + 
					"count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\",  " + 
					"count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\",  " + 
					"count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\"  " + 
					"from \"pncServiceChild\"  " + 
					"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceChild\".\"providerId\" " + 
					"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid="+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceChild\".\"providerId\" " + 
					"where (case when count_provider>1 then  " + 
					"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
					"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
					"AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pncc.zillaid=\"ProviderDB\".zillaid AND pncc.upazilaid=\"ProviderDB\".upazilaid AND pncc.unionid=\"ProviderDB\".unionid    " + 
					"left join   " + 
					"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"Delivery\"  " + 
					"from \"delivery\"  " + 
					"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"delivery\".\"providerId\" " + 
					"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid="+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"delivery\".\"providerId\" " + 
					"where (case when count_provider>1 then  " + 
					"(case when \"ExDate\" is NOT NULL then \"outcomeDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"outcomeDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
					"else \"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
					"AND \"delivery\".\"deliveryDoneByThisProvider\"=1 group by \"providerId\",zillaid,upazilaid,unionid) as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" and delivery.zillaid=\"ProviderDB\".zillaid AND delivery.upazilaid=\"ProviderDB\".upazilaid AND delivery.unionid=\"ProviderDB\".unionid    " + 
					"join  \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\"   " + 
					"join  (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila +") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   " + 
					"where \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila +" AND ((\"ProviderDB\".\"ProvType\" = 2 OR \"ProviderDB\".\"ProvType\" = 3) AND \"ProviderDB\".csba = 1)  " + 
					"AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			
			
			/*sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\", "
					//+ "(CASE WHEN anc.\"ANC\" IS NULL THEN 0 ELSE anc.\"ANC\" END) AS \"ANC\", "
					+ "(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\", "
					+ "(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\", "
					+ "(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\", "
					+ "(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\", "
					//+ "(CASE WHEN pnc.\"PNC\" IS NULL THEN 0 ELSE pnc.\"PNC\" END) AS \"PNC\", "
					+ "(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\", "
					+ "(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\", "
					+ "(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\", "
					+ "(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\", "
					//+ "(CASE WHEN pncc.\"PNC-N\" IS NULL THEN 0 ELSE pncc.\"PNC-N\" END) AS \"PNC-N\", "
					+ "(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\", "
					+ "(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\", "
					+ "(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\", "
					+ "(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\", "
					+ "(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END),"
					+ " ((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\" "
					+ " from \"ProviderDB\" "
					+ " left join"
					+ " (select \"providerId\",count (*) as \"ANC\","
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\""
					+ " from \"ancService\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') AND client in (2, 22) group by \"providerId\") as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ " left join "
					+ " (select \"providerId\",count (*) as \"PNC\","
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\""
					+ " from \"pncServiceMother\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') AND client in (2, 22) group by \"providerId\") as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ " left join "
					+ " (select \"providerId\",count (*) as \"PNC-N\","
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\""
					+ " from \"pncServiceChild\" where (\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"') AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') AND client in (2, 22) group by \"providerId\") as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ " left join "
					+ " (select \"providerId\",count (*) as \"Delivery\" from \"delivery\" where \"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' and \"deliveryDoneByThisProvider\"='1' AND client in (2, 22) group by \"providerId\") as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ " join "
					+ " \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" "
					+ " join "
					+ " (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid "
					+ " where \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila
					+ " AND ((\"ProviderDB\".\"ProvType\" = 2 OR \"ProviderDB\".\"ProvType\" = 3) AND \"ProviderDB\".csba = 1) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";*/
					//+ " AND ((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END))>0 ";
			
			
			//System.out.println(sql);
			
			resultString = GetResultSet.getFinalResult(sql,zilla, upazila);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return resultString;
	}

}
