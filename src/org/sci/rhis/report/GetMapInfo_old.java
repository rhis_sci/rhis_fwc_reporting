
package org.sci.rhis.report;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

public abstract class GetMapInfo_old implements Runnable {
	
	public static LinkedHashMap<String, LinkedHashMap<String, String>> resultSetThread = new LinkedHashMap<String, LinkedHashMap<String, String>>();
	
	public static JSONObject getInfo(JSONObject geoInfo) {
		JSONObject info = new JSONObject();
		try{
			String geoSql = "";
			int zilla = Integer.parseInt(geoInfo.getString("zilla"));
			int upazila = Integer.parseInt(geoInfo.getString("upazila"));
			String type = geoInfo.getString("type");
			
			if(type.equals("0")){
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
				
				JSONObject zilla_info = new JSONObject();
				JSONObject upazila_info = new JSONObject();
				
				double total_population;
				double male;
				double female;
				
				double temp_total_population;
				double temp_male;
				double temp_female;
				
				double db_male;
				double db_female;
				double db_rate;
				
				Calendar c = Calendar.getInstance();
				int year = c.get(Calendar.YEAR);
				int total_year = year-2017;
				
				String population_sql = "select * from \"CensusData\"";
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,population_sql);
				ResultSet rs = dbObject.getResultSet();
				ResultSetMetaData metadata = rs.getMetaData();
				
				while(rs.next()){
					
					db_male = Double.parseDouble(rs.getString(metadata.getColumnName(6)));
					db_female = Double.parseDouble(rs.getString(metadata.getColumnName(7)));
					db_rate = Double.parseDouble(rs.getString(metadata.getColumnName(4)));
					
					male = Math.exp((db_rate/100)*total_year)*db_male;
					female = Math.exp((db_rate/100)*total_year)*db_female;
					total_population = male + female;
					
					//System.out.println(Double.toString(total_population));
					//population_info.put("Total", total_population);
					//info.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2))+"_"+rs.getString(metadata.getColumnName(3)), population_info);
					
					if(!info.has(rs.getString(metadata.getColumnName(1)))){
						JSONObject population_info = new JSONObject();
						population_info.put("Total", total_population);
						population_info.put("Male", male);
						population_info.put("Female", female);
						info.put(rs.getString(metadata.getColumnName(1)), population_info);
					}
					else{
						JSONObject population_info = new JSONObject();
						zilla_info = info.getJSONObject(rs.getString(metadata.getColumnName(1)));
						temp_total_population = Double.parseDouble(zilla_info.getString("Total")) + total_population;
						temp_male = Double.parseDouble(zilla_info.getString("Male")) + male;
						temp_female = Double.parseDouble(zilla_info.getString("Female")) + female;
						
						//System.out.println(zilla_info.getString("Total"));
						
						population_info.put("Total", temp_total_population);
						population_info.put("Male", temp_male);
						population_info.put("Female", temp_female);
						info.put(rs.getString(metadata.getColumnName(1)), population_info);
						
					}
					
					if(!info.has(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)))){
						JSONObject population_info = new JSONObject();
						population_info.put("Total", total_population);
						population_info.put("Male", male);
						population_info.put("Female", female);
						info.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)), population_info);
					}
					else{
						JSONObject population_info = new JSONObject();
						upazila_info = info.getJSONObject(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)));
						temp_total_population = Double.parseDouble(upazila_info.getString("Total")) + total_population;
						temp_male = Double.parseDouble(upazila_info.getString("Male")) + male;
						temp_female = Double.parseDouble(upazila_info.getString("Female")) + female;
						
						population_info.put("Total", temp_total_population);
						population_info.put("Male", temp_male);
						population_info.put("Female", temp_female);
						info.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2)), population_info);
					}
					
					JSONObject population_info = new JSONObject();
					population_info.put("Total", total_population);
					population_info.put("Male", male);
					population_info.put("Female", female);
					info.put(rs.getString(metadata.getColumnName(1))+"_"+rs.getString(metadata.getColumnName(2))+"_"+rs.getString(metadata.getColumnName(3)), population_info);
				
				}
				System.out.println(info);
				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
			}
			else if(type.equals("1")){
				
				LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();
				int tempValue;
				ArrayList<String> zilla_array = new ArrayList<String>();
				LinkedHashMap<String, LinkedHashMap<Integer, String>> upazila_array = new LinkedHashMap<String, LinkedHashMap<Integer, String>>();
				String temp_zilla;
				
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
				
				//geoSql = "select \"UPAZILANAMEENG\", \"UPAZILAID\" from \"Upazila\" where \"ZILLAID\"="+zilla;
				
				geoSql = "select \"UPAZILANAMEENG\", \"UPAZILAID\", \"ZILLAID\" from \"Upazila\" where \"ZILLAID\" in (36,42,51,93) order by \"ZILLAID\" asc";
	
				//System.out.println(geoSql);
				
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,geoSql);
				ResultSet rs = dbObject.getResultSet();
				
				rs.last();
				int rowCount = rs.getRow();
				rs.beforeFirst();
				
				ResultSetMetaData metadata = rs.getMetaData();
				
				Thread[] threads = new Thread[rowCount];
				
				long startTime = System.currentTimeMillis();
		        
				int j = 0;
				int l = 0;;
				while(rs.next()){
//					LinkedHashMap<String, String> resultSet = new LinkedHashMap<String, String>();
//					resultSet = getZillaResult(Integer.toString(zilla), rs.getString(metadata.getColumnName(2)));
					
					//threads[j] = new Thread(run(Integer.toString(zilla), rs.getString(metadata.getColumnName(2)), j));
					threads[j] = new Thread(run(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)), j));
					
					threads[j].start();

					j++;
					
//					for (String key : resultSet.keySet()) {
//						if(j>1)
//							tempValue = Integer.parseInt(finalResult.get(key))+Integer.parseInt(resultSet.get(key));
//						else
//							tempValue = Integer.parseInt(resultSet.get(key));
//						finalResult.put(key, Integer.toString(tempValue));
//					}
					if(!zilla_array.contains(rs.getString(metadata.getColumnName(3)))){
						l = 0;
						zilla_array.add(rs.getString(metadata.getColumnName(3)));
						upazila_array.put(rs.getString(metadata.getColumnName(3)), new LinkedHashMap<Integer, String>());
					}
					
					upazila_array.get(rs.getString(metadata.getColumnName(3))).put(l, rs.getString(metadata.getColumnName(2)));
					l++;
							
				}
				
				for(int m=0;m<zilla_array.size();m++){
					finalResult.put(zilla_array.get(m), new LinkedHashMap<String, String>());
					for (Integer k : upazila_array.get(zilla_array.get(m)).keySet()) {
					//for(int k=0;k<rowCount;k++){
						LinkedHashMap<String, String> resultSet = new LinkedHashMap<String, String>();
						resultSet = resultSetThread.get(zilla_array.get(m)+"_"+upazila_array.get(zilla_array.get(m)).get(k));
						System.out.println(resultSet);
						for (String key : resultSet.keySet()) {
							if(k>1)
								tempValue = Integer.parseInt(finalResult.get(zilla_array.get(m)).get(key))+Integer.parseInt(resultSet.get(key));
							else
								tempValue = Integer.parseInt(resultSet.get(key));
							finalResult.get(zilla_array.get(m)).put(key, Integer.toString(tempValue));
						}
					}
				}
				
				long stopTime = System.currentTimeMillis();
		        long elapsedTime = stopTime - startTime;
		        System.out.println(elapsedTime);
				
				
				//String zilla_info = "select count(distinct(\"UPAZILAID\")) as upz_count, count(\"UNIONID\") as union_count from \"Unions\" where \"ZILLAID\"="+zilla+" and \"MUNICIPALITYID\" is null";
		        
		        String zilla_info = "select count(distinct(\"UPAZILAID\")) as upz_count, count(\"UNIONID\") as union_count, \"ZILLAID\" as zillaid from \"Unions\" where \"ZILLAID\" in (36,42,51,93) and \"MUNICIPALITYID\" is null group by \"ZILLAID\"";
				
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,zilla_info);
				rs = dbObject.getResultSet();
				metadata = rs.getMetaData();
				
				while(rs.next()){
					finalResult.get(rs.getString("zillaid")).put("Upazila_count", rs.getString("upz_count"));
					finalResult.get(rs.getString("zillaid")).put("Union_count", rs.getString("union_count"));
					
					JSONObject temp_info = new JSONObject(finalResult.get(rs.getString("zillaid")));
					info.put(rs.getString("zillaid"), temp_info);
				}

				
				
				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
			}
			else if(type.equals("2")){
			
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
				
				String upazila_info_sql = "select \"ZILLAID\", \"UPAZILAID\", count(\"UNIONID\") as union_count from \"Unions\" where \"ZILLAID\"="+zilla+" and \"MUNICIPALITYID\" is null group by \"ZILLAID\",\"UPAZILAID\"";
				
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,upazila_info_sql);
				ResultSet rs = dbObject.getResultSet();
				ResultSetMetaData metadata = rs.getMetaData();
				
				LinkedHashMap<String, String> upazilaInfo = new LinkedHashMap<String, String>();
				while(rs.next()){
					upazilaInfo.put(rs.getString("ZILLAID")+"_"+rs.getString("UPAZILAID"), rs.getString("union_count"));
				}
				
				JSONObject temp_info = new JSONObject(upazilaInfo);
				info.put("Upazila_info", temp_info);
				
				/*String facility_info_sql = "select facilityid, lat, lon, facility_name || ' ' || facility_type as \"Facility_Name\", facility_category from facility_registry where zillaid="+zilla;
				
				//dbObject = dbOp.dbCreateStatement(dbObject);
				DBInfoHandler dbObject_main_db = new FacilityDB().facilityDBInfo();
				dbObject_main_db = dbOp.dbExecute(dbObject_main_db,facility_info_sql);
				rs = dbObject_main_db.getResultSet();
				metadata = rs.getMetaData();
				
				JSONObject facilityInfo = new JSONObject();
				JSONArray facility_array = new JSONArray();
				while(rs.next()){
					facilityInfo = new JSONObject();
					facilityInfo.put(metadata.getColumnName(1), rs.getString("facilityid"));
					facilityInfo.put(metadata.getColumnName(2), rs.getString("lat"));
					facilityInfo.put(metadata.getColumnName(3), rs.getString("lon"));
					facilityInfo.put(metadata.getColumnName(4), rs.getString("Facility_Name"));
					facilityInfo.put(metadata.getColumnName(5), rs.getString("facility_category"));
					
					facility_array.put(facilityInfo);
				}
				
				//JSONObject temp_facility_info = new JSONObject(facilityInfo);
				info.put("Facility_info", facility_array);*/
				
				geoSql = "select \"UPAZILANAMEENG\", \"UPAZILAID\" from \"Upazila\" where \"ZILLAID\"="+zilla;
	
				//System.out.println(geoSql);
				
				//dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,geoSql);
				rs = dbObject.getResultSet();
				metadata = rs.getMetaData();
				
				JSONObject geo_name = new JSONObject();
				JSONObject geo_key = new JSONObject();
				while(rs.next()){
					//if(type.equals("2")){
						//if(rs.getString(metadata.getColumnName(2)).equals("71")){
							geo_name.put("Upazila Name", rs.getString(metadata.getColumnName(1)));
							info.put(Integer.toString(zilla)+"_"+rs.getString(metadata.getColumnName(2)), geo_name);
							info.put(Integer.toString(zilla)+"_"+rs.getString(metadata.getColumnName(2)), getUpazilaResult(Integer.toString(zilla), rs.getString(metadata.getColumnName(2))));
							geo_key = info.getJSONObject(Integer.toString(zilla)+"_"+rs.getString(metadata.getColumnName(2)));
							geo_key.put("Upazila Name", rs.getString(metadata.getColumnName(1)));
							//upz_key.put(getUpazilaResult(Integer.toString(zilla), rs.getString(metadata.getColumnName(2))));
							//info.get(Integer.toString(zilla)+"_"+rs.getString(metadata.getColumnName(2)))).put("Upazila_name", rs.getString(metadata.getColumnName(1)));
						//}
					//}
					//else if(type.equals("3")){
						//if(rs.getString(metadata.getColumnName(2)).equals("71"))
							//info = getUnionResult(Integer.toString(zilla), rs.getString(metadata.getColumnName(2)));//.put(Integer.toString(zilla)+"_"+rs.getString(metadata.getColumnName(2))+"_"+rs.getString(metadata.getColumnName(3)), getUnionResult(Integer.toString(zilla), rs.getString(metadata.getColumnName(2))));
					//}
					
					
					
				}
				
				dbOp.closeStatement(dbObject);
				dbOp.closeConn(dbObject);
			}
			else if(type.equals("3")){
				
				/*DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();
				
				String facility_info_sql = "select facilityid, lat, lon, facility_name || ' ' || facility_type as \"Facility_Name\", facility_category from facility_registry where zillaid="+zilla+" and upazilaid="+upazila;
				
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,facility_info_sql);
				ResultSet rs = dbObject.getResultSet();
				ResultSetMetaData metadata = rs.getMetaData();
				
				JSONObject facilityInfo = new JSONObject();
				JSONArray facility_array = new JSONArray();
				while(rs.next()){
					facilityInfo = new JSONObject();
					facilityInfo.put(metadata.getColumnName(1), rs.getString("facilityid"));
					facilityInfo.put(metadata.getColumnName(2), rs.getString("lat"));
					facilityInfo.put(metadata.getColumnName(3), rs.getString("lon"));
					facilityInfo.put(metadata.getColumnName(4), rs.getString("Facility_Name"));
					facilityInfo.put(metadata.getColumnName(5), rs.getString("facility_category"));
					
					facility_array.put(facilityInfo);
				}
				
				//JSONObject temp_facility_info = new JSONObject(facilityInfo);
				
				System.out.println(facility_array);*/
				
				info = getUnionResult(Integer.toString(zilla), Integer.toString(upazila));//.put(Integer.toString(zilla)+"_"+rs.getString(metadata.getColumnName(2))+"_"+rs.getString(metadata.getColumnName(3)), getUnionResult(Integer.toString(zilla), rs.getString(metadata.getColumnName(2))));
				
				//info.put("Facility_info", facility_array);
			}
			else if(type.equals("4")){
				
				DBOperation dbOp = new DBOperation();
				DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();
				
				String facility_info_sql = "";
				
				if(upazila>0)
					facility_info_sql = "select facilityid, lat, lon, facility_name || ' ' || facility_type as \"Facility_Name\", facility_category from facility_registry where zillaid="+zilla+" and upazilaid="+upazila;
				else
					facility_info_sql = "select facilityid, lat, lon, facility_name || ' ' || facility_type as \"Facility_Name\", facility_category from facility_registry where zillaid="+zilla;
				
				
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject,facility_info_sql);
				ResultSet rs = dbObject.getResultSet();
				ResultSetMetaData metadata = rs.getMetaData();
				
				JSONObject facilityInfo = new JSONObject();
				JSONArray facility_array = new JSONArray();
				while(rs.next()){
					facilityInfo = new JSONObject();
					facilityInfo.put(metadata.getColumnName(1), rs.getString("facilityid"));
					facilityInfo.put(metadata.getColumnName(2), rs.getString("lat"));
					facilityInfo.put(metadata.getColumnName(3), rs.getString("lon"));
					facilityInfo.put(metadata.getColumnName(4), rs.getString("Facility_Name"));
					facilityInfo.put(metadata.getColumnName(5), rs.getString("facility_category"));
					
					facility_array.put(facilityInfo);
				}
				
				info.put("Facility_info", facility_array);
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return info;
	}
	
	public static Runnable run(String zilla, String upazila, int sl){
		//resultSetThread.put(sl, getZillaResult(zilla, upazila));
		resultSetThread.put(zilla+"_"+upazila, getZillaResult(zilla, upazila));
		//System.out.println(sl);
		return null;
	}
	
	public static LinkedHashMap<String, String> getZillaResult(String zilla, String upazila){
		
		String sql;
		String start_date;
		String end_date;
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
	    
		JSONObject upzResult = new JSONObject();
		
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH)+1;
		
		int daysInMonth = CalendarDate.daysInMonth(year, month);
		start_date = year + "-" + month + "-" + "01";
		end_date = year + "-" + month + "-" + daysInMonth;
		
		LinkedHashMap<String, String> resultSet = new LinkedHashMap<String, String>();
		
		try{
			
			sql = "select "
					+ "anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\","
					+ "(delivery.\"Normal Delivery\" + delivery.\"C-Section Delivery\") as \"Total Delivery\", "
					+ "pnc.\"PNC1\", pnc.\"PNC2\", pnc.\"PNC3\", pnc.\"PNC4\", "
					+ "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\" "
					+ "from \"ProviderDB\" "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, count(\"ancService\".*) as \"Total ANC\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric < 5 then 1 else NULL end) as \"ANC1\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 5 and 6 then 1 else NULL end) as \"ANC2\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 7 and 8 then 1 else NULL end) as \"ANC3\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric = 9 then 1 else NULL end) as \"ANC4\", "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\", "
					+ "count(case when \"ancService\".\"refer\"=1 then 1 else NULL end) AS \"ANCRef\" "
					+ "from \"ProviderDB\" "
					+ "left join \"ancService\" on \"ancService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join \"pregWomen\" on \"pregWomen\".\"healthId\"=\"ancService\".\"healthId\" and \"pregWomen\".\"pregNo\"=\"ancService\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+") "
					+ "and ((\"ancService\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') ) "
					+ "group by \"ProviderDB\".zillaid) as anc on \"ProviderDB\".zillaid=anc.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, "
					+ "count(case when (delivery.\"outcomeType\"=1) THEN 1 ELSE NULL END) AS \"Normal Delivery\", "
					+ "count(case when (delivery.\"outcomeType\"=2) THEN 1 ELSE NULL END) AS \"C-Section Delivery\", "
					+ "count(case when (delivery.\"applyOxytocin\"=1 and delivery.\"applyTraction\"=1 and delivery.\"uterusMassage\"=1) then 1 else NULL end) AS \"AMTSL\", "
					+ "(case when (sum(delivery.\"liveBirth\") is not NULL) then sum(delivery.\"liveBirth\") else 0 end) AS \"Live brith\", "
					+ "(case when (sum(delivery.\"stillBirth\") is not NULL) then sum(delivery.\"stillBirth\") else 0 end) AS \"Still brith\", "
					+ "count(case when delivery.\"refer\"=1 then 1 else NULL end) AS \"DeliveryRef\" "
					+ "from \"ProviderDB\" "
					+ "left join delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "and ((delivery.\"outcomeDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (delivery.\"deliveryDoneByThisProvider\" = '1') ) "
					+ "group by \"ProviderDB\".zillaid) as delivery on \"ProviderDB\".zillaid=delivery.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, count(\"pncServiceMother\".*) as \"Total PNC\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric > 48 then 1 else NULL end) as \"PNC4\", "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\", "
					+ "count(case when (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1') then 1 else NULL end) as \"Postpartum FP\", "
					+ "count(case when \"pncServiceMother\".\"refer\"=1 then 1 else NULL end) AS \"PNCRef\" "
					+ "from \"ProviderDB\" "
					+ "left join \"pncServiceMother\" on \"pncServiceMother\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join delivery on delivery.\"healthId\"=\"pncServiceMother\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceMother\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+") "
					+ "and ((\"pncServiceMother\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') ) "
					+ "group by \"ProviderDB\".zillaid) as pnc on \"ProviderDB\".zillaid=pnc.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, count(\"pncServiceChild\".*) as \"Total PNC-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric > 48 then 1 else NULL end) as \"PNC4-N\" "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\" "
					+ "from \"ProviderDB\" "
					+ "left join \"pncServiceChild\" on \"pncServiceChild\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join delivery on delivery.\"healthId\"=\"pncServiceChild\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+") "
					+ "and ((\"pncServiceChild\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') ) "
					+ "group by \"ProviderDB\".zillaid) as pncn on \"ProviderDB\".zillaid=pncn.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, "
					+ "(count(case when \"newBorn\".\"refer\" = 1 then 1 else NULL end) + count(case when (\"pncServiceChild\".\"refer\" = 1 AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= 28) then 1 else NULL end)) as \"New Born Refer\" "
					+ "from \"ProviderDB\" "
					+ "join \"newBorn\" on \"newBorn\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "join \"pncServiceChild\" on \"newBorn\".\"healthId\"=\"pncServiceChild\".\"healthId\" and \"newBorn\".\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + " ' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
					+ "and (\"newBorn\".\"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') and (\"pncServiceChild\".\"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') "
					+ "group by \"ProviderDB\".zillaid) as refer on \"ProviderDB\".zillaid=refer.zillaid "
					+ "where \"ProviderDB\".zillaid="+zilla+" "
					+ "group by anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
					+ "delivery.\"Normal Delivery\", delivery.\"C-Section Delivery\", "
					+ "pnc.\"PNC1\", pnc.\"PNC2\", pnc.\"PNC3\", pnc.\"PNC4\", "
					+ "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\" ";
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
		    
			int j =0;
			while(rs.next()){
				String getValue;	
				for (int i = 1; i <= columnCount; i++) {
					if(rs.getString(metadata.getColumnName(i)) != null && !rs.getString(metadata.getColumnName(i)).isEmpty())
						getValue = rs.getString(metadata.getColumnName(i));
					else
						getValue = "0";
					/*
					if(rs.getString(metadata.getColumnName(i)).equals(""))
						getValue = "0";
					else
						getValue = rs.getString(metadata.getColumnName(i));
						*/
					
					resultSet.put(metadata.getColumnName(i), getValue);
			    }	
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return resultSet;
		
	}
	
	public static JSONObject getUpazilaResult(String zilla, String upazila){
		
		String sql;
		String sql_population;
		String monthSelect;
		String start_date;
		String end_date;
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
	    
		JSONObject upzResult = new JSONObject();
		
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH)+1;
		
		int daysInMonth = CalendarDate.daysInMonth(year, month);
		start_date = year + "-" + month + "-" + "01";
		end_date = year + "-" + month + "-" + daysInMonth;
		
		try{
			
			sql = "select "
					+ "anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\","
					+ "(delivery.\"Normal Delivery\" + delivery.\"C-Section Delivery\") as \"Total Delivery\", "
					+ "pnc.\"PNC1\", pnc.\"PNC2\", pnc.\"PNC3\", pnc.\"PNC4\", "
					+ "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\" "
					+ "from \"ProviderDB\" "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, count(\"ancService\".*) as \"Total ANC\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric < 5 then 1 else NULL end) as \"ANC1\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 5 and 6 then 1 else NULL end) as \"ANC2\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 7 and 8 then 1 else NULL end) as \"ANC3\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric = 9 then 1 else NULL end) as \"ANC4\", "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\", "
					+ "count(case when \"ancService\".\"refer\"=1 then 1 else NULL end) AS \"ANCRef\" "
					+ "from \"ProviderDB\" "
					+ "left join \"ancService\" on \"ancService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join \"pregWomen\" on \"pregWomen\".\"healthId\"=\"ancService\".\"healthId\" and \"pregWomen\".\"pregNo\"=\"ancService\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+") "
					+ "and ((\"ancService\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') ) "
					+ "group by \"ProviderDB\".zillaid) as anc on \"ProviderDB\".zillaid=anc.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, "
					+ "count(case when (delivery.\"outcomeType\"=1) THEN 1 ELSE NULL END) AS \"Normal Delivery\", "
					+ "count(case when (delivery.\"outcomeType\"=2) THEN 1 ELSE NULL END) AS \"C-Section Delivery\", "
					+ "count(case when (delivery.\"applyOxytocin\"=1 and delivery.\"applyTraction\"=1 and delivery.\"uterusMassage\"=1) then 1 else NULL end) AS \"AMTSL\", "
					+ "(case when (sum(delivery.\"liveBirth\") is not NULL) then sum(delivery.\"liveBirth\") else 0 end) AS \"Live brith\", "
					+ "(case when (sum(delivery.\"stillBirth\") is not NULL) then sum(delivery.\"stillBirth\") else 0 end) AS \"Still brith\", "
					+ "count(case when delivery.\"refer\"=1 then 1 else NULL end) AS \"DeliveryRef\" "
					+ "from \"ProviderDB\" "
					+ "left join delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "and ((delivery.\"outcomeDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (delivery.\"deliveryDoneByThisProvider\" = '1') ) "
					+ "group by \"ProviderDB\".zillaid) as delivery on \"ProviderDB\".zillaid=delivery.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, count(\"pncServiceMother\".*) as \"Total PNC\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric > 48 then 1 else NULL end) as \"PNC4\", "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\", "
					+ "count(case when (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1') then 1 else NULL end) as \"Postpartum FP\", "
					+ "count(case when \"pncServiceMother\".\"refer\"=1 then 1 else NULL end) AS \"PNCRef\" "
					+ "from \"ProviderDB\" "
					+ "left join \"pncServiceMother\" on \"pncServiceMother\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join delivery on delivery.\"healthId\"=\"pncServiceMother\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceMother\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+") "
					+ "and ((\"pncServiceMother\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') ) "
					+ "group by \"ProviderDB\".zillaid) as pnc on \"ProviderDB\".zillaid=pnc.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, count(\"pncServiceChild\".*) as \"Total PNC-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric > 48 then 1 else NULL end) as \"PNC4-N\" "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\" "
					+ "from \"ProviderDB\" "
					+ "left join \"pncServiceChild\" on \"pncServiceChild\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join delivery on delivery.\"healthId\"=\"pncServiceChild\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+") "
					+ "and ((\"pncServiceChild\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') ) "
					+ "group by \"ProviderDB\".zillaid) as pncn on \"ProviderDB\".zillaid=pncn.zillaid "
					+ "left join "
					+ "(select \"ProviderDB\".zillaid, "
					+ "(count(case when \"newBorn\".\"refer\" = 1 then 1 else NULL end) + count(case when (\"pncServiceChild\".\"refer\" = 1 AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= 28) then 1 else NULL end)) as \"New Born Refer\" "
					+ "from \"ProviderDB\" "
					+ "join \"newBorn\" on \"newBorn\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "join \"pncServiceChild\" on \"newBorn\".\"healthId\"=\"pncServiceChild\".\"healthId\" and \"newBorn\".\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + " ' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + ") "
					+ "and (\"newBorn\".\"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') and (\"pncServiceChild\".\"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') "
					+ "group by \"ProviderDB\".zillaid) as refer on \"ProviderDB\".zillaid=refer.zillaid "
					+ "where \"ProviderDB\".zillaid="+zilla+" "
					+ "group by anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
					+ "delivery.\"Normal Delivery\", delivery.\"C-Section Delivery\", "
					+ "pnc.\"PNC1\", pnc.\"PNC2\", pnc.\"PNC3\", pnc.\"PNC4\", "
					+ "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\" ";
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
			while(rs.next()){
					
				for (int i = 1; i <= columnCount; i++) {
					//System.out.println(((rs.getObject(metadata.getColumnName(i))==null)?"":rs.getString(metadata.getColumnName(i))));
					upzResult.put(metadata.getColumnName(i), ((rs.getObject(metadata.getColumnName(i))==null)?"":rs.getString(metadata.getColumnName(i))));
			    	      
			    }	
			}
			//System.out.println(upzResult);
			if(!rs.isClosed()){
				rs.close();
			}
			
			sql_population = "";
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return upzResult;
		
	}
	
	public static JSONObject getUnionResult(String zilla, String upazila){
		String sql;
		String start_date;
		String end_date;
	
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
	    
		JSONObject unionResult = new JSONObject();
		
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH)+1;
		
		int daysInMonth = CalendarDate.daysInMonth(year, month);
		start_date = year + "-" + month + "-" + "01";
		end_date = year + "-" + month + "-" + daysInMonth;
		
		try{
			
			sql = "select \"Unions\".\"UNIONID\", \"Unions\".\"UNIONNAMEENG\" as \"Union Name\", "
					+ "anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
					+ "(delivery.\"Normal Delivery\" + delivery.\"C-Section Delivery\") as \"Total Delivery\", "
					+ "pnc.\"PNC1\", pnc.\"PNC2\", pnc.\"PNC3\", pnc.\"PNC4\", "
					+ "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\" "
					//+ "nb.\"Chlorehexidin\" "
					+ "from \"Unions\" "
					+ "left join "
					+ "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", count(\"ancService\".*) as \"Total ANC\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric < 5 then 1 else NULL end) as \"ANC1\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 5 and 6 then 1 else NULL end) as \"ANC2\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric  between 7 and 8 then 1 else NULL end) as \"ANC3\", "
//					+ "count(case when extract('month' from  age( \"ancService\".\"visitDate\", \"LMP\"))::numeric = 9 then 1 else NULL end) as \"ANC4\", "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\", "
					+ "count(case when \"ancService\".\"refer\"=1 then 1 else NULL end) AS \"ANCRef\" "
					+ "from \"ProviderDB\" "
					+ "left join \"ancService\" on \"ancService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join \"pregWomen\" on \"pregWomen\".\"healthId\"=\"ancService\".\"healthId\" and \"pregWomen\".\"pregNo\"=\"ancService\".\"pregNo\" "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila+") "
					+ "and ((\"ancService\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') ) "
					+ "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as anc on \"Unions\".\"ZILLAID\"=anc.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=anc.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=anc.\"UNIONID\" "
					+ "left join "
					+ "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", "
					+ "count(case when (delivery.\"outcomeType\"=1) THEN 1 ELSE NULL END) AS \"Normal Delivery\", "
					+ "count(case when (delivery.\"outcomeType\"=2) THEN 1 ELSE NULL END) AS \"C-Section Delivery\", "
					+ "count(case when (delivery.\"applyOxytocin\"=1 and delivery.\"applyTraction\"=1 and delivery.\"uterusMassage\"=1) then 1 else NULL end) AS \"AMTSL\", "
					+ "(case when (sum(delivery.\"liveBirth\") is not NULL) then sum(delivery.\"liveBirth\") else 0 end) AS \"Live brith\", "
					+ "(case when (sum(delivery.\"stillBirth\") is not NULL) then sum(delivery.\"stillBirth\") else 0 end) AS \"Still brith\", "
					+ "count(case when delivery.\"refer\"=1 then 1 else NULL end) AS \"DeliveryRef\" "
					+ "from \"ProviderDB\" "
					+ "left join delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "and ((delivery.\"outcomeDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (delivery.\"deliveryDoneByThisProvider\" = '1') ) "
					+ "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as delivery on \"Unions\".\"ZILLAID\"=delivery.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=delivery.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=delivery.\"UNIONID\" "
					+ "left join "
					+ "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", count(\"pncServiceMother\".*) as \"Total PNC\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3\", "
//					+ "count(case when extract('day' from  age( \"pncServiceMother\".\"visitDate\", \"outcomeDate\"))::numeric > 48 then 1 else NULL end) as \"PNC4\", "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\", "
					+ "count(case when (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1') then 1 else NULL end) as \"Postpartum FP\", "
					+ "count(case when \"pncServiceMother\".\"refer\"=1 then 1 else NULL end) AS \"PNCRef\" "
					+ "from \"ProviderDB\" "
					+ "left join \"pncServiceMother\" on \"pncServiceMother\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join delivery on delivery.\"healthId\"=\"pncServiceMother\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceMother\".\"pregNo\" "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila+") "
					+ "and ((\"pncServiceMother\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') ) "
					+ "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as pnc on \"Unions\".\"ZILLAID\"=pnc.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=pnc.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=pnc.\"UNIONID\" "
					+ "left join "
					+ "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", count(\"pncServiceChild\".*) as \"Total PNC-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric < 2 then 1 else NULL end) as \"PNC1-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 2 and 6 then 1 else NULL end) as \"PNC2-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric between 7 and 41 then 1 else NULL end) as \"PNC3-N\", "
//					+ "count(case when extract('day' from  age( \"pncServiceChild\".\"visitDate\", \"outcomeDate\"))::numeric > 48 then 1 else NULL end) as \"PNC4-N\" "
					+ " count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\","
					+ " count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\","
					+ " count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\","
					+ " count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\" "
					+ "from \"ProviderDB\" "
					+ "left join \"pncServiceChild\" on \"pncServiceChild\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join delivery on delivery.\"healthId\"=\"pncServiceChild\".\"healthId\" and delivery.\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila+") "
					+ "and ((\"pncServiceChild\".\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) and (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') ) "
					+ "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as pncn on \"Unions\".\"ZILLAID\"=pncn.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=pncn.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=pncn.\"UNIONID\" "
					+ "left join "
					+ "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", "
					+ "(count(case when \"newBorn\".\"refer\" = 1 then 1 else NULL end) + count(case when (\"pncServiceChild\".\"refer\" = 1 AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= 28) then 1 else NULL end)) as \"New Born Refer\" "
					+ "from \"ProviderDB\" "
					+ "join \"newBorn\" on \"newBorn\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "join \"pncServiceChild\" on \"newBorn\".\"healthId\"=\"pncServiceChild\".\"healthId\" and \"newBorn\".\"pregNo\"=\"pncServiceChild\".\"pregNo\" "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + " ' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = " + zilla + " and \"ProviderDB\".upazilaid = "+upazila+") "
					+ "and (\"newBorn\".\"outcomeDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') and (\"pncServiceChild\".\"visitDate\" BETWEEN '" + start_date + "' AND '" + end_date + "') "
					+ "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as refer on \"Unions\".\"ZILLAID\"=refer.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=refer.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=refer.\"UNIONID\" "
					/*+ "left join "
					+ "(select \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\", "
					+ "sum (case when \"newBorn\".\"chlorehexidin\" = 1 then 1 ELSE 0 end) as \"Chlorehexidin\" "
					+ "from \"ProviderDB\" "
					+ "left join \"newBorn\" on \"newBorn\".\"providerId\"=\"ProviderDB\".\"ProvCode\" "
					+ "left join delivery on delivery.\"healthId\"=\"newBorn\".\"healthId\" and delivery.\"pregNo\"=\"newBorn\".\"pregNo\" "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "where (\"ProviderDB\".\"ProvType\" in (4,5,101,17)) and (\"ProviderDB\".\"ExDate\">='" + start_date + "' or \"ProviderDB\".\"ExDate\" IS NULL) and (\"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila+") "
					+ "and ((delivery.\"outcomeDate\" BETWEEN ('" + start_date + "') AND ('" + end_date + "')) ) "
					+ "group by \"Unions\".\"ZILLAID\",\"Unions\".\"UPAZILAID\",\"Unions\".\"UNIONID\") as nb on \"Unions\".\"ZILLAID\"=nb.\"ZILLAID\" and \"Unions\".\"UPAZILAID\"=nb.\"UPAZILAID\" and \"Unions\".\"UNIONID\"=nb.\"UNIONID\" "*/
					+ "join \"ProviderDB\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "where \"Unions\".\"ZILLAID\"="+zilla+" and \"Unions\".\"UPAZILAID\"="+upazila+" "
					+ "and \"ProviderDB\".\"ProvType\" in (4,5,101,17) "
					+ "group by \"Unions\".\"UNIONID\", \"Unions\".\"UNIONNAMEENG\", anc.\"ANC1\", anc.\"ANC2\", anc.\"ANC3\", anc.\"ANC4\", "
					+ "delivery.\"Normal Delivery\", delivery.\"C-Section Delivery\", "
					+ "pnc.\"PNC1\", pnc.\"PNC2\", pnc.\"PNC3\", pnc.\"PNC4\", "
					+ "pncn.\"PNC1-N\", pncn.\"PNC2-N\", pncn.\"PNC3-N\", pncn.\"PNC4-N\" ";
			
			System.out.println(sql);
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
			while(rs.next()){
				JSONObject unionValue = new JSONObject();	
				for (int i = 2; i <= columnCount; i++) {
					//System.out.println(((rs.getObject(metadata.getColumnName(i))==null)?"":rs.getString(metadata.getColumnName(i))));
					unionValue.put(metadata.getColumnName(i), ((rs.getObject(metadata.getColumnName(i))==null)?"":rs.getString(metadata.getColumnName(i))));      
			    }
				unionResult.put(zilla + "_" + upazila + "_" + rs.getString(metadata.getColumnName(1)), unionValue);	
			}
			//System.out.println(upzResult);
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return unionResult;
		
	}

}
