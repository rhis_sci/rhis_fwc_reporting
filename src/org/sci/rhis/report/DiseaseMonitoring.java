package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.util.CalendarDate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DiseaseMonitoring {

    public static String reportViewLevel;
    public static String zilla;
    public static String upazila;
    public static String union;
    public static String resultString;
    public static String sql;
    public static String date_type;
    public static String monthSelect;
    public static String yearSelect;
    public static String start_date;
    public static String end_date;

    public static String getResultSet(JSONObject reportCred){
        resultString = "";
        try{
            reportViewLevel = reportCred.getString("reportViewLevel");
            zilla = reportCred.getString("report1_zilla");
            upazila = reportCred.getString("report1_upazila");
            union = reportCred.getString("report1_union");
            date_type = reportCred.getString("report1_dateType");

            DBSchemaInfo table_schema = new DBSchemaInfo(zilla);

            if(date_type.equals("1"))
            {
                monthSelect = reportCred.getString("report1_month");
                String[] date = monthSelect.split("-");
                int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                start_date = monthSelect + "-01";
                end_date = monthSelect + "-"+daysInMonth;
            }
            else if(date_type.equals("3"))
            {
                yearSelect = reportCred.getString("report1_year");
                start_date = yearSelect + "-01-01";
                if(Integer.parseInt(yearSelect)==Calendar.getInstance().get(Calendar.YEAR)){
                    Calendar cal = Calendar.getInstance();
                    end_date = yearSelect + "-"+new SimpleDateFormat("MM").format(cal.getTime())+"-"+CalendarDate.daysInMonth(Integer.parseInt(yearSelect), Integer.parseInt(new SimpleDateFormat("MM").format(cal.getTime())));
                }
                else
                    end_date = yearSelect + "-12-31";
            }
            else
            {
                start_date = reportCred.getString("report1_start_date");
                end_date = reportCred.getString("report1_end_date");
            }

            resultString = GetResultSet.getFinalResultDiseaseMonitoring(reportViewLevel,zilla,upazila,union,start_date,end_date,yearSelect);

        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e);
        }

        return resultString;
    }

    public static String getSql(String sql_view_level, String zilla, String upazila, String union, String start_date, String end_date){
        String create_sql = "";
        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);

        System.out.println(sql_view_level);

        try {
            String sql_condition = "";
            String limit = "";

            if(sql_view_level.equals("2") || sql_view_level.equals("3") || sql_view_level.equals("4"))
                limit = "limit 10";

            if(sql_view_level.equals("1"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " ";
            else if(sql_view_level.equals("2"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " ";
            else if(sql_view_level.equals("3"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " ";
            else if(sql_view_level.equals("4"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " and " + table_schema.getColumn("table", "providerdb_unionid",new String[]{union},"=") + " ";

            create_sql = "select year as \"YEAR\", to_char(to_timestamp(month::text, 'MM'),'Mon') as \"MONTH\", " +
                    " " + table_schema.getColumn("gp_disease_list", "disease_list_en_detail") + " as \"Disease Name\", count as \"Disease Count\" from (" +
                    " select year, month, disease, count(disease) as count from (" +
                    " select " +
                    " date_part('year', " + table_schema.getColumn("", "gpservice_visitdate") + ") as year, " +
                    " date_part('month', " + table_schema.getColumn("", "gpservice_visitdate") + ") as month, " +
                    " substring ( regexp_split_to_table( substring(" + table_schema.getColumn("", "gpservice_disease") + " from '\\[(.+)\\]' ), ',') from '\"([0-9]+)\"')::integer as disease from (" +
                    " select " + table_schema.getColumn("", "gpservice_visitdate") + ", " + table_schema.getColumn("", "gpservice_disease") + " from " + table_schema.getTable("table_gpservice") + " " +
                    " join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "gpservice_providerid") + " " +
                    " join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                    " and " + sql_condition + " " +
                    " group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "gpservice_providerid") + " " +
                    " where " + table_schema.getColumn("", "gpservice_disease") + " <> '[]' AND " + table_schema.getColumn("", "gpservice_disease") + " <> '[\"0\"]' " +
                    " and (case when "+table_schema.getColumn("","gpservice_visitdate")+" is not null then (case when count_provider>1 then " +
                    " (case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","gpservice_visitdate")+" between '"+ start_date +" ' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","gpservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +" ' end) " +
                    " else "+ table_schema.getColumn("", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" end)  "+
//                  "else  table_schema.getColumn("","gpservice_visitdate")+" is null " +
                    "end) " +
                    " AND " + sql_condition +
                    " AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") " +
                    " ) sq " +
                    " ) sq2 " +
                    " inner join (" +
                    " select substring ( regexp_split_to_table( substring(" + table_schema.getColumn("", "gpservice_disease") + " from '\\[(.+)\\]' ), ',') from '\"([0-9]+)\"')::integer as disease_id" +
                    " from " + table_schema.getTable("table_gpservice") + "  " +
                    " join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "gpservice_providerid") + " " +
                    " join (select count(*) as count_provider, " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where (" + table_schema.getColumn("", "providerdb_exdate", new String[]{start_date}, ">=") + " or " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ") " +
                    " and " + sql_condition +
                    " group by " + table_schema.getColumn("", "providerdb_provcode") + ") ct_prov on ct_prov." + table_schema.getColumn("", "providerdb_provcode") + "=" + table_schema.getColumn("table", "gpservice_providerid") + " " +
                    " where " + table_schema.getColumn("", "gpservice_disease") + " <> '[]' AND " + table_schema.getColumn("", "gpservice_disease") + " <> '[\"0\"]' " +
                    " and  (case when "+table_schema.getColumn("","gpservice_visitdate")+" is not null then (case when count_provider>1 then " +
                    " (case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","gpservice_visitdate")+" between '"+ start_date +" ' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","gpservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +" ' end) " +
                    " else "+ table_schema.getColumn("", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" end) else "+table_schema.getColumn("","gpservice_visitdate")+" is null end) " +
                    " AND " + sql_condition +
                    " AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") " +
                    " group by disease_id order by count(*) desc " + limit +
                    " )sq3 on disease_id = disease" +
                    " group by year, month, disease" +
                    " ) sq4 inner join " + table_schema.getTable("table_disease_list_en") + " gp_disease_list  on " + table_schema.getColumn("", "disease_list_en_id") + " = disease" +
                    " order by year asc, month asc, count desc";


        }
        catch (Exception e){
            System.out.println(e);
        }
        System.out.println(create_sql);
        return create_sql;
    }
}
