package org.sci.rhis.report;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.json.JSONObject;
import org.sci.rhis.db.*;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class OtherTask {
	
	public static Integer zilla;
	public static String zillaname;
	public static Integer upazila;
	public static Integer union;
	public static String unionname;
	public static Integer providerType;
	public static Integer providerCode;
	public static String providerPass;
	public static String providerName;
	public static String providerMob;
	public static String facilityName;
	public static String csba;
	public static String systemupdatedate;
	public static String base_url;
	public static String sync_url;
	public static String sql_providerdb;
	public static String sql_nodeDetails;
	public static String sql_sym_node;
	public static String DB_name;
	public static String servername;
	public static String nodename;
	public static String changetype;
	public static String version;
	public static String sql_assignAppVersion;
	public static String sql_provider;
	public static String sql_upazila;
	
	private static int READ_TIMEOUT = 15000;
	private static int CONNECTION_TIMEOUT = 15000;
	
	public static void main(String[] args){

		DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(19));
		
//		String[] upazila_list = {"09","15","41","44","63","91","55"};
		//String[] upazila_list = {"09","23","25","28","38","47","57","66","76","85","95"};
		//String[] upazila_list = {"33","43","58","65","73"};
		//String[] upazila_list = {"02","05","11","26","44","68","77"};
		//String[] upazila_list = {"07","10","21","36","47","80","83","85","87"};
		String[] upazila_list = {"09","15","18","27","31","33","36","40","54","67","72","74","75","81","87","94"};
//		String[] upazila_list = {"46"};
		int i=0;
		
		for(int j=0;j<upazila_list.length;j++){
			//System.out.println(upazila_list[j]);
		
		DBOperation dbOp = new DBOperation();
		//DBInfoHandler dbObject =  new FacilityDB().dynamicDBInfo("36", upazila_list[j]);
		DBInfoHandler dbObject =  new FacilityDB().facilityDistDBInfo("19");
		boolean status = false;
		
		DBInfoHandler detailDomain = new DBInfoHandler();
		Properties prop = ConfInfoRetrieve.readingConf(detailDomain.getDomainLoc());
		String[] DomainDetails = prop.getProperty("DOMAININFO_19").split(",");
		
		String host=DomainDetails[0];
//	    String user=DomainDetails[1];
//	    String password=DomainDetails[2];
//	    Integer portNum=Integer.parseInt(DomainDetails[3]);
		
		try{
			
			//sql_provider = "select * from \"ProviderDB\" join \"Zilla\" on \"Zilla\".\"ZILLAID\"=\"ProviderDB\".zillaid join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid where \"ProviderDB\".\"ProvType\" in (4,17,101,5,6) and \"ProviderDB\".zillaid=36"; // and \"ProviderDB\".upazilaid="+((upazila_list[j]=="09")?"9":upazila_list[j]);
			//sql_provider = "select * from \"ProviderDB\" join \"Zilla\" on \"Zilla\".\"ZILLAID\"=\"ProviderDB\".zillaid join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid where \"ProviderDB\".\"ProvCode\" in (19172,68829)";
//			sql_provider = "select * from \"ProviderDB\" join \"Zilla\" on \"Zilla\".\"ZILLAID\"=\"ProviderDB\".zillaid join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid where \"ProviderDB\".\"ProvType\" in (2,3) and \"ProviderDB\".csba=1 and \"ProviderDB\".zillaid=36 and \"ProviderDB\".upazilaid="+Integer.parseInt(upazila_list[j])+" and \"ProviderDB\".\"ProvCode\" not in (select \"ProvCode\" from node_details where \"ProvCode\" in (select \"ProvCode\" from \"ProviderDB\" where \"ProvType\" in (2,3) and csba=1))";
//			sql_provider = "select * from \"ProviderDB\" join \"Zilla\" on \"Zilla\".\"ZILLAID\"=\"ProviderDB\".zillaid join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid where \"ProviderDB\".\"ProvType\" in (2,3) and \"ProviderDB\".csba=1 and \"ProviderDB\".zillaid=75 and \"ProviderDB\".upazilaid="+Integer.parseInt(upazila_list[j]);//+" and \"ProviderDB\".\"ProvType\" in (2,3) and csba=1";
			sql_provider = "SELECT * from providerdb JOIN zilla ON zilla.zillaid=providerdb.zillaid JOIN unions ON providerdb.zillaid=unions.zillaid AND providerdb.upazilaid=unions.upazilaid AND providerdb.unionid=unions.unionid WHERE providerdb.zillaid=19 and providerdb.upazilaid="+upazila_list[j]+" and providerid in(193685,193161,193226,193224,193225,193337,193318,193514,193823)";
//			sql_provider = "SELECT * from providerdb JOIN zilla ON zilla.zillaid=providerdb.zillaid JOIN unions ON providerdb.zillaid=unions.zillaid AND providerdb.upazilaid=unions.upazilaid AND providerdb.unionid=unions.unionid WHERE providerdb.zillaid=27 and providerdb.upazilaid="+upazila_list[j]+" and provtype in (2,3) and csba=1";
//			sql_provider = "SELECT * from providerdb JOIN node_details using(providerid) LEFT JOIN dblink('host=10.12.1.22 dbname=RHIS_94_"+upazila_list[j]+" user=rhis_admin password=rhis12#rhis','select node_id from sym_node_security') as sym_node_security (node_id text) on node_details.id=sym_node_security.node_id  JOIN zilla ON zilla.zillaid=providerdb.zillaid JOIN unions ON providerdb.zillaid=unions.zillaid AND providerdb.upazilaid=unions.upazilaid AND providerdb.unionid=unions.unionid WHERE providerdb.zillaid=94 and providerdb.upazilaid="+upazila_list[j]+" and node_id is null";
//			sql_provider = "SELECT * from providerdb JOIN zilla ON zilla.zillaid=providerdb.zillaid JOIN unions ON providerdb.zillaid=unions.zillaid AND providerdb.upazilaid=unions.upazilaid AND providerdb.unionid=unions.unionid WHERE providerdb.zillaid=51 and providerdb.upazilaid="+upazila_list[j]+" and providerid>564009";
					//+ "providerid in(424110,424106,426105,425106,426101,424111,426104,424102,425109,424109,424104,424103,426103,424112,425113,424125,425112,425115,424119,24126,424135,424136,425114,424122,424121);";
			
			//sql_provider = "select * from \"ProviderDB\" join \"Zilla\" on \"Zilla\".\"ZILLAID\"=\"ProviderDB\".zillaid join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid where \"ProviderDB\".\"ProvType\" in (4,17,101,5,6) and \"ProviderDB\".zillaid=75 and \"ProviderDB\".upazilaid="+Integer.parseInt(upazila_list[j]);
			
					//+ "\"ProvType\"=3 and csba=1";
//			System.out.println(sql_provider);
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql_provider);
			ResultSet rs = dbObject.getResultSet();
			
//			ResultSetMetaData metadata = rs.getMetaData();
//		    int columnCount = metadata.getColumnCount();
			while(rs.next()){
				i++;
				//System.out.println(i);
//				zilla = Integer.parseInt(rs.getString("zillaid"));
//				//zilla = Integer.parseInt(formValue.getString("zillaid"));
//				zillaname = rs.getString("ZILLANAMEENG").toLowerCase();
//				upazila = Integer.parseInt(rs.getString("upazilaid"));
//				union = Integer.parseInt(rs.getString("unionid"));
//				unionname = rs.getString("UNIONNAMEENG");
//				providerType = Integer.parseInt(rs.getString("ProvType"));
//				providerCode = Integer.parseInt(rs.getString("ProvCode"));
//				providerName = rs.getString("ProvName");
//				providerMob = rs.getString("MobileNo");
//				providerPass = rs.getString("ProvPass");
//				facilityName = rs.getString("FacilityName");
//				csba = rs.getString("csba");
//				systemupdatedate = rs.getString("SystemUpdateDT");
				
				zilla = Integer.parseInt(rs.getString("zillaid"));
				//zilla = Integer.parseInt(formValue.getString("zillaid"));
				zillaname = rs.getString("zillanameeng").toLowerCase();
				upazila = Integer.parseInt(rs.getString("upazilaid"));
				union = Integer.parseInt(rs.getString("unionid"));
				unionname = rs.getString("unionnameeng");
				providerType = Integer.parseInt(rs.getString("provtype"));
				providerCode = Integer.parseInt(rs.getString("providerid"));
				providerName = rs.getString("provname");
				providerMob = rs.getString("mobileno");
				providerPass = rs.getString("provpass");
				facilityName = rs.getString("facilityname");
				csba = rs.getString("csba");
				systemupdatedate = rs.getString("systemupdatedt");
				
				if(zilla.equals(36))
					base_url = "http://"+ host +":8080/RHIS_FWC/";
				else if(zilla.equals(93))
					base_url = "http://mchdrhis.icddrb.org:8080/eMIS/";
				else
					base_url = "http://"+ host +":8080/eMIS/";
					//base_url = "http://mamoni.net:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";
				
				/*else if(zilla.equals(93) && upazila.equals(9))
					base_url = "http://mchdrhis.icddrb.org:8080/CcahWebservice_facility/";
				else if(zilla.equals(93))
					base_url = "http://mchdrhis.icddrb.org:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";
				else
					base_url = "http://mamoni.net:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";*/
				
				if(zilla.equals(36) && upazila.equals(71))
					sync_url = "http://"+ host +":31415/sync/";
				else if(zilla.equals(93) && upazila.equals(9))
					sync_url = "http://mchdrhis.icddrb.org:31415/sync/";
				else
				{
					Integer zilla_port;
					if(zilla<10)
					{
						zilla_port = zilla+10;
						if(zilla.equals(93))
							sync_url = "http://mchdrhis.icddrb.org:"+zilla_port+String.format("%02d", upazila)+"4/sync/";
						else
							sync_url = "http://"+ host +":"+zilla_port+String.format("%02d", upazila)+"4/sync/";
					}	
					else{
						if(zilla>64)
						{
							zilla_port = zilla-55;
							if(zilla.equals(93))
								sync_url = "http://mchdrhis.icddrb.org:"+zilla_port+String.format("%02d", upazila)+"2/sync/";
							else
								sync_url = "http://"+ host +":"+zilla_port+String.format("%02d", upazila)+"2/sync/";
						}
						else
						{
							if(zilla.equals(93))
								sync_url = "http://mchdrhis.icddrb.org:"+zilla+String.format("%02d", upazila)+"0/sync/";
							else
								sync_url = "http://"+ host +":"+zilla+String.format("%02d", upazila)+"0/sync/";
						}
					}
				}
				//facilityName = formValue.get("facilityname");

				sql_providerdb = "insert into "+table_schema.getTable("table_providerdb")+" ("+table_schema.getColumn("","providerdb_divid")+", "+table_schema.getColumn("","providerdb_zillaid")+", "+table_schema.getColumn("","providerdb_upazilaid")+", "+table_schema.getColumn("","providerdb_unionid")+", "
						+""+table_schema.getColumn("","providerdb_provtype")+", "+table_schema.getColumn("","providerdb_provcode")+", "+table_schema.getColumn("","providerdb_provname")+", "+table_schema.getColumn("","providerdb_mobileno")+", "+table_schema.getColumn("","providerdb_endate")+", "
						+""+table_schema.getColumn("","providerdb_active")+", "+table_schema.getColumn("","providerdb_devicesetting")+", "+table_schema.getColumn("","providerdb_systemupdatedt")+", "+table_schema.getColumn("","providerdb_healthidrequest")+", "+table_schema.getColumn("","providerdb_tablestructurerequest")+", "
						+""+table_schema.getColumn("","providerdb_areaupdate")+", "+table_schema.getColumn("","providerdb_provpass")+")"
						+ "VALUES("
						+ "(SELECT "+table_schema.getColumn("","zilla_divid")+" FROM "+table_schema.getTable("table_zilla")+" WHERE "+table_schema.getColumn("", "zilla_zillaid",new String[]{Integer.toString(zilla)},"=")+"),"
						+ zilla + "," + upazila + "," + union + "," + providerType + "," + providerCode + ", '" + providerName + "', "
						+ providerMob + ", CURRENT_TIMESTAMP, '1', '2', "
						+ "'2', '2', '2', '2', '"+providerPass+"') "
						+ "RETURNING "+table_schema.getColumn("","providerdb_provcode")+"";

				sql_nodeDetails = "insert into "+table_schema.getTable("table_node_details")+" ("+table_schema.getColumn("","node_details_provcode")+", "+table_schema.getColumn("","node_details_provtype")+", "+table_schema.getColumn("","node_details_client")+", "+table_schema.getColumn("","node_details_id")+", "+table_schema.getColumn("","node_details_server")+", "+table_schema.getColumn("","node_details_base_url")+", "+table_schema.getColumn("","node_details_sync_url")+")"
						+ "VALUES("+providerCode+", "+providerType+", 2, "
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace("+table_schema.getColumn("","unions_unionnameeng")+", ' ', '')) from "+table_schema.getTable("table_unions")+" where "+table_schema.getColumn("", "unions_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "unions_upazilaid",new String[]{Integer.toString(upazila)},"=")+" AND "+table_schema.getColumn("", "unions_unionid",new String[]{Integer.toString(union)},"=")+"), "
						+ "(SELECT format('%s-"+String.format("%02d", zilla)+"-"+String.format("%02d", upazila)+"', lower("+table_schema.getColumn("","zilla_zillanameeng")+")) from "+table_schema.getTable("table_zilla")+" where "+table_schema.getColumn("", "zilla_zillaid",new String[]{Integer.toString(zilla)},"=")+"), "
						+ "'"+base_url+"', '"+sync_url+"')"
						+ "RETURNING "+table_schema.getColumn("","node_details_provcode")+"";
					
					
//					System.out.println(sql_providerdb);
					DBInfoHandler dbObject_district = new FacilityDB().facilityDBInfo("19");
//					dbOp.dbStatementExecute(dbObject_district,sql_providerdb);
					dbOp.dbStatementExecute(dbObject_district,sql_nodeDetails);
//
//					dbOp.closeStatement(dbObject_district);
//					dbOp.closeConn(dbObject_district);
					
//					System.out.println(base_url);
//					System.out.println(sync_url);
					
//					sql_sym_node = "insert into sym_node (node_id,node_group_id,external_id,sync_enabled,sync_url,schema_version,"
//							+ "symmetric_version,database_type,database_version,heartbeat_time,timezone_offset,batch_to_send_count,"
//							+ "batch_in_error_count,created_at_node_id) "
//							+ "values ("
//							+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+"), "
//							+ "'FWV',"
//							+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+"), "
//							+ "1,null,null,null,null,null,CURRENT_TIMESTAMP,null,0,0,"
//							+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+")) ";

				sql_sym_node = "insert into sym_node (node_id,node_group_id,external_id,sync_enabled,sync_url,schema_version,"
						+ "symmetric_version,database_type,database_version,heartbeat_time,timezone_offset,batch_to_send_count,"
						+ "batch_in_error_count,created_at_node_id) "
						+ "values ("
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace("+table_schema.getColumn("","unions_unionnameeng")+", ' ', '')) from "+table_schema.getTable("table_unions")+" where "+table_schema.getColumn("", "unions_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "unions_upazilaid",new String[]{Integer.toString(upazila)},"=")+" AND "+table_schema.getColumn("", "unions_unionid",new String[]{Integer.toString(union)},"=")+"), "
						+ "'FWV',"
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace("+table_schema.getColumn("","unions_unionnameeng")+", ' ', '')) from "+table_schema.getTable("table_unions")+" where "+table_schema.getColumn("", "unions_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "unions_upazilaid",new String[]{Integer.toString(upazila)},"=")+" AND "+table_schema.getColumn("", "unions_unionid",new String[]{Integer.toString(union)},"=")+"), "
						+ "1,null,null,null,null,null,CURRENT_TIMESTAMP,null,0,0,"
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace("+table_schema.getColumn("","unions_unionnameeng")+", ' ', '')) from "+table_schema.getTable("table_unions")+" where "+table_schema.getColumn("", "unions_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "unions_upazilaid",new String[]{Integer.toString(upazila)},"=")+" AND "+table_schema.getColumn("", "unions_unionid",new String[]{Integer.toString(union)},"=")+")) ";
					
					
					//dbOp.dbStatementExecute(dbObject,sql_sym_node);
					
//					System.out.println(sql_sym_node);
					
					DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo("19", upazila_list[j]);
					//System.out.println("Line2");
					dbOp.dbStatementExecute(dbObject_dynamic,sql_providerdb);
					dbOp.dbStatementExecute(dbObject_dynamic,sql_nodeDetails);
//					dbOp.dbStatementExecute(dbObject_dynamic,sql_sym_node);
//
					dbOp.closeStatement(dbObject_dynamic);
					dbOp.closeConn(dbObject_dynamic);
				        
					if(zilla.equals(93) && upazila.equals(9))
						servername = "tangail-93-9";
					else
						servername = zillaname+"-"+String.format("%02d", zilla)+"-"+String.format("%02d", upazila);
			        nodename = unionname.replaceAll("\\s+","")+"_"+providerCode+"_TAB";
			        
			        String command1="bash /var/lib/symmetricds/symmetric-server-3.9.0/bin/symadmin --engine "+servername+" open-registration FWV "+nodename;
			        System.out.println(command1);

				JSONObject jsonStr = new JSONObject();
				jsonStr.put("providerid", providerCode);
				jsonStr.put("zillaid", zilla);
				jsonStr.put("upazilaid", upazila);
				jsonStr.put("symserver", servername);
				jsonStr.put("symnode", nodename);

				String URL = "";

//					if(zilla==19)
//						URL = "http://"+host+":8080/eMIS_19/";
//					else
				URL = "http://"+host+":8080/eMIS_"+zilla+"/";

//				serverComm(URL + "updownstatus", "info=" + jsonStr);
			        
//			        JSONObject jsonStr = new JSONObject();
//					jsonStr.put("providerid", providerCode);
//					jsonStr.put("zillaid", zilla);
//					jsonStr.put("upazilaid", upazila);
//					jsonStr.put("symserver", servername);
//					jsonStr.put("symnode", nodename);
//
//					String URL = "http://"+host+":8080/eMIS/";
//					serverComm(URL + "updownstatus", "info=" + jsonStr);
						
				        /*String command1="bash /var/lib/symmetricds/symmetric-server-3.7.27/bin/symadmin --engine "+servername+" open-registration FWV "+nodename;
				        System.out.println(command1);
					    ArrayList<String> result = new ArrayList<String>();
					    
					    java.util.Properties config = new java.util.Properties(); 
				    	config.put("StrictHostKeyChecking", "no");
				    	JSch jsch = new JSch();
				    	Session session=jsch.getSession(user, host, portNum);
				    	session.setPassword(password);
				    	session.setConfig(config);
				    	session.connect();
				    	System.out.println("Connected");
				    	
				    	Channel channel=session.openChannel("exec");
				        ((ChannelExec)channel).setCommand(command1);
				        channel.setInputStream(null);
				        ((ChannelExec)channel).setErrStream(System.err);
				        
				        InputStream in=channel.getInputStream();
				        channel.connect();
				        
				     // Read the output from the input stream we set above
				        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				        String line;
				        
				        //Read each line from the buffered reader and add it to result list
				        // You can also simple print the result here
				        while ((line = reader.readLine()) != null)
				        {
				        	result.add(line);
				        }
				        System.out.println(Arrays.asList(result)); 
				        //retrieve the exit status of the remote command corresponding to this channel
				        int exitStatus = channel.getExitStatus();
				        
				        channel.disconnect();
				        session.disconnect();
				        System.out.println("DONE");*/
			
			}
			
			//System.out.println(status);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		}
	}

	private static void serverComm(String uri, String jsonString){

		try{
			URL url = new URL(uri);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(READ_TIMEOUT);
			conn.setConnectTimeout(CONNECTION_TIMEOUT);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			OutputStream os = conn.getOutputStream();
			BufferedWriter writer =
					new BufferedWriter(
							new OutputStreamWriter(os, "UTF-8"));
			writer.write(jsonString);

			writer.flush();
			writer.close();
			os.close();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println("No response!");
			}
			else {
				InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				String testString = stringBuilder.toString();
				System.out.println(testString);
			}
		}
		catch(MalformedURLException mul) {
			mul.getStackTrace();
		}
		catch (ProtocolException pe) {
			pe.getStackTrace();
		}
		catch (IOException io) {
			io.getStackTrace();

		}
		catch (Exception e) {
			e.getStackTrace();
		}
	}

}
