package org.sci.rhis.report;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.EnglishtoBangla;

public class FacilityInfoEntry {
	
	public static Integer zilla;
	public static String zillaname;
	public static Integer upazila;
	public static Integer union;
	public static String unionname;
	public static String unionfacilityname;
	public static String[] facilitytype;
	
	public static JSONObject getFacilityType(){
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.facilityDBInfo();

		DBSchemaInfo table_schema = new DBSchemaInfo("36");
		
		String sql = "select * from "+table_schema.getTable("table_facility_type")+"";
		
		JSONObject facility_info = new JSONObject();
		
		try{
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			
			ResultSetMetaData metadata = rs.getMetaData();
		    
			while(rs.next()){
				facility_info.put(rs.getString(metadata.getColumnName(1)), rs.getString(metadata.getColumnName(2)));
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//System.out.println(facility_info);
		return facility_info;
		
	}
	
	public static boolean saveFacilityInfo(JSONObject formValue) throws SQLException{

		zilla = Integer.parseInt(formValue.getString("zillaid"));
		//System.out.println(zilla);
		unionfacilityname = formValue.getString("UnionFacilityName");
		System.out.println("Union Facility Name="+unionfacilityname);

		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);
		dbObject = dbOp.dbCreateStatement(dbObject);

		DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
		
		boolean status = false;
		
		String facility_no = new String("");
		
		zillaname = formValue.getString("zillaname");
//		if(!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))
			upazila = Integer.parseInt(formValue.getString("upazilaid"));
//		else
//			upazila = 0;
//		if(!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null")){
			union = Integer.parseInt(formValue.getString("unionid"));
			unionname = formValue.getString("unionname");
//		}
//		else
//			union = 0;
		facilitytype = formValue.getString("facilitytype").split("_");
		
//		System.out.println("Unionid=" + union);

		/** Upazila and Union ID formating: make it 2 digits**
		 * @Author: Evana
		 * @Date: 13/05/2019
		 * */

//		int length = String.valueOf(upazila).length();
//		int ulength = String.valueOf(union).length();
//
//		int new_upazila = 0;
//		int new_union = 0;
//
//		if(length<2)
//		{
//			//upazila = upazila
//			 new_upazila = Integer.valueOf(String.valueOf(upazila) + String.valueOf(0));
//		}
//		else
//		{
//			 new_upazila = upazila;
//		}
//
//		if(ulength<2)
//		{
//			//upazila = upazila
//			new_union = Integer.valueOf(String.valueOf(union) + String.valueOf(0));
//		}
//		else
//		{
//			new_union = union;
//		}

		//String get_facility_id = formValue.getString("zillaid") + ((!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))?formValue.getString("upazilaid"):"00") + ((!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))?formValue.getString("unionid"):"00") + "1";

//		String get_facility_id = formValue.getString("zillaid") + ((!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))? String.format("%02d", upazila):"00") + ((!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))?String.format("%02d", union):"00") + "1";

		String get_facility_id = formValue.getString("zillaid") +  String.format("%02d", upazila) + String.format("%02d", union) + "1";

		String check_facilityid = "";
		
//		if(!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))
			check_facilityid = "select * from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("", "facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("", "facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and "+table_schema.getColumn("", "facility_registry_unionid",new String[]{Integer.toString(union)},"=");
//		else if((!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null")) && (formValue.getString("unionid").equals("") || formValue.getString("unionid").equals("null")))
//			check_facilityid = "select * from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("", "facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("", "facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and "+table_schema.getColumn("", "facility_registry_unionid",new String[]{},"isnull")+"";
//		else if((formValue.getString("upazilaid").equals("") || formValue.getString("upazilaid").equals("null")) && (formValue.getString("unionid").equals("") || formValue.getString("unionid").equals("null")))
//			check_facilityid = "select * from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("", "facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("", "facility_registry_upazilaid",new String[]{},"isnull")+" and "+table_schema.getColumn("", "facility_registry_unionid",new String[]{},"isnull")+"";
		
		dbObject = dbOp.dbExecute(dbObject,check_facilityid);
		ResultSet rs = dbObject.getResultSet();
		
		rs.last();
		int rowCount = rs.getRow();
		
		Integer facility_id = Integer.parseInt(get_facility_id) + rowCount;
		
//		String num_rows = Integer.toString(rowCount+1);
//
//		if(rowCount>1)
//			facility_no = "-"+EnglishtoBangla.ConvertNumberToBangla(num_rows);
//		else
//			facility_no = "";

		String sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_upazilaid")+", "
				+ table_schema.getColumn("", "facility_registry_unionid")+", "+table_schema.getColumn("", "facility_registry_facilityid")+", "+table_schema.getColumn("", "facility_registry_facility_type")+", "
				+ table_schema.getColumn("", "facility_registry_facility_name")+", "+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
				+ "VALUES("+ zilla +", "+ upazila +", "+ union +", "+ facility_id +", '"+ facilitytype[1] +"', '"+ unionfacilityname + "', "
				+ facilitytype[0] +") "
				+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
		
//		if(!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null")){
//
//			/*
//			if(Integer.parseInt(facilitytype[0])==5)
//			{
//			System.out.println("Facility Type 1st =UHC");
//			}
//			*/
//			System.out.println("Facility Type 1st query");
//
//			/*
//			sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_upazilaid")+", "
//					+ table_schema.getColumn("", "facility_registry_unionid")+", "+table_schema.getColumn("", "facility_registry_facilityid")+", "+table_schema.getColumn("", "facility_registry_facility_type")+", "
//					+ table_schema.getColumn("", "facility_registry_facility_name")+", "+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
//					+ "VALUES("+ zilla +", "+ upazila +", "+ union +", "+ facility_id +", '"+ facilitytype[1] +"', "
//					+ "(SELECT "+table_schema.getColumn("", "unions_unionname")+" from "+table_schema.getTable("table_unions")+" where "+table_schema.getColumn("", "unions_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "unions_upazilaid",new String[]{Integer.toString(upazila)},"=")+" AND "+table_schema.getColumn("", "unions_unionid",new String[]{Integer.toString(union)},"=")+") || ' ' || "
//					+ "(SELECT "+table_schema.getColumn("", "facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("", "facility_type_type",new String[]{facilitytype[0]},"=")+") || '"+ facility_no +"', "+ facilitytype[0] +") "
//					+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//			*/
//
//			sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_upazilaid")+", "
//					+ table_schema.getColumn("", "facility_registry_unionid")+", "+table_schema.getColumn("", "facility_registry_facilityid")+", "+table_schema.getColumn("", "facility_registry_facility_type")+", "
//					+ table_schema.getColumn("", "facility_registry_facility_name")+", "+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
//					+ "VALUES("+ zilla +", "+ upazila +", "+ union +", "+ facility_id +", '"+ facilitytype[1] +"', '"+ unionfacilityname + "', "
//					+ facilitytype[0] +") "
//					+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//
//
//			//System.out.println("Unionid Not NUll="+sql);
//		}
//		else if((!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null")) && (formValue.getString("unionid").equals("") || formValue.getString("unionid").equals("null"))){
//
//			if(Integer.parseInt(facilitytype[0])==5)
//			{
//
//				System.out.println("Facility Type 2nd query");
//
////				sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_upazilaid")+", "
////						+table_schema.getColumn("", "facility_registry_facilityid")+", "+table_schema.getColumn("", "facility_registry_facility_type")+", "+table_schema.getColumn("", "facility_registry_facility_name")+", "
////						+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
////						+ "VALUES("+ zilla +", "+ upazila +", "+ facility_id +", '"+ facilitytype[1] +"', "
////						+ "'MCH-FP Unit,'||' '||(SELECT "+table_schema.getColumn("", "upazila_upazilaname")+" from "+table_schema.getTable("table_upazila")+" where "+table_schema.getColumn("", "upazila_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "upazila_upazilaid",new String[]{Integer.toString(upazila)},"=")+") || ' ' || "
////						+ "(SELECT "+table_schema.getColumn("", "facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("", "facility_type_type",new String[]{facilitytype[0]},"=")+") || '"+ facility_no +"', "+ facilitytype[0] +") "
////						+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//
//
//				sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_upazilaid")+", "
//						+table_schema.getColumn("", "facility_registry_facilityid")+", "+table_schema.getColumn("", "facility_registry_facility_type")+", "+table_schema.getColumn("", "facility_registry_facility_name")+", "
//						+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
//						+ "VALUES("+ zilla +", "+ upazila +", "+ facility_id +", '"+ facilitytype[1] +"', '"+ unionfacilityname + "', "
//						+ facilitytype[0] +") "
//						//+ "'MCH-FP Unit,'||' '||(SELECT "+table_schema.getColumn("", "upazila_upazilaname")+" from "+table_schema.getTable("table_upazila")+" where "+table_schema.getColumn("", "upazila_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "upazila_upazilaid",new String[]{Integer.toString(upazila)},"=")+") || ' ' || "
//						//+ "(SELECT "+table_schema.getColumn("", "facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("", "facility_type_type",new String[]{facilitytype[0]},"=")+") || '"+ facility_no +"', "+ facilitytype[0] +") "
//						+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//
//			}
//			else
//			{
//				System.out.println("Facility Type 3rd query");
//				System.out.println("FacilityType="+ facilitytype[0]);
//
//
////				sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_upazilaid")+", "
////						+table_schema.getColumn("", "facility_registry_facilityid")+", "+table_schema.getColumn("", "facility_registry_facility_type")+", "+table_schema.getColumn("", "facility_registry_facility_name")+", "
////						+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
////						+ "VALUES("+ zilla +", "+ upazila +", "+ facility_id +", '"+ facilitytype[1] +"', "
////						+ "(SELECT "+table_schema.getColumn("", "upazila_upazilaname")+" from "+table_schema.getTable("table_upazila")+" where "+table_schema.getColumn("", "upazila_zillaid",new String[]{Integer.toString(zilla)},"=")+" AND "+table_schema.getColumn("", "upazila_upazilaid",new String[]{Integer.toString(upazila)},"=")+") || ' ' || "
////						+ "(SELECT "+table_schema.getColumn("", "facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("", "facility_type_type",new String[]{facilitytype[0]},"=")+") || '"+ facility_no +"', "+ facilitytype[0] +") "
////						+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//
//				sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_upazilaid")+", "
//						+table_schema.getColumn("", "facility_registry_facilityid")+", "+table_schema.getColumn("", "facility_registry_facility_type")+", "+table_schema.getColumn("", "facility_registry_facility_name")+", "
//						+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
//						+ "VALUES("+ zilla +", "+ upazila +", "+ facility_id +", '"+ facilitytype[1] +"', '"+ unionfacilityname + "', "
//						+ facilitytype[0] +") "
//						+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//
//
//			}
//
//
//			//System.out.println("2nd SQL="+sql);
//		}
//		else if((formValue.getString("upazilaid").equals("") || formValue.getString("upazilaid").equals("null")) && (formValue.getString("unionid").equals("") || formValue.getString("unionid").equals("null"))){
//
//			/*
//			if(Integer.parseInt(facilitytype[0])==5)
//			{
//				System.out.println("Facility Type 3rd=UHC");
//			}
//			*/
//			System.out.println("Facility Type 4th query");
//
////			sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_facilityid")+", "
////					+table_schema.getColumn("", "facility_registry_facility_type")+", "+table_schema.getColumn("", "facility_registry_facility_name")+", "+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
////					+ "VALUES("+ zilla +", "+ facility_id +", '"+ facilitytype[1] +"', "
////					+ "(SELECT "+table_schema.getColumn("", "zilla_zillaname")+" from "+table_schema.getTable("table_zilla")+" where "+table_schema.getColumn("", "zilla_zillaid",new String[]{Integer.toString(zilla)},"=")+") || ' ' || "
////					+ "(SELECT "+table_schema.getColumn("", "facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("", "facility_type_type",new String[]{facilitytype[0]},"=")+") || '"+ facility_no +"', "+ facilitytype[0] +") "
////					+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//
//
//
//			sql = "insert into "+table_schema.getTable("table_facility_registry")+" ("+table_schema.getColumn("", "facility_registry_zillaid")+", "+table_schema.getColumn("", "facility_registry_facilityid")+", "
//					+table_schema.getColumn("", "facility_registry_facility_type")+", "+table_schema.getColumn("", "facility_registry_facility_name")+", "+table_schema.getColumn("", "facility_registry_facility_type_id")+")"
//					+ "VALUES("+ zilla +", "+ facility_id +", '"+ facilitytype[1] +"', '"+ unionfacilityname + "', "
//					+ facilitytype[0] +") "
//					+ "RETURNING "+table_schema.getColumn("", "facility_registry_facilityid")+"";
//
//		}
		System.out.println(sql);
		
		
		try{
			ResultSet rs2 = dbOp.dbExecute(dbObject,sql).getResultSet();
			while(rs2.next()){
				status = true;
			}
			if(status==true){
                setJsonForAPICall(formValue,facility_id,facilitytype[1]);
            }
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		
		return status;
		
	}

	public static void setJsonForAPICall(JSONObject formValue, Integer facilityid, String facilitytype){
	    formValue.put("facilityid",facilityid);
	    formValue.put("facility_name",formValue.get("UnionFacilityName"));
	    formValue.put("facility_type",facilitytype);
		formValue.put("lat","lat");
		formValue.put("lon","lon");
		formValue.put("facility_name_eng",formValue.get("UnionFacilityName"));

    }


}
