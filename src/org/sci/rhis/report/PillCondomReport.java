package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import java.time.YearMonth;

public class PillCondomReport {
	
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String dateType;
    public static String yearSelect;
    public static String monthSelect;
	public static String startDate;
	public static String endDate;
    public static String resultSet;

	public static String getResultSet(JSONObject reportCred){
        String tableHtml = "";
        try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			union = reportCred.getString("report1_union");
			dateType = reportCred.getString("report1_dateType");
            String facility_id =  reportCred.getString("facilityid");
            startDate = LocalDate.now().withDayOfMonth(1).toString();
            endDate =LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).toString();

            try {
                if(dateType.equals("1")) {
                    String[] date = monthSelect.split("-");
                    int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                    startDate = monthSelect + "-01";
                    endDate = monthSelect + "-" + daysInMonth;
                }
                else if(dateType.equals("3"))
                {
                    yearSelect = reportCred.getString("report1_year");
                    startDate = yearSelect + "-01-01";
                    endDate = yearSelect + "-12-31";
                }else {
                    startDate = reportCred.getString("report1_start_date");
                    endDate = reportCred.getString("report1_end_date");
                    if(startDate.equals("")){
                        LocalDate date= LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
                        startDate = date.withDayOfMonth(1).toString();
                    }
                }
                DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
                FacilityDB dbFacility2nd = new FacilityDB();
                DBInfoHandler dbObject2nd = dbFacility2nd.facilityDBInfo(Integer.parseInt(zilla));
                DBOperation dbOp2nd = new DBOperation();
                dbObject2nd = dbOp2nd.dbCreateStatement(dbObject2nd);
                dbObject2nd = dbOp2nd.dbExecute(dbObject2nd, "SELECT "+table_schema.getColumn("","facility_provider_provider_id")+" from facility_provider where facility_id = "+facility_id);
                ResultSet provider_obj = dbObject2nd.getResultSet();
                String providerList = "";
                while (provider_obj.next()) {
                    String id = provider_obj.getString("providerid");
                    providerList += id +",";
                }
                providerList = providerList.substring(0, providerList.length() - 1);
                String sql = "SELECT " +
                        "CASE when rs."+ table_schema.getColumn("","regserial_serialno") +" IS NULL then '' else CONCAT(rs."+ table_schema.getColumn("","regserial_serialno") +",'/',rs."+ table_schema.getColumn("","regserial_systementrydate") +") end as serial, " +
                        "CONCAT(cm."+ table_schema.getColumn("","clientmap_name") +",'</br>(',cm."+ table_schema.getColumn("","clientmap_healthid") +",')') as client_name, "+
                        "cm.age as client_age, "+
                        "cm."+ table_schema.getColumn("","clientmap_husbandname") +" as client_husband, "+
                        "cm."+ table_schema.getColumn("","clientmap_hhno") +" as client_house_holding, "+
                        "cm."+ table_schema.getColumn("","clientmap_mobileno") +" as client_mobile, "+
                        "vil."+ table_schema.getColumn("","village_villagename") +" as client_village, "+
                        "CASE WHEN elc."+ table_schema.getColumn("","elco_son") +" IS NULL THEN 0 ELSE elc."+ table_schema.getColumn("","elco_son") +"::integer END as client_son, "+
                        "CASE WHEN elc."+ table_schema.getColumn("","elco_dau") +" IS NULL THEN 0 ELSE elc."+ table_schema.getColumn("","elco_dau") +"::integer END as client_dau, "+
                        "CASE WHEN fpe."+ table_schema.getColumn("","fpexamination_bpsystolic") +" IS NULL THEN '' ELSE CONCAT(fpe."+ table_schema.getColumn("","fpexamination_bpsystolic") +",'/',fpe."+ table_schema.getColumn("","fpexamination_bpdiastolic") +") END as client_bp, "+
                        "CASE fpe."+ table_schema.getColumn("","fpexamination_jaundice") +"::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as client_jaundice, "+
                        "CASE  fpe."+ table_schema.getColumn("","fpexamination_diabetes") +"::integer when 1 then 'নাই' when 2 then 'আছে' else '' END as client_diabetic, "+
                        "CASE pc."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_1st,  " +
                        "CASE pil2."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_2nd,  " +
                        "CASE pil3."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_3rd,  " +
                        "CASE pil4."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_4th,  " +
                        "CASE pil5."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_5th,  " +
                        "CASE pil6."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_6th,  " +
                        "CASE pil7."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_7th,  " +
                        "CASE pil8."+ table_schema.getColumn("","pillcondomservice_methodtype") +"::integer when 1 then concat('সুখী বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র')  when 10 then concat('আপন বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 999 then concat('অন্যান্য বড়ি </br>',pc."+ table_schema.getColumn("","pillcondomservice_amount") +",' &nbsp;চক্র') when 3 then 'ইনজেকটেবলস'  when 4 then 'আইইউডি'  when 6 then 'ইমপ্ল্যান্ট'  when 7 then '্থায়ী পদ্ধতি</br>(পুরুষ)'  when 8 then '্থায়ী পদ্ধতি</br>(মহিলা)'  when 100 then 'পার্শ্ব প্রতিক্রিয়ার</br> জন্য' when 101 then 'সন্তান নিবে' when 102 then 'মজুদ সংকট<br>সরবরাহ নাই' when 103 then 'স্বামী বিদেশে</br>কাছে নাই' ELSE '' END " +
                        "as pil_8th  " +

                        " FROM "+ table_schema.getTable("table_pillcondomservice")  +" as pc " +
                        "LEFT JOIN "+ table_schema.getTable("table_fpexamination")  +" as fpe ON pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" = fpe."+ table_schema.getColumn("","fpexamination_healthid") +" and fpe."+ table_schema.getColumn("","fpexamination_serviceid") +"=1 and fpe."+ table_schema.getColumn("","fpexamination_fptype") +"=1 "+
                        "LEFT JOIN "+ table_schema.getTable("table_pillcondomservice")  +" as pil2 ON pil2."+ table_schema.getColumn("","pillcondomservice_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pil2."+ table_schema.getColumn("","pillcondomservice_serviceid") +"=2 "+
                        "LEFT JOIN "+ table_schema.getTable("table_pillcondomservice")  +" as pil3 ON pil3."+ table_schema.getColumn("","pillcondomservice_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pil3."+ table_schema.getColumn("","pillcondomservice_serviceid") +"=3 "+
                        "LEFT JOIN "+ table_schema.getTable("table_pillcondomservice")  +" as pil4 ON pil4."+ table_schema.getColumn("","pillcondomservice_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pil4."+ table_schema.getColumn("","pillcondomservice_serviceid") +"=4 "+
                        "LEFT JOIN "+ table_schema.getTable("table_pillcondomservice")  +" as pil5 ON pil5."+ table_schema.getColumn("","pillcondomservice_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pil5."+ table_schema.getColumn("","pillcondomservice_serviceid") +"=5 "+
                        "LEFT JOIN "+ table_schema.getTable("table_pillcondomservice")  +" as pil6 ON pil6."+ table_schema.getColumn("","pillcondomservice_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pil6."+ table_schema.getColumn("","pillcondomservice_serviceid") +"=6 "+
                        "LEFT JOIN "+ table_schema.getTable("table_pillcondomservice")  +" as pil7 ON pil7."+ table_schema.getColumn("","pillcondomservice_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pil7."+ table_schema.getColumn("","pillcondomservice_serviceid") +"=7 "+
                        "LEFT JOIN "+ table_schema.getTable("table_pillcondomservice")  +" as pil8 ON pil8."+ table_schema.getColumn("","pillcondomservice_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pil8."+ table_schema.getColumn("","pillcondomservice_serviceid") +"=8 "+
                        "LEFT JOIN "+ table_schema.getTable("table_clientmap")  +" as cm ON cm."+ table_schema.getColumn("","clientmap_generatedid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pc."+ table_schema.getColumn("","pillcondomservice_serviceid") +" = 1 " +
                        "LEFT JOIN "+ table_schema.getTable("table_regserial")  +" as rs ON rs."+ table_schema.getColumn("","regserial_healthid") +"=cm."+ table_schema.getColumn("","clientmap_healthid") +" and rs."+ table_schema.getColumn("","regserial_servicecategory") +" =1 and rs."+ table_schema.getColumn("","regserial_providerid") +" IN ("+providerList+") and pc."+ table_schema.getColumn("","pillcondomservice_serviceid") +" = 1  " +
                        "LEFT JOIN "+ table_schema.getTable("table_elco")  +" as elc ON elc."+ table_schema.getColumn("","elco_healthid") +" = pc."+ table_schema.getColumn("","pillcondomservice_healthid") +" and pc."+ table_schema.getColumn("","pillcondomservice_serviceid") +" = 1 " +
                        "LEFT JOIN "+ table_schema.getTable("table_village")  +" as vil ON cm."+ table_schema.getColumn("","clientmap_villageid") +" = vil."+ table_schema.getColumn("","village_villageid") +" and cm."+ table_schema.getColumn("","clientmap_mouzaid") +" = vil."+ table_schema.getColumn("","village_mouzaid") +" and cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = vil."+ table_schema.getColumn("","village_upazilaid") +" and cm."+ table_schema.getColumn("","clientmap_unionid") +" = vil."+ table_schema.getColumn("","village_unionid") +"  and cm."+ table_schema.getColumn("","clientmap_zillaid") +" = vil."+ table_schema.getColumn("","village_zillaid") +" " +
                        "WHERE  pc."+ table_schema.getColumn("","pillcondomservice_providerid") +" IN ("+providerList+") and pc."+ table_schema.getColumn("","pillcondomservice_serviceid") +" = 1 and pc."+ table_schema.getColumn("","pillcondomservice_visitdate") +" BETWEEN '"+startDate+"' AND '"+endDate+"' order by pc."+ table_schema.getColumn("","pillcondomservice_visitdate") +"  asc";
                System.out.println(sql);
                FacilityDB dbFacility = new FacilityDB();
                DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla,upazila);

                DBOperation dbOp = new DBOperation();
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject, sql);
                ResultSet rs = dbObject.getResultSet();
                //*******************************************//

                List<JSONObject> jsonObj = new ArrayList<JSONObject>();
                ResultSetMetaData meta = rs.getMetaData();
                while (rs.next()) {
                    HashMap<String, String> data = new HashMap<String, String>();
                    for (int i = 1; i <= meta.getColumnCount(); i++) {
                        String key = meta.getColumnName(i);
                        String value = rs.getString(key);
                        if(value == null){
                            value = "";
                        }
                        data.put(key, value);
                    }
                    jsonObj.add(new JSONObject(data));
                }
                return jsonObj.toString();

                //*******************************************//
            }catch (Exception e){
                System.out.println(e);
            }

//			System.out.println(startDate+"   "+endDate);
		}
		catch(Exception e){
			System.out.println(e);
            resultSet = "Bad Url Request";
		}
		return resultSet;
	}

}
