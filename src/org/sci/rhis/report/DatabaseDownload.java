package org.sci.rhis.report;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.json.JSONObject;
import org.sci.rhis.db.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class DatabaseDownload {

    public static Integer zilla;
    public static String zillaname;
    public static Integer upazila;
    public static String providerCode;
    public static String servername;
    public static String nodename;
    public static String sql_nodeDetails;
    public static String sql_sym_node_security;

    public static boolean setDownloadStatus(JSONObject formValue){

        zilla = Integer.parseInt(formValue.getString("zillaid"));

        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

        DBOperation dbOp = new DBOperation();

        boolean status = false;

        DBInfoHandler detailDomain = new DBInfoHandler();
        Properties prop = ConfInfoRetrieve.readingConf(detailDomain.getDomainLoc());
        String[] DomainDetails = prop.getProperty("DOMAININFO_"+zilla).split(",");

        String host=DomainDetails[0];
        String user=DomainDetails[1];
        String password=DomainDetails[2];
        Integer portNum=Integer.parseInt(DomainDetails[3]);

        try{


            upazila = Integer.parseInt(formValue.getString("upazilaid"));
            providerCode = formValue.getString("providerId");

            sql_nodeDetails = "update "+table_schema.getTable("table_node_details")+" set " +
                    table_schema.getColumn("","node_details_dbdownloadstatus",new String[]{"0"},"=")+" " +
                    "where "+table_schema.getColumn("","node_details_provcode",new String[]{providerCode},"=")+" " +
                    "RETURNING "+table_schema.getColumn("","node_details_id")+" as node_id";


            DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
            ResultSet rs = dbOp.dbExecute(dbObject,sql_nodeDetails).getResultSet();

            if(rs.next()){

                if(zilla.equals(93) && upazila.equals(9))
                    servername = "tangail-93-9";
                else
                    servername = zillaname+"-"+String.format("%02d", zilla)+"-"+String.format("%02d", upazila);
                nodename = rs.getString("node_id");

                String command1="bash /var/lib/symmetricds/symmetric-server-3.7.27/bin/symadmin --engine "+servername+" open-registration FWV "+nodename;
                ArrayList<String> result = new ArrayList<String>();

                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                JSch jsch = new JSch();
                Session session=jsch.getSession(user, host, portNum);
                session.setPassword(password);
                session.setConfig(config);
                session.connect();
                System.out.println("Connected");

                Channel channel=session.openChannel("exec");
                ((ChannelExec)channel).setCommand(command1);
                channel.setInputStream(null);
                ((ChannelExec)channel).setErrStream(System.err);

                InputStream in=channel.getInputStream();
                channel.connect();

                // Read the output from the input stream we set above
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;

                //Read each line from the buffered reader and add it to result list
                // You can also simple print the result here
                while ((line = reader.readLine()) != null)
                {
                    result.add(line);
                }
                System.out.println(Arrays.asList(result));
                //retrieve the exit status of the remote command corresponding to this channel
                int exitStatus = channel.getExitStatus();

                channel.disconnect();
                session.disconnect();
                System.out.println("DONE");

                if(!result.isEmpty()){
                    sql_sym_node_security = "update sym_node_security set initial_load_enabled=0 where node_id="+nodename;
                    DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
                    dbOp.dbExecuteUpdate(dbObject_dynamic,sql_sym_node_security);

                    dbOp.closeStatement(dbObject_dynamic);
                    dbOp.closeConn(dbObject_dynamic);
                    dbObject_dynamic = null;
                }
            }
            else{
                status = false;
            }

            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;


            //System.out.println(status);

        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{

        }

        return status;
    }
}
