package org.sci.rhis.report;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;

import org.json.JSONObject;
import org.sci.rhis.db.*;

public class GetFacilityInfo {
	
	public static Integer zilla;
	public static Integer upazila;
	public static Integer union;
	public static String supervisorId;
	public static String facilityType;
	
	public static JSONObject getFacilityInfo(JSONObject formValue) throws NumberFormatException, SQLException{
		if(formValue.has("zillaid")) {
			if(!formValue.getString("zillaid").equals("") && !formValue.getString("zillaid").equals("null"))
				zilla = Integer.parseInt(formValue.getString("zillaid"));
			else
				zilla = 0;
		}
		else
			zilla = 0;

		DBSchemaInfo table_schema = new DBSchemaInfo();
		
		if(formValue.has("upazilaid")) {
			if(!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))
				upazila = Integer.parseInt(formValue.getString("upazilaid"));
			else
				upazila = 0;
		}
		else
			upazila = 0;
		
		if(formValue.has("unionid")) {
			if(!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))
				union = Integer.parseInt(formValue.getString("unionid"));
			else
				union = 0;
		}
		else
			union = 0;
		
		facilityType = formValue.getString("facilityType");
		
		if(formValue.has("supervisorId"))
			supervisorId = formValue.getString("supervisorId");
		else
			supervisorId = "0";
		
		System.out.println(facilityType);
		
		if(!supervisorId.equals("0")) {
			DBOperation dbOp = new DBOperation();
			DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo();
			String sql_provider = "select * from "+table_schema.getTable("table_providerdb")+" where "+table_schema.getColumn("","providerdb_provcode")+"="+supervisorId;
			System.out.println(sql_provider);
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql_provider);
			ResultSet rs = dbObject.getResultSet();
			
			while(rs.next()){
				zilla = Integer.parseInt(rs.getString("zillaid"));
				upazila = Integer.parseInt(rs.getString("upazilaid"));
			}
			
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);
		
		String sql = "";
		
		if(!supervisorId.equals("0")) {
			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" "
				+ "inner join "+table_schema.getTable("table_facility_provider")+" on "+table_schema.getColumn("table","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" "
				+ "where "+table_schema.getColumn("table","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("table","facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=")+" group by "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+"";
		}
//		else if(facilityType.equals("0") && union==0)
//			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=");
//		else if(facilityType.equals("0"))
//			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and "+table_schema.getColumn("","facility_registry_unionid",new String[]{Integer.toString(union)},"=");
//		else if(facilityType.equals("6") || facilityType.equals("7"))
//			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","facility_registry_facility_type_id",new String[]{facilityType},"=");
//		else if(facilityType.equals("5"))
//			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and "+table_schema.getColumn("","facility_registry_facility_type_id",new String[]{facilityType},"=");
		else {
			if(facilityType.equals("0"))
				sql = "select " + table_schema.getColumn("", "facility_registry_facilityid") + ", " + table_schema.getColumn("", "facility_registry_facility_name") + " from " + table_schema.getTable("table_facility_registry") + " where " + table_schema.getColumn("", "facility_registry_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "facility_registry_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "facility_registry_unionid", new String[]{Integer.toString(union)}, "=");
			else
				sql = "select " + table_schema.getColumn("", "facility_registry_facilityid") + ", " + table_schema.getColumn("", "facility_registry_facility_name") + " from " + table_schema.getTable("table_facility_registry") + " where " + table_schema.getColumn("", "facility_registry_zillaid", new String[]{Integer.toString(zilla)}, "=") + " and " + table_schema.getColumn("", "facility_registry_upazilaid", new String[]{Integer.toString(upazila)}, "=") + " and " + table_schema.getColumn("", "facility_registry_unionid", new String[]{Integer.toString(union)}, "=") + " and " + table_schema.getColumn("", "facility_registry_facility_type_id", new String[]{facilityType}, "=");
		}

		System.out.println(sql);

//		if(!formValue.getString("unionid").equals("") || !formValue.getString("unionid").equals("null"))
//			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and "+table_schema.getColumn("","facility_registry_unionid",new String[]{Integer.toString(union)},"=");
//		else if((!formValue.getString("upazilaid").equals("") || !formValue.getString("upazilaid").equals("null")) && (formValue.getString("unionid").equals("") || formValue.getString("unionid").equals("null")))
//			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and unionid is null";
//		else if((formValue.getString("upazilaid").equals("") || formValue.getString("upazilaid").equals("null")) && (formValue.getString("unionid").equals("") || formValue.getString("unionid").equals("null")))
//			sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "+table_schema.getColumn("","facility_registry_facility_name")+" from "+table_schema.getTable("table_facility_registry")+" where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")+" and upazilaid is null and unionid is null";
		
		
		JSONObject facility_info = new JSONObject();
		
		try{
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			
			ResultSetMetaData metadata = rs.getMetaData();
		    
			while(rs.next()){
				facility_info.put(rs.getString(metadata.getColumnName(1)), rs.getString(metadata.getColumnName(2)));
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		System.out.println(facility_info);
		return facility_info;
		
	}

	public static JSONObject implementInfo(){
		JSONObject implementFacilityInfo = new JSONObject();

		try {
			DBListProperties setZillaList = new DBListProperties();
			Properties db_prop = setZillaList.setProperties();
			String[] zilla_list = db_prop.getProperty("zilla_all").split(",");
			String sql = "";

			for(int i=0; i<zilla_list.length;i++){
				DBSchemaInfo table_schema = new DBSchemaInfo(zilla_list[i]);

				sql = "select initcap("+table_schema.getColumn("","zilla_zillanameeng")+") as zilla_name,"+table_schema.getColumn("","facility_registry_facility_type")+" as facility_type, " +
						"count(distinct("+table_schema.getColumn("", "facility_provider_facility_id")+")) as total_facility " +
						"from "+table_schema.getTable("table_facility_registry")+" " +
						"join "+table_schema.getTable("table_facility_provider")+" on " +table_schema.getColumn("table", "facility_provider_facility_id")+"="+table_schema.getColumn("table", "facility_registry_facilityid")+" " +
						"join "+table_schema.getTable("table_zilla")+" using("+table_schema.getColumn("", "facility_registry_zillaid")+") " +
						"where "+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+" " +
						"group by "+table_schema.getColumn("","facility_registry_facility_type")+","+table_schema.getColumn("","zilla_zillanameeng");

				System.out.println(sql);

				implementFacilityInfo.put(zilla_list[i], GetResultSet.getFacilityInfoResult(sql,zilla_list[i]));
			}
		}catch (Exception e){
			e.printStackTrace();
		}


		return implementFacilityInfo;
	}

	/**
	 * @Author: evana
	 * @Date: 13/06/2019
	 * @Title: Facility Detail information
	 * **/

	public static String getFacilityDetails(JSONObject formValue){
		String resultString = "";
        String sql = "";
		zilla = Integer.parseInt(formValue.getString("report1_zilla"));
		//upazila = Integer.parseInt(formValue.getString("report1_upazila"));

		if(formValue.get("report1_upazila").equals(""))
		{
			upazila = 0;
			//System.out.println("Null Upazilaid="+ upazila);
		}
		else
		{
			upazila = Integer.parseInt(formValue.getString("report1_upazila"));
		}

		//int numberOfDigits = String.valueOf(upazila).length();

		System.out.println("FormValue="+ formValue);
		//System.out.println("numberOfDigits="+ numberOfDigits);
		//System.out.println("Upazilaid="+ upazila);

		DBSchemaInfo table_schema = new DBSchemaInfo();
		try{
			facilityType = formValue.getString("facilityType");

			//facilityType = Integer.parseInt(formValue.getString("facilityType"));
			//System.out.println("FacilityType="+ facilityType);


			if(upazila==0)
			{
				//System.out.println("upazilla is empty="+ upazila);

                 sql = "select "+table_schema.getColumn("", "facility_registry_facilityid")+" as \"Facility ID\","
                        + table_schema.getColumn("", "facility_registry_facility_name")+" as \"Facility Name\", "
                        + "(case when facilityid in (afp.facility_id)  then 'Active' when facilityid in (ifp.facility_id)  then 'Inactive' else 'Never Assigned' end) as \"Status\", "+table_schema.getColumn("", "facility_registry_facility_type")+" as \"Facility Type\", "+table_schema.getColumn("", "facility_registry_facility_category")+" as \"Facility Category\", "+table_schema.getColumn("table", "zilla_zillanameeng")+" as \"Zilla Name\", "+table_schema.getColumn("table","upazila_upazilanameeng")+" as \"Upazila Name\" "
                        + " from "+table_schema.getTable("table_facility_registry")+" left join "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table", "facility_registry_zillaid")+"="+table_schema.getColumn("table", "unions_zillaid")+" and "+table_schema.getColumn("table", "facility_registry_upazilaid")+"="+table_schema.getColumn("table","unions_upazilaid")+" and "+table_schema.getColumn("table", "facility_registry_unionid")+"="+table_schema.getColumn("table", "unions_unionid")+" "
                        + "left join "+table_schema.getTable("table_upazila")+" on "+table_schema.getColumn("table", "facility_registry_zillaid")+"="+table_schema.getColumn("table","upazila_zillaid")+" and "+table_schema.getColumn("table", "facility_registry_upazilaid")+"="+table_schema.getColumn("table","upazila_upazilaid")+" "
                        + "left join "+table_schema.getTable("table_zilla")+" on "+table_schema.getColumn("table", "facility_registry_zillaid")+"="+table_schema.getColumn("table","zilla_zillaid")+" "
                        + "left join (select  distinct facility_id from" +table_schema.getTable("table_facility_provider")+ "where "+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")
                        + ")as afp on afp.facility_id"+"="+ table_schema.getColumn("table", "facility_registry_facilityid")
                        + "left join (select  distinct facility_id from" +table_schema.getTable("table_facility_provider")+ " fp where not exists (select 1 from"
                        + table_schema.getTable("table_facility_provider")+ " p where p.facility_id = fp.facility_id and"
                        +table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+")"
                        + ")as ifp on ifp.facility_id"+"="+ table_schema.getColumn("table", "facility_registry_facilityid")
                        +"where "+table_schema.getColumn("table", "facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=");
                //System.out.println("Facility Details without upazilaid="+sql);


			}
			else
			{

				 sql =  "select "+table_schema.getColumn("", "facility_registry_facilityid")+" as \"Facility ID\","
                        + table_schema.getColumn("", "facility_registry_facility_name")+" as \"Facility Name\", "
                        + "(case when facilityid in (afp.facility_id)  then 'Active' when facilityid in (ifp.facility_id)  then 'Inactive' else 'Never Assigned' end) as \"Status\", "+table_schema.getColumn("", "facility_registry_facility_type")+" as \"Facility Type\", "+table_schema.getColumn("", "facility_registry_facility_category")+" as \"Facility Category\", "+table_schema.getColumn("table", "zilla_zillanameeng")+" as \"Zilla Name\", "+table_schema.getColumn("table","upazila_upazilanameeng")+" as \"Upazila Name\" "
                        + " from "+table_schema.getTable("table_facility_registry")+" left join "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table", "facility_registry_zillaid")+"="+table_schema.getColumn("table", "unions_zillaid")+" and "+table_schema.getColumn("table", "facility_registry_upazilaid")+"="+table_schema.getColumn("table","unions_upazilaid")+" and "+table_schema.getColumn("table", "facility_registry_unionid")+"="+table_schema.getColumn("table", "unions_unionid")+" "
                        + "left join "+table_schema.getTable("table_upazila")+" on "+table_schema.getColumn("table", "facility_registry_zillaid")+"="+table_schema.getColumn("table","upazila_zillaid")+" and "+table_schema.getColumn("table", "facility_registry_upazilaid")+"="+table_schema.getColumn("table","upazila_upazilaid")+" "
                        + "left join "+table_schema.getTable("table_zilla")+" on "+table_schema.getColumn("table", "facility_registry_zillaid")+"="+table_schema.getColumn("table","zilla_zillaid")+" "
                        + "left join (select  distinct facility_id from" +table_schema.getTable("table_facility_provider")+ "where "+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")
                        + ")as afp on afp.facility_id"+"="+ table_schema.getColumn("table", "facility_registry_facilityid")
                        + "left join (select  distinct facility_id from" +table_schema.getTable("table_facility_provider")+ " fp where not exists (select 1 from"
                        + table_schema.getTable("table_facility_provider")+ " p where p.facility_id = fp.facility_id and"
                        +table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+")"
                        + ")as ifp on ifp.facility_id"+"="+ table_schema.getColumn("table", "facility_registry_facilityid")
                        + "where "+table_schema.getColumn("table", "facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=")
						 +" and" +table_schema.getColumn("table", "facility_registry_upazilaid",new String[]{Integer.toString(upazila)},"=");

				//System.out.println("Facility Details with upazila id="+sql);

//				resultString = GetResultSet.getFacilityResult(sql,zilla,upazila,facilityType);
			}

			if(!facilityType.equals("All")){
				sql += " and "+table_schema.getColumn("", "facility_registry_facility_type",new String[]{facilityType},"=");

			}

			resultString = GetResultSet.getFacilityResult(sql,zilla,upazila,facilityType,true);



		}
		catch(Exception e){
			System.out.println(e);
		}

		return resultString;
	}

	/**
	 * Facility Management section
	 * formtypevalue = 28
	 * @return facility wise provider info
	 */

	public static String getFacilitywiseProviderList(JSONObject formValue){
		String resultString = "";
		String sql = "";
		zilla = Integer.parseInt(formValue.getString("report1_zilla"));

		if(formValue.get("report1_upazila").equals("")) {
			upazila = 0;
		} else {
			upazila = Integer.parseInt(formValue.getString("report1_upazila"));
		}

		DBSchemaInfo table_schema = new DBSchemaInfo();
		try{
			facilityType = formValue.getString("facilityType");

		    sql = "select z.zillanameeng as zilla,up.upazilanameeng as upazila,concat(fr.facility_name,' (',fp.facility_id,')') as Facility," +
					" STRING_AGG ((case when ap.provider_id is not null" +
					" then concat(pd.provname,'(Additional -',ap.provider_id,')')" +
					" else concat(pd.provname,'(',fp.providerid,')') end" +
					") ,',') providers" +
					" from "+table_schema.getTable("table_facility_registry")+" fr" +
					" inner join "+table_schema.getTable("table_facility_provider")+" fp on fp.facility_id=fr.facilityid" +
					" left join "+table_schema.getTable("table_zilla")+" z using (zillaid)" +
					" left join "+table_schema.getTable("table_upazila")+" up using (zillaid,upazilaid)" +
					" left join "+table_schema.getTable("table_associated_provider_id")+" ap on fp.providerid = ap.associated_id" +
					" left join "+table_schema.getTable("table_providerdb")+" pd on (pd.providerid = fp.providerid or pd.providerid= ap.provider_id) and " +
					  table_schema.getColumn("","providerdb_active",new String[]{"1"},"=")+
					" where fr.zillaid ="+zilla+ " and "+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=");


		    String condition = " group by z.zillanameeng,up.upazilanameeng,fr.facility_name,facility_id";

            //upazila is not 'All'
			if(!upazila.equals(0)){
				sql += " and fr.upazilaid = "+upazila;
			}

			if(!facilityType.equals("All")){
				sql += " and "+table_schema.getColumn("", "facility_registry_facility_type",new String[]{facilityType},"=");
			}

			sql += condition;
			resultString = GetResultSet.getFacilityResult(sql,zilla,upazila,facilityType,false);

		}
		catch(Exception e){
			System.out.println(e);
		}
		return resultString;
	}

	/**
	 * @Author: Evana
	 * @Date: 25/06/2019
	 * @Description: Get only Facility Name of Bangla Font
	 * **/

	public static JSONObject getFacilityName(JSONObject formValue) throws NumberFormatException, SQLException{
		if(formValue.has("zillaid")) {
			if(!formValue.getString("zillaid").equals("") && !formValue.getString("zillaid").equals("null"))
				zilla = Integer.parseInt(formValue.getString("zillaid"));
			else
				zilla = 0;
		}
		else
			zilla = 0;


		if(formValue.has("upazilaid")) {
			if(!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))
				upazila = Integer.parseInt(formValue.getString("upazilaid"));
			else
				upazila = 0;
		}
		else
			upazila = 0;

		if(formValue.has("unionid")) {
			if(!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))
				union = Integer.parseInt(formValue.getString("unionid"));
			else
				union = 0;
		}
		else
			union = 0;

		facilityType = formValue.getString("facilityType");

		//System.out.println("facilityType="+facilityType);

		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBSchemaInfo table_schema = new DBSchemaInfo();

		DBInfoHandler dbObject = dbFacility.facilityDBInfo(36);
		//DBInfoHandler dbObject_new = dbFacility.facilityDBInfo(zilla);

		System.out.println("Database="+ dbObject.getDBName());
		String sql = "";
		String sector_sql = "";


		if((upazila == 0)&&(union ==0))
		{
			sql = "select "+table_schema.getColumn("","facility_type_type")+", "+table_schema.getColumn("","facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("","facility_type_type",new String[]{Integer.toString(Integer.parseInt(facilityType))},"=");
			sector_sql = "select "+table_schema.getColumn("","zilla_zillaid")+","+table_schema.getColumn("","zilla_zillaname")+" from "+table_schema.getTable("table_zilla")+" where "+table_schema.getColumn("","zilla_zillaid",new String[]{Integer.toString(zilla)},"=");

			//System.out.println("zilla sql=" +sql);
			//System.out.println("zilla sector sql=" +sector_sql);
		}
		else if((union==0)&&(upazila != 0))
		{
			sql = "select "+table_schema.getColumn("","facility_type_type")+", "+table_schema.getColumn("","facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("","facility_type_type",new String[]{Integer.toString(Integer.parseInt(facilityType))},"=");
			sector_sql = "select "+table_schema.getColumn("","upazila_upazilaid")+","+table_schema.getColumn("","upazila_upazilaname")+" from "+table_schema.getTable("table_upazila")+" where "+table_schema.getColumn("","upazila_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","upazila_upazilaid",new String[]{Integer.toString(upazila)},"=");

			//System.out.println("zilla sql=" +sql);
			//System.out.println("upazila sector sql=" +sector_sql);

		}
		else if((union !=0)&&(upazila != 0))
		{
			sql = "select "+table_schema.getColumn("","facility_type_type")+", "+table_schema.getColumn("","facility_type_description_bangla")+" from "+table_schema.getTable("table_facility_type")+" where "+table_schema.getColumn("","facility_type_type",new String[]{Integer.toString(Integer.parseInt(facilityType))},"=");
			sector_sql = "select "+table_schema.getColumn("","unions_unionid")+","+table_schema.getColumn("","unions_unionname")+" from "+table_schema.getTable("table_unions")+" where "+table_schema.getColumn("","unions_zillaid",new String[]{Integer.toString(zilla)},"=")+" and "+table_schema.getColumn("","unions_upazilaid",new String[]{Integer.toString(upazila)},"=")+" and "+table_schema.getColumn("","unions_unionid",new String[]{Integer.toString(union)},"=");

			//System.out.println("zilla sql=" +sql);
			//System.out.println("upazila sector sql=" +sector_sql);

		}


		System.out.println(sql);
		JSONObject facility_info = new JSONObject();

		try{

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);

			ResultSet rs = dbObject.getResultSet();
			ResultSetMetaData metadata = rs.getMetaData();


			/**** Get sector sql data ***/

			//dbObject_new = dbOp.dbCreateStatement(dbObject_new);
			//dbObject_new = dbOp.dbExecute(dbObject_new,sector_sql);
			//ResultSet sector_rs = dbObject_new.getResultSet();
			//ResultSetMetaData metadata_sector = sector_rs.getMetaData();

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sector_sql);
			ResultSet sector_rs = dbObject.getResultSet();
			ResultSetMetaData metadata_sector = sector_rs.getMetaData();

			while(sector_rs.next()){
				//facility_info.put(sector_rs.getString(metadata_sector.getColumnName(1)), "  "+sector_rs.getString(metadata_sector.getColumnName(2))+"  ");
				facility_info.put("1", sector_rs.getString(metadata_sector.getColumnName(2))+"  ");

			}
			//facility_info.put("3", " ,");

			if(!sector_rs.isClosed()){
				sector_rs.close();
			}

			// Get facility name of selected facility type

			while(rs.next()){
				facility_info.put(rs.getString(metadata.getColumnName(1)), rs.getString(metadata.getColumnName(2)));
			}
			if(!rs.isClosed()){
				rs.close();
			}
			//System.out.println("Second Json FacilityInfo=" +facility_info);

		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;

			//dbOp.closeResultSet(dbObject_new);
			//dbOp.closePreparedStatement(dbObject_new);
			//dbOp.closeConn(dbObject_new);
			//dbObject_new = null;
		}
		//System.out.println("Json FacilityInfo=" +facility_info);

		return facility_info;

	}

	/**
	 * @Author: Evana
	 * @Date: 17/09/2019
	 * @Description: Get Facility information for modification
	 * **/

	public static JSONObject getFacilityEditInfo(JSONObject formValue) throws NumberFormatException, SQLException{
		if(formValue.has("zillaid")) {
			if(!formValue.getString("zillaid").equals("") && !formValue.getString("zillaid").equals("null"))
				zilla = Integer.parseInt(formValue.getString("zillaid"));
			else
				zilla = 0;
		}
		else
			zilla = 0;


		if(formValue.has("upazilaid")) {
			if(!formValue.getString("upazilaid").equals("") && !formValue.getString("upazilaid").equals("null"))
				upazila = Integer.parseInt(formValue.getString("upazilaid"));
			else
				upazila = 0;
		}
		else
			upazila = 0;

		if(formValue.has("unionid")) {
			if(!formValue.getString("unionid").equals("") && !formValue.getString("unionid").equals("null"))
				union = Integer.parseInt(formValue.getString("unionid"));
			else
				union = 0;
		}
		else
			union = 0;

		//facilityType = formValue.getString("facilityType");
		String facilityId = formValue.getString("facilityId");

		//System.out.println("facilityType="+facilityType);

		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBSchemaInfo table_schema = new DBSchemaInfo();

		//DBInfoHandler dbObject = dbFacility.facilityDBInfo(36);
		DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

		System.out.println("Database="+ dbObject.getDBName());
		String sql = "";
		String sector_sql = "";

		sql = "select "+table_schema.getColumn("","facility_registry_facilityid")+", "
				+table_schema.getColumn("","facility_registry_facility_type_id")+", "
				+table_schema.getColumn("","facility_registry_facility_type")+", "
				+table_schema.getColumn("","facility_registry_facility_name")+", "
				+table_schema.getColumn("","facility_registry_zillaid")+", "
				+table_schema.getColumn("","facility_registry_upazilaid")+", "
				+table_schema.getColumn("","facility_registry_unionid")
				+" from "+table_schema.getTable("table_facility_registry")+" where "
				+table_schema.getColumn("","facility_registry_facilityid",new String[]{Integer.toString(Integer.parseInt(facilityId))},"=")
				+" and" +table_schema.getColumn("", "facility_registry_zillaid",new String[]{Integer.toString(zilla)},"=");


		System.out.println("facility sql=" +sql);
		//System.out.println(sql);
		JSONObject facility_info = new JSONObject();

		try{

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);

			ResultSet rs = dbObject.getResultSet();
			ResultSetMetaData metadata = rs.getMetaData();

			while(rs.next()){
				facility_info.put("facilityid", rs.getString(metadata.getColumnName(1)));
				facility_info.put("facility_type_id", rs.getString(metadata.getColumnName(2)));
				facility_info.put("facility_type", rs.getString(metadata.getColumnName(3)));
				facility_info.put("facility_name", rs.getString(metadata.getColumnName(4)));
				facility_info.put("zillaid", rs.getString(metadata.getColumnName(5)));
				facility_info.put("upazilaid", rs.getString(metadata.getColumnName(6)));
				facility_info.put("unionid", rs.getString(metadata.getColumnName(7)));
			}
			if(!rs.isClosed()){
				rs.close();
			}

		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;

		}
		System.out.println("Json FacilityInfo=" +facility_info);

		return facility_info;

	}

	/*
	 * Facility Information Edit
	 * @Author: evana.yasmin
	 * @Date: 18/09/2019
	 * */

	public static boolean updateFacilityInfo(JSONObject formValue) {


		//DBSchemaInfo table_schema = new DBSchemaInfo();
		boolean status = false;


		try{

			Integer facilityid = Integer.parseInt(formValue.getString("facilityid"));
			Integer zillaid = Integer.parseInt(formValue.getString("zillaid"));
			Integer upazilaid = Integer.parseInt(formValue.getString("upazilaid"));
			String facilityname = formValue.getString("editfacName");



				//other district db(42,51,75) with upazilla
				//change district with upazilla.

				DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zillaid));
				FacilityDB dbFacility = new FacilityDB();
				DBOperation dbOp = new DBOperation();
				//DBInfoHandler dbObject = dbFacility.facilityDBInfo(zillacode);
				DBInfoHandler dbObject = dbFacility.facilityDBInfo(Integer.toString(zillaid));

				System.out.println("Only zillaid=" + upazilaid);

				/****  Update Zilla Db****/
				String sql_updatefacility = "update "+table_schema.getTable("table_facility_registry")+" set "+table_schema.getColumn("", "facility_registry_facility_name",new String[]{facilityname},"=")+" "
						+" where "+table_schema.getColumn("", "facility_registry_zillaid",new String[]{Integer.toString(zillaid)},"=")
						+" and "+table_schema.getColumn("", "facility_registry_upazilaid",new String[]{Integer.toString(upazilaid)},"=")+" "
						+" and "+table_schema.getColumn("", "facility_registry_facilityid",new String[]{Integer.toString(facilityid)},"=")+" " ;

				System.out.println(sql_updatefacility);

				Integer sql_status = dbOp.dbExecuteUpdate(dbObject,sql_updatefacility);

				/*
				if(sql_status == 0)
				{
					System.out.println("Facility Name can not be updated");
				}
				else
				{
					System.out.println("Facility Name successfully updated");
				}
				*/

				/*** Update zilla db end****/

				if(sql_status == 0)
				{
					return status = false;
				}
				else
				{
					return status = true;
				}

		}
		catch(Exception e){
			System.out.println(e);
			status = false;
		}

        /*
        finally{
            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        */

		return status;


	}




}
