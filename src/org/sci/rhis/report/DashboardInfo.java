package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.util.Calendar;

public class DashboardInfo {

    public static JSONObject getServiceStatistics(String providerId, String method_type){

        JSONObject serviceInfo = new JSONObject();

        try{
            DBSchemaInfo table_schema = new DBSchemaInfo("36");

            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH)+1;
            String days = "01";
            int daysInMonth = CalendarDate.daysInMonth(year, month);

            String start_date = year + "-" + month + "-" + days;
            String end_date = year + "-" + month + "-" + daysInMonth;

            DBOperation dbOp = new DBOperation();
            DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo();
            String sql_provider = "select "+table_schema.getColumn("", "providerdb_provtype")+" as provider_type from " + table_schema.getTable("table_providerdb") + " where "+table_schema.getColumn("", "providerdb_provcode",new String[]{providerId},"=") + " and "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull");
            //System.out.println(sql_provider);
            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject,sql_provider);
            ResultSet rs_provider = dbObject.getResultSet();

            String provider_type = "";

            while (rs_provider.next()){
                provider_type = rs_provider.getString("provider_type");
            }

            String zilla = "";
            String zilla_name = "";
            String upazila = "";
            String upazila_name = "";

            if(provider_type.equals("999")) {
                String sql_zilla = "select " + table_schema.getColumn("", "zilla_zillanameeng") + " as zilla_name, " + table_schema.getColumn("", "zilla_zillaid") + " as zilla_id "
                        + "from " + table_schema.getTable("table_zilla") + " "
                        + "where " + table_schema.getColumn("", "zilla_zillaid") + " in (36,42,51,75,93)";

                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject,sql_zilla);
                ResultSet rs = dbObject.getResultSet();

                while (rs.next()) {
                    zilla = rs.getString("zilla_id");
                    zilla_name = rs.getString("zilla_name");
                    String[] getResult = GetResultSet.getFinalResultUpazilaWise("2", method_type, "", zilla, start_date, end_date).split(" table/graph/map ");
                    System.out.println(getResult[3].replace("=", ":"));
                    serviceInfo.put(zilla_name, getResult[3].replace("=", ":"));
                }
            }
            else {
                String sql_upazila = "select " + table_schema.getColumn("", "upazila_upazilanameeng") + " as upazila_name, " + table_schema.getColumn("", "upazila_zillaid") + " as zilla_id, " + table_schema.getColumn("", "upazila_upazilaid") + " as upazila_id "
                        + "from " + table_schema.getTable("table_upazila") + " where " + table_schema.getColumn("", "upazila_zillaid") + " in "
                        + "(select " + table_schema.getColumn("", "providerdb_zillaid") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("", "providerdb_provcode", new String[]{providerId}, "=") + " and " + table_schema.getColumn("", "providerdb_exdate", new String[]{}, "isnull") + ")";

                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject,sql_upazila);
                ResultSet rs = dbObject.getResultSet();

                while (rs.next()) {
                    zilla = rs.getString("zilla_id");
                    upazila = rs.getString("upazila_id");
                    upazila_name = rs.getString("upazila_name");
                    String[] getResult = GetResultSet.getFinalResultUnionWise("3", method_type, "", zilla, upazila, start_date, end_date).split(" table/graph/map ");
                    System.out.println(getResult[3].replace("=", ":"));
                    serviceInfo.put(upazila_name, getResult[3].replace("=", ":"));
                }
            }

            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return serviceInfo;

    }

    public static String getZillaId(String providerId){

        String zilla = "";

        try{
            DBSchemaInfo table_schema = new DBSchemaInfo("36");

            DBOperation dbOp = new DBOperation();
            DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo();
            String sql_provider = "select "+table_schema.getColumn("", "providerdb_zillaid")+" as zilla_id from " + table_schema.getTable("table_providerdb") + " where "+table_schema.getColumn("", "providerdb_provcode",new String[]{providerId},"=") + " and "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull");
            //System.out.println(sql_provider);
            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject,sql_provider);
            ResultSet rs = dbObject.getResultSet();

            while (rs.next()){
                zilla = rs.getString("zilla_id");
            }

            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return zilla;

    }
}
