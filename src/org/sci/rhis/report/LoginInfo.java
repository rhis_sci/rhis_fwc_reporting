package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.util.CalendarDate;

public class LoginInfo {
	public static String sql;
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String date_type;
	public static String monthSelect;
	public static String yearSelect;
	public static String start_date;
	public static String end_date;
	public static String providertype;
	public static String utype;
	public static String reportViewLevel;

	public static String getResultSetMonitoringTool(JSONObject reportCred){

		String resultString = "";
		DBSchemaInfo table_schema = new DBSchemaInfo();
		try{
			date_type = reportCred.getString("report1_dateType");
			providertype = reportCred.getString("providertype");
			utype = reportCred.getString("utype");

			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else if(date_type.equals("3"))
			{
				yearSelect = reportCred.getString("report1_year");
				start_date = yearSelect + "-01-01";
				end_date = yearSelect + "-12-31";
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}

			String providertype_condition = "";

			if(!providertype.equals("") && !providertype.equals("null"))
				providertype_condition = "and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{providertype},"=");
			else if(utype.equals("998") || utype.equals("994"))
				providertype_condition = "and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"997","14","15","18","19"},"in");
			else if(utype.equals("997"))
				providertype_condition = "and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"14","15","18","19"},"in");
			else
				providertype_condition = "";

			sql = "select "+table_schema.getColumn("","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider Code\", "+table_schema.getColumn("table","providertype_typename")+" as \"Provider Type\", "
					+ ""+table_schema.getColumn("","providerdb_mobileno")+" as \"Mobile No\", "+table_schema.getColumn("table","zilla_zillanameeng")+", "+table_schema.getColumn("table","upazila_upazilanameeng")+" as \"Upazila Name\", "
					+ ""+table_schema.getColumn("table","unions_unionnameeng")+" as \"Assign Area\", "+table_schema.getColumn("","providerdb_facilityname")+" as \"Facility Name\", count(*) as \"Number of Login\" "
					+ "from "+ table_schema.getTable("table_providerdb") +" left join "+ table_schema.getTable("table_unions") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","unions_zillaid")+" and "+table_schema.getColumn("table","providerdb_upazilaid")+"="+table_schema.getColumn("table","unions_upazilaid")+" and "+table_schema.getColumn("table","providerdb_unionid")+"="+table_schema.getColumn("table","unions_unionid")+" "
					+ "left join "+ table_schema.getTable("table_last_login_detail") +" on "+table_schema.getColumn("table","last_login_detail_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" "
					+ "left join "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providertype_provtype")+"="+table_schema.getColumn("table","providerdb_provtype")+" "
					+ "left join "+ table_schema.getTable("table_upazila") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","upazila_zillaid")+" and "+table_schema.getColumn("table","providerdb_upazilaid")+"="+table_schema.getColumn("table","upazila_upazilaid")+" "
					+ "left join "+ table_schema.getTable("table_zilla") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","zilla_zillaid")+" "
					+ "where " + table_schema.getColumn("", "last_login_detail_monitoring_tool",new String[]{"1"},"=") + " and " + table_schema.getColumn("", "last_login_detail_login_success",new String[]{"true"},"=") + " and "+ table_schema.getColumn("table", "last_login_detail_login_time",new String[]{start_date,end_date},"between") +" "+providertype_condition+" "
					+ "group by "+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("","providerdb_provname")+", "+table_schema.getColumn("table","providertype_typename")+", "+table_schema.getColumn("","providerdb_mobileno")+", "+table_schema.getColumn("table","zilla_zillanameeng")+", "+table_schema.getColumn("table","upazila_upazilanameeng")+", "+table_schema.getColumn("table","unions_unionnameeng")+", "+table_schema.getColumn("","providerdb_facilityname")+" ";



			System.out.println(sql);

			resultString = GetResultSet.getFinalResult(sql, 0);

		}
		catch(Exception e){
			System.out.println(e);
		}

		return resultString;
	}

	public static String getResultSetProvider(JSONObject reportCred){
		System.out.println(reportCred);
		zilla = reportCred.getString("report1_zilla");
		upazila = reportCred.getString("report1_upazila");
		union = reportCred.getString("report1_union");
		reportViewLevel = reportCred.getString("reportViewLevel");
		String resultString = "";
		DBSchemaInfo table_schema = null;
//		if(zilla.equals("36") && upazila.equals("71"))
//			table_schema = new DBSchemaInfo();
//		else
			table_schema = new DBSchemaInfo(zilla);

		try{
			providertype = reportCred.getString("providertype");

			String providertype_condition = "";
			String geo_condition = "";

			if (reportViewLevel.equals("2"))
				geo_condition = "";
			else if(reportViewLevel.equals("3"))
				geo_condition = " and "+table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=")+" and "+table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=");
			else if(reportViewLevel.equals("4"))
				geo_condition = " and "+table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=")+" and "+table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=")+" and "+table_schema.getColumn("table", "providerdb_unionid",new String[]{union},"=");

			if(!providertype.equals("") && !providertype.equals("null") && (providertype.equals("2") || providertype.equals("3")))
				providertype_condition = "and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{providertype},"=")+" and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +"";
			else if(!providertype.equals("") && !providertype.equals("null"))
				providertype_condition = "and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{providertype},"=");
			else
				providertype_condition = "and ("+ table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") +" or ("+ table_schema.getColumn("table", "providerdb_provtype",new String[]{"2","3"},"in") +" and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +"))";

			sql = "select "+table_schema.getColumn("","providerdb_provname")+" as \"Provider Name\", (case when "+ table_schema.getColumn("table", "providertype_provtype",new String[]{"2","3"},"in") +" and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +" then "+table_schema.getColumn("table","providertype_typename")+" || ' (CSBA)' else "+table_schema.getColumn("table","providertype_typename")+" end) as \"Provider Type\", "+table_schema.getColumn("","providerdb_mobileno")+" as \"Mobile No\", "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider Code\", \"Last Login Attempt\", "
					+ table_schema.getColumn("", "last_login_detail_login_request_detail") + "::json->'version' as \"Used Version\", " + table_schema.getColumn("", "node_details_version") + " as \"Assigned Version\", "
					+ "(case when "+ table_schema.getColumn("", "last_login_detail_login_success",new String[]{"false"},"=") +" then 'False' else (case when "+ table_schema.getColumn("", "last_login_detail_login_success",new String[]{"true"},"=") +" then 'True' else 'NEVER' end) end) as \"Login Status\", "
					+ table_schema.getColumn("table","zilla_zillanameeng")+" as \"Zilla Name\", "+table_schema.getColumn("table","upazila_upazilanameeng")+" as \"Upazila Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Assign Area\", "+table_schema.getColumn("","providerdb_facilityname")+" as \"Facility Name\" "
					+ "from "+ table_schema.getTable("table_providerdb") +" join "+ table_schema.getTable("table_unions") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","unions_zillaid")+" and "+table_schema.getColumn("table","providerdb_upazilaid")+"="+table_schema.getColumn("table","unions_upazilaid")+" and "+table_schema.getColumn("table","providerdb_unionid")+"="+table_schema.getColumn("table","unions_unionid")+" "
					+ "left join " + table_schema.getTable("table_node_details") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "node_details_provcode") + " "
					+ "left join (select "+table_schema.getColumn("","last_login_detail_provider_id")+", max("+ table_schema.getColumn("", "last_login_detail_login_time") +") as \"Last Login Attempt\"  from "+ table_schema.getTable("table_last_login_detail") +" where login_request_detail is not null group by "+table_schema.getColumn("","last_login_detail_provider_id")+") login_info on login_info."+table_schema.getColumn("","last_login_detail_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" "
					+ "left join "+ table_schema.getTable("table_last_login_detail") +" on login_info."+table_schema.getColumn("","last_login_detail_provider_id")+"="+table_schema.getColumn("table","last_login_detail_provider_id")+" and login_info.\"Last Login Attempt\"="+ table_schema.getColumn("table", "last_login_detail_login_time") +" "
					+ "left join "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providertype_provtype")+"="+table_schema.getColumn("table","providerdb_provtype")+" "
					+ "left join "+ table_schema.getTable("table_upazila") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","upazila_zillaid")+" and "+table_schema.getColumn("table","providerdb_upazilaid")+"="+table_schema.getColumn("table","upazila_upazilaid")+" "
					+ "left join "+ table_schema.getTable("table_zilla") +" on "+table_schema.getColumn("table","providerdb_zillaid")+"="+table_schema.getColumn("table","zilla_zillaid")+" "
					+ "where "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+" and "+table_schema.getColumn("table", "providerdb_active",new String[]{"1"},"=")+" "+geo_condition+" "+providertype_condition+" "
					+ "group by "+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("","providerdb_provname")+", "+table_schema.getColumn("table","providertype_provtype")+", "+ table_schema.getColumn("table", "providerdb_csba") +", "+table_schema.getColumn("table","providertype_typename")+", "+table_schema.getColumn("","providerdb_mobileno")+", "+table_schema.getColumn("table","zilla_zillanameeng")+", "+table_schema.getColumn("table","upazila_upazilanameeng")+", "+table_schema.getColumn("table","unions_unionnameeng")+", "+table_schema.getColumn("","providerdb_facilityname")+", \"Last Login Attempt\", "+ table_schema.getColumn("", "last_login_detail_login_success") +" "
					+ ","+table_schema.getColumn("", "last_login_detail_login_request_detail")+", "+table_schema.getColumn("", "node_details_version")+" "
					+ "order by \"Last Login Attempt\" asc";



			System.out.println(sql);

//			if(zilla.equals("36") && upazila.equals("71"))
//				resultString = GetResultSet.getFinalResultNew(sql, Integer.parseInt(zilla));
//			else
				resultString = GetResultSet.getFinalResult(sql, Integer.parseInt(zilla));
		}
		catch(Exception e){
			System.out.println(e);
		}

		return resultString;
	}
}
