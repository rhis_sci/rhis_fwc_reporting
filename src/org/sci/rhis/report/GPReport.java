package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

//import java.time.YearMonth;

public class GPReport {

    public static String zilla;
    public static String upazila;
    public static String union;
    public static String dateType;
    public static String yearSelect;
    public static String monthSelect;
    public static String startDate;
    public static String endDate;
    public static String resultSet;

    public static String getResultSet(JSONObject reportCred) {
        String tableHtml = "";
        try {
            zilla = reportCred.getString("report1_zilla");
            upazila = reportCred.getString("report1_upazila");
            union = reportCred.getString("report1_union");
            dateType = reportCred.getString("report1_dateType");
            String facility_id = reportCred.getString("facilityid");
            startDate = LocalDate.now().withDayOfMonth(1).toString();
            endDate = LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).toString();

            try {
                if (dateType.equals("1")) {
                    monthSelect = reportCred.getString("report1_month");
                    String[] date = monthSelect.split("-");
                    int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                    startDate = monthSelect + "-01";
                    endDate = monthSelect + "-" + daysInMonth;
                } else if (dateType.equals("3")) {
                    yearSelect = reportCred.getString("report1_year");
                    startDate = yearSelect + "-01-01";
                    endDate = yearSelect + "-12-31";
                } else {
                    startDate = reportCred.getString("report1_start_date");
                    endDate = reportCred.getString("report1_end_date");
                    if (startDate.equals("")) {
                        LocalDate date = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
                        startDate = date.withDayOfMonth(1).toString();
                    }
                }

                DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
                FacilityDB dbFacility2nd = new FacilityDB();
                DBInfoHandler dbObject2nd = dbFacility2nd.facilityDBInfo(Integer.parseInt(zilla));
                DBOperation dbOp2nd = new DBOperation();
                dbObject2nd = dbOp2nd.dbCreateStatement(dbObject2nd);
                dbObject2nd = dbOp2nd.dbExecute(dbObject2nd, "SELECT " + table_schema.getColumn("", "facility_provider_provider_id") + " from facility_provider where facility_id = " + facility_id);
                ResultSet provider_obj = dbObject2nd.getResultSet();
                String providerList = "";
                while (provider_obj.next()) {
                    String id = provider_obj.getString("providerid");
                    providerList += id + ",";
                }

                dbOp2nd.closeResultSet(dbObject2nd);
                dbOp2nd.closePreparedStatement(dbObject2nd);
                dbOp2nd.closeConn(dbObject2nd);
                dbObject2nd = null;

                providerList = providerList.substring(0, providerList.length() - 1);
                String sql = "SELECT gp.*, cm.* , vil." + table_schema.getColumn("", "village_villagename") + " as village_name, " +
                        "cm." + table_schema.getColumn("", "clientmap_generatedid") + "as healthid, " +
                        "concat(date_part('year',age(gp." + table_schema.getColumn("", "gpservice_visitdate") + ", cm.dob)),'বছর  ', date_part('month',age(gp." + table_schema.getColumn("", "gpservice_visitdate") + ", cm.dob)),'মাস  ', date_part('day',age(gp." + table_schema.getColumn("", "gpservice_visitdate") + ", cm.dob)),'দিন') as client_age," +
                        "CASE gp." + table_schema.getColumn("", "gpservice_anemia") + "::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END as gp_anemia, " +
                        "CASE gp." + table_schema.getColumn("", "gpservice_lungs") + "::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়'  else '' END as gp_lungs, " +
                        "CASE gp." + table_schema.getColumn("", "gpservice_liver") + "::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়'  else '' END as gp_liver, " +
                        "CASE gp." + table_schema.getColumn("", "gpservice_splin") + "::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়'  else '' END as gp_splin, " +
                        "CASE gp." + table_schema.getColumn("", "gpservice_tonsil") + "::integer when 1 then 'স্বাভাবিক' when 2 then 'স্বাভাবিক নয়'  else '' END as gp_tonsil, " +
                        " uni." + table_schema.getColumn("", "unions_unionname") + " as union_name, upa." + table_schema.getColumn("", "upazila_upazilaname") + " as upazila_name, zil." + table_schema.getColumn("", "zilla_zillaname") + " as zilla_name, " +
                        "CASE gp." + table_schema.getColumn("", "gpservice_edema") + "::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as gp_edema, " +
                        "CASE gp." + table_schema.getColumn("", "gpservice_jaundice") + "::integer when 1 then 'নাই' when 2 then '+(অল্প)' when 3 then '++ (মোটামুটি)' when 4 then '+++(অধিক)' else '' END as gp_jaundice, " +
                        "CASE gp." + table_schema.getColumn("", "gpservice_anemia") + "::integer when 1 then '+' when 2 then '++' when 3 then '+++' else '' END  as gp_anemia, " +
                        "to_char(gp." + table_schema.getColumn("", "gpservice_visitdate") + ",'DD MM YYYY') as visit_date, " +
                        "CASE WHEN gp." + table_schema.getColumn("", "gpservice_bpsystolic") + " IS NULL THEN '' ELSE CONCAT(gp." + table_schema.getColumn("", "gpservice_bpsystolic") + ",'/',gp." + table_schema.getColumn("", "gpservice_bpdiastolic") + ") END as bp " +
                        " FROM " + table_schema.getTable("table_gpservice") + " as gp " +
                        "INNER JOIN " + table_schema.getTable("table_clientmap") + " as cm ON cm." + table_schema.getColumn("", "clientmap_generatedid") + " = gp." + table_schema.getColumn("", "gpservice_healthid") + " " +
                        "LEFT JOIN " + table_schema.getTable("table_village") + " as vil ON cm." + table_schema.getColumn("", "clientmap_villageid") + " = vil." + table_schema.getColumn("", "village_villageid") + " and cm." + table_schema.getColumn("", "clientmap_mouzaid") + " = vil." + table_schema.getColumn("", "village_mouzaid") + " and cm." + table_schema.getColumn("", "clientmap_upazilaid") + " = vil." + table_schema.getColumn("", "village_upazilaid") + " and cm." + table_schema.getColumn("", "clientmap_unionid") + " = vil." + table_schema.getColumn("", "village_unionid") + "  and cm." + table_schema.getColumn("", "clientmap_zillaid") + " = vil." + table_schema.getColumn("", "village_zillaid") + " " +
                        "LEFT JOIN " + table_schema.getTable("table_unions") + " as uni ON cm." + table_schema.getColumn("", "clientmap_upazilaid") + " = uni." + table_schema.getColumn("", "unions_upazilaid") + " and cm." + table_schema.getColumn("", "clientmap_unionid") + " = uni." + table_schema.getColumn("", "unions_unionid") + "  and cm." + table_schema.getColumn("", "clientmap_zillaid") + " = uni." + table_schema.getColumn("", "unions_zillaid") + " " +
                        "LEFT JOIN " + table_schema.getTable("table_upazila") + " as upa ON cm." + table_schema.getColumn("", "clientmap_upazilaid") + " = upa." + table_schema.getColumn("", "upazila_upazilaid") + " and cm." + table_schema.getColumn("", "clientmap_zillaid") + " = upa." + table_schema.getColumn("", "upazila_zillaid") + " " +
                        "LEFT JOIN " + table_schema.getTable("table_zilla") + " as zil ON cm." + table_schema.getColumn("", "clientmap_zillaid") + " = zil." + table_schema.getColumn("", "zilla_zillaid") + " " +
                        "WHERE  gp." + table_schema.getColumn("", "gpservice_providerid") + " IN (" + providerList + ") and gp." + table_schema.getColumn("", "gpservice_visitdate") + " BETWEEN '" + startDate + "' AND '" + endDate + "' order by gp." + table_schema.getColumn("", "gpservice_visitdate") + " asc";
                System.out.println(sql);
                FacilityDB dbFacility = new FacilityDB();
                DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

                DBOperation dbOp = new DBOperation();
                dbObject = dbOp.dbCreateStatement(dbObject);

                LinkedHashMap<String, String> symptomList = getSymptomList(dbObject, dbOp);
                LinkedHashMap<String, String> treatmentList = getTreatmentList(dbObject, dbOp);

                dbObject = dbOp.dbExecute(dbObject, sql);
                ResultSet rs = dbObject.getResultSet();
                //*******************************************//

                List<JSONObject> jsonObj = new ArrayList<JSONObject>();
                ResultSetMetaData meta = rs.getMetaData();

                while (rs.next()) {
                    String symptom_sql = null; //treatment
                    HashMap<String, String> data = new HashMap<String, String>();
                    for (int i = 1; i <= meta.getColumnCount(); i++) {
                        String key = meta.getColumnName(i);

                        String value = rs.getString(key);
                        if (value == null) {
                            value = "";
                        }

                        //process to fetch the symptomlist and treatmentlist table data
                        if (key.equals("symptom") || key.equals("treatment")){
                            if (rs.getString(key) != null) {
                                value = findDetail(key.equals("symptom") ? symptomList : treatmentList, key, rs);
                            }
                        }

                        data.put(key, value);
                    }
                    jsonObj.add(new JSONObject(data));
                }

                dbOp.closeResultSet(dbObject);
                dbOp.closePreparedStatement(dbObject);
                dbOp.closeConn(dbObject);
                dbObject = null;

                return jsonObj.toString();

                //*******************************************//
            } catch (Exception e) {
                System.out.println(e);
            }

//			System.out.println(startDate+"   "+endDate);
        } catch (Exception e) {
            System.out.println(e);
            resultSet = "Bad Url Request";
        }
        return resultSet;
    }

    private static String findDetail(LinkedHashMap<String, String> symptomTreatmentList, String key, ResultSet rs) throws SQLException {
        String details = "";

        try {
            String[] DBlist = rs.getString(key).replaceAll("[\\]\\[\"]", "").split(",");
            for (String symptom : DBlist) {
                details += symptomTreatmentList.get(symptom) + ",";
            }
            details = details.replaceAll(",*$", "");
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return details;
    }

    private static LinkedHashMap<String, String> getSymptomList(DBInfoHandler dbObject, DBOperation dbOp){
        LinkedHashMap<String, String> symptomList = new LinkedHashMap<String, String>();
        try {
            String symptom_sql = "select * from symptomlist";
            dbObject = dbOp.dbExecute(dbObject, symptom_sql);
            ResultSet rs = dbObject.getResultSet();
            while (rs.next()){
                symptomList.put(rs.getString("id"), rs.getString("detail"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return symptomList;
    }

    private static LinkedHashMap<String, String> getTreatmentList(DBInfoHandler dbObject, DBOperation dbOp){
        LinkedHashMap<String, String> treatmentList = new LinkedHashMap<String, String>();
        try {
            String treatment_sql = "select * from treatmentlist";
            dbObject = dbOp.dbExecute(dbObject, treatment_sql);
            ResultSet rs = dbObject.getResultSet();
            while (rs.next()){
                treatmentList.put(rs.getString("id"), rs.getString("detail"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return treatmentList;
    }
}
