package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import java.time.YearMonth;

public class                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    IUDRegister {
	
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String dateType;
    public static String yearSelect;
    public static String monthSelect;
	public static String startDate;
	public static String endDate;
    public static String resultSet;

	public static String getResultSet(JSONObject reportCred){
        String tableHtml = "";
        try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			union = reportCred.getString("report1_union");
			dateType = reportCred.getString("report1_dateType");
            String facility_id =  reportCred.getString("facilityid");
            startDate = LocalDate.now().withDayOfMonth(1).toString();
            endDate =LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).toString();

            try {
                if(dateType.equals("1")) {
                    monthSelect = reportCred.getString("report1_month");
                    String[] date = monthSelect.split("-");
                    int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                    startDate = monthSelect + "-01";
                    endDate = monthSelect + "-" + daysInMonth;
                }
                else if(dateType.equals("3"))
                {
                    yearSelect = reportCred.getString("report1_year");
                    startDate = yearSelect + "-01-01";
                    endDate = yearSelect + "-12-31";
                }else {
                    startDate = reportCred.getString("report1_start_date");
                    endDate = reportCred.getString("report1_end_date");
                    if(startDate.equals("")){
                        LocalDate date= LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
                        startDate = date.withDayOfMonth(1).toString();
                    }
                }
                DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
                FacilityDB dbFacility2nd = new FacilityDB();
                DBInfoHandler dbObject2nd = dbFacility2nd.facilityDBInfo(Integer.parseInt(zilla));
                DBOperation dbOp2nd = new DBOperation();
                dbObject2nd = dbOp2nd.dbCreateStatement(dbObject2nd);
                dbObject2nd = dbOp2nd.dbExecute(dbObject2nd, "SELECT "+table_schema.getColumn("","facility_provider_provider_id")+" from facility_provider where facility_id = "+facility_id);
                ResultSet provider_obj = dbObject2nd.getResultSet();
                String providerList = "";
                while (provider_obj.next()) {
                    String id = provider_obj.getString("providerid");
                    providerList += id +",";
                }
                providerList = providerList.substring(0, providerList.length() - 1);
                String sql = "SELECT cm.*,vil.*,uni.*,upa.*,zil.*,elc.*, ius.*, iufs.*," +
                        "to_char(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+",'DD MM YYYY') as iud_date, "+
                        "to_char(iufs."+table_schema.getColumn("", "iudfollowupservice_followupdate")+",'DD MM YYYY') as followup_date, "+
                        "to_char(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+"+ interval '1 month' ,'DD MM YYYY') as first_fixed_date,"+
                        "to_char(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+"+ interval '6 month' ,'DD MM YYYY') as second_fixed_date,"+
                        "to_char(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+"+ interval '1 year' ,'DD MM YYYY') as third_fixed_date,"+
                        "CASE  ius."+table_schema.getColumn("", "iudservice_clientallowance")+"::integer WHEN 2 THEN 'দেওয়া হয়নি' when 1 then 'দেয়া হয়েছে' ELSE '' END as client_allowance, " +
                        "CASE  ius."+table_schema.getColumn("", "iudservice_iudafterdelivery")+"::integer WHEN 1 THEN concat(date_part('month',age(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+", cm.dob)),'  মাস')  ELSE '' END as iud_after_deli, " +
                        "CASE WHEN age(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+", cm.dob) IS NULL THEN '' ELSE concat(date_part('year',age(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+", cm.dob)),'বছর  ', date_part('month',age(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+", cm.dob)),'মাস  ', date_part('day',age(ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+", cm.dob)),'দিন') END as client_age, " +
                        "iufs."+table_schema.getColumn("", "iudfollowupservice_iudremovereason")+" as iud_remover_reason,"+
                        "to_char(iufs."+table_schema.getColumn("", "iudfollowupservice_iudremovedate")+",'DD MM YYYY') as iud_remover_date,"+
                        "CASE  iufs."+table_schema.getColumn("", "iudfollowupservice_fpmethod")+"::integer WHEN 1 THEN 'খাবার বড়ি (সুখী)' when 2 then 'কনডম' when 3 then 'ইনজেকটেবলস' when 8 then 'ইসিপি' when 10 then 'খাবার বড়ি (আপন)' else '' end as method_name,"+
                        "iufs."+table_schema.getColumn("", "iudfollowupservice_iudremoverdesignation")+" as iud_remover_deg "+

                        " FROM "+ table_schema.getTable("table_iudservice")+" as ius " +
                        " LEFT JOIN "+ table_schema.getTable("table_iudfollowupservice")+" as iufs ON ius."+table_schema.getColumn("", "iudservice_healthid")+" = iufs."+table_schema.getColumn("", "iudfollowupservice_healthid")+"" +

                        " LEFT JOIN "+ table_schema.getTable("table_clientmap")+" as cm ON cm."+table_schema.getColumn("", "clientmap_generatedid")+" = ius."+table_schema.getColumn("", "iudservice_healthid")+"" +
                        " LEFT JOIN "+ table_schema.getTable("table_elco")+" as elc ON elc."+table_schema.getColumn("", "elco_healthid")+" = ius."+table_schema.getColumn("", "iudservice_healthid")+"" +
                        " LEFT JOIN "+ table_schema.getTable("table_village")+" as vil ON cm."+table_schema.getColumn("","clientmap_villageid")+" = vil."+table_schema.getColumn("","village_villageid")+" and cm."+table_schema.getColumn("","clientmap_mouzaid")+" = vil."+table_schema.getColumn("","village_mouzaid")+" and cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = vil."+table_schema.getColumn("","village_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_unionid")+" = vil."+table_schema.getColumn("","village_unionid")+"  and cm."+table_schema.getColumn("","clientmap_zillaid")+" = vil."+table_schema.getColumn("","village_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_unions")+" as uni ON cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = uni."+table_schema.getColumn("","unions_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_unionid")+" = uni."+table_schema.getColumn("","unions_unionid")+"  and cm."+table_schema.getColumn("","clientmap_zillaid") +" = uni."+table_schema.getColumn("","unions_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_upazila")+" as upa ON cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = upa."+table_schema.getColumn("","upazila_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_zillaid") +" = upa."+table_schema.getColumn("","upazila_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_zilla")+" as zil ON cm."+table_schema.getColumn("","clientmap_zillaid") +" = zil."+table_schema.getColumn("","zilla_zillaid")+" " +

                        "WHERE ius."+table_schema.getColumn("", "iudservice_providerid")+" IN ("+providerList+") and   ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+" BETWEEN '"+startDate+"' AND '"+endDate+"' or iufs."+ table_schema.getColumn("", "iudfollowupservice_followupdate")+" BETWEEN '"+startDate+"' AND '"+endDate+"'order by ius."+table_schema.getColumn("", "iudservice_iudimplantdate")+"  asc";
                System.out.println(sql);
                FacilityDB dbFacility = new FacilityDB();
                DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla,upazila);

                DBOperation dbOp = new DBOperation();
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject, sql);
                ResultSet rs = dbObject.getResultSet();
                //*******************************************//

                List<JSONObject> jsonObj = new ArrayList<JSONObject>();
                ResultSetMetaData meta = rs.getMetaData();
                while (rs.next()) {
                    HashMap<String, String> data = new HashMap<String, String>();
                    for (int i = 1; i <= meta.getColumnCount(); i++) {
                        String key = meta.getColumnName(i);
                        String value = rs.getString(key);
                        if(value == null){
                            value = "";
                        }
                        data.put(key, value);
                    }
                    jsonObj.add(new JSONObject(data));
                }
                return jsonObj.toString();

                //*******************************************//
            }catch (Exception e){
                System.out.println(e);
            }

//			System.out.println(startDate+"   "+endDate);
		}
		catch(Exception e){
			System.out.println(e);
            resultSet = "Bad Url Request";
		}
		return resultSet;
	}

}
