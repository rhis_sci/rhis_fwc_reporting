package org.sci.rhis.report;

//import java.time.YearMonth;

import org.json.JSONObject;
import org.sci.rhis.util.CalendarDate;

public class AggregateMNC {
	
	public static String zilla;
	public static String upazila;
	public static String resultString;
	public static String sql;
	public static String date_type;
	public static String monthSelect;
	public static String yearSelect;
	public static String start_date;
	public static String end_date;
	public static String db_name;
	
	public static String getResultSet(JSONObject reportCred){
		resultString = "";
		try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			date_type = reportCred.getString("report1_dateType");
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else if(date_type.equals("3"))
			{
				yearSelect = reportCred.getString("report1_year");
				start_date = yearSelect + "-01-01";
				end_date = yearSelect + "-12-31";
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
			sql = "select anc.\"Year\", anc.\"month\" as \"Month\", (CASE WHEN \"ANC\" IS NULL THEN 0 ELSE \"ANC\" END),(CASE WHEN \"PNC\" IS NULL THEN 0 ELSE \"PNC\" END), (CASE WHEN \"DELIVERY\" IS NULL THEN 0 ELSE \"DELIVERY\" END) "
					+ " from( "
					+ " select date_part('year', \"visitDate\") as \"Year\" "
					+ " ,date_part('month', \"visitDate\") as \"Month\" "
					+ " , to_char(\"visitDate\", 'MONTH') as month "
					+ " , count(*) as \"ANC\" "
					+ " from \"ancService\" "
					+ " where \"providerId\" in "
					+ "(select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+" AND (\"ProvType\" = 4 OR \"ProvType\" = 5 OR \"ProvType\" = 101 OR \"ProvType\" = 17)) "
					+ " AND \"visitDate\" >= '"+ start_date +"' AND \"visitDate\" <= '"+ end_date +"' "
					+ " Group by \"Year\" "
					+ " , \"Month\" "
					+ " , month "
					+ " Order by \"Year\" asc "
					+ " , \"Month\" asc "
					+ " ) as anc "
					+ " left join "
					+ " ( "
					+ " select date_part('year', \"visitDate\") as \"Year\" "
					+ " ,date_part('month', \"visitDate\") as \"Month\" "
					+ " , to_char(\"visitDate\", 'MONTH') as month "
					+ " , count(*) as \"PNC\" "
					+ " from \"pncServiceMother\" "
					+ " where \"providerId\" in "
					+ "      (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+" AND (\"ProvType\" = 4 OR \"ProvType\" = 5 OR \"ProvType\" = 101 OR \"ProvType\" = 17)) "
					+ " AND \"visitDate\" >= '"+ start_date +"' AND \"visitDate\" <= '"+ end_date +"' "
					+ " Group by \"Year\" "
					+ " , \"Month\" "
					+ " , month "
					+ " Order by \"Year\" asc "
					+ " , \"Month\" asc "
					+ " ) as pnc "
					+ " using (\"Year\",\"Month\") "
					+ " left join "
					+ " ( "
					+ " select date_part('year', \"outcomeDate\") as \"Year\" "
					+ " ,date_part('month', \"outcomeDate\") as \"Month\" "
					+ " , to_char(\"outcomeDate\", 'MONTH') as month "
					+ " , count(*) as \"DELIVERY\" "
					+ " from \"delivery\" "
					+ " where \"providerId\" in "
					+ "       (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+" AND (\"ProvType\" = 4 OR \"ProvType\" = 5 OR \"ProvType\" = 101 OR \"ProvType\" = 17)) "
					+ "  AND \"deliveryDoneByThisProvider\"=1 AND \"outcomeDate\" >= '"+ start_date +"' AND \"systemEntryDate\" <= '"+ end_date +"' "
					+ " Group by \"Year\" "
					+ " , \"Month\" "
					+ " , month "
					+ " Order by \"Year\" asc "
					+ " , \"Month\" asc "
					+ " ) as delivery "
					+ " using (\"Year\",\"Month\") ";
			
			resultString = GetResultSet.getFinalResult(sql, zilla, upazila);
			//System.out.println(sql);
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return resultString;
	}

}
