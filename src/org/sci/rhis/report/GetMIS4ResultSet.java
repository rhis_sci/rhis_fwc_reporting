package org.sci.rhis.report;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.LinkedHashMap;

import org.sci.rhis.db.*;

public class GetMIS4ResultSet {
	
	
	
	
	public static LinkedHashMap<String, LinkedHashMap<String, String>> getFinalResult(String sql, String zilla, String upazila){
		
		LinkedHashMap<String, LinkedHashMap<String, String>> service = new LinkedHashMap<String, LinkedHashMap<String, String>>();
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
		
		try{
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
			while(rs.next()){
				service.put(rs.getString(metadata.getColumnName(1)), (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
				for (int i = 2; i <= columnCount; i++) {
					service.get(rs.getString(metadata.getColumnName(1))).put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));
			    }
					
			}
			
			//System.out.println((LinkedHashMap<String, LinkedHashMap<String, String>>) service);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return service;
		
	}

}
