package org.sci.rhis.report;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class OtherTaskMIS3 {
    public static void main(String args[]) throws SQLException, ParseException {


//        String  mis3Info = MIS3Report.getResultSet(reportCred);
//        System.out.println("MIS3 Result: "+mis3Info);
        DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(36));

        HashMap<String, String> monthList = new HashMap<String, String>();
        monthList.put("Jan", "01");
        monthList.put("Feb", "02");
        monthList.put("Mar", "03");
        monthList.put("Apr", "04");
        monthList.put("May", "05");
        monthList.put("Jun", "06");
        monthList.put("Jul", "07");
        monthList.put("Aug", "08");
        monthList.put("Sep", "09");
        monthList.put("Oct", "10");
        monthList.put("Nov", "11");
        monthList.put("Dec", "12");

        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject =  new FacilityDB().facilityDBInfo(36);

        String sql = "select zillaid, upazilaid, fr.facilityid, providerid from facility_registry fr join (select distinct(facility_id) as facilityid from facility_provider where is_active=1) fp using(facilityid) join (select providerid, facility_id from providerdb join facility_provider using(providerid) where active=1 and is_active=1 and provtype=4) p on p.facility_id=fr.facilityid where zillaid=36 and upazilaid=71";
        dbObject = dbOp.dbCreateStatement(dbObject);
        dbObject = dbOp.dbExecute(dbObject,sql);
        ResultSet rs = dbObject.getResultSet();

        while(rs.next()){
            Calendar calendar = Calendar.getInstance();
            calendar.set(2018, 9, 1);
            String[] monthInfo = calendar.getTime().toString().split(" ");
            int year = Integer.parseInt(monthInfo[5]);
            int month = Integer.parseInt(monthList.get(monthInfo[1]));
            int year_sd = Integer.parseInt(monthInfo[5]);
            int month_sd = Integer.parseInt(monthList.get(monthInfo[1]));
            String start_date = "";
            String end_date = "";
            for(int i=1;i<=12;i++){
                int daysInMonth = CalendarDate.daysInMonth(year, month);
                start_date = year + "-" + month + "-1";
                end_date = year + "-" + month + "-" + daysInMonth;

                JSONObject reportCred = new JSONObject();
                reportCred.put("facilityId", rs.getString("facilityid"));
                reportCred.put("report1_zilla", rs.getString("zillaid"));
                reportCred.put("report1_upazila", rs.getString("upazilaid"));
                reportCred.put("report1_start_date", start_date);
                reportCred.put("report1_end_date", end_date);
                reportCred.put("providerId", "");
                reportCred.put("csba", "2");
                reportCred.put("mis3ViewType", "");
                reportCred.put("mis3Submit", "0");
                reportCred.put("mis3ViewType", "2");
                reportCred.put("jsonRequest", "0");
                reportCred.put("type", "4");
                reportCred.put("report1_dateType", "2");
                reportCred.put("jsonRequest", "1");

                String sql_check_submission = "select * from mis3_report_submission where facility_id="+rs.getString("facilityid")+" and report_year="+year+" and report_month="+month;
                DBOperation dbOp2 = new DBOperation();
                DBInfoHandler dbObject2 =  new FacilityDB().facilityDBInfo(36);
                dbObject2 = dbOp2.dbExecute(dbObject2,sql_check_submission);
                ResultSet rs2 = dbObject2.getResultSet();
                rs2.last();
                int rowCount = rs2.getRow();

                month_sd++;
                if(month_sd>12)
                    month_sd = 1;
                if(month_sd==1)
                    year_sd++;

                if(rowCount==0) {
                    String sql_submission_insert = "insert into " + table_schema.getTable("table_mis3_report_submission") + " (" + table_schema.getColumn("", "mis3_report_submission_facility_id") + "," + table_schema.getColumn("", "mis3_report_submission_report_year") + "," + table_schema.getColumn("", "mis3_report_submission_report_month") + "," + table_schema.getColumn("", "mis3_report_submission_submitted_by") + "," + table_schema.getColumn("", "mis3_report_submission_submission_date") + "," + table_schema.getColumn("", "mis3_report_submission_submission_status") + ") "
                            + "values(" + rs.getString("facilityid") + "," + year + "," + month + "," + rs.getString("providerid") + ",'" + year_sd + "-" + String.format("%02d", month_sd) + "-01" + "',2)";
//                    System.out.println(sql_submission_insert);
//                    dbOp2.dbStatementExecute(dbObject2,sql_submission_insert);
                }

                String sql_check_submission_data = "select * from mis3_report_submission_data where facility_id="+rs.getString("facilityid")+" and report_year="+year+" and report_month="+month;
                dbObject2 = dbOp2.dbExecute(dbObject2,sql_check_submission_data);
                rs2 = dbObject2.getResultSet();
                rs2.last();
                int rowCount2 = rs2.getRow();

                if(rowCount2==0){
//                    String  mis3Info = MIS3Report.getResultSet(reportCred);
//                    JSONObject entryData = new JSONObject(mis3Info);
//                    System.out.println("MIS3 Result: "+entryData);
//                    JSONArray keys = entryData.names ();
//                    for (int j = 0; j < keys.length (); ++j) {
//                        String sql_insert = "insert into "+ table_schema.getTable("table_mis3_report_submission_data") +" ("+table_schema.getColumn("","mis3_report_submission_data_facility_id")+", "+table_schema.getColumn("","mis3_report_submission_data_report_month")+"," +
//                                ""+table_schema.getColumn("","mis3_report_submission_data_report_year")+","+table_schema.getColumn("","mis3_report_submission_data_field_name")+","+table_schema.getColumn("","mis3_report_submission_data_field_value")+") " +
//                                "values("+rs.getString("facilityid")+", "+month+","+year+",'"+keys.getString (j)+"',"+((entryData.getString(keys.getString (j)).equals(""))?0:entryData.getString(keys.getString (j)))+")";
//
//                        System.out.println(sql_insert);
//
//                        dbOp2.dbStatementExecute(dbObject2, sql_insert);
//                    }
                }

                System.out.println(rs.getString("facilityid")+" "+ year+ " "+month+" "+ rowCount+" "+rowCount2);

                month++;
                if(month>12)
                    month = 1;
                if(month==1)
                    year++;

                dbOp2.closeResultSet(dbObject2);
                dbOp2.closePreparedStatement(dbObject2);
                dbOp2.closeConn(dbObject2);
                dbObject2 = null;
            }
        }
        dbOp.closeResultSet(dbObject);
        dbOp.closePreparedStatement(dbObject);
        dbOp.closeConn(dbObject);
        dbObject = null;
    }
}
