package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class PregnantWomenList {

    public static String getList(JSONObject reportCred) {
        String resultString = "";
        String zilla = reportCred.getString("report1_zilla");
        int fromAge = Integer.parseInt(reportCred.getString("fromAge"));
        int toAge = Integer.parseInt(reportCred.getString("toAge"));

        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
        try {
            String upazila = reportCred.getString("report1_upazila");
            String union = reportCred.getString("report1_union");

            String geo_condition = "";
            if (union.isEmpty())
                geo_condition = table_schema.getColumn("cm", "clientmap_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("cm", "clientmap_upazilaid", new String[]{upazila}, "=");
            else
                geo_condition = table_schema.getColumn("cm", "clientmap_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("cm", "clientmap_upazilaid", new String[]{upazila}, "=") + " and " + table_schema.getColumn("cm", "clientmap_unionid", new String[]{union}, "=");

            String sql = "SELECT (CASE WHEN " + table_schema.getColumn("cm", "clientmap_healthid") + "!=" + table_schema.getColumn("cm", "clientmap_generatedid") + " THEN " + table_schema.getColumn("cm", "clientmap_healthid") + " else NULL end) AS \"Health ID\"," +
                    "(CASE WHEN " + table_schema.getColumn("cm", "clientmap_healthid") + "=" + table_schema.getColumn("cm", "clientmap_generatedid") + " THEN " + table_schema.getColumn("cm", "clientmap_generatedid") + " else NULL end) AS \"NRC ID\"," +
                    table_schema.getColumn("z", "zilla_zillanameeng") + " as \"Zilla Name\"," + table_schema.getColumn("up", "upazila_upazilanameeng") + " as \"Upazila Name\"," + table_schema.getColumn("un", "unions_unionnameeng") + " as \"Union Name\"," +
                    table_schema.getColumn("v", "village_villagenameeng") + " as \"Village Name\"," + table_schema.getColumn("cm", "clientmap_name") + " as \"Client Name\"," + table_schema.getColumn("cm", "clientmap_husbandname") + " as \"Client Husband Name\"," + table_schema.getColumn("cm", "clientmap_mobileno") + " as \"Client Mobile No.\"," +
                    "age(" + table_schema.getColumn("cm", "clientmap_dob") + ") as \"Client Age\"," + table_schema.getColumn("pw", "pregwomen_lmp") + " as \"LMP\"," +
                    table_schema.getColumn("pw", "pregwomen_edd") + " as \"EDD\"," + table_schema.getColumn("pw", "pregwomen_para") + " as \"Para\"," + table_schema.getColumn("pw", "pregwomen_gravida") + " as \"Gravida\"" +
                    ",(select CASE WHEN count(*)>0 THEN count(*) else null end from " + table_schema.getTable("table_ancservice") + " as a    where " + table_schema.getColumn("a", "ancservice_healthid") + " = " + table_schema.getColumn("cm", "clientmap_healthid") + ") as \"#ANC Visits\"  " +
                    "FROM " + table_schema.getTable("table_pregwomen") + " AS pw " +
                    "LEFT JOIN " + table_schema.getTable("table_delivery") + " AS d using(" + table_schema.getColumn("", "delivery_healthid") + "," + table_schema.getColumn("", "delivery_pregno") + ") " +
                    "LEFT JOIN " + table_schema.getTable("table_elco") + " AS e using(" + table_schema.getColumn("", "elco_healthid") + ") " +
                    "INNER JOIN " + table_schema.getTable("table_clientmap") + " AS cm ON " + table_schema.getColumn("pw", "pregwomen_healthid") + " = " + table_schema.getColumn("cm", "clientmap_generatedid") + " AND " +
                    table_schema.getColumn("cm", "clientmap_healthid", new String[]{}, "isnotnull") + " AND " + geo_condition + " " +
                    "LEFT JOIN " + table_schema.getTable("table_member") + " AS m ON " + table_schema.getColumn("m", "member_healthid") + "=" + table_schema.getColumn("cm", "clientmap_healthid") + " AND (" + table_schema.getColumn("m", "member_extype", new String[]{"0"}, "=") + " OR " + table_schema.getColumn("m", "member_extype", new String[]{}, "isnull") + ") AND " + table_schema.getColumn("m", "member_healthid", new String[]{}, "isnotnull") + " " +
                    "INNER JOIN " + table_schema.getTable("table_unions") + " as un ON cm." + table_schema.getColumn("", "clientmap_upazilaid") + " = un." + table_schema.getColumn("", "unions_upazilaid") + " and cm." + table_schema.getColumn("", "clientmap_unionid") + " = un." + table_schema.getColumn("", "unions_unionid") + "  and cm." + table_schema.getColumn("", "clientmap_zillaid") + " = un." + table_schema.getColumn("", "unions_zillaid") + " " +
                    "INNER JOIN " + table_schema.getTable("table_upazila") + " as up on " + table_schema.getColumn("cm", "clientmap_zillaid") + "=" + table_schema.getColumn("up", "upazila_zillaid") + " and " + table_schema.getColumn("cm", "clientmap_upazilaid") + "=" + table_schema.getColumn("up", "upazila_upazilaid") + " " +
                    "INNER JOIN " + table_schema.getTable("table_zilla") + " as z on " + table_schema.getColumn("cm", "clientmap_zillaid") + "=" + table_schema.getColumn("z", "zilla_zillaid") + " " +
                    "INNER JOIN " + table_schema.getTable("table_village") + " as v ON cm." + table_schema.getColumn("", "clientmap_villageid") + " = v." + table_schema.getColumn("", "village_villageid") + " and cm." + table_schema.getColumn("", "clientmap_mouzaid") + " = v." + table_schema.getColumn("", "village_mouzaid") + " and cm." + table_schema.getColumn("", "clientmap_upazilaid") + " = v." + table_schema.getColumn("", "village_upazilaid") + " and cm." + table_schema.getColumn("", "clientmap_unionid") + " = v." + table_schema.getColumn("", "village_unionid") + "  and cm." + table_schema.getColumn("", "clientmap_zillaid") + " = v." + table_schema.getColumn("", "village_zillaid") + " " +
                    "WHERE CURRENT_DATE BETWEEN (" + table_schema.getColumn("pw", "pregwomen_lmp") + ") AND (" + table_schema.getColumn("pw", "pregwomen_edd") + " + INTERVAL '14 days') AND " + table_schema.getColumn("pw", "pregwomen_pregno") + " IN " +
                    "(SELECT MAX(" + table_schema.getColumn("", "pregwomen_pregno") + ") FROM " + table_schema.getTable("table_pregwomen") + " WHERE " + table_schema.getColumn("", "pregwomen_healthid") + " = " + table_schema.getColumn("pw", "pregwomen_healthid") + ") " +
                    "AND " + table_schema.getColumn("d", "delivery_pregno", new String[]{}, "isnull");

            if(fromAge !=0 && toAge !=0){
                sql+= " and  cm.age between "+fromAge+" and "+toAge;
            }


            System.out.println(sql);

            resultString = GetResultSet.getFinalResult(sql, zilla, upazila);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }

    public static List<JSONObject> getPregwomenServiceDetails(JSONObject reportCred) {
        List<JSONObject> resList = new ArrayList<JSONObject>();
        HashMap<String, String> detailsMap = new HashMap<String, String>();
        //column for ancservice
        String[] ancservice = new String[]{"urinesugar", "edema", "urinealbumin","refercentername"};//,"refercentername"

        BigInteger healthid;
        String zilla = reportCred.getString("zilla");
        String upazila = reportCred.getString("upazilaid");
        if (reportCred.getString("healthid").equals(""))
            healthid = new BigInteger(reportCred.getString("nrcid"));
        else
            healthid = new BigInteger(reportCred.getString("healthid"));


        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        try {
            String sql = "select healthid,visitdate,bpsystolic,bpdiastolic,weight,edema,urinesugar,urinealbumin,uterusheight,fetusheartrate,refer from ancservice " + //refercentername
                    " where healthid = " + healthid;
//                    "and visitdate between \'"+lmp+"\' and \'"+edd+"\'";

            /* String sql = "select healthid,visitdate,bpsystolic,bpdiastolic,weight,edema,urinesugar,urinealbumin,uterusheight,fetusheartrate,CASE\n" +
                    "WHEN refercentername='1' THEN 'মেডিকেল কলেজ হাসপাতাল'\n" +
                    "WHEN refercentername='2' THEN 'জেলা সদর হাসপাতাল'\n" +
                    "WHEN refercentername='3' THEN 'মা ও শিশু কল্যাণ কেন্দ্র'\n" +
                    "WHEN refercentername='4' THEN 'উপজেলা স্বাস্থ্য কমপ্লেক্স'\n" +
                    "WHEN refercentername='5' THEN 'ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র'\n" +
                    "WHEN refercentername='6' THEN 'কমিউনিটি ক্লিনিক'\n" +
                    "WHEN refercentername='7' THEN 'এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র'\n" +
                    "WHEN refercentername='8' THEN 'প্রাইভেট ক্লিনিক / হাসপাতাল'\n" +
                    "WHEN refercentername='9' THEN 'অন্যান্য'\n" +
                    "ELSE 'নাই'\n" +
                    "END AS refer from ancservice " +
                    " where healthid = " + healthid;*/

            System.out.println(sql);

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            // get column names
            ResultSetMetaData rsMeta = rs.getMetaData();
            int columnCnt = rsMeta.getColumnCount();
            List<String> columnNames = new ArrayList<String>();
            for (int i = 1; i <= columnCnt; i++) {
                columnNames.add(rsMeta.getColumnName(i));
            }

            if (rs.last()) {
                detailsMap = getMasterDatadropdownList("ancservice", ancservice);
            }
            rs.beforeFirst();

            while (rs.next()) { // convert each object to an human readable JSON object
                JSONObject obj = new JSONObject();
                for (int i = 1; i <= columnCnt; i++) {
                    String key = columnNames.get(i - 1);
                    String value = rs.getString(i);
                    if (value == null)
                        value = "0";
                    obj.put(key, value);

                }
                //put details in reference of code
                for (String service : ancservice) {
                    if (obj.has(service)) {
                        obj.put(service, detailsMap.get(service + ":" + obj.getString(service)));
                    }
                }
                resList.add(obj);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBOperation.closeDBInfoHandler(dbOp, dbObject);
//            dbOp.closeResultSet(dbObject);
//            dbOp.closePreparedStatement(dbObject);
//            dbOp.closeConn(dbObject);
//            dbObject = null;
        }
        return resList;

    }

    /**
     * @param tablename
     * @param fieldNameList
     * @return detail map of ancservice for specific code(ie: 1,2..)
     */
    public static HashMap<String, String> getMasterDatadropdownList(String tablename, String[] fieldNameList) {
        HashMap<String, String> maplist = new HashMap<String, String>();
        String sql = "select field_name,display_value,code from master_data_dropdowns where table_name like('%" + tablename + "%')" + " and (";
        String condition = "";
        for (String fieldName : fieldNameList) {
            condition = condition + "field_name like('%" + fieldName + "%') or ";
        }
        condition = condition.replaceAll("(?m)or\\s*$", ")");
        sql = sql + condition;

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        dbObject = dbOp.dbCreateStatement(dbObject);
        dbObject = dbOp.dbExecute(dbObject, sql);
        ResultSet rs = dbObject.getResultSet();
        List<String> ancserviceList = Arrays.asList(fieldNameList);
        try {
            while (rs.next()) {
                String[] fieldnameList = rs.getString(1).split(",");
                for (String field : fieldnameList) {
                    if (ancserviceList.contains(field)) {
                        maplist.put(field + ":" + rs.getString(3), rs.getString(2));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBOperation.closeDBInfoHandler(dbOp, dbObject);

        }
        return maplist;

    }
}


