package org.sci.rhis.report;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.json.JSONObject;
import org.sci.rhis.db.ConfInfoRetrieve;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class CreateNewDatabase {
	
	public static Integer zilla;
	public static String zillaname;
	public static Integer upazila;
	public static String dbname;
	public static String dburl;
	public static String dbusername;
	public static String dbuserpass;
	public static String sql;
	public static String servername;

	private static int READ_TIMEOUT = 15000;
	private static int CONNECTION_TIMEOUT = 15000;
	
	public static boolean createDB(JSONObject formValue){
		
//		DBOperation dbOp = new DBOperation();
//		DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();
		
		boolean status = false;
		
		zilla = Integer.parseInt(formValue.getString("zilla"));
		
		DBInfoHandler detailDomain = new DBInfoHandler();
		Properties prop = ConfInfoRetrieve.readingConf(detailDomain.getDomainLoc());
		String[] DomainDetails = prop.getProperty("DOMAININFO_"+zilla).split(",");
		
		String host=DomainDetails[0];
//	    String user=DomainDetails[1];
//	    String password=DomainDetails[2];
//	    Integer portNum=Integer.parseInt(DomainDetails[3]);
	    
		
		try{
			
			
			zilla = Integer.parseInt(formValue.getString("zilla"));
			zillaname = formValue.getString("zillaname");
			upazila = Integer.parseInt(formValue.getString("upazila"));
			dbname = "RHIS_"+zilla+"_"+String.format("%02d", upazila);
			dburl = formValue.getString("dburl");
			dbusername = formValue.getString("dbusername");
			dbuserpass = formValue.getString("dbuserpass");
			
			sql = "CREATE DATABASE \""+dbname+"\" WITH TEMPLATE = \"RHIS_DB_DoNotTouch\"";

//			if(dburl.equals(""))
//			{
//				dburl = "jdbc:postgresql://localhost:5432/";
//				dbusername = "postgres";
//				dbuserpass = "123456";
//			}
//			else
//				dburl = "jdbc:postgresql://"+dburl+"/";
//			
//			System.out.println(dburl);
//			System.out.println(dbusername);
//			System.out.println(dbuserpass);
			System.out.println(sql);

			DBInfoHandler detailFacility = new DBInfoHandler();
			Properties prop_db = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
			String[] dbDetails = null;
			dbDetails = prop_db.getProperty("FACILITY_"+zilla).split(",");

			Connection c = DriverManager.getConnection(dbDetails[0]+dbDetails[1], "postgres", "postgres");
			//c.setAutoCommit(false);
			Statement statement = c.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			statement.execute(sql);

			statement.close();
			c.close();
			
//			DBOperation dbOp = new DBOperation();
//			DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
			
//			dbOp.dbExecuteUpdate(dbObject, sql);
			
//			Connection c = DriverManager.getConnection(dburl, dbusername, dbuserpass);
//			Statement statement = c.createStatement();
//			statement.executeUpdate(sql);
			
			status = true;
			
//			dbOp.closeStatement(dbObject);
//			dbOp.closeConn(dbObject);
			
//			c.close();
			
//			dbOp.dbExecute(dbObject,sql);
			
			servername = zillaname+"-"+String.format("%02d", zilla)+"-"+String.format("%02d", upazila)+"_ngo";

			JSONObject jsonStr = new JSONObject();
			jsonStr.put("providerid", "");
			jsonStr.put("zillaid", zilla);
			jsonStr.put("upazilaid", upazila);
			jsonStr.put("symserver", servername);
			jsonStr.put("symnode", "");

			String URL = "";

//			if(zilla == 69)
//				URL = "http://"+host+":8080/eMIS_69/";
//			else
				URL = "http://"+host+":8080/eMIS_NGO_"+zilla+"/";

			System.out.println(URL);

			serverComm(URL + "updownstatus", "info=" + jsonStr);
			
			/*String command1="bash /var/lib/symmetricds/symmetric-server-3.7.27/bin/symadmin --engine "+servername+" create-sym-tables";
		    ArrayList<String> result = new ArrayList<String>();

		    System.out.println(command1);

		    java.util.Properties config = new java.util.Properties();
	    	config.put("StrictHostKeyChecking", "no");
	    	JSch jsch = new JSch();
	    	Session session=jsch.getSession(user, host, portNum);
	    	session.setPassword(password);
	    	session.setConfig(config);
	    	session.connect();
	    	System.out.println("Connected");

	    	Channel channel=session.openChannel("exec");
	        ((ChannelExec)channel).setCommand(command1);
	        channel.setInputStream(null);
	        ((ChannelExec)channel).setErrStream(System.err);

	        InputStream in=channel.getInputStream();
	        channel.connect();

	     // Read the output from the input stream we set above
	        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	        String line;

	        //Read each line from the buffered reader and add it to result list
	        // You can also simple print the result here
	        while ((line = reader.readLine()) != null)
	        {
	        	result.add(line);
	        }
	        System.out.println(Arrays.asList(result));
	        //retrieve the exit status of the remote command corresponding to this channel
	        int exitStatus = channel.getExitStatus();

	        channel.disconnect();
	        session.disconnect();
	        System.out.println("DONE");*/
			
		}
		catch(Exception e){
			System.out.println(e);
			status = false;
			
			e.printStackTrace();
		}
		
		return status;
	}

	private static void serverComm(String uri, String jsonString){

		try{
			URL url = new URL(uri);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(READ_TIMEOUT);
			conn.setConnectTimeout(CONNECTION_TIMEOUT);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			OutputStream os = conn.getOutputStream();
			BufferedWriter writer =
					new BufferedWriter(
							new OutputStreamWriter(os, "UTF-8"));
			writer.write(jsonString);

			writer.flush();
			writer.close();
			os.close();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println("No response!");
			}
			else {
				InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				StringBuilder stringBuilder = new StringBuilder();
				String bufferedStrChunk;
				while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
					stringBuilder.append(bufferedStrChunk);
				}
				String testString = stringBuilder.toString();
				System.out.println(testString);
			}
		}
		catch(MalformedURLException mul) {
			mul.getStackTrace();
		}
		catch (ProtocolException pe) {
			pe.getStackTrace();
		}
		catch (IOException io) {
			io.getStackTrace();

		}
		catch (Exception e) {
			e.getStackTrace();
		}
	}
}
