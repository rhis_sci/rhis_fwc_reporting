package org.sci.rhis.report;

import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;

//import jdk.nashorn.internal.parser.JSONParser;

public class GetPSResultSet {

//	public static String resultString;
//	public static String graphString;

	public static String getFinalResult(String sql, String zilla, String upazila) {
		String resultString = "";
		DBInfoHandler dbObject;
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		if(upazila.equals("0"))
			dbObject = dbFacility.facilityDBInfo(zilla);
		else
		    dbObject = dbFacility.dynamicDBInfo(zilla, upazila);


		LinkedHashMap<String, String> referCenter = new LinkedHashMap<String, String>();

		referCenter.put("0", "");
		referCenter.put("1", "মেডিকেল কলেজ হাসপাতাল");
		referCenter.put("2", "জেলা সদর হাসপাতাল");
		referCenter.put("3", "মা ও শিশু কল্যাণ কেন্দ্র");
		referCenter.put("4", "উপজেলা স্বাস্থ্য কমপ্লেক্স");
		referCenter.put("5", "ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র");
		referCenter.put("6", "কমিউনিটি ক্লিনিক");
		referCenter.put("7", "এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র");
		referCenter.put("8", "প্রাইভেট ক্লিনিক / হাসপাতাল");
		referCenter.put("9", "অন্যান্য");

		try {

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject, sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
			int columnCount = metadata.getColumnCount();

			//resultString = "<table class=\"table\" id=\"reportTableScroll\" style='display: block !important;max-height: 500px !important;overflow-y: scroll !important;'><thead><tr class='danger'><th colspan='3'>Total Number Of Results: </th><td colspan='"+(columnCount-3)+"'>"+rowCount+"</td></tr><tr class='danger'>";
			resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

			for (int i = 1; i <= columnCount; i++) {
				resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
				//System.out.println(metadata.getColumnName(i) + ", ");
			}
			resultString = resultString + "</tr></thead><tbody>";
		    		
			/*dbObject.getPreparedStatement().setString(1,reportCred.getString("report1_zilla"));
			dbObject.getPreparedStatement().setString(2,reportCred.getString("report1_upozilla"));
			dbObject.getPreparedStatement().setString(3,reportCred.getString("report1_zilla"));
			dbObject.getPreparedStatement().setString(4,reportCred.getString("report1_upozilla"));
			dbObject.getPreparedStatement().setString(5,reportCred.getString("report1_zilla"));
			dbObject.getPreparedStatement().setString(6,reportCred.getString("report1_upozilla"));
			dbObject.getPreparedStatement().setString(7,reportCred.getString("report1_zilla"));
			dbObject.getPreparedStatement().setString(8,reportCred.getString("report1_upozilla"));*/

			//dbObject = dbOp.dbExecute(dbObject);
			//ResultSet rs = dbObject.getResultSet();

			//searchList.put("count", 0 );
			int j = 0;
			while (rs.next()) {
				j++;
				if ((j % 2) == 0) {
					resultString = resultString + "<tr>";
				} else {
					resultString = resultString + "<tr>";
				}

				for (int i = 1; i <= columnCount; i++) {
					if (metadata.getColumnName(i).equals("Refer Center Name"))
						resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : referCenter.get(rs.getString(metadata.getColumnName(i)))) + "</td>";
					else
						resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
					//System.out.println(metadata.getColumnName(i) + ", ");
				}
				resultString = resultString + "</tr>";
				//System.out.println(rs.getString("providerId"));
				/*JSONObject resultSet = new JSONObject();
				
				searchList.put("count", searchList.getInt("count") + 1 );
				
				resultSet.put("providerId",rs.getString("providerId"));
				resultSet.put("ProvName",rs.getString("ProvName"));
				resultSet.put("ANC",((rs.getObject("ANC")==null)?"0":rs.getString("ANC")));
				resultSet.put("PNC",((rs.getObject("PNC")==null)?"0":rs.getString("PNC")));
				resultSet.put("PNC-N",((rs.getObject("PNC-N")==null)?"0":rs.getString("PNC-N")));
				resultSet.put("Delivery",((rs.getObject("Delivery")==null)?"0":rs.getString("Delivery")));
				resultSet.put("TOTAL",((rs.getObject("TOTAL")==null)?"0":rs.getString("TOTAL")));
				
				searchList.put(searchList.getString("count"), resultSet);
				/*resultList[i][1] = rs.getString("providerId");
				resultList[i][2] = rs.getString("ProvName");
				resultList[i][3] = ((rs.getObject("ANC")==null)?"0":rs.getString("ANC"));
				resultList[i][4] = ((rs.getObject("PNC")==null)?"0":rs.getString("PNC"));
				resultList[i][5] = ((rs.getObject("PNC-N")==null)?"0":rs.getString("PNC-N"));
				resultList[i][6] = ((rs.getObject("Delivery")==null)?"0":rs.getString("Delivery"));
				resultList[i][7] = ((rs.getObject("TOTAL")==null)?"0":rs.getString("TOTAL"));
				i++;	*/
			}

			resultString = resultString + "</tbody></table>";

			//System.out.println(resultString);
			//System.out.println(resultList[2][1]);
			if (!rs.isClosed()) {
				rs.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return resultString;

	}

	public static String getFinalResultFiltered(String sql, String zilla, String upazila) {
		String resultString = "";
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = null;

		if (upazila.equals("0"))
			dbObject = dbFacility.facilityDBInfo(zilla);
		else
			dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

		LinkedHashMap<String, String> referCenter = new LinkedHashMap<String, String>();

		referCenter.put("0", "");
		referCenter.put("1", "মেডিকেল কলেজ হাসপাতাল");
		referCenter.put("2", "জেলা সদর হাসপাতাল");
		referCenter.put("3", "মা ও শিশু কল্যাণ কেন্দ্র");
		referCenter.put("4", "উপজেলা স্বাস্থ্য কমপ্লেক্স");
		referCenter.put("5", "ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র");
		referCenter.put("6", "কমিউনিটি ক্লিনিক");
		referCenter.put("7", "এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র");
		referCenter.put("8", "প্রাইভেট ক্লিনিক / হাসপাতাল");
		referCenter.put("9", "অন্যান্য");

		try {

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject, sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
			int columnCount = metadata.getColumnCount();

			resultString = "<ul class=\"legend\"> " +
					"<li><span class=\"eclampsia\"></span> Eclampsia/Pre-Eclampsia but not Referred</li> " +
					"<li><span class=\"high_bp_albumin\"></span> High BP with Albumin but not Referred</li> " +
					"<li><span class=\"high_bp\"></span> High BD but not Referred</li> " +
					"</ul><br /><br />";

			//resultString = "<table class=\"table filterdTable\" id=\"reportTableScroll\" style='display: block !important;max-height: 500px !important;overflow-y: scroll !important;'><tr class='danger'>";
			resultString += "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTableFilter\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

			for (int i = 1; i <= columnCount; i++) {
				resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
				//System.out.println(metadata.getColumnName(i) + ", ");
			}

			resultString = resultString + "</tr><thead><tfoot><tr>";

//		    for (int i = 1; i <= columnCount; i++) {
//		    	resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
//		    	//System.out.println(metadata.getColumnName(i) + ", ");      
//		    }
//		    //resultString = resultString + "</tr></thead><tbody>";
//		    
//		    resultString = resultString	+ "</tr><tfoot><tr>";

			for (int i = 1; i <= columnCount; i++) {
				resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
				//System.out.println(metadata.getColumnName(i) + ", ");
			}
			resultString = resultString + "</tr></tfoot><tbody>";

			//int j =0;
			String columnName = "EDD";
			while (rs.next()) {
				//j++;
//				if(columnName.equals(metadata.getColumnName(6))){
//					SimpleDateFormat sdf = new SimpleDateFormat("DD/mm/YYYY");
//			        java.util.Date date1 = sdf.parse(rs.getString("Visit Date"));
//			        java.util.Date date2 = sdf.parse(rs.getString("EDD"));
				//System.out.println(rs.getString("Delivery Date"));
//					if(date1.compareTo(date2) > 0 && rs.getString("Delivery Date") == null){
//						resultString = resultString + "<tr style='background-color: red'>";
//					}
//					else{
//						resultString = resultString + "<tr>";
//					}

//			        if(rs.getString("Eligible for Referral").equals("YES") && rs.getString("Referred").equals("NO")){
//						resultString = resultString + "<tr style='background-color: ornage'>";
//					}
//					else{
//						resultString = resultString + "<tr>";
//					}
//				}
//				else

//				if(rs.getString("Eligible for Referral").equals("YES") && rs.getString("Referred").equals("NO")){

				String[] bp = rs.getString("BP").split("/");

				if (rs.getString("Referred").equals("NO") && rs.getString("Seizure/<br />Unconsciousness").equals("YES")) {//Eclampsia condition
					resultString = resultString + "<tr style='background-color: #f39b71'>";
				} else if (rs.getString("Referred").equals("NO") && ((rs.getString("Albumin").equals("YES") && rs.getString("Headache/<br />Blurred Vision/<br />Abdominal Pain").equals("YES")) || Integer.parseInt(bp[1]) >= 110)) {//Pre-Eclampsia condition
					resultString = resultString + "<tr style='background-color: #f39b71'>";
				} else if (rs.getString("Referred").equals("NO") && rs.getString("Albumin").equals("YES")) {//High BP and only albumin condition
					resultString = resultString + "<tr style='background-color: #eee861'>";
				} else if (rs.getString("Referred").equals("NO") && rs.getString("Albumin").equals("NO")) {//only high bp condition
					resultString = resultString + "<tr style='background-color: #8c91cf'>";
				} else
					resultString = resultString + "<tr>";

				for (int i = 1; i <= columnCount; i++) {
					//if(metadata.getColumnName(i).equals("Refer Center Name"))
					//resultString = resultString + "<td style='text-align: center'>" + ((rs.getObject(metadata.getColumnName(i))==null)?"":referCenter.get(rs.getString(metadata.getColumnName(i)))) + "</td>";
					//else
					resultString = resultString + "<td style='text-align: center'>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
					//System.out.println(metadata.getColumnName(i) + ", ");
				}
				resultString = resultString + "</tr>";
			}

			//resultString = resultString + "<tr class='danger'><th colspan='3'>Total Number Of Results: </th><td colspan='"+(columnCount-3)+"'>"+rowCount+"</td></tr></tbody></table>";
			resultString = resultString + "</tbody></table>";

			if (!rs.isClosed()) {
				rs.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return resultString;

	}

	public static String getFinalResultLBW(String sql, String zilla, String upazila) {
		String resultString = "";
		DBInfoHandler dbObject;
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		if(upazila.equals("0"))
			dbObject = dbFacility.facilityDBInfo(zilla);
		else
			dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

		LinkedHashMap<String, String> referCenter = new LinkedHashMap<String, String>();

		referCenter.put("0", "");
		referCenter.put("1", "মেডিকেল কলেজ হাসপাতাল");
		referCenter.put("2", "জেলা সদর হাসপাতাল");
		referCenter.put("3", "মা ও শিশু কল্যাণ কেন্দ্র");
		referCenter.put("4", "উপজেলা স্বাস্থ্য কমপ্লেক্স");
		referCenter.put("5", "ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র");
		referCenter.put("6", "কমিউনিটি ক্লিনিক");
		referCenter.put("7", "এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র");
		referCenter.put("8", "প্রাইভেট ক্লিনিক / হাসপাতাল");
		referCenter.put("9", "অন্যান্য");

		try {
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject, sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
			int columnCount = metadata.getColumnCount();
			String tableTagStart = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTableFilter\" cellspacing=\"0\" width=\"100%\">";
			String tableTagEnd = "</table>";
			String tableHeaderFooter = "";
			String tableBody = "";
			String[] referCenterName = {"", "মেডিকেল কলেজ হাসপাতাল", "জেলা সদর হাসপাতাল ", "মা ও শিশু কল্যাণ কেন্দ্র", "উপজেলা স্বাস্থ্য কমপ্লেক্স", "ইউনিয়ন স্বাস্থ্য ও পরিবার কল্যান কেন্দ্র", "কমিউনিটি ক্লিনিক", "এনজিও হাসপাতাল / ক্লিনিক / সেবা কেন্দ্র", "প্রাইভেট ক্লিনিক / হাসপাতাল", "অন্যান্য"};
			String[] refReasons = {"", "জন্মগত শ্বাসরুদ্ধতা (asphyxia)", "অপরিণত জন্ম (Prematurity)", "কম ওজন নিয়ে জন্ম"};

			String centerName = "";

			// Generate html table header and footer
			tableHeaderFooter += "<thead>";
			tableHeaderFooter += "<tr>";
			for (int i = 1; i <= columnCount; i++) {
				tableHeaderFooter += "<th>" + metadata.getColumnName(i) + "</th>";
			}
			tableHeaderFooter += "</tr>";
			tableHeaderFooter += "</thead>";

			// Generate html table header and footer
//			tableHeaderFooter += "<thead>";
//			tableHeaderFooter += "<tr>";
//			for (int i = 1; i <= columnCount; i++) {
//				tableHeaderFooter += "<th>" + metadata.getColumnName(i) + "</th>";
//			}
//			tableHeaderFooter += "</tr>";
//			tableHeaderFooter += "</thead>";

			tableHeaderFooter += "<tfoot>";
			tableHeaderFooter += "<tr>";
			for (int i = 1; i <= columnCount; i++) {
				tableHeaderFooter += "<th>" + metadata.getColumnName(i) + "</th>";
			}
			tableHeaderFooter += "</tr>";
			tableHeaderFooter += "</tfoot>";

			//Generate body
			tableBody += "<tbody>";
			while (rs.next()) {
				if (rs.getString("Referred").equals("YES")) {
					tableBody += "<tr>";
					for (int i = 1; i <= columnCount; i++) {
						String value = rs.getString(metadata.getColumnName(i)) == null ? "" : rs.getString(metadata.getColumnName(i));
						if (metadata.getColumnName(i).equals("Refer Center Name")) {
							try {
								centerName = referCenterName[Integer.parseInt(rs.getString("Refer Center Name"))];
							} catch (Exception e) {
								centerName = "";
							}
							tableBody += "<td style='text-align: left'>" + centerName + "</td>";
						} else if (metadata.getColumnName(i).equals("Refer Reason")) {
							String reasonsIds = rs.getString("Refer Reason").replace('[', ' ').replace(']', ' ').replace('"', ' ')
									.replace("0", " ").replace("1", refReasons[1]).replace("2", refReasons[2]).replace("3", refReasons[3]);
							tableBody += "<td style='text-align: left'>" + reasonsIds + "</td>";
						} else if (metadata.getColumnName(i).equals("Weight(gm)")) {
							Double weight = Double.parseDouble(rs.getString("Weight(gm)")) * 1000;
							tableBody += "<td style='text-align: center'>" + Math.round(weight * Math.pow(10, 1)) / Math.pow(10, 1) + "</td>";
						} else if (metadata.getColumnName(i).equals("Delivery Place")) {
							if (value.equals("Home")) {
								tableBody += "<td style='text-align: left'>" + value + "</td>";
							} else {
								try {
									centerName = referCenterName[Integer.parseInt(value)];
								} catch (Exception e) {
									centerName = "";
								}
								tableBody += "<td style='text-align: left'>" + centerName + "</td>";
							}
						} else {
							tableBody += "<td style='text-align: center'>" + value + "</td>";
						}
					}
					tableBody += "</tr>";
				} else {
					//#efbad7
					if (Double.parseDouble(rs.getString("Weight(gm)")) < 2.5 && Double.parseDouble(rs.getString("Weight(gm)")) >= 2.0) {
						tableBody += "<tr style='background-color: #FFFF99'>";
					} else if (Double.parseDouble(rs.getString("Weight(gm)")) < 2.0 && Double.parseDouble(rs.getString("Weight(gm)")) >= 1.8) {
						tableBody += "<tr style='background-color: #FFD27F'>";
					} else if (Double.parseDouble(rs.getString("Weight(gm)")) < 1.8 && Double.parseDouble(rs.getString("Weight(gm)")) >= 1.2) {
						tableBody += "<tr style='background-color: #FFACBC'>";
					} else if (Double.parseDouble(rs.getString("Weight(gm)")) < 1.2) {
						tableBody += "<tr style='background-color: #FF8484'>";
					} else {
						tableBody += "<tr style='background-color: #FAD7A0'>";
					}
					for (int i = 1; i <= columnCount; i++) {
						String value = rs.getString(metadata.getColumnName(i)) == null ? "" : rs.getString(metadata.getColumnName(i));
						if (metadata.getColumnName(i).equals("Refer Center Name")) {
							try {
								centerName = referCenterName[Integer.parseInt(rs.getString("Refer Center Name"))];
							} catch (Exception e) {
								centerName = "";
							}
							tableBody += "<td style='text-align: left'>" + centerName + "</td>";
						} else if (metadata.getColumnName(i).equals("Refer Reason")) {
							tableBody += "<td style='text-align: left'></td>";
						} else if (metadata.getColumnName(i).equals("Weight(gm)")) {
							Double weight = Double.parseDouble(rs.getString("Weight(gm)")) * 1000;
							tableBody += "<td style='text-align: center'>" + Math.round(weight * Math.pow(10, 1)) / Math.pow(10, 1) + "</td>";
						} else if (metadata.getColumnName(i).equals("Delivery Place")) {
							if (value.equals("Home")) {
								tableBody += "<td style='text-align: left'>" + value + "</td>";
							} else {
								try {
									centerName = referCenterName[Integer.parseInt(value)];
								} catch (Exception e) {
									centerName = "";
								}
								tableBody += "<td style='text-align: left'>" + centerName + "</td>";
							}
						} else {
							tableBody += "<td style='text-align: center'>" + value + "</td>";
						}
					}
					tableBody += "</tr>";
				}
			}
			tableBody += "</tbody>";

			String legend = "<ul class=\"legend\"> " +
					"<li><span class=\"weight_2000\"></span> 2000>=Weight>2500 and not Referred</li> " +
					"<li><span class=\"weight_1800\"></span> 1800>=Weight>2000 and not Referred</li> " +
					"<li><span class=\"weight_1200\"></span> 1200>=Weight>1800 and not Referred</li> " +
					"<li><span class=\"weight_less_1200\"></span> Weight<1200 and not Referred</li> " +
					"</ul><br /><br />";

//			resultString = tableTagStart+ tableHeaderFooter + tableBody + tableHeaderFooter +tableTagEnd;
			resultString = legend + tableTagStart + tableHeaderFooter + tableBody + tableTagEnd;
			//System.out.println("RRRRRRRRRRRR");
			System.out.println(resultString);
			if (!rs.isClosed()) {
				rs.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		return resultString;

	}

	public static String getFinalResultDAN(String sql, String zilla, String upazila) {
		String resultString = "";
		DBInfoHandler dbObject;
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		if(upazila.equals("0")){
			dbObject = dbFacility.facilityDBInfo(zilla);
		}else {
			 dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
		}

		try {

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject, sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
			int columnCount = metadata.getColumnCount();

			LinkedHashMap<String, String> countTotal = new LinkedHashMap<String, String>();

			//resultString = "<table class=\"table\" id=\"reportTableScroll\" style='display: block !important;max-height: 500px !important;overflow-y: scroll !important;'><thead><tr class='danger'><th colspan='3'>Total Number Of Results: </th><td colspan='"+(columnCount-3)+"'>"+rowCount+"</td></tr><tr class='danger'>";
			resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

			for (int i = 1; i <= columnCount; i++) {
				resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
				//System.out.println(metadata.getColumnName(i) + ", ");
			}
			resultString = resultString + "</tr></thead><tbody>";


			int j = 0;
			while (rs.next()) {
				j++;
				if ((j % 2) == 0) {
					resultString = resultString + "<tr>";
				} else {
					resultString = resultString + "<tr>";
				}

				if (j > 1) {
					countTotal.put("Delivery", Integer.toString(Integer.parseInt(countTotal.get("Delivery")) + Integer.parseInt(rs.getString("Delivery"))));
					countTotal.put("LiveBirth", Integer.toString(Integer.parseInt(countTotal.get("LiveBirth")) + Integer.parseInt(rs.getString("Live Birth"))));
					countTotal.put("StillBirth", Integer.toString(Integer.parseInt(countTotal.get("StillBirth")) + Integer.parseInt(rs.getString("Still Birth"))));
					countTotal.put("Chlorehexidin", Integer.toString(Integer.parseInt(countTotal.get("Chlorehexidin")) + Integer.parseInt(rs.getString("Chlorehexidin"))));
					countTotal.put("Oxytocin", Integer.toString(Integer.parseInt(countTotal.get("Oxytocin")) + Integer.parseInt(rs.getString("Oxytocin"))));
					countTotal.put("Control Chord Traction", Integer.toString(Integer.parseInt(countTotal.get("Control Chord Traction")) + Integer.parseInt(rs.getString("Control Chord Traction"))));
					countTotal.put("Uterus Massage", Integer.toString(Integer.parseInt(countTotal.get("Uterus Massage")) + Integer.parseInt(rs.getString("Uterus Massage"))));
					countTotal.put("AMTSL", Integer.toString(Integer.parseInt(countTotal.get("AMTSL")) + Integer.parseInt(rs.getString("AMTSL"))));
					countTotal.put("Resuscitation", Integer.toString(Integer.parseInt(countTotal.get("Resuscitation")) + Integer.parseInt(rs.getString("Resuscitation"))));
					countTotal.put("Dry in 1 Minute", Integer.toString(Integer.parseInt(countTotal.get("Dry in 1 Minute")) + Integer.parseInt(rs.getString("Dry in 1 Minute"))));
					countTotal.put("BagNMask", Integer.toString(Integer.parseInt(countTotal.get("BagNMask")) + Integer.parseInt(rs.getString("BagNMask"))));
					countTotal.put("BreastFeed", Integer.toString(Integer.parseInt(countTotal.get("BreastFeed")) + Integer.parseInt(rs.getString("Breast Feed"))));
					countTotal.put("lowBirthWeight", Integer.toString(Integer.parseInt(countTotal.get("lowBirthWeight")) + Integer.parseInt(rs.getString("Low Birth Weight"))));
					countTotal.put("skinTouch", Integer.toString(Integer.parseInt(countTotal.get("skinTouch")) + Integer.parseInt(rs.getString("Skin Touch"))));
				} else {
					countTotal.put("Delivery", rs.getString("Delivery"));
					countTotal.put("LiveBirth", rs.getString("Live Birth"));
					countTotal.put("StillBirth", rs.getString("Still Birth"));
					countTotal.put("Chlorehexidin", rs.getString("Chlorehexidin"));
					countTotal.put("Oxytocin", rs.getString("Oxytocin"));
					countTotal.put("Control Chord Traction", rs.getString("Control Chord Traction"));
					countTotal.put("Uterus Massage", rs.getString("Uterus Massage"));
					countTotal.put("AMTSL", rs.getString("AMTSL"));
					countTotal.put("Resuscitation", rs.getString("Resuscitation"));
					countTotal.put("Dry in 1 Minute", rs.getString("Dry in 1 Minute"));
					countTotal.put("BagNMask", rs.getString("BagNMask"));
					countTotal.put("BreastFeed", rs.getString("Breast Feed"));
					countTotal.put("lowBirthWeight", rs.getString("Low Birth Weight"));
					countTotal.put("skinTouch", rs.getString("Skin Touch"));
				}

				for (int i = 1; i <= columnCount; i++) {
					resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
					//System.out.println(metadata.getColumnName(i) + ", ");
				}
				resultString = resultString + "</tr>";
				//System.out.println(rs.getString("providerId"));

			}

			if (j > 0) {
//				if(((j+1)%2)==0){
//					resultString = resultString + "<tfoot><tr class='success'>";
//				}
//				else{
//					resultString = resultString + "<tfoot><tr>";
//				}

				resultString = resultString + "<tfoot><tr class='success'>";

				DecimalFormat df = new DecimalFormat("0.00");

				resultString = resultString + "<td colspan='2' align='center'><b>Total :</b> </td>";
				resultString = resultString + "<td>" + countTotal.get("Delivery") + "</td>";
				resultString = resultString + "<td>" + countTotal.get("LiveBirth") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("LiveBirth")) / (Double.parseDouble(countTotal.get("LiveBirth")) + Double.parseDouble(countTotal.get("StillBirth")))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("StillBirth") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("StillBirth")) / (Double.parseDouble(countTotal.get("LiveBirth")) + Double.parseDouble(countTotal.get("StillBirth")))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("Chlorehexidin") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("Chlorehexidin")) / Double.parseDouble(countTotal.get("LiveBirth"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("Oxytocin") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("Oxytocin")) / Double.parseDouble(countTotal.get("Delivery"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("Control Chord Traction") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("Control Chord Traction")) / Double.parseDouble(countTotal.get("Delivery"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("Uterus Massage") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("Uterus Massage")) / Double.parseDouble(countTotal.get("Delivery"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("AMTSL") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("AMTSL")) / Double.parseDouble(countTotal.get("Delivery"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("Resuscitation") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("Resuscitation")) / Double.parseDouble(countTotal.get("LiveBirth"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("Dry in 1 Minute") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("Dry in 1 Minute")) / Double.parseDouble(countTotal.get("LiveBirth"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("BagNMask") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("BagNMask")) / Double.parseDouble(countTotal.get("LiveBirth"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("BreastFeed") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("BreastFeed")) / Double.parseDouble(countTotal.get("LiveBirth"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("lowBirthWeight") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("lowBirthWeight")) / Double.parseDouble(countTotal.get("LiveBirth"))) * 100) + "</td>";
				resultString = resultString + "<td>" + countTotal.get("skinTouch") + "</td>";
				resultString = resultString + "<td>" + df.format((Double.parseDouble(countTotal.get("skinTouch")) / Double.parseDouble(countTotal.get("LiveBirth"))) * 100) + "</td>";

				resultString = resultString + "</tr></tfoot>";
			}

			resultString = resultString + "</tbody></table>";

			if (!rs.isClosed()) {
				rs.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		return resultString;

	}
}
