package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.util.CalendarDate;

public class ProviderSupervision {
	
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String union_query ;
	public static String sql;
	public static String date_type;
	public static String monthSelect;
	public static String yearSelect;
	public static String start_date;
	public static String end_date;
	public static String serviceType;
	public static String indicatorType;
	public static String sbaType;
	public static String deliveryPlace;
	public static String viewServiceBy;
	public static String tableName;
	public static String condition;

	
	public static String getResultSet(JSONObject reportCred){
		String resultString;
		zilla = reportCred.getString("report1_zilla");
		DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
		resultString = "";
		try{
			upazila = reportCred.getString("report1_upazila");
			union = reportCred.getString("report1_union");
			date_type = reportCred.getString("report1_dateType");

			if(reportCred.has("serviceType")) {
				serviceType = reportCred.getString("serviceType");
			}
			indicatorType = reportCred.getString("indicatorType");
			if(reportCred.has("sbaType")) {
				sbaType = reportCred.getString("sbaType");
			}
			if(reportCred.has("deliveryPlace")) {
				deliveryPlace = reportCred.getString("deliveryPlace");
			}
			if(reportCred.has("viewServiceBy")) {
				viewServiceBy = reportCred.getString("viewServiceBy");
			}
			
			//System.out.println(union);
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else if(date_type.equals("3"))
			{
				yearSelect = reportCred.getString("report1_year");
				start_date = yearSelect + "-01-01";
				end_date = yearSelect + "-12-31";
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
			if(serviceType.equals("ANC"))
				tableName = table_schema.getTable("table_ancservice");
			else if(serviceType.equals("PNC"))
				tableName = table_schema.getTable("table_pncservicemother");
			
			if(indicatorType.equals("HBP"))
				condition = "("+tableName+"."+table_schema.getColumn("","ancservice_bpsystolic",new String[]{"140"},">")+" OR "+tableName+"."+table_schema.getColumn("","ancservice_bpdiastolic",new String[]{"90"},">")+")";
			
			if(indicatorType.equals("NRC"))
			{
				String upazila_condion = " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " ";
				//upazila "All"
				if(upazila.equals("0"))
					upazila_condion = "";


				if(viewServiceBy.equals("1"))
					sql = "select \"Union Name\", ";
				else
					sql = "select \"Provider ID\", \"Provider Name\", \"Union Name\", ";
				
				sql = sql + "SUM(\"Total Service\") as \"Total Service\",  "
						+ "SUM(\"SNRC\") || ' (' || CONCAT(ROUND(((SUM(\"SNRC\")*100)/(SUM(\"SNRC\")+SUM(\"SHID\")))::numeric, 2), ' %') || ')' as \"Service By NRC\", "
						+ "SUM(\"SHID\") || ' (' || CONCAT(ROUND(((SUM(\"SHID\")*100)/(SUM(\"SNRC\")+SUM(\"SHID\")))::numeric, 2), ' %') || ')' as \"Service By Health ID\", "
						+ "SUM(\"New NRC Create\") as \"New NRC Create\" "
						+ "from ( ";
				
				if(serviceType.equals("ANC") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_ancservice")+" on "+table_schema.getColumn("table","ancservice_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","ancservice_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","ancservice_providerid")+" "
							+ "where (case when count_provider>1  then  "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","ancservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","ancservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  "
							+ "else "+ table_schema.getColumn("", "ancservice_visitdate",new String[]{start_date,end_date},"between") +" end) "
							+ "AND ("+table_schema.getColumn("", "ancservice_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("", "ancservice_serviceid",new String[]{"0"},"=")+") AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}	
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("PNC") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_pncservicemother")+" on "+table_schema.getColumn("table","pncservicemother_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","pncservicemother_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","pncservicemother_providerid")+" "
							+ "where (case when count_provider>1 then  "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pncservicemother_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pncservicemother_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  "
							+ "else "+ table_schema.getColumn("", "pncservicemother_visitdate",new String[]{start_date,end_date},"between") +" end) "
							+ "AND ("+table_schema.getColumn("", "pncservicemother_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("", "pncservicemother_servicesource",new String[]{"0"},"=")+") AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("PNC-N") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_pncservicechild")+" on "+table_schema.getColumn("table","pncservicechild_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","pncservicechild_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","pncservicechild_providerid")+" "
							+ "where (case when count_provider>1 then  "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pncservicechild_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pncservicechild_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  "
							+ "else "+ table_schema.getColumn("", "pncservicechild_visitdate",new String[]{start_date,end_date},"between") +" end) "
							+ "AND ("+table_schema.getColumn("", "pncservicechild_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("", "pncservicechild_servicesource",new String[]{"0"},"=")+") AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("Delivery") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_delivery")+" on "+table_schema.getColumn("table","delivery_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","delivery_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","delivery_providerid")+" "
							+ "where (case when count_provider>1 then  "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","delivery_outcomedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","delivery_outcomedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  "
							+ "else "+ table_schema.getColumn("", "delivery_outcomedate",new String[]{start_date,end_date},"between") +" end) "
							+ "AND "+ table_schema.getColumn("", "delivery_deliverydonebythisprovider",new String[]{"1"},"=") +" AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}	
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("PILLCON") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_pillcondomservice")+" on "+table_schema.getColumn("table","pillcondomservice_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","pillcondomservice_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
						    + "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","pillcondomservice_providerid")+" "
							+ "where (case when count_provider>1 then "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pillcondomservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pillcondomservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
							+ "else "+ table_schema.getColumn("", "pillcondomservice_visitdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("IUD") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_iudservice")+" on "+table_schema.getColumn("table","iudservice_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","iudservice_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") +  upazila_condion //" and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " "
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","iudservice_providerid")+" "
							+ "where (case when count_provider>1 then "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","iudservice_iudimplantdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","iudservice_iudimplantdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
							+ "else "+ table_schema.getColumn("", "iudservice_iudimplantdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("IUDFOL") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_iudfollowupservice")+" on "+table_schema.getColumn("table","iudfollowupservice_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","iudfollowupservice_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","iudfollowupservice_providerid")+" "
							+ "where (case when count_provider>1 then "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","iudfollowupservice_followupdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","iudfollowupservice_followupdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
							+ "else "+ table_schema.getColumn("", "iudfollowupservice_followupdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("Implant") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";

					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_implantservice")+" on "+table_schema.getColumn("table","implantservice_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","implantservice_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","implantservice_providerid")+" "
							+ "where (case when count_provider>1 then "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","implantservice_implantimplantdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","implantservice_implantimplantdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
							+ "else "+ table_schema.getColumn("", "implantservice_implantimplantdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";

					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("IMPFOL") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";

					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_implantfollowupservice")+" on "+table_schema.getColumn("table","implantfollowupservice_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","implantfollowupservice_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","implantfollowupservice_providerid")+" "
							+ "where (case when count_provider>1 then "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","implantfollowupservice_followupdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","implantfollowupservice_followupdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
							+ "else "+ table_schema.getColumn("", "implantfollowupservice_followupdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";

					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("INJ") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_womaninjectable")+" on "+table_schema.getColumn("table","womaninjectable_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","womaninjectable_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","womaninjectable_providerid")+" "
							+ "where (case when count_provider>1 then "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","womaninjectable_dosedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","womaninjectable_dosedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) "
							+ "else "+ table_schema.getColumn("", "womaninjectable_dosedate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				if(serviceType.equals("ALL"))
					sql = sql + "UNION ALL ";
				if(serviceType.equals("GP") || serviceType.equals("ALL"))
				{
					if(viewServiceBy.equals("1"))
						sql = sql + "select "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					else
						sql = sql + "select "+table_schema.getColumn("table","providerdb_provcode")+" as \"Provider ID\", "+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", "+table_schema.getColumn("table","unions_unionnameeng")+" as \"Union Name\", ";
					
					sql = sql + "COUNT(*) as \"Total Service\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SNRC\",  "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"<>"+table_schema.getColumn("table","clientmap_healthid")+") THEN 1 ELSE NULL END) as \"SHID\", "
							+ "COUNT(CASE WHEN ("+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","clientmap_healthid")+" AND ("+table_schema.getColumn("table","clientmap_systementrydate",new String[]{start_date,end_date},"between")+")) THEN 1 ELSE NULL END) AS \"New NRC Create\"  "
							+ "from "+table_schema.getTable("table_clientmap")+" "
							+ "JOIN "+table_schema.getTable("table_gpservice")+" on "+table_schema.getColumn("table","gpservice_healthid")+"="+table_schema.getColumn("table","clientmap_generatedid")+"  "
							+ "JOIN "+table_schema.getTable("table_providerdb")+" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","gpservice_providerid")+" "
							+ "JOIN "+table_schema.getTable("table_unions")+" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"  "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and "+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + upazila_condion
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","gpservice_providerid")+" "
							+ "where (case when "+table_schema.getColumn("","gpservice_visitdate")+" is not null then (case when count_provider>1 then "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","gpservice_visitdate")+" between '"+ start_date +" ' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","gpservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +" ' end) "
							+ "else "+ table_schema.getColumn("", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" end) else "+table_schema.getColumn("","gpservice_visitdate")+" is null end) "
							+ "GROUP BY ";
									
					if(viewServiceBy.equals("1"))
						sql = sql + ""+table_schema.getColumn("table","unions_unionnameeng")+" ";
					else
						sql = sql + ""+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","unions_unionnameeng")+" ";
				}
				
				if(viewServiceBy.equals("1"))
					sql = sql + ") \"NRC\" GROUP BY \"Union Name\" ORDER BY \"Total Service\" DESC";
				else
					sql = sql + ") \"NRC\" GROUP BY \"Provider ID\", \"Provider Name\", \"Union Name\" ORDER BY \"Total Service\" DESC";

				resultString = GetPSResultSet.getFinalResult(sql, zilla, upazila);
			}	
			else if(indicatorType.equals("HBP"))
			{
				String upazila_query = "";
				//added upazila "all"
				if(upazila.equals("0")) {
					union_query = "";
				}else {

					if (!union.equals("null") && !union.isEmpty()) {
						upazila_query = " AND upazilaid = " + upazila;
						union_query = " AND unionid = " + union;
					}else{
						upazila_query = " AND upazilaid = " + upazila;
						union_query = "";
					}
				}


				
				if(serviceType.equals("PNC")){
					sql = "select "+tableName+"."+table_schema.getColumn("","pncservicemother_healthid")+" as \"Health ID\", "
						+ "(CASE WHEN "+table_schema.getColumn("table", "clientmap_name",new String[]{},"isnotnull")+" THEN "+table_schema.getColumn("table", "clientmap_name")+" ELSE "+table_schema.getColumn("table", "member_nameeng")+" END) as \"Name\", "
						+ tableName+"."+table_schema.getColumn("", "pncservicemother_visitdate")+" as \"Visit Date\", "+tableName+"."+table_schema.getColumn("", "pncservicemother_bpsystolic")+" as \"BP Systolic\", "+tableName+"."+table_schema.getColumn("", "pncservicemother_bpdiastolic")+" as \"BP Diastolic\", "
						+ "(CASE WHEN "+tableName+"."+ table_schema.getColumn("", "pncservicemother_refer",new String[]{"1"},"=") +" THEN 'Refered' ELSE 'Not Refered' END) as \"Refer\", "
						+ tableName+"."+table_schema.getColumn("", "pncservicemother_refercentername")+" as \"Refer Center Name\" "
						+ "from "+tableName+" "
						+ "left join "
						+ ""+table_schema.getTable("table_clientmap")+" on "+table_schema.getColumn("table","clientmap_generatedid")+"="+tableName+"."+table_schema.getColumn("","pncservicemother_healthid")+" "
						+ "left join "
						+ ""+table_schema.getTable("table_member")+" on "+table_schema.getColumn("table","member_healthid")+"="+table_schema.getColumn("table","clientmap_healthid")+" "
						+ "where "+condition
						+ " AND "+tableName+"."+table_schema.getColumn("","pncservicemother_providerid")+" IN "
						+ "       (select "+table_schema.getColumn("","providerdb_provcode")+" from "+table_schema.getTable("table_providerdb")+" where " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") +upazila_query+union_query+" AND ("+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") +")) "
						+ "AND "+tableName+"."+table_schema.getColumn("", "pncservicemother_visitdate")+" BETWEEN ('" + start_date + "') AND ('" + end_date +"') ";
					
					resultString = GetPSResultSet.getFinalResult(sql, zilla, upazila);
				}
				else{
					String sql_condition = "";
					//added upazila "all"
					if(upazila == null || upazila.equals("")||upazila.equals("0"))
						sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " ";
					else if((upazila != null || !upazila.equals("")) && (union == null || union.equals("")))
						sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " ";
					else if(union != null || !union.equals(""))
						sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " and " + table_schema.getColumn("table", "providerdb_unionid",new String[]{union},"=") + " ";

					sql = "select c."+table_schema.getColumn("","clientmap_healthid")+" as \"Health ID\", '0' || c."+table_schema.getColumn("","clientmap_mobileno")+" as \"Mobile No\", u."+table_schema.getColumn("","upazila_upazilanameeng")+" as \"Upazila Name\", "
							+ "un."+table_schema.getColumn("","unions_unionnameeng")+" as \"Union Name\", \"anc\"."+table_schema.getColumn("","ancservice_providerid")+" as \"Provider ID\",to_char("+table_schema.getColumn("","pregwomen_lmp")+", 'DD/mm/YYYY') as \"LMP\", to_char("+table_schema.getColumn("","pregwomen_edd")+", 'DD/mm/YYYY') as \"EDD\" "
							+ ", to_char((case when "+table_schema.getColumn("", "ancservice_visitdate",new String[]{},"isnull")+" then anc."+table_schema.getColumn("", "ancservice_systementrydate")+" else "+table_schema.getColumn("","ancservice_visitdate")+" end ), 'DD/mm/YYYY') as \"Visit Date\" "
							+ ", "+table_schema.getColumn("","ancservice_serviceid")+" as \"Visit\""
							+ ", div(extract('month' from  age( "+table_schema.getColumn("","ancservice_visitdate")+", "+table_schema.getColumn("", "pregwomen_lmp")+"))::numeric + 1,3) as \"Trimester\" "
							+ ", format('%s/%s', "+table_schema.getColumn("", "ancservice_bpsystolic")+", "+table_schema.getColumn("", "ancservice_bpdiastolic")+") as \"BP\" "
							+ ", (case when "+table_schema.getColumn("", "ancservice_urinealbumin")+"::integer > 1 then 'YES' else 'NO' end) as \"Albumin\" "
							+ ", (case when "+table_schema.getColumn("", "ancservice_edema")+"::integer > 1 then 'YES' else 'NO' end) as \"Edema\" "
							+ ", (case when (anc."+table_schema.getColumn("", "ancservice_symptom")+" like '%\"3\"%' or anc."+table_schema.getColumn("", "ancservice_symptom")+" like '%\"5\"%') then 'YES' else 'NO' end) as \"Headache/<br />Blurred Vision/<br />Abdominal Pain\" "
							+ ", (case when (anc."+table_schema.getColumn("", "ancservice_symptom")+" like '%\"2\"%' or anc."+table_schema.getColumn("", "ancservice_symptom")+" like '%\"6\"%') then 'YES' else 'NO' end) as \"Seizure/<br />Unconsciousness\" "
							+ ", (case when anc."+table_schema.getColumn("", "ancservice_treatment")+" like '%\"2\"%' then 'YES' else 'NO' end) as \"Loding Dose<br />Given\" "
							+ ", (case when "+table_schema.getColumn("anc", "ancservice_refer",new String[]{"1"},"=")+" then 'YES' else 'NO' end ) as \"Referred\" "
							+ ", to_char("+table_schema.getColumn("", "delivery_outcomedate")+", 'DD/mm/YYYY') as \"Delivery Date\""
							+ ", (case when "+table_schema.getColumn("", "delivery_outcometype",new String[]{"1"},"=")+" then 'Normal' else (case when "+table_schema.getColumn("", "delivery_outcometype",new String[]{"2"},"=")+" then 'Cesarean' else NULL end) end) as \"Delivery Type\" "
							+ ", (case when "+table_schema.getColumn("","ancservice_visitdate")+" = "+table_schema.getColumn("", "delivery_outcomedate")+" then 'YES' else 'NO' end) as \"ImmediateDelivery\" "
							+ ", "+table_schema.getColumn("", "delivery_convulsion")+" as \"Convulsion\", "+table_schema.getColumn("", "delivery_livebirth")+" as \"Live Birth\", "+table_schema.getColumn("", "delivery_stillbirth")+" as \"Still Birth\" "
							+ ", (case when "+table_schema.getColumn("", "ancservice_bpsystolic",new String[]{"300"},">")+" OR "+table_schema.getColumn("", "ancservice_bpdiastolic",new String[]{"150"},">")+" then 'YES' else 'NO' end) as \"Error\" "
							+ "from "+table_schema.getTable("table_ancservice")+" anc  "
							+ "left join "+table_schema.getTable("table_pregwomen")+" using ("+table_schema.getColumn("","pregwomen_healthid")+", "+table_schema.getColumn("", "pregwomen_pregno")+")  "
							+ "left join "+table_schema.getTable("table_delivery")+" using ("+table_schema.getColumn("","delivery_healthid")+", "+table_schema.getColumn("", "delivery_pregno")+") "
							+ "inner join "+table_schema.getTable("table_clientmap")+" c on anc."+table_schema.getColumn("","ancservice_healthid")+" = c."+table_schema.getColumn("","clientmap_generatedid")+" "
							+ "join "+table_schema.getTable("table_upazila")+" u on c."+table_schema.getColumn("","clientmap_zillaid")+"=u."+table_schema.getColumn("","upazila_zillaid")+" and c."+table_schema.getColumn("","clientmap_upazilaid")+"=u."+table_schema.getColumn("","upazila_upazilaid")+" "
							+ "join "+table_schema.getTable("table_unions")+" un on c."+table_schema.getColumn("","clientmap_zillaid")+"=un."+table_schema.getColumn("","unions_zillaid")+" and c."+table_schema.getColumn("","clientmap_upazilaid")+"=un."+table_schema.getColumn("","unions_upazilaid")+" and c."+table_schema.getColumn("","clientmap_unionid")+"=un."+table_schema.getColumn("","unions_unionid")+" "
							+ "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("anc","ancservice_providerid")+ " "
							+ "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") "
							+ "and " + sql_condition + " "
							+ "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=anc."+table_schema.getColumn("","ancservice_providerid")+" "
							+ "where ("+table_schema.getColumn("", "ancservice_bpdiastolic",new String[]{"90"},">=")+")  "
							+ "AND (case when count_provider>1  then  "
							+ "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","ancservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","ancservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  "
							+ "else "+ table_schema.getColumn("", "ancservice_visitdate",new String[]{start_date,end_date},"between") +" end) ";

							//sql = 	sql + "AND "+ table_schema.getColumn("", "ancservice_visitdate",new String[]{start_date,end_date},"between") +" ";

								//sql = 	sql + "AND c."+table_schema.getColumn("","clientmap_zillaid",new String[]{zilla},"=")+" AND c."+table_schema.getColumn("","clientmap_upazilaid",new String[]{upazila},"=")+" AND c."+table_schema.getColumn("","clientmap_unionid",new String[]{union},"=")+" ";
					
					sql = 	sql + "order by "+ table_schema.getColumn("", "ancservice_visitdate") +" asc";

					System.out.println(sql);
					
					resultString = GetPSResultSet.getFinalResultFiltered(sql, zilla, upazila);
				}
			}
			else if(indicatorType.equals("DAN"))
			{
				
				sql = "select \"Year\", month as \"Month\", total_delivery as \"Delivery\" "
						+ ", \"LiveBirth\" as \"Live Birth\",trunc((d_n.\"LiveBirth\"::numeric/(d_n.\"LiveBirth\"::numeric+d_n.\"StillBirth\"::numeric))*100, 2) as \"Live Birth(%)\" "
						+ ", \"StillBirth\" as \"Still Birth\",trunc((d_n.\"StillBirth\"::numeric/(d_n.\"LiveBirth\"::numeric+d_n.\"StillBirth\"::numeric))*100, 2) as \"Still Birth(%)\" "
						+ ", \"Chlorehexidin\",(case when d_n.\"LiveBirth\"::numeric = 0 then 0 else trunc((d_n.\"Chlorehexidin\"::numeric/d_n.\"LiveBirth\"::numeric)*100, 2) end) as \"Chlorehexidin(%)\" "
						+ ", d_n.\"Oxytocin\", trunc((d_n.\"Oxytocin\"::numeric/total_delivery)*100, 2) as \"Oxytocin(%)\" "
						+ ", d_n.\"Control Chord Traction\", trunc((d_n.\"Control Chord Traction\"::numeric/total_delivery)*100, 2) as \"Control Chord Traction(%)\" "
						+ ", d_n.\"Uterus Massage\", trunc((d_n.\"Uterus Massage\"::numeric/total_delivery)*100, 2) as \"Uterus Massage(%)\" "
						+ ", d_n.\"AMTSL\"::numeric, trunc((d_n.\"AMTSL\"::numeric/total_delivery)*100, 2) as \"AMTSL(%)\" "
						+ ", d_n.\"Resuscitation\", (case when d_n.\"LiveBirth\"::numeric = 0 then 0 else trunc((d_n.\"Resuscitation\"::numeric/d_n.\"LiveBirth\"::numeric)*100, 2) end) as \"Resuscitation(%)\" "
						+ ", d_n.\"dryinoneminute\" as \"Dry in 1 Minute\", (case when d_n.\"LiveBirth\"::numeric = 0 then 0 else trunc((d_n.\"dryinoneminute\"::numeric/d_n.\"LiveBirth\"::numeric)*100, 2) end) as \"Dry in 1 Minute(%)\" "
						+ ", d_n.\"bagNMask\" as \"BagNMask\", (case when d_n.\"LiveBirth\"::numeric = 0 then 0 else trunc((d_n.\"bagNMask\"::numeric/d_n.\"LiveBirth\"::numeric)*100, 2) end) as \"BagNMask(%)\" "
						+ ", d_n.\"breastFeed\" as \"Breast Feed\", (case when d_n.\"LiveBirth\"::numeric = 0 then 0 else trunc((d_n.\"breastFeed\"::numeric/d_n.\"LiveBirth\"::numeric)*100, 2) end) as \"Breast Feed(%)\" "
						+ ", d_n.\"lowBirthWeight\" as \"Low Birth Weight\", (case when d_n.\"LiveBirth\"::numeric = 0 then 0 else trunc((d_n.\"lowBirthWeight\"::numeric/d_n.\"LiveBirth\"::numeric)*100, 2) end) as \"Low Birth Weight(%)\" "
						+ ", d_n.\"skinTouch\" as \"Skin Touch\", (case when d_n.\"LiveBirth\"::numeric = 0 then 0 else trunc((d_n.\"skinTouch\"::numeric/d_n.\"LiveBirth\"::numeric)*100, 2) end) as \"Skin Touch(%)\" "
						+ "from "
						+ " "
						+ "( "
						+ "select   "
						+ "date_part('year'::text, d."+table_schema.getColumn("", "delivery_outcomedate")+") AS \"Year\", "
						+ "to_char(d."+table_schema.getColumn("", "delivery_outcomedate")+"::timestamp with time zone, 'MONTH'::text) AS month, "
						+ " sum (case when ("+table_schema.getColumn("","delivery_applyoxytocin",new String[]{"1"},"=")+" and n."+table_schema.getColumn("","newborn_childno",new String[]{"1"},"=")+") then 1 else 0 end) as \"Oxytocin\" "
						+ ", sum (case when ("+table_schema.getColumn("","delivery_applytraction",new String[]{"1"},"=")+" and n."+table_schema.getColumn("","newborn_childno",new String[]{"1"},"=")+") then 1 else 0 end) as \"Control Chord Traction\" "
						+ ", sum (case when ("+table_schema.getColumn("","delivery_uterusmassage",new String[]{"1"},"=")+" and n."+table_schema.getColumn("","newborn_childno",new String[]{"1"},"=")+") then 1 else 0 end) as \"Uterus Massage\" "
						+ ", sum (case when ("+table_schema.getColumn("","delivery_applyoxytocin",new String[]{"1"},"=")+" and "+table_schema.getColumn("","delivery_applytraction",new String[]{"1"},"=")+" and "+table_schema.getColumn("","delivery_uterusmassage",new String[]{"1"},"=")+" and n."+table_schema.getColumn("","newborn_childno",new String[]{"1"},"=")+") then 1 else 0 end) as \"AMTSL\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_resassitation",new String[]{"1"},"=")+" then 1 else 0 end) as \"Resuscitation\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_bagnmask",new String[]{"1"},"=")+" then 1 else 0 end) as \"bagNMask\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_chlorehexidin",new String[]{"1"},"=")+" then 1 else 0 end) as \"Chlorehexidin\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_dryingafterbirth",new String[]{"1"},"=")+" then 1 else 0 end) as \"dryinoneminute\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_breastfeed",new String[]{"1"},"=")+" then 1 else 0 end) as \"breastFeed\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_birthstatus",new String[]{"1"},"=")+" then 1 else 0 end) as \"LiveBirth\" "
						+ ", sum (case when ("+table_schema.getColumn("","newborn_birthstatus",new String[]{"2"},"=")+" or "+table_schema.getColumn("","newborn_birthstatus",new String[]{"3"},"=")+") then 1 else 0 end) as \"StillBirth\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_birthweight")+"<2.5 then 1 else 0 end) as \"lowBirthWeight\" "
						+ ", sum (case when "+table_schema.getColumn("","newborn_skintouch",new String[]{"1"},"=")+" then 1 else 0 end) as \"skinTouch\" "
						+ ", count (distinct("+table_schema.getColumn("","delivery_healthid")+")) as total_delivery "
						+ "from "+table_schema.getTable("table_delivery")+" d left join "+table_schema.getTable("table_newborn")+" n using ("+table_schema.getColumn("","newborn_healthid")+", "+table_schema.getColumn("", "newborn_pregno")+") "
						+ "where "+ table_schema.getColumn("d", "delivery_outcomedate",new String[]{start_date,end_date},"between") +" and " + table_schema.getColumn("n", "newborn_healthid",new String[]{},"isnotnull") +" ";
				
				if(deliveryPlace.equals("1"))
					sql = 	sql + "AND "+ table_schema.getColumn("d", "delivery_outcomeplace",new String[]{"1"},"=") +" ";
				else if(deliveryPlace.equals("2"))
					sql = 	sql + "AND "+ table_schema.getColumn("d", "delivery_outcomeplace",new String[]{"1"},">") +" ";

				//added upazila "all"
				if(upazila.equals("0")){
					sql = 	sql + "AND "+ table_schema.getColumn("d", "delivery_providerid") +" in (select "+ table_schema.getColumn("", "providerdb_provcode") +" from "+table_schema.getTable("table_providerdb")+" where " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " AND ("+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") +")) ";

				}else {
					if (union != null && !union.isEmpty())
						sql = sql + "AND " + table_schema.getColumn("d", "delivery_providerid") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=") + " AND unionid=" + union + " AND (" + table_schema.getColumn("", "providerdb_provtype", new String[]{"4", "5", "6", "101", "17"}, "in") + ")) ";
				   //union All
				    else
						sql = sql + "AND " + table_schema.getColumn("d", "delivery_providerid") + " in (select " + table_schema.getColumn("", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=") + " AND (" + table_schema.getColumn("", "providerdb_provtype", new String[]{"4", "5", "6", "101", "17"}, "in") + ")) ";
				}
				sql = 	sql + "GROUP BY date_part('year'::text, d."+table_schema.getColumn("", "delivery_outcomedate")+"), date_part('month'::text, d."+table_schema.getColumn("", "delivery_outcomedate")+"), to_char(d."+table_schema.getColumn("", "delivery_outcomedate")+"::timestamp with time zone, 'MONTH'::text) "
						+ "ORDER BY date_part('year'::text, d."+table_schema.getColumn("", "delivery_outcomedate")+"), date_part('month'::text, d."+table_schema.getColumn("", "delivery_outcomedate")+")  "
						+ ") as d_n";
				System.out.println(sql);
				resultString = GetPSResultSet.getFinalResultDAN(sql, zilla, upazila);
			}else if(indicatorType.equals("LBW")){
				sql = "Select " +
						""+ table_schema.getColumn("table", "providertype_provtype") +" as \"Provider Type\"," +
						""+ table_schema.getColumn("table", "providerdb_provcode") +" as \"Provider Code\", " +
						""+table_schema.getColumn("table","providerdb_provname")+" as \"Provider Name\", " +
						"concat('0',"+ table_schema.getColumn("table", "providerdb_mobileno") +"::BIGINT) as \"Provider Mobile\","+
						""+table_schema.getColumn("table","newborn_healthid")+" as \"Client Health ID\"," +
						"concat('0',"+table_schema.getColumn("table","clientmap_mobileno")+"::BIGINT) as \"Client Mobile\", " +
						""+table_schema.getColumn("table", "pregwomen_lmp")+" as \"LMP\"," +
						""+table_schema.getColumn("table", "pregwomen_edd")+" as \"EDD\"," +
						"TRUNC(DATE_PART('day', "+table_schema.getColumn("table", "newborn_outcomedate")+"::TIMESTAMP-"+table_schema.getColumn("table", "pregwomen_lmp")+"::TIMESTAMP)/7) as \"Pregnancy Duration(week)\"," +
						""+table_schema.getColumn("table", "newborn_outcomedate")+" as \"Delivery Date\"," +
						"concat(case when "+ table_schema.getColumn("table", "newborn_outcomeplace",new String[]{"1"},"=") +" then 'Home' else "+ table_schema.getColumn("table", "delivery_deliverycentername") +" end) as \"Delivery Place\"," +
						"(case when "+table_schema.getColumn("table", "newborn_outcometype",new String[]{"1"},"=")+" then 'Normal' else (case when "+ table_schema.getColumn("table", "newborn_outcomeplace",new String[]{"2"},"=") +" then 'Cesarean' else NULL end) end) as \"Delivery Type\"," +
						""+ table_schema.getColumn("table", "newborn_childno") +" as \"Child No\"," +
						"(case when "+ table_schema.getColumn("table", "newborn_gender",new String[]{"1"},"=") +" then 'Male' else 'Female' end) as \"Gender\"," +
						""+table_schema.getColumn("table","newborn_birthweight")+" as \"Weight(gm)\"," +
						"(case when "+ table_schema.getColumn("table", "newborn_refer",new String[]{"1"},"=") +" then 'YES' else 'NO' end ) as \"Referred\"," +
						""+ table_schema.getColumn("table", "newborn_referreason") +" as \"Refer Reason\"," +
						""+ table_schema.getColumn("table", "newborn_refercentername") +" as \"Refer Center Name\"," +
						"case when "+table_schema.getColumn("table","newborn_birthweight")+"<1.0 or "+table_schema.getColumn("table","newborn_birthweight")+">=9.0 then 'Error' else '' end as \"Error\"" +
						"from "+table_schema.getTable("table_newborn")+" JOIN "+table_schema.getTable("table_providerdb")+" ON "+ table_schema.getColumn("table", "newborn_providerid") +"="+ table_schema.getColumn("table", "providerdb_provcode") +" " +
						"join "+table_schema.getTable("table_clientmap")+" On "+table_schema.getColumn("table","newborn_healthid")+" = "+table_schema.getColumn("table","clientmap_generatedid")+" " +
						"join "+table_schema.getTable("table_delivery")+" ON "+table_schema.getColumn("table","delivery_healthid")+" = "+table_schema.getColumn("table","newborn_healthid")+" and "+table_schema.getColumn("table", "newborn_pregno")+"="+table_schema.getColumn("table", "delivery_pregno")+"" +
						"join "+table_schema.getTable("table_pregwomen")+" ON "+table_schema.getColumn("table","pregwomen_healthid")+" = "+table_schema.getColumn("table","newborn_healthid")+" and "+table_schema.getColumn("table", "newborn_pregno")+"="+table_schema.getColumn("table", "pregwomen_pregno")+"" +
						"join "+table_schema.getTable("table_providertype")+" ON "+table_schema.getColumn("table","providertype_provtype")+" = "+table_schema.getColumn("table","providerdb_provtype")+" " +
						"where "+table_schema.getColumn("table","newborn_birthweight")+" < 2.5 " +
						"AND "+ table_schema.getColumn("table", "newborn_outcomedate",new String[]{start_date,end_date},"between") +" ";

				if (reportCred.getString("deliveryPlace").equals("1")){
					sql += " AND  "+ table_schema.getColumn("table", "newborn_outcomeplace",new String[]{"1"},"=") +" ";
				}else if(reportCred.getString("deliveryPlace").equals("2")){
					sql += " AND  "+ table_schema.getColumn("table", "newborn_outcomeplace",new String[]{"2"},"=") +" ";
				}

				//added upazila "all"
				if(upazila.equals("0")){
					sql = 	sql + "AND "+ table_schema.getColumn("table", "newborn_providerid") +" in (select "+ table_schema.getColumn("table", "providerdb_provcode") +" from "+table_schema.getTable("table_providerdb")+" where " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") +" AND ("+ table_schema.getColumn("", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") +")) ";

				}else {
					if (union != null && !union.isEmpty())
						sql = sql + "AND " + table_schema.getColumn("table", "newborn_providerid") + " in (select " + table_schema.getColumn("table", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=") + " AND unionid=" + union + " AND (" + table_schema.getColumn("", "providerdb_provtype", new String[]{"4", "5", "6", "101", "17"}, "in") + ")) ";
					else
						sql = sql + "AND " + table_schema.getColumn("table", "newborn_providerid") + " in (select " + table_schema.getColumn("table", "providerdb_provcode") + " from " + table_schema.getTable("table_providerdb") + " where " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=") + " AND (" + table_schema.getColumn("", "providerdb_provtype", new String[]{"4", "5", "6", "101", "17"}, "in") + ")) ";
				}
				sql = 	sql + "order by "+table_schema.getColumn("table", "newborn_outcomedate")+","+ table_schema.getColumn("table", "newborn_childno") +" asc";

				System.out.println("=="+sql);
				resultString = GetPSResultSet.getFinalResultLBW(sql, zilla, upazila);
			}


		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return resultString;
	}

}
