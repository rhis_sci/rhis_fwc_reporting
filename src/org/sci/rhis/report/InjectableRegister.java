package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import java.time.YearMonth;

public class InjectableRegister {
	
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String dateType;
    public static String yearSelect;
    public static String monthSelect;
	public static String startDate;
	public static String endDate;
    public static String resultSet;

	public static String getResultSet(JSONObject reportCred){
        String tableHtml = "";
        try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			union = reportCred.getString("report1_union");
			dateType = reportCred.getString("report1_dateType");
            String facility_id =  reportCred.getString("facilityid");
            startDate = LocalDate.now().withDayOfMonth(1).toString();
            endDate =LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).toString();
                try {
                    if(dateType.equals("1")) {
                        monthSelect = reportCred.getString("report1_month");
                        String[] date = monthSelect.split("-");
                        int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                        startDate = monthSelect + "-01";
                        endDate = monthSelect + "-" + daysInMonth;
                    }
                    else if(dateType.equals("3"))
                    {
                        yearSelect = reportCred.getString("report1_year");
                        startDate = yearSelect + "-01-01";
                        endDate = yearSelect + "-12-31";
                    }else {
                        startDate = reportCred.getString("report1_start_date");
                        endDate = reportCred.getString("report1_end_date");
                        if(startDate.equals("")){
                            LocalDate date= LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
                            startDate = date.withDayOfMonth(1).toString();
                        }
                    }
                DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
                FacilityDB dbFacility2nd = new FacilityDB();
                DBInfoHandler dbObject2nd = dbFacility2nd.facilityDBInfo(Integer.parseInt(zilla));
                DBOperation dbOp2nd = new DBOperation();
                dbObject2nd = dbOp2nd.dbCreateStatement(dbObject2nd);
                dbObject2nd = dbOp2nd.dbExecute(dbObject2nd, "SELECT "+table_schema.getColumn("","facility_provider_provider_id")+" from facility_provider where facility_id = "+facility_id);
                ResultSet provider_obj = dbObject2nd.getResultSet();
                String providerList = "";
                while (provider_obj.next()) {
                    String id = provider_obj.getString("providerid");
                    providerList += id +",";
                }
                providerList = providerList.substring(0, providerList.length() - 1);
                String sql = "SELECT cm.*,vil.*,uni.*,upa.*,zil.*,fpe.*,elc.*," +
                        "CASE WHEN age(wi."+table_schema.getColumn("","womaninjectable_dosedate")+", cm.dob) IS NULL THEN '' ELSE concat(date_part('year',age(wi."+table_schema.getColumn("","womaninjectable_dosedate")+", cm.dob)),'বছর  ', date_part('month',age(wi."+table_schema.getColumn("","womaninjectable_dosedate")+", cm.dob)),'মাস  ', date_part('day',age(wi."+table_schema.getColumn("","womaninjectable_dosedate")+", cm.dob)),'দিন') END as client_age, " +
                        "rs."+table_schema.getColumn("","regserial_serialno")+"  as serial, " +
                        "to_char(wi."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date1, "+
                        "to_char(wi2."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date2, "+
                        "to_char(wi3."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date3, "+
                        "to_char(wi4."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date4, "+
                        "to_char(wi5."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date5, "+
                        "to_char(wi6."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date6, "+
                        "to_char(wi7."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date7, "+
                        "to_char(wi8."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date8, "+
                        "to_char(wi9."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date9, "+
                        "to_char(wi10."+table_schema.getColumn("","womaninjectable_dosedate")+",'DD MM YYYY') as date10, "+

                        "to_char(wi."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date2, "+
                        "to_char(wi2."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date3, "+
                        "to_char(wi3."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date4, "+
                        "to_char(wi4."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date5, "+
                        "to_char(wi5."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date6, "+
                        "to_char(wi6."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date7, "+
                        "to_char(wi7."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date8, "+
                        "to_char(wi8."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date9, "+
                        "to_char(wi9."+table_schema.getColumn("","womaninjectable_dosedate")+"::date + '90 day'::INTERVAL,'DD MM YYYY') as due_date10, "+

                        "CASE WHEN elc."+table_schema.getColumn("","elco_son")+" IS NULL THEN 0 ELSE elc."+table_schema.getColumn("","elco_son")+"::integer END as client_son, "+
                        "CASE WHEN elc."+table_schema.getColumn("","elco_dau")+" IS NULL THEN 0 ELSE elc."+table_schema.getColumn("","elco_dau")+"::integer END as client_dau, "+
                        "to_char(fpe."+table_schema.getColumn("","fpexamination_lmp")+",'DD MM YYYY') as client_lmp, "+
                        "CASE WHEN fpe."+table_schema.getColumn("","fpexamination_bpsystolic")+" IS NULL THEN '' ELSE CONCAT(fpe."+table_schema.getColumn("","fpexamination_bpsystolic")+",'/',fpe."+table_schema.getColumn("","fpexamination_bpdiastolic")+") END as client_bp, "+
                        "CASE fpe."+table_schema.getColumn("","fpexamination_breastcondition")+"::integer when 0 then '' when 1 then 'নাই' when 2 then 'আছে' else '' END as client_breast "+
                        " FROM "+ table_schema.getTable("table_womaninjectable")+" as wi " +
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi2 ON wi2."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi2."+table_schema.getColumn("","womaninjectable_doseid")+"=2 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi3 ON wi3."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi3."+table_schema.getColumn("","womaninjectable_doseid")+"=3 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi4 ON wi4."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi4."+table_schema.getColumn("","womaninjectable_doseid")+"=4 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi5 ON wi5."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi5."+table_schema.getColumn("","womaninjectable_doseid")+"=5 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi6 ON wi6."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi6."+table_schema.getColumn("","womaninjectable_doseid")+"=6 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi7 ON wi7."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi7."+table_schema.getColumn("","womaninjectable_doseid")+"=7 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi8 ON wi8."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi8."+table_schema.getColumn("","womaninjectable_doseid")+"=8 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi9 ON wi9."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi9."+table_schema.getColumn("","womaninjectable_doseid")+"=9 "+
                        "LEFT JOIN "+ table_schema.getTable("table_womaninjectable")+" as wi10 ON wi10."+table_schema.getColumn("","womaninjectable_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi10."+table_schema.getColumn("","womaninjectable_doseid")+"=10 "+
                        "LEFT JOIN "+ table_schema.getTable("table_fpexamination")+" as fpe ON wi."+table_schema.getColumn("","womaninjectable_healthid")+" = fpe."+table_schema.getColumn("","fpexamination_healthid")+" and fpe."+table_schema.getColumn("","fpexamination_serviceid")+"=1 and fpe."+table_schema.getColumn("","fpexamination_fptype")+"=2 "+
                        " LEFT JOIN "+ table_schema.getTable("table_clientmap")+" as cm ON cm."+table_schema.getColumn("","clientmap_generatedid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi."+table_schema.getColumn("","womaninjectable_doseid")+" = 1 " +
                        " LEFT JOIN "+ table_schema.getTable("table_elco")+" as elc ON elc."+table_schema.getColumn("","elco_healthid")+" = wi."+table_schema.getColumn("","womaninjectable_healthid")+" and wi."+table_schema.getColumn("","womaninjectable_doseid")+" = 1 " +
                        " LEFT JOIN "+ table_schema.getTable("table_regserial")+" as rs ON rs."+table_schema.getColumn("","regserial_healthid")+"=cm."+table_schema.getColumn("","clientmap_healthid")+" and rs."+table_schema.getColumn("","regserial_servicecategory")+" =1 and rs."+table_schema.getColumn("","regserial_providerid")+" IN ("+providerList+") and wi."+table_schema.getColumn("","womaninjectable_doseid")+" = 1  " +
                        " LEFT JOIN "+ table_schema.getTable("table_village")+" as vil ON cm."+table_schema.getColumn("","clientmap_villageid")+" = vil."+table_schema.getColumn("","village_villageid")+" and cm."+table_schema.getColumn("","clientmap_mouzaid")+" = vil."+table_schema.getColumn("","village_mouzaid")+" and cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = vil."+table_schema.getColumn("","village_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_unionid")+" = vil."+table_schema.getColumn("","village_unionid")+"  and cm."+table_schema.getColumn("","clientmap_zillaid")+" = vil."+table_schema.getColumn("","village_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_unions")+" as uni ON cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = uni."+table_schema.getColumn("","unions_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_unionid")+" = uni."+table_schema.getColumn("","unions_unionid")+"  and cm."+table_schema.getColumn("","clientmap_zillaid") +" = uni."+table_schema.getColumn("","unions_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_upazila")+" as upa ON cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = upa."+table_schema.getColumn("","upazila_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_zillaid") +" = upa."+table_schema.getColumn("","upazila_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_zilla")+" as zil ON cm."+table_schema.getColumn("","clientmap_zillaid") +" = zil."+table_schema.getColumn("","zilla_zillaid")+" " +

                        "WHERE wi."+table_schema.getColumn("","womaninjectable_doseid")+"=1 and wi."+table_schema.getColumn("","womaninjectable_providerid")+" IN ("+providerList+") and   wi."+table_schema.getColumn("","womaninjectable_dosedate")+" BETWEEN '"+startDate+"' AND '"+endDate+"' order by wi."+table_schema.getColumn("","womaninjectable_dosedate")+"  asc";
                System.out.println(sql);
                FacilityDB dbFacility = new FacilityDB();
                DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla,upazila);

                DBOperation dbOp = new DBOperation();
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject, sql);
                ResultSet rs = dbObject.getResultSet();
                //*******************************************//

                List<JSONObject> jsonObj = new ArrayList<JSONObject>();
                ResultSetMetaData meta = rs.getMetaData();
                while (rs.next()) {
                    HashMap<String, String> data = new HashMap<String, String>();
                    for (int i = 1; i <= meta.getColumnCount(); i++) {
                        String key = meta.getColumnName(i);
                        String value = rs.getString(key);
                        if(value == null){
                            value = "";
                        }
                        data.put(key, value);
                    }
                    jsonObj.add(new JSONObject(data));
                }
                return jsonObj.toString();

                //*******************************************//
            }catch (Exception e){
                System.out.println(e);
            }

//			System.out.println(startDate+"   "+endDate);
		}
		catch(Exception e){
			System.out.println(e);
            resultSet = "Bad Url Request";
		}
		return resultSet;
	}

}
