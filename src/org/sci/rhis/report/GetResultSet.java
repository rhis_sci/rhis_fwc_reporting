package org.sci.rhis.report;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.model.GetResult;
import org.sci.rhis.model.GetSSSql;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.*;

public class GetResultSet {

//	public static String resultString;
//	public static String graphString;

    public static String upazilaSql;

    public static LinkedHashMap<String, LinkedHashMap<String, String>> resultSetThread = new LinkedHashMap<String, LinkedHashMap<String, String>>();
    public static JSONObject searchInfo = null;

    public static String getFinalResult(String sql, String zilla, String upazila) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            for (int i = 1; i <= columnCount; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }
            resultString = resultString + "</tr></thead><tbody>";
            int j = 0;
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr style=\"cursor:pointer\">";
                } else {
                    resultString = resultString + "<tr style=\"cursor:pointer\">";
                }

                for (int i = 1; i <= columnCount; i++) {
                    resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                }
                resultString = resultString + "</tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    /*
     * Provider Information Edit for Superadmin and admin user
     * @Author: Evana
     * Code Date: 18/02/2019
     * */

    public static String getProviderFinalResult(String sql, Integer zilla, Integer providerType) {

        String resultString = "";
        String sdate = "";
        String frdate = "";

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = null;
        if (providerType != 0 && providerType != 4 && providerType != 5 && providerType != 6 && providerType != 17 && providerType != 101 && providerType != 2 && providerType != 3)
            dbObject = dbFacility.facilityDBInfo();
        else
            dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            for (int i = 1; i <= columnCount; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }
            resultString = resultString + "<th>Action</th></tr></thead><tbody>";

            int j = 0;
            int providerCode = 0;
            int zillaid = 0;
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount; i++) {
                    //resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i))==null)?"":rs.getString(metadata.getColumnName(i))) + "</td>";
                    if (i == 3) {
                        resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : "0" + rs.getString(metadata.getColumnName(i))) + "</td>";

                    } else {

                        resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";


                    }
                }
                //providerCode = rs.getObject(metadata.getColumnName(i));
                providerCode = (int) rs.getObject(metadata.getColumnName(2));

                //System.out.println("provider code =" + providerCode);
                //System.out.println("Zilla code=" + zilla);

                resultString = resultString + "<td><a title='Edit Profile Information' class='btn btn-primary' href='javascript:void(0)' onClick='editProvider( " + providerCode + "," + zilla + "," + providerType + ");'><span class='glyphicon glyphicon-pencil'></span></a></td></tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    public static String Dateformat(String ndate) throws ParseException {

        String strDate = ndate;

        //String startTime = "08/11/2008 00:00";
        // This could be MM/dd/yyyy, you original value is ambiguous
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        Date dateValue = input.parse(strDate);

        SimpleDateFormat input1 = new SimpleDateFormat("dd-MM-yyyy");
        String ndateValue = input1.format(dateValue);
        //System.out.println("formated date=" + ndateValue);
        return ndateValue;
    }

    public static boolean isDateValid(String date) {
        String DATE_FORMAT = "yyyy-MM-dd";
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }


    public static String getFinalResult(String sql, Integer zilla) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject;
        /**
         * 0 is not zillaid. to quick fix I used it as this func is called by two section.so this param is needed.
         */
        if (zilla == 0) {
            dbObject = dbFacility.facilityDBInfo(); // to view monitoring login info it will point facility_central

        } else {
            dbObject = dbFacility.facilityDBInfo(zilla); // to view user login info it will point specific zilla
        }

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            for (int i = 1; i <= columnCount; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }
            resultString = resultString + "</tr></thead><tbody>";

            int j = 0;
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount; i++) {
                    resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                }
                resultString = resultString + "</tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    public static String getFinalResultNew(String sql, Integer zilla) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            for (int i = 1; i <= columnCount; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }
            resultString = resultString + "</tr></thead><tbody>";

            int j = 0;
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount; i++) {
                    resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                }
                resultString = resultString + "</tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    public static String getFinalResult(String sql, String zilla, String upazila, String type) {
        return getFinalResult(sql, zilla, upazila, type, "false");
    }

    /**
     * @param dbDownload if false then it will work for view syncronization status
     *                   if true then it will work for automatic db download process for servlet DBdownloadRequest
     */
    public static <T> T getFinalResult(String sql, String zilla, String upazila, String type, String dbDownload) {
        Map<String, String> providerList = new HashMap<>();
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            //resultString = "<div class=\"row\"><div class=\"col-md-12\"><a href='http://mchdrhis.icddrb.org:8080/facility_offline_db/RHIS_36_71.zip' target='blank' class='btn btn-info'>Download DB</a></div>";
            if (!dbDownload.equals("true")) {
                resultString = "<div class=\"row\">";
                resultString += "<div class=\"col-md-12\">" +
                        "<ul class=\"legend\"> " +
                        "<li><span class=\"no_recent_login\"></span>14 days difference between Heartbeat and Last Login or No Login history</li> " +
                        "<li><span class=\"hb_ll_29\"></span> Heartbeat and Last Login time both have 14 days gap till today </li> " +
                        "<li><span class=\"registration_problem\"></span> Major registration problem </li> " +
                        "</ul><br /><br /></div>";

                resultString += "<div class=\"col-md-12\"><table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

                for (int i = 1; i <= columnCount; i++) {
                    resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
                }
//			if(type.equals("14")){
//				resultString = resultString + "<th>Action</th>";
//			}
                resultString = resultString + "</tr></thead><tbody>";
            }

            int j = 0;
            while (rs.next()) {
                j++;
                if (type.equals("14")) {
                    long diff, heartbeatAge, lastLoginAge;
                    diff = heartbeatAge = lastLoginAge = 0;
                    if (!(rs.getString("LAST LOGIN").equals("NO RECENT LOGIN") || rs.getString("HEARTBEAT").equals("NO HEARTBAT"))) {
                        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                        // changed this as last login time takes new format yyyy-MM-dd HH:mm:ss.SSS from user
                        LocalDateTime heartbeat = LocalDateTime.parse(getTimeInFormattedPattern(rs.getString("HEARTBEAT")), fmt);
                        LocalDateTime lastLogin = LocalDateTime.parse(getTimeInFormattedPattern(rs.getString("LAST LOGIN")), fmt);
                        LocalDateTime now = LocalDateTime.now();
                        diff = ChronoUnit.DAYS.between(heartbeat, lastLogin);
                        heartbeatAge = ChronoUnit.DAYS.between(heartbeat, now);
                        lastLoginAge = ChronoUnit.DAYS.between(lastLogin, now);
                    }

                    //for automatic db download servlet:DbdownloadRequest
                    boolean checkProviderForDBDownload = diff > 14 || rs.getString("LAST LOGIN").equals("NO RECENT LOGIN");
                    boolean noHeartbeat = rs.getString("HEARTBEAT").equals("NO HEARTBAT");
                    if (dbDownload.equals("true")) {
                        if (checkProviderForDBDownload) {
                            providerList.put(rs.getString("Providerid"), rs.getString("format"));
                        } else if (heartbeatAge > 14 && lastLoginAge > 14) { //purple colour row
                            //&& Integer.parseInt(rs.getString("UNSYNC DATA")) > 30000 //previous condition
                            providerList.put(rs.getString("Providerid"), rs.getString("format"));
                        } else if (noHeartbeat) {
                            providerList.put(rs.getString("Providerid"), rs.getString("format") + ",(No Heartbeat)");
                        }
                    } else { //normal process for view sync status
                        if (checkProviderForDBDownload) {
                            resultString = resultString + "<tr style='background-color: #f39b71'>";  //orange color
                        } else if (heartbeatAge > 14 && lastLoginAge > 14) {  //previously it was 29 days
                            resultString = resultString + "<tr style='background-color: #8c91cf'>";  //purple color
                        } else if (noHeartbeat) {
                            resultString = resultString + "<tr style='background-color: #ff001882'>"; // red color
                        } else {
                            resultString = resultString + "<tr>";
                        }
                        for (int i = 1; i <= columnCount; i++) {
                            resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                        }
//					if(diff >29){
//						resultString += "<td> <button class='btn btn-success'><i class='fa fa-refresh'> Reset</i></button></td>";
//					}else{
//						resultString += "<td></td>";
//					}
                    }
                } else {
                    resultString = resultString + "<tr>";
                    for (int i = 1; i <= columnCount; i++) {
                        resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                    }
                }

//				if(type.equals("14"))
//					resultString = resultString + "<th><button class='btn btn-info downloadDB' value='RHIS_"+zilla+"_"+upazila+"'>Download Offline DB</button></th>";
                resultString = resultString + "</tr>";
            }


            resultString = resultString + "</tbody></table></div>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        if (dbDownload.equals("true")) {
            return (T) providerList;
        } else {
            return (T) resultString;
        }

    }

    /**
     * It takes any format date (ex: yyyy-MM-dd HH:mm:ss, yyyy-MM-dd HH:mm:ss.SSS)
     *
     * @return this format yyyy-MM-dd HH:mm:ss
     */
    public static String getTimeInFormattedPattern(String DateInput) {
        String pattern = "yyyy-MM-dd HH:mm:ss[.SSS][.SS][.S]"; //"[yyyy-MM-dd[['T'][ ]HH:mm:ss[.SSSSSSSz][.SSS[XXX][X]]]]";
        SimpleDateFormat simpleDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        TemporalAccessor accessor = formatter.parse(DateInput);
        ZonedDateTime zTime = LocalDateTime.from(accessor).atZone(ZoneOffset.UTC);

        Date date = new Date(zTime.toEpochSecond() * 1000);
        simpleDateFormatter.setTimeZone(TimeZone.getTimeZone(ZoneOffset.UTC));
        return simpleDateFormatter.format(date);
    }


    public static String getFinalResultProviderWise(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date) {
        String graphString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, LinkedHashMap<String, String>> tableHeader = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject graphInfoTemp = null;
        String resultset = new String();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            GetSSSql getSSSql = new GetSSSql();
            dbObject = dbOp.dbExecute(dbObject, getSSSql.getSql(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date));
            getSSSql = null;
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            String colVal = "";
            String tempVal = "";
            int j = 1;

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr>";
            String table_body = "<tbody>";
            graphString = "";
            while (rs.next()) {

                colVal = form_type.equals("1") ? rs.getString(metadata.getColumnName(7)) : rs.getString(metadata.getColumnName(6));

                temp = new JSONObject();
                for (int i = 1; i <= columnCount; i++) {
                    if (!graphMap.containsKey(metadata.getColumnName(i)) && ((i > 7 && form_type.equals("1")) || (i > 6 && form_type.equals("11")))) {
                        graphMap.put(metadata.getColumnName(i), (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                    }
                    if (!tableHeader.containsKey(metadata.getColumnName(i)) && ((i != 7 && form_type.equals("1")) || (i != 6 && form_type.equals("11")))) {
                        tableHeader.put(metadata.getColumnName(i), (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                        table_header += "<th>" + metadata.getColumnName(i) + "</th>";
                    }
                    if (((i > 7 && form_type.equals("1")) || (i > 6 && form_type.equals("11")))) {
                        //temp.put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));
                        graphMap.get(metadata.getColumnName(i)).put(colVal, rs.getString(metadata.getColumnName(i)));
                    }
                    if (((i != 7 && form_type.equals("1")) || (i != 6 && form_type.equals("11"))))
                        table_body += "<td>" + rs.getString(metadata.getColumnName(i)) + "</td>";
                }
                table_body += "</tr>";
                //mapInfo.put(zilla + "_" + upazila + "_" + rs.getString(metadata.getColumnName(2)),temp);
            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";

            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    temp.put(key2, graphMap.get(key).get(key2));
                    graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }
            resultset = table + " table/graph " + graphInfo.toString();// + " table/graph/map " + mapInfo.toString();
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        //return searchList;
        return resultset;

    }

    public static String getFinalResultUnionWise(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date) {
        String graphString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject temp_item = null;
        JSONObject graphInfoTemp = null;
        String resultset = new String();
        JSONObject dashboard = new JSONObject();


        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            GetSSSql getSSSql = new GetSSSql();
            dbObject = dbOp.dbExecute(dbObject, getSSSql.getSql(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date));
            getSSSql = null;
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            String colVal = "";
            String colVal2 = "";
            String colVal3 = "";
            String tempVal = "";
            int j = 1;

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr>";
            if (sql_view_type.equals("4"))
                table_header += "<th>Facility Name</th><th>Upazila Name</th>";
            table_header += "<th>Union Name</th>";
            String table_body = "<tbody>";
            graphString = "";
            while (rs.next()) {

                colVal = rs.getString(metadata.getColumnName(2));

                if (sql_view_type.equals("4")) {
                    colVal2 = rs.getString(metadata.getColumnName(3));
                    colVal3 = rs.getString(metadata.getColumnName(4));
                }

                if (tempVal.equals(colVal)) {
                    colVal = rs.getString(metadata.getColumnName(2)) + "-" + j;
                    j++;
                } else
                    tempVal = rs.getString(metadata.getColumnName(2));

                temp = new JSONObject();
                LinkedHashMap<String, String> dashboard_temp = new LinkedHashMap<String, String>();
                if (sql_view_type.equals("4")) {
                    table_body += "<tr><td>" + rs.getString(metadata.getColumnName(2)) + "</td>";
                    table_body += "<td>" + rs.getString(metadata.getColumnName(3)) + "</td>";
                    table_body += "<td>" + rs.getString(metadata.getColumnName(4)) + "</td>";
                } else
                    table_body += "<tr><td>" + rs.getString(metadata.getColumnName(2)) + "</td>";
                for (int i = (sql_view_type.equals("4") ? 6 : 3); i <= columnCount; i++) {
                    temp_item = new JSONObject();
                    if (!graphMap.containsKey(metadata.getColumnName(i))) {
                        graphMap.put(metadata.getColumnName(i), (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                        table_header += "<th>" + metadata.getColumnName(i) + "</th>";
                    }

                    dashboard_temp.put('"' + metadata.getColumnName(i) + '"', '"' + rs.getString(metadata.getColumnName(i)) + '"');

                    temp.put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));

                    if (sql_view_type.equals("4"))
                        graphMap.get(metadata.getColumnName(i)).put(colVal + "_" + colVal2 + "_" + colVal3, rs.getString(metadata.getColumnName(i)));
                    else
                        graphMap.get(metadata.getColumnName(i)).put(colVal, rs.getString(metadata.getColumnName(i)));

                    table_body += "<td>" + rs.getString(metadata.getColumnName(i)) + "</td>";
                }
                table_body += "</tr>";
                if (sql_view_type.equals("4"))
                    mapInfo.put(zilla + "_" + upazila + "_" + rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(4)), temp);
                else
                    mapInfo.put(zilla + "_" + upazila + "_" + rs.getString(metadata.getColumnName(1)), temp);
                //System.out.println(dashboard_temp);
                dashboard.put(colVal, dashboard_temp);
            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";
            //System.out.println(dashboard);
            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    temp.put(key2, graphMap.get(key).get(key2));
                    graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }
            //System.out.println(dashboard);
            resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString() + " table/graph/map " + dashboard.toString();
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        //return searchList;
        return resultset;

    }

    public static String getResultUnionWise(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date) {
        String graphString = "";
        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject temp_item = null;
        JSONObject graphInfoTemp = null;
        String resultset = new String();
        JSONObject dashboard = new JSONObject();


        try {
            String colVal = "";
            String colVal2 = "";
            String colVal3 = "";
            String tempVal = "";
            int j = 1;

            GetResult getResult = new GetResult(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date);
            finalResult = getResult.getSSAggrResultMap();
            getResult = null;

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr>";
            if (sql_view_type.equals("4"))
                table_header += "<th>Facility Name</th><th>Upazila Name</th>";
            table_header += "<th>Union Name</th>";
            String table_body = "<tbody>";
            graphString = "";
            for (String key : finalResult.keySet()) {
                String[] key_val = key.split("_");
                if (sql_view_type.equals("3"))
                    colVal = finalResult.get(key).get("unionnameeng");
                else if (sql_view_type.equals("4")) {
                    colVal = finalResult.get(key).get("facility_name");
                    colVal2 = finalResult.get(key).get("upazilanameeng");
                    colVal3 = finalResult.get(key).get("unionnameeng");
                }

                if (tempVal.equals(colVal)) {
                    colVal = sql_view_type.equals("3") ? finalResult.get(key).get("unionnameeng") : finalResult.get(key).get("facility_name") + "-" + j;
                    j++;
                } else
                    tempVal = sql_view_type.equals("3") ? finalResult.get(key).get("unionnameeng") : finalResult.get(key).get("facility_name");

                temp = new JSONObject();
                LinkedHashMap<String, String> dashboard_temp = new LinkedHashMap<String, String>();
                if (sql_view_type.equals("4")) {
                    table_body += "<tr><td>" + finalResult.get(key).get("facility_name") + "</td>";
                    table_body += "<td>" + finalResult.get(key).get("upazilanameeng") + "</td>";
                    table_body += "<td>" + finalResult.get(key).get("unionnameeng") + "</td>";
                } else
                    table_body += "<tr><td>" + finalResult.get(key).get("unionnameeng") + "</td>";
                for (String key2 : finalResult.get(key).keySet()) {
                    if (!key2.equals("unionnameeng") && !key2.equals("upazilanameeng") && !key2.equals("facility_name")) {
                        temp_item = new JSONObject();
                        if (!graphMap.containsKey(key2)) {
                            graphMap.put(key2, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                            table_header += "<th>" + key2 + "</th>";
                        }

                        dashboard_temp.put('"' + key2 + '"', '"' + finalResult.get(key).get(key2) + '"');

                        temp.put(key2, finalResult.get(key).get(key2));

                        if (sql_view_type.equals("4"))
                            graphMap.get(key2).put(colVal + "_" + colVal2 + "_" + colVal3, finalResult.get(key).get(key2));
                        else
                            graphMap.get(key2).put(colVal, finalResult.get(key).get(key2));

                        table_body += "<td>" + finalResult.get(key).get(key2) + "</td>";
                    }
                }
                table_body += "</tr>";
                if (sql_view_type.equals("4"))
                    mapInfo.put(zilla + "_" + upazila + "_" + key_val[2] + "_" + colVal3, temp);
                else
                    mapInfo.put(zilla + "_" + upazila + "_" + key_val[2], temp);
                //System.out.println(dashboard_temp);
                dashboard.put(colVal, dashboard_temp);
            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";
            //System.out.println(dashboard);
            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    temp.put(key2, graphMap.get(key).get(key2));
                    graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }
            //System.out.println(dashboard);
            resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString() + " table/graph/map " + dashboard.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
        //return searchList;
        return resultset;

    }

    public static String getFinalResultUpazilaWise(String sql_view_type, String sql_method_type, String form_type, String zilla, String start_date, String end_date) {
        String graphString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(Integer.parseInt(zilla));

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> getGraphResult = new LinkedHashMap<String, String>();

        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject graphInfoTemp = null;
        String resultset = new String();
        JSONObject dashboard = new JSONObject();

        try {

            GetResult getResult = new GetResult(sql_view_type, sql_method_type, form_type, zilla, "0", start_date, end_date);
            finalResult = getResult.getSSAggrResultMap();
            getResult = null;

            upazilaSql = "select " + table_schema.getColumn("", "upazila_upazilanameeng") + ", " + table_schema.getColumn("", "upazila_upazilaid") + " from " + table_schema.getTable("table_upazila") + " where " + table_schema.getColumn("", "upazila_zillaid") + "=" + zilla;

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, upazilaSql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();

            String colVal = "";

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr><th>Upazila Name</th>";
            String table_body = "<tbody>";
            graphString = "";
            boolean active_db;
            while (rs.next()) {
                active_db = GetResultSet.check_active_db(zilla, rs.getString(metadata.getColumnName(2)));
                if (active_db) {
                    colVal = rs.getString(metadata.getColumnName(1));
                    getGraphResult = finalResult.get(zilla + "_" + rs.getString(metadata.getColumnName(2)));

                    //for newly implemented upazila where we dont get data(ex for 12_3 no data exists in finalresult)
                    if (!(getGraphResult == null)) {
                        temp = new JSONObject();
                        LinkedHashMap<String, String> dashboard_temp = new LinkedHashMap<String, String>();
                        table_body += "<tr><td>" + rs.getString(metadata.getColumnName(1)) + "</td>";

                        for (String key : getGraphResult.keySet()) {
                            if (!graphMap.containsKey(key)) {
                                graphMap.put(key, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                                table_header += "<th>" + key + "</th>";
                            }

                            dashboard_temp.put('"' + key + '"', '"' + getGraphResult.get(key) + '"');

                            temp.put(key, getGraphResult.get(key));
                            graphMap.get(key).put(rs.getString(metadata.getColumnName(1)), getGraphResult.get(key));
                            table_body += "<td>" + getGraphResult.get(key) + "</td>";
                        }

                        table_body += "</tr>";
                        mapInfo.put(zilla + "_" + rs.getString(metadata.getColumnName(2)), temp);
                        dashboard.put(colVal, dashboard_temp);
                    }
                }

            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";

            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    temp.put(key2, graphMap.get(key).get(key2));
                    graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }

            resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString() + " table/graph/map " + dashboard.toString();

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return resultset;

    }

    public static LinkedHashMap<String, String> getUpazilaResult(String sql, String zilla, String upazila) {

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        LinkedHashMap<String, String> upzGraphResult = new LinkedHashMap<String, String>();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();


            int j = 0;
            while (rs.next()) {

                for (int i = 1; i <= columnCount; i++) {
                    upzGraphResult.put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));

                }
            }
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return upzGraphResult;

    }

    public static String getFinalResultZillaWise(String sql_view_type, String sql_method_type, String form_type, String start_date, String end_date) {
        String graphString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        DBSchemaInfo table_schema = new DBSchemaInfo("36");

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject graphInfoTemp = null;
        String geoSql = "";
        String resultset = new String();

        try {
            LinkedHashMap<String, LinkedHashMap<String, String>> finalResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();
            LinkedHashMap<String, String> zilla_name = new LinkedHashMap<String, String>();
            int tempValue;
            ArrayList<String> zilla_array = new ArrayList<String>();
            ArrayList<String> div_array = new ArrayList<String>();
            LinkedHashMap<String, LinkedHashMap<Integer, String>> upazila_array = new LinkedHashMap<String, LinkedHashMap<Integer, String>>();
            String temp_zilla;

            DBListProperties setZillaList = new DBListProperties();
            Properties db_prop = setZillaList.setProperties();
            String[] zilla_list = db_prop.getProperty("zilla_all").split(",");

            geoSql = "select " + table_schema.getColumn("", "upazila_upazilanameeng") + ", " + table_schema.getColumn("table", "upazila_upazilaid") + ", " + table_schema.getColumn("table", "zilla_zillaid") + ", " + table_schema.getColumn("table", "zilla_zillanameeng") + ", " + table_schema.getColumn("table", "zilla_divid") + "  from " + table_schema.getTable("table_upazila") + " join " + table_schema.getTable("table_zilla") + " on " + table_schema.getColumn("table", "zilla_zillaid") + "=" + table_schema.getColumn("table", "upazila_zillaid") + " where " + table_schema.getColumn("table", "upazila_zillaid", zilla_list, "in") + " order by " + table_schema.getColumn("table", "upazila_zillaid") + " asc";

            System.out.println("GeoSQL query=" + geoSql);

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, geoSql);
            ResultSet rs = dbObject.getResultSet();

            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();

            ResultSetMetaData metadata = rs.getMetaData();

//			ExecutorService pool = Executors.newFixedThreadPool(rowCount);
//			List<Future<JSONObject>> threadResult = new ArrayList<Future<JSONObject>>();
//
//			Thread[] threads = new Thread[rowCount];

            long startTime = System.currentTimeMillis();

            int l = 0;
            boolean active_db;
//			GetSSSql getSSSql = new GetSSSql();
            while (rs.next()) {
                active_db = GetResultSet.check_active_db(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)));
                if (active_db) {
//					String sql = getSSSql.getSql(sql_view_type, sql_method_type, form_type, rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)), start_date, end_date);
//
//					threadResult.add(pool.submit(new GetThreadResult(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(2)), sql)));

                    if (!zilla_array.contains(rs.getString(metadata.getColumnName(3)))) {
                        l = 0;
                        zilla_array.add(rs.getString(metadata.getColumnName(3)));
                        div_array.add(rs.getString(metadata.getColumnName(3)) + "-" + rs.getString(metadata.getColumnName(5)) + "-" + rs.getString(metadata.getColumnName(4)));
                        zilla_name.put(rs.getString(metadata.getColumnName(3)), rs.getString(metadata.getColumnName(4)));
                        upazila_array.put(rs.getString(metadata.getColumnName(3)), new LinkedHashMap<Integer, String>());
                    }

                    upazila_array.get(rs.getString(metadata.getColumnName(3))).put(l, rs.getString(metadata.getColumnName(2)));
                    l++;
                }

            }
//			getSSSql = null;

//			pool.shutdown();
//			while (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
//				System.out.println("Awaiting completion of threads.");
//			}
//
//			JSONObject jsonResultSet = new JSONObject();
//			for(Future<JSONObject> result : threadResult){
//				JSONObject resultSet = new JSONObject(String.valueOf(result.get()));
//				Iterator<String> iterator = resultSet.keys();
//				while (iterator.hasNext()) {
//					String key = iterator.next();
//					jsonResultSet.put(key, resultSet.get(key));
//				}
//			}
//
//			System.out.println(jsonResultSet);
//
//			for(int m=0;m<zilla_list.length;m++){
//				finalResult.put(zilla_list[m], new LinkedHashMap<String, String>());
//				String[] upazila_list = db_prop.getProperty(zilla_list[m]).split(",");
//				for (int n = 0; n < upazila_list.length; n++) {
//					JSONObject upazilaResultSet = new JSONObject(jsonResultSet.getString(zilla_list[m]+"_"+upazila_list[n]));
//					Iterator<String> iterator = upazilaResultSet.keys();
//					while (iterator.hasNext()) {
//						String key = iterator.next();
//						if(n>0)
//							tempValue = Integer.parseInt(finalResult.get(zilla_list[m]).get(key))+Integer.parseInt(upazilaResultSet.getString(key));
//						else
//							tempValue = Integer.parseInt(upazilaResultSet.getString(key));
//						finalResult.get(zilla_list[m]).put(key, Integer.toString(tempValue));
//					}
//				}
//			}

            GetResult getResult = new GetResult(sql_view_type, sql_method_type, form_type, "0", "0", start_date, end_date);
            finalResult = getResult.getSSAggrResultMap();
            getResult = null;

            System.out.println("Final Result=" + finalResult);

            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;
            System.out.println(elapsedTime);

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr><th>Zilla Name</th>";
            String table_body = "<tbody>";
            for (String key : finalResult.keySet()) {
                temp = new JSONObject();
                table_body += "<tr><td>" + zilla_name.get(key) + "</td>";
                for (String key2 : finalResult.get(key).keySet()) {
                    if (!graphMap.containsKey(key2)) {
                        graphMap.put(key2, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                        table_header += "<th>" + key2 + "</th>";
                    }
                    temp.put(key2, finalResult.get(key).get(key2));
                    graphMap.get(key2).put(zilla_name.get(key), finalResult.get(key).get(key2));

                    table_body += "<td>" + finalResult.get(key).get(key2) + "</td>";
                }
                table_body += "</tr>";
                mapInfo.put(key, temp);
            }

            /***
             * @Author: evana
             * @Date: 03/07/2019
             * @Desc: Division seperation
             * *****/
            JSONObject divInfo = new JSONObject();
            String temp1 = "";

            for (int m = 0; m < div_array.size(); m++) {
                temp1 = div_array.get(m);
                divInfo.put(temp1, temp1);
                temp1 = "";
            }


            System.out.println("Mapinfo=" + mapInfo);
            System.out.println("Divinfo=" + divInfo);

            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";

            graphInfo = new JSONArray();
            for (String key : graphMap.keySet()) {

                temp = new JSONObject();
                graphInfoTemp = new JSONObject();

                graphString = graphString + "{type: \"stackedColumn\",";
                graphString = graphString + "name: \"" + key + "\",";
                graphString = graphString + "showInLegend: \"true\",";
                graphString = graphString + "dataPoints: [";
                for (String key2 : graphMap.get(key).keySet()) {
                    temp.put(key2, graphMap.get(key).get(key2));
                    graphString = graphString + "{ y: " + ((graphMap.get(key).get(key2) == null) ? "0" : graphMap.get(key).get(key2)) + ", label: \"" + key2 + "\"},";
                }
                graphString = graphString + "] },";

                graphInfoTemp.put(key, temp);
                graphInfo.put(graphInfoTemp);

            }

            System.out.println(graphInfo);
            //resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString();
            resultset = table + " table/graph/map " + graphInfo.toString() + " table/graph/map " + mapInfo.toString() + " table/graph/map " + divInfo.toString();

            System.out.println(resultset);
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultset;

    }

    public static Runnable run(String zilla, String upazila, String sql) {
        resultSetThread.put(zilla + "_" + upazila, getZillaResult(sql, zilla, upazila));
        return null;
    }

    public static LinkedHashMap<String, String> getZillaResult(String sql, String zilla, String upazila) {

        String start_date;
        String end_date;

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        JSONObject upzResult = new JSONObject();

        LinkedHashMap<String, String> resultSet = new LinkedHashMap<String, String>();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();


            int j = 0;
            while (rs.next()) {
                String getValue;
                for (int i = 1; i <= columnCount; i++) {
                    if (rs.getString(metadata.getColumnName(i)) != null && !rs.getString(metadata.getColumnName(i)).isEmpty())
                        getValue = rs.getString(metadata.getColumnName(i));
                    else
                        getValue = "0";

                    resultSet.put(metadata.getColumnName(i), getValue);
                }
            }
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultSet;

    }

    public static String getProviderResult(String sql, Integer zilla, Integer providerType) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = null;
        if (providerType != 4 && providerType != 5 && providerType != 6 && providerType != 17 && providerType != 101)
            dbObject = dbFacility.facilityDBInfo();
        else
            dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            for (int i = 1; i <= columnCount; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }
            resultString = resultString + "</tr></thead><tbody>";

            int j = 0;
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount; i++) {
                    resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                }
                resultString = resultString + "</tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    /**
     * @Author: Evana
     * @Date: 13/06/2019
     * @Description: Retrieve Facility Details List
     **/

    public static String getFacilityResult(String sql, Integer zilla, Integer upazila, String facilityType, boolean isActionColumnNeed) {
        String resultString = "";
        String facilityid = "";

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = null;

        dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\" overflow=\"hidden\"><thead><tr>";

            for (int i = 1; i <= columnCount; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }
            if (isActionColumnNeed) {
                resultString = resultString + "<th>Action</th>";
            }

            resultString = resultString + "</tr></thead><tbody>";

            int j = 0;
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount; i++) {
                    resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                }
                //resultString = resultString + "<td>--</td></tr>";
                //facilityid = (int) rs.getObject(metadata.getColumnName(1));
                facilityid = rs.getString(metadata.getColumnName(1));

                if (isActionColumnNeed) {
                    resultString = resultString + "<td><a title='Edit Facility Information' class='btn btn-primary' href='javascript:void(0)' onClick='editfacility( " + facilityid + "," + zilla + ");'><span class='glyphicon glyphicon-pencil'></span></a></td></tr>";
                }

            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    public static String getMIS3SubmissionList(String sql, String zilla) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(Integer.parseInt(zilla));

        DateFormatSymbols month_name = new DateFormatSymbols();

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap\" id=\"reportApprovalDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            resultString = resultString + "<th>Facility Name</th>" +
                    "<th>Year</th>" +
                    "<th>Month</th>" +
                    "<th>Submitted By</th>" +
                    "<th>Submitted Date</th>" +
                    "<th>Status</th>" +
                    "<th>View Details</th>";

            resultString = resultString + "</tr></thead><tbody>";
            int j = 0;
            while (rs.next()) {
                resultString = resultString + "<tr>";
                resultString = resultString + "<td>" + rs.getString("facility_name") + "</td>" +
                        "<td>" + rs.getString("report_year") + "</td>" +
                        "<td>" + month_name.getMonths()[Integer.parseInt(rs.getString("report_month")) - 1] + "</td>" +
                        "<td>" + rs.getString("submitted_by") + "</td>" +
                        "<td>" + rs.getString("submission_date") + "</td>" +
                        "<td>" + ((rs.getString("submission_status").equals("0")) ? "Waiting for Approval" : ((rs.getString("submission_status").equals("1")) ? "Waiting for DHIS2 submission" : "Re-submit after rejection")) + "</td>";
                if (rs.getString("submission_status").equals("1"))
                    resultString = resultString + "<td><button class='btn btn-warning' onclick='dhis2Submit(\"" + zilla + "\",\"" + rs.getString("upazilaid") + "\",\"" + rs.getString("facility_id") + "\",\"" + rs.getString("report_year") + "-" + rs.getString("report_month") + "\")'>DHIS2 Submit</button></td>";
                else
                    resultString = resultString + "<td><button class='btn btn-info' onclick='viewMIS3(\"" + zilla + "\",\"" + rs.getString("facility_id") + "\",\"" + rs.getString("report_year") + "-" + rs.getString("report_month") + "\")'>View Details</button></td>";
                resultString = resultString + "</tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    public static String getSatelitePlanningSubmissionList(String sql, String zilla, String upazila, String resultString) {

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

//			resultString = "<table class=\"table table-striped table-bordered nowrap\" id=\"reportApprovalDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";
//
//			resultString = resultString + "<th>Provider Name</th>" +
//					"<th>Facility Name</th>" +
//					"<th>Year</th>" +
//					"<th>Submitted Date</th>" +
//					"<th>Status</th>" +
//					"<th>View Details</th>";
//
//			resultString = resultString + "</tr></thead><tbody>";


            while (rs.next()) {
                resultString = resultString + "<tr>";
                resultString = resultString + "<td>" + rs.getString("provider_name") + "</td>" +
                        "<td>" + rs.getString("facility_name") + "</td>" +
                        "<td>" + rs.getString("planning_year") + "</td>" +
                        "<td>" + rs.getString("submit_date") + "</td>" +
                        "<td>" + ((rs.getString("status").equals("0") || rs.getString("status").equals("3")) ? "Submitted" : "Re-submit after rejection") + "</td>" +
                        "<td><button class='btn btn-info' onclick='viewSatelitePlanning(\"" + zilla + "\",\"" + upazila + "\",\"" + rs.getString("planning_id") + "\", \"" + rs.getString("planning_year") + "\")'>View Details</button></td>";
                resultString = resultString + "</tr>";
            }


            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    public static String getTableHeader() {
        String resultString = "";
        resultString = "<table class=\"table table-striped table-bordered nowrap\" id=\"reportApprovalDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

        resultString = resultString + "<th>Provider Name</th>" +
                "<th>Facility Name</th>" +
                "<th>Year</th>" +
                "<th>Submitted Date</th>" +
                "<th>Status</th>" +
                "<th>View Details</th>";
        resultString = resultString + "</tr></thead><tbody>";
        return resultString;

    }

    public static String getFinalResultDiseaseMonitoring(String reportViewLevel, String zilla, String upazila, String union, String start_date, String end_date, String year) {
        String graphString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = null;

        /**
         *  previously it extracted data from dist DB for dist view and upazila DB for upazila wise view
         *  Currently, we are getting all data from dist DB as we get all data updated from upazila DB by synchronization
         */

//        if (reportViewLevel.equals("2"))
        dbObject = dbFacility.facilityDistDBInfo(zilla);

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, LinkedHashMap<String, String>> tableHeader = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject graphInfoTemp = null;
        JSONArray tempItem = null;
        String resultset = new String();

        HashMap<String, String> monthList = new HashMap<String, String>();

//		monthList.put("Jan", "01");
//		monthList.put("Feb", "02");
//		monthList.put("Mar", "03");
//		monthList.put("Apr", "04");
//		monthList.put("May", "05");
//		monthList.put("Jun", "06");
//		monthList.put("Jul", "07");
//		monthList.put("Aug", "08");
//		monthList.put("Sep", "09");
//		monthList.put("Oct", "10");
//		monthList.put("Nov", "11");
//		monthList.put("Dec", "12");

        monthList.put("01", "Jan");
        monthList.put("02", "Feb");
        monthList.put("03", "Mar");
        monthList.put("04", "Apr");
        monthList.put("05", "May");
        monthList.put("06", "Jun");
        monthList.put("07", "Jul");
        monthList.put("08", "Aug");
        monthList.put("09", "Sep");
        monthList.put("10", "Oct");
        monthList.put("11", "Nov");
        monthList.put("12", "Dec");

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, DiseaseMonitoring.getSql(reportViewLevel, zilla, upazila, union, start_date, end_date));
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            String month_year = "";
            String itemName = "";
            String itemValue = "";
            int j = 1;

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr>";
            String table_body = "<tbody>";
            graphString = "";
            while (rs.next()) {
                month_year = rs.getString(metadata.getColumnName(2)) + "-" + rs.getString(metadata.getColumnName(1));
                itemName = rs.getString(metadata.getColumnName(3));
                itemValue = rs.getString(metadata.getColumnName(4));

                if (!graphMap.containsKey(itemName))
                    graphMap.put(itemName, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());

                graphMap.get(itemName).put(month_year, itemValue);

                for (int i = 1; i <= columnCount; i++) {
                    if (!tableHeader.containsKey(metadata.getColumnName(i)) && i <= columnCount) {
                        tableHeader.put(metadata.getColumnName(i), (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                        table_header += "<th>" + metadata.getColumnName(i) + "</th>";
                    }
                    if (i <= columnCount) {
                        table_body += "<td>" + rs.getString(metadata.getColumnName(i)) + "</td>";
                    }
                }
                table_body += "</tr>";
            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";

            graphInfo = new JSONArray();
            int month_num;
            if (Integer.parseInt(year) < CalendarDate.currentDayMonthYear("Year"))
                month_num = 12;
            else
                month_num = CalendarDate.currentDayMonthYear("Month") - 1;
            for (String key : graphMap.keySet()) {

                graphInfoTemp = new JSONObject();
                tempItem = new JSONArray();
                for (int m = 1; m <= month_num; m++) {
                    temp = new JSONObject();
                    month_year = monthList.get(String.format("%02d", m)) + "-" + year;
                    if (graphMap.get(key).containsKey(month_year)) {
                        temp.put("YearMonth", month_year);
                        temp.put("Num", graphMap.get(key).get(month_year));
                        //temp.put(month_year, graphMap.get(key).get(month_year));
                    } else {
                        temp.put("YearMonth", month_year);
                        temp.put("Num", "0");
                        //temp.put(month_year, "0");
                    }
                    tempItem.put(temp);
                }
//				for(String key2 : graphMap.get(key).keySet())
//				{
//					temp.put(key2, graphMap.get(key).get(key2));
//				}

                graphInfoTemp.put(key, tempItem);
                graphInfo.put(graphInfoTemp);

            }
            System.out.println(graphInfo);
            resultset = table + " table/graph " + graphInfo.toString();
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        System.out.println(resultset);
        return resultset;

    }

    public static JSONObject getFacilityInfoResult(String sql, String zilla) {
        JSONObject resultInfo = new JSONObject();
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        try {
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            String zilla_name = "";
            while (rs.next()) {
                if (zilla_name.equals(""))
                    zilla_name = rs.getString("zilla_name");

                resultInfo.put(rs.getString("facility_type"), rs.getString("total_facility"));
            }
            resultInfo.put("Name", zilla_name);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return resultInfo;
    }

    public static JSONObject getMIS3SubmissionInfo(String sql, String zilla) {
        JSONObject resultInfo = new JSONObject();
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        try {
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();
            String zilla_name = "";
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++)
                    resultInfo.put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return resultInfo;
    }

    public static String getMIS3SubmissionDetails(String sql, String zilla, String year, String month) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            for (int i = 1; i <= columnCount - 1; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }
            resultString = resultString + "<th>View MIS3</th>";
            resultString = resultString + "</tr></thead><tbody>";
            int j = 0;
            String status = "";
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount - 1; i++) {
                    if (metadata.getColumnName(i).equals("Status")) {
                        status = rs.getString(metadata.getColumnName(i));
                        if (status == null)
                            resultString = resultString + "<td><span class='label label-warning'>Not Submitted</span></td>";
                        else if (status.equals("10"))
                            resultString = resultString + "<td><span class='label label-warning'>Inactive Facility</span></td>";
//						else if(status.equals("1"))
//							resultString = resultString + "<td><span class='label label-warning'>DHIS2 Submit Remaining</span></td>";
                        else if (status.equals("0") || status.equals("3"))
                            resultString = resultString + "<td><span class='label label-primary'>Waiting For Approval</span></td>";
                        else if (status.equals("2"))
                            resultString = resultString + "<td><span class='label label-danger'>Rejected</span></td>";
                        else if (status.equals("4") || status.equals("1"))
                            resultString = resultString + "<td><span class='label label-success'>Approved</span></td>";
                    } else
                        resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                }
                if (status != null && rs.getString("Status").equals("10"))
                    resultString = resultString + "<td></td>";
                else
                    resultString = resultString + "<td><button class='btn btn-info' onclick='viewMIS3(\"" + zilla + "\",\"" + rs.getString("facility_id") + "\",\"" + year + "-" + month + "\")'>View Details</button></td>";
                resultString = resultString + "</tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

    public static boolean check_active_db(String zilla, String upazila) {
        boolean active = false;

        try {
            DBListProperties setZillaList = new DBListProperties();
            Properties db_prop = setZillaList.setProperties();

            String upazila_list = db_prop.getProperty(zilla);

            if (upazila_list.contains(upazila))
                active = true;
            else
                active = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return active;
    }


    //Shahed 20 Feb 2020 15:46
    public static String getmHealthResult(String sql) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.mHealthDBInfo();


        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            //Move to last record
            rs.last();

            //Count Total result found
            int rowCount = rs.getRow();

            //return to first row
            rs.beforeFirst();

            //Finding Meta data
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();


            String jsonString = "[";
            while (rs.next()) {
                String data = null;

                //Initiate row
                jsonString = jsonString + "{";

                for (int i = 1; i <= columnCount; i++) {

                    data = rs.getString(metadata.getColumnName(i));

                    //If not NUmber add double quote around the value
                    if (!isNumeric(data))
                        data = "\"" + data + "\"";

                    //Creating Json
                    jsonString = jsonString + "\"" + metadata.getColumnName(i) + "\":" + data + ",";

                }

                //Remove last comma
                jsonString = jsonString.substring(0, jsonString.length() - 1);

                //compliting one row
                jsonString = jsonString + "},";

            }

            //Remove last comma
            jsonString = jsonString.substring(0, jsonString.length() - 1);

            jsonString = jsonString + "]";


            //System.out.println(jsonString);


            //System.out.println("M health result");
            //resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            //for (int i = 1; i <= columnCount; i++) {
            //resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            //}
            //resultString = resultString + "</tr></thead><tbody>";
            //int j =0;
            //while(rs.next()){


//				j++;
//				if((j%2)==0){
//					resultString = resultString + "<tr>";
//				}
//				else{
//					resultString = resultString + "<tr>";
//				}
//
//				for (int i = 1; i <= columnCount; i++) {
//					resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i))==null)?"":rs.getString(metadata.getColumnName(i))) + "</td>";
//				}
//				resultString = resultString + "</tr>";
            //}

            //resultString = resultString + "</tbody></table>";

            //if(!rs.isClosed()){
            //rs.close();
            //}

            return jsonString;

            //System.out.println(resultString);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return null;
    }


    public static boolean isNumeric(final String str) {

        // null or empty
        if (str == null || str.length() == 0) {
            return false;
        }

        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;

    }

    public static String getFeverColdCoughTrend(String sql, String report1_graphViewType) {
        String graphString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        LinkedHashMap<String, LinkedHashMap<String, String>> graphMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, LinkedHashMap<String, String>> tableHeader = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        JSONArray graphInfo = null;
        JSONObject temp = null;
        JSONObject graphInfoTemp = null;
        JSONArray tempItem = null;
        String resultset = new String();

        HashMap<String, String> monthList = new HashMap<String, String>();

        monthList.put("01", "Jan");
        monthList.put("02", "Feb");
        monthList.put("03", "Mar");
        monthList.put("04", "Apr");
        monthList.put("05", "May");
        monthList.put("06", "Jun");
        monthList.put("07", "Jul");
        monthList.put("08", "Aug");
        monthList.put("09", "Sep");
        monthList.put("10", "Oct");
        monthList.put("11", "Nov");
        monthList.put("12", "Dec");

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            String day_week_month_year = "";
            String itemName = "";
            String itemValue = "";
            String year = "";
            int j = 1;

            JSONObject mapInfo = new JSONObject();
            String table = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\">";
            String table_header = "<thead><tr>";
            String table_body = "<tbody>";
            graphString = "";

            int c = 0;
            if (report1_graphViewType.equals("1"))
                c = 3;
            else if (report1_graphViewType.equals("2"))
                c = 4;
            else
                c = 2;

            while (rs.next()) {
                if (report1_graphViewType.equals("1")) {
                    day_week_month_year = rs.getString(metadata.getColumnName(2)) + "-" + rs.getString(metadata.getColumnName(1));
                    year = rs.getString(metadata.getColumnName(1));
                } else if (report1_graphViewType.equals("2")) {
                    day_week_month_year = rs.getString(metadata.getColumnName(3));
                } else {
                    day_week_month_year = rs.getString(metadata.getColumnName(1));
                }

                for (int i = 1; i <= columnCount; i++) {
                    if (i >= c) {
                        itemName = metadata.getColumnName(i);
                        itemValue = rs.getString(metadata.getColumnName(i));

                        if (!graphMap.containsKey(itemName))
                            graphMap.put(itemName, (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());

                        graphMap.get(itemName).put(day_week_month_year, itemValue);
                    }
                    if (!tableHeader.containsKey(metadata.getColumnName(i)) && i <= columnCount) {
                        tableHeader.put(metadata.getColumnName(i), (LinkedHashMap<String, String>) new LinkedHashMap<String, String>());
                        table_header += "<th>" + metadata.getColumnName(i) + "</th>";
                    }
                    if (i <= columnCount) {
                        table_body += "<td>" + (rs.getString(metadata.getColumnName(i)) != null ? rs.getString(metadata.getColumnName(i)) : "0") + "</td>";
                    }
                }
                table_body += "</tr>";
            }
            table += table_header + "</tr></thead>" + table_body + "</tbody></table>";

            graphInfo = new JSONArray();

            int month_num = 0;
            if (report1_graphViewType.equals("1")) {
                if (Integer.parseInt(year) < CalendarDate.currentDayMonthYear("Year"))
                    month_num = 12;
                else
                    month_num = CalendarDate.currentDayMonthYear("Month") - 1;
            }

            System.out.println(graphMap);
            for (String key : graphMap.keySet()) {

                graphInfoTemp = new JSONObject();
                tempItem = new JSONArray();
                if (report1_graphViewType.equals("1")) {
                    for (int m = 1; m <= month_num; m++) {
                        temp = new JSONObject();
                        day_week_month_year = monthList.get(String.format("%02d", m)) + "-" + year;
                        if (graphMap.get(key).containsKey(day_week_month_year)) {
                            temp.put("YearMonth", day_week_month_year);
                            temp.put("Num", graphMap.get(key).get(day_week_month_year));
                        } else {
                            temp.put("YearMonth", day_week_month_year);
                            temp.put("Num", "0");
                        }
                        tempItem.put(temp);
                    }
                } else {
                    for (String key2 : graphMap.get(key).keySet()) {
                        temp = new JSONObject();
                        temp.put("YearMonth", key2);
                        temp.put("Num", (graphMap.get(key).get(key2) != null ? graphMap.get(key).get(key2) : "0"));
                        tempItem.put(temp);
                    }
                }

                graphInfoTemp.put(key, tempItem);
                graphInfo.put(graphInfoTemp);

            }
            System.out.println(graphInfo);
            resultset = table + " table/graph " + graphInfo.toString();
            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        System.out.println(resultset);
        return resultset;

    }


    public static String get_sms_waiting(String sql) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.mHealthDBInfo();


        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            //Move to last record
            rs.last();

            //Count Total result found
            int rowCount = rs.getRow();


            //return to first row
            rs.beforeFirst();

            //Finding Meta data
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();


            String table_body = "";

            while (rs.next()) {
                table_body += "<tr>";
                table_body += "<td>" + rs.getString("requestid") + "</td>";
                table_body += "<td>" + rs.getString("eligible_service") + "</td>";
                table_body += "<td>" + rs.getString("zillanameeng") + "</td>";
                table_body += "<td>" + rs.getString("upazilanameeng") + "</td>";
                table_body += "<td>" + rs.getString("unionnameeng") + "</td>";
                table_body += "<td>" + rs.getString("entrydatetime") + "</td>";
                table_body += "<td>" + rs.getString("sms_type") + "</td>";
                table_body += "<td>" + rs.getString("status") + "</td>";
                table_body += "</tr>";

            }


            return table_body;


            //System.out.println(resultString);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return null;
    }

    public static String get_sms_group_count(String sql) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.mHealthDBInfo();


        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            //Move to last record
            rs.last();

            //Count Total result found
            int rowCount = rs.getRow();


            //return to first row
            rs.beforeFirst();

            //Finding Meta data
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();


            String table_body = "";

            while (rs.next()) {

                table_body += "<tr>";

                table_body += "<td>" + rs.getString("sms_type") + "</td>";
                table_body += "<td>" + rs.getString("count") + "</td>";
                table_body += "</tr>";

            }


            return table_body;


            //System.out.println(resultString);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return null;
    }


    public static String sms_location_wise_distribution(String sql) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.mHealthDBInfo();


        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            //Move to last record
            rs.last();

            //Count Total result found
            int rowCount = rs.getRow();


            //return to first row
            rs.beforeFirst();

            //Finding Meta data
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();


            String table_body = "";

            while (rs.next()) {

                table_body += "<tr>";

                table_body += "<td>" + rs.getString("zillanameeng") + "</td>";
                table_body += "<td>" + rs.getString("upazilanameeng") + "</td>";
                table_body += "<td>" + rs.getString("count") + "</td>";
                table_body += "<td>" + rs.getString("text") + "</td>";
                table_body += "<td>" + rs.getString("text_delivered") + "</td>";
                table_body += "<td>" + rs.getString("text_failed") + "</td>";
                table_body += "<td>" + rs.getString("voice") + "</td>";
                table_body += "<td>" + rs.getString("voice_delivered") + "</td>";
                table_body += "<td>" + rs.getString("voice_failed") + "</td>";
                table_body += "</tr>";

            }


            return table_body;


            //System.out.println(resultString);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return null;
    }

    public static String sms_service_wise_distribution(String sql) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.mHealthDBInfo();


        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();

            //Move to last record
            rs.last();

            //Count Total result found
            int rowCount = rs.getRow();


            //return to first row
            rs.beforeFirst();

            //Finding Meta data
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            String thead = "";

            String table_body = "";

            while (rs.next()) {
                thead += "<th>" + rs.getString("eligible_service") + "</th>";
                table_body += "<td>" + rs.getString("count") + "</td>";
            }

            String table = "";
            table += "<thead><tr>" + thead.toUpperCase() + "</tr></thead>";
            table += "<tbody><tr>" + table_body + "</tr></tbody>";
            table += "";


            return table;


            //System.out.println(resultString);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }

        return null;
    }


    public static String getFWAResult(String sql, Integer zilla, Integer upazila, String version_no, int communityActive, Boolean isActionColumnNeed) {
        String resultString = "";
        String providerid = "";
        String union = "";

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = null;

        dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\" overflow=\"hidden\"><thead><tr>";

            for (int i = 1; i <= columnCount; i++) {
                if (!metadata.getColumnName(i).equals("unionid")) {
                    resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
                }
            }
            if (isActionColumnNeed) {
                resultString = resultString + "<th>Action</th>";
            }

            resultString = resultString + "</tr></thead><tbody>";

            int j = 0;
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount; i++) {
                    if (!metadata.getColumnName(i).equals("unionid")) {
                        resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                    }
                }
                providerid = rs.getString("providerid");
                union = rs.getString("unionid");

                if (isActionColumnNeed) {
                    resultString = resultString + "<td><a title='Make CSBA' class='btn btn-primary' href='javascript:void(0)' onClick='addCSBA( " + zilla + "," + upazila + "," + union + "," + providerid + "," + version_no + "," + communityActive + ");'>Make CSBA</a></td></tr>";
                }
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }

}
