package org.sci.rhis.report;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

public class MIS3SubmitInfo {
	
	public static boolean submitMIS3(JSONObject reportCred){

		boolean status = false;
		
		Integer zilla = Integer.parseInt(reportCred.getString("report1_zilla"));
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

		DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));
		
		try{
			if(reportCred.getString("facilityId").equals("") || reportCred.getString("facilityId").equals("null")) {
				String facility_info = "select " + table_schema.getColumn("", "facility_provider_facility_id") + " as facility_id from " + table_schema.getTable("table_facility_provider") + " where " + table_schema.getColumn("", "facility_provider_provider_id", new String[]{reportCred.getString("providerId")}, "=")+" and "+table_schema.getColumn("", "facility_provider_is_active",new String[]{"1"},"=");
				System.out.println(facility_info);
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject, facility_info);
				ResultSet rs = dbObject.getResultSet();

				while (rs.next()) {
					reportCred.put("facilityId", rs.getString("facility_id"));
				}
			}

			String facility_id = reportCred.getString("facilityId");
			String[] report_date = (reportCred.getString("report1_start_date")).split("-");
			String submitted_by = "";
			if(reportCred.has("providerId"))
				submitted_by = reportCred.getString("providerId");
			String report_year = report_date[0];
			String report_month = report_date[1];
			String submission_status = "";
			if(reportCred.has("submissionStatus"))
				submission_status = reportCred.getString("submissionStatus");
			String supervisor_comment = "";
			if(submission_status.equals("1") || submission_status.equals("2"))
				supervisor_comment = reportCred.getString("supervisorComment");
			
			String sql = "";
			if(submission_status.equals("1") || submission_status.equals("2") || submission_status.equals("3") || submission_status.equals("4")) {
				String sql_supervisor_comment = "";
				if(submission_status.equals("1") || submission_status.equals("2"))
					sql_supervisor_comment = table_schema.getColumn("","mis3_report_submission_supervisor_comment")+"='"+supervisor_comment+"',";
				else
					sql_supervisor_comment = "";

				sql = "update " + table_schema.getTable("table_mis3_report_submission") + " set "+table_schema.getColumn("","mis3_report_submission_submission_status")+"="+submission_status+", "+sql_supervisor_comment+" "+table_schema.getColumn("","mis3_report_submission_approval_date")+"=CURRENT_TIMESTAMP "
							+ "where "+table_schema.getColumn("","mis3_report_submission_facility_id")+"="+facility_id+" and "+table_schema.getColumn("","mis3_report_submission_report_year")+"="+report_year+" and "+table_schema.getColumn("","mis3_report_submission_report_month")+"="+report_month;
				
				dbOp.dbExecuteUpdate(dbObject,sql);
			}
			else {
				String submission_date;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            Date current_date = sdf.parse(sdf.format(new Date()));
	            System.out.println((String) sdf.format(current_date));
	            Date last_date = null;
	            if(report_month.equals("12")){
	            	last_date = sdf.parse((Integer.parseInt(report_year)+1)+"-"+(Integer.parseInt(report_month)+1)+"-07 23:59:59");
	            }
	            else {
	            	last_date = sdf.parse(report_year+"-"+(Integer.parseInt(report_month)+1)+"-07 23:59:59");
	            }
	            System.out.println((String) sdf.format(last_date));	
	            if(current_date.after(last_date))
	            	submission_date = sdf.format(last_date);
	            else
	            	submission_date = sdf.format(current_date);
	            
				sql = "insert into " + table_schema.getTable("table_mis3_report_submission") + " ("+table_schema.getColumn("","mis3_report_submission_facility_id")+","+table_schema.getColumn("","mis3_report_submission_report_year")+","+table_schema.getColumn("","mis3_report_submission_report_month")+","+table_schema.getColumn("","mis3_report_submission_submitted_by")+","+table_schema.getColumn("","mis3_report_submission_submission_date")+","+table_schema.getColumn("","mis3_report_submission_submission_status")+") "
							+ "values("+facility_id+","+report_year+","+report_month+","+submitted_by+",'"+submission_date+"',0)";
				
				dbOp.dbStatementExecute(dbObject,sql);
			}
			
			System.out.println(sql);
			
			status = true;
			
		}
		catch(Exception e){
            dbObject = new FacilityDB().facilityDBInfo();
            String error_sql = "insert into mis3_submission_error (request_json, error_text) values ('"+reportCred+"', '"+e+"')";
            dbOp.dbStatementExecute(dbObject,error_sql);
			System.out.println(e);
			status = false;
		}
		finally{
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		
		return status;
	}

	public static String getApprovalList(JSONObject infoApproval){
		String zilla = "";
		String[] upazilas = null;
		String approvalList = "";

		DBSchemaInfo table_schema = new DBSchemaInfo("36");

		String supervisorId = infoApproval.getString("supervisorId");
		String monthFIlter = "";
		if(infoApproval.has("month")) {
			String[] yearMonth = infoApproval.getString("month").split("-");
			monthFIlter = "and " + table_schema.getColumn("", "mis3_report_submission_report_year",new String[]{yearMonth[0]},"=") + " and " + table_schema.getColumn("", "mis3_report_submission_report_month",new String[]{yearMonth[1]},"=");
		}

		try {
			DBOperation dbOp = new DBOperation();
			DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();

			String sql_supervisorInfo = "select " + table_schema.getColumn("", "mis3_approver_approval_zilla") + " as approval_zilla, " + table_schema.getColumn("", "mis3_approver_approval_upazilas") + " as approval_upazilas from " + table_schema.getTable("table_mis3_approver") + " where " + table_schema.getColumn("", "mis3_approver_active",new String[]{"1"},"=") + " and " + table_schema.getColumn("", "mis3_approver_providerid",new String[]{supervisorId},"=");

			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject, sql_supervisorInfo);
			ResultSet rs = dbObject.getResultSet();
			while (rs.next()){
				zilla = rs.getString("approval_zilla");
				upazilas = rs.getString("approval_upazilas").replaceAll("[^a-zA-Z0-9\\,]", "").split(",");
			}

			if (!rs.isClosed()) {
				rs.close();
			}

			String sql_submissionInfo = "select "+table_schema.getColumn("", "mis3_report_submission_facility_id")+" as facility_id, "+table_schema.getColumn("", "mis3_report_submission_report_year")+" as report_year, " +
										table_schema.getColumn("", "mis3_report_submission_report_month")+" as report_month, "+table_schema.getColumn("", "mis3_report_submission_submission_date")+" as submission_date, " +
										table_schema.getColumn("", "facility_registry_facility_name")+" as facility_name, "+table_schema.getColumn("", "providerdb_provname")+" as submitted_by, " +
										table_schema.getColumn("", "mis3_report_submission_submission_status")+" as submission_status, " +table_schema.getColumn("table", "providerdb_upazilaid")+" as upazilaid " +
										"from "+ table_schema.getTable("table_mis3_report_submission") +" join "+ table_schema.getTable("table_facility_registry") +" on "+table_schema.getColumn("table", "facility_registry_facilityid")+"="+table_schema.getColumn("table", "mis3_report_submission_facility_id")+" " +
										"left join "+ table_schema.getTable("table_associated_provider_id") +" on "+table_schema.getColumn("table", "associated_provider_id_associated_id")+"="+table_schema.getColumn("table", "mis3_report_submission_submitted_by")+" "+
										"join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "mis3_report_submission_submitted_by")+" or " +table_schema.getColumn("table", "providerdb_provcode")+"="+table_schema.getColumn("table", "associated_provider_id_provider_id")+" "+
										"where "+table_schema.getColumn("table", "mis3_report_submission_submission_status",new String[]{"0","1","3"},"in")+" " +
										"and "+table_schema.getColumn("table","facility_registry_zillaid",new String[]{zilla},"=")+" and "+table_schema.getColumn("table","facility_registry_upazilaid",upazilas,"in")+" and "+table_schema.getColumn("table","providerdb_active",new String[]{"1"},"=") + " " +
										monthFIlter;
			System.out.println(sql_submissionInfo);
			approvalList = GetResultSet.getMIS3SubmissionList(sql_submissionInfo,zilla);
		}
		catch (Exception e){
			e.printStackTrace();
		}

		return  approvalList;
	}

	public static boolean mis3DataEntry(JSONObject entryInfo){
		boolean status = false;

		byte[] valueDecoded = Base64.getDecoder().decode(entryInfo.getString("params"));
		String getValue = new String(valueDecoded);//StringUtils.newStringUtf8(Base64.decodeBase64(request.getQueryString()));

		System.out.println(getValue);

		String[] parameter = getValue.split("&");

		JSONObject reportCred = new JSONObject();

		for(int i=0;i<parameter.length;i++)
		{

			String[] parameterValue = parameter[i].split("=");
			if(parameterValue.length>1)
				reportCred.put(parameterValue[0], parameterValue[1]);
			else
				reportCred.put(parameterValue[0], "");

			//System.out.println(parameterValue[1]);
		}

		Integer zilla = Integer.parseInt(reportCred.getString("report1_zilla"));

		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

		DBSchemaInfo table_schema = new DBSchemaInfo(Integer.toString(zilla));

		try{
			if(reportCred.getString("facilityId").equals("") || reportCred.getString("facilityId").equals("null")) {
				String facility_info = "select " + table_schema.getColumn("", "facility_provider_facility_id") + " as facility_id from " + table_schema.getTable("table_facility_provider") + " where " + table_schema.getColumn("", "facility_provider_provider_id", new String[]{reportCred.getString("providerId")}, "=")+" and "+table_schema.getColumn("", "facility_provider_is_active",new String[]{"1"},"=");
				System.out.println(facility_info);
				dbObject = dbOp.dbCreateStatement(dbObject);
				dbObject = dbOp.dbExecute(dbObject, facility_info);
				ResultSet rs = dbObject.getResultSet();

				while (rs.next()) {
					reportCred.put("facilityId", rs.getString("facility_id"));
				}
			}

			String facility_id = reportCred.getString("facilityId");
			String[] report_date = (reportCred.getString("report1_start_date")).split("-");
			String report_year = report_date[0];
			String report_month = report_date[1];

			String sql_delete_existing_data = "delete from " + table_schema.getTable("table_mis3_report_submission_data") + " where "+table_schema.getColumn("table","mis3_report_submission_data_facility_id",new String[]{facility_id},"=")+" and "+table_schema.getColumn("table","mis3_report_submission_data_report_month",new String[]{report_month},"=")+" and "+table_schema.getColumn("table","mis3_report_submission_data_report_year",new String[]{report_year},"=");
			dbOp.dbExecuteUpdate(dbObject, sql_delete_existing_data);

			JSONObject entryData = new JSONObject(entryInfo.getString("entryData"));
			JSONArray keys = entryData.names ();
			for (int i = 0; i < keys.length (); ++i) {
				String sql_insert = "insert into "+ table_schema.getTable("table_mis3_report_submission_data") +" ("+table_schema.getColumn("","mis3_report_submission_data_facility_id")+", "+table_schema.getColumn("","mis3_report_submission_data_report_month")+"," +
									""+table_schema.getColumn("","mis3_report_submission_data_report_year")+","+table_schema.getColumn("","mis3_report_submission_data_field_name")+","+table_schema.getColumn("","mis3_report_submission_data_field_value")+") " +
									"values("+facility_id+", "+report_month+","+report_year+",'"+keys.getString (i)+"',"+((entryData.getString(keys.getString (i)).equals(""))?0:entryData.getString(keys.getString (i)))+")";

				System.out.println(sql_insert);

				dbOp.dbStatementExecute(dbObject, sql_insert);
			}
		}
		catch(Exception e){
			System.out.println(e);
			status = false;
		}
		finally{
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}

		return status;
	}

	public static String MIS3SubmisssionInfo(JSONObject reportCred){
		JSONObject mis3SubmissionInfo = new JSONObject();

		DBSchemaInfo table_schema = new DBSchemaInfo();

		String[] report_month_year = reportCred.getString("report1_month").split("-");
		int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(report_month_year[0]), Integer.parseInt(report_month_year[1]));
		String report_month_start_date = report_month_year[0]+"-"+report_month_year[1]+"-01";
		String report_month_end_date = report_month_year[0]+"-"+report_month_year[1]+"-"+daysInMonth;
		String zilla = reportCred.getString("report1_zilla");
		String upazila = null;
		if(!reportCred.getString("report1_upazila").equals(""))
			upazila = reportCred.getString("report1_upazila");

		String inactive_facility_query = "select "+table_schema.getColumn("","facility_provider_facility_id")+" from " +
				"(select distinct("+table_schema.getColumn("","facility_provider_facility_id")+") as facility_id " +
				"from "+ table_schema.getTable("table_facility_provider") +" " +
				"join "+ table_schema.getTable("table_facility_registry") +" on "+table_schema.getColumn("table","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
				"left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" and "+table_schema.getColumn("table","providerdb_endate")+"::date="+table_schema.getColumn("table","facility_provider_start_date")+"::date and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","101","17"},"in")+" " +
				"left join "+ table_schema.getTable("table_associated_provider_id") +" on "+table_schema.getColumn("table","associated_provider_id_associated_id")+"="+table_schema.getColumn("table","facility_provider_provider_id")+" "+
				"left join "+ table_schema.getTable("table_provider_leave_status") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","provider_leave_status_providerid")+" or "+table_schema.getColumn("table","provider_leave_status_providerid")+"="+table_schema.getColumn("table","associated_provider_id_provider_id")+" " +
				"where ("+table_schema.getColumn("","facility_provider_is_active",new String[]{"2"},"=")+" and "+table_schema.getColumn("table","facility_provider_end_date")+"<='"+report_month_end_date+"') " +
				"or ("+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+" and "+table_schema.getColumn("table","facility_provider_start_date")+">'"+report_month_end_date+"') " +
				"or ("+table_schema.getColumn("table","provider_leave_status_providerid")+" is not null and ("+table_schema.getColumn("table","provider_leave_status_leave_end_date")+" is null or "+table_schema.getColumn("table","provider_leave_status_leave_end_date")+">'"+report_month_end_date+"')) " +
				"and ("+table_schema.getColumn("table", "providerdb_provcode",new String[]{},"isnotnull")+" or "+table_schema.getColumn("table", "associated_provider_id_associated_id",new String[]{},"isnotnull")+")) as inactive_provider " +
				"left join " +
				"(select distinct("+table_schema.getColumn("","facility_provider_facility_id")+") as facility_id  " +
				"from "+ table_schema.getTable("table_facility_provider") +" " +
				"join "+ table_schema.getTable("table_facility_registry") +" on "+table_schema.getColumn("table","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
				"left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" and "+table_schema.getColumn("table","providerdb_endate")+"::date="+table_schema.getColumn("table","facility_provider_start_date")+"::date and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","101","17"},"in")+" " +
				"left join "+ table_schema.getTable("table_associated_provider_id") +" on "+table_schema.getColumn("table","associated_provider_id_associated_id")+"="+table_schema.getColumn("table","facility_provider_provider_id")+" "+
				"left join "+ table_schema.getTable("table_provider_leave_status") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","provider_leave_status_providerid")+" or "+table_schema.getColumn("table","provider_leave_status_providerid")+"="+table_schema.getColumn("table","associated_provider_id_provider_id")+" " +
				"where (("+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+" and "+table_schema.getColumn("table","facility_provider_start_date")+"<='"+report_month_end_date+"') or ("+table_schema.getColumn("","facility_provider_is_active",new String[]{"2"},"=")+" and "+table_schema.getColumn("table","facility_provider_end_date")+">'"+report_month_end_date+"'))  " +
				"and (("+table_schema.getColumn("table","provider_leave_status_providerid")+" is not null and "+table_schema.getColumn("table","provider_leave_status_leave_end_date")+"<='"+report_month_end_date+"') or "+table_schema.getColumn("table","provider_leave_status_leave_end_date")+" is null) " +
				"and ("+table_schema.getColumn("table", "providerdb_provcode",new String[]{},"isnotnull")+" or "+table_schema.getColumn("table", "associated_provider_id_associated_id",new String[]{},"isnotnull")+")) as active_provider using("+table_schema.getColumn("","facility_provider_facility_id")+") " +
				"where active_provider.facility_id is null";

		try {
			String sql = "select count(*) as total_facility, " +
					"(count(*)-count(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 1 else null end)) as active_facility, "+
					"count(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 1 else null end) as inactive_facility, "+
					"count("+table_schema.getColumn("table","mis3_report_submission_facility_id")+") as submited, " +
					"((count(*)-count(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 1 else null end))-count("+table_schema.getColumn("table","mis3_report_submission_facility_id")+")) as not_submited, " +
					"count(case when "+table_schema.getColumn("","mis3_report_submission_submission_status",new String[]{"1", "4"},"in")+" then 1 else null end) as approved, " +
					"count(case when "+table_schema.getColumn("","mis3_report_submission_submission_status",new String[]{"0","3"},"in")+" then 1 else null end) as approve_waiting, " +
					"count(case when "+table_schema.getColumn("","mis3_report_submission_submission_status",new String[]{"2"},"=")+" then 1 else null end) as rejected, " +
					"count(case when "+table_schema.getColumn("","mis3_report_submission_submission_status",new String[]{"1"},"=")+" then 1 else null end) as dhis2_submit " +
					"from "+ table_schema.getTable("table_facility_registry") +" " +
					"join ( select distinct("+table_schema.getColumn("table","facility_provider_facility_id")+") as facility_id from "
                   + table_schema.getTable("table_facility_provider") +" " +"join"+ table_schema.getTable("table_associated_provider_id")+" on"+table_schema.getColumn("table","associated_provider_id_associated_id")+"="+table_schema.getColumn("table","facility_provider_provider_id")+" " +
                   "where"+table_schema.getColumn("table","facility_provider_start_date")+"<='"+report_month_end_date+"' " +
					"union select distinct("+table_schema.getColumn("table","facility_provider_facility_id")+") as facility_id from "+ table_schema.getTable("table_facility_provider") +" " +
					"join "+ table_schema.getTable("table_providerdb") +" using("+table_schema.getColumn("","providerdb_provcode")+") " +
					"where "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","101","17"},"in")+" " +
					"and "+table_schema.getColumn("table","facility_provider_start_date")+" < '"+report_month_end_date+"') "+
					"as fp on "+table_schema.getColumn("fp","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
					"left join("+inactive_facility_query+") inactive_facility using ("+table_schema.getColumn("","facility_provider_facility_id")+") " +
					"left join "+ table_schema.getTable("table_mis3_report_submission") +" on "+table_schema.getColumn("table","facility_registry_facilityid")+"="+table_schema.getColumn("table","mis3_report_submission_facility_id")+" " +
					"and "+table_schema.getColumn("","mis3_report_submission_report_month",new String[]{report_month_year[1]},"=")+" and "+table_schema.getColumn("","mis3_report_submission_report_year",new String[]{report_month_year[0]},"=")+" " +
					"where "+table_schema.getColumn("","facility_registry_zillaid",new String[]{zilla},"=")+" ";

			if(!reportCred.getString("report1_upazila").equals(""))
				sql += "and "+table_schema.getColumn("","facility_registry_upazilaid",new String[]{upazila},"=")+" ";

//			sql += "and "+table_schema.getColumn("table","facility_registry_facilityid")+" in " +
//					"(select distinct("+table_schema.getColumn("","facility_provider_facility_id")+") from "+ table_schema.getTable("table_facility_provider") +" where "+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+")";

			System.out.println(sql);
			mis3SubmissionInfo = mis3SubmissionInfo.put("Aggregated", GetResultSet.getMIS3SubmissionInfo(sql, zilla));

			String sql_all_facility = "select "+table_schema.getColumn("","facility_registry_facility_name")+" as \"Facility Name\", initcap("+table_schema.getColumn("","upazila_upazilanameeng")+") as \"Upazila Name\", " +
					"initcap("+table_schema.getColumn("","unions_unionnameeng")+") as \"Union Name\", " +
					"(case when "+table_schema.getColumn("ap","facility_provider_facility_id")+" is not null and providers is not null then providers||', '|| assoc_providers " +
					"when "+table_schema.getColumn("ap","facility_provider_facility_id")+" is not null and providers is null then assoc_providers else providers end) as \"Provider Details\", " +
					"(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 10 else "+table_schema.getColumn("","mis3_report_submission_submission_status")+" end) as \"Status\", " +
					table_schema.getColumn("table","facility_registry_facilityid") +" as facility_id " +
					"from "+ table_schema.getTable("table_facility_registry") +" " +
					"left join (" +
					"select "+table_schema.getColumn("table","facility_provider_facility_id")+", " +
					"string_agg((case when (("+table_schema.getColumn("table","facility_provider_end_date")+" is null and "+table_schema.getColumn("table","providerdb_exdate")+" is null) " +
					"or ("+table_schema.getColumn("table","facility_provider_end_date")+">'"+report_month_start_date+"' and "+table_schema.getColumn("table","providerdb_exdate")+">'"+report_month_start_date+"')) "+
					"then initcap("+table_schema.getColumn("table","providerdb_provname")+") || '(' || "+table_schema.getColumn("table","providerdb_mobileno")+" || ')' else null end), ', ') as providers " +
					"from "+ table_schema.getTable("table_facility_provider") +" " +
					"join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" and "+table_schema.getColumn("table","providerdb_endate")+"::date="+table_schema.getColumn("table","facility_provider_start_date")+"::date and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","101","17"},"in")+" " +
					"where "+table_schema.getColumn("table","facility_provider_start_date")+" < '"+report_month_end_date+"' "+
//					"and (("+table_schema.getColumn("table","facility_provider_end_date")+" is null and "+table_schema.getColumn("table","providerdb_exdate")+" is null) " +
//					"or ("+table_schema.getColumn("table","facility_provider_end_date")+">'"+report_month_start_date+"' and "+table_schema.getColumn("table","providerdb_exdate")+">'"+report_month_start_date+"')) "+
					"group by "+table_schema.getColumn("table","facility_provider_facility_id")+" " +
					") fp on "+table_schema.getColumn("fp","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
					"left join (select "+table_schema.getColumn("","facility_provider_facility_id")+", "+"string_agg ( distinct("+table_schema.getColumn("table","providerdb_provname")+" || '(' || "+table_schema.getColumn("table","providerdb_mobileno")+" || ')'), ',')as assoc_providers "+
					"from "+ table_schema.getTable("table_facility_provider") +" " +
					"join "+ table_schema.getTable("table_associated_provider_id") +" on "+table_schema.getColumn("table","associated_provider_id_associated_id")+"="+table_schema.getColumn("table","facility_provider_provider_id")+" " +
					"join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","associated_provider_id_provider_id")+" " +
					"where (("+table_schema.getColumn("","facility_provider_is_active",new String[]{"2"},"=")+" and "+table_schema.getColumn("table","facility_provider_end_date")+">'"+report_month_end_date+"') " +
					"or ("+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+" and "+table_schema.getColumn("table","facility_provider_start_date")+"<='"+report_month_end_date+"')) " +
					"and "+table_schema.getColumn("","providerdb_active",new String[]{"1"},"=")+ " " +
					"group by "+table_schema.getColumn("table","facility_provider_facility_id")+" " +
					") ap on "+table_schema.getColumn("ap","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" "+
					"left join("+inactive_facility_query+") inactive_facility on "+table_schema.getColumn("inactive_facility","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" "+
					"left join (" +
					"select "+table_schema.getColumn("","mis3_report_submission_facility_id")+", "+table_schema.getColumn("","mis3_report_submission_submission_status")+" " +
					"from "+ table_schema.getTable("table_mis3_report_submission") +" " +
					"where "+table_schema.getColumn("","mis3_report_submission_report_year",new String[]{report_month_year[0]},"=")+" and "+table_schema.getColumn("","mis3_report_submission_report_month",new String[]{report_month_year[1]},"=")+" " +
					") mrs on "+table_schema.getColumn("mrs","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
					"left join "+ table_schema.getTable("table_upazila") +" on "+table_schema.getColumn("table","upazila_zillaid")+"="+table_schema.getColumn("table","facility_registry_zillaid")+" and "+table_schema.getColumn("table","upazila_upazilaid")+"="+table_schema.getColumn("table","facility_registry_upazilaid")+" " +
					"left join "+ table_schema.getTable("table_unions") +" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","facility_registry_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","facility_registry_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","facility_registry_unionid")+" " +
					"where "+table_schema.getColumn("table","facility_registry_zillaid",new String[]{zilla},"=")+" "
					+"and (providers is not null or assoc_providers is not null )";
			if(!reportCred.getString("report1_upazila").equals(""))
				sql_all_facility += "and "+table_schema.getColumn("table","facility_registry_upazilaid",new String[]{upazila},"=");

			System.out.println(sql_all_facility);
			mis3SubmissionInfo = mis3SubmissionInfo.put("Individual", GetResultSet.getMIS3SubmissionDetails(sql_all_facility, zilla, report_month_year[0], report_month_year[1]));
		}
		catch (Exception e){
			e.printStackTrace();
		}

		return mis3SubmissionInfo.toString();
	}

}
