package org.sci.rhis.report;

import org.json.JSONObject;

public class MIS3ReportTable {

    public static String getFacilityTable(JSONObject mis3_json, String mis3ViewType){
        String resultString = "";
        String anc1text="", anc2text="", anc3text="", anc4text="", pncm1text="", pncm2text="", pncm3text="", pncm4text="", pncn1text="", pncn2text="", pncn3text="", pncn4text="";
        try {
            if(mis3ViewType.equals("2")) {
                anc1text = "পরিদর্শন - ১ (৪ মাসের মধ্যে)";
                anc2text = "পরিদর্শন - ২ (৬ মাসের মধ্যে)";
                anc3text = "পরিদর্শন - ৩ (৮ মাসের মধ্যে)";
                anc4text = "পরিদর্শন - ৪ (৯ মাসে)";

                pncm1text = "পরিদর্শন - ১ (২৪ ঘন্টার মধ্যে)";
                pncm2text = "পরিদর্শন - ২ (২-৩ দিনের মধ্যে)";
                pncm3text = "পরিদর্শন - ৩ (৭-১৪ দিনের মধ্যে)";
                pncm4text = "পরিদর্শন - ৪ (৪২-৪৮ দিনের মধ্যে)";

                pncn1text = "পরিদর্শন - ১ (২৪ ঘন্টার মধ্যে)";
                pncn2text = "পরিদর্শন - ২ (২-৩ দিনের মধ্যে)";
                pncn3text = "পরিদর্শন - ৩ (৭-১৪ দিনের মধ্যে)";
                pncn4text = "পরিদর্শন - ৪ (৪২-৪৮ দিনের মধ্যে)";
            }
            else {
                anc1text = "পরিদর্শন - ১";
                anc2text = "পরিদর্শন - ২";
                anc3text = "পরিদর্শন - ৩";
                anc4text = "পরিদর্শন - ৪";

                pncm1text = "পরিদর্শন - ১";
                pncm2text = "পরিদর্শন - ২";
                pncm3text = "পরিদর্শন - ৩";
                pncm4text = "পরিদর্শন - ৪";

                pncn1text = "পরিদর্শন - ১";
                pncn2text = "পরিদর্শন - ২";
                pncn3text = "পরিদর্শন - ৩";
                pncn4text = "পরিদর্শন - ৪";
            }

            resultString += "<table class=\"table page_1 table_hide\" id=\"table_1\" width=\"700\" border=\"1\" cellspacing=\"0\" style='font-size:11px;'><tr class='danger'>"
                    + "<td style='text-align: center;vertical-align: middle;border-top: 1px solid black;'></td>"
                    + "<td colspan='4' style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>সেবার ধরণ</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মোট</td>"
                    + "</tr>";

            resultString += "<tr class='danger'><td rowspan='30' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>পরিবার পরিকল্পনা পদ্ধতি</sapn></td></tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='3' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>খাবার বড়ি</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>পুরাতন</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pill_old_center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pill_old_satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("pill_old_center")) + Integer.parseInt(mis3_json.getString("pill_old_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>নতুন</td>"
                    + "<td align='center'>" + mis3_json.getString("pill_new_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pill_new_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pill_new_center")) + Integer.parseInt(mis3_json.getString("pill_new_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>মোট</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pill_new_center")) + Integer.parseInt(mis3_json.getString("pill_old_center"))) + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pill_new_satelite")) + Integer.parseInt(mis3_json.getString("pill_old_satelite"))) + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pill_new_center")) + Integer.parseInt(mis3_json.getString("pill_old_center")) + Integer.parseInt(mis3_json.getString("pill_new_satelite")) + Integer.parseInt(mis3_json.getString("pill_old_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='3' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>কনডম</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>পুরাতন</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("condom_old_center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("condom_old_satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("condom_old_center")) + Integer.parseInt(mis3_json.getString("condom_old_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>নতুন</td>"
                    + "<td align='center'>" + mis3_json.getString("condom_new_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("condom_new_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("condom_new_center")) + Integer.parseInt(mis3_json.getString("condom_new_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>মোট</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("condom_new_center")) + Integer.parseInt(mis3_json.getString("condom_old_center"))) + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("condom_new_satelite")) + Integer.parseInt(mis3_json.getString("condom_old_satelite"))) + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("condom_new_center")) + Integer.parseInt(mis3_json.getString("condom_old_center")) + Integer.parseInt(mis3_json.getString("condom_new_satelite")) + Integer.parseInt(mis3_json.getString("condom_old_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='5' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইনজেকটেবল</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>নতুন</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("new_inject_center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("new_inject_satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("new_inject_center")) + Integer.parseInt(mis3_json.getString("new_inject_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>পরবর্তী ডোজ</td>"
                    + "<td align='center'>" + mis3_json.getString("old_inject_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("old_inject_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("old_inject_center")) + Integer.parseInt(mis3_json.getString("old_inject_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>মোট</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("new_inject_center")) + Integer.parseInt(mis3_json.getString("old_inject_center"))) + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("new_inject_satelite")) + Integer.parseInt(mis3_json.getString("old_inject_satelite"))) + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("new_inject_center")) + Integer.parseInt(mis3_json.getString("old_inject_center")) + Integer.parseInt(mis3_json.getString("new_inject_satelite")) + Integer.parseInt(mis3_json.getString("old_inject_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>অনুসরণ</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>পার্শ্ব প্রতিক্রিয়া/জটিলতার জন্য সেবা</td>"
                    + "<td align='center'>" + mis3_json.getString("sideEffect_inj_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("sideEffect_inj_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("sideEffect_inj_center")) + Integer.parseInt(mis3_json.getString("sideEffect_inj_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='6' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>আইইউডি</td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>নতুন</td>"
                    + "<td style='border-top: 1px solid black;'>স্বাভাবিক</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("iud_normal_center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("iud_normal_satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("iud_normal_center")) + Integer.parseInt(mis3_json.getString("iud_normal_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td>প্রসব পরবর্তী</td>"
                    + "<td align='center'>" + mis3_json.getString("iud_after_delivery_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("iud_after_delivery_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("iud_after_delivery_center")) + Integer.parseInt(mis3_json.getString("iud_after_delivery_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>খুলে ফেলা</td>"
                    + "<td style='border-top: 1px solid black;'>মেয়াদ পূর্ণ হওয়ার পর</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("after_time_finish_center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("after_time_finish_satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("after_time_finish_center")) + Integer.parseInt(mis3_json.getString("after_time_finish_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td>মেয়াদ পূর্ণ হওয়ার পূর্বে</td>"
                    + "<td align='center'>" + mis3_json.getString("before_time_finish_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("before_time_finish_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("before_time_finish_center")) + Integer.parseInt(mis3_json.getString("before_time_finish_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>অনুসরণ</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("iud_followup_center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("iud_followup_satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("iud_followup_center")) + Integer.parseInt(mis3_json.getString("iud_followup_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>পার্শ্ব প্রতিক্রিয়া/জটিলতার জন্য সেবা</td>"
                    + "<td align='center'>" + mis3_json.getString("iud_sideeffect_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("iud_sideeffect_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("iud_sideeffect_center")) + Integer.parseInt(mis3_json.getString("iud_sideeffect_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='5' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইমপ্ল্যান্ট</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>নতুন</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("implant_new")+"</td>"
                    + "<td bgcolor='gray' rowspan='3' align='center' style='border-top: 1px solid black;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("implant_new")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>খুলে ফেলা</td>"
                    + "<td style='border-top: 1px solid black;'>মেয়াদ পূর্ণ হওয়ার পর</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("implant_after_time_finish")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("implant_after_time_finish")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td>মেয়াদ পূর্ণ হওয়ার পূর্বে</td>"
                    + "<td align='center'>"+mis3_json.getString("implant_before_time_finish")+"</td>"
                    + "<td align='center'>"+mis3_json.getString("implant_before_time_finish")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>অনুসরণ</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("implant_followup_center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("implant_followup_satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("implant_followup_center")) + Integer.parseInt(mis3_json.getString("implant_followup_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>পার্শ্ব প্রতিক্রিয়া/জটিলতার জন্য সেবা</td>"
                    + "<td align='center'>" + mis3_json.getString("implant_sideeffect_center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("implant_sideeffect_satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("implant_sideeffect_center")) + Integer.parseInt(mis3_json.getString("implant_sideeffect_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='7' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>স্থায়ী পদ্ধতি</span></td>"
                    + "<td rowspan='3' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>পুরুষ</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>সম্পাদন</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("permanent_male_execution")?mis3_json.getString("permanent_male_execution"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;background-color: gray;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("permanent_male_execution")?mis3_json.getString("permanent_male_execution"):"0") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>অনুসরণ</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_male_followup_center")?mis3_json.getString("permanent_male_followup_center"):"0") + "</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_male_followup_satelite")?mis3_json.getString("permanent_male_followup_satelite"):"0") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt((mis3_json.has("permanent_male_followup_center")?mis3_json.getString("permanent_male_followup_center"):"0")) + Integer.parseInt((mis3_json.has("permanent_male_followup_satelite")?mis3_json.getString("permanent_male_followup_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>পার্শ্ব প্রতিক্রিয়া/জটিলতার জন্য সেবা</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_male_sideeffect_center")?mis3_json.getString("permanent_male_sideeffect_center"):"0") + "</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_male_sideeffect_satelite")?mis3_json.getString("permanent_male_sideeffect_satelite"):"0") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt((mis3_json.has("permanent_male_sideeffect_center")?mis3_json.getString("permanent_male_sideeffect_center"):"0")) + Integer.parseInt((mis3_json.has("permanent_male_sideeffect_satelite")?mis3_json.getString("permanent_male_sideeffect_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='4' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>মহিলা</td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সম্পাদন</td>"
                    + "<td style='border-top: 1px solid black;'>স্বাভাবিক</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("permanent_female_execution_normal")?mis3_json.getString("permanent_female_execution_normal"):"0") + "</td>"
                    + "<td bgcolor='gray' rowspan='2' align='center' style='border-top: 1px solid black;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("permanent_female_execution_normal")?mis3_json.getString("permanent_female_execution_normal"):"0") + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td>প্রসব পরবর্তী</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_female_execution_after_delivery")?mis3_json.getString("permanent_female_execution_after_delivery"):"0") + "</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_female_execution_after_delivery")?mis3_json.getString("permanent_female_execution_after_delivery"):"0") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>অনুসরণ</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("permanent_female_followup_center")?mis3_json.getString("permanent_female_followup_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("permanent_female_followup_satelite")?mis3_json.getString("permanent_female_followup_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("permanent_female_followup_center")?mis3_json.getString("permanent_female_followup_center"):"0")) + Integer.parseInt((mis3_json.has("permanent_female_followup_satelite")?mis3_json.getString("permanent_female_followup_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>পার্শ্ব প্রতিক্রিয়া/জটিলতার জন্য সেবা</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_female_sideeffect_center")?mis3_json.getString("permanent_female_sideeffect_center"):"0") + "</td>"
                    + "<td align='center'>" + (mis3_json.has("permanent_female_sideeffect_satelite")?mis3_json.getString("permanent_female_sideeffect_satelite"):"0") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt((mis3_json.has("permanent_female_sideeffect_center")?mis3_json.getString("permanent_female_sideeffect_center"):"0")) + Integer.parseInt((mis3_json.has("permanent_female_sideeffect_satelite")?mis3_json.getString("permanent_female_sideeffect_satelite"):"0"))) + "</td>"
                    + "</tr>";



            resultString += "<tr class='danger'><td rowspan='28' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>প্রজনন স্বাস্থ্য সেবা</sapn></td></tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='5' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;font-size: 9px;margin-top: 20px;'>টি টি প্রাপ্ত ম্হিলা</span></td>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>১ম ডোজ</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3'>২ম ডোজ</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3'>৩ম ডোজ</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3'>৪ম ডোজ</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3'>৫ম ডোজ</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='4' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);'>গর্ভকালীন সেবা</span></td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>" + anc1text + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("anc1center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("anc1satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("anc1center")) + Integer.parseInt(mis3_json.getString("anc1satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + anc2text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("anc2center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("anc2satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("anc2center")) + Integer.parseInt(mis3_json.getString("anc2satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + anc3text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("anc3center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("anc3satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("anc3center")) + Integer.parseInt(mis3_json.getString("anc3satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + anc4text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("anc4center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("anc4satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("anc4center")) + Integer.parseInt(mis3_json.getString("anc4satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='5' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>প্রসব সেবা</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>স্বাভাবিক</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("normaldelivery") + "</td>"
                    + "<td bgcolor='gray' rowspan='6' style='border-top: 1px solid black;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("normaldelivery") + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>সিজারিয়ান</td>"
                    + "<td align='center'>" + mis3_json.getString("csectiondelivery") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("csectiondelivery") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনা (AMTSL) অনুসরণ করে প্রসব করানো</td>"
                    + "<td align='center'>" + mis3_json.getString("amtsl") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("amtsl") + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>জীবিত জন্ম (Live Birth)</td>"
                    + "<td align='center'>" + mis3_json.getString("livebirth") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("livebirth") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>মৃত জন্ম (Still Birth)</td>"
                    + "<td align='center'>" + mis3_json.getString("stillbirth") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("stillbirth") + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' class='td_color' style='border-top: 1px solid black;'>গর্ভপাত পরবর্তী সেবা প্রদানের সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pacservicecount") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pacservicecount") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='9' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>প্রসবোত্তর সেবা</span></td>"
                    + "<td rowspan='5' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>মা</span></td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>" + pncm1text + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pncmother1center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pncmother1satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("pncmother1center")) + Integer.parseInt(mis3_json.getString("pncmother1satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + pncm2text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmother2center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmother2satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmother2center")) + Integer.parseInt(mis3_json.getString("pncmother2satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + pncm3text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmother3center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmother3satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmother3center")) + Integer.parseInt(mis3_json.getString("pncmother3satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + pncm4text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmother4center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmother4satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmother4center")) + Integer.parseInt(mis3_json.getString("pncmother4satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি প্রদানের সংখ্যা</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmotherfpcenter") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncmotherfpsatelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmotherfpcenter")) + Integer.parseInt(mis3_json.getString("pncmotherfpsatelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='4' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>নবজাতক</span></td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>" + pncn1text + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pncchild1center") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("pncchild1satelite") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("pncchild1center")) + Integer.parseInt(mis3_json.getString("pncchild1satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + pncn2text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncchild2center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncchild2satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncchild2center")) + Integer.parseInt(mis3_json.getString("pncchild2satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + pncn3text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncchild3center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncchild3satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncchild3center")) + Integer.parseInt(mis3_json.getString("pncchild3satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + pncn4text + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncchild4center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("pncchild4satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncchild4center")) + Integer.parseInt(mis3_json.getString("pncchild4satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='3' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>রেফার</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>গর্ভকালীন, প্রসবকালীন ও প্রসবোত্তর জটিলতার সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("ancrefercenter")) + Integer.parseInt(mis3_json.getString("deliveryrefercenter")) + Integer.parseInt(mis3_json.getString("pncmotherrefercenter"))) + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("ancrefersatelite")) + Integer.parseInt(mis3_json.getString("deliveryrefersatelite")) + Integer.parseInt(mis3_json.getString("pncmotherrefersatelite"))) + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("ancrefercenter")) + Integer.parseInt(mis3_json.getString("deliveryrefercenter")) + Integer.parseInt(mis3_json.getString("pncmotherrefercenter")) + Integer.parseInt(mis3_json.getString("ancrefersatelite")) + Integer.parseInt(mis3_json.getString("deliveryrefersatelite")) + Integer.parseInt(mis3_json.getString("pncmotherrefersatelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>একলাম্পসিয়া রোগীকে MgSO4 ইনজেকশন প্রদানের সংখ্যা</td>"
                    + "<td align='center'>" + mis3_json.getString("ancmgso4center") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("ancmgso4satelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("ancmgso4center")) + Integer.parseInt(mis3_json.getString("ancmgso4satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>নবজাতককে জটিলতার জন্য প্রেরণের সংখ্যা</td>"
                    + "<td align='center'>" + mis3_json.getString("newbornreferalcenter") + "</td>"
                    + "<td align='center'>" + mis3_json.getString("newbornreferalsatelite") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("newbornreferalcenter")) + Integer.parseInt(mis3_json.getString("newbornreferalsatelite"))) + "</td>"
                    + "</tr></table>";

            resultString += "<table class=\"table page_2 table_hide\" id=\"table_2\" width=\"700\" border=\"1\" cellspacing=\"0\" style='margin-top:30px;font-size:13.25px;display:none;'>"
                    + "<tr><td colspan='8' style='text-align: right;border-right: 1px solid rgba(255,255,255,0);border-left: 1px solid rgba(255,255,255,0);border-top: 1px solid rgba(255,255,255,0);'>এমআইএস ফরম-৩<br>পৃষ্ঠা-২</td></tr>"
                    + "<tr class='danger'>"
                    + "<td style='text-align: center;vertical-align: middle;border-top: 1px solid black;'></td>"
                    + "<td colspan='4' style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>সেবার ধরণ</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>কেন্দ্র</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>স্যাটেলাইট</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মোট</td>"
                    + "</tr>";

            resultString += "<tr class='danger'><td rowspan='13' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>প্রজনন স্বাস্থ্য সেবা</sapn></td></tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>বন্ধ্যাত্ব সেবা</td>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>চিকিৎসা / পরামর্শ </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("infertility_treatment_center")?mis3_json.getString("infertility_treatment_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("infertility_treatment_satelite")?mis3_json.getString("infertility_treatment_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("infertility_treatment_center")?mis3_json.getString("infertility_treatment_center"):"0")) + Integer.parseInt((mis3_json.has("infertility_treatment_satelite")?mis3_json.getString("infertility_treatment_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3'>রেফার </td>"
                    + "<td align='center'>" + (mis3_json.has("infertility_refer_center")?mis3_json.getString("infertility_refer_center"):"0") + "</td>"
                    + "<td align='center'>" + (mis3_json.has("infertility_refer_satelite")?mis3_json.getString("infertility_refer_satelite"):"0") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt((mis3_json.has("infertility_refer_center")?mis3_json.getString("infertility_refer_center"):"0")) + Integer.parseInt((mis3_json.has("infertility_refer_satelite")?mis3_json.getString("infertility_refer_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='5' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 50px;margin-top: 25px;'>কিশোর-কিশোরীর <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;সেবা <br />&nbsp;&nbsp;&nbsp;(১০-১৯ বৎসর)</span></td>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>আয়রন ও ফলিক এসিড বড়ি গ্রহনকারীর সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("gp_ifa_center")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("gp_ifa_satelite")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+(Integer.parseInt(mis3_json.getString("gp_ifa_center")) + Integer.parseInt(mis3_json.getString("gp_ifa_satelite")))+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3'>প্রজননতন্ত্রের সংক্রমণ ও যৌনবাহিত রোগ নিরাময়ে চিকিৎসা প্রাপ্ত কিশোর কিশোরীর সংখ্যা  </td>"
                    + "<td align='center'>"+mis3_json.getString("gp_rta_sta_center")+"</td>"
                    + "<td align='center'>"+mis3_json.getString("gp_rta_sta_satelite")+"</td>"
                    + "<td align='center'>"+(Integer.parseInt(mis3_json.getString("gp_rta_sta_center")) + Integer.parseInt(mis3_json.getString("gp_rta_sta_satelite")))+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3'>প্রজননতন্ত্রের সংক্রমণ ও যৌনবাহিত বিষয়ে কাউন্সেলিং প্রাপ্ত কিশোর কিশোরীর সংখ্যা  </td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3'>বয়:সন্ধিকালীন পরিবর্তন বিষয়ে কাউন্সেলিং প্রাপ্ত  কিশোর কিশোরীর সংখ্যা   </td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3'>স্যানিটারী প্যাড গ্রহনকারী কিশোর কিশোরীর সংখ্যা  </td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>RTI/STI সংক্রান্ত চিকিৎসা প্রদান</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("gp_rta_sta_all_center")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("gp_rta_sta_all_satelite")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+(Integer.parseInt(mis3_json.getString("gp_rta_sta_all_center")) + Integer.parseInt(mis3_json.getString("gp_rta_sta_all_satelite")))+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4'>ইসিপি গ্রহনকারী সংখ্যা </td>"
                    + "<td align='center'>" + (mis3_json.has("ecp_taken_center")?mis3_json.getString("ecp_taken_center"):"0") + "</td>"
                    + "<td align='center'>" + (mis3_json.has("ecp_taken_satelite")?mis3_json.getString("ecp_taken_satelite"):"0") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt((mis3_json.has("ecp_taken_center")?mis3_json.getString("ecp_taken_center"):"0")) + Integer.parseInt((mis3_json.has("ecp_taken_satelite")?mis3_json.getString("ecp_taken_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4'>মিসোপ্রোস্টল বড়ি গ্রহণ কারী সংখ্যা </td>"
                    + "<td align='center'>"+(Integer.parseInt(mis3_json.getString("ancmisoprostolcenter")) + Integer.parseInt(mis3_json.getString("deliverymisoprostol")))+"</td>"
                    + "<td align='center'>"+mis3_json.getString("ancmisoprostolsatelite")+"</td>"
                    + "<td align='center'>"+(Integer.parseInt(mis3_json.getString("ancmisoprostolcenter")) + Integer.parseInt(mis3_json.getString("ancmisoprostolsatelite")) + Integer.parseInt(mis3_json.getString("deliverymisoprostol")))+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4'>এম আর ( এম ভি এ ) সেবা প্রদানের সংখ্যা  </td>"
                    + "<td align='center'>" + (mis3_json.has("mr_mva_treatment_center")?mis3_json.getString("mr_mva_treatment_center"):"0") + "</td>"
                    + "<td align='center'>" + (mis3_json.has("mr_mva_treatment_satelite")?mis3_json.getString("mr_mva_treatment_satelite"):"0") + "</td>"
                    + "<td align='center'>" + (Integer.parseInt((mis3_json.has("mr_mva_treatment_center")?mis3_json.getString("mr_mva_treatment_center"):"0")) + Integer.parseInt((mis3_json.has("mr_mva_treatment_satelite")?mis3_json.getString("mr_mva_treatment_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4'>এম আর এম ( ঔষধের মাধ্যমে এম আর) সেবা প্রদানের সংখ্যা </td>"
                    + "<td align='center'>" + (mis3_json.has("mrm_treatment")?mis3_json.getString("mrm_treatment"):"0") + "</td>"
                    + "<td align='center' style='background-color: gray;'></td>"
                    + "<td align='center'>" + (mis3_json.has("mrm_treatment")?mis3_json.getString("mrm_treatment"):"0") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='5' class='danger' style='border-top: 1px solid black;'>মাতৃমৃত্যু</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("maternal_death")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("maternal_death")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='3' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>সাধারণ রোগী</td>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>পুরুষ</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("gp_male_center")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;background-color: gray;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("gp_male_center")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3'>মহিলা </td>"
                    + "<td align='center'>"+mis3_json.getString("gp_female_center")+"</td>"
                    + "<td align='center'>"+mis3_json.getString("gp_female_satelite")+"</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("gp_female_center")) + Integer.parseInt(mis3_json.getString("gp_female_satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3'>মোট </td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("gp_female_center")) + Integer.parseInt(mis3_json.getString("gp_male_center"))) + "</td>"
                    + "<td align='center'>"+mis3_json.getString("gp_female_satelite")+"</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("gp_female_center")) + Integer.parseInt(mis3_json.getString("gp_female_satelite")) + Integer.parseInt(mis3_json.getString("gp_male_center"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='5' class='danger' style='border-top: 1px solid black;'>অন্ত বিভাগে ভর্তি মহিলা রোগীর সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "<td rowspan='4' align='center' style='border-top: 1px solid black;background-color: gray;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='5' class='danger' style='border-top: 1px solid black;'>অন্ত বিভাগে ভর্তি শিশু রোগীর সংখ্যা </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='5' class='danger' style='border-top: 1px solid black;'>কেন্দ্রে কয়টি স্বাস্থ্য শিক্ষা (বিসিসি) কার্যক্রম সম্পন্ন করা হয়েছে</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("bcc_from_center")?mis3_json.getString("bcc_from_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("bcc_from_center")?mis3_json.getString("bcc_from_center"):"0") + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='5' class='danger' style='border-top: 1px solid black;'>এসএসসি এমও কর্তৃক কয়টি স্কুল স্বাস্থ্য শিক্ষা (বিসিসি) কার্যক্রম সম্পাদন করা হয়েছে</td>"
                    + "<td align='center' style='border-top: 1px solid black;background-color: gray;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("bcc_from_sscmo")?mis3_json.getString("bcc_from_sscmo"):"0") + "</td>"
                    + "</tr>";

            resultString += "<tr class='danger'><td rowspan='13' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>পুষ্টি সেবা</sapn></td></tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>আই ওয়াই সি এফ , আই এফ এ , ভিটামিন -এ , হাত ধোয়া , মায়ের পুষ্টি বিষয়ক কাউন্সেলিং </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("nutrition_counseling_center")?mis3_json.getString("nutrition_counseling_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("nutrition_counseling_satelite")?mis3_json.getString("nutrition_counseling_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("nutrition_counseling_center")?mis3_json.getString("nutrition_counseling_center"):"0")) + Integer.parseInt((mis3_json.has("nutrition_counseling_satelite")?mis3_json.getString("nutrition_counseling_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>গর্ভবতি মাকে আয়রন ও ফলিক এসিড বড়ি দেয়া হয়েছে কি না</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("pregwomen_ifa_given_center")?mis3_json.getString("pregwomen_ifa_given_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("pregwomen_ifa_given_satelite")?mis3_json.getString("pregwomen_ifa_given_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("pregwomen_ifa_given_center")?mis3_json.getString("pregwomen_ifa_given_center"):"0")) + Integer.parseInt((mis3_json.has("pregwomen_ifa_given_satelite")?mis3_json.getString("pregwomen_ifa_given_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>০-৩ মাস বয়সী শিশুর মাকে আয়রন ও ফলিক এসিড বড়ি দেয়া হয়েছে কি না</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("0-3_month_monther_ifa_given_center")?mis3_json.getString("0-3_month_monther_ifa_given_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("0-3_month_monther_ifa_given_satelite")?mis3_json.getString("0-3_month_monther_ifa_given_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("0-3_month_monther_ifa_given_center")?mis3_json.getString("0-3_month_monther_ifa_given_center"):"0")) + Integer.parseInt((mis3_json.has("0-3_month_monther_ifa_given_satelite")?mis3_json.getString("0-3_month_monther_ifa_given_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>৬-২৩ মাস বয়সী শিশুকে মাল্টিপল মাইক্রোনিউট্রয়েন্ট পাউডার (এম এন পি ) দেয়া হয়েছে ।</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("6-23_month_child_mnp_given_center")?mis3_json.getString("6-23_month_child_mnp_given_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("6-23_month_child_mnp_given_satelite")?mis3_json.getString("6-23_month_child_mnp_given_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("6-23_month_child_mnp_given_center")?mis3_json.getString("6-23_month_child_mnp_given_center"):"0")) + Integer.parseInt((mis3_json.has("6-23_month_child_mnp_given_satelite")?mis3_json.getString("6-23_month_child_mnp_given_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>৬-৫৯ মাস বয়সী শিশুকে ভিটামিন- এ দেয়া হয়েছে কিনা </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("6-59_month_child_vitamin-a_given_center")?mis3_json.getString("6-59_month_child_vitamin-a_given_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("6-59_month_child_vitamin-a_given_satelite")?mis3_json.getString("6-59_month_child_vitamin-a_given_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("6-59_month_child_vitamin-a_given_center")?mis3_json.getString("6-59_month_child_vitamin-a_given_center"):"0")) + Integer.parseInt((mis3_json.has("6-59_month_child_vitamin-a_given_satelite")?mis3_json.getString("6-59_month_child_vitamin-a_given_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>২৪-৫৯ মাস বয়সী শিশুকে কৃমি নাশক বড়ি দেয়া হয়েছে কিনা </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("24-59_month_child_wormpills_given_center")?mis3_json.getString("24-59_month_child_wormpills_given_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("24-59_month_child_wormpills_given_satelite")?mis3_json.getString("24-59_month_child_wormpills_given_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("24-59_month_child_wormpills_given_center")?mis3_json.getString("24-59_month_child_wormpills_given_center"):"0")) + Integer.parseInt((mis3_json.has("24-59_month_child_wormpills_given_satelite")?mis3_json.getString("24-59_month_child_wormpills_given_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>৬-৫৯ মাস বয়সী ডায়রিয়া আক্রান্ত শিশুকে খাবার স্যালাইনের সাথে জিংক বড়ি দেয়া হয়েছে কিনা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("6-59_child_saline_given_center")?mis3_json.getString("6-59_child_saline_given_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("6-59_child_saline_given_satelite")?mis3_json.getString("6-59_child_saline_given_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("6-59_child_saline_given_center")?mis3_json.getString("6-59_child_saline_given_center"):"0")) + Integer.parseInt((mis3_json.has("6-59_child_saline_given_satelite")?mis3_json.getString("6-59_child_saline_given_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>MAM(মাঝারি তীব্র অপুষ্টি) আক্রান্ত শিশুকে সনাক্ত করা হয়েছে  এবং চিকিৎসা দেয়া হয়েছে (৬-৫৯ মাস বয়সী)</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("mam_center")?mis3_json.getString("mam_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("mam_satelite")?mis3_json.getString("mam_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("mam_center")?mis3_json.getString("mam_center"):"0")) + Integer.parseInt((mis3_json.has("mam_satelite")?mis3_json.getString("mam_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>SAM(মারাত্বক তীব্র অপুষ্টি) আক্রান্ত শিশুকে সনাক্ত করা হয়েছে  এবং রেফার করা হয়েছে  হয়েছে (৬-৫৯ মাস বয়সী)</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("sam_center")?mis3_json.getString("sam_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("sam_satelite")?mis3_json.getString("sam_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("sam_center")?mis3_json.getString("sam_center"):"0")) + Integer.parseInt((mis3_json.has("sam_satelite")?mis3_json.getString("sam_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>শিশুর Stunting (বয়সের তুলনায় কম উচ্চতা সম্পন্ন) সনাক্ত করা হয়েছে (০-৫৯  মাস বয়সী)</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("stunting_center")?mis3_json.getString("stunting_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("stunting_satelite")?mis3_json.getString("stunting_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("stunting_center")?mis3_json.getString("stunting_center"):"0")) + Integer.parseInt((mis3_json.has("stunting_satelite")?mis3_json.getString("stunting_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>শিশুর Wasting ( উচ্চতার তুলনায় কম ওজন সম্পন্ন) সনাক্ত করা হয়েছে (০-৫৯  মাস বয়সী)</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("wasting_center")?mis3_json.getString("wasting_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("wasting_satelite")?mis3_json.getString("wasting_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("wasting_center")?mis3_json.getString("wasting_center"):"0")) + Integer.parseInt((mis3_json.has("wasting_satelite")?mis3_json.getString("wasting_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' style='border-top: 1px solid black;'>শিশুর Under weight (বয়সের তুলনায় কম ওজন সম্পন্ন) সনাক্ত করা হয়েছে (০-৫৯  মাস বয়সী)</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("under_weight_center")?mis3_json.getString("under_weight_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("under_weight_satelite")?mis3_json.getString("under_weight_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("under_weight_center")?mis3_json.getString("under_weight_center"):"0")) + Integer.parseInt((mis3_json.has("under_weight_satelite")?mis3_json.getString("under_weight_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString += "<tr class='danger'><td rowspan='16' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>শিশু সেবা (০-৫৯ মাস)</sapn></td></tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='7' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 50px;margin-top: 25px;'>নবজাতক</span></td>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>১ মিনিটের মধ্যে মোছানোর সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("dryingafterbirth")+"</td>"
                    + "<td align='center' rowspan='8' style='border-top: 1px solid black;background-color: gray;'></td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("dryingafterbirth")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>নাড়ি কাটার পর ৭.১% ক্লোরহেক্সিডিন ব্যবহারের সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("chlorehexidin")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("chlorehexidin")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>নাড়ি কাটার পরপরই শিশুকে মায়ের ত্বকে লাগানো হয়েছে </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("skintouch")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("skintouch")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানোর সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("breastfeed")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("breastfeed")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>জন্মকালীন শ্বাসকষ্টে আক্রান্ত শিশুর ব্যাগ ও মাস্ক ব্যাবহার করে রিসাসসিটেট করার সংখ্যা </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("bagnmask")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("bagnmask")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>কম ওজনের (জন্ম ওজন ২.৫ এর কম কজেী ) শিশুর সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("lowbirthweight")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("lowbirthweight")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>অপরিনত (৩৭ সপ্তাহ পূর্ণ হবার পূর্বে জন্ম ) শিশুর সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("immaturebirth")+"</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("immaturebirth")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='4' class='danger' style='border-top: 1px solid black;'>নবজাতকের মৃত্যু (০-৮ দিন)</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='7' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 50px;margin-top: 25px;'>টিকা প্রাপ্ত (0-18 মাস) <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;শিশুর সংখ্যা</span></td>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>বিসিজি </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("bcg_center")?mis3_json.getString("bcg_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("bcg_satelite")?mis3_json.getString("bcg_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("bcg_center")?mis3_json.getString("bcg_center"):"0")) + Integer.parseInt((mis3_json.has("bcg_satelite")?mis3_json.getString("bcg_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='3' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;width: 12%;'>ওপিডি ও প্যান্টাভেলেন্ট<br />(ডিপিডি,হেপ-বি,হিব)</td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;width: 10%;'>পিসিভি</td>"
                    + "<td style='border-top: 1px solid black;width: 50%;'>১</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("opv_pcv_1_center")?mis3_json.getString("opv_pcv_1_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("opv_pcv_1_satelite")?mis3_json.getString("opv_pcv_1_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("opv_pcv_1_center")?mis3_json.getString("opv_pcv_1_center"):"0")) + Integer.parseInt((mis3_json.has("opv_pcv_1_satelite")?mis3_json.getString("opv_pcv_1_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td style='border-top: 1px solid black;'>২</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("opv_pcv_2_center")?mis3_json.getString("opv_pcv_2_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("opv_pcv_2_satelite")?mis3_json.getString("opv_pcv_2_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("opv_pcv_2_center")?mis3_json.getString("opv_pcv_2_center"):"0")) + Integer.parseInt((mis3_json.has("opv_pcv_2_satelite")?mis3_json.getString("opv_pcv_2_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td style='border-top: 1px solid black;border-right: 1px solid rgba(255,255,255,0);'></td>"
                    + "<td style='border-top: 1px solid black;'>৩</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("opv_3_center")?mis3_json.getString("opv_3_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("opv_3_satelite")?mis3_json.getString("opv_3_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("opv_3_center")?mis3_json.getString("opv_3_center"):"0")) + Integer.parseInt((mis3_json.has("opv_3_satelite")?mis3_json.getString("opv_3_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td style='border-top: 1px solid black;border-right: 1px solid rgba(255,255,255,0);'></td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>পিসিভি-৩</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("pcv_3_center")?mis3_json.getString("pcv_3_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("pcv_3_satelite")?mis3_json.getString("pcv_3_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("pcv_3_center")?mis3_json.getString("pcv_3_center"):"0")) + Integer.parseInt((mis3_json.has("pcv_3_satelite")?mis3_json.getString("pcv_3_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>এম আর ও ওপিভি-৪</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("mro_opv_center")?mis3_json.getString("mro_opv_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("mro_opv_satelite")?mis3_json.getString("mro_opv_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("mro_opv_center")?mis3_json.getString("mro_opv_center"):"0")) + Integer.parseInt((mis3_json.has("mro_opv_satelite")?mis3_json.getString("mro_opv_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='3' style='border-top: 1px solid black;'>হামের টিকা </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("measles_vaccine_center")?mis3_json.getString("measles_vaccine_center"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (mis3_json.has("measles_vaccine_satelite")?mis3_json.getString("measles_vaccine_satelite"):"0") + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("measles_vaccine_center")?mis3_json.getString("measles_vaccine_center"):"0")) + Integer.parseInt((mis3_json.has("measles_vaccine_satelite")?mis3_json.getString("measles_vaccine_satelite"):"0"))) + "</td>"
                    + "</tr>";

            resultString += "</table>";

            resultString += "<table class=\"table page_3 table_hide\" id=\"table_3\" width=\"700\" border=\"1\" cellspacing=\"0\" style='margin-top:60px;font-size:13px;display:none;'>"
                    + "<tr><td colspan='7' style='text-align: left;border-right: 1px solid rgba(255,255,255,0);border-left: 1px solid rgba(255,255,255,0);border-top: 1px solid rgba(255,255,255,0);'><h3>অসুস্থ শিশুর সমন্বিত চিকিৎসা ব্যবস্থাপনা (IMCI)</h3></td>"
                    + "<td colspan='6' style='text-align: right;border-right: 1px solid rgba(255,255,255,0);border-left: 1px solid rgba(255,255,255,0);border-top: 1px solid rgba(255,255,255,0);'>এমআইএস ফরম-৩<br>পৃষ্ঠা-৩</td></tr>";

            resultString += "<tr class='danger'>"
                    + "<td rowspan='3' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 10px;margin-top: 40px;'>ক্রমিক নং</span></td>"
                    + "<td colspan='12' style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>শিশু (০-৫৯ মাস) সেবা</td>"
                    + "</tr>";

            resultString += "<tr class='danger'>"
                    + "<td rowspan='2' style='text-align: center;border-top: 1px solid black;'>রোগের নাম</td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>০-২৮ দিন</td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>২৯-৫৯ দিন</td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>২মাস - <১ বৎসর</td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>১ - ৫ বৎসর</td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>মোট</td>"
                    + "<td rowspan='2' style='text-align: center;border-top: 1px solid black;'>সর্বমোট</td>"
                    + "</tr>";

            resultString += "<tr class='danger'>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>ছেলে </td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মেয়ে</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>ছেলে </td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মেয়ে</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>ছেলে </td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মেয়ে</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>ছেলে </td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মেয়ে</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>ছেলে </td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মেয়ে</td>"
                    + "</tr>";

            resultString += "<tr class='danger'>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(১)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(২)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(৩)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(৪)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(৫)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(৬)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(৭)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(৮)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(৯)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(১০)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(১১)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(১২)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>(১৩)</td>"
                    + "</tr>";

            Integer male_total = null;
            Integer female_total = null;

            male_total = (Integer.parseInt((mis3_json.has("very_serious_disease_0-28days_male")?mis3_json.getString("very_serious_disease_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("very_serious_disease_29-59days_male")?mis3_json.getString("very_serious_disease_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("very_serious_disease_2month-1year_male")?mis3_json.getString("very_serious_disease_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("very_serious_disease_1-5year_male")?mis3_json.getString("very_serious_disease_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("very_serious_disease_0-28days_female")?mis3_json.getString("very_serious_disease_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("very_serious_disease_29-59days_female")?mis3_json.getString("very_serious_disease_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("very_serious_disease_2month-1year_female")?mis3_json.getString("very_serious_disease_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("very_serious_disease_1-5year_female")?mis3_json.getString("very_serious_disease_1-5year_female"):"0")));

            resultString += "<tr>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>১.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>খুব মারাত্মক রোগ</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_0-28days_male")?mis3_json.getString("very_serious_disease_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_0-28days_female")?mis3_json.getString("very_serious_disease_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_29-59days_male")?mis3_json.getString("very_serious_disease_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_29-59days_female")?mis3_json.getString("very_serious_disease_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_2month-1year_male")?mis3_json.getString("very_serious_disease_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_2month-1year_female")?mis3_json.getString("very_serious_disease_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_1-5year_male")?mis3_json.getString("very_serious_disease_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("very_serious_disease_1-5year_female")?mis3_json.getString("very_serious_disease_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("pneumonia_0-28days_male")?mis3_json.getString("pneumonia_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("pneumonia_29-59days_male")?mis3_json.getString("pneumonia_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("pneumonia_2month-1year_male")?mis3_json.getString("pneumonia_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("pneumonia_1-5year_male")?mis3_json.getString("pneumonia_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("pneumonia_0-28days_female")?mis3_json.getString("pneumonia_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("pneumonia_29-59days_female")?mis3_json.getString("pneumonia_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("pneumonia_2month-1year_female")?mis3_json.getString("pneumonia_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("pneumonia_1-5year_female")?mis3_json.getString("pneumonia_1-5year_female"):"0")));

            resultString += "<tr class='success'>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>২.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>নিউমোনিয়া</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_0-28days_male")?mis3_json.getString("pneumonia_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_0-28days_female")?mis3_json.getString("pneumonia_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_29-59days_male")?mis3_json.getString("pneumonia_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_29-59days_female")?mis3_json.getString("pneumonia_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_2month-1year_male")?mis3_json.getString("pneumonia_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_2month-1year_female")?mis3_json.getString("pneumonia_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_1-5year_male")?mis3_json.getString("pneumonia_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("pneumonia_1-5year_female")?mis3_json.getString("pneumonia_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("cold_cough_0-28days_male")?mis3_json.getString("cold_cough_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("cold_cough_29-59days_male")?mis3_json.getString("cold_cough_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("cold_cough_2month-1year_male")?mis3_json.getString("cold_cough_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("cold_cough_1-5year_male")?mis3_json.getString("cold_cough_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("cold_cough_0-28days_female")?mis3_json.getString("cold_cough_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("cold_cough_29-59days_female")?mis3_json.getString("cold_cough_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("cold_cough_2month-1year_female")?mis3_json.getString("cold_cough_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("cold_cough_1-5year_female")?mis3_json.getString("cold_cough_1-5year_female"):"0")));

            resultString += "<tr>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>৩.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>সর্দি কাশি (নিউমোনিয়া নয়)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_0-28days_male")?mis3_json.getString("cold_cough_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_0-28days_female")?mis3_json.getString("cold_cough_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_29-59days_male")?mis3_json.getString("cold_cough_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_29-59days_female")?mis3_json.getString("cold_cough_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_2month-1year_male")?mis3_json.getString("cold_cough_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_2month-1year_female")?mis3_json.getString("cold_cough_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_1-5year_male")?mis3_json.getString("cold_cough_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("cold_cough_1-5year_female")?mis3_json.getString("cold_cough_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("diarrhea_0-28days_male")?mis3_json.getString("diarrhea_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("diarrhea_29-59days_male")?mis3_json.getString("diarrhea_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("diarrhea_2month-1year_male")?mis3_json.getString("diarrhea_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("diarrhea_1-5year_male")?mis3_json.getString("diarrhea_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("diarrhea_0-28days_female")?mis3_json.getString("diarrhea_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("diarrhea_29-59days_female")?mis3_json.getString("diarrhea_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("diarrhea_2month-1year_female")?mis3_json.getString("diarrhea_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("diarrhea_1-5year_female")?mis3_json.getString("diarrhea_1-5year_female"):"0")));

            resultString += "<tr class='success'>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>৪.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>ডায়রিয়া</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_0-28days_male")?mis3_json.getString("diarrhea_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_0-28days_female")?mis3_json.getString("diarrhea_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_29-59days_male")?mis3_json.getString("diarrhea_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_29-59days_female")?mis3_json.getString("diarrhea_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_2month-1year_male")?mis3_json.getString("diarrhea_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_2month-1year_female")?mis3_json.getString("diarrhea_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_1-5year_male")?mis3_json.getString("diarrhea_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("diarrhea_1-5year_female")?mis3_json.getString("diarrhea_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("malaria_0-28days_male")?mis3_json.getString("malaria_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("malaria_29-59days_male")?mis3_json.getString("malaria_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("malaria_2month-1year_male")?mis3_json.getString("malaria_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("malaria_1-5year_male")?mis3_json.getString("malaria_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("malaria_0-28days_female")?mis3_json.getString("malaria_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("malaria_29-59days_female")?mis3_json.getString("malaria_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("malaria_2month-1year_female")?mis3_json.getString("malaria_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("malaria_1-5year_female")?mis3_json.getString("malaria_1-5year_female"):"0")));

            resultString += "<tr>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>৫.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>জ্বর - ম্যালেরিয়া</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_0-28days_male")?mis3_json.getString("malaria_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_0-28days_female")?mis3_json.getString("malaria_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_29-59days_male")?mis3_json.getString("malaria_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_29-59days_female")?mis3_json.getString("malaria_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_2month-1year_male")?mis3_json.getString("malaria_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_2month-1year_female")?mis3_json.getString("malaria_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_1-5year_male")?mis3_json.getString("malaria_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malaria_1-5year_female")?mis3_json.getString("malaria_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("fever_not_malaria_0-28days_male")?mis3_json.getString("fever_not_malaria_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("fever_not_malaria_29-59days_male")?mis3_json.getString("fever_not_malaria_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("fever_not_malaria_2month-1year_male")?mis3_json.getString("fever_not_malaria_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("fever_not_malaria_1-5year_male")?mis3_json.getString("fever_not_malaria_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("fever_not_malaria_0-28days_female")?mis3_json.getString("fever_not_malaria_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("fever_not_malaria_29-59days_female")?mis3_json.getString("fever_not_malaria_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("fever_not_malaria_2month-1year_female")?mis3_json.getString("fever_not_malaria_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("fever_not_malaria_1-5year_female")?mis3_json.getString("fever_not_malaria_1-5year_female"):"0")));

            resultString += "<tr class='success'>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>৬.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>জ্বর - ম্যালেরিয়া নয়</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_0-28days_male")?mis3_json.getString("fever_not_malaria_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_0-28days_female")?mis3_json.getString("fever_not_malaria_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_29-59days_male")?mis3_json.getString("fever_not_malaria_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_29-59days_female")?mis3_json.getString("fever_not_malaria_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_2month-1year_male")?mis3_json.getString("fever_not_malaria_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_2month-1year_female")?mis3_json.getString("fever_not_malaria_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_1-5year_male")?mis3_json.getString("fever_not_malaria_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("fever_not_malaria_1-5year_female")?mis3_json.getString("fever_not_malaria_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("measles_0-28days_male")?mis3_json.getString("measles_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("measles_29-59days_male")?mis3_json.getString("measles_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("measles_2month-1year_male")?mis3_json.getString("measles_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("measles_1-5year_male")?mis3_json.getString("measles_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("measles_0-28days_female")?mis3_json.getString("measles_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("measles_29-59days_female")?mis3_json.getString("measles_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("measles_2month-1year_female")?mis3_json.getString("measles_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("measles_1-5year_female")?mis3_json.getString("measles_1-5year_female"):"0")));

            resultString += "<tr>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>৭.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>হাম</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_0-28days_male")?mis3_json.getString("measles_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_0-28days_female")?mis3_json.getString("measles_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_29-59days_male")?mis3_json.getString("measles_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_29-59days_female")?mis3_json.getString("measles_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_2month-1year_male")?mis3_json.getString("measles_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_2month-1year_female")?mis3_json.getString("measles_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_1-5year_male")?mis3_json.getString("measles_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("measles_1-5year_female")?mis3_json.getString("measles_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("ear_problem_0-28days_male")?mis3_json.getString("ear_problem_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("ear_problem_29-59days_male")?mis3_json.getString("ear_problem_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("ear_problem_2month-1year_male")?mis3_json.getString("ear_problem_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("ear_problem_1-5year_male")?mis3_json.getString("ear_problem_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("ear_problem_0-28days_female")?mis3_json.getString("ear_problem_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("ear_problem_29-59days_female")?mis3_json.getString("ear_problem_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("ear_problem_2month-1year_female")?mis3_json.getString("ear_problem_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("ear_problem_1-5year_female")?mis3_json.getString("ear_problem_1-5year_female"):"0")));

            resultString += "<tr class='success'>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>৮.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>কানের সমস্যা</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_0-28days_male")?mis3_json.getString("ear_problem_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_0-28days_female")?mis3_json.getString("ear_problem_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_29-59days_male")?mis3_json.getString("ear_problem_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_29-59days_female")?mis3_json.getString("ear_problem_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_2month-1year_male")?mis3_json.getString("ear_problem_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_2month-1year_female")?mis3_json.getString("ear_problem_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_1-5year_male")?mis3_json.getString("ear_problem_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("ear_problem_1-5year_female")?mis3_json.getString("ear_problem_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("malnutrition_0-28days_male")?mis3_json.getString("malnutrition_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("malnutrition_29-59days_male")?mis3_json.getString("malnutrition_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("malnutrition_2month-1year_male")?mis3_json.getString("malnutrition_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("malnutrition_1-5year_male")?mis3_json.getString("malnutrition_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("malnutrition_0-28days_female")?mis3_json.getString("malnutrition_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("malnutrition_29-59days_female")?mis3_json.getString("malnutrition_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("malnutrition_2month-1year_female")?mis3_json.getString("malnutrition_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("malnutrition_1-5year_female")?mis3_json.getString("malnutrition_1-5year_female"):"0")));

            resultString += "<tr>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>৯.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>অপুষ্টি</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_0-28days_male")?mis3_json.getString("malnutrition_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_0-28days_female")?mis3_json.getString("malnutrition_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_29-59days_male")?mis3_json.getString("malnutrition_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_29-59days_female")?mis3_json.getString("malnutrition_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_2month-1year_male")?mis3_json.getString("malnutrition_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_2month-1year_female")?mis3_json.getString("malnutrition_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_1-5year_male")?mis3_json.getString("malnutrition_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("malnutrition_1-5year_female")?mis3_json.getString("malnutrition_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("other_disease_0-28days_male")?mis3_json.getString("other_disease_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("other_disease_29-59days_male")?mis3_json.getString("other_disease_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("other_disease_2month-1year_male")?mis3_json.getString("other_disease_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("other_disease_1-5year_male")?mis3_json.getString("other_disease_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("other_disease_0-28days_female")?mis3_json.getString("other_disease_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("other_disease_29-59days_female")?mis3_json.getString("other_disease_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("other_disease_2month-1year_female")?mis3_json.getString("other_disease_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("other_disease_1-5year_female")?mis3_json.getString("other_disease_1-5year_female"):"0")));

            resultString += "<tr class='success'>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>১০.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>অন্যান্য</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_0-28days_male")?mis3_json.getString("other_disease_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_0-28days_female")?mis3_json.getString("other_disease_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_29-59days_male")?mis3_json.getString("other_disease_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_29-59days_female")?mis3_json.getString("other_disease_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_2month-1year_male")?mis3_json.getString("other_disease_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_2month-1year_female")?mis3_json.getString("other_disease_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_1-5year_male")?mis3_json.getString("other_disease_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("other_disease_1-5year_female")?mis3_json.getString("other_disease_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            male_total = (Integer.parseInt((mis3_json.has("imci_referred_0-28days_male")?mis3_json.getString("imci_referred_0-28days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("imci_referred_29-59days_male")?mis3_json.getString("imci_referred_29-59days_male"):"0"))+
                    Integer.parseInt((mis3_json.has("imci_referred_2month-1year_male")?mis3_json.getString("imci_referred_2month-1year_male"):"0"))+
                    Integer.parseInt((mis3_json.has("imci_referred_1-5year_male")?mis3_json.getString("imci_referred_1-5year_male"):"0")));

            female_total = (Integer.parseInt((mis3_json.has("imci_referred_0-28days_female")?mis3_json.getString("imci_referred_0-28days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("imci_referred_29-59days_female")?mis3_json.getString("imci_referred_29-59days_female"):"0"))+
                    Integer.parseInt((mis3_json.has("imci_referred_2month-1year_female")?mis3_json.getString("imci_referred_2month-1year_female"):"0"))+
                    Integer.parseInt((mis3_json.has("imci_referred_1-5year_female")?mis3_json.getString("imci_referred_1-5year_female"):"0")));

            resultString += "<tr>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>১১.</td>"
                    + "<td class='danger' style='border-top: 1px solid black;'>রেফারকৃত</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_0-28days_male")?mis3_json.getString("imci_referred_0-28days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_0-28days_female")?mis3_json.getString("imci_referred_0-28days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_29-59days_male")?mis3_json.getString("imci_referred_29-59days_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_29-59days_female")?mis3_json.getString("imci_referred_29-59days_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_2month-1year_male")?mis3_json.getString("imci_referred_2month-1year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_2month-1year_female")?mis3_json.getString("imci_referred_2month-1year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_1-5year_male")?mis3_json.getString("imci_referred_1-5year_male"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("imci_referred_1-5year_female")?mis3_json.getString("imci_referred_1-5year_female"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + male_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + female_total + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (male_total+female_total) + "</td>"
                    + "</tr>";

            resultString += "</table>";

            resultString += "<table class=\"table page_3 table_hide\" id=\"table_4\" width=\"700\" border=\"1\" cellspacing=\"0\" style='margin-top:40px;font-size:13px;display:none;'>"
                    + "<tr><td colspan='21' style='text-align: center;border-right: 1px solid rgba(255,255,255,0);border-left: 1px solid rgba(255,255,255,0);border-top: 1px solid rgba(255,255,255,0);'><h3>মাসিক মওজুদ ও বিতরণের হিসাব বিষয়ক</h3></td></tr>";

            resultString += "<tr class='danger'>"
                    + "<td colspan='2' rowspan='2' style='border-top: 1px solid black;border-left: 1px solid black;'></td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>খাবার বড়ি (চক্র)</td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>কনডম (পিস)</span></td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>ইনজেকটেবল</td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>আই ইউ ডি (পিস)</span></td>"
                    + "<td colspan='2' style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>ইমপ্ল্যান্ট (সেট)</td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ইসিপি (ডোজ)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>মিসো-প্রোস্টল (ডোজ)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>এম আর এম (প্যাক)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>৭.১% ক্লোরহেক্সিডিন</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ইনজেকশন MgSO4</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ইনজেকশন অক্সিটোসিন (ডোজ)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>এমএনপি (স্যাসেট)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>স্যানিটারী প্যাড (সংখ্যা)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>এম আর (এমভিএ) কিট (সংখ্যা)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ডেলিভারী কিট (সংখ্যা)</span></td>"
                    + "<td rowspan='2' class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ডিডিএস কিট (সংখ্যা)</span></td>"
                    + "</tr>";

            resultString += "<tr class='danger'>"
                    + "<td class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>সুখি</span></td>"
                    + "<td class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>আপন</span></td>"
                    + "<td class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ভায়াল</span></td>"
                    + "<td class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>সিরিঞ্জ</span></td>"
                    + "<td class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ইমপ্ল্যানন</span></td>"
                    + "<td class='danger' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 160px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>জেডেল</span></td>"
                    + "</tr>";

            resultString += "<tr>"
                    + "<td colspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>পূর্বের মওজুদ</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_shukhi")?mis3_json.getString("previous_stock_shukhi"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_apon")?mis3_json.getString("previous_stock_apon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_condom")?mis3_json.getString("previous_stock_condom"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_viale")?mis3_json.getString("previous_stock_viale"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_syringe")?mis3_json.getString("previous_stock_syringe"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_iud")?mis3_json.getString("previous_stock_iud"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_implanon")?mis3_json.getString("previous_stock_implanon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_jadelle")?mis3_json.getString("previous_stock_jadelle"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_ecp")?mis3_json.getString("previous_stock_ecp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_misoprostol")?mis3_json.getString("previous_stock_misoprostol"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_mrm_pac")?mis3_json.getString("previous_stock_mrm_pac"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_chlorehexidin")?mis3_json.getString("previous_stock_chlorehexidin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_mgso4")?mis3_json.getString("previous_stock_mgso4"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_oxytocin")?mis3_json.getString("previous_stock_oxytocin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_mnp")?mis3_json.getString("previous_stock_mnp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_sanitary_pad")?mis3_json.getString("previous_stock_sanitary_pad"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_mr_kit")?mis3_json.getString("previous_stock_mr_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_delivery_kit")?mis3_json.getString("previous_stock_delivery_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("previous_stock_dds_kit")?mis3_json.getString("previous_stock_dds_kit"):"0") + "</td>"
                    + "</tr>";

            resultString += "<tr class='success'>"
                    + "<td colspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>চলতি মাসে<br>পাওয়া গেছে (+)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_shukhi")?mis3_json.getString("stock_in_shukhi"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_apon")?mis3_json.getString("stock_in_apon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_condom")?mis3_json.getString("stock_in_condom"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_viale")?mis3_json.getString("stock_in_viale"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_syringe")?mis3_json.getString("stock_in_syringe"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_iud")?mis3_json.getString("stock_in_iud"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_implanon")?mis3_json.getString("stock_in_implanon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_jadelle")?mis3_json.getString("stock_in_jadelle"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_ecp")?mis3_json.getString("stock_in_ecp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_misoprostol")?mis3_json.getString("stock_in_misoprostol"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_mrm_pac")?mis3_json.getString("stock_in_mrm_pac"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_chlorehexidin")?mis3_json.getString("stock_in_chlorehexidin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_mgso4")?mis3_json.getString("stock_in_mgso4"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_oxytocin")?mis3_json.getString("stock_in_oxytocin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_mnp")?mis3_json.getString("stock_in_mnp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_sanitary_pad")?mis3_json.getString("stock_in_sanitary_pad"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_mr_kit")?mis3_json.getString("stock_in_mr_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_delivery_kit")?mis3_json.getString("stock_in_delivery_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("stock_in_dds_kit")?mis3_json.getString("stock_in_dds_kit"):"0") + "</td>"
                    + "</tr>";

            resultString += "<tr>"
                    + "<td colspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>চলতি মাসের<br>মোট মওজুদ</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_shukhi")?mis3_json.getString("previous_stock_shukhi"):"0")) + Integer.parseInt((mis3_json.has("stock_in_shukhi")?mis3_json.getString("stock_in_shukhi"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_apon")?mis3_json.getString("previous_stock_apon"):"0")) + Integer.parseInt((mis3_json.has("stock_in_apon")?mis3_json.getString("stock_in_apon"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_condom")?mis3_json.getString("previous_stock_condom"):"0")) + Integer.parseInt((mis3_json.has("stock_in_condom")?mis3_json.getString("stock_in_condom"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_viale")?mis3_json.getString("previous_stock_viale"):"0")) + Integer.parseInt((mis3_json.has("stock_in_viale")?mis3_json.getString("stock_in_viale"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_syringe")?mis3_json.getString("previous_stock_syringe"):"0")) + Integer.parseInt((mis3_json.has("stock_in_syringe")?mis3_json.getString("stock_in_syringe"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_iud")?mis3_json.getString("previous_stock_iud"):"0")) + Integer.parseInt((mis3_json.has("stock_in_iud")?mis3_json.getString("stock_in_iud"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_implanon")?mis3_json.getString("previous_stock_implanon"):"0")) + Integer.parseInt((mis3_json.has("stock_in_implanon")?mis3_json.getString("stock_in_implanon"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_jadelle")?mis3_json.getString("previous_stock_jadelle"):"0")) + Integer.parseInt((mis3_json.has("stock_in_jadelle")?mis3_json.getString("stock_in_jadelle"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_ecp")?mis3_json.getString("previous_stock_ecp"):"0")) + Integer.parseInt((mis3_json.has("stock_in_ecp")?mis3_json.getString("stock_in_ecp"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_misoprostol")?mis3_json.getString("previous_stock_misoprostol"):"0")) + Integer.parseInt((mis3_json.has("stock_in_misoprostol")?mis3_json.getString("stock_in_misoprostol"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mrm_pac")?mis3_json.getString("previous_stock_mrm_pac"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mrm_pac")?mis3_json.getString("stock_in_mrm_pac"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_chlorehexidin")?mis3_json.getString("previous_stock_chlorehexidin"):"0")) + Integer.parseInt((mis3_json.has("stock_in_chlorehexidin")?mis3_json.getString("stock_in_chlorehexidin"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mgso4")?mis3_json.getString("previous_stock_mgso4"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mgso4")?mis3_json.getString("stock_in_mgso4"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_oxytocin")?mis3_json.getString("previous_stock_oxytocin"):"0")) + Integer.parseInt((mis3_json.has("stock_in_oxytocin")?mis3_json.getString("stock_in_oxytocin"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mnp")?mis3_json.getString("previous_stock_mnp"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mnp")?mis3_json.getString("stock_in_mnp"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_sanitary_pad")?mis3_json.getString("previous_stock_sanitary_pad"):"0")) + Integer.parseInt((mis3_json.has("stock_in_sanitary_pad")?mis3_json.getString("stock_in_sanitary_pad"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mr_kit")?mis3_json.getString("previous_stock_mr_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mr_kit")?mis3_json.getString("stock_in_mr_kit"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_delivery_kit")?mis3_json.getString("previous_stock_delivery_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_delivery_kit")?mis3_json.getString("stock_in_delivery_kit"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_dds_kit")?mis3_json.getString("previous_stock_dds_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_dds_kit")?mis3_json.getString("stock_in_dds_kit"):"0"))) + "</td>"
                    + "</tr>";

            resultString += "<tr class='success'>"
                    + "<td rowspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>সমন্বয়</td>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>(+)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_shukhi")?mis3_json.getString("adjustment_stock_plus_shukhi"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_apon")?mis3_json.getString("adjustment_stock_plus_apon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_condom")?mis3_json.getString("adjustment_stock_plus_condom"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_viale")?mis3_json.getString("adjustment_stock_plus_viale"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_syringe")?mis3_json.getString("adjustment_stock_plus_syringe"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_iud")?mis3_json.getString("adjustment_stock_plus_iud"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_implanon")?mis3_json.getString("adjustment_stock_plus_implanon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_jadelle")?mis3_json.getString("adjustment_stock_plus_jadelle"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_ecp")?mis3_json.getString("adjustment_stock_plus_ecp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_misoprostol")?mis3_json.getString("adjustment_stock_plus_misoprostol"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_mrm_pac")?mis3_json.getString("adjustment_stock_plus_mrm_pac"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_chlorehexidin")?mis3_json.getString("adjustment_stock_plus_chlorehexidin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_mgso4")?mis3_json.getString("adjustment_stock_plus_mgso4"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_oxytocin")?mis3_json.getString("adjustment_stock_plus_oxytocin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_mnp")?mis3_json.getString("adjustment_stock_plus_mnp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_sanitary_pad")?mis3_json.getString("adjustment_stock_plus_sanitary_pad"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_mr_kit")?mis3_json.getString("adjustment_stock_plus_mr_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_delivery_kit")?mis3_json.getString("adjustment_stock_plus_delivery_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_plus_dds_kit")?mis3_json.getString("adjustment_stock_plus_dds_kit"):"0") + "</td>"
                    + "</tr>";

            resultString += "<tr>"
                    + "<td class='danger' style='text-align: center;border-top: 1px solid black;'>(-)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_shukhi")?mis3_json.getString("adjustment_stock_minus_shukhi"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_apon")?mis3_json.getString("adjustment_stock_minus_apon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_condom")?mis3_json.getString("adjustment_stock_minus_condom"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_viale")?mis3_json.getString("adjustment_stock_minus_viale"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_syringe")?mis3_json.getString("adjustment_stock_minus_syringe"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_iud")?mis3_json.getString("adjustment_stock_minus_iud"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_implanon")?mis3_json.getString("adjustment_stock_minus_implanon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_jadelle")?mis3_json.getString("adjustment_stock_minus_jadelle"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_ecp")?mis3_json.getString("adjustment_stock_minus_ecp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_misoprostol")?mis3_json.getString("adjustment_stock_minus_misoprostol"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_mrm_pac")?mis3_json.getString("adjustment_stock_minus_mrm_pac"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_chlorehexidin")?mis3_json.getString("adjustment_stock_minus_chlorehexidin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_mgso4")?mis3_json.getString("adjustment_stock_minus_mgso4"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_oxytocin")?mis3_json.getString("adjustment_stock_minus_oxytocin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_mnp")?mis3_json.getString("adjustment_stock_minus_mnp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_sanitary_pad")?mis3_json.getString("adjustment_stock_minus_sanitary_pad"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_mr_kit")?mis3_json.getString("adjustment_stock_minus_mr_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_delivery_kit")?mis3_json.getString("adjustment_stock_minus_delivery_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("adjustment_stock_minus_dds_kit")?mis3_json.getString("adjustment_stock_minus_dds_kit"):"0") + "</td>"
                    + "</tr>";

            resultString += "<tr class='success'>"
                    + "<td colspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>সর্বমোট</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_shukhi")?mis3_json.getString("previous_stock_shukhi"):"0")) + Integer.parseInt((mis3_json.has("stock_in_shukhi")?mis3_json.getString("stock_in_shukhi"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_shukhi")?mis3_json.getString("adjustment_stock_plus_shukhi"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_shukhi")?mis3_json.getString("adjustment_stock_minus_shukhi"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_apon")?mis3_json.getString("previous_stock_apon"):"0")) + Integer.parseInt((mis3_json.has("stock_in_apon")?mis3_json.getString("stock_in_apon"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_apon")?mis3_json.getString("adjustment_stock_plus_apon"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_apon")?mis3_json.getString("adjustment_stock_minus_apon"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_condom")?mis3_json.getString("previous_stock_condom"):"0")) + Integer.parseInt((mis3_json.has("stock_in_condom")?mis3_json.getString("stock_in_condom"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_condom")?mis3_json.getString("adjustment_stock_plus_condom"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_condom")?mis3_json.getString("adjustment_stock_minus_condom"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_viale")?mis3_json.getString("previous_stock_viale"):"0")) + Integer.parseInt((mis3_json.has("stock_in_viale")?mis3_json.getString("stock_in_viale"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_viale")?mis3_json.getString("adjustment_stock_plus_viale"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_viale")?mis3_json.getString("adjustment_stock_minus_viale"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_syringe")?mis3_json.getString("previous_stock_syringe"):"0")) + Integer.parseInt((mis3_json.has("stock_in_syringe")?mis3_json.getString("stock_in_syringe"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_syringe")?mis3_json.getString("adjustment_stock_plus_syringe"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_syringe")?mis3_json.getString("adjustment_stock_minus_syringe"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_iud")?mis3_json.getString("previous_stock_iud"):"0")) + Integer.parseInt((mis3_json.has("stock_in_iud")?mis3_json.getString("stock_in_iud"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_iud")?mis3_json.getString("adjustment_stock_plus_iud"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_iud")?mis3_json.getString("adjustment_stock_minus_iud"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_implanon")?mis3_json.getString("previous_stock_implanon"):"0")) + Integer.parseInt((mis3_json.has("stock_in_implanon")?mis3_json.getString("stock_in_implanon"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_implanon")?mis3_json.getString("adjustment_stock_plus_implanon"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_implanon")?mis3_json.getString("adjustment_stock_minus_implanon"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_jadelle")?mis3_json.getString("previous_stock_jadelle"):"0")) + Integer.parseInt((mis3_json.has("stock_in_jadelle")?mis3_json.getString("stock_in_jadelle"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_jadelle")?mis3_json.getString("adjustment_stock_plus_jadelle"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_jadelle")?mis3_json.getString("adjustment_stock_minus_jadelle"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_ecp")?mis3_json.getString("previous_stock_ecp"):"0")) + Integer.parseInt((mis3_json.has("stock_in_ecp")?mis3_json.getString("stock_in_ecp"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_ecp")?mis3_json.getString("adjustment_stock_plus_ecp"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_ecp")?mis3_json.getString("adjustment_stock_minus_ecp"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_misoprostol")?mis3_json.getString("previous_stock_misoprostol"):"0")) + Integer.parseInt((mis3_json.has("stock_in_misoprostol")?mis3_json.getString("stock_in_misoprostol"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_misoprostol")?mis3_json.getString("adjustment_stock_plus_misoprostol"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_misoprostol")?mis3_json.getString("adjustment_stock_minus_misoprostol"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mrm_pac")?mis3_json.getString("previous_stock_mrm_pac"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mrm_pac")?mis3_json.getString("stock_in_mrm_pac"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mrm_pac")?mis3_json.getString("adjustment_stock_plus_mrm_pac"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mrm_pac")?mis3_json.getString("adjustment_stock_minus_mrm_pac"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_chlorehexidin")?mis3_json.getString("previous_stock_chlorehexidin"):"0")) + Integer.parseInt((mis3_json.has("stock_in_chlorehexidin")?mis3_json.getString("stock_in_chlorehexidin"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_chlorehexidin")?mis3_json.getString("adjustment_stock_plus_chlorehexidin"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_chlorehexidin")?mis3_json.getString("adjustment_stock_minus_chlorehexidin"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mgso4")?mis3_json.getString("previous_stock_mgso4"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mgso4")?mis3_json.getString("stock_in_mgso4"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mgso4")?mis3_json.getString("adjustment_stock_plus_mgso4"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mgso4")?mis3_json.getString("adjustment_stock_minus_mgso4"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_oxytocin")?mis3_json.getString("previous_stock_oxytocin"):"0")) + Integer.parseInt((mis3_json.has("stock_in_oxytocin")?mis3_json.getString("stock_in_oxytocin"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_oxytocin")?mis3_json.getString("adjustment_stock_plus_oxytocin"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_oxytocin")?mis3_json.getString("adjustment_stock_minus_oxytocin"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mnp")?mis3_json.getString("previous_stock_mnp"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mnp")?mis3_json.getString("stock_in_mnp"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mnp")?mis3_json.getString("adjustment_stock_plus_mnp"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mnp")?mis3_json.getString("adjustment_stock_minus_mnp"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_sanitary_pad")?mis3_json.getString("previous_stock_sanitary_pad"):"0")) + Integer.parseInt((mis3_json.has("stock_in_sanitary_pad")?mis3_json.getString("stock_in_sanitary_pad"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_sanitary_pad")?mis3_json.getString("adjustment_stock_plus_sanitary_pad"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_sanitary_pad")?mis3_json.getString("adjustment_stock_minus_sanitary_pad"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mr_kit")?mis3_json.getString("previous_stock_mr_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mr_kit")?mis3_json.getString("stock_in_mr_kit"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mr_kit")?mis3_json.getString("adjustment_stock_plus_mr_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mr_kit")?mis3_json.getString("adjustment_stock_minus_mr_kit"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_delivery_kit")?mis3_json.getString("previous_stock_delivery_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_delivery_kit")?mis3_json.getString("stock_in_delivery_kit"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_delivery_kit")?mis3_json.getString("adjustment_stock_plus_delivery_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_delivery_kit")?mis3_json.getString("adjustment_stock_minus_delivery_kit"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_dds_kit")?mis3_json.getString("previous_stock_dds_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_dds_kit")?mis3_json.getString("stock_in_dds_kit"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_dds_kit")?mis3_json.getString("adjustment_stock_plus_dds_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_dds_kit")?mis3_json.getString("adjustment_stock_minus_dds_kit"):"0"))) + "</td>"
                    + "</tr>";

            resultString += "<tr>"
                    + "<td colspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>চলতি মাসে<br>বিতরণ করা<br>হয়েছে (-)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + mis3_json.getString("pill_shukhi_total") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + mis3_json.getString("pill_apon_total") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + mis3_json.getString("condom_total") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("new_inject_center")) + Integer.parseInt(mis3_json.getString("old_inject_center")) + Integer.parseInt(mis3_json.getString("new_inject_satelite")) + Integer.parseInt(mis3_json.getString("old_inject_satelite"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("new_inject_center")) + Integer.parseInt(mis3_json.getString("old_inject_center")) + Integer.parseInt(mis3_json.getString("new_inject_satelite")) + Integer.parseInt(mis3_json.getString("old_inject_satelite"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("iud_normal_center")) + Integer.parseInt(mis3_json.getString("iud_normal_satelite")) + Integer.parseInt(mis3_json.getString("iud_after_delivery_center")) + Integer.parseInt(mis3_json.getString("iud_after_delivery_satelite"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + mis3_json.getString("implant_implanon") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + mis3_json.getString("implant_jadelle") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("ecp_taken_center")?mis3_json.getString("ecp_taken_center"):"0")) + Integer.parseInt((mis3_json.has("ecp_taken_satelite")?mis3_json.getString("ecp_taken_satelite"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("ancmisoprostolcenter")) + Integer.parseInt(mis3_json.getString("ancmisoprostolsatelite")) + Integer.parseInt(mis3_json.getString("deliverymisoprostol"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("mrm_pac")?mis3_json.getString("mrm_pac"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + mis3_json.getString("chlorehexidin") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("ancmgso4center")) + Integer.parseInt(mis3_json.getString("ancmgso4satelite"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("deliverycxytocin")?mis3_json.getString("deliverycxytocin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("mnp")?mis3_json.getString("mnp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("sanitary_pad")?mis3_json.getString("sanitary_pad"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("mr_kit")?mis3_json.getString("mr_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("delivery_kit")?mis3_json.getString("delivery_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("dds_kit")?mis3_json.getString("dds_kit"):"0") + "</td>"
                    + "</tr>";

            resultString += "<tr class='success'>"
                    + "<td colspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>অবশিষ্ট</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_shukhi")?mis3_json.getString("previous_stock_shukhi"):"0")) + Integer.parseInt((mis3_json.has("stock_in_shukhi")?mis3_json.getString("stock_in_shukhi"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_shukhi")?mis3_json.getString("adjustment_stock_plus_shukhi"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_shukhi")?mis3_json.getString("adjustment_stock_minus_shukhi"):"0")) + Integer.parseInt("-" + mis3_json.getString("pill_shukhi_total"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_apon")?mis3_json.getString("previous_stock_apon"):"0")) + Integer.parseInt((mis3_json.has("stock_in_apon")?mis3_json.getString("stock_in_apon"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_apon")?mis3_json.getString("adjustment_stock_plus_apon"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_apon")?mis3_json.getString("adjustment_stock_minus_apon"):"0")) + Integer.parseInt("-" + mis3_json.getString("pill_apon_total"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_condom")?mis3_json.getString("previous_stock_condom"):"0")) + Integer.parseInt((mis3_json.has("stock_in_condom")?mis3_json.getString("stock_in_condom"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_condom")?mis3_json.getString("adjustment_stock_plus_condom"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_condom")?mis3_json.getString("adjustment_stock_minus_condom"):"0")) + Integer.parseInt("-" + mis3_json.getString("condom_total"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_viale")?mis3_json.getString("previous_stock_viale"):"0")) + Integer.parseInt((mis3_json.has("stock_in_viale")?mis3_json.getString("stock_in_viale"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_viale")?mis3_json.getString("adjustment_stock_plus_viale"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_viale")?mis3_json.getString("adjustment_stock_minus_viale"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(mis3_json.getString("new_inject_center")) + Integer.parseInt(mis3_json.getString("old_inject_center")) + Integer.parseInt(mis3_json.getString("new_inject_satelite")) + Integer.parseInt(mis3_json.getString("old_inject_satelite"))))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_syringe")?mis3_json.getString("previous_stock_syringe"):"0")) + Integer.parseInt((mis3_json.has("stock_in_syringe")?mis3_json.getString("stock_in_syringe"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_syringe")?mis3_json.getString("adjustment_stock_plus_syringe"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_syringe")?mis3_json.getString("adjustment_stock_minus_syringe"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(mis3_json.getString("new_inject_center")) + Integer.parseInt(mis3_json.getString("old_inject_center")) + Integer.parseInt(mis3_json.getString("new_inject_satelite")) + Integer.parseInt(mis3_json.getString("old_inject_satelite"))))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_iud")?mis3_json.getString("previous_stock_iud"):"0")) + Integer.parseInt((mis3_json.has("stock_in_iud")?mis3_json.getString("stock_in_iud"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_iud")?mis3_json.getString("adjustment_stock_plus_iud"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_iud")?mis3_json.getString("adjustment_stock_minus_iud"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(mis3_json.getString("iud_normal_center")) + Integer.parseInt(mis3_json.getString("iud_normal_satelite")) + Integer.parseInt(mis3_json.getString("iud_after_delivery_center")) + Integer.parseInt(mis3_json.getString("iud_after_delivery_satelite"))))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_implanon")?mis3_json.getString("previous_stock_implanon"):"0")) + Integer.parseInt((mis3_json.has("stock_in_implanon")?mis3_json.getString("stock_in_implanon"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_implanon")?mis3_json.getString("adjustment_stock_plus_implanon"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_implanon")?mis3_json.getString("adjustment_stock_minus_implanon"):"0")) + Integer.parseInt("-" + mis3_json.getString("implant_implanon"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_jadelle")?mis3_json.getString("previous_stock_jadelle"):"0")) + Integer.parseInt((mis3_json.has("stock_in_jadelle")?mis3_json.getString("stock_in_jadelle"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_jadelle")?mis3_json.getString("adjustment_stock_plus_jadelle"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_jadelle")?mis3_json.getString("adjustment_stock_minus_jadelle"):"0")) + Integer.parseInt("-" + mis3_json.getString("implant_jadelle"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_ecp")?mis3_json.getString("previous_stock_ecp"):"0")) + Integer.parseInt((mis3_json.has("stock_in_ecp")?mis3_json.getString("stock_in_ecp"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_ecp")?mis3_json.getString("adjustment_stock_plus_ecp"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_ecp")?mis3_json.getString("adjustment_stock_minus_ecp"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt((mis3_json.has("ecp_taken_center")?mis3_json.getString("ecp_taken_center"):"0")) + Integer.parseInt((mis3_json.has("ecp_taken_satelite")?mis3_json.getString("ecp_taken_satelite"):"0"))))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_misoprostol")?mis3_json.getString("previous_stock_misoprostol"):"0")) + Integer.parseInt((mis3_json.has("stock_in_misoprostol")?mis3_json.getString("stock_in_misoprostol"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_misoprostol")?mis3_json.getString("adjustment_stock_plus_misoprostol"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_misoprostol")?mis3_json.getString("adjustment_stock_minus_misoprostol"):"0")) + Integer.parseInt("-" + mis3_json.getString("ancmisoprostolcenter")) + Integer.parseInt("-" + mis3_json.getString("ancmisoprostolsatelite")) + Integer.parseInt("-" + mis3_json.getString("deliverymisoprostol"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mrm_pac")?mis3_json.getString("previous_stock_mrm_pac"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mrm_pac")?mis3_json.getString("stock_in_mrm_pac"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mrm_pac")?mis3_json.getString("adjustment_stock_plus_mrm_pac"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mrm_pac")?mis3_json.getString("adjustment_stock_minus_mrm_pac"):"0")) + Integer.parseInt("-" + (mis3_json.has("mrm_pac")?mis3_json.getString("mrm_pac"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_chlorehexidin")?mis3_json.getString("previous_stock_chlorehexidin"):"0")) + Integer.parseInt((mis3_json.has("stock_in_chlorehexidin")?mis3_json.getString("stock_in_chlorehexidin"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_chlorehexidin")?mis3_json.getString("adjustment_stock_plus_chlorehexidin"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_chlorehexidin")?mis3_json.getString("adjustment_stock_minus_chlorehexidin"):"0")) + Integer.parseInt("-" + mis3_json.getString("chlorehexidin"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mgso4")?mis3_json.getString("previous_stock_mgso4"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mgso4")?mis3_json.getString("stock_in_mgso4"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mgso4")?mis3_json.getString("adjustment_stock_plus_mgso4"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mgso4")?mis3_json.getString("adjustment_stock_minus_mgso4"):"0")) + Integer.parseInt("-" + Integer.toString(Integer.parseInt(mis3_json.getString("ancmgso4center")) + Integer.parseInt(mis3_json.getString("ancmgso4satelite"))))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_oxytocin")?mis3_json.getString("previous_stock_oxytocin"):"0")) + Integer.parseInt((mis3_json.has("stock_in_oxytocin")?mis3_json.getString("stock_in_oxytocin"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_oxytocin")?mis3_json.getString("adjustment_stock_plus_oxytocin"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_oxytocin")?mis3_json.getString("adjustment_stock_minus_oxytocin"):"0")) + Integer.parseInt("-" + (mis3_json.has("deliverycxytocin")?mis3_json.getString("deliverycxytocin"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mnp")?mis3_json.getString("previous_stock_mnp"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mnp")?mis3_json.getString("stock_in_mnp"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mnp")?mis3_json.getString("adjustment_stock_plus_mnp"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mnp")?mis3_json.getString("adjustment_stock_minus_mnp"):"0")) + Integer.parseInt("-" + (mis3_json.has("mnp")?mis3_json.getString("mnp"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_sanitary_pad")?mis3_json.getString("previous_stock_sanitary_pad"):"0")) + Integer.parseInt((mis3_json.has("stock_in_sanitary_pad")?mis3_json.getString("stock_in_sanitary_pad"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_sanitary_pad")?mis3_json.getString("adjustment_stock_plus_sanitary_pad"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_sanitary_pad")?mis3_json.getString("adjustment_stock_minus_sanitary_pad"):"0")) + Integer.parseInt("-" + (mis3_json.has("sanitary_pad")?mis3_json.getString("sanitary_pad"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_mr_kit")?mis3_json.getString("previous_stock_mr_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_mr_kit")?mis3_json.getString("stock_in_mr_kit"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_mr_kit")?mis3_json.getString("adjustment_stock_plus_mr_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_mr_kit")?mis3_json.getString("adjustment_stock_minus_mr_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("mr_kit")?mis3_json.getString("mr_kit"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_delivery_kit")?mis3_json.getString("previous_stock_delivery_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_delivery_kit")?mis3_json.getString("stock_in_delivery_kit"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_delivery_kit")?mis3_json.getString("adjustment_stock_plus_delivery_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_delivery_kit")?mis3_json.getString("adjustment_stock_minus_delivery_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("delivery_kit")?mis3_json.getString("delivery_kit"):"0"))) + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (Integer.parseInt((mis3_json.has("previous_stock_dds_kit")?mis3_json.getString("previous_stock_dds_kit"):"0")) + Integer.parseInt((mis3_json.has("stock_in_dds_kit")?mis3_json.getString("stock_in_dds_kit"):"0")) + Integer.parseInt((mis3_json.has("adjustment_stock_plus_dds_kit")?mis3_json.getString("adjustment_stock_plus_dds_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("adjustment_stock_minus_dds_kit")?mis3_json.getString("adjustment_stock_minus_dds_kit"):"0")) + Integer.parseInt("-" + (mis3_json.has("dds_kit")?mis3_json.getString("dds_kit"):"0"))) + "</td>"
                    + "</tr>";

            resultString += "<tr>"
                    + "<td colspan='2' class='danger' style='text-align: left;border-top: 1px solid black;'>চলতি মাসে<br>কখনও মওজুদ<br>শূণ্য হয়ে থাকলে<br>(কোড)</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_shukhi")?mis3_json.getString("zero_stock_code_shukhi"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_apon")?mis3_json.getString("zero_stock_code_apon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_condom")?mis3_json.getString("zero_stock_code_condom"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_viale")?mis3_json.getString("zero_stock_code_viale"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_syringe")?mis3_json.getString("zero_stock_code_syringe"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_iud")?mis3_json.getString("zero_stock_code_iud"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_implanon")?mis3_json.getString("zero_stock_code_implanon"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_jadelle")?mis3_json.getString("zero_stock_code_jadelle"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_ecp")?mis3_json.getString("zero_stock_code_ecp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_misoprostol")?mis3_json.getString("zero_stock_code_misoprostol"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_mrm_pac")?mis3_json.getString("zero_stock_code_mrm_pac"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_chlorehexidin")?mis3_json.getString("zero_stock_code_chlorehexidin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_mgso4")?mis3_json.getString("zero_stock_code_mgso4"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_oxytocin")?mis3_json.getString("zero_stock_code_oxytocin"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_mnp")?mis3_json.getString("zero_stock_code_mnp"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_sanitary_pad")?mis3_json.getString("zero_stock_code_sanitary_pad"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_mr_kit")?mis3_json.getString("zero_stock_code_mr_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_delivery_kit")?mis3_json.getString("zero_stock_code_delivery_kit"):"0") + "</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>" + (mis3_json.has("zero_stock_code_dds_kit")?mis3_json.getString("zero_stock_code_dds_kit"):"0") + "</td>"
                    + "</tr>";

            resultString += "</table>";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return resultString;
    }

    public static String getCSBATable(JSONObject mis3_json, String mis3ViewType){
        String resultString = "";
        String anc1text="", anc2text="", anc3text="", anc4text="", pncm1text="", pncm2text="", pncm3text="", pncm4text="", pncn1text="", pncn2text="", pncn3text="", pncn4text="";
        try {
            resultString += "<table class=\"table page_1 table_hide\" id=\"table_1\" width=\"700\" border=\"1\" cellspacing=\"0\" style='font-size:11px;'><tr class='danger'>"
                    + "<td colspan='4' style='text-align: center;border-top: 1px solid black;border-left: 1px solid black;'>সেবার ধরণ</td>"
                    + "<td style='text-align: center;border-top: 1px solid black;'>মোট</td>"
                    + "</tr>";

            //resultString += "<tr class='danger'><td rowspan='28' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>প্রজনন স্বাস্থ্য সেবা</sapn></td></tr>";

                        /*resultString +=  "<tr>"
                                        + "<td rowspan='5' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;font-size: 9px;margin-top: 20px;'>টি টি প্রাপ্ত ম্হিলা</span></td>"
                                        + "<td colspan='3' style='border-top: 1px solid black;'>১ম ডোজ</td>"
                                        + "<td align='center' style='border-top: 1px solid black;'>0</td>"
                                        + "</tr>";

                        resultString +=  "<tr class='success'>"
                                        + "<td colspan='3'>২ম ডোজ</td>"
                                        + "<td align='center'>0</td>"
                                        + "</tr>";

                        resultString +=  "<tr>"
                                        + "<td colspan='3'>৩ম ডোজ</td>"
                                        + "<td align='center'>0</td>"
                                        + "</tr>";

                        resultString +=  "<tr class='success'>"
                                        + "<td colspan='3'>৪ম ডোজ</td>"
                                        + "<td align='center'>0</td>"
                                        + "</tr>";

                        resultString +=  "<tr>"
                                        + "<td colspan='3'>৫ম ডোজ</td>"
                                        + "<td align='center'>0</td>"
                                        + "</tr>";
                        */

            resultString +=  "<tr>"
                    + "<td rowspan='4' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);'>গর্ভবতী মহিলার যত</span></td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>" + anc1text + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("anc1center")) + Integer.parseInt(mis3_json.getString("anc1satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + anc2text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("anc2center")) + Integer.parseInt(mis3_json.getString("anc2satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + anc3text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("anc3center")) + Integer.parseInt(mis3_json.getString("anc3satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + anc4text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("anc4center")) + Integer.parseInt(mis3_json.getString("anc4satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='3' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>প্রসবকালীন সেবা</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>মোট ডেলিভারীর সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("normaldelivery")) + Integer.parseInt(mis3_json.getString("csectiondelivery"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনা (অগঞঝখ) অনুসরন করে ডেলিভারীর সংখ্যা</td>"
                    + "<td align='center'>" + mis3_json.getString("amtsl") + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>মোট কতজনকে মিসোপ্রোস্টল বড়ি খাওয়ানো হয়েছে</td>"
                    + "<td align='center'>" + mis3_json.getString("deliverymisoprostol") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='9' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>নবজাতক সংক্রান্ত তথ্য</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>জীবিত জন্ম (Live Birth)</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + mis3_json.getString("livebirth") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>মৃত জন্ম (Still Birth)</td>"
                    + "<td align='center'>" + mis3_json.getString("stillbirth") + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='5' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>নবজাতকের  তাৎক্ষনিক পরিচর্যা</td>"
                    + "<td style='border-top: 1px solid black;'>কতজনকে ১ মিনিটের মধ্যে মোছানো হয়েছে</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("dryingafterbirth")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td style='border-top: 1px solid black;'>কতজনের ক্ষেত্রে নাড়িকাটার পর পর মায়ের ত্বকে লাগানো হয়েছে </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("skintouch")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td style='border-top: 1px solid black;'>কতজনের ক্ষেত্রে নাড়িকাটার পর ক্লোরহেক্সিডিন ব্যবহার করা হয়েছে</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("chlorehexidin")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td style='border-top: 1px solid black;'>কতজনকে জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("breastfeed")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td style='border-top: 1px solid black;'>জন্মকালীন শ্বাসকষ্টে আক্রান্ত কতজনকে ব্যাগ ও মাস্ক ব্যবহার করে রিসাসিটেট করা হয়েছে </td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("bagnmask")+"</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>কম ওজন (২.৫ কেজির কম) নিয়ে জন্মগ্রহনকারী নবজাতকের সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("lowbirthweight")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>অপরিনত (৩৭ সপ্তাহের আগে জন্ম নেয়া) নবজাতকের সংখ্যা</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>"+mis3_json.getString("immaturebirth")+"</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td rowspan='9' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>প্রসবোত্তর সেবা</span></td>"
                    + "<td rowspan='5' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>মা</span></td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>" + pncm1text + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("pncmother1center")) + Integer.parseInt(mis3_json.getString("pncmother1satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + pncm2text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmother2center")) + Integer.parseInt(mis3_json.getString("pncmother2satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + pncm3text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmother3center")) + Integer.parseInt(mis3_json.getString("pncmother3satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + pncm4text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmother4center")) + Integer.parseInt(mis3_json.getString("pncmother4satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি প্রদানের সংখ্যা</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncmotherfpcenter")) + Integer.parseInt(mis3_json.getString("pncmotherfpsatelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='4' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;'>নবজাতক</span></td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>" + pncn1text + "</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("pncchild1center")) + Integer.parseInt(mis3_json.getString("pncchild1satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + pncn2text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncchild2center")) + Integer.parseInt(mis3_json.getString("pncchild2satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>" + pncn3text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncchild3center")) + Integer.parseInt(mis3_json.getString("pncchild3satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>" + pncn4text + "</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("pncchild4center")) + Integer.parseInt(mis3_json.getString("pncchild4satelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td rowspan='3' colspan='2' class='danger' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>রেফারেল</td>"
                    + "<td colspan='2' style='border-top: 1px solid black;'>মোট কতজনকে গর্ভকালীন, প্রসবকালীন ও গর্ভোত্তর জটিলতার জন্য রেফার করা হয়েছে</td>"
                    + "<td align='center' style='border-top: 1px solid black;'>" + (Integer.parseInt(mis3_json.getString("ancrefercenter")) + Integer.parseInt(mis3_json.getString("deliveryrefercenter")) + Integer.parseInt(mis3_json.getString("pncmotherrefercenter")) + Integer.parseInt(mis3_json.getString("ancrefersatelite")) + Integer.parseInt(mis3_json.getString("deliveryrefersatelite")) + Integer.parseInt(mis3_json.getString("pncmotherrefersatelite"))) + "</td>"
                    + "</tr>";

            resultString +=  "<tr>"
                    + "<td colspan='2'>মোট কতজন একলামসিয়া রোগীকে MgSO4 ইনজেকশন দিয়ে রেফার করা হয়েছে</td>"
                    + "<td align='center'>0</td>"
                    + "</tr>";

            resultString +=  "<tr class='success'>"
                    + "<td colspan='2'>মোট কতজন নবজাতককে জটিলতার জন্য রেফার করা হয়েছে</td>"
                    + "<td align='center'>" + (Integer.parseInt(mis3_json.getString("newbornreferalcenter")) + Integer.parseInt(mis3_json.getString("newbornreferalsatelite"))) + "</td>"
                    + "</tr>";

            resultString += "</table>";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return resultString;
    }
}
