package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.model.GetResult;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SatelitePlanningSubmitInfo {

    public static String getApprovalList(JSONObject infoApproval) {
        String zilla = "";
        //List<String> upazilaList = new ArrayList<String>();
//        String upazila = "";
        String up = "";
        String approvalList = "";
        String monthFIlter = "";

        DBSchemaInfo table_schema = new DBSchemaInfo("36");
//        String supervisorId = infoApproval.getString("supervisorId");
        if (infoApproval.has("month")) {
            String[] yearMonth = infoApproval.getString("month").split("-");
            monthFIlter = "and " + table_schema.getColumn("table", "satelite_session_planning_year", new String[]{yearMonth[0]}, "=") + " and " + table_schema.getColumn("table", "satelite_session_planning_month", new String[]{yearMonth[1]}, "=");
        }

        try {
            ResultSet rs = getsql_supervisorInfo(infoApproval);
            while (rs.next()) {
                zilla = rs.getString("zilla_id");
//            upazilaList = Arrays.asList(rs.getString("upazila_id"));
                up = rs.getString("upazila_id");
                //["2","5","11","26","44","68","71","77"]
                up = up.replaceAll("[\\[\\]]", "");
            }

            String[] upList = up.split(",");

            if (!rs.isClosed()) {
                rs.close();
            }

            table_schema = new DBSchemaInfo(zilla);


            String resultString = GetResultSet.getTableHeader();

            for (String upazila : upList) {

                upazila = upazila.replaceAll("\"", "");

                String sql_submissionInfo = "select " + table_schema.getColumn("", "satelite_session_planning_planning_id") + " as planning_id, " + table_schema.getColumn("", "satelite_session_planning_year") + " as planning_year, " +
                        "to_char(TO_DATE (" + table_schema.getColumn("", "satelite_session_planning_month") + "::text, 'MM'), 'Month') as planning_month," +
                        table_schema.getColumn("", "satelite_session_planning_submit_date") + " as submit_date, " +
                        table_schema.getColumn("", "providerdb_facilityname") + " as facility_name, " + table_schema.getColumn("", "providerdb_provname") + " as provider_name, " + table_schema.getColumn("", "satelite_session_planning_status") + " as status " +
                        "from " + table_schema.getTable("table_satelite_session_planning") +
                        "inner join " + table_schema.getTable("table_facility_registry") + " on " + table_schema.getColumn("table", "facility_registry_facilityid") + "=" + table_schema.getColumn("table", "satelite_session_planning_facility_id") +
                        "inner join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "satelite_session_planning_providerid") +
                        "and " + table_schema.getColumn("table", "providerdb_systementrydate") + " = " +
                        "(select max(" + table_schema.getColumn("table", "providerdb_systementrydate") + ")from " + table_schema.getTable("table_providerdb") +
                        "where " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "satelite_session_planning_providerid") + ")" +
                        " where " + table_schema.getColumn("table", "satelite_session_planning_status", new String[]{"0", "3"}, "in") + " " +
                        monthFIlter +
                        " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=");


//                String sql_submissionInfo = "select " + table_schema.getColumn("", "satelite_session_planning_planning_id") + " as planning_id, " + table_schema.getColumn("", "satelite_session_planning_year") + " as planning_year, " +
//                        table_schema.getColumn("", "satelite_session_planning_submit_date") + " as submit_date, " +
//                        table_schema.getColumn("", "providerdb_facilityname") + " as facility_name, " + table_schema.getColumn("", "providerdb_provname") + " as provider_name, " + table_schema.getColumn("", "satelite_session_planning_status") + " as status " +
//                        "from " + table_schema.getTable("table_satelite_session_planning") +
//                        "inner join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "satelite_session_planning_providerid") + " where " + table_schema.getColumn("table", "satelite_session_planning_status", new String[]{"0", "3"}, "in") + " " +
//                        monthFIlter +
//                        " and " + table_schema.getColumn("table", "providerdb_zillaid", new String[]{zilla}, "=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid", new String[]{upazila}, "=");
//                System.out.println(sql_submissionInfo);

                approvalList = GetResultSet.getSatelitePlanningSubmissionList(sql_submissionInfo, zilla, upazila, resultString);
                resultString = approvalList;
            }
            approvalList = approvalList + "</tbody></table>";

        } catch (Exception e) {
            e.printStackTrace();
        }

        return approvalList;
    }

    public static ResultSet getsql_supervisorInfo(JSONObject infoApproval) {
        ResultSet rs = null;
        DBSchemaInfo table_schema = new DBSchemaInfo("36");
        String supervisorId = infoApproval.getString("supervisorId");

        try {
            DBOperation dbOp = new DBOperation();
            DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();

            String sql_mis3_approver = "select " + table_schema.getColumn("", "mis3_approver_approval_zilla") + " as zilla_id, " + table_schema.getColumn("", "mis3_approver_approval_upazilas") + " as upazila_id from "
                    + table_schema.getTable("table_mis3_approver")
                    + " where " + table_schema.getColumn("", "mis3_approver_providerid", new String[]{supervisorId}, "=") + " and " + table_schema.getColumn("", "mis3_approver_active", new String[]{"1"}, "=");

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql_mis3_approver);
            rs = dbObject.getResultSet();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;

    }

    public static JSONObject getPlanningInfo(JSONObject reportCred) {
        String zilla = "";
        String upazila = "";
        String planning_id = "";
        String[] planning_date = null;

        JSONObject sessionPlanningData = new JSONObject();

        zilla = reportCred.getString("zilla");
        upazila = reportCred.getString("upazila");
        planning_id = reportCred.getString("planningId");

        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
//        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            String sql_getSessionPlanningData = "select " + table_schema.getColumn("", "satelite_planning_detail_schedule_date") + " as schedule_date, " + table_schema.getColumn("", "satelite_planning_detail_satelite_name") + " as satelite_name, " +
                    table_schema.getColumn("", "satelite_planning_detail_fwa_name") + " as fwa_name, " + table_schema.getColumn("", "satelite_planning_detail_villageid") + " as villageid " +
                    "from " + table_schema.getTable("table_satelite_planning_detail") + " where " + table_schema.getColumn("", "satelite_planning_detail_planning_id", new String[]{planning_id}, "=");

            System.out.println(sql_getSessionPlanningData);
            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql_getSessionPlanningData);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();

            int j = 0;
            //sessionPlanningData += "[";
            while (rs.next()) {
                j++;
                JSONObject data = new JSONObject();
                data.put("title", rs.getString("satelite_name"));
                data.put("start", rs.getString("schedule_date"));
                data.put("fwa", rs.getString("fwa_name"));
                data.put("village", rs.getString("villageid"));
                sessionPlanningData.put(Integer.toString(j), data);

            }
//            System.out.println(sessionPlanningData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sessionPlanningData;
    }

    public static void updateApprovalStatus(JSONObject infoApproval) {
        /**
         * status = 0, submitted
         * status = 1, approved
         * status = 2, reject
         * status = 3, waiting for supervisor approval
         */
        String zilla = "";
        String upazila = "";
        String planning_id = "";
        String status = "";
        String approveSatelite_sql = "";
        String rejectSatelite_sql = "";
        String comment = "";
        String providerId = "";
        String notice = "Apnar satelite plan onumodon kora hoese";

        status = infoApproval.getString("approve_status");
        zilla = infoApproval.getString("zillaid");
        upazila = infoApproval.getString("upazila_id");
        planning_id = infoApproval.getString("planning_id");
        if (status.equals("2")) {
            comment = infoApproval.getString("reject_comment");
        }
//        ResultSet rs = getsql_supervisorInfo(infoApproval);

        try {

//            while (rs.next()){


            DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
            FacilityDB dbFacility = new FacilityDB();
            DBOperation dbOp = new DBOperation();
//            DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
            DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

            String sql_providerid = "select providerid from " + table_schema.getTable("table_satelite_session_planning")
                    + "where " + table_schema.getColumn("", "satelite_session_planning_planning_id", new String[]{planning_id}, "=");

            ResultSet rs = dbOp.dbExecute(dbObject, sql_providerid).getResultSet();
            while (rs.next()) {
                providerId = rs.getString("providerid");

            }


            if (status.equals("1")) {

                approveSatelite_sql = "update " + table_schema.getTable("table_satelite_session_planning")
                        + " set " + table_schema.getColumn("", "satelite_session_planning_status") + "=" + status
                        + " ," + table_schema.getColumn("", "satelite_session_planning_approve_date") + "=CURRENT_TIMESTAMP "
                        + " where " + table_schema.getColumn("", "satelite_session_planning_zillaid", new String[]{zilla}, "=")
                        + " and " + table_schema.getColumn("", "satelite_session_planning_upazilaid", new String[]{upazila}, "=")
                        + " and " + table_schema.getColumn("", "satelite_session_planning_planning_id", new String[]{planning_id}, "=");
                dbOp.dbExecuteUpdate(dbObject, approveSatelite_sql);

                String updateNotice_sql = "update" + table_schema.getTable("table_node_details")
                        + " set " + table_schema.getColumn("", "node_details_changerequest") + "= " + "'{\"notice\":\"" + notice + "\"}'"
                        + "where " + table_schema.getColumn("", "node_details_provcode", new String[]{providerId}, "=");
                dbOp.dbExecuteUpdate(dbObject, updateNotice_sql);

            } else {
                rejectSatelite_sql = "update " + table_schema.getTable("table_satelite_session_planning")
                        + " set " + table_schema.getColumn("", "satelite_session_planning_status") + "=" + status
                        + " ," + table_schema.getColumn("", "satelite_session_planning_comments") + "= \'" + comment + "\'"
                        + " where " + table_schema.getColumn("", "satelite_session_planning_zillaid", new String[]{zilla}, "=")
                        + " and " + table_schema.getColumn("", "satelite_session_planning_upazilaid", new String[]{upazila}, "=")
                        + " and " + table_schema.getColumn("", "satelite_session_planning_planning_id", new String[]{planning_id}, "=");
                dbOp.dbExecuteUpdate(dbObject, rejectSatelite_sql);

                notice = "Apnar satelite plan batil kora hoese";

                String updateNotice_sql = "update" + table_schema.getTable("table_node_details")
                        + " set " + table_schema.getColumn("", "node_details_changerequest") + "= " + "'{\"notice\":\"" + notice + "\"}'"
                        + "where " + table_schema.getColumn("", "node_details_provcode", new String[]{providerId}, "=");
                dbOp.dbExecuteUpdate(dbObject, updateNotice_sql);

            }

//            }


        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public static String satelitePlanningSubmisssionInfo(JSONObject reportCred){
        JSONObject sspSubmissionInfo = new JSONObject();

        DBSchemaInfo table_schema = new DBSchemaInfo();

        String[] report_month_year = reportCred.getString("report1_month").split("-");
        int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(report_month_year[0]), Integer.parseInt(report_month_year[1]));
        String report_month_start_date = report_month_year[0]+"-"+report_month_year[1]+"-01";
        String report_month_end_date = report_month_year[0]+"-"+report_month_year[1]+"-"+daysInMonth;
        String zilla = reportCred.getString("report1_zilla");
        String upazila = null;
        if(!reportCred.getString("report1_upazila").equals(""))
            upazila = reportCred.getString("report1_upazila");

        String inactive_facility_query = "select "+table_schema.getColumn("","facility_provider_facility_id")+" from " +
                "(select distinct("+table_schema.getColumn("","facility_provider_facility_id")+") as facility_id " +
                "from "+ table_schema.getTable("table_facility_provider") +" " +
                "join "+ table_schema.getTable("table_facility_registry") +" on "+table_schema.getColumn("table","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
                "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" and "+table_schema.getColumn("table","providerdb_endate")+"::date="+table_schema.getColumn("table","facility_provider_start_date")+"::date and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","101","17"},"in")+" " +
                "left join "+ table_schema.getTable("table_associated_provider_id") +" on "+table_schema.getColumn("table","associated_provider_id_associated_id")+"="+table_schema.getColumn("table","facility_provider_provider_id")+" "+
                "left join "+ table_schema.getTable("table_provider_leave_status") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","provider_leave_status_providerid")+" or "+table_schema.getColumn("table","provider_leave_status_providerid")+"="+table_schema.getColumn("table","associated_provider_id_provider_id")+" " +
                "where ("+table_schema.getColumn("","facility_provider_is_active",new String[]{"2"},"=")+" and "+table_schema.getColumn("table","facility_provider_end_date")+"<='"+report_month_end_date+"') " +
                "or ("+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+" and "+table_schema.getColumn("table","facility_provider_start_date")+">'"+report_month_end_date+"') " +
                "or ("+table_schema.getColumn("table","provider_leave_status_providerid")+" is not null and ("+table_schema.getColumn("table","provider_leave_status_leave_end_date")+" is null or "+table_schema.getColumn("table","provider_leave_status_leave_end_date")+">'"+report_month_end_date+"')) " +
                "and ("+table_schema.getColumn("table", "providerdb_provcode",new String[]{},"isnotnull")+" or "+table_schema.getColumn("table", "associated_provider_id_associated_id",new String[]{},"isnotnull")+")) as inactive_provider " +
                "left join " +
                "(select distinct("+table_schema.getColumn("","facility_provider_facility_id")+") as facility_id  " +
                "from "+ table_schema.getTable("table_facility_provider") +" " +
                "join "+ table_schema.getTable("table_facility_registry") +" on "+table_schema.getColumn("table","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
                "left join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" and "+table_schema.getColumn("table","providerdb_endate")+"::date="+table_schema.getColumn("table","facility_provider_start_date")+"::date and "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","101","17"},"in")+" " +
                "left join "+ table_schema.getTable("table_associated_provider_id") +" on "+table_schema.getColumn("table","associated_provider_id_associated_id")+"="+table_schema.getColumn("table","facility_provider_provider_id")+" "+
                "left join "+ table_schema.getTable("table_provider_leave_status") +" on "+table_schema.getColumn("table","facility_provider_provider_id")+"="+table_schema.getColumn("table","provider_leave_status_providerid")+" or "+table_schema.getColumn("table","provider_leave_status_providerid")+"="+table_schema.getColumn("table","associated_provider_id_provider_id")+" " +
                "where (("+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+" and "+table_schema.getColumn("table","facility_provider_start_date")+"<='"+report_month_end_date+"') or ("+table_schema.getColumn("","facility_provider_is_active",new String[]{"2"},"=")+" and "+table_schema.getColumn("table","facility_provider_end_date")+">'"+report_month_end_date+"'))  " +
                "and (("+table_schema.getColumn("table","provider_leave_status_providerid")+" is not null and "+table_schema.getColumn("table","provider_leave_status_leave_end_date")+"<='"+report_month_end_date+"') or "+table_schema.getColumn("table","provider_leave_status_leave_end_date")+" is null) " +
                "and ("+table_schema.getColumn("table", "providerdb_provcode",new String[]{},"isnotnull")+" or "+table_schema.getColumn("table", "associated_provider_id_associated_id",new String[]{},"isnotnull")+")) as active_provider using("+table_schema.getColumn("","facility_provider_facility_id")+") " +
                "where active_provider.facility_id is null";

        try {
            String sql = "select count(*) as total_facility, " +
                    "(count(*)-count(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 1 else null end)) as active_facility, "+
                    "count(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 1 else null end) as inactive_facility, "+
                    "count("+table_schema.getColumn("table","satelite_session_planning_facility_id")+") as submited, " +
                    "((count(*)-count(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 1 else null end))-count("+table_schema.getColumn("table","satelite_session_planning_facility_id")+")) as not_submited, " +
                    "count(case when "+table_schema.getColumn("","satelite_session_planning_status",new String[]{"1", "4"},"in")+" then 1 else null end) as approved, " +
                    "count(case when "+table_schema.getColumn("","satelite_session_planning_status",new String[]{"0","3"},"in")+" then 1 else null end) as approve_waiting, " +
                    "count(case when "+table_schema.getColumn("","satelite_session_planning_status",new String[]{"2"},"=")+" then 1 else null end) as rejected " +
                    "from "+ table_schema.getTable("table_facility_registry") +" " +
                    "join ( select distinct("+table_schema.getColumn("table","facility_provider_facility_id")+") as facility_id from "
                    + table_schema.getTable("table_facility_provider") +" " +"join"+ table_schema.getTable("table_associated_provider_id")+" on"+table_schema.getColumn("table","associated_provider_id_associated_id")+"="+table_schema.getColumn("table","facility_provider_provider_id")+" " +
                    "where"+table_schema.getColumn("table","facility_provider_start_date")+"<='"+report_month_end_date+"' " +
                    "union select distinct("+table_schema.getColumn("table","facility_provider_facility_id")+") as facility_id from "+ table_schema.getTable("table_facility_provider") +" " +
                    "join "+ table_schema.getTable("table_providerdb") +" using("+table_schema.getColumn("","providerdb_provcode")+") " +
                    "where "+table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","101","17"},"in")+" " +
                    "and "+table_schema.getColumn("table","facility_provider_start_date")+" < '"+report_month_end_date+"') "+
                    "as fp on "+table_schema.getColumn("fp","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
                    "left join("+inactive_facility_query+") inactive_facility using ("+table_schema.getColumn("","facility_provider_facility_id")+") " +
                    "left join "+ table_schema.getTable("table_satelite_session_planning") +" on "+table_schema.getColumn("table","facility_registry_facilityid")+"="+table_schema.getColumn("table","satelite_session_planning_facility_id")+" " +
                    "and "+table_schema.getColumn("","satelite_session_planning_month",new String[]{report_month_year[1]},"=")+" and "+table_schema.getColumn("","satelite_session_planning_year",new String[]{report_month_year[0]},"=")+" " +
                    "where "+table_schema.getColumn("table","facility_registry_zillaid",new String[]{zilla},"=")+" ";

            if(!reportCred.getString("report1_upazila").equals(""))
                sql += "and "+table_schema.getColumn("table","facility_registry_upazilaid",new String[]{upazila},"=")+" ";

//			sql += "and "+table_schema.getColumn("table","facility_registry_facilityid")+" in " +
//					"(select distinct("+table_schema.getColumn("","facility_provider_facility_id")+") from "+ table_schema.getTable("table_facility_provider") +" where "+table_schema.getColumn("","facility_provider_is_active",new String[]{"1"},"=")+")";

            sspSubmissionInfo = sspSubmissionInfo.put("Aggregated", GetResultSet.getMIS3SubmissionInfo(sql, zilla));

            String sql_all_facility = "select "+table_schema.getColumn("","facility_registry_facility_name")+" as \"Facility Name\", initcap("+table_schema.getColumn("","upazila_upazilanameeng")+") as \"Upazila Name\", " +
                    "initcap("+table_schema.getColumn("","unions_unionnameeng")+") as \"Union Name\", "
                    + table_schema.getColumn("mrs","providerdb_provname")+ "as \"Provider Details\","+
                    "(case when "+table_schema.getColumn("inactive_facility", "facility_provider_facility_id", new String[]{}, "isnotnull")+" then 10 else "+table_schema.getColumn("","satelite_session_planning_status")+" end) as \"Status\" " +
                    "from "+ table_schema.getTable("table_facility_registry") +" " +
                    "left join("+inactive_facility_query+") inactive_facility on "+table_schema.getColumn("inactive_facility","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" "+
                    "left join (" +
                    "select "+table_schema.getColumn("","satelite_session_planning_facility_id")+", "+table_schema.getColumn("","satelite_session_planning_status")+" " +
                    ", "+table_schema.getColumn("","providerdb_provname")+" " +
                    "from "+ table_schema.getTable("table_satelite_session_planning") +" " +
                    "inner join " + table_schema.getTable("table_providerdb") + " on " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "satelite_session_planning_providerid") +
                    "and " + table_schema.getColumn("table", "providerdb_systementrydate") + " = " +
                    "(select max(" + table_schema.getColumn("table", "providerdb_systementrydate") + ")from " + table_schema.getTable("table_providerdb") +
                    "where " + table_schema.getColumn("table", "providerdb_provcode") + "=" + table_schema.getColumn("table", "satelite_session_planning_providerid") + ")" +
                    "where "+table_schema.getColumn("","satelite_session_planning_year",new String[]{report_month_year[0]},"=")+" and "+table_schema.getColumn("","satelite_session_planning_month",new String[]{report_month_year[1]},"=")+" " +
                    ") mrs on "+table_schema.getColumn("mrs","facility_provider_facility_id")+"="+table_schema.getColumn("table","facility_registry_facilityid")+" " +
                    "left join "+ table_schema.getTable("table_upazila") +" on "+table_schema.getColumn("table","upazila_zillaid")+"="+table_schema.getColumn("table","facility_registry_zillaid")+" and "+table_schema.getColumn("table","upazila_upazilaid")+"="+table_schema.getColumn("table","facility_registry_upazilaid")+" " +
                    "left join "+ table_schema.getTable("table_unions") +" on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","facility_registry_zillaid")+" and "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","facility_registry_upazilaid")+" and "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","facility_registry_unionid")+" " +
                    "where "+table_schema.getColumn("table","facility_registry_zillaid",new String[]{zilla},"=")+" and "+table_schema.getColumn("inactive_facility","facility_provider_facility_id")+ "is null";

            if(!reportCred.getString("report1_upazila").equals(""))
                sql_all_facility += " and "+table_schema.getColumn("table","facility_registry_upazilaid",new String[]{upazila},"=");

            System.out.println(sql_all_facility);
            sspSubmissionInfo = sspSubmissionInfo.put("Individual", getSateliteSubmissionDetails(sql_all_facility, zilla, report_month_year[0], report_month_year[1]));
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return sspSubmissionInfo.toString();
    }

    public static String getSateliteSubmissionDetails(String sql, String zilla, String year, String month) {
        String resultString = "";
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);

        try {

            dbObject = dbOp.dbCreateStatement(dbObject);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            resultString = "<table class=\"table table-striped table-bordered nowrap reportExport\" id=\"reportDataTable\" cellspacing=\"0\" width=\"100%\"><thead><tr>";

            for (int i = 1; i <= columnCount ; i++) {
                resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
            }

            resultString = resultString + "</tr></thead><tbody>";
            int j = 0;
            String status = "";
            while (rs.next()) {
                j++;
                if ((j % 2) == 0) {
                    resultString = resultString + "<tr>";
                } else {
                    resultString = resultString + "<tr>";
                }

                for (int i = 1; i <= columnCount ; i++) {
                    if (metadata.getColumnName(i).equals("Status")) {
                        status = rs.getString(metadata.getColumnName(i));
                        if (status == null)
                            resultString = resultString + "<td><span class='label label-warning'>Not Submitted</span></td>";
                        else if (status.equals("10"))
                            resultString = resultString + "<td><span class='label label-warning'>Inactive Facility</span></td>";
//						else if(status.equals("1"))
//							resultString = resultString + "<td><span class='label label-warning'>DHIS2 Submit Remaining</span></td>";
                        else if (status.equals("0") || status.equals("3"))
                            resultString = resultString + "<td><span class='label label-primary'>Waiting for Approval</span></td>";
                        else if (status.equals("2"))
                            resultString = resultString + "<td><span class='label label-danger'>Rejected</span></td>";
                        else if (status.equals("4") || status.equals("1"))
                            resultString = resultString + "<td><span class='label label-success'>Approved</span></td>";
                    } else
                        resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))) + "</td>";
                }
//                if (status != null && rs.getString("Status").equals("10"))
//                    resultString = resultString + "<td></td>";
//                else
//                    resultString = resultString + "<td><button class='btn btn-info' onclick='viewMIS3(\"" + zilla + "\",\"" + rs.getString("facility_id") + "\",\"" + year + "-" + month + "\")'>View Details</button></td>";
//
               resultString = resultString + "</tr>";
            }

            resultString = resultString + "</tbody></table>";

            if (!rs.isClosed()) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        }
        return resultString;

    }
}
