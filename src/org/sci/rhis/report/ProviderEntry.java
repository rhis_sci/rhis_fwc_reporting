package org.sci.rhis.report;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.json.JSONObject;
import org.sci.rhis.db.ConfInfoRetrieve;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class ProviderEntry {
	public static Integer zilla;
	public static String zillaname;
	public static Integer upazila;
	public static Integer union;
	public static String unionname;
	public static Integer providerType;
	public static Integer providerCode;
	public static String providerPass;
	public static String providerName;
	public static String providerMob;
	public static String facilityName;
	public static String base_url;
	public static String sync_url;
	public static String sql_providerdb;
	public static String sql_nodeDetails;
	public static String sql_sym_node;
	public static String DB_name;
	public static String servername;
	public static String nodename;
	public static String changetype;
	public static String version;
	public static String sql_assignAppVersion;
	
	public static boolean saveProvider(JSONObject formValue){
		
		zilla = Integer.parseInt(formValue.getString("zillaid"));
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
		boolean status = false;
		
		DBInfoHandler detailDomain = new DBInfoHandler();
		Properties prop = ConfInfoRetrieve.readingConf(detailDomain.getDomainLoc());
		String[] DomainDetails = prop.getProperty("DOMAININFO_"+zilla).split(",");
		
		String host=DomainDetails[0];
	    String user=DomainDetails[1];
	    String password=DomainDetails[2];
	    Integer portNum=Integer.parseInt(DomainDetails[3]);
		
		try{
			//zilla = Integer.parseInt(formValue.getString("zillaid"));
			zillaname = formValue.getString("zillaname");
			upazila = Integer.parseInt(formValue.getString("upazilaid"));
			union = Integer.parseInt(formValue.getString("unionid"));
			unionname = formValue.getString("unionname");
			providerType = Integer.parseInt(formValue.getString("providertype"));
			providerCode = Integer.parseInt(formValue.getString("providercode"));
			providerPass = formValue.getString("providerpass");
			providerName = formValue.getString("providername");
			providerMob = formValue.getString("providermobile");
			
			base_url = "http://10.12.2.18:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";
			sync_url = "http://10.12.2.18:"+zilla+String.format("%02d", upazila)+"0/sync/";
			
			/*if(zilla.equals(36) && upazila.equals(71))
				base_url = "http://mamoni.net:8080/RHIS_FWC/";
			else if(zilla.equals(93))
				base_url = "http://mchdrhis.icddrb.org:8080/eMIS/";
			else
				base_url = "http://mamoni.net:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";
			
			else if(zilla.equals(93) && upazila.equals(9))
				base_url = "http://mchdrhis.icddrb.org:8080/CcahWebservice_facility/";
			else if(zilla.equals(93))
				base_url = "http://mchdrhis.icddrb.org:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";
			else
				base_url = "http://mamoni.net:8080/RHIS_FWC_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila)+"/";
			
			if(zilla.equals(36) && upazila.equals(71))
				sync_url = "http://mamoni.net:31415/sync/";
			else if(zilla.equals(93) && upazila.equals(9))
				sync_url = "http://mchdrhis.icddrb.org:31415/sync/tangail-93-9";
			else
			{
				Integer zilla_port;
				if(zilla<10)
				{
					zilla_port = zilla+10;
					if(zilla.equals(93))
						sync_url = "http://mchdrhis.icddrb.org:"+zilla_port+String.format("%02d", upazila)+"4/sync/";
					else
						sync_url = "http://mamoni.net:"+zilla_port+String.format("%02d", upazila)+"4/sync/";
				}	
				else{
					if(zilla>64)
					{
						zilla_port = zilla-55;
						if(zilla.equals(93))
							sync_url = "http://mchdrhis.icddrb.org:"+zilla_port+String.format("%02d", upazila)+"2/sync/";
						else
							sync_url = "http://mamoni.net:"+zilla_port+String.format("%02d", upazila)+"2/sync/";
					}
					else
					{
						if(zilla.equals(93))
							sync_url = "http://mchdrhis.icddrb.org:"+zilla+String.format("%02d", upazila)+"0/sync/";
						else
							sync_url = "http://mamoni.net:"+zilla+String.format("%02d", upazila)+"0/sync/";
					}
				}
			}*/
			//facilityName = formValue.get("facilityname");
			
			if(providerType==999 || providerType==998 || providerType==994)
			{
				sql_providerdb = "insert into \"ProviderDB\" (\"Divid\", zillaid, upazilaid, unionid, "
						+"\"ProvType\", \"ProvCode\", \"ProvName\", \"MobileNo\", \"EnDate\", \"ExDate\", "
						+"\"Active\", \"DeviceSetting\", \"SystemUpdateDT\", \"HealthIDRequest\", \"TableStructureRequest\", "
						+"\"AreaUpdate\", \"ProvPass\", \"FacilityName\")"
						+ "VALUES("
						+ "(SELECT \"DIVID\" FROM \"Zilla\" WHERE \"ZILLAID\" = " + zilla + "),"
						+ zilla + "," + upazila + "," + union + "," + providerType + "," + providerCode + ", '" + providerName + "', "
						+ providerMob + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '1', '2', date '2001-09-28' + integer '7', "
						+ "'2', '2', '2', '"+providerPass+"', '"+formValue.get("facilityname")+"')" 
		    			+ "RETURNING \"ProvCode\"";
				
				dbOp.dbExecute(dbObject,sql_providerdb);
				status = true;
			}
			else
			{
				sql_nodeDetails = "insert into node_details (\"ProvCode\", \"ProvType\", client, id, server, base_url, sync_url)"
						+ "VALUES("+providerCode+", "+providerType+", 2, "
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+"), "
						+ "(SELECT format('%s-"+String.format("%02d", zilla)+"-"+String.format("%02d", upazila)+"', lower(\"ZILLANAMEENG\")) from \"Zilla\" where \"ZILLAID\" = "+zilla+"), "
						+ "'"+base_url+"', '"+sync_url+"')"
						+ "RETURNING \"ProvCode\"";
				
				
				//System.out.println(sql_nodeDetails);
				ResultSet rs = dbOp.dbExecute(dbObject,sql_nodeDetails).getResultSet();
				
				sql_providerdb = "insert into \"ProviderDB\" (\"Divid\", zillaid, upazilaid, unionid, "
						+"\"ProvType\", \"ProvCode\", \"ProvName\", \"MobileNo\", \"EnDate\", \"ExDate\", "
						+"\"Active\", \"DeviceSetting\", \"SystemUpdateDT\", \"HealthIDRequest\", \"TableStructureRequest\", "
						+"\"AreaUpdate\", \"ProvPass\", \"FacilityName\")"
						+ "VALUES("
						+ "(SELECT \"DIVID\" FROM \"Zilla\" WHERE \"ZILLAID\" = " + zilla + "),"
						+ zilla + "," + upazila + "," + union + "," + providerType + "," + providerCode + ", '" + providerName + "', "
						+ providerMob + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '1', '2', '2000-01-01', "
						+ "'2', '2', '2', '"+providerPass+"', " 
						+ " (SELECT format('%s "+formValue.get("facilityname")+"', \"UNIONNAME\") from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+")) "
		    			+ "RETURNING \"ProvCode\"";
				
				//System.out.println(sql_providerdb);
				
				sql_sym_node = "insert into sym_node (node_id,node_group_id,external_id,sync_enabled,sync_url,schema_version,"
						+ "symmetric_version,database_type,database_version,heartbeat_time,timezone_offset,batch_to_send_count,"
						+ "batch_in_error_count,created_at_node_id) "
						+ "values ("
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+"), "
						+ "'FWV',"
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+"), "
						+ "1,null,null,null,null,null,CURRENT_TIMESTAMP,null,0,0,"
						+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+")) ";
				
				if(rs.next()){
					status = true;
					dbOp.dbExecute(dbObject,sql_providerdb);
					//dbOp.dbExecute(dbObject,sql_sym_node);
					
//					DB_name = "RHIS_"+zilla+"_"+String.format("%02d", upazila);
//					Connection c = DriverManager.getConnection("jdbc:postgresql://10.10.10.17:5432/"+DB_name, "postgres", "postgres");
//					//c.setAutoCommit(false);
//					Statement statement = c.createStatement();
					
					sql_nodeDetails = "insert into node_details (\"ProvCode\", \"ProvType\", client, id, server, base_url, sync_url)"
							+ "VALUES("+providerCode+", "+providerType+", 2, "
							+ "(SELECT format('%s_"+providerCode+"_TAB', replace(\"UNIONNAMEENG\", ' ', '')) from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+"), "
							+ "(SELECT format('%s-"+String.format("%02d", zilla)+"-"+String.format("%02d", upazila)+"', lower(\"ZILLANAMEENG\")) from \"Zilla\" where \"ZILLAID\" = "+zilla+"), "
							+ "'"+base_url+"', '"+sync_url+"')";
					
					sql_providerdb = "insert into \"ProviderDB\" (\"Divid\", zillaid, upazilaid, unionid, "
							+"\"ProvType\", \"ProvCode\", \"ProvName\", \"MobileNo\", \"EnDate\", \"ExDate\", "
							+"\"Active\", \"DeviceSetting\", \"SystemUpdateDT\", \"HealthIDRequest\", \"TableStructureRequest\", "
							+"\"AreaUpdate\", \"ProvPass\", \"FacilityName\")"
							+ "VALUES("
							+ "(SELECT \"DIVID\" FROM \"Zilla\" WHERE \"ZILLAID\" = " + zilla + "),"
							+ zilla + "," + upazila + "," + union + "," + providerType + "," + providerCode + ", '" + providerName + "', "
							+ providerMob + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '1', '2', date '2001-09-28' + integer '7', "
							+ "'2', '2', '2', '"+providerPass+"', " 
							+ " (SELECT format('%s "+formValue.get("facilityname")+"', \"UNIONNAME\") from \"Unions\" where \"ZILLAID\" = "+zilla+" AND \"UPAZILAID\" = "+upazila+" AND \"UNIONID\" = "+union+")) ";
					
					//System.out.println("Line1");
					
					DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
					//System.out.println("Line2");
					dbOp.dbExecute(dbObject_dynamic,sql_nodeDetails);
					//System.out.println("Line3");
					dbOp.dbExecute(dbObject_dynamic,sql_providerdb);
					dbOp.dbExecute(dbObject_dynamic,sql_sym_node);
					
//					statement.execute(sql_nodeDetails);
//					statement.execute(sql_providerdb);
//					statement.execute(sql_sym_node);
					
//					statement.close();
//			        c.close();
					
					dbOp.closeStatement(dbObject_dynamic);
					dbOp.closeConn(dbObject_dynamic);
			        
//					if(zilla.equals(93) && upazila.equals(9))
//						servername = "tangail-93-9";
//					else
					servername = zillaname+"-"+String.format("%02d", zilla)+"-"+String.format("%02d", upazila);
			        nodename = unionname.replaceAll("\\s+","")+"_"+providerCode+"_TAB";
					
			        String command1="bash /var/lib/symmetricds/symmetric-server-3.7.27/bin/symadmin --engine "+servername+" open-registration FWV "+nodename;
				    ArrayList<String> result = new ArrayList<String>();
				    
				    java.util.Properties config = new java.util.Properties(); 
			    	config.put("StrictHostKeyChecking", "no");
			    	JSch jsch = new JSch();
			    	Session session=jsch.getSession(user, host, portNum);
			    	session.setPassword(password);
			    	session.setConfig(config);
			    	session.connect();
			    	System.out.println("Connected");
			    	
			    	Channel channel=session.openChannel("exec");
			        ((ChannelExec)channel).setCommand(command1);
			        channel.setInputStream(null);
			        ((ChannelExec)channel).setErrStream(System.err);
			        
			        InputStream in=channel.getInputStream();
			        channel.connect();
			        
			     // Read the output from the input stream we set above
			        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			        String line;
			        
			        //Read each line from the buffered reader and add it to result list
			        // You can also simple print the result here
			        while ((line = reader.readLine()) != null)
			        {
			        	result.add(line);
			        }
			        System.out.println(Arrays.asList(result)); 
			        //retrieve the exit status of the remote command corresponding to this channel
			        int exitStatus = channel.getExitStatus();
			        
			        channel.disconnect();
			        session.disconnect();
			        System.out.println("DONE");
				}
		        else{
		        	status = false;
		        }
			}
			
			//System.out.println(status);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		
		return status;
	}
	
	public static boolean assignHealthID(JSONObject formValue){

		boolean status = false;
		String sql_assignHealthID;
		
		try{
			
			
			zilla = Integer.parseInt(formValue.getString("zillaid"));
			upazila = Integer.parseInt(formValue.getString("upazilaid"));
			union = Integer.parseInt(formValue.getString("unionid"));
			providerType = Integer.parseInt(formValue.getString("providertype"));
			providerCode = Integer.parseInt(formValue.getString("providercode"));
			
			//DB_name = "RHIS_"+String.format("%02d", zilla)+"_"+String.format("%02d", upazila);
			
			Connection c = DriverManager.getConnection("jdbc:postgresql://10.10.10.17:5432/PRS2", "postgres", "postgres");
			//c.setAutoCommit(false);
			Statement statement = c.createStatement();
			
			
			sql_assignHealthID = "SELECT sp_generatehealthid('"+zilla+"','"+upazila+"','"+union+"','"+providerType+"','"+providerCode+"',5000)";
			System.out.println(sql_assignHealthID);
			statement.executeQuery(sql_assignHealthID);
			
			status = true;
			
			statement.close();
	        c.close();
			
		}
		catch(Exception e){
			System.out.println(e);
			status = false;
		}
		
		return status;
	}
	
	public static boolean assignAppVersion(JSONObject formValue){

		boolean status = false;
		
		zilla = Integer.parseInt(formValue.getString("zillaid"));
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);
		
		try{
			changetype = formValue.getString("changetype");
			upazila = Integer.parseInt(formValue.getString("upazilaid"));
			if(!changetype.equals("2"))
				union = Integer.parseInt(formValue.getString("unionid"));
			if(changetype.equals("4"))
				providerCode = Integer.parseInt(formValue.getString("providercode"));
			version = formValue.getString("version");
			
			if(changetype.equals("2"))
				sql_assignAppVersion = "update node_details set version='"+version+"' where \"ProvCode\" in (select \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid="+upazila+")";
			else if(changetype.equals("3"))
				sql_assignAppVersion = "update node_details set version='"+version+"' where \"ProvCode\" in (select \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid="+upazila+" and unionid="+union+")";
			else if(changetype.equals("4"))
				sql_assignAppVersion = "update node_details set version='"+version+"' where \"ProvCode\"="+providerCode;
			
			//System.out.println(sql_assignAppVersion);
			
			dbOp.dbExecuteUpdate(dbObject,sql_assignAppVersion);
			
			DBInfoHandler dbObject_dynamic = new FacilityDB().dynamicDBInfo(Integer.toString(zilla), Integer.toString(upazila));
			dbOp.dbExecuteUpdate(dbObject_dynamic,sql_assignAppVersion);
			
			dbOp.closeStatement(dbObject_dynamic);
			dbOp.closeConn(dbObject_dynamic);
			
			status = true;
			
		}
		catch(Exception e){
			System.out.println(e);
			status = false;
		}
		finally{
			dbOp.closeStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		
		return status;
	}
}
