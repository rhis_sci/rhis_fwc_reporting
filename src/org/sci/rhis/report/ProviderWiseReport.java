package org.sci.rhis.report;

//import java.time.YearMonth;

import org.json.JSONObject;
import org.sci.rhis.util.CalendarDate;

public class ProviderWiseReport {

	public static String methodType;
	public static String zilla;
	public static String upazila;
	public static String resultString;
	public static String graphString;
	public static String sql;
	public static String date_type;
	public static String monthSelect;
	public static String start_date;
	public static String end_date;
	public static String db_name;
	
	public static String getResultSet(JSONObject reportCred){
		resultString = "";
		try{
			methodType = reportCred.getString("methodType");
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			date_type = reportCred.getString("report1_dateType");
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
			//System.out.println(date_type);
			
			/*sql = "select (anc.\"providerId\") as \"ProviderId\", anc.\"ProvName\", \"ANC\", \"PNC\", \"PNC-N\", \"Delivery\" ,"
					+ "sum(\"ANC\" + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\" " 
					+ " from " 

					+ "(select \"providerId\", \"ProvName\", count (*) as \"ANC\" "
					+ "FROM \"ancService\", \"ProviderDB\" "
					+ "where \"providerId\" in "
					+ "(select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "AND \"ancService\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "AND \"ancService\".\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "group by \"providerId\", \"ProvName\" "
					+ ") AS anc "
					+ "Left join "
					+ "(select \"providerId\", \"ProvName\", count (*) as \"PNC\" "
					+ "FROM \"pncServiceMother\", \"ProviderDB\" "
					+ "where \"providerId\" in "
					+ " (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "AND \"pncServiceMother\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "AND \"pncServiceMother\".\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "group by \"providerId\", \"ProvName\" "
					+ ") AS pnc "
					+ "on anc.\"providerId\" = pnc.\"providerId\" "
					+ "Left join "
					+ "(select \"providerId\", \"ProvName\", count (*) as \"PNC-N\" "
					+ "FROM \"pncServiceChild\", \"ProviderDB\" "
					+ "where \"providerId\" in "
					+ "  (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "  AND \"pncServiceChild\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "  AND \"pncServiceChild\".\"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "  group by \"providerId\", \"ProvName\" "
					+ "  ) AS pncc "
					+ "  on anc.\"providerId\" = pncc.\"providerId\" "
					+ "  left join "
					+ "  ( select \"providerId\", \"ProvName\", count (*) as \"Delivery\" "
					+ "  FROM \"delivery\", \"ProviderDB\" "
					+ "  where \"providerId\" in "
					+ "  (select \"ProvCode\" from \"ProviderDB\" where zillaid = "+zilla+" AND upazilaid = "+upazila+") "
					+ "  AND \"delivery\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "  AND \"delivery\".\"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
					+ "  group by \"providerId\", \"ProvName\" "
					+ ") as delivery "
					+ "on anc.\"providerId\" = delivery.\"providerId\" "
					+ "group by anc.\"providerId\", anc.\"ProvName\", \"ANC\", \"PNC\", \"PNC-N\",\"Delivery\" "
					+ "order by anc.\"providerId\", anc.\"ProvName\" ";*/
			
			if(methodType.equals("1"))
			{
				sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\",  " + 
						"(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " + 
						"(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " + 
						"(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " + 
						"(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " + 
						"(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) AS \"Delivery\",  " + 
						"(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " + 
						"(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " + 
						"(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " + 
						"(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " + 
						"(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " + 
						"(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " + 
						"(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " + 
						"(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\",  " + 
						"((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " + 
						"from \"ProviderDB\"   " + 
						"left join  " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"ANC\",  " + 
						"count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\",  " + 
						"count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\",  " + 
						"count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\",  " + 
						"count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\"  " + 
						"from \"ancService\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"ancService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ancService\".\"providerId\" " + 
						"where (case when count_provider>1  then  " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and anc.zillaid=\"ProviderDB\".zillaid AND anc.upazilaid=\"ProviderDB\".upazilaid AND anc.unionid=\"ProviderDB\".unionid    " + 
						"left join   " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC\",  " + 
						"count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\",  " + 
						"count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\",  " + 
						"count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\",  " + 
						"count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\"  " + 
						"from \"pncServiceMother\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceMother\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceMother\".\"providerId\" " + 
						"where (case when count_provider>1 then  " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pnc.zillaid=\"ProviderDB\".zillaid AND pnc.upazilaid=\"ProviderDB\".upazilaid AND pnc.unionid=\"ProviderDB\".unionid    " + 
						"left join   " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC-N\",  " + 
						"count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\",  " + 
						"count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\",  " + 
						"count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\",  " + 
						"count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\"  " + 
						"from \"pncServiceChild\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceChild\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceChild\".\"providerId\" " + 
						"where (case when count_provider>1 then  " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pncc.zillaid=\"ProviderDB\".zillaid AND pncc.upazilaid=\"ProviderDB\".upazilaid AND pncc.unionid=\"ProviderDB\".unionid    " + 
						"left join   " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"Delivery\"  " + 
						"from \"delivery\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"delivery\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"delivery\".\"providerId\" " + 
						"where (case when count_provider>1 then  " + 
						"(case when \"ExDate\" is NOT NULL then \"outcomeDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"outcomeDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND \"delivery\".\"deliveryDoneByThisProvider\"=1 group by \"providerId\",zillaid,upazilaid,unionid) as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" and delivery.zillaid=\"ProviderDB\".zillaid AND delivery.upazilaid=\"ProviderDB\".upazilaid AND delivery.unionid=\"ProviderDB\".unionid    " + 
						"join  \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\"   " + 
						"join  (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila +") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   " + 
						"where \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila +" AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6)  " + 
						"AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			}
			else if(methodType.equals("2"))
			{
				sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\", " + 
						"(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", " + 
						"(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", " + 
						"(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  " + 
						"(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  " + 
						"(CASE WHEN implant.\"IMPLANT\" IS NULL THEN 0 ELSE implant.\"IMPLANT\" END) AS \"IMPLANT\",  " + 
						"(CASE WHEN implantfollowup.\"IMPLANT Followup\" IS NULL THEN 0 ELSE implantfollowup.\"IMPLANT Followup\" END) AS \"IMPLANT Followup\",  " +
						"(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\",  " + 
						"((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"IMPLANT\" is NULL then 0 else \"IMPLANT\" END) + (case when \"IMPLANT Followup\" is NULL then 0 else \"IMPLANT Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\" " + 
						"from \"ProviderDB\"   " + 
						"left join  " + 
						"(select \"providerId\",zillaid,upazilaid,unionid," + 
						"COUNT(CASE WHEN (\"methodType\"=1 OR \"methodType\"=10) THEN 1 ELSE NULL END) as \"Pill\", " + 
						"COUNT(CASE WHEN \"methodType\"=2 THEN 1 ELSE NULL END) as \"Condom\" " + 
						"from \"pillCondomService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pillCondomService\".\"providerId\"" + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pillCondomService\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as pillcondom on pillcondom.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pillcondom.zillaid=\"ProviderDB\".zillaid AND pillcondom.upazilaid=\"ProviderDB\".upazilaid AND pillcondom.unionid=\"ProviderDB\".unionid " + 
						"left join   " + 
						"(select \"providerId\",count (*) as \"IUD\",zillaid,upazilaid,unionid " + 
						"from \"iudService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudService\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"iudImplantDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"iudImplantDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"iudImplantDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as iud on iud.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iud.zillaid=\"ProviderDB\".zillaid AND iud.upazilaid=\"ProviderDB\".upazilaid AND iud.unionid=\"ProviderDB\".unionid " + 
						"left join " + 
						"(select \"providerId\",count (*) as \"IUD Followup\",zillaid,upazilaid,unionid " + 
						"from \"iudFollowupService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudFollowupService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudFollowupService\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"followupDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"followupDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as iudfollowup on iudfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iudfollowup.zillaid=\"ProviderDB\".zillaid AND iudfollowup.upazilaid=\"ProviderDB\".upazilaid AND iudfollowup.unionid=\"ProviderDB\".unionid " + 
						"left join   " + 
						"(select \"providerId\",count (*) as \"IMPLANT\",zillaid,upazilaid,unionid " + 
						"from \"implantService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantService\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"implantImplantDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"implantImplantDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"implantImplantDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implant on implant.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implant.zillaid=\"ProviderDB\".zillaid AND implant.upazilaid=\"ProviderDB\".upazilaid AND implant.unionid=\"ProviderDB\".unionid " + 
						"left join " + 
						"(select \"providerId\",count (*) as \"IMPLANT Followup\",zillaid,upazilaid,unionid " + 
						"from \"implantFollowupService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantFollowupService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"followupDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"followupDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implantfollowup on implantfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implantfollowup.zillaid=\"ProviderDB\".zillaid AND implantfollowup.upazilaid=\"ProviderDB\".upazilaid AND implantfollowup.unionid=\"ProviderDB\".unionid " +
						"left join   " + 
						"(select \"providerId\",count (*) as \"Injectable\",zillaid,upazilaid,unionid " + 
						"from \"womanInjectable\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"womanInjectable\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"womanInjectable\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"doseDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"doseDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"doseDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as injectable on injectable.\"providerId\"=\"ProviderDB\".\"ProvCode\" and injectable.zillaid=\"ProviderDB\".zillaid AND injectable.upazilaid=\"ProviderDB\".upazilaid AND injectable.unionid=\"ProviderDB\".unionid " + 
						"left join   " + 
						"(select \"Dist\", \"Upz\", \"UN\", count(*) as \"ELCO\" " + 
						"from \"Member\" " + 
						"where date_part('year', age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) < '49' AND date_part('year',  age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) > '15' AND \"Sex\" = '2' group by \"Dist\", \"Upz\", \"UN\") as member on member.\"Dist\"=\"ProviderDB\".zillaid AND member.\"Upz\"=\"ProviderDB\".upazilaid AND member.\"UN\"=\"ProviderDB\".unionid   " + 
						"join \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" " + 
						"join   " + 
						"(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila +") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   " + 
						"where \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila +" AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			}
			else
			{
				sql = "select \"ProviderDB\".\"ProvCode\" as \"ProviderID\", \"ProviderDB\".\"ProvName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", \"FacilityName\", unions.\"UnionName\", "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END) AS \"Male\", "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END) AS \"Female\", "
						+ "(COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END)+COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END)) AS \"TOTAL\" "
						+ "from \"ProviderDB\"   "
						+ "left join  "
						+ "\"gpService\" on \"gpService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" and \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
						+ "left join "
						+ "\"clientMap\" on \"clientMap\".\"generatedId\"=\"gpService\".\"healthId\" "
						+ "join "
						+ "\"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" "
						+ "join "
						+ "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   "
						+ "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) "
						+ "and \"ProvType\" in (4,101,17,5,6) "
						+ "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ProviderDB\".\"ProvCode\" "  
						+ "where (case when \"visitDate\" is not null then (case when count_provider>1 then "
						+ "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end) "
						+ "else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) else \"visitDate\" is null end) "
						+ "AND \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) "
						+ "GROUP BY \"ProviderDB\".\"ProvCode\", \"ProviderDB\".\"ProvName\", \"FacilityName\", \"ProviderType\".\"TypeName\", \"ProviderDB\".\"MobileNo\", unions.\"UnionName\"";
			}	


			
			System.out.println(sql);
			
			resultString = GetResultSet.getFinalResult(sql,zilla, upazila);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return resultString;
	}
	
	public static JSONObject getGraphSet(JSONObject reportCred){
		
		JSONObject graphInfo = null;
		
		try{
			methodType = reportCred.getString("methodType");
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			date_type = reportCred.getString("report1_dateType");
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
			if(methodType.equals("1"))
			{
				sql = "select (CASE WHEN ct_prov.count_provider>1 THEN (\"ProviderDB\".\"ProvName\" || '(' || unions.\"UnionName\" || ')') ELSE \"ProviderDB\".\"ProvName\" END) AS \"ProvName\",  " + 
						"(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " + 
						"(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " + 
						"(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " + 
						"(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " + 
						"(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) AS \"Delivery\",  " + 
						"(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " + 
						"(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " + 
						"(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " + 
						"(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " + 
						"(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " + 
						"(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " + 
						"(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " + 
						"(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\"  " + 
						//"((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " + 
						"from \"ProviderDB\"   " + 
						"left join  " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"ANC\",  " + 
						"count(case when \"serviceId\"=1 then 1 else null end) as \"ANC1\",  " + 
						"count(case when \"serviceId\"=2 then 1 else null end) as \"ANC2\",  " + 
						"count(case when \"serviceId\"=3 then 1 else null end) as \"ANC3\",  " + 
						"count(case when \"serviceId\"=4 then 1 else null end) as \"ANC4\"  " + 
						"from \"ancService\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"ancService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ancService\".\"providerId\" " +  
						"where (case when count_provider>1  then  " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as anc on anc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and anc.zillaid=\"ProviderDB\".zillaid AND anc.upazilaid=\"ProviderDB\".upazilaid AND anc.unionid=\"ProviderDB\".unionid    " + 
						"left join   " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC\",  " + 
						"count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1\",  " + 
						"count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2\",  " + 
						"count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3\",  " + 
						"count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4\"  " + 
						"from \"pncServiceMother\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceMother\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceMother\".\"providerId\" " +  
						"where (case when count_provider>1 then  " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as pnc on pnc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pnc.zillaid=\"ProviderDB\".zillaid AND pnc.upazilaid=\"ProviderDB\".upazilaid AND pnc.unionid=\"ProviderDB\".unionid    " + 
						"left join   " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"PNC-N\",  " + 
						"count(case when \"serviceId\"=1 then 1 else null end) as \"PNC1-N\",  " + 
						"count(case when \"serviceId\"=2 then 1 else null end) as \"PNC2-N\",  " + 
						"count(case when \"serviceId\"=3 then 1 else null end) as \"PNC3-N\",  " + 
						"count(case when \"serviceId\"=4 then 1 else null end) as \"PNC4-N\"  " + 
						"from \"pncServiceChild\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pncServiceChild\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pncServiceChild\".\"providerId\" " +  
						"where (case when count_provider>1 then  " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') group by \"providerId\",zillaid,upazilaid,unionid) as pncc on pncc.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pncc.zillaid=\"ProviderDB\".zillaid AND pncc.upazilaid=\"ProviderDB\".upazilaid AND pncc.unionid=\"ProviderDB\".unionid    " + 
						"left join   " + 
						"(select \"providerId\",zillaid,upazilaid,unionid,count (*) as \"Delivery\"  " + 
						"from \"delivery\"  " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"delivery\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"delivery\".\"providerId\" " +  
						"where (case when count_provider>1 then  " + 
						"(case when \"ExDate\" is NOT NULL then \"outcomeDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"outcomeDate\" between \"EnDate\"::date and '"+ end_date +"' end)  " + 
						"else \"outcomeDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) " + 
						"AND \"delivery\".\"deliveryDoneByThisProvider\"=1 group by \"providerId\",zillaid,upazilaid,unionid) as delivery on delivery.\"providerId\"=\"ProviderDB\".\"ProvCode\" and delivery.zillaid=\"ProviderDB\".zillaid AND delivery.upazilaid=\"ProviderDB\".upazilaid AND delivery.unionid=\"ProviderDB\".unionid    " + 
						"join  \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\"   " +
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ProviderDB\".\"ProvCode\" " + 
						"join  (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila +") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   " + 
						"where \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila +" AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6)  " + 
						"AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			}
			else if(methodType.equals("2"))
			{
				sql = "select (CASE WHEN ct_prov.count_provider>1 THEN (\"ProviderDB\".\"ProvName\" || '(' || unions.\"UnionName\" || ')') ELSE \"ProviderDB\".\"ProvName\" END) AS \"ProvName\",  " + 
						"(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", " + 
						"(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", " + 
						"(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  " + 
						"(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  " + 
						"(CASE WHEN implant.\"IMPLANT\" IS NULL THEN 0 ELSE implant.\"IMPLANT\" END) AS \"IMPLANT\",  " + 
						"(CASE WHEN implantfollowup.\"IMPLANT Followup\" IS NULL THEN 0 ELSE implantfollowup.\"IMPLANT Followup\" END) AS \"IMPLANT Followup\",  " +
						"(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\"  " + 
						//"((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\"   " + 
						"from \"ProviderDB\"   " + 
						"left join  " + 
						"(select \"providerId\",zillaid,upazilaid,unionid," + 
						"COUNT(CASE WHEN (\"methodType\"=1 OR \"methodType\"=10) THEN 1 ELSE NULL END) as \"Pill\", " + 
						"COUNT(CASE WHEN \"methodType\"=2 THEN 1 ELSE NULL END) as \"Condom\" " + 
						"from \"pillCondomService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"pillCondomService\".\"providerId\"" + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"pillCondomService\".\"providerId\" " + 
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as pillcondom on pillcondom.\"providerId\"=\"ProviderDB\".\"ProvCode\" and pillcondom.zillaid=\"ProviderDB\".zillaid AND pillcondom.upazilaid=\"ProviderDB\".upazilaid AND pillcondom.unionid=\"ProviderDB\".unionid " + 
						"left join   " + 
						"(select \"providerId\",count (*) as \"IUD\",zillaid,upazilaid,unionid " + 
						"from \"iudService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudService\".\"providerId\"" + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudService\".\"providerId\" " + 
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"iudImplantDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"iudImplantDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"iudImplantDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as iud on iud.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iud.zillaid=\"ProviderDB\".zillaid AND iud.upazilaid=\"ProviderDB\".upazilaid AND iud.unionid=\"ProviderDB\".unionid " + 
						"left join   " + 
						"(select \"providerId\",count (*) as \"IUD Followup\",zillaid,upazilaid,unionid " + 
						"from \"iudFollowupService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"iudFollowupService\".\"providerId\"" + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"iudFollowupService\".\"providerId\" " + 
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"followupDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"followupDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as iudfollowup on iudfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and iudfollowup.zillaid=\"ProviderDB\".zillaid AND iudfollowup.upazilaid=\"ProviderDB\".upazilaid AND iudfollowup.unionid=\"ProviderDB\".unionid " + 
						"left join   " + 
						"(select \"providerId\",count (*) as \"IMPLANT\",zillaid,upazilaid,unionid " + 
						"from \"implantService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantService\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"implantImplantDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"implantImplantDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"implantImplantDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implant on implant.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implant.zillaid=\"ProviderDB\".zillaid AND implant.upazilaid=\"ProviderDB\".upazilaid AND implant.unionid=\"ProviderDB\".unionid " + 
						"left join " + 
						"(select \"providerId\",count (*) as \"IMPLANT Followup\",zillaid,upazilaid,unionid " + 
						"from \"implantFollowupService\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"implantFollowupService\".\"providerId\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"implantFollowupService\".\"providerId\" " +  
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"followupDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"followupDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"followupDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) group by \"providerId\",zillaid,upazilaid,unionid) as implantfollowup on implantfollowup.\"providerId\"=\"ProviderDB\".\"ProvCode\" and implantfollowup.zillaid=\"ProviderDB\".zillaid AND implantfollowup.upazilaid=\"ProviderDB\".upazilaid AND implantfollowup.unionid=\"ProviderDB\".unionid " +
						"left join   " + 
						"(select \"providerId\",count (*) as \"Injectable\",zillaid,upazilaid,unionid " + 
						"from \"womanInjectable\" " + 
						"join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"womanInjectable\".\"providerId\"" + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"womanInjectable\".\"providerId\" " + 
						"where (case when count_provider>1 then " + 
						"(case when \"ExDate\" is NOT NULL then \"doseDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"doseDate\" between \"EnDate\"::date and '"+ end_date +"' end) " + 
						"else \"doseDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) group by \"providerId\",zillaid,upazilaid,unionid) as injectable on injectable.\"providerId\"=\"ProviderDB\".\"ProvCode\" and injectable.zillaid=\"ProviderDB\".zillaid AND injectable.upazilaid=\"ProviderDB\".upazilaid AND injectable.unionid=\"ProviderDB\".unionid " + 
						"left join   " + 
						"(select \"Dist\", \"Upz\", \"UN\", count(*) as \"ELCO\" " + 
						"from \"Member\" " + 
						"where date_part('year', age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) < '49' AND date_part('year',  age('"+ end_date +"',to_date(\"DOB\", 'YYYY-MM-DD'))) > '15' AND \"Sex\" = '2' group by \"Dist\", \"Upz\", \"UN\") as member on member.\"Dist\"=\"ProviderDB\".zillaid AND member.\"Upz\"=\"ProviderDB\".upazilaid AND member.\"UN\"=\"ProviderDB\".unionid   " + 
						"join \"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" " + 
						"join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) " +
						"and \"ProvType\" in (4,101,17,5,6) " +
						"group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ProviderDB\".\"ProvCode\" " +
						"join (select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila +") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   " + 
						"where \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila +" AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL)";
			}
			else
			{
				sql = "select (CASE WHEN ct_prov.count_provider>1 THEN (\"ProviderDB\".\"ProvName\" || '(' || unions.\"UnionName\" || ')') ELSE \"ProviderDB\".\"ProvName\" END) AS \"ProvName\",  "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END) AS \"Male\", "
						+ "COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END) AS \"Female\", "
						+ "(COUNT(CASE WHEN (\"clientMap\".gender=1) THEN 1 ELSE NULL END)+COUNT(CASE WHEN (\"clientMap\".gender=2) THEN 1 ELSE NULL END)) AS \"TOTAL\" "
						+ "from \"ProviderDB\"   "
						+ "left join  "
						+ "\"gpService\" on \"gpService\".\"providerId\"=\"ProviderDB\".\"ProvCode\" and \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' "
						+ "left join "
						+ "\"clientMap\" on \"clientMap\".\"generatedId\"=\"gpService\".\"healthId\" "
						+ "join "
						+ "\"ProviderType\" on \"ProviderDB\".\"ProvType\"=\"ProviderType\".\"ProvType\" "
						+ "join "
						+ "(select \"ZILLAID\",\"UPAZILAID\",\"UNIONID\",\"UNIONNAMEENG\" as \"UnionName\" from \"Unions\" where \"ZILLAID\"="+zilla+" and \"UPAZILAID\"="+upazila+") as unions on unions.\"ZILLAID\"=\"ProviderDB\".zillaid AND unions.\"UPAZILAID\"=\"ProviderDB\".upazilaid AND unions.\"UNIONID\"=\"ProviderDB\".unionid   "
						+ "join (select count(*) as count_provider, \"ProvCode\" from \"ProviderDB\" where zillaid="+zilla+" and upazilaid = "+upazila +" and (\"ExDate\"> '"+ start_date +"' or \"ExDate\" is NULL) "
						+ "and \"ProvType\" in (4,101,17,5,6) "
						+ "group by \"ProvCode\") ct_prov on ct_prov.\"ProvCode\"=\"ProviderDB\".\"ProvCode\" "
						+ "where (case when \"visitDate\" is not null then (case when count_provider>1 then "
						+ "(case when \"ExDate\" is NOT NULL then \"visitDate\" between '"+ start_date +"' AND \"ExDate\"::date else \"visitDate\" between \"EnDate\"::date and '"+ end_date +"' end) "
						+ "else \"visitDate\" BETWEEN '"+ start_date +"' AND '"+ end_date +"' end) else \"visitDate\" is null end) "
						+ "AND \"ProviderDB\".zillaid = "+zilla+" AND \"ProviderDB\".upazilaid = "+upazila
						+ " AND (\"ProviderDB\".\"ProvType\" = 4 OR \"ProviderDB\".\"ProvType\" = 101 OR \"ProviderDB\".\"ProvType\" = 17 OR \"ProviderDB\".\"ProvType\" = 5 OR \"ProviderDB\".\"ProvType\" = 6) AND (\"ProviderDB\".\"ExDate\">='"+ start_date +"' OR \"ProviderDB\".\"ExDate\" IS NULL) "
						+ "GROUP BY \"ProviderDB\".\"ProvCode\", \"ProviderDB\".\"ProvName\", \"ProviderDB\".zillaid, \"ProviderDB\".upazilaid, \"ProviderDB\".unionid, ct_prov.count_provider, unions.\"UnionName\"";
			}	
				


			
			System.out.println(sql);
			
			//graphInfo = GetResultSet.getFinalGraphResult(sql, zilla, upazila);
			
			//System.out.println(graphString);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return graphInfo;
	}

}
