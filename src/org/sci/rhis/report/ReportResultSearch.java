package org.sci.rhis.report;

import org.json.JSONObject;

public class ReportResultSearch {

    public static String getSearchResult(JSONObject reportCred) {
        String resultString = "";
        String type;
        String formType;
        System.out.println("Json Object=" + reportCred);
        try {
            if (reportCred.has("type")) {
                type = reportCred.getString("type");
                switch (type) {
                    case "1":
                        resultString = ServiceStatistics.getResultSet(reportCred);

                        break;

                    case "2":
                        resultString = AggregateMNC.getResultSet(reportCred);
                        break;

                    case "3":
                        resultString = ServiceStatistics.getResultSet(reportCred);
                        break;

                    case "4":
                        resultString = MIS3Report.getResultSet(reportCred);
                        break;

                    case "5":
                        resultString = UpazilaWiseReport.getResultSet(reportCred);
                        break;

                    case "6":
                        resultString = ANC7Days.getResultSet(reportCred);
                        break;

                    case "7":
                        resultString = DeliveryIssue3Months.getResultSet(reportCred);
                        break;

                    case "8":
                        resultString = MIS4Report.getResultSet(reportCred);
                        break;

                    case "9":
                        resultString = ProviderSupervision.getResultSet(reportCred);
                        break;

                    case "10":
                        resultString = ServiceStatistics.getResultSetRHS(reportCred);
                        break;

                    case "11":
                        resultString = ServiceStatistics.getResultSet(reportCred);
                        break;

                    case "12":
                        resultString = LoginInfo.getResultSetMonitoringTool(reportCred);
                        break;

                    case "13":
                        resultString = LoginInfo.getResultSetProvider(reportCred);
                        break;

                    case "14":
                        resultString = SynchronizationIssues.getResultSet(reportCred);
                        break;

                    case "18":
                        resultString = MNCReport.getResultSet(reportCred);
                        break;

                    case "16":
                        resultString = PillCondomReport.getResultSet(reportCred);
                        break;

                    case "17":
                        resultString = GPReport.getResultSet(reportCred);
                        break;

                    case "19":
                        resultString = InjectableRegister.getResultSet(reportCred);
                        break;

                    case "20":
                        resultString = IUDRegister.getResultSet(reportCred);
                        break;

                    case "21":
                        resultString = IMPLANTRegister.getResultSet(reportCred);
                        break;

                    case "22":
                        resultString = DiseaseMonitoring.getResultSet(reportCred);
                        break;

                    case "23":
                        resultString = RegistrationIssue.CheckProviderSymRegistration(reportCred);
                        break;

                    case "25":
                        resultString = PregnantWomenList.getList(reportCred);
                        break;

                    case "26":
                        resultString = MIS3SubmitInfo.MIS3SubmisssionInfo(reportCred);
                        break;

                    case "27":
                        resultString = SatelitePlanningSubmitInfo.satelitePlanningSubmisssionInfo(reportCred);
                        break;

                    case "28":
                        resultString = GetQEDInfo.getResultSet(reportCred);
                        break;

                    case "29":
                        resultString = FeverColdCoughReport.getResultSet(reportCred);
                        break;

                    case "30":
                        resultString = FeverColdCoughReport.resultTrend(reportCred);
                        break;
                    case "31":
                        resultString = ClientInformation.getList(reportCred);
                        break;

                }
            } else if (reportCred.has("formType")) {
                formType = reportCred.getString("formType");
                switch (formType) {
                    case "6":
                        resultString = ProviderInfo.getProviderDetails(reportCred);
                        break;

                    case "26":
                        resultString = ProviderInfo.getUserDetails(reportCred);
                        break;

                    case "27":
                        resultString = GetFacilityInfo.getFacilityDetails(reportCred);
                        break;

                    case "28":
                        resultString = GetFacilityInfo.getFacilitywiseProviderList(reportCred);
                        break;

                    case "addcsba":
                        resultString = ProviderInfo.getFWAList(reportCred);
//                        ResultSet rs = GetResult.getResultSet(requestInfo,"emis_hr_info");

                        break;

                }

            }


            //resultString = GetResultSet.getFinalResult(sql);
        } catch (Exception e) {
            System.out.println(e);
        }
        //return searchList;
        return resultString;
    }

    public static JSONObject getGraphResult(JSONObject reportCred) {
        JSONObject graphInfo = null;
        String type;
        try {
            type = reportCred.getString("type");

            switch (type) {
                case "1":
                    graphInfo = ProviderWiseReport.getGraphSet(reportCred);
                    break;

                case "3":
                    graphInfo = ServiceStatistics.getGraphSet(reportCred);
                    break;
            }


            //resultString = GetResultSet.getFinalResult(sql);
        } catch (Exception e) {
            System.out.println(e);
        }
        //return searchList;
        return graphInfo;
    }

}
