package org.sci.rhis.report;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.ConfInfoRetrieve;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBSchemaInfo;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class MhealthReport {

    public static String sql;
    private static Integer TotalMSGCount =0;

    private static String start_date = "";
    private static String end_date="";
    private static String zillaid = null;
    private static String upazilaid = null;
    private static String unionid = null;

    public static String getResultSet(int result,JSONObject info){

        String resultString = "";
        String start_date = "";
        String end_date="";
        String zillaid=null;
        String upazilaid=null;
        String unionid=null;

        //process form input
        processing_from_data(info);

        try{

            if(result ==1)
            {
                if(!start_date.isEmpty() && !end_date.isEmpty())
                {

                    if(zillaid == null && upazilaid==null) {
                    sql = "select count(distinct(healthid)) from sms_detail join sms_status using(requestid) where status='DELIVERED'";
                    sql +=" and message_delivery_datetime::date between '"+start_date+" 00:00:00'  and '"+end_date+" 23:59:59'";
                    }else{
                        sql = "select count(distinct(healthid)) from sms_detail join sms_status using(requestid)" ;
                        sql +=" inner join zilla using (zillaid) inner join upazila using (zillaid,upazilaid)";
                        sql +=" where status='DELIVERED'";
                        sql +=" and zillaid = "+zillaid;
                        if(!(upazilaid.equals(""))) {
                            sql +=" and upazilaid = " +upazilaid;
                        }

                        sql +=" and message_delivery_datetime::date between '"+start_date+" 00:00:00'  and '"+end_date+" 23:59:59'";
                    }
                }
                else
                    sql = "select count(distinct(healthid)) from sms_detail join sms_status using(requestid) where status='DELIVERED'";

            }
            else {
                if(!start_date.isEmpty() && !end_date.isEmpty())
                {
                    if(zillaid == null && upazilaid==null) {
                        sql = " select eligible_service, count(*) from sms_detail join sms_status using(requestid) where status='DELIVERED'";
                        sql += " and message_delivery_datetime::date between '" + start_date + "' and '" + end_date + "'";
                        sql += "group by eligible_service order by eligible_service";
                    }else{
                        sql = " select eligible_service, count(*) from sms_detail join sms_status using(requestid)";
                        sql +=" inner join zilla using (zillaid) inner join upazila using (zillaid,upazilaid)";
                        sql +=" where status='DELIVERED'";
                        sql +=" and zillaid = "+zillaid;
                        if(!(upazilaid.equals(""))) {
                            sql += " and upazilaid = " + upazilaid;
                        }
                        sql += " and message_delivery_datetime::date between '" + start_date + "' and '" + end_date + "'";
                        sql += "group by eligible_service order by eligible_service";

                    }
                }
                else
                    sql = "select eligible_service, count(*) from sms_detail join sms_status using(requestid) where status='DELIVERED' group by eligible_service order by eligible_service";


            }

            String resultSet = GetResultSet.getmHealthResult(sql);


//            if (result ==1)
//                resultString = TotalResult(resultSet);
//
//            if(result ==2){
//                resultString = DetailResult(resultSet);
//            }
        }
        catch(Exception e){
            System.out.println(e);
        }

        return resultString;
    }

    public static String TotalSMSSent(){
        return String.valueOf(TotalMSGCount);
    }


    public static String DetailResult(String s){
        TotalMSGCount = 0;
        try {
            //JSONObject json = new JSONObject(s);
            JSONArray jsonArray = new JSONArray(s);
            String table = "<table class=\"table table-bordered\" id = \"mhealth-resut-table\">";
            String table_head = "<thead><tr>";
            String table_row = "<tbody><tr>";


            for(int i=0;i<jsonArray.length();i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String key = jsonObject.optString("eligible_service");

                //upper case first later
                //key = key.substring(0, 1).toUpperCase() + key.substring(1);

                if(key.equals("anc1"))
                    key = "ANC 1";
                if(key.equals("anc2"))
                    key = "ANC 2";
                if(key.equals("anc3"))
                    key = "ANC 3";
                if(key.equals("anc4"))
                    key = "ANC 4";
                if(key.equals("delivery1"))
                    key = "Delivery 1";
                if(key.equals("delivery2"))
                    key = "Delivery 2";
                if(key.equals("pnc3"))
                    key = "PNC 3";
                if(key.equals("pnc4"))
                    key = "PNC 4";
                if(key.equals("ppfp"))
                    key = "PPFP";

                String val = jsonObject.optString("count");

                //Count Total message delivered
                TotalMSGCount += Integer.parseInt(val);

                table_head += "<th class=\"text-center\">"+ key +"</th>";
                table_row += "<td align=\"center\">"+ val +"</td>";

            }


                table_head += "</thead>";
                table_row += "</tbody>";


            table += table_head +table_row +"</table>";

            return table;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }


    public static String TotalResult(String s){

        try {
            //JSONObject json = new JSONObject(s);
            JSONArray jsonArray = new JSONArray(s);

            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String val = jsonObject.optString("count");

           // System.out.println(val);

            return val;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String  sms_waiting(JSONObject info) throws SQLException {

//        System.out.println("Form post info");
//        System.out.println(info);

        //process form input
        processing_from_data(info);


        String sql = "select * from sms_detail  sd " +
                " left join sms_status ss on ss.requestid = sd.requestid " +
                " left join zilla z on z.zillaid = sd.zillaid" +
                " left join upazila uz on uz.zillaid = sd.zillaid and uz.upazilaid = sd.upazilaid" +
                " left join unions un on un.zillaid = sd.zillaid and un.upazilaid = sd.upazilaid and un.unionid = sd.unionid"+
                " where entrydatetime::date between '"+start_date+"' and '"+end_date+"'";

        if(zillaid != null && !zillaid.equals("")){
            sql += " and sd.zillaid = " + zillaid;
        }

        if(upazilaid != null && !upazilaid.equals("")){
            sql += " and sd.upazilaid = " + upazilaid;
        }

        //System.out.println(sql);

        String  waiting_sms_list = GetResultSet.get_sms_waiting(sql);


//        System.out.println("Print body");
        System.out.println(waiting_sms_list);
        return waiting_sms_list;


    }

    public static String  sms_type_count(JSONObject info) throws SQLException {

        //process form input
        processing_from_data(info);

        String sql = "select case when sd.sms_type =1 then 'Text' when sd.sms_type = 2 then 'Voice' end as sms_type, count(*) from sms_detail  sd " +
                " left join sms_status ss on ss.requestid = sd.requestid" +
                " where entrydatetime::date between '"+start_date+"' and '"+end_date+"' and ss.status !='REJECT'" ;

        if(zillaid != null && !zillaid.equals("")){
            sql += " and sd.zillaid = " + zillaid;
        }

        if(upazilaid != null && !upazilaid.equals("")){
            sql += " and sd.upazilaid = " + upazilaid;
        }

        sql += " group by sd.sms_type";

        String  sms_group_count = GetResultSet.get_sms_group_count(sql);

        //System.out.println("sms_group_county");
        //System.out.println(sms_group_count);
        return sms_group_count;


    }


    public static String  sms_location_wise_distribution(JSONObject info) throws SQLException {

        //process form input
        processing_from_data(info);

        String sql = "select z.zillanameeng, uz.upazilanameeng, sd.zillaid, sd.upazilaid, count(*)," +
                "      count(case when sd.sms_type = 1 then 1 end )as \"text\"," +
                "      count(case when sd.sms_type = 1 and ss.status = 'DELIVERED' then 1 end )as \"text_delivered\"," +
                "      count(case when sd.sms_type = 1 and ss.status != 'DELIVERED' then 1 end )as \"text_failed\"," +

                "      count(case when sd.sms_type = 2 then 1 end )as \"Voice\"," +
                "      count(case when sd.sms_type = 2 and ss.status = 'Received' then 1 end )as \"voice_delivered\"," +
                "      count(case when sd.sms_type = 2 and ss.status != 'Received' then 1 end )as \"voice_failed\"" +

                "      from sms_detail  sd" +
                "      left join sms_status ss on ss.requestid = sd.requestid" +
                "      left join zilla z on z.zillaid = sd.zillaid" +
                "      left join upazila uz on uz.zillaid = sd.zillaid and uz.upazilaid = sd.upazilaid" +
                " where entrydatetime::date between '"+start_date+"' and '"+end_date+"' and ss.status !='REJECT'" ;

        if(zillaid != null && !zillaid.equals("")){
            sql += " and sd.zillaid = " + zillaid;
        }

        if(upazilaid != null && !upazilaid.equals("")){
            sql += " and sd.upazilaid = " + upazilaid;
        }

        sql +=" group by sd.zillaid, sd.upazilaid,z.zillanameeng, uz.upazilanameeng order by zillanameeng asc,upazilanameeng asc";

        String  result = GetResultSet.sms_location_wise_distribution(sql);

      //  System.out.println("result");
        //System.out.println(result);
        return result;

    }

    public static String sms_service_wise(JSONObject info){

        //process form input
        processing_from_data(info);

        String sql = "select sd.eligible_service, count (*) from sms_detail sd" +
                " left join sms_status ss on ss.requestid = sd.requestid" +
                " left join zilla z on z.zillaid = sd.zillaid" +
                " left join upazila uz on uz.zillaid = sd.zillaid and uz.upazilaid = sd.upazilaid" +
                " where entrydatetime::date between '"+start_date+"' and '"+end_date+"' and ss.status !='REJECT'";

        if(zillaid != null && !zillaid.equals("")){
            sql += " and sd.zillaid = " + zillaid;
        }

        if(upazilaid != null && !upazilaid.equals("")){
            sql += " and sd.upazilaid = " + upazilaid;
        }

        sql += " group by sd.eligible_service";

       // System.out.println(sql);

        String  result = GetResultSet.sms_service_wise_distribution(sql);

        return result;
    }

    public static void processing_from_data(JSONObject info){

        //Initialize current date and assign to start and end date
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        start_date = dateFormat.format(date).toString();
        end_date = dateFormat.format(date).toString();

        if(info.has("start_date") && !info.getString("start_date").isEmpty()){
            start_date = info.getString("start_date");
        }

        if(info.has("end_date") && !info.getString("end_date").isEmpty() ){
            end_date = info.getString("end_date");
        }

        if(info.has("zillaid") && !info.getString("zillaid").isEmpty() && !info.getString("zillaid").equals("null")){
            zillaid = info.getString("zillaid");
        }else{
            zillaid = null;
        }

        if(info.has("upazilaid") && !info.getString("upazilaid").isEmpty() && !info.getString("upazilaid").equals("null")){
            upazilaid = info.getString("upazilaid");
        }else {
            upazilaid = null;
        }

        if(info.has("unionid") && !info.getString("unionid").isEmpty() && !info.getString("unionid").equals("null") && !info.getString("unionid").equals("")){
            unionid = info.getString("unionid");
        }else {
            unionid = null;
        }



    }


}
