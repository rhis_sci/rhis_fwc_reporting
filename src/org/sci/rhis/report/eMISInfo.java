package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Calendar;
import java.util.LinkedHashMap;

public class eMISInfo {
    public static JSONObject geteMISInfo() {

        JSONObject emis_info = new JSONObject();

        try {
            FacilityDB dbFacility = new FacilityDB();
            DBOperation dbOp = new DBOperation();
            DBInfoHandler dbObject = null;

            String zilla[] = {"36","42","51","75","93"};
//            String zilla[] = {"36"};

            //LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>>>>>> info = new LinkedHashMap<>();
            JSONObject all_district = new JSONObject();
            int z=0,up=0,un=0,p=0,f=0;

            for(int i=0; i<zilla.length;i++){
                DBSchemaInfo table_schema = new DBSchemaInfo(zilla[i]);

                String sql = "select p."+table_schema.getColumn("", "providerdb_zillaid")+" as zillaid, p."+table_schema.getColumn("", "providerdb_upazilaid")+" as upazilaid, " +
                        "p."+table_schema.getColumn("", "providerdb_unionid")+" as unionid, p."+table_schema.getColumn("", "providerdb_provcode")+" as providerid, pt."+table_schema.getColumn("", "providertype_typename")+" as provtype, " +
                        "INITCAP(p."+table_schema.getColumn("", "providerdb_provname")+") as provname, p."+table_schema.getColumn("", "providerdb_mobileno")+" as mobileno, " +
                        "INITCAP(z."+table_schema.getColumn("", "zilla_zillanameeng")+") as zillaname, INITCAP(u."+table_schema.getColumn("", "upazila_upazilanameeng")+") as upazilaname, " +
                        "INITCAP(un."+table_schema.getColumn("", "unions_unionnameeng")+") as unionname, fr."+table_schema.getColumn("", "facility_registry_facility_name")+" as facility_name, " +
                        "fr."+table_schema.getColumn("", "facility_registry_facilityid")+" as facilityid,  fr."+table_schema.getColumn("", "facility_registry_facility_type")+" as facility_type " +
                        "from "+table_schema.getTable("table_providerdb")+" p " +
                        "join "+table_schema.getTable("table_zilla")+" z on z."+table_schema.getColumn("", "zilla_zillaid")+"= p."+table_schema.getColumn("", "providerdb_zillaid")+"  " +
                        "join "+table_schema.getTable("table_providertype")+" pt using("+table_schema.getColumn("", "providertype_provtype")+")  " +
                        "join "+table_schema.getTable("table_upazila")+" u on u."+table_schema.getColumn("", "upazila_zillaid")+"= p."+table_schema.getColumn("", "providerdb_zillaid")+" and u."+table_schema.getColumn("", "upazila_upazilaid")+"= p."+table_schema.getColumn("", "providerdb_upazilaid")+" " +
                        "join "+table_schema.getTable("table_unions")+" un on un."+table_schema.getColumn("", "unions_zillaid")+"= p."+table_schema.getColumn("", "providerdb_zillaid")+" and un."+table_schema.getColumn("", "unions_upazilaid")+"= p."+table_schema.getColumn("", "providerdb_upazilaid")+" and un."+table_schema.getColumn("", "unions_unionid")+"= p."+table_schema.getColumn("", "providerdb_unionid")+" " +
                        "join "+table_schema.getTable("table_facility_provider")+" fp on fp."+table_schema.getColumn("", "facility_provider_provider_id")+"=p."+table_schema.getColumn("", "providerdb_provcode")+" " +
                        "join "+table_schema.getTable("table_facility_registry")+" fr on fr."+table_schema.getColumn("", "facility_registry_facilityid")+"=fp."+table_schema.getColumn("", "facility_provider_facility_id")+" " +
                        "where fr."+table_schema.getColumn("", "facility_registry_facility_type_id",new String[]{"4"},"=")+" and p."+table_schema.getColumn("", "providerdb_active",new String[]{"1"},"=")+" and fp."+table_schema.getColumn("", "facility_provider_is_active",new String[]{"1"},"=")+" "+
                        "order by p."+table_schema.getColumn("", "providerdb_zillaid")+", p."+table_schema.getColumn("", "providerdb_upazilaid")+", p."+table_schema.getColumn("", "providerdb_unionid")+", fr."+table_schema.getColumn("", "facility_registry_facilityid")+", p."+table_schema.getColumn("", "providerdb_provcode");

                System.out.println(sql);
                dbObject = dbFacility.facilityDBInfo(zilla[i]);
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject,sql);
                ResultSet rs = dbObject.getResultSet();

                ResultSetMetaData metadata = rs.getMetaData();

                JSONObject zilla_json = new JSONObject();
                JSONObject upazila_json = new JSONObject();
                JSONObject union_json = new JSONObject();
                JSONObject facility_info = new JSONObject();
                JSONObject facility_providers = new JSONObject();
                JSONObject provider_info = null;
                String zilla_name = "";
                String zilla_id = "";
                String upazila_name = "";
                String union_name = "";
                String facility_id = "";
                String facility_name = "";
                int z_up=0,z_un=0,z_p=0,z_f=0,up_un=0,up_p=0,up_f=0,un_p=0,un_f=0,f_p=0;
                while(rs.next()){
                    facility_name = rs.getString("facilityid");
                    if(zilla_id.equals("")) {
                        zilla_name = rs.getString("zillaname");
                        zilla_id = rs.getString("zillaid");
                    }
                    if(!union_json.has(rs.getString("facilityid"))){
                        if(!facility_id.equals("")){
                            facility_info.put("Provider#",f_p);
                            facility_info.put("Provider Info", facility_providers);
                            union_json.put(facility_id, facility_info);
                            facility_info = new JSONObject();
                        }
                        f_p = 0;
                        f++;z_f++;up_f++;un_f++;
                        facility_id = rs.getString("facilityid");
                        if(upazila_json.has(rs.getString("unionid")))
                            union_json.put(facility_id, new JSONObject());
                        facility_info.put("Facility Name", rs.getString("facility_name"));
                        facility_info.put("Facility ID", rs.getString("facilityid"));
                        facility_info.put("Facility Type", rs.getString("facility_type"));
                        facility_providers = new JSONObject();
                    }
                    if(!upazila_json.has(rs.getString("unionid"))){
                        if(!union_name.equals("")) {
                            union_json.put("Facility#", un_f);
                            union_json.put("Provider#", un_p);
                            upazila_json.put(union_name, union_json);
                            union_json = new JSONObject();
                        }
                        union_json.put(facility_id, new JSONObject());
                        un_f=0;un_p=0;
                        un++;z_un++;up_un++;
                        union_name = rs.getString("unionid");
//                        System.out.println(z_un+ " "+rs.getString("unionid")+" "+rs.getString("unionname"));
                        if(zilla_json.has(rs.getString("upazilaid")))
                            upazila_json.put(union_name, new JSONObject());
                        union_json.put("Name", rs.getString("unionname"));
                        union_json.put("ID", rs.getString("unionid"));
                    }
                    if(!zilla_json.has(rs.getString("upazilaid"))){
                        if(!upazila_name.equals("")) {
                            upazila_json.put("Union#", up_un);
                            upazila_json.put("Facility#", up_f);
                            upazila_json.put("Provider#", up_p);
                            zilla_json.put(upazila_name, upazila_json);
                            upazila_json = new JSONObject();
                        }
                        upazila_json.put(union_name, new JSONObject());
                        up_un=0;up_f=0;up_p=0;
                        up++;z_up++;
                        upazila_name = rs.getString("upazilaid");
                        zilla_json.put(upazila_name, new JSONObject());
                        upazila_json.put("Name", rs.getString("upazilaname"));
                        upazila_json.put("ID", rs.getString("upazilaid"));
                    }
                    provider_info = new JSONObject();
                    provider_info.put("Name", rs.getString("provname"));
                    provider_info.put("Type", rs.getString("provtype"));
                    provider_info.put("MobileNo", rs.getString("mobileno"));
                    provider_info.put("Provider ID", rs.getString("providerid"));
                    facility_providers.put(rs.getString("providerid"),provider_info);
                    p++;z_p++;up_p++;un_p++;f_p++;
                }
                facility_info.put("Provider#",f_p);
                facility_info.put("Provider Info", facility_providers);
                union_json.put(facility_id, facility_info);
                union_json.put("Facility#", un_f);
                union_json.put("Provider#", un_p);
                upazila_json.put(union_name, union_json);
                upazila_json.put("Union#", up_un);
                upazila_json.put("Facility#", up_f);
                upazila_json.put("Provider#", up_p);
                zilla_json.put(upazila_name, upazila_json);
                z++;
                zilla_json.put("Name", zilla_name);
                zilla_json.put("ID", zilla_id);
                zilla_json.put("Upazila#", z_up);
                zilla_json.put("Union#", z_un);
                zilla_json.put("Facility#", z_f);
                zilla_json.put("Provider#", z_p);
                all_district.put(zilla_name, zilla_json);
            }
            all_district.put("Zilla#", z);
            all_district.put("Upazila#", up);
            all_district.put("Union#", un);
            all_district.put("Facility#", f);
            all_district.put("Provider#", p);
            emis_info.put("Bangladesh",all_district);

            dbOp.closeStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.print(emis_info);
        return emis_info;
    }
}
