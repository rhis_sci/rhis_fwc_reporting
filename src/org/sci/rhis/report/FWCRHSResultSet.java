package org.sci.rhis.report;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.LinkedHashMap;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

public class FWCRHSResultSet {
	
	public static String resultString;
	public static String graphString;
	
	public static String upazilaSql;
	
	public static String getFinalResult(String sql, String zilla, String upazila){
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
		resultString = "";
		try{
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
		    resultString = "<table class=\"table scrollbar reportExport\" id=\"reportTableScroll\" border='1' style='display: block !important;max-height: 500px !important;overflow-y: scroll !important;'><thead><tr class='danger'>";
		    resultString = resultString + "<th rowspan='3'>" + metadata.getColumnName(1) + "</th>"
	    				+ "<th rowspan='2' colspan='4' style='text-align: center;border-bottom: 1px solid black;'>ANC</th>"
	    				+ "<th rowspan='2' colspan='5' style='text-align: center;border-bottom: 1px solid black;'>Delivery Services</th>"
	    				+ "<th colspan='9' style='text-align: center;border-bottom: 1px solid black;'>Postnatal Care</th>"
	    				+ "<th rowspan='2' colspan='3' style='text-align: center;border-bottom: 1px solid black;'>Reffered</th>"
	    				+ "</tr><tr class='danger'>"
	    				+ "<th colspan='5' style='text-align: center;border-bottom: 1px solid black;'>Mother</th>"
	    				+ "<th colspan='4' style='text-align: center;border-bottom: 1px solid black;'>Neonatal</th>"
	    				+ "</tr><tr class='danger'>";
		    
		    for (int i = 2; i <= columnCount; i++) {
		    	resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
		    	//System.out.println(metadata.getColumnName(i) + ", ");      
		    }
		    resultString = resultString + "</tr></thead><tbody>";
		    
			int j =0;
			while(rs.next()){
				j++;
				if((j%2)==0){
					resultString = resultString + "<tr class='success'>";
				}
				else{
					resultString = resultString + "<tr>";
				}
					
				for (int i = 1; i <= columnCount; i++) {
			    	resultString = resultString + "<td>" + ((rs.getObject(metadata.getColumnName(i))==null)?"0":rs.getString(metadata.getColumnName(i))) + "</td>";
			    	//System.out.println(metadata.getColumnName(i) + ", ");      
			    }
				resultString = resultString + "</tr>";
				
			}
			
			resultString = resultString + "</tbody></table>";
			
			//System.out.println(resultString);
			//System.out.println(resultList[2][1]);
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return resultString;
		
	}
	
	public static String getFinalResultUpazilaWise(String sql, String zilla){
		
		try{
			FacilityDB dbFacility = new FacilityDB();
			DBOperation dbOp = new DBOperation();
			DBInfoHandler dbObject = dbFacility.facilityDBInfo(Integer.parseInt(zilla));
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
		    resultString = "<table class=\"table scrollbar reportExport\" id=\"reportTableScroll\" border='1' style='display: block !important;max-height: 500px !important;overflow-y: scroll !important;'><thead><tr class='danger'>";
		    resultString = resultString + "<th rowspan='3'>Upazila Name</th>";
		    resultString = resultString + "<th rowspan='2' colspan='4' style='text-align: center;border-bottom: 1px solid black;'>ANC</th>"
    				+ "<th rowspan='2' colspan='5' style='text-align: center;border-bottom: 1px solid black;'>Delivery Services</th>"
    				+ "<th colspan='9' style='text-align: center;border-bottom: 1px solid black;'>Postnatal Care</th>"
    				+ "<th rowspan='2' colspan='3' style='text-align: center;border-bottom: 1px solid black;'>Reffered</th>"
    				+ "</tr><tr class='danger'>"
    				+ "<th colspan='5' style='text-align: center;border-bottom: 1px solid black;'>Mother</th>"
    				+ "<th colspan='4' style='text-align: center;border-bottom: 1px solid black;'>Neonatal</th>"
    				+ "</tr><tr class='danger'>";
		    for (int i = 1; i <= columnCount; i++) {
		    	resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
		    	//System.out.println(metadata.getColumnName(i) + ", ");      
		    }
		    resultString = resultString + "</tr></thead><tbody>";
		    
		    if(!rs.isClosed()){
				rs.close();
			}
		    
		    dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		    
		    upazilaSql = "select \"UPAZILANAMEENG\", \"UPAZILAID\" from \"Upazila\" where \"ZILLAID\"="+zilla;
		    
		    FacilityDB dbFacilityUpz = new FacilityDB();
			DBOperation dbOpUpz = new DBOperation();
		    DBInfoHandler dbObjectUpz = dbFacilityUpz.facilityDBInfo(Integer.parseInt(zilla));
		    dbObjectUpz = dbOpUpz.dbCreateStatement(dbObjectUpz);
		    dbObjectUpz = dbOpUpz.dbExecute(dbObjectUpz,upazilaSql);
			ResultSet rs_upz = dbObjectUpz.getResultSet();
			ResultSetMetaData metadataUpz = rs_upz.getMetaData();
			int j =0;
			while(rs_upz.next()){
				j++;
				if((j%2)==0){
					resultString = resultString + "<tr class='success'>";
				}
				else{
					resultString = resultString + "<tr>";
				}
				
				resultString = resultString + "<td>" + rs_upz.getString(metadataUpz.getColumnName(1)) + "</td>";
				
				resultString = resultString + getUpazilaResult(sql, zilla, rs_upz.getString(metadataUpz.getColumnName(2)));
				
				resultString = resultString + "</tr>";

			}
			
			resultString = resultString + "</tbody></table>";
			
			if(!rs_upz.isClosed()){
				rs_upz.close();
			}
			
			
			dbOpUpz.closeResultSet(dbObjectUpz);
			dbOpUpz.closePreparedStatement(dbObjectUpz);
			dbOpUpz.closeConn(dbObjectUpz);
			dbObjectUpz = null;
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		return resultString;
		
	}
	
	public static String getFinalResultZillaWise(String sql, String zilla){
		
		LinkedHashMap<String, String> finalResult = new LinkedHashMap<String, String>();
		LinkedHashMap<Integer, String> columnName = new LinkedHashMap<Integer, String>();
		int tempValue;
		
		try{
			FacilityDB dbFacility = new FacilityDB();
			DBOperation dbOp = new DBOperation();
			DBInfoHandler dbObject = dbFacility.facilityDBInfo(Integer.parseInt(zilla));
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
		    resultString = "<table class=\"table scrollbar reportExport\" id=\"reportTableScroll\" border='1' style='display: block !important;max-height: 500px !important;overflow-y: scroll !important;'><thead><tr class='danger'>";
		    resultString = resultString + "<th rowspan='3'>Zilla Name</th>";
		    resultString = resultString + "<th rowspan='2' colspan='4' style='text-align: center;border-bottom: 1px solid black;'>ANC</th>"
    				+ "<th rowspan='2' colspan='5' style='text-align: center;border-bottom: 1px solid black;'>Delivery Services</th>"
    				+ "<th colspan='9' style='text-align: center;border-bottom: 1px solid black;'>Postnatal Care</th>"
    				+ "<th rowspan='2' colspan='3' style='text-align: center;border-bottom: 1px solid black;'>Reffered</th>"
    				+ "</tr><tr class='danger'>"
    				+ "<th colspan='5' style='text-align: center;border-bottom: 1px solid black;'>Mother</th>"
    				+ "<th colspan='4' style='text-align: center;border-bottom: 1px solid black;'>Neonatal</th>"
    				+ "</tr><tr class='danger'>";
		    for (int i = 1; i <= columnCount; i++) {
		    	columnName.put(i, metadata.getColumnName(i));
		    	resultString = resultString + "<th>" + metadata.getColumnName(i) + "</th>";
		    	//System.out.println(metadata.getColumnName(i) + ", ");      
		    }
		    resultString = resultString + "</tr></thead><tbody><tr>";
		    
		    if(!rs.isClosed()){
				rs.close();
			}
		    
		    dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		    
		    upazilaSql = "select \"Upazila\".\"UPAZILANAMEENG\", \"Upazila\".\"UPAZILAID\", \"Zilla\".\"ZILLANAMEENG\" from \"Upazila\" join \"Zilla\" on \"Zilla\".\"ZILLAID\"=\"Upazila\".\"ZILLAID\" where \"Upazila\".\"ZILLAID\"="+zilla;
		    
		    FacilityDB dbFacilityUpz = new FacilityDB();
			DBOperation dbOpUpz = new DBOperation();
		    DBInfoHandler dbObjectUpz = dbFacilityUpz.facilityDBInfo(Integer.parseInt(zilla));
		    dbObjectUpz = dbOpUpz.dbCreateStatement(dbObjectUpz);
		    dbObjectUpz = dbOpUpz.dbExecute(dbObjectUpz,upazilaSql);
			ResultSet rs_upz = dbObjectUpz.getResultSet();
			ResultSetMetaData metadataUpz = rs_upz.getMetaData();
			int j =0;
			while(rs_upz.next()){
				LinkedHashMap<String, String> resultSet = new LinkedHashMap<String, String>();
				resultSet = getZillaResult(sql, zilla, rs_upz.getString(metadataUpz.getColumnName(2)));
				j++;
				
				if(j==1)
					resultString = resultString + "<td>" + rs_upz.getString(metadataUpz.getColumnName(3)) + "</td>";
					
				
				for (String key : resultSet.keySet()) {
					if(j>1)
						tempValue = Integer.parseInt(finalResult.get(key))+Integer.parseInt(resultSet.get(key));
					else
						tempValue = Integer.parseInt(resultSet.get(key));
					finalResult.put(key, Integer.toString(tempValue));
				}

			}
			
			for (int i = 1; i <= columnCount; i++) {
				resultString = resultString + "<td>" + finalResult.get(columnName.get(i)) + "</td>";
			}
			
			resultString = resultString + "</tr></tbody></table>";
			
			if(!rs_upz.isClosed()){
				rs_upz.close();
			}
			
			
			dbOpUpz.closeResultSet(dbObjectUpz);
			dbOpUpz.closePreparedStatement(dbObjectUpz);
			dbOpUpz.closeConn(dbObjectUpz);
			dbObjectUpz = null;
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		return resultString;
		
	}
	
	public static String getUpazilaResult(String sql, String zilla, String upazila){
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
	    
		String upzResult = "";
		
		try{
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
		    
			int j =0;
			while(rs.next()){
					
				for (int i = 1; i <= columnCount; i++) {
					upzResult = upzResult + "<td>" + ((rs.getObject(metadata.getColumnName(i))==null)?"0":rs.getString(metadata.getColumnName(i))) + "</td>";
			    	      
			    }	
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return upzResult;
		
	}
	
	public static LinkedHashMap<String, String> getZillaResult(String sql, String zilla, String upazila){
		
		FacilityDB dbFacility = new FacilityDB();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
	    
		String upzResult = "";
		
		LinkedHashMap<String, String> resultSet = new LinkedHashMap<String, String>();
		
		try{
			
			dbObject = dbOp.dbCreateStatement(dbObject);
			dbObject = dbOp.dbExecute(dbObject,sql);
			ResultSet rs = dbObject.getResultSet();
			rs.last();
			int rowCount = rs.getRow();
			rs.beforeFirst();
			ResultSetMetaData metadata = rs.getMetaData();
		    int columnCount = metadata.getColumnCount();
		    
		    
			int j =0;
			while(rs.next()){
				String getValue;	
				for (int i = 1; i <= columnCount; i++) {
					if(rs.getString(metadata.getColumnName(i)).equals(""))
						getValue = "0";
					else
						getValue = rs.getString(metadata.getColumnName(i));
					
					resultSet.put(metadata.getColumnName(i), getValue);
			    }	
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			dbOp.closeResultSet(dbObject);
			dbOp.closePreparedStatement(dbObject);
			dbOp.closeConn(dbObject);
			dbObject = null;
		}
		//return searchList;
		return resultSet;
		
	}
	
	

}
