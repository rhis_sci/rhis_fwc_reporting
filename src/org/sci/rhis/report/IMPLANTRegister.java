package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.util.CalendarDate;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import java.time.YearMonth;

public class IMPLANTRegister {
	
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String dateType;
    public static String yearSelect;
    public static String monthSelect;
	public static String startDate;
	public static String endDate;
    public static String resultSet;

	public static String getResultSet(JSONObject reportCred){
        String tableHtml = "";
        try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			union = reportCred.getString("report1_union");
			dateType = reportCred.getString("report1_dateType");
            String facility_id =  reportCred.getString("facilityid");
            startDate = LocalDate.now().withDayOfMonth(1).toString();
            endDate =LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).toString();

                try {
                    if(dateType.equals("1")) {
//                        startDate = reportCred.getString("report1_month")+"-01";
//                        LocalDate date= LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
//                        endDate = date.withDayOfMonth(date.getMonth().maxLength()).toString();
                        monthSelect = reportCred.getString("report1_month");
                        String[] date = monthSelect.split("-");
                        int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
                        startDate = monthSelect + "-01";
                        endDate = monthSelect + "-" + daysInMonth;
                    }
                    else if(dateType.equals("3"))
                    {
                        yearSelect = reportCred.getString("report1_year");
                        startDate = yearSelect + "-01-01";
                        endDate = yearSelect + "-12-31";
                    }else {
                        startDate = reportCred.getString("report1_start_date");
                        endDate = reportCred.getString("report1_end_date");
                        if(startDate.equals("")){
                            LocalDate date= LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US));
                            startDate = date.withDayOfMonth(1).toString();
                        }
                    }
                DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
                FacilityDB dbFacility2nd = new FacilityDB();
                DBInfoHandler dbObject2nd = dbFacility2nd.facilityDBInfo(Integer.parseInt(zilla));
                DBOperation dbOp2nd = new DBOperation();
                dbObject2nd = dbOp2nd.dbCreateStatement(dbObject2nd);
//                dbObject2nd = dbOp2nd.dbExecute(dbObject2nd, "SELECT fp."+table_schema.getColumn("table", "facility_provider_provider_id")+",pr."+table_schema.getColumn("table", "providerdb_zillaid")+",pr."+table_schema.getColumn("table", "providerdb_upazilaid")+",pr."+table_schema.getColumn("table", "providerdb_unionid")+
//                        " from "+table_schema.getTable("table_facility_provider")+" as fp"+
//                        " LEFT join "+table_schema.getTable("table_providerdb")+" as pr on pr."+table_schema.getColumn("table", "providerdb_provcode")+"= fp."+table_schema.getColumn("table", "facility_provider_provider_id")+
//                        " where fp."+table_schema.getColumn("", "facility_provider_facility_id")+"="+facility_id);
                dbObject2nd = dbOp2nd.dbExecute(dbObject2nd, "SELECT "+table_schema.getColumn("","facility_provider_provider_id")+" from facility_provider where facility_id = "+facility_id);
                //System.out.println(facility_id);
                ResultSet provider_obj = dbObject2nd.getResultSet();
                String providerList = "";
                int x= 0;
                while (provider_obj.next()) {
                    String id = provider_obj.getString("providerid");
                    providerList += id +",";
                    if(providerList!="" && x == 0){
                        DBInfoHandler dbObject3rd = dbFacility2nd.facilityDBInfo(Integer.parseInt(zilla));
                        DBOperation dbOp3rd = new DBOperation();
                        dbObject3rd = dbOp3rd.dbCreateStatement(dbObject3rd);
                        dbObject3rd = dbOp3rd.dbExecute(dbObject3rd, "SELECT " +table_schema.getColumn("table", "providerdb_zillaid")+","+table_schema.getColumn("table", "providerdb_upazilaid")+","+table_schema.getColumn("table", "providerdb_unionid")+
                                " from "+table_schema.getTable("table_providerdb")+" where "+table_schema.getColumn("table", "providerdb_provcode")+" = "+id);
                        ResultSet provider_obj2 = dbObject3rd.getResultSet();
                        while (provider_obj2.next()) {
                            upazila = provider_obj2.getString("upazilaid");
                        }
                    }
                    x++;
                }
                providerList = providerList.substring(0, providerList.length() - 1);
                String  duration = "1" ;
                String sql = "SELECT cm.*,vil.*,uni.*,upa.*,zil.*,elc.*, ims.*, imfs.*," +
                        "to_char(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+",'DD MM YYYY') as implant_date, "+
                        "to_char(imfs."+table_schema.getColumn("", "implantfollowupservice_followupdate")+",'DD MM YYYY') as followup_date, "+
                        "to_char(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+"+ interval '1 month' ,'DD MM YYYY') as first_fixed_date,"+
                        "to_char(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+"+ interval '6 month' ,'DD MM YYYY') as second_fixed_date,"+
                        "to_char(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+"+ interval '1 year' ,'DD MM YYYY') as third_fixed_date,"+
                        "CASE  ims."+table_schema.getColumn("", "implantservice_clientallowance")+"::integer WHEN 2 THEN 'দেওয়া হয়নি' when 1 then 'দেয়া হয়েছে' ELSE '' END as client_allowance, " +
                        "CASE ims."+table_schema.getColumn("","implantservice_implanttype")+"::integer WHEN 2 THEN 'জ্যাডেল' WHEN 1 Then 'ইমপ্ল্যানন' ELSE '' END as implant_name,"+
                        "CASE  ims."+table_schema.getColumn("","implantservice_implantafterdelivery")+"::integer WHEN 1 THEN concat(date_part('month',age(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+", cm.dob)),'  মাস')  ELSE '' END as implant_after_deli, " +
                        "CASE WHEN age(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+", cm.dob) IS NULL THEN '' ELSE concat(date_part('year',age(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+", cm.dob)),'বছর  ', date_part('month',age(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+", cm.dob)),'মাস  ', date_part('day',age(ims."+table_schema.getColumn("", "implantservice_implantimplantdate")+", cm.dob)),'দিন') END as client_age," +
                        "imfs."+table_schema.getColumn("", "implantfollowupservice_implantremovereason")+" as implant_remover_reason,"+
                        "to_char(imfs."+table_schema.getColumn("", "implantfollowupservice_implantremovedate")+",'DD MM YYYY') as implant_remover_date,"+
                        "CASE  imfs."+table_schema.getColumn("", "implantfollowupservice_fpmethod")+"::integer WHEN 1 THEN 'খাবার বড়ি (সুখী)' when 2 then 'কনডম' when 3 then 'ইনজেকটেবলস' when 8 then 'ইসিপি' when 10 then 'খাবার বড়ি (আপন)' else '' end as method_name,"+
                        "imfs."+table_schema.getColumn("", "implantfollowupservice_implantremoverdesignation")+" as implant_remover_deg "+

                        " FROM "+ table_schema.getTable("table_implantservice")+" as ims " +
//                        "LEFT JOIN LATERAL( SELECT ARRAY(SELECT imfs.* FROM"+table_schema.getTable("table_implantfollowupservice")+" AS imfs WHERE ims.\"healthId\" = imfs.\"healthId\") AS follow_up_services ) imfss ON true"+
//                        "(SELECT imfs.*"+table_schema.getTable("table_implantfollowupservice")+" as imfs ON ims.\"healthId\" = imfs.\"healthId\")"+
                        " LEFT JOIN "+table_schema.getTable("table_implantfollowupservice")+" as imfs ON ims."+table_schema.getColumn("","implantservice_healthid")+" = imfs."+table_schema.getColumn("","implantfollowupservice_healthid")+""+

                        " LEFT JOIN "+ table_schema.getTable("table_clientmap")+" as cm ON cm."+table_schema.getColumn("","clientmap_generatedid")+" = ims."+table_schema.getColumn("","implantservice_healthid")+"" +
                        " LEFT JOIN "+ table_schema.getTable("table_elco")+" as elc ON elc."+table_schema.getColumn("","elco_healthid")+" = ims."+table_schema.getColumn("","implantservice_healthid")+"" +
                        " LEFT JOIN "+ table_schema.getTable("table_village")+" as vil ON cm."+table_schema.getColumn("","clientmap_villageid")+" = vil."+table_schema.getColumn("","village_villageid")+" and cm."+table_schema.getColumn("","clientmap_mouzaid")+" = vil."+table_schema.getColumn("","village_mouzaid")+" and cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = vil."+table_schema.getColumn("","village_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_unionid")+" = vil."+table_schema.getColumn("","village_unionid")+"  and cm."+table_schema.getColumn("","clientmap_zillaid")+" = vil."+table_schema.getColumn("","village_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_unions")+" as uni ON cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = uni."+table_schema.getColumn("","unions_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_unionid")+" = uni."+table_schema.getColumn("","unions_unionid")+"  and cm."+table_schema.getColumn("","clientmap_zillaid") +" = uni."+table_schema.getColumn("","unions_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_upazila")+" as upa ON cm."+ table_schema.getColumn("","clientmap_upazilaid") +" = upa."+table_schema.getColumn("","upazila_upazilaid")+" and cm."+table_schema.getColumn("","clientmap_zillaid") +" = upa."+table_schema.getColumn("","upazila_zillaid")+" " +
                        " LEFT JOIN "+ table_schema.getTable("table_zilla")+" as zil ON cm."+table_schema.getColumn("","clientmap_zillaid") +" = zil."+table_schema.getColumn("","zilla_zillaid")+" " +

                        "WHERE ims."+table_schema.getColumn("","implantservice_providerid") +" IN ("+providerList+") and   ims."+table_schema.getColumn("","implantservice_implantimplantdate") +" BETWEEN '"+startDate+"' AND '"+endDate+"' or imfs."+ table_schema.getColumn("", "implantfollowupservice_followupdate")+" BETWEEN '"+startDate+"' AND '"+endDate+"' order by ims."+table_schema.getColumn("","implantservice_implantimplantdate") +"  asc";
                System.out.println(sql);
                FacilityDB dbFacility = new FacilityDB();
                DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla,upazila);

                DBOperation dbOp = new DBOperation();
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbObject = dbOp.dbExecute(dbObject, sql);
                ResultSet rs = dbObject.getResultSet();
                //*******************************************//

                List<JSONObject> jsonObj = new ArrayList<JSONObject>();
                ResultSetMetaData meta = rs.getMetaData();
                while (rs.next()) {
                    HashMap<String, String> data = new HashMap<String, String>();
                    for (int i = 1; i <= meta.getColumnCount(); i++) {
                        String key = meta.getColumnName(i);
                        String value = rs.getString(key);
                        if(value == null){
                            value = "";
                        }
                        data.put(key, value);
                    }
                    jsonObj.add(new JSONObject(data));
                }
//                System.out.println(jsonObj.toString());
                return jsonObj.toString();

                //*******************************************//
            }catch (Exception e){
                System.out.println(e);
            }

//			System.out.println(startDate+"   "+endDate);
		}
		catch(Exception e){
			System.out.println(e);
            resultSet = "Bad Url Request";
		}
		return resultSet;
	}

}
