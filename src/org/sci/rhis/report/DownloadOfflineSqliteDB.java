package org.sci.rhis.report;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.json.JSONObject;
import org.sci.rhis.db.ConfInfoRetrieve;
import org.sci.rhis.db.DBInfoHandler;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class DownloadOfflineSqliteDB {

    public static boolean download(JSONObject reportCred){
        boolean status = false;

        String dbName = reportCred.getString("dbName");
        Integer zilla = Integer.parseInt(dbName.split("_")[1]);

        DBInfoHandler detailDomain = new DBInfoHandler();
        Properties prop = ConfInfoRetrieve.readingConf(detailDomain.getDomainLoc());
        String[] DomainDetails = prop.getProperty("DOMAININFO_"+zilla).split(",");

        String host=DomainDetails[0];
        String user=DomainDetails[1];
        String password=DomainDetails[2];
        Integer portNum=Integer.parseInt(DomainDetails[3]);

        try {
            String command1="bash /usr/share/tomcat/webapps/sqlite_db_creation/pg_to_sqlite3.sh /usr/share/tomcat/webapps/sqlite_db_creation/table_list.txt "+dbName;
            ArrayList<String> result = new ArrayList<String>();

            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session=jsch.getSession(user, host, portNum);
            session.setPassword(password);
            session.setConfig(config);
            session.connect();
            System.out.println("Connected");

            Channel channel=session.openChannel("exec");
            ((ChannelExec)channel).setCommand(command1);
            channel.setInputStream(null);
            ((ChannelExec)channel).setErrStream(System.err);

            InputStream in=channel.getInputStream();
            channel.connect();

            // Read the output from the input stream we set above
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;

            //Read each line from the buffered reader and add it to result list
            // You can also simple print the result here
            while ((line = reader.readLine()) != null)
            {
                result.add(line);
            }
            System.out.println(Arrays.asList(result));
            //retrieve the exit status of the remote command corresponding to this channel
            int exitStatus = channel.getExitStatus();

            channel.disconnect();
            session.disconnect();
            System.out.println("DONE");
        }
        catch (Exception e){
            status = false;
            e.printStackTrace();
        }

        return status;
    }
}
