package org.sci.rhis.report;

import org.json.JSONObject;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.EnglishtoBangla;





//import java.time.YearMonth;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MIS4Report {
	
	public static String zilla;
	public static String upazila;
	public static String union;
	public static String resultString;
	public static String sql_zilla;
	public static String sql_upazila;
	public static String sql_union;
	public static String sql_elco;
	public static String sql_injectable;
	public static String sql_iud;
	public static String sql_iud_followup;
	public static String sql_pill_comdom;
	public static String sql_anc;
	public static String sql_pac;
	public static String sql_newborn;
	public static String sql_child;
	public static String sql_mother;
	public static String sql_delivery;
	public static String date_type;
	public static String monthSelect;
	public static String start_date;
	public static String end_date;
	public static String bangla_month;
	public static String bangla_year;
	public static String db_name;
	
	final static int referYes = 1;
	final static int newborn = 28;
	
	final static int normalDelivery = 1;
	final static int csectionDelivery = 2;
	final static int applyOxytocinYes = 1;
	final static int applyTractionYes = 1;
	final static int applyUterusMassageYes = 1;
	final static int deliveryDoneByThisProviderYes = 1;
	
	static Map<String, String> zilla_info = new HashMap<String, String>();
	static Map<String, String> upazila_info = new HashMap<String, String>();
	static Map<String, String> union_info = new HashMap<String, String>();
	static LinkedHashMap<String, LinkedHashMap<String, String>> elco = new LinkedHashMap<String, LinkedHashMap<String, String>>();
	static Map<String, String> injectable = new HashMap<String, String>();
	static Map<String, String> iud = new HashMap<String, String>();
	static Map<String, String> iud_followup = new HashMap<String, String>();
	static LinkedHashMap<String, LinkedHashMap<String, String>> pill_condom = new LinkedHashMap<String, LinkedHashMap<String, String>>();
	static Map<String, String> anc = new HashMap<String, String>();
	static Map<String, String> pac = new HashMap<String, String>();
	static Map<String, String> pnc_newborn = new HashMap<String, String>();
	static Map<String, String> pnc_child = new HashMap<String, String>();
	static Map<String, String> pnc_mother = new HashMap<String, String>();
	static Map<String, String> delivery = new HashMap<String, String>();
	
	static LinkedHashMap<String, LinkedHashMap<String, String>> all_service = new LinkedHashMap<String, LinkedHashMap<String, String>>();
	
	
	
	public static String getResultSet(JSONObject reportCred){
		resultString = "";
		try{
			zilla = reportCred.getString("report1_zilla");
			upazila = reportCred.getString("report1_upazila");
			union = reportCred.getString("report1_union");
			date_type = reportCred.getString("report1_dateType");
			
			//System.out.println(reportCred);
			
			//db_name = "RHIS_"+String.format("%02d", Integer.parseInt(zilla))+"_"+String.format("%02d", Integer.parseInt(upazila));
			//System.out.println(db_name);
			if(date_type.equals("1"))
			{
				monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");
				//YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				//int daysInMonth = yearMonthObject.lengthOfMonth();
				int daysInMonth = CalendarDate.daysInMonth(Integer.parseInt(date[0]), Integer.parseInt(date[1]));
				start_date = monthSelect + "-01";
				end_date = monthSelect + "-"+daysInMonth;
				
				bangla_month = EnglishtoBangla.ConvertMonth(date[1]);
				bangla_year = EnglishtoBangla.ConvertNumberToBangla(date[0]);
				
			}
			else
			{
				start_date = reportCred.getString("report1_start_date");
				end_date = reportCred.getString("report1_end_date");
			}
			
//			sql_zilla = "select * from \"Zilla\" where \"ZILLAID\"="+zilla;
//			zilla_info = GetMIS3ResultSet.getFinalResult(sql_zilla, "getString", zilla, upazila);
//			
//			sql_upazila = "select * from \"Upazila\" where \"ZILLAID\" = "+zilla+" and \"UPAZILAID\" = "+upazila;
//			upazila_info = GetMIS3ResultSet.getFinalResult(sql_upazila, "getString", zilla, upazila);
//			
//			sql_union = "select * from \"Unions\" where \"ZILLAID\" = "+zilla+" and \"UPAZILAID\" = "+upazila+" and \"UNIONID\" = "+union;
//			union_info = GetMIS3ResultSet.getFinalResult(sql_union, "getString", zilla, upazila);
						
			sql_elco = "SELECT \"Unions\".\"UNIONNAMEENG\", count(distinct(\"elcoVisit\".\"healthId\")) as \"3\" from \"elcoVisit\" "
					+ "join \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=\"elcoVisit\".\"providerId\" "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "where \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila+" and \"elcoVisit\".\"vDate\" BETWEEN ('" + start_date + "') AND ('" + end_date +"') group by \"Unions\".\"UNIONNAMEENG\"";
			System.out.println(sql_elco);
			elco = GetMIS4ResultSet.getFinalResult(sql_elco, zilla, upazila);
			
			all_service.putAll(elco);
			
			sql_pill_comdom = "SELECT \"Unions\".\"UNIONNAMEENG\", "
					+ "COUNT(CASE WHEN ((pcs1.\"methodType\"=1 OR pcs1.\"methodType\"=10) AND pcs1.\"isNewClient\"=1) THEN 1 ELSE NULL END) as pill_new, "
					+ "COUNT(CASE WHEN ((pcs1.\"methodType\"=1 OR pcs1.\"methodType\"=10) AND pcs1.\"isNewClient\"=2) THEN 1 ELSE NULL END) as pill_old, "
					+ "COUNT(CASE WHEN (pcs1.\"methodType\"=2 AND pcs1.\"isNewClient\"=1) THEN 1 ELSE NULL END) as condom_new, "
					+ "COUNT(CASE WHEN (pcs1.\"methodType\"=2 AND pcs1.\"isNewClient\"=2) THEN 1 ELSE NULL END) as condom_old, "
					+ "COUNT(CASE WHEN ((pcs1.\"methodType\"=1 OR pcs1.\"methodType\"=10) AND ((pcs2.\"methodType\"!=1 AND pcs2.\"methodType\"!=10))) THEN 1 ELSE NULL END) as pill_to_other,  "
					+ "COUNT(CASE WHEN ((pcs2.\"methodType\"=1 OR pcs2.\"methodType\"=10) AND pcs1.\"methodType\" IN (100,101,102,103)) THEN 1 ELSE NULL END) as pill_to_none, " 
					+ "COUNT(CASE WHEN (pcs1.\"methodType\"=2 AND (pcs2.\"methodType\"!=2)) THEN 1 ELSE NULL END) as condom_to_other, "  
					+ "COUNT(CASE WHEN (pcs2.\"methodType\"=2 AND pcs1.\"methodType\" IN (100,101,102,103)) THEN 1 ELSE NULL END) as condom_to_none " 
					+ "FROM \"pillCondomService\" pcs1 " 
					+ "LEFT JOIN \"pillCondomService\" pcs2 on pcs1.\"healthId\"=pcs2.\"healthId\" and pcs2.\"visitDate\"=(select max(\"visitDate\") from \"pillCondomService\" where \"visitDate\"<pcs1.\"visitDate\") "
					+ "JOIN \"ProviderDB\" on \"ProviderDB\".\"ProvCode\"=pcs1.\"providerId\" "
					+ "JOIN \"Unions\" on \"Unions\".\"ZILLAID\"=\"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\"=\"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\"=\"ProviderDB\".unionid "
					+ "WHERE \"ProviderDB\".zillaid = "+zilla+" and \"ProviderDB\".upazilaid = "+upazila+" and pcs1.\"visitDate\" BETWEEN ('" + start_date + "') AND ('" + end_date +"') GROUP BY \"Unions\".\"UNIONNAMEENG\"";
			System.out.println(sql_pill_comdom);
			pill_condom = GetMIS4ResultSet.getFinalResult(sql_pill_comdom, zilla, upazila);
			
			all_service.putAll(pill_condom);
			
			System.out.println((LinkedHashMap<String, LinkedHashMap<String, String>>) all_service);
			
			
			resultString = "<div style='float: right;'><button type='button' class='btn btn-default pre_button' value='0' disabled><i class='fa fa-chevron-left' aria-hidden='true'></i></button><span class='showing_page'>&nbsp&nbsp 1 of 2 &nbsp&nbsp</span><button type='button' class='btn btn-default next_button' value='2'><i class='fa fa-chevron-right' aria-hidden='true'></i></button></div>";
			
			resultString += "<table class=\"table\" id=\"table_header_1\" width=\"100%\" cellspacing=\"0\" style='font-size:10px;'><tr>"
							+ "<td><div style='border: 1px solid black;text-align: center;'>দুটি সন্তানের বেশি নয়<br>একটি হলে ভাল হয়।</div></td>"
							+ "<td style='text-align: center;'><b>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>পরিবার পরিকল্পনা অধিদপ্তর<br>পরিবার পরিকল্পনা, মা ও শিশু স্বাস্থ্য কার্যক্রমের মাসিক অগ্রগতির প্রতিবেদন</b>"
							+ "<br>(উপজেলা/থানা পরিবার পরিকল্পনা অফিস কর্তৃক পূরণীয়)<br>"
							+ "<b>মাসঃ</b> "+bangla_month+" <b>সালঃ</b> "+bangla_year+"</td>"
							+ "<td style='text-align: right;' class='page_no'>এমআইএস ফরম-৪<br>পৃষ্ঠা-১</td>"
							+ "</tr><tr><td colspan='3'><img width='60' src='image/dgfp_logo.png'></td></tr><tr></table>";
					
	
			resultString += "<table class=\"table\" id=\"table_header_2\" width=\"100%\" cellspacing=\"0\" style='font-size:10px;'><tr>"
							+ "<td style='text-align: center;'><b>উপজেলা/থানাঃ </b> "+upazila_info.get("UPAZILANAME")+"</td>"
							+ "<td style='text-align: center;'><b>জেলাঃ</b> "+zilla_info.get("ZILLANAME")+"</td>"
							+"</tr></table>";
			
			resultString += "<table class=\"table page_1 table_hide\" id=\"table_1\" width=\"100%\" border=\"1\" cellspacing=\"0\" style='font-size:11px;'><tr>"
							+ "<td rowspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>ক্রমিক নং</sapn></td>"
							+ "<td rowspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইউনিয়নের নাম</td>"
							+ "<td rowspan='5' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট সক্ষম দম্পতির সংখ্যা</sapn></td>"
							+ "<td colspan='32' style='text-align: center;border-top: 1px solid black;'><b>পরিবার পরিকল্পনা পদ্ধতি গ্রহণকারী</b></td>"
							+ "<td rowspan='5' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>গ্রহণকারীর হার (CAR)</sapn></td>"
							+ "</tr>";
			
			resultString += "<tr>"
							+ "<td rowspan='2' colspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>খাবার বড়ি</td>"
							+ "<td rowspan='2' colspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>কনডম</td>"
							+ "<td rowspan='2' colspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইনজেকটেবল</td>"
							+ "<td rowspan='2' colspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>আইইউডি</td>"
							+ "<td rowspan='2' colspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইমপ্ল্যান্ট</td>"
							+ "<td colspan='6' style='text-align: center;border-top: 1px solid black;'>স্থায়ী পদ্ধতি</td>"
							+ "<td rowspan='4' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>সর্বমোট গ্রহণকারী</sapn></td>"
							+ "</tr>";
			
			resultString += "<tr>"
							+ "<td colspan='3' style='text-align: center;border-top: 1px solid black;'>পুরুষ</td>"
							+ "<td colspan='3' style='text-align: center;border-top: 1px solid black;'>মহিলা</td>"
							+ "</tr>";
			
			resultString += "<tr>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>নতুন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>পুরাতন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট</sapn></td>"
							+ "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>ছেড়ে দিয়েছে</td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>নতুন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>পুরাতন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট</sapn></td>"
							+ "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>ছেড়ে দিয়েছে</td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>নতুন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>পুরাতন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট</sapn></td>"
							+ "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>ছেড়ে দিয়েছে</td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>নতুন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>পুরাতন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট</sapn></td>"
							+ "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>ছেড়ে দিয়েছে</td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>নতুন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>পুরাতন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট</sapn></td>"
							+ "<td colspan='2' style='text-align: center;border-top: 1px solid black;'>ছেড়ে দিয়েছে</td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>নতুন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>পুরাতন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>নতুন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>পুরাতন</sapn></td>"
							+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>মোট</sapn></td>"
							+ "</tr>";
			
			resultString += "<tr>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>কোন পদ্ধতি নেয়নি</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>অন্য পদ্ধতি নিয়েছে</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>কোন পদ্ধতি নেয়নি</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>অন্য পদ্ধতি নিয়েছে</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>কোন পদ্ধতি নেয়নি</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>অন্য পদ্ধতি নিয়েছে</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>কোন পদ্ধতি নেয়নি</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>অন্য পদ্ধতি নিয়েছে</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>কোন পদ্ধতি নেয়নি</sapn></td>"
							+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>অন্য পদ্ধতি নিয়েছে</sapn></td>"
							+ "</tr>";
			
			resultString += "<tr>";
					for(int c=1;c<=36;c++)
					{
						resultString += "<td style='text-align: center;border-top: 1px solid black;'>"+c+"</td>";
					}	
			resultString += "</tr>";
			
			resultString += "</table>";
			
			resultString += "<table class=\"table page_2 table_hide\" id=\"table_2\" width=\"700\" border=\"1\" cellspacing=\"0\" style='margin-top:10px;font-size:13px;display:none;'>"
					+ "<tr><td colspan='37' style='text-align: right;border-right: 1px solid rgba(255,255,255,0);border-left: 1px solid rgba(255,255,255,0);border-top: 1px solid rgba(255,255,255,0);'>এমআইএস ফরম-৪<br>পৃষ্ঠা-২</td></tr>"
					+ "<tr>"
					+ "<td rowspan='4' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 10px;'>ক্রমিক নং</sapn></td>"
					+ "<td rowspan='4' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>ইউনিয়নের নাম</td>"
					+ "<td colspan='35' style='text-align: center;border-top: 1px solid black;'><b>প্রজনন স্বাস্থ্য সেবা</b></td>"
					+ "</tr>";
			
			resultString += "<tr>"
					+ "<td rowspan='2' colspan='3' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>চলতি মাসে গর্ভবতীর সংখ্যা</td>"
					+ "<td rowspan='3' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পূর্ববর্তী মাসে মোট গর্ভবতীর সংখ্যা</td>"
					+ "<td rowspan='3' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ইউনিয়নের সর্বমোট গর্ভবতীর সংখ্যা</td>"
					+ "<td rowspan='2' colspan='4' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>গর্ভকালীন সেবার তথ্য</td>"
					+ "<td colspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>প্রসব সেবার তথ্য</td>"
					+ "<td colspan='8' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>প্রসবোত্তর সেবার তথ্য</td>"
					+ "<td rowspan='3' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>গর্ভপাত পরবর্তী সেবা প্রদানের সংখ্যা</td>"
					+ "<td rowspan='3' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি প্রদানের সংখ্যা</td>"
					+ "<td colspan='4' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>রেফার</td>"
					+ "<td rowspan='2' colspan='2' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>বন্ধ্যা দম্পতি</td>"
					+ "<td rowspan='2' colspan='5' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>টি টি প্রাপ্ত ম্হিলা সংখ্যা</td>"
					+ "</tr>";
			
			resultString += "<tr>"
					+ "<td colspan='2' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>বাড়ি</td>"
					+ "<td colspan='2' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>হাসপাতাল / ক্লিনিক</td>"
					+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;margin-bottom: 5px;'>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনা (AMTSL)<br>অনুসরণ করে প্রসব করানোর সংখ্যা</td>"
					+ "<td colspan='4' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>মা</td>"
					+ "<td colspan='4' style='text-align: center;vertical-align: middle;border-top: 1px solid black;'>নবজাতক</td>"
					+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>ঝুঁকিপূর্ণ/জটিল গর্ভবতীর সংখ্যা</td>"
					+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;margin-bottom: 5px;'>গর্ভকালীন, প্রসবকালীন ও প্রসবোত্তর  জটিলতার<br>সংখ্যা</td>"
					+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 40px;margin-bottom: 5px;'>একলাম্পসিয়া রোগীকে MgSO4 ইনজেকশন<br>প্রদানের সংখ্যা</td>"
					+ "<td rowspan='2' style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 100px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>নবজাতককে জটিলতার জন্য প্রেরণের সংখ্যা</td>"
					+ "</tr>";
			
			resultString += "<tr>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>নতুন</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পুরাতন</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>মোট</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -১</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -২</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -৩</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -৪</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>প্রশিক্ষণপ্রাপ্ত ব্যক্তি দ্বারা</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>প্রশিক্ষণ বিহীন ব্যক্তি দ্বারা</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>স্বাভাবিক</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>সিজারিয়ান</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -১</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -২</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -৩</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -৪</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -১</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -২</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -৩</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরিদর্শন -৪</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>পরামর্শ প্রাপ্ত </td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>রেফারকৃত</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>১ম</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>২য়</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>৩য়</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>৪র্থ</td>"
					+ "<td style='text-align: center;vertical-align: bottom;border-top: 1px solid black;height: 230px;'><span style='display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg);white-space: nowrap;width: 25px;margin-bottom: 5px;'>৫ম</td>"
					+ "</tr>";
			
			resultString += "<tr>"
					+ "<td style='text-align: center;vertical-align: middle;border-top: 1px solid black;'></td>"
					+ "<td style='text-align: center;vertical-align: middle;border-top: 1px solid black;'></td>";
			for(int c=37;c<=71;c++)
			{
				resultString += "<td style='text-align: center;border-top: 1px solid black;'>"+c+"</td>";
			}	
			resultString += "</tr>";
			
			resultString += "</table>";
					
			resultString += "<div style='float: right;'><button type='button' class='btn btn-default pre_button' value='0' disabled><i class='fa fa-chevron-left' aria-hidden='true'></i></button><span class='showing_page'>&nbsp&nbsp 1 of 2 &nbsp&nbsp</span><button type='button' class='btn btn-default next_button' value='2'><i class='fa fa-chevron-right' aria-hidden='true'></i></button></div>";
			
			//System.out.println(resultString);
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		return resultString;
	}

}
