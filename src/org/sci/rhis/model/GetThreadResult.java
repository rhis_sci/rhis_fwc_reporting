package org.sci.rhis.model;

import org.json.JSONObject;
import org.sci.rhis.report.GetMapInfo;

import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

public class GetThreadResult implements Callable<JSONObject> {
    private String zilla;
    private String upazila;
    private String sql;
    private JSONObject getInfo = new JSONObject();

    public GetThreadResult(String zilla, String upazila, String sql){
        this.zilla = zilla;
        this.upazila = upazila;
        this.sql = sql;
    }

    @Override
    public JSONObject call() {
        GetResult getResult = new GetResult(sql, zilla, upazila);
        try {
            getInfo.put(zilla+"_"+upazila, getResult.getResultData());
//            System.out.println(getInfo);
            return getInfo;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            getResult = null;

        }

        return getInfo;
    }
}
