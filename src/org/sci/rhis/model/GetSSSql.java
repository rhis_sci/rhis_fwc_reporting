package org.sci.rhis.model;

import org.sci.rhis.db.ConfInfoRetrieve;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.util.AES;

import java.util.Properties;

public class GetSSSql {

    public String getSql(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date){
        String create_sql = "";
        DBSchemaInfo table_schema = new DBSchemaInfo();

        DBInfoHandler detailFacility = new DBInfoHandler();
        Properties prop = ConfInfoRetrieve.readingConf(detailFacility.getPropLoc());
        String[] dbDetails = AES.decrypt(prop.getProperty("FACILITY_"+zilla), "rhis_db_info").split(",");
        String[] dbUrl = ((dbDetails[0].split("//"))[1]).split(":");
//        System.out.println(dbUrl[0]);

        try {
            String sql_condition = "";

            String dblink_providerdb = "join dblink('host="+dbUrl[0]+" port="+dbUrl[1].replace("/", "")+" dbname="+dbDetails[1]+" user="+dbDetails[2]+" password="+dbDetails[3]+"',$$ " +
                    "select "+ table_schema.getColumn("","facility_provider_provider_id")+", "+ table_schema.getColumn("","associated_provider_id_provider_id")+", " +
                    table_schema.getColumn("","associated_provider_id_associated_id")+", "+"(case when \"associated_id\" is null then"+table_schema.getColumn("","facility_registry_facility_name")+" else" +table_schema.getColumn("","facility_registry_facility_name")+" ||' (additional)'" +"end )as facility_name"+", " +
                    table_schema.getColumn("","upazila_upazilanameeng")+" as \"Upazila Name\", " +
                    table_schema.getColumn("","unions_unionnameeng")+" as \"Union Name\", " + table_schema.getColumn("table", "unions_unionid") + ", "+ table_schema.getColumn("table","facility_registry_facilityid")+" " +
                    "from  "+ table_schema.getTable("table_facility_provider") +" " +
                    "join "+ table_schema.getTable("table_facility_registry") +" on "+ table_schema.getColumn("table","facility_provider_facility_id")+"="+ table_schema.getColumn("table","facility_registry_facilityid")+" " +
                    "left join "+ table_schema.getTable("table_associated_provider_id") +" on "+ table_schema.getColumn("table","facility_provider_provider_id")+"="+ table_schema.getColumn("table","associated_provider_id_associated_id")+" " +
                    "left join " + table_schema.getTable("table_upazila") + " on "+table_schema.getColumn("table","upazila_zillaid")+"="+table_schema.getColumn("table","facility_registry_zillaid")+" AND "+table_schema.getColumn("table","upazila_upazilaid")+"="+table_schema.getColumn("table","facility_registry_upazilaid")+" " +
                    "left join " + table_schema.getTable("table_unions") + " on "+table_schema.getColumn("table","unions_zillaid")+"="+table_schema.getColumn("table","facility_registry_zillaid")+" AND "+table_schema.getColumn("table","unions_upazilaid")+"="+table_schema.getColumn("table","facility_registry_upazilaid")+" AND "+table_schema.getColumn("table","unions_unionid")+"="+table_schema.getColumn("table","facility_registry_unionid")+ " " +
                    "where "+ table_schema.getColumn("table","facility_registry_zillaid", new String[]{zilla}, "=")+ " and "+ table_schema.getColumn("table","facility_registry_upazilaid", new String[]{upazila}, "=")+ " and " +
                    "("+ table_schema.getColumn("", "facility_provider_is_active",new String[]{"1"},"=") +" or ("+ table_schema.getColumn("", "facility_provider_is_active",new String[]{"2"},"=") +" and "+ table_schema.getColumn("", "facility_provider_end_date",new String[]{start_date},">=") +")) $$) as facility_info " +
                    "("+ table_schema.getColumn("","facility_provider_provider_id")+" integer, "+ table_schema.getColumn("","associated_provider_id_provider_id")+" integer, "+table_schema.getColumn("","associated_provider_id_associated_id")+" integer, "+table_schema.getColumn("","facility_registry_facility_name")+" text, \"Upazila Name\" text, \"Union Name\" text, "+ table_schema.getColumn("", "unions_unionid") +" integer, "+ table_schema.getColumn("","facility_registry_facilityid")+" integer) " +
                    "on "+ table_schema.getColumn("facility_info","facility_provider_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" or "+ table_schema.getColumn("facility_info","associated_provider_id_provider_id")+"="+table_schema.getColumn("table","providerdb_provcode")+" ";

            String dblink_service =  "left join dblink('host="+dbUrl[0]+" port="+dbUrl[1].replace("/", "")+" dbname="+dbDetails[1]+" user="+dbDetails[2]+" password="+dbDetails[3]+"',$$ " +
                    "select "+ table_schema.getColumn("","associated_provider_id_provider_id")+", "+table_schema.getColumn("","associated_provider_id_associated_id")+" " +
                    "from "+ table_schema.getTable("table_associated_provider_id") +" " +
                    "join "+ table_schema.getTable("table_facility_provider") +" on "+ table_schema.getColumn("table","facility_provider_provider_id")+"="+ table_schema.getColumn("table","associated_provider_id_associated_id")+" " +
                    "where "+ table_schema.getColumn("", "facility_provider_is_active",new String[]{"1"},"=") +" or ("+ table_schema.getColumn("", "facility_provider_is_active",new String[]{"2"},"=") +" and "+ table_schema.getColumn("", "facility_provider_end_date",new String[]{start_date},">=") +") $$) as associated_provider_id " +
                    "(providerid integer, associated_id integer) on associated_provider_id.associated_id=";

            if(sql_view_type.equals("1"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " ";
            else if(sql_view_type.equals("2"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " ";
            else if(sql_view_type.equals("3") || sql_view_type.equals("4") || form_type.equals("1"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"4","5","6","101","17"},"in") + " and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " and " + table_schema.getColumn("", "providerdb_endate",new String[]{end_date},"<=") + " ";
            else if(form_type.equals("11"))
                sql_condition = table_schema.getColumn("table", "providerdb_provtype",new String[]{"2","3"},"in") + " and "+ table_schema.getColumn("table", "providerdb_csba",new String[]{"1"},"=") +" and " + table_schema.getColumn("table", "providerdb_zillaid",new String[]{zilla},"=") + " and " + table_schema.getColumn("table", "providerdb_upazilaid",new String[]{upazila},"=") + " and " + table_schema.getColumn("", "providerdb_endate",new String[]{end_date},"<=") + " ";

            String select_element = "";
            if(sql_view_type.equals("3"))
                select_element = table_schema.getColumn("facility_info", "unions_unionid")+", (CASE WHEN \"Union Name\" is not null then \"Union Name\" else '' end) as \"Union Name\", ";
            else if(sql_view_type.equals("4"))
                select_element = table_schema.getColumn("facility_info", "unions_unionid")+", "+table_schema.getColumn("facility_info", "facility_registry_facility_name")+" as \"Facility Name\", \"Upazila Name\", (CASE WHEN \"Union Name\" is not null then \"Union Name\" else '' end) as \"Union Name\", "+table_schema.getColumn("facility_info", "facility_registry_facilityid")+", ";
            else if(form_type.equals("1")) {
                select_element = table_schema.getColumn("table", "providerdb_provname") + " \"Provider Name\", " + table_schema.getColumn("table", "providerdb_provcode") + " as \"Provider ID\", " +
                        table_schema.getColumn("table", "providertype_typename") + " as \"Provider Type\", " + table_schema.getColumn("table", "providerdb_mobileno") + " as \"Mobile No.\", " +table_schema.getColumn("","facility_registry_facility_name")+ " as \"Facility Name\", " +
                        "(CASE WHEN \"Union Name\" is not null then \"Union Name\" else '' end) as \"Union Name\", "+
                        "(CASE WHEN ct_prov.count_provider>1 THEN (" + table_schema.getColumn("table", "providerdb_provname") + " || '(' || \"Union Name\" || ')') ELSE " + table_schema.getColumn("table", "providerdb_provname") + " END) AS \"ProvName\", ";
            }
            else if(form_type.equals("11")){
                select_element = table_schema.getColumn("table", "providerdb_provname") + " \"Provider Name\", " + table_schema.getColumn("table", "providerdb_provcode") + " as \"Provider ID\", " +
                        table_schema.getColumn("table", "providertype_typename") + " as \"Provider Type\", " + table_schema.getColumn("table", "providerdb_mobileno") + " as \"Mobile No.\", " +
                        "(CASE WHEN \"Union Name\" is not null then \"Union Name\" else '' end) as \"Union Name\", "+
                        "(CASE WHEN ct_prov.count_provider>1 THEN (" + table_schema.getColumn("table", "providerdb_provname") + " || '(' || \"Union Name\" || ')') ELSE " + table_schema.getColumn("table", "providerdb_provname") + " END) AS \"ProvName\", ";
            }
            else
                select_element = "";

            String sum = "";
            if(form_type.equals("1") || form_type.equals("11"))
                sum = "";
            else
                sum = "SUM";

            if(sql_method_type.equals("1") || form_type.equals("11"))
            {
                create_sql = "select " + select_element +
                        sum+"(CASE WHEN anc.\"ANC1\" IS NULL THEN 0 ELSE anc.\"ANC1\" END) AS \"ANC1\",  " +
                        sum+"(CASE WHEN anc.\"ANC2\" IS NULL THEN 0 ELSE anc.\"ANC2\" END) AS \"ANC2\",  " +
                        sum+"(CASE WHEN anc.\"ANC3\" IS NULL THEN 0 ELSE anc.\"ANC3\" END) AS \"ANC3\",  " +
                        sum+"(CASE WHEN anc.\"ANC4\" IS NULL THEN 0 ELSE anc.\"ANC4\" END) AS \"ANC4\",  " +
                        sum+"(CASE WHEN delivery.\"Delivery\" IS NULL THEN 0 ELSE delivery.\"Delivery\" END) as \"Delivery\",  " +
//						sum+"(CASE WHEN delivery.livebirth IS NULL THEN 0 ELSE delivery.livebirth END) as \"Live Birth\",  " +
//						sum+"(CASE WHEN delivery.stillbirth IS NULL THEN 0 ELSE delivery.stillbirth END) as \"Still Birth\",  " +
//						sum+"(CASE WHEN delivery.amtsl IS NULL THEN 0 ELSE delivery.amtsl END) as \"AMTSL\",  " +
                        sum+"(CASE WHEN pnc.\"PNC1\" IS NULL THEN 0 ELSE pnc.\"PNC1\" END) AS \"PNC1-M\",  " +
                        sum+"(CASE WHEN pnc.\"PNC2\" IS NULL THEN 0 ELSE pnc.\"PNC2\" END) AS \"PNC2-M\",  " +
                        sum+"(CASE WHEN pnc.\"PNC3\" IS NULL THEN 0 ELSE pnc.\"PNC3\" END) AS \"PNC3-M\",  " +
                        sum+"(CASE WHEN pnc.\"PNC4\" IS NULL THEN 0 ELSE pnc.\"PNC4\" END) AS \"PNC4-M\",  " +
                        sum+"(CASE WHEN pncc.\"PNC1-N\" IS NULL THEN 0 ELSE pncc.\"PNC1-N\" END) AS \"PNC1-N\",  " +
                        sum+"(CASE WHEN pncc.\"PNC2-N\" IS NULL THEN 0 ELSE pncc.\"PNC2-N\" END) AS \"PNC2-N\",  " +
                        sum+"(CASE WHEN pncc.\"PNC3-N\" IS NULL THEN 0 ELSE pncc.\"PNC3-N\" END) AS \"PNC3-N\",  " +
                        sum+"(CASE WHEN pncc.\"PNC4-N\" IS NULL THEN 0 ELSE pncc.\"PNC4-N\" END) AS \"PNC4-N\"  " +
                        //sum+"((case when \"ANC\" is NULL then 0 else \"ANC\" END) + (case when \"PNC\" is NULL then 0 else \"PNC\" END) + (case when \"PNC-N\" is NULL then 0 else \"PNC-N\" END) + (case when \"Delivery\" is NULL then 0 else \"Delivery\" END)) as \"TOTAL\"   " +
                        "from " + table_schema.getTable("table_providerdb") + " " +
                        (!form_type.equals("11")?dblink_providerdb:"") +
                        "left join  " +
                        "(select "+ table_schema.getColumn("table","ancservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"ANC\",  " +
                        "count(case when " + table_schema.getColumn("", "ancservice_serviceid",new String[]{"1"},"=") + " then 1 else null end) as \"ANC1\",  " +
                        "count(case when "+table_schema.getColumn("", "ancservice_serviceid",new String[]{"2"},"=")+" then 1 else null end) as \"ANC2\",  " +
                        "count(case when "+table_schema.getColumn("", "ancservice_serviceid",new String[]{"3"},"=")+" then 1 else null end) as \"ANC3\",  " +
                        "count(case when "+table_schema.getColumn("", "ancservice_serviceid",new String[]{"4"},"=")+" then 1 else null end) as \"ANC4\"  " +
                        "from  " + table_schema.getTable("table_ancservice") + " " +
                        (!form_type.equals("11")?dblink_service + table_schema.getColumn("table","ancservice_providerid"):"")+" " +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","ancservice_providerid")+ " " +
                        (!form_type.equals("11")?"or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid ":" ") +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","ancservice_providerid")+" " +
                        (!form_type.equals("11")?"or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid ": " ") +
                        "where ";

                if(form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "ancservice_client",new String[]{"2","22"},"in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","ancservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","ancservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " + "else " +
                        "case when "+table_schema.getColumn("", "providerdb_endate", new String[]{start_date},">")+" then "+
                        table_schema.getColumn("","ancservice_visitdate")+" between" + table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"'"+
                        "else "+ table_schema.getColumn("", "ancservice_visitdate",new String[]{start_date,end_date},"between")  +" end"+ " end) " +
                        "AND ("+table_schema.getColumn("", "ancservice_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("", "ancservice_servicesource",new String[]{"0"},"=")+") AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","ancservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as anc on "+(!form_type.equals("11")?"(anc."+table_schema.getColumn("","ancservice_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("anc","ancservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+")":"anc."+table_schema.getColumn("","ancservice_providerid")+"="+table_schema.getColumn("table","providerdb_provcode"))+" and anc."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND anc."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND anc."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "left join   " +
                        "(select "+ table_schema.getColumn("table","pncservicemother_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"PNC\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid",new String[]{"1"},"=") + " then 1 else null end) as \"PNC1\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid",new String[]{"2"},"=") + " then 1 else null end) as \"PNC2\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid",new String[]{"3"},"=") + " then 1 else null end) as \"PNC3\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicemother_serviceid",new String[]{"4"},"=") + " then 1 else null end) as \"PNC4\"  " +
                        "from  " + table_schema.getTable("table_pncservicemother") + " " +
                        (!form_type.equals("11")?dblink_service + table_schema.getColumn("table","pncservicemother_providerid")+" ": " ") +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","pncservicemother_providerid")+ " " +
                        (!form_type.equals("11")?"or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid " : " ") +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","pncservicemother_providerid")+" " +
                        (!form_type.equals("11")?"or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid ":" ") +
                        "where ";

                if(form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "pncservicemother_client",new String[]{"2","22"},"in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pncservicemother_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pncservicemother_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " +
                        "else "+ table_schema.getColumn("", "pncservicemother_visitdate",new String[]{start_date,end_date},"between") +" end) " +
                        "AND ("+table_schema.getColumn("", "pncservicemother_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("", "pncservicemother_servicesource",new String[]{"0"},"=")+") AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","pncservicemother_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as pnc on "+(!form_type.equals("11")?"(pnc."+table_schema.getColumn("","pncservicemother_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("pnc","pncservicemother_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+")":"pnc."+table_schema.getColumn("","pncservicemother_providerid")+"="+table_schema.getColumn("table","providerdb_provcode"))+" and pnc."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND pnc."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND pnc."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "left join   " +
                        "(select "+ table_schema.getColumn("table","pncservicechild_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"PNC-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid",new String[]{"1"},"=") + " then 1 else null end) as \"PNC1-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid",new String[]{"2"},"=") + " then 1 else null end) as \"PNC2-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid",new String[]{"3"},"=") + " then 1 else null end) as \"PNC3-N\",  " +
                        "count(case when " + table_schema.getColumn("", "pncservicechild_serviceid",new String[]{"4"},"=") + " then 1 else null end) as \"PNC4-N\"  " +
                        "from  " + table_schema.getTable("table_pncservicechild") + " " +
                        (!form_type.equals("11")?dblink_service + table_schema.getColumn("table","pncservicechild_providerid")+" ": " ") +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","pncservicechild_providerid")+ " " +
                        (!form_type.equals("11")?"or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid " : " ") +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","pncservicechild_providerid")+" " +
                        (!form_type.equals("11")?"or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid ": " ") +
                        "where ";

                if(form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "pncservicechild_client",new String[]{"2","22"},"in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pncservicechild_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pncservicechild_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " +
                        "else "+ table_schema.getColumn("", "pncservicechild_visitdate",new String[]{start_date,end_date},"between") +" end) " +
                        "AND ("+table_schema.getColumn("", "pncservicechild_servicesource",new String[]{},"isnull")+" OR "+table_schema.getColumn("", "pncservicechild_servicesource",new String[]{"0"},"=")+") AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","pncservicechild_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as pncc on "+(!form_type.equals("11")?"(pncc."+table_schema.getColumn("","pncservicechild_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("pncc","pncservicechild_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+")":"pncc."+ table_schema.getColumn("","pncservicechild_providerid")+"="+table_schema.getColumn("table","providerdb_provcode"))+" and pncc."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND pncc."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND pncc."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "left join   " +
                        "(select "+ table_schema.getColumn("table","delivery_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"Delivery\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "delivery_applyoxytocin",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "delivery_applytraction",new String[]{"1"},"=")+" AND "+table_schema.getColumn("table", "delivery_uterusmassage",new String[]{"1"},"=")+") THEN 1 ELSE NULL END) AS amtsl, " +
                        "SUM("+table_schema.getColumn("table","delivery_livebirth")+") as livebirth, "+
                        "SUM("+table_schema.getColumn("table","delivery_stillbirth")+") as stillbirth "+
                        "from  " + table_schema.getTable("table_delivery") + " " +
                        (!form_type.equals("11")?dblink_service + table_schema.getColumn("table","delivery_providerid")+" ": " ") +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","delivery_providerid")+ " " +
                        (!form_type.equals("11")?"or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid " : " ") +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","delivery_providerid")+" " +
                        (!form_type.equals("11")?"or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid ": " ") +
                        "where ";

                if(form_type.equals("11"))
                    create_sql += table_schema.getColumn("table", "delivery_client",new String[]{"2","22"},"in") + " and ";

                create_sql += "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","delivery_outcomedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","delivery_outcomedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " +
                        "else "+ table_schema.getColumn("", "delivery_outcomedate",new String[]{start_date,end_date},"between") +" end) ";

                if(!form_type.equals("11"))
                    create_sql += "AND "+ table_schema.getColumn("", "delivery_deliverydonebythisprovider",new String[]{"1"},"=") +" ";

                create_sql += "AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","delivery_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as delivery on "+(!form_type.equals("11")?"(delivery."+table_schema.getColumn("","delivery_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("delivery","delivery_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+")" : "delivery."+ table_schema.getColumn("","delivery_providerid")+"="+table_schema.getColumn("table","providerdb_provcode"))+" and delivery."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND delivery."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND delivery."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "join  "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providerdb_provtype")+"="+table_schema.getColumn("table","providertype_provtype")+"   ";

                if(form_type.equals("1") || form_type.equals("11")) {
                    create_sql += "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                            "and " + sql_condition + " " +
                            "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","providerdb_provcode")+" ";
                }

                if(form_type.equals("11"))
                    create_sql += "join  (select "+table_schema.getColumn("","unions_zillaid")+","+table_schema.getColumn("","unions_upazilaid")+","+table_schema.getColumn("","unions_unionid")+","+table_schema.getColumn("","unions_unionnameeng")+" as \"Union Name\" from "+ table_schema.getTable("table_unions") +" where "+table_schema.getColumn("","unions_zillaid")+"="+zilla+" and "+table_schema.getColumn("","unions_upazilaid")+"="+upazila +") as unions on unions."+table_schema.getColumn("","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND unions."+table_schema.getColumn("","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND unions."+table_schema.getColumn("","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"   ";

                create_sql += "where  " + sql_condition + " " +
                        "AND ("+ table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") ";

                if(sql_view_type.equals("3"))
                    create_sql += "GROUP BY \"Union Name\", "+table_schema.getColumn("facility_info", "unions_unionid")+" ";
                else if(sql_view_type.equals("4"))
                    create_sql += "GROUP BY "+table_schema.getColumn("facility_info", "unions_unionid")+", "+table_schema.getColumn("facility_info", "facility_registry_facilityid")+", \"Facility Name\", \"Upazila Name\", \"Union Name\"";
            }
            else if(sql_method_type.equals("2"))
            {

                create_sql = "select " + select_element +
                        sum+"(CASE WHEN pillcondom.\"Pill\" IS NULL THEN 0 ELSE pillcondom.\"Pill\" END) AS \"Pill\", " +
                        sum+"(CASE WHEN pillcondom.\"Condom\" IS NULL THEN 0 ELSE pillcondom.\"Condom\" END) AS \"Condom\", " +
                        sum+"(CASE WHEN iud.\"IUD\" IS NULL THEN 0 ELSE iud.\"IUD\" END) AS \"IUD\",  " +
                        sum+"(CASE WHEN iudfollowup.\"IUD Followup\" IS NULL THEN 0 ELSE iudfollowup.\"IUD Followup\" END) AS \"IUD Followup\",  " +
                        sum+"(CASE WHEN implant.\"IMPLANT\" IS NULL THEN 0 ELSE implant.\"IMPLANT\" END) AS \"IMPLANT\",  " +
                        sum+"(CASE WHEN implantfollowup.\"IMPLANT Followup\" IS NULL THEN 0 ELSE implantfollowup.\"IMPLANT Followup\" END) AS \"IMPLANT Followup\",  " +
                        sum+"(CASE WHEN injectable.\"Injectable\" IS NULL THEN 0 ELSE injectable.\"Injectable\" END) AS \"Injectable\"  " +
                        //sum+"((case when \"Pill\" is NULL then 0 else \"Pill\" END) + (case when \"Condom\" is NULL then 0 else \"Condom\" END) + (case when \"IUD\" is NULL then 0 else \"IUD\" END) + (case when \"IUD Followup\" is NULL then 0 else \"IUD Followup\" END) + (case when \"IMPLANT\" is NULL then 0 else \"IMPLANT\" END) + (case when \"IMPLANT Followup\" is NULL then 0 else \"IMPLANT Followup\" END) + (case when \"Injectable\" is NULL then 0 else \"Injectable\" END)) as \"TOTAL\" " +
                        "from " + table_schema.getTable("table_providerdb") + "   " +
                        dblink_providerdb +
                        "left join  " +
                        "(select "+ table_schema.getColumn("table","pillcondomservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+"," +
                        "COUNT(CASE WHEN ("+ table_schema.getColumn("", "pillcondomservice_methodtype",new String[]{"1"},"=") +" OR "+ table_schema.getColumn("", "pillcondomservice_methodtype",new String[]{"10"},"=") +") THEN 1 ELSE NULL END) as \"Pill\", " +
                        "COUNT(CASE WHEN "+ table_schema.getColumn("", "pillcondomservice_methodtype",new String[]{"2"},"=") +" THEN 1 ELSE NULL END) as \"Condom\" " +
                        "from " + table_schema.getTable("table_pillcondomservice") + " " +
                        dblink_service + table_schema.getColumn("table","pillcondomservice_providerid")+" " +
                        "join " + table_schema.getTable("table_providerdb") + " on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","pillcondomservice_providerid")+"" +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","pillcondomservice_providerid")+" " +
                        "where (case when count_provider>1 then " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","pillcondomservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","pillcondomservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) " +
                        "else "+ table_schema.getColumn("", "pillcondomservice_visitdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+ table_schema.getColumn("table","pillcondomservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as pillcondom on (pillcondom."+ table_schema.getColumn("","pillcondomservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+ table_schema.getColumn("pillcondom","pillcondomservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and pillcondom."+ table_schema.getColumn("","providerdb_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND pillcondom."+ table_schema.getColumn("","providerdb_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND pillcondom."+ table_schema.getColumn("","providerdb_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+" " +
                        "left join   " +
                        "(select "+ table_schema.getColumn("table","iudservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"IUD\" " +
                        "from " + table_schema.getTable("table_iudservice") + " " +
                        dblink_service + table_schema.getColumn("table","iudservice_providerid")+" " +
                        "join " + table_schema.getTable("table_providerdb") + " on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","iudservice_providerid")+" " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","iudservice_providerid")+" " +
                        "where (case when count_provider>1 then " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","iudservice_iudimplantdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","iudservice_iudimplantdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) " +
                        "else "+ table_schema.getColumn("", "iudservice_iudimplantdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+ table_schema.getColumn("table","iudservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as iud on (iud."+ table_schema.getColumn("","iudservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+ table_schema.getColumn("iud","iudservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and iud."+ table_schema.getColumn("","providerdb_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND iud."+ table_schema.getColumn("","providerdb_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND iud."+ table_schema.getColumn("","providerdb_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+" " +
                        "left join " +
                        "(select "+ table_schema.getColumn("table","iudfollowupservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"IUD Followup\" " +
                        "from " + table_schema.getTable("table_iudfollowupservice") + " " +
                        dblink_service + table_schema.getColumn("table","iudfollowupservice_providerid")+" " +
                        "join " + table_schema.getTable("table_providerdb") + " on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","iudfollowupservice_providerid")+" " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","iudfollowupservice_providerid")+" " +
                        "where (case when count_provider>1 then " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","iudfollowupservice_followupdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","iudfollowupservice_followupdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) " +
                        "else "+ table_schema.getColumn("", "iudfollowupservice_followupdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+ table_schema.getColumn("table","iudfollowupservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as iudfollowup on (iudfollowup."+ table_schema.getColumn("","iudfollowupservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+ table_schema.getColumn("iudfollowup","iudfollowupservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and iudfollowup."+ table_schema.getColumn("","providerdb_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND iudfollowup."+ table_schema.getColumn("","providerdb_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND iudfollowup."+ table_schema.getColumn("","providerdb_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+" " +
                        "left join   " +
                        "(select "+ table_schema.getColumn("table","implantservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"IMPLANT\" " +
                        "from " + table_schema.getTable("table_implantservice") + " " +
                        dblink_service + table_schema.getColumn("table","implantservice_providerid")+" " +
                        "join " + table_schema.getTable("table_providerdb") + " on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","implantservice_providerid")+" " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","implantservice_providerid")+" " +
                        "where (case when count_provider>1 then " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","implantservice_implantimplantdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","implantservice_implantimplantdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) " +
                        "else "+ table_schema.getColumn("", "implantservice_implantimplantdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+ table_schema.getColumn("table","implantservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as implant on (implant."+ table_schema.getColumn("","implantservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+ table_schema.getColumn("implant","implantservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and implant."+ table_schema.getColumn("","providerdb_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND implant."+ table_schema.getColumn("","providerdb_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND implant."+ table_schema.getColumn("","providerdb_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+" " +
                        "left join " +
                        "(select "+ table_schema.getColumn("table","implantfollowupservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"IMPLANT Followup\" " +
                        "from " + table_schema.getTable("table_implantfollowupservice") + " " +
                        dblink_service + table_schema.getColumn("table","implantfollowupservice_providerid")+" " +
                        "join " + table_schema.getTable("table_providerdb") + " on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","implantfollowupservice_providerid")+" " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","implantfollowupservice_providerid")+" " +
                        "where (case when count_provider>1 then " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","implantfollowupservice_followupdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","implantfollowupservice_followupdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) " +
                        "else "+ table_schema.getColumn("", "implantfollowupservice_followupdate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+ table_schema.getColumn("table","implantfollowupservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as implantfollowup on (implantfollowup."+ table_schema.getColumn("","implantfollowupservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+ table_schema.getColumn("implantfollowup","implantfollowupservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and implantfollowup."+ table_schema.getColumn("","providerdb_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND implantfollowup."+ table_schema.getColumn("","providerdb_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND implantfollowup."+ table_schema.getColumn("","providerdb_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+" " +
                        "left join   " +
                        "(select "+ table_schema.getColumn("table","womaninjectable_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",count (*) as \"Injectable\" " +
                        "from " + table_schema.getTable("table_womaninjectable") + " " +
                        dblink_service + table_schema.getColumn("table","womaninjectable_providerid")+" " +
                        "join " + table_schema.getTable("table_providerdb") + " on "+table_schema.getColumn("table","providerdb_provcode")+"="+table_schema.getColumn("table","womaninjectable_providerid")+" " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","womaninjectable_providerid")+" " +
                        "where (case when count_provider>1 then " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","womaninjectable_dosedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","womaninjectable_dosedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end) " +
                        "else "+ table_schema.getColumn("", "womaninjectable_dosedate",new String[]{start_date,end_date},"between") +" end) AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+ table_schema.getColumn("table","womaninjectable_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as injectable on (injectable."+ table_schema.getColumn("","womaninjectable_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+ table_schema.getColumn("injectable","womaninjectable_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and injectable."+ table_schema.getColumn("","providerdb_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND injectable."+ table_schema.getColumn("","providerdb_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND injectable."+ table_schema.getColumn("","providerdb_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+" ";

                if(form_type.equals("1")) {
                    create_sql += "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                            "and " + sql_condition + " " +
                            "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","providerdb_provcode")+" ";
                }
                if(form_type.equals("1"))
                    create_sql += "join  "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providerdb_provtype")+"="+table_schema.getColumn("table","providertype_provtype")+"   ";

//                if(sql_view_type.equals("3") || form_type.equals("1"))
//                    create_sql += "join  (select "+table_schema.getColumn("","unions_zillaid")+","+table_schema.getColumn("","unions_upazilaid")+","+table_schema.getColumn("","unions_unionid")+","+table_schema.getColumn("","unions_unionnameeng")+" as \"UnionName\" from "+ table_schema.getTable("table_unions") +" where "+table_schema.getColumn("","unions_zillaid")+"="+zilla+" and "+table_schema.getColumn("","unions_upazilaid")+"="+upazila +") as unions on unions."+table_schema.getColumn("","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND unions."+table_schema.getColumn("","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND unions."+table_schema.getColumn("","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"   ";

                create_sql +=	"where " + sql_condition + " AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") ";

                if(sql_view_type.equals("3"))
                    create_sql += "GROUP BY \"Union Name\", "+table_schema.getColumn("facility_info", "unions_unionid")+" ";
                else if(sql_view_type.equals("4"))
                    create_sql += "GROUP BY "+table_schema.getColumn("facility_info", "unions_unionid")+", "+table_schema.getColumn("facility_info", "facility_registry_facilityid")+", \"Facility Name\", \"Upazila Name\", \"Union Name\"";

                //System.out.println(create_sql);

            }
            else if(sql_method_type.equals("3"))
            {
                create_sql = "select " + select_element
                        + "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "clientmap_gender",new String[]{"1"},"=") +") THEN 1 ELSE NULL END) AS \"Male\", "
                        + "COUNT(CASE WHEN ("+ table_schema.getColumn("table", "clientmap_gender",new String[]{"2"},"=") +") THEN 1 ELSE NULL END) AS \"Female\" "
                        //+ "(COUNT(CASE WHEN ("+ table_schema.getColumn("table", "clientmap_gender",new String[]{"1"},"=") +") THEN 1 ELSE NULL END)+COUNT(CASE WHEN ("+ table_schema.getColumn("table", "clientmap_gender",new String[]{"2"},"=") +") THEN 1 ELSE NULL END)) AS \"TOTAL\" "
                        + "from " + table_schema.getTable("table_providerdb") + " "
                        + dblink_providerdb
                        + "left join  "
                        + "" + table_schema.getTable("table_gpservice") + " on ("+table_schema.getColumn("table","gpservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("table","gpservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and "+ table_schema.getColumn("", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" "
                        + "left join "
                        + "" + table_schema.getTable("table_clientmap") + " on "+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","gpservice_healthid")+" "
                        + "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
                        + "and " + sql_condition + " "
                        + "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","providerdb_provcode")+" ";

                if(form_type.equals("1"))
                    create_sql += "join  "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providerdb_provtype")+"="+table_schema.getColumn("table","providertype_provtype")+"   ";

//                if(sql_view_type.equals("3") || form_type.equals("1"))
//                    create_sql += "join  (select "+table_schema.getColumn("","unions_zillaid")+","+table_schema.getColumn("","unions_upazilaid")+","+table_schema.getColumn("","unions_unionid")+","+table_schema.getColumn("","unions_unionnameeng")+" as \"UnionName\" from "+ table_schema.getTable("table_unions") +" where "+table_schema.getColumn("","unions_zillaid")+"="+zilla+" and "+table_schema.getColumn("","unions_upazilaid")+"="+upazila +") as unions on unions."+table_schema.getColumn("","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND unions."+table_schema.getColumn("","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND unions."+table_schema.getColumn("","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"   ";

                create_sql += "where (case when "+table_schema.getColumn("","gpservice_visitdate")+" is not null then (case when count_provider>1 then "
                        + "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","gpservice_visitdate")+" between '"+ start_date +" ' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","gpservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +" ' end) "
                        + "else "+ table_schema.getColumn("", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" end) else "+table_schema.getColumn("","gpservice_visitdate")+" is null end) "
                        + " AND " + sql_condition + " AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") ";
                if(sql_view_type.equals("3"))
                    create_sql += "GROUP BY \"Union Name\", "+table_schema.getColumn("facility_info", "unions_unionid")+" ";
                else if(sql_view_type.equals("4"))
                    create_sql += "GROUP BY "+table_schema.getColumn("facility_info", "unions_unionid")+", "+table_schema.getColumn("facility_info", "facility_registry_facilityid")+", \"Facility Name\", \"Upazila Name\", \"Union Name\"";
                else if (form_type.equals("1"))
                    create_sql += "GROUP BY "+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","providertype_typename")+", "+table_schema.getColumn("table","providerdb_mobileno")+", "+table_schema.getColumn("table","providerdb_facilityname")+", ct_prov.count_provider, \"Union Name\", "+table_schema.getColumn("","facility_registry_facility_name")+" ";

                //create_sql += "order by \"TOTAL\" desc";
            }
            else if(sql_method_type.equals("4")){
                create_sql= "select " + select_element + " " +
                        sum + "(CASE WHEN death.\"Number of pre-discharge maternal deaths\" IS NULL THEN 0 ELSE death.\"Number of pre-discharge maternal deaths\" END) AS \"Maternal Deaths\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge Maternal deaths due to Haemorrhage\" IS NULL THEN 0 ELSE death.\"Pre-discharge Maternal deaths due to Haemorrhage\" END) AS \"Maternal Deaths-Haemorrhage\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge Maternal deaths due to Preeclampsia/Eclampsia\" IS NULL THEN 0 ELSE death.\"Pre-discharge Maternal deaths due to Preeclampsia/Eclampsia\" END) AS \"Maternal Deaths-Preeclampsia/Eclampsia\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge Maternal deaths due to Sepsis\" IS NULL THEN 0 ELSE death.\"Pre-discharge Maternal deaths due to Sepsis\" END) AS \"Maternal Deaths-Sepsis\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge Maternal deaths due to Other causes\" IS NULL THEN 0 ELSE death.\"Pre-discharge Maternal deaths due to Other causes\" END) AS \"Maternal Deaths-Other\"," +
                        sum + "(CASE WHEN death.\"Number of pre-discharge neonatal deaths\" IS NULL THEN 0 ELSE death.\"Number of pre-discharge neonatal deaths\" END) AS \"Neonatal Deaths\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge neonatal deaths due to Prematurity\" IS NULL THEN 0 ELSE death.\"Pre-discharge neonatal deaths due to Prematurity\" END) AS \"Neonatal Deaths-Prematurity\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge neonatal deaths due to Birth Asphyxia\" IS NULL THEN 0 ELSE death.\"Pre-discharge neonatal deaths due to Birth Asphyxia\" END) AS \"Neonatal Deaths-Birth Asphyxia\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge neonatal deaths due to Sepsis\" IS NULL THEN 0 ELSE death.\"Pre-discharge neonatal deaths due to Sepsis\" END) AS \"Neonatal Deaths-Sepsis\"," +
                        sum + "(CASE WHEN death.\"Pre-discharge neonatal deaths due to Other causes\" IS NULL THEN 0 ELSE death.\"Pre-discharge neonatal deaths due to Other causes\" END) AS \"Neonatal Deaths-Other\"," +
                        sum + "(CASE WHEN newborn.\"Total Newborn\" IS NULL THEN 0 ELSE newborn.\"Total Newborn\" END) AS \"Total Newborn\"," +
                        sum + "(CASE WHEN newborn.\"Institutional Stillbirth\" IS NULL THEN 0 ELSE newborn.\"Institutional Stillbirth\" END) AS \"Stillbirth\"," +
                        sum + "(CASE WHEN newborn.\"Institutional Stillbirth - Fresh\" IS NULL THEN 0 ELSE newborn.\"Institutional Stillbirth - Fresh\" END) AS \"Stillbirth-Fresh\"," +
                        sum + "(CASE WHEN newborn.\"Institutional Stillbirth - Macerated\" IS NULL THEN 0 ELSE newborn.\"Institutional Stillbirth - Macerated\" END) AS \"Stillbirth-Macerated\"," +
                        sum + "(CASE WHEN newborn.\"Total Newborns Breastfed\" IS NULL THEN 0 ELSE newborn.\"Total Newborns Breastfed\" END) AS \"Breastfed\"," +
                        sum + "(CASE WHEN newborn.\"Total Newborns Oxytocin\" IS NULL THEN 0 ELSE newborn.\"Total Newborns Oxytocin\" END) AS \"Oxytocin\"," +
                        sum + "(CASE WHEN newborn.\"Total Newborns with Birthweight Documented\" IS NULL THEN 0 ELSE newborn.\"Total Newborns with Birthweight Documented\" END) AS \"Birthweight\"," +
                        sum + "(CASE WHEN newborn.\"Total Newborns Weighing ≤ 2,000g\" IS NULL THEN 0 ELSE newborn.\"Total Newborns Weighing ≤ 2,000g\" END) AS \"Weighing≤2,000g\", " +
                        sum + "(CASE WHEN newborn.\"Total Delivery\" IS NULL THEN 0 ELSE newborn.\"Total Delivery\" END) AS \"Total Delivery\" " +
                        "from " + table_schema.getTable("table_providerdb") + " " +
                        dblink_providerdb +
                        "left join  " +
                        "(select "+ table_schema.getColumn("table","death_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",  " +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"15", "27"},"between") + " then 1 else null end) as \"Number of pre-discharge maternal deaths\",  " +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"15"},"=") + " then 1 else null end) as \"Pre-discharge Maternal deaths due to Haemorrhage\",  " +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"19"},"=") + " then 1 else null end) as \"Pre-discharge Maternal deaths due to Preeclampsia/Eclampsia\",  " +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"21"},"=") + " then 1 else null end) as \"Pre-discharge Maternal deaths due to Sepsis\"," +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"15","19","21"},"notin") + " and " + table_schema.getColumn("", "death_causeofdeath",new String[]{"15","27"},"between") + " then 1 else null end) as \"Pre-discharge Maternal deaths due to Other causes\"," +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"28", "47"},"between") + " then 1 else null end) as \"Number of pre-discharge neonatal deaths\",  " +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"30"},"=") + " then 1 else null end) as \"Pre-discharge neonatal deaths due to Prematurity\"," +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"29"},"=") + " then 1 else null end) as \"Pre-discharge neonatal deaths due to Birth Asphyxia\"," +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"36"},"=") + " then 1 else null end) as \"Pre-discharge neonatal deaths due to Sepsis\"," +
                        "count(case when " + table_schema.getColumn("", "death_causeofdeath",new String[]{"30","29","36"},"notin") + " and " + table_schema.getColumn("", "death_causeofdeath",new String[]{"28","47"},"between") + " then 1 else null end) as \"Pre-discharge neonatal deaths due to Other causes\"  " +
                        "from  " + table_schema.getTable("table_death") + " " +
                        "join " + table_schema.getTable("table_delivery") +" on "+table_schema.getColumn("table","death_healthid")+"=" +table_schema.getColumn("table","delivery_healthid")+ " " +
                        dblink_service + table_schema.getColumn("table","death_providerid")+" " +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","death_providerid")+ " " +
                        "or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","death_providerid")+" " +
                        "or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid " +
                        "where "+
                        "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","death_deathdt")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","death_deathdt")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " +
                        "else "+ table_schema.getColumn("", "death_deathdt",new String[]{start_date,end_date},"between") +" end) "+
                        "AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","death_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as death on (death."+table_schema.getColumn("","death_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("death","death_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and death."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND death."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND death."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "left join  " +
                        "(select "+ table_schema.getColumn("table","newborn_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",  " +
                        "count(*) as \"Total Newborn\", " +
                        "count(case when ("+table_schema.getColumn("","newborn_birthstatus",new String[]{"2"},"=")+" or "+table_schema.getColumn("","newborn_birthstatus",new String[]{"3"},"=")+") then 1 else null end) as \"Institutional Stillbirth\", "+
                        "count(case when "+table_schema.getColumn("","newborn_birthstatus",new String[]{"2"},"=")+" then 1 else null end) as \"Institutional Stillbirth - Fresh\", "+
                        "count(case when "+table_schema.getColumn("","newborn_birthstatus",new String[]{"3"},"=")+" then 1 else null end) as \"Institutional Stillbirth - Macerated\", "+
                        "count(case when "+table_schema.getColumn("","newborn_breastfeed",new String[]{"1"},"=")+" then 1 else null end) as \"Total Newborns Breastfed\", "+
                        "count(distinct(case when "+table_schema.getColumn("","delivery_applyoxytocin",new String[]{"1"},"=")+" then "+table_schema.getColumn("table","newborn_healthid")+" else null end)) as \"Total Newborns Oxytocin\", "+
                        "count(case when "+table_schema.getColumn("","newborn_birthweight",new String[]{},"isnotnull")+" then 1 else null end) as \"Total Newborns with Birthweight Documented\", "+
                        "count(case when "+table_schema.getColumn("","newborn_birthweight",new String[]{"2"},"<=")+" then 1 else null end) as \"Total Newborns Weighing ≤ 2,000g\", " +
                        "count(distinct("+table_schema.getColumn("table","delivery_healthid")+","+table_schema.getColumn("","delivery_pregno")+")) as \"Total Delivery\" "+
                        "from  " + table_schema.getTable("table_newborn") + " " +
                        "join " + table_schema.getTable("table_delivery") + " using ("+table_schema.getColumn("","newborn_healthid")+", "+table_schema.getColumn("","newborn_pregno")+") " +
                        dblink_service + table_schema.getColumn("table","newborn_providerid")+" " +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","newborn_providerid")+ " " +
                        "or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","newborn_providerid")+" " +
                        "or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid " +
                        "where "+
                        "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","newborn_outcomedate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","newborn_outcomedate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " +
                        "else "+ table_schema.getColumn("table", "newborn_outcomedate",new String[]{start_date,end_date},"between") +" end) "+
                        "AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","newborn_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as newborn on (newborn."+table_schema.getColumn("","newborn_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("newborn","newborn_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and newborn."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND newborn."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND newborn."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "join  "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providerdb_provtype")+"="+table_schema.getColumn("table","providertype_provtype")+"   "+
                        "where  " + sql_condition + " " +
                        "AND ("+ table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") ";

                if(sql_view_type.equals("3"))
                    create_sql += "GROUP BY \"Union Name\", "+table_schema.getColumn("facility_info", "unions_unionid")+" ";
                else if(sql_view_type.equals("4"))
                    create_sql += "GROUP BY "+table_schema.getColumn("facility_info", "unions_unionid")+", "+table_schema.getColumn("facility_info", "facility_registry_facilityid")+", \"Facility Name\", \"Upazila Name\", \"Union Name\"";


            }
            else if(sql_method_type.equals("5"))
            {
                create_sql= "select " + select_element + " " +
                        sum + "(CASE WHEN gpservice.\"Fever_60+\" IS NULL THEN 0 ELSE gpservice.\"Fever_60+\" END) AS \"Fever_60+\"," +
                        sum + "(CASE WHEN gpservice.\"Fever_0-59\" IS NULL THEN 0 ELSE gpservice.\"Fever_0-59\" END) AS \"Fever_0-59\"," +
                        sum + "(CASE WHEN ancservice.\"Fever_PW\" IS NULL THEN 0 ELSE ancservice.\"Fever_PW\" END) AS \"Fever_PW\"," +
                        sum + "(CASE WHEN gpservice.\"Cold_60+\" IS NULL THEN 0 ELSE gpservice.\"Cold_60+\" END) AS \"Cold_60+\"," +
                        sum + "(CASE WHEN gpservice.\"Cold_0-59\" IS NULL THEN 0 ELSE gpservice.\"Cold_0-59\" END) AS \"Cold_0-59\"," +
                        sum + "(CASE WHEN gpservice.\"Cough_60+\" IS NULL THEN 0 ELSE gpservice.\"Cough_60+\" END) AS \"Cough_60+\"," +
                        sum + "(CASE WHEN gpservice.\"Cough_0-59\" IS NULL THEN 0 ELSE gpservice.\"Cough_0-59\" END) AS \"Cough_0-59\"," +
                        sum + "(CASE WHEN gpservice.\"Shortness_Breath_60+\" IS NULL THEN 0 ELSE gpservice.\"Shortness_Breath_60+\" END) AS \"Shortness_Breath_60+\"," +
                        sum + "(CASE WHEN gpservice.\"Shortness_Breath_0-59\" IS NULL THEN 0 ELSE gpservice.\"Shortness_Breath_0-59\" END) AS \"Shortness_Breath_0-59\"," +
                        sum + "(CASE WHEN ancservice.\"Shortness_Breath_PW\" IS NULL THEN 0 ELSE ancservice.\"Shortness_Breath_PW\" END) AS \"Shortness_Breath_PW\"," +
                        sum + "(CASE WHEN gpservice.\"Headache_60+\" IS NULL THEN 0 ELSE gpservice.\"Headache_60+\" END) AS \"Headache_60+\"," +
                        sum + "(CASE WHEN gpservice.\"Headache_0-59\" IS NULL THEN 0 ELSE gpservice.\"Headache_0-59\" END) AS \"Headache_0-59\"," +
                        sum + "(CASE WHEN gpservice.\"Sore_Throat_60+\" IS NULL THEN 0 ELSE gpservice.\"Sore_Throat_60+\" END) AS \"Sore_Throat_60+\"," +
                        sum + "(CASE WHEN gpservice.\"Sore_Throat_0-59\" IS NULL THEN 0 ELSE gpservice.\"Sore_Throat_0-59\" END) AS \"Sore_Throat_0-59\" " +
                        "from " + table_schema.getTable("table_providerdb") + " " +
                        dblink_providerdb +
                        "left join  " +
                        "(select "+ table_schema.getColumn("table","gpservice_providerid")+","+table_schema.getColumn("table", "providerdb_zillaid")+","+table_schema.getColumn("table", "providerdb_upazilaid")+","+table_schema.getColumn("table", "providerdb_unionid")+",  " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_60+\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_0-59\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_60+\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_0-59\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_60+\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_0-59\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and ("+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"9\"%' or "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"10\"%')) THEN 1 ELSE NULL END) AS \"Shortness_Breath_60+\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and ("+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"9\"%' or "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"10\"%')) THEN 1 ELSE NULL END) AS \"Shortness_Breath_0-59\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_60+\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_0-59\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_60+\", " +
                        "COUNT(CASE WHEN (date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_0-59\" " +
                        "from  " + table_schema.getTable("table_gpservice") + " " +
                        "join " + table_schema.getTable("table_clientmap") +" on "+table_schema.getColumn("table","gpservice_healthid")+"=" +table_schema.getColumn("table","clientmap_generatedid")+ " " +
                        dblink_service + table_schema.getColumn("table","gpservice_providerid")+" " +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","gpservice_providerid")+ " " +
                        "or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","gpservice_providerid")+" " +
                        "or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid " +
                        "where "+
                        "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("","gpservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("","gpservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " +
                        "else "+ table_schema.getColumn("", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" end) "+
                        "AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","gpservice_providerid")+","+table_schema.getColumn("table", "providerdb_zillaid")+","+table_schema.getColumn("table", "providerdb_upazilaid")+","+table_schema.getColumn("table", "providerdb_unionid")+") as gpservice on (gpservice."+table_schema.getColumn("","gpservice_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("gpservice","gpservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and gpservice."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND gpservice."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND gpservice."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "left join  " +
                        "(select "+ table_schema.getColumn("table","ancservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+",  " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_disease")+" like '%\"5\"%') THEN 1 ELSE NULL END) AS \"Fever_PW\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_disease")+" like '%\"2\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_PW\" " +
                        "from  " + table_schema.getTable("table_ancservice") + " " +
                        dblink_service + table_schema.getColumn("table","ancservice_providerid")+" " +
                        "join "+ table_schema.getTable("table_providerdb") +" on "+table_schema.getColumn("table","providerdb_provcode")+"=" +table_schema.getColumn("table","ancservice_providerid")+ " " +
                        "or "+table_schema.getColumn("table","providerdb_provcode")+" = associated_provider_id.providerid " +
                        "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from "+ table_schema.getTable("table_providerdb") +" where ("+ table_schema.getColumn("", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") " +
                        "and " + sql_condition + " " +
                        "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","ancservice_providerid")+" " +
                        "or ct_prov."+table_schema.getColumn("","providerdb_provcode")+"=associated_provider_id.providerid " +
                        "where "+
                        "(case when count_provider>1  then  " +
                        "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","ancservice_visitdate")+" between '"+ start_date +"' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","ancservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +"' end)  " +
                        "else "+ table_schema.getColumn("table", "ancservice_visitdate",new String[]{start_date,end_date},"between") +" end) "+
                        "AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") group by "+table_schema.getColumn("table","ancservice_providerid")+","+table_schema.getColumn("", "providerdb_zillaid")+","+table_schema.getColumn("", "providerdb_upazilaid")+","+table_schema.getColumn("", "providerdb_unionid")+") as ancservice on (ancservice."+table_schema.getColumn("","ancservice_providerid")+"="+ table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("ancservice","ancservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and ancservice."+table_schema.getColumn("", "providerdb_zillaid")+"="+table_schema.getColumn("table", "providerdb_zillaid")+" AND ancservice."+table_schema.getColumn("", "providerdb_upazilaid")+"="+table_schema.getColumn("table", "providerdb_upazilaid")+" AND ancservice."+table_schema.getColumn("", "providerdb_unionid")+"="+table_schema.getColumn("table", "providerdb_unionid")+"    " +
                        "join  "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providerdb_provtype")+"="+table_schema.getColumn("table","providertype_provtype")+"   "+
                        "where  " + sql_condition + " " +
                        "AND ("+ table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=") +" or "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnull")+") ";

                if(sql_view_type.equals("3"))
                    create_sql += "GROUP BY \"Union Name\", "+table_schema.getColumn("facility_info", "unions_unionid")+" ";
                else if(sql_view_type.equals("4"))
                    create_sql += "GROUP BY "+table_schema.getColumn("facility_info", "unions_unionid")+", "+table_schema.getColumn("facility_info", "facility_registry_facilityid")+", \"Facility Name\", \"Upazila Name\", \"Union Name\"";

                /*create_sql = "select " + select_element
//                        + "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 0 and 10 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_0-10\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 11 and 30 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_11-30\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 31 and 50 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_31-50\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 51 and 60 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_51-60\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 61 and 70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_61-70\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date)))>70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_70+\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 0 and 10 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_0-10\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 11 and 30 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_11-30\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 31 and 50 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_31-50\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 51 and 60 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_51-60\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 61 and 70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_61-70\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date)))>70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_70+\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 0 and 10 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_0-10\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 11 and 30 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_11-30\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 31 and 50 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_31-50\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 51 and 60 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_51-60\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 61 and 70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_61-70\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date)))>70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_70+\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 0 and 10 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"9\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_0-10\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 11 and 30 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"9\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_11-30\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 31 and 50 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"9\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_31-50\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 51 and 60 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"9\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_51-60\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 61 and 70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"9\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_61-70\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date)))>70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"9\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_70+\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 0 and 10 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"10\"%') THEN 1 ELSE NULL END) AS \"Asthma_0-10\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 11 and 30 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"10\"%') THEN 1 ELSE NULL END) AS \"Asthma_11-30\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 31 and 50 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"10\"%') THEN 1 ELSE NULL END) AS \"Asthma_31-50\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 51 and 60 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"10\"%') THEN 1 ELSE NULL END) AS \"Asthma_51-60\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 61 and 70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"10\"%') THEN 1 ELSE NULL END) AS \"Asthma_61-70\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date)))>70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"10\"%') THEN 1 ELSE NULL END) AS \"Asthma_70+\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 0 and 10 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_0-10\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 11 and 30 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_11-30\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 31 and 50 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_31-50\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 51 and 60 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_51-60\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 61 and 70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_61-70\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date)))>70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_70+\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 0 and 10 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_0-10\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 11 and 30 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_11-30\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 31 and 50 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_31-50\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 51 and 60 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_51-60\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date))) between 61 and 70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_61-70\", " +
//                        "COUNT(CASE WHEN (EXTRACT(YEAR FROM age(cast("+table_schema.getColumn("", "clientmap_dob")+" as date)))>70 and "+table_schema.getColumn("", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_70+\" "
                        + "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_60+\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"1\"%') THEN 1 ELSE NULL END) AS \"Fever_0-59\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_visitdate",new String[]{},"isnotnull")+" and "+table_schema.getColumn("table", "ancservice_disease")+" like '%\"5\"%') THEN 1 ELSE NULL END) AS \"Fever_PW\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_60+\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"12\"%') THEN 1 ELSE NULL END) AS \"Cold_0-59\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_60+\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"13\"%') THEN 1 ELSE NULL END) AS \"Cough_0-59\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and ("+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"9\"%' or "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"10\"%')) THEN 1 ELSE NULL END) AS \"Shortness_Breath_60+\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and ("+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"9\"%' or "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"10\"%')) THEN 1 ELSE NULL END) AS \"Shortness_Breath_0-59\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "ancservice_visitdate",new String[]{},"isnotnull")+" and "+table_schema.getColumn("table", "ancservice_disease")+" like '%\"2\"%') THEN 1 ELSE NULL END) AS \"Shortness_Breath_PW\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_60+\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"30\"%') THEN 1 ELSE NULL END) AS \"Headache_0-59\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) >=60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_60+\", " +
                        "COUNT(CASE WHEN ("+table_schema.getColumn("table", "gpservice_visitdate",new String[]{},"isnotnull")+" and date_part('year',age("+table_schema.getColumn("table","gpservice_visitdate")+", "+table_schema.getColumn("", "clientmap_dob")+")) <60 and "+table_schema.getColumn("table", "gpservice_symptom")+" like '%\"39\"%') THEN 1 ELSE NULL END) AS \"Sore_Throat_0-59\" "
                        + "from " + table_schema.getTable("table_providerdb") + " "
                        + dblink_providerdb
                        + "left join  "
                        + "" + table_schema.getTable("table_gpservice") + " on ("+table_schema.getColumn("table","gpservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("table","gpservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and "+ table_schema.getColumn("table", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" "
                        + "left join  "
                        + "" + table_schema.getTable("table_ancservice") + " on ("+table_schema.getColumn("table","ancservice_providerid")+"="+table_schema.getColumn("facility_info","facility_provider_provider_id")+" or "+table_schema.getColumn("table","ancservice_providerid")+"="+table_schema.getColumn("facility_info","associated_provider_id_associated_id")+") and "+ table_schema.getColumn("table", "ancservice_visitdate",new String[]{start_date,end_date},"between") +" "
                        + "left join "
                        + "" + table_schema.getTable("table_clientmap") + " on "+table_schema.getColumn("table","clientmap_generatedid")+"="+table_schema.getColumn("table","gpservice_healthid")+" "
                        + "join (select count(*) as count_provider, "+table_schema.getColumn("","providerdb_provcode")+" from " + table_schema.getTable("table_providerdb") + " where ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") "
                        + "and " + sql_condition + " "
                        + "group by "+table_schema.getColumn("","providerdb_provcode")+") ct_prov on ct_prov."+table_schema.getColumn("","providerdb_provcode")+"="+table_schema.getColumn("table","providerdb_provcode")+" ";

                if(form_type.equals("1"))
                    create_sql += "join  "+ table_schema.getTable("table_providertype") +" on "+table_schema.getColumn("table","providerdb_provtype")+"="+table_schema.getColumn("table","providertype_provtype")+"   ";

//                if(sql_view_type.equals("3") || form_type.equals("1"))
//                    create_sql += "join  (select "+table_schema.getColumn("","unions_zillaid")+","+table_schema.getColumn("","unions_upazilaid")+","+table_schema.getColumn("","unions_unionid")+","+table_schema.getColumn("","unions_unionnameeng")+" as \"UnionName\" from "+ table_schema.getTable("table_unions") +" where "+table_schema.getColumn("","unions_zillaid")+"="+zilla+" and "+table_schema.getColumn("","unions_upazilaid")+"="+upazila +") as unions on unions."+table_schema.getColumn("","unions_zillaid")+"="+table_schema.getColumn("table","providerdb_zillaid")+" AND unions."+table_schema.getColumn("","unions_upazilaid")+"="+table_schema.getColumn("table","providerdb_upazilaid")+" AND unions."+table_schema.getColumn("","unions_unionid")+"="+table_schema.getColumn("table","providerdb_unionid")+"   ";

                create_sql += "where ((case when "+table_schema.getColumn("table","gpservice_visitdate")+" is not null then (case when count_provider>1 then "
                        + "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","gpservice_visitdate")+" between '"+ start_date +" ' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","gpservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +" ' end) "
                        + "else "+ table_schema.getColumn("table", "gpservice_visitdate",new String[]{start_date,end_date},"between") +" end) else "+table_schema.getColumn("table","gpservice_visitdate")+" is null end) "
                        + "or (case when "+table_schema.getColumn("table","ancservice_visitdate")+" is not null then (case when count_provider>1 then "
                        + "(case when "+table_schema.getColumn("", "providerdb_exdate",new String[]{},"isnotnull")+" then "+table_schema.getColumn("table","ancservice_visitdate")+" between '"+ start_date +" ' AND "+table_schema.getColumn("","providerdb_exdate")+"::date else "+table_schema.getColumn("table","ancservice_visitdate")+" between "+table_schema.getColumn("","providerdb_endate")+"::date and '"+ end_date +" ' end) "
                        + "else "+ table_schema.getColumn("table", "ancservice_visitdate",new String[]{start_date,end_date},"between") +" end) else "+table_schema.getColumn("table","ancservice_visitdate")+" is null end)) "
                        + " AND " + sql_condition + " AND ("+table_schema.getColumn("table", "providerdb_exdate",new String[]{start_date},">=")+" or "+table_schema.getColumn("table", "providerdb_exdate",new String[]{},"isnull")+") ";
                if(sql_view_type.equals("3"))
                    create_sql += "GROUP BY \"Union Name\", "+table_schema.getColumn("facility_info", "unions_unionid")+" ";
                else if(sql_view_type.equals("4"))
                    create_sql += "GROUP BY "+table_schema.getColumn("facility_info", "unions_unionid")+", "+table_schema.getColumn("facility_info", "facility_registry_facilityid")+", \"Facility Name\", \"Union Name\"";
                else if (form_type.equals("1"))
                    create_sql += "GROUP BY "+table_schema.getColumn("table","providerdb_provcode")+", "+table_schema.getColumn("table","providerdb_provname")+", "+table_schema.getColumn("table","providertype_typename")+", "+table_schema.getColumn("table","providerdb_mobileno")+", "+table_schema.getColumn("table","providerdb_facilityname")+", ct_prov.count_provider, \"Union Name\", "+table_schema.getColumn("","facility_registry_facility_name")+" ";
                */
                //create_sql += "order by \"TOTAL\" desc";
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
//		System.out.println(create_sql);
        return create_sql;
    }
}
