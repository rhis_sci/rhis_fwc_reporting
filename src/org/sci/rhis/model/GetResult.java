package org.sci.rhis.model;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBListProperties;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class GetResult {
    private String sql;
    private String zilla;
    private String upazila;
    private String sql_view_type;
    private String sql_method_type;
    private String form_type;
    private String start_date;
    private String end_date;


    public GetResult(String sql, String zilla, String upazila) {
        this.sql = sql;
        this.zilla = zilla;
        this.upazila = upazila;
    }

    public GetResult(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date) {
        this.sql_view_type = sql_view_type;
        this.sql_method_type = sql_method_type;
        this.form_type = form_type;
        this.zilla = zilla;
        this.upazila = upazila;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public JSONObject getResultData() {
        JSONObject data = new JSONObject();
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = null;
        if (zilla.equals(""))
            dbObject = dbFacility.facilityDBInfo();
        else if (!upazila.equals(""))
            dbObject = dbFacility.dynamicDBInfo(zilla, upazila);
        else
            dbObject = dbFacility.facilityDBInfo(zilla);
        try {
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            while (rs.next()) {
                String getValue;
                for (int i = 1; i <= columnCount; i++) {
                    data.put(metadata.getColumnName(i), rs.getString(metadata.getColumnName(i)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;

        }
        return data;
    }

    public JSONObject getThreadResult() {
        JSONObject jsonResultSet = new JSONObject();
        try {
            int rowCount = this.count_upazila();
            ExecutorService pool = Executors.newFixedThreadPool(rowCount);
            List<Future<JSONObject>> threadResult = new ArrayList<Future<JSONObject>>();

            DBListProperties setZillaList = new DBListProperties();
            Properties db_prop = setZillaList.setProperties();
            String[] zilla_list = db_prop.getProperty("zilla").split(",");
            GetSSSql getSSSql = new GetSSSql();

            if (zilla.equals("0")) {
                for (int i = 0; i < zilla_list.length; i++) {
                    String[] upazila_list = db_prop.getProperty(zilla_list[i]).split(",");
                    for (int j = 0; j < upazila_list.length; j++) {

                        sql = getSSSql.getSql(sql_view_type, sql_method_type, form_type, zilla_list[i], upazila_list[j], start_date, end_date);

                        threadResult.add(pool.submit(new GetThreadResult(zilla_list[i], upazila_list[j], sql)));
                    }
                }
            } else {
                String[] upazila_list = db_prop.getProperty(zilla).split(",");
                for (int j = 0; j < upazila_list.length; j++) {
                    sql = getSSSql.getSql(sql_view_type, sql_method_type, form_type, zilla, upazila_list[j], start_date, end_date);

                    threadResult.add(pool.submit(new GetThreadResult(zilla, upazila_list[j], sql)));
                }
            }

            getSSSql = null;

            pool.shutdown();
            while (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                System.out.println("Awaiting completion of threads.");
            }

            for (Future<JSONObject> result : threadResult) {
                JSONObject resultSet = new JSONObject(String.valueOf(result.get()));
                Iterator<String> iterator = resultSet.keys();
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    jsonResultSet.put(key, resultSet.get(key));
                }
            }

//            System.out.println(jsonResultSet);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

        return jsonResultSet;
    }

    public JSONObject getUnionResult() {
        JSONObject data = new JSONObject();
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.dynamicDBInfo(zilla, upazila);

        try {
            GetSSSql getSSSql = new GetSSSql();
            sql = getSSSql.getSql(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            while (rs.next()) {
                JSONObject unionData = new JSONObject();
                for (int i = 2; i <= columnCount; i++) {
                    unionData.put(metadata.getColumnName(i), ((rs.getObject(metadata.getColumnName(i)) == null) ? "" : rs.getString(metadata.getColumnName(i))));
                }
                data.put(zilla + "_" + upazila + "_" + rs.getString(metadata.getColumnName(1)), unionData);
            }

            getSSSql = null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;

        }
        return data;
    }

    public int count_upazila() {
        int count = 0;
        try {
            DBListProperties setZillaList = new DBListProperties();
            Properties db_prop = setZillaList.setProperties();
            if (zilla.equals("0")) {
                String[] zilla_list = db_prop.getProperty("zilla").split(",");
                for (int i = 0; i < zilla_list.length; i++) {
                    String[] upazila_list = db_prop.getProperty(zilla_list[i]).split(",");
                    count += upazila_list.length;
                }
            } else {
                String[] upazila_list = db_prop.getProperty(zilla).split(",");
                count = upazila_list.length;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public JSONObject getSSAggrResult() {
        JSONObject result_data = new JSONObject();

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        GetSSAggrSql getSSAggrSql = new GetSSAggrSql();

        try {
            sql = getSSAggrSql.getSql(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            int j = 0;
            if (sql_view_type.equals("1"))
                j = 2;
            else if (sql_view_type.equals("2"))
                j = 3;
            else if (sql_view_type.equals("3"))
                j = 4;

            String col_val;
            while (rs.next()) {
                JSONObject data = new JSONObject();
                for (int i = j; i <= columnCount; i++) {
                    if (rs.getString(metadata.getColumnName(i)) != null)
                        col_val = rs.getString(metadata.getColumnName(i));
                    else
                        col_val = "0";

                    data.put(metadata.getColumnName(i), col_val);
                }
                if (sql_view_type.equals("1"))
                    result_data.put(rs.getString(metadata.getColumnName(1)), data);
                else if (sql_view_type.equals("2"))
                    result_data.put(rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2)), data);
                else if (sql_view_type.equals("3"))
                    result_data.put(rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2)) + "_" + rs.getString(metadata.getColumnName(3)), data);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
            getSSAggrSql = null;
        }
        return result_data;
    }

    public LinkedHashMap<String, LinkedHashMap<String, String>> getSSAggrResultMap() {
        LinkedHashMap<String, LinkedHashMap<String, String>> result_data = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        GetSSAggrSql getSSAggrSql = new GetSSAggrSql();

        try {
            sql = getSSAggrSql.getSql(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            int j = 0;
            if (sql_view_type.equals("1"))
                j = 2;
            else if (sql_view_type.equals("2"))
                j = 3;
            else if (sql_view_type.equals("3"))
                j = 4;
            else if (sql_view_type.equals("4"))
                j = 5;

            String col_val;
            String key = null;
            while (rs.next()) {
                if (sql_view_type.equals("1"))
                    key = rs.getString(metadata.getColumnName(1));
                else if (sql_view_type.equals("2"))
                    key = rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2));
                else if (sql_view_type.equals("3"))
                    key = rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2)) + "_" + rs.getString(metadata.getColumnName(3));
                else if (sql_view_type.equals("4"))
                    key = rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2)) + "_" + rs.getString(metadata.getColumnName(3)) + "_" + rs.getString(metadata.getColumnName(4));

                result_data.put(key, new LinkedHashMap<String, String>());

                for (int i = j; i <= columnCount; i++) {
                    if (rs.getString(metadata.getColumnName(i)) != null)
                        col_val = rs.getString(metadata.getColumnName(i));
                    else
                        col_val = "0";

                    result_data.get(key).put(metadata.getColumnName(i), col_val);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
            getSSAggrSql = null;
        }
        return result_data;
    }


    public LinkedHashMap<String, LinkedHashMap<String, String>> getSSAggrResultMap_timeseries() {
        LinkedHashMap<String, LinkedHashMap<String, String>> result_data = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        GetSSAggrSql getSSAggrSql = new GetSSAggrSql();

        try {
            sql = getSSAggrSql.getSql_update(sql_view_type, sql_method_type, form_type, zilla, upazila, start_date, end_date);
            dbObject = dbOp.dbExecute(dbObject, sql);
            ResultSet rs = dbObject.getResultSet();
            ResultSetMetaData metadata = rs.getMetaData();
            int columnCount = metadata.getColumnCount();

            int j = 0;
            if (sql_view_type.equals("1"))
                j = 2;
            else if (sql_view_type.equals("2"))
                j = 3;
            else if (sql_view_type.equals("3"))
                j = 4;
            else if (sql_view_type.equals("4"))
                j = 5;

            String col_val;
            String key = null;
            while (rs.next()) {
                if (sql_view_type.equals("1"))
                    key = rs.getString(metadata.getColumnName(1)) + "_" + rs.getString("month").trim() + "_" + rs.getString("Year").trim();
                else if (sql_view_type.equals("2"))
                    key = rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2)) + "_" + rs.getString("month").trim() + "_" + rs.getString("Year").trim();
                else if (sql_view_type.equals("3"))
                    key = rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2)) + "_" + rs.getString(metadata.getColumnName(3)) + "_" + rs.getString("month").trim() + "_" + rs.getString("Year").trim();
                else if (sql_view_type.equals("4"))
                    key = rs.getString(metadata.getColumnName(1)) + "_" + rs.getString(metadata.getColumnName(2)) + "_" + rs.getString(metadata.getColumnName(3)) + "_" + rs.getString(metadata.getColumnName(4)).trim() + "_" + rs.getString("month") + "_" + rs.getString("Year").trim();

                result_data.put(key, new LinkedHashMap<String, String>());

                for (int i = j; i <= columnCount; i++) {
                    if (rs.getString(metadata.getColumnName(i)) != null)
                        col_val = rs.getString(metadata.getColumnName(i));
                    else
                        col_val = "0";

                    result_data.get(key).put(metadata.getColumnName(i), col_val);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.closeResultSet(dbObject);
            dbOp.closePreparedStatement(dbObject);
            dbOp.closeConn(dbObject);
            dbObject = null;
            getSSAggrSql = null;
        }
        return result_data;
    }

    /**
     * @param request(json)
     * @return resultset against any specific dboperation(ex: select * from providerdb)
     */
    public static ResultSet getResultSet(JSONObject request, String table_name) {
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        String zilla = request.getString("zilla");
        String upz = request.getString("upazila");
        String div = request.getString("division");

        String sql = null;
        if (table_name.equalsIgnoreCase("web_fn_provider_sync_status")) {
            String end_date = request.getString("end_date");
            if (div.equals("0")) {
                sql = "select * from " + table_name + "('','','" + end_date + "')";
            } else {
                sql = "select * from " + table_name + "('" + zilla + "','','" + end_date + "')";
            }

            if (!(upz.isEmpty()) && !(upz.equals("null")))
                sql = "select * from " + table_name + "('" + zilla + "','" + upz + "','" + end_date + "')";
                dbOp.dbExecute(dbObject, sql);

        } else if (table_name.equalsIgnoreCase("emis_hr_info") || table_name.equalsIgnoreCase("provider_tab_storage_info")) {
            if (div.equals("0")) {
                sql = "select * from " + table_name;
            } else {
                sql = "select * from " + table_name + " where zillaid= " + zilla;
            }


            if (!(upz.isEmpty()) && !(upz.equals("null")))
                sql += " and upazilaid = " + upz;
            dbOp.dbExecute(dbObject, sql);

        }
        ResultSet rs = dbObject.getResultSet();
        return rs;

    }




    /**
     * @param request(json)
     * @return resultset against any specific dboperation(ex: select * from providerdb)
     */
    public static JSONObject getDataSet(JSONObject request) {
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();

        /*String delivery = request.getString("delivery");
        String gp = request.getString("gp");
        String anc = request.getString("anc");
        String pillcondom = request.getString("pillcondom");
        String iud = request.getString("iud");
        String pregwomen = request.getString("pregwomen");*/

        JSONObject datObj = new JSONObject();
        datObj.put("delivery", request.getString("delivery"));
        datObj.put("gp", request.getString("gp"));
        datObj.put("anc", request.getString("anc"));
        datObj.put("pillcondom", request.getString("pillcondom"));
        datObj.put("iud", request.getString("iud"));
        datObj.put("pregwomen", request.getString("pregwomen"));

        return datObj;

    }
}
