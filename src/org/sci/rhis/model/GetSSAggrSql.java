package org.sci.rhis.model;

public class GetSSAggrSql {

    public String getSql(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date){
        String create_sql = "";
        try {
            create_sql = "select fr_data.*, ";
            if(sql_method_type.equals("1")) //MNC Data
                create_sql += "\"ANC1\",\"ANC2\",\"ANC3\",\"ANC4\",\"Delivery\",\"PNC1\",\"PNC2\",\"PNC3\",\"PNC4\",\"PNC1-N\",\"PNC2-N\",\"PNC3-N\",\"PNC4-N\" ";
            else if (sql_method_type.equals("2")) //FP Data
                create_sql += "\"Pill\",\"Condom\",\"IUD\",\"IUD Followup\",\"Implant\",\"Impalnt Followup\",\"Injectable\",\"Permanet Method\",\"Permanet Method Followup\" ";
            else if (sql_method_type.equals("3")) //GP Data
                create_sql += "\"Male\",\"Female\" ";
            else if (sql_method_type.equals("4")) //QED
                create_sql += "\"Maternal Deaths\",\"Maternal Deaths-Haemorrhage\",\"Maternal Deaths-Preeclampsia/Eclampsia\",\"Maternal Deaths-Sepsis\",\"Maternal Deaths-Other\",\"Neonatal Deaths\",\"Neonatal Deaths-Prematurity\",\"Neonatal Deaths-Birth Asphyxia\",\"Neonatal Deaths-Sepsis\",\"Neonatal Deaths-Other\",\"Total Newborn\",\"Stillbirth\",\"Stillbirth-Fresh\",\"Stillbirth-Macerated\",\"Breastfed\",\"Oxytocin\",\"Birthweight\",\"Weighing≤2,000g\",\"Total Delivery\" ";
            else if (sql_method_type.equals("5")) //Flu Like Symptom
                create_sql += "\"Fever_60+\",\"Fever_0-59\",\"Fever_PW\",\"Cold_60+\",\"Cold_0-59\",\"Cough_60+\",\"Cough_0-59\",\"Shortness_Breath_60+\",\"Shortness_Breath_0-59\",\"Shortness_Breath_PW\",\"Headache_60+\",\"Headache_0-59\",\"Sore_Throat_60+\",\"Sore_Throat_0-59\" ";
            else if (sql_method_type.equals("6")) //child service Data
                create_sql += "\"Boy\",\"Girl\", \"Child(0-1)\",\"Child(1-5)\"";
            create_sql += "from(select ";
            if(sql_view_type.equals("1")) //All District
                create_sql += "fr.zillaid ";
            else if (sql_view_type.equals("2")) //District All Upazila
                create_sql += "fr.zillaid,fr.upazilaid ";
            else if (sql_view_type.equals("3")) //Upazila All Union
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid ";
            else if(sql_view_type.equals("4")) //Upazila All Facility
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid,fr.facilityid,up.upazilanameeng,un.unionnameeng,fr.facility_name ";
            if (sql_view_type.equals("3") && (form_type.equals("3")||form_type.equals("29")))
                create_sql += ", un.unionnameeng ";
            create_sql += "from emis_facility_all_facility_registry fr " +
                    "inner join emis_facility_all_facility_provider fp on fp.facility_id=fr.facilityid " +
                    "inner join zilla z on z.zillaid=fr.zillaid ";
            if (sql_view_type.equals("2") || sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "inner join upazila up on up.zillaid=fr.zillaid and up.upazilaid=fr.upazilaid ";
            if (sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "inner join unions un on un.zillaid=fr.zillaid and un.upazilaid=fr.upazilaid and un.unionid=fr.unionid ";
            create_sql += "where ";
            if (sql_view_type.equals("2") || (sql_view_type.equals("4") && upazila.equals("")))
                create_sql += "fr.zillaid="+zilla+" and  ";
            else if(sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "fr.zillaid="+zilla+" and fr.upazilaid="+upazila+" and  ";
            create_sql += "(end_date::date>='"+start_date+"' or end_date is null) ";
            if(sql_view_type.equals("1"))
                create_sql += "group by 1";
            else if (sql_view_type.equals("2"))
                create_sql += "group by 1,2";
            else if (sql_view_type.equals("3"))
                create_sql += "group by 1,2,3";
            else if(sql_view_type.equals("4"))
                create_sql += "group by 1,2,3,4,5,6,7";
            if (sql_view_type.equals("3") && (form_type.equals("3")||form_type.equals("29")))
                create_sql += ",4";
            create_sql += ") fr_data " +
                    "left join (select ";
            if(sql_view_type.equals("1"))
                create_sql += "fr.zillaid ";
            else if (sql_view_type.equals("2"))
                create_sql += "fr.zillaid,fr.upazilaid ";
            else if (sql_view_type.equals("3"))
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid ";
            else if(sql_view_type.equals("4"))
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid,fr.facilityid ";//,up.upazilanameeng,un.unionnameeng,fr.facility_name ";
//            if (sql_view_type.equals("3") && form_type.equals("3"))
//                create_sql += ", un.unionnameeng ";
            if(sql_method_type.equals("1")) {
                create_sql += ",sum((mnc_data->>'ANC1')::int) as \"ANC1\" " +
                        ",sum((mnc_data->>'ANC2')::int) as \"ANC2\" " +
                        ",sum((mnc_data->>'ANC3')::int) as \"ANC3\" " +
                        ",sum((mnc_data->>'ANC4')::int) as \"ANC4\" " +
                        ",sum((mnc_data->>'Delivery')::int) as \"Delivery\" " +
                        ",sum((mnc_data->>'PNC1')::int) as \"PNC1\" " +
                        ",sum((mnc_data->>'PNC2')::int) as \"PNC2\" " +
                        ",sum((mnc_data->>'PNC3')::int) as \"PNC3\" " +
                        ",sum((mnc_data->>'PNC4')::int) as \"PNC4\" " +
                        ",sum((mnc_data->>'PNC1-N')::int) as \"PNC1-N\" " +
                        ",sum((mnc_data->>'PNC-N')::int) as \"PNC2-N\" " +
                        ",sum((mnc_data->>'PNC3-N')::int) as \"PNC3-N\" " +
                        ",sum((mnc_data->>'PNC4-N')::int) as \"PNC4-N\" ";
            }
            else if (sql_method_type.equals("2")) {
                create_sql += ",sum((fp_data->>'Pill')::int) as \"Pill\" " +
                        ",sum((fp_data->>'Condom')::int) as \"Condom\" " +
                        ",sum((fp_data->>'IUD')::int) as \"IUD\" " +
                        ",sum((fp_data->>'IUD Followup')::int) as \"IUD Followup\" " +
                        ",sum((fp_data->>'Implant')::int) as \"Implant\" " +
                        ",sum((fp_data->>'Impalnt Followup')::int) as \"Impalnt Followup\" " +
                        ",sum((fp_data->>'Injectable')::int) as \"Injectable\" " +
                        ",sum((fp_data->>'Permanet Method')::int) as \"Permanet Method\" " +
                        ",sum((fp_data->>'Permanet Method Followup')::int) as \"Permanet Method Followup\" ";
            }
            else if(sql_method_type.equals("3")) {
                create_sql += ",sum((gp_data->>'Male')::int) as \"Male\" " +
                        ",sum((gp_data->>'Female')::int) as \"Female\" ";
            }
            else if(sql_method_type.equals("4")){
                create_sql += ",sum((death_data->>'Maternal Deaths')::int) as \"Maternal Deaths\" " +
                        ",sum((death_data->>'Haemorrhage Deaths')::int) as \"Maternal Deaths-Haemorrhage\" " +
                        ",sum((death_data->>'Preeclampsia Eclampsia Deaths')::int) as \"Maternal Deaths-Preeclampsia/Eclampsia\" " +
                        ",sum((death_data->>'Maternal Sepsis Deaths')::int) as \"Maternal Deaths-Sepsis\" " +
                        ",sum((death_data->>'Maternal Deaths Other')::int) as \"Maternal Deaths-Other\" " +
                        ",sum((death_data->>'Meonatal Deaths')::int) as \"Neonatal Deaths\" " +
                        ",sum((death_data->>'Prematurity Deaths')::int) as \"Neonatal Deaths-Prematurity\" " +
                        ",sum((death_data->>'Birth Asphyxia Deaths')::int) as \"Neonatal Deaths-Birth Asphyxia\" " +
                        ",sum((death_data->>'Neonatal Sepsis Deaths')::int) as \"Neonatal Deaths-Sepsis\" " +
                        ",sum((death_data->>'Neonatal Deaths Other')::int) as \"Neonatal Deaths-Other\" " +
                        ",sum((mnc_data->>'Total_Newborn')::int) as \"Total Newborn\" " +
                        ",sum((mnc_data->>'Stillbirth')::int) as \"Stillbirth\" " +
                        ",sum((mnc_data->>'Stillbirth_Fresh')::int) as \"Stillbirth-Fresh\" " +
                        ",sum((mnc_data->>'Stillbirth_Macerated')::int) as \"Stillbirth-Macerated\" " +
                        ",sum((mnc_data->>'Breastfed')::int) as \"Breastfed\" " +
                        ",sum((mnc_data->>'Oxytocin')::int) as \"Oxytocin\" " +
                        ",sum((mnc_data->>'Birthweight_Documented')::int) as \"Birthweight\" " +
                        ",sum((mnc_data->>'Weighing ≤ 2,000g')::int) as \"Weighing≤2,000g\" " +
                        ",sum((mnc_data->>'Delivery')::int) as \"Total Delivery\" ";
            }
            else if(sql_method_type.equals("5")){
                create_sql += ",sum((gp_data->>'Fever_60+')::int) as \"Fever_60+\" " +
                        ",sum((gp_data->>'Fever_0-59')::int) as \"Fever_0-59\" " +
                        ",sum((mnc_data->>'Fever_PW')::int) as \"Fever_PW\" " +
                        ",sum((gp_data->>'Cold_60+')::int) as \"Cold_60+\" " +
                        ",sum((gp_data->>'Cold_0-59')::int) as \"Cold_0-59\" " +
                        ",sum((gp_data->>'Cough_60+')::int) as \"Cough_60+\" " +
                        ",sum((gp_data->>'Cough_0-59')::int) as \"Cough_0-59\" " +
                        ",sum((gp_data->>'Shortness_Breath_60+')::int) as \"Shortness_Breath_60+\" " +
                        ",sum((gp_data->>'Shortness_Breath_0-59')::int) as \"Shortness_Breath_0-59\" " +
                        ",sum((mnc_data->>'Shortness_Breath_PW')::int) as \"Shortness_Breath_PW\" " +
                        ",sum((gp_data->>'Headache_60+')::int) as \"Headache_60+\" " +
                        ",sum((gp_data->>'Headache_0-59')::int) as \"Headache_0-59\" " +
                        ",sum((gp_data->>'Sore_Throat_60+')::int) as \"Sore_Throat_60+\" " +
                        ",sum((gp_data->>'Sore_Throat_0-59')::int) as \"Sore_Throat_0-59\" ";
            }
            //child service data
            else if(sql_method_type.equals("6")){
                    create_sql += ",sum((child_data->>'Boy')::int) as \"Boy\" " +
                                  ",sum((child_data->>'Girl')::int) as \"Girl\" "+
                                  ",sum((child_data->>'Child_0-1')::int) as \"Child(0-1)\" "+
                                  ",sum((child_data->>'Child_1-5')::int) as \"Child(1-5)\" ";
            }
            create_sql += "from emis_facility_aggregate_data ad " +
                    "inner join emis_facility_all_facility_provider fp using(providerid) " +
                    "inner join emis_facility_all_facility_registry fr on fr.facilityid=fp.facility_id and fr.zillaid=ad.zillaid and fr.upazilaid=ad.upazilaid ";
//            if(sql_view_type.equals("4"))
//                create_sql += "inner join upazila up on up.zillaid=fr.zillaid and up.upazilaid=fr.upazilaid ";
//            if (sql_view_type.equals("3") || sql_view_type.equals("4"))
//                create_sql += "inner join unions un on un.zillaid=fr.zillaid and un.upazilaid=fr.upazilaid and un.unionid=fr.unionid ";
            create_sql += "where ";
            if (sql_view_type.equals("2") || (sql_view_type.equals("4") && upazila.equals("")))
                create_sql += "fr.zillaid="+zilla+" and  ";
            else if(sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "fr.zillaid="+zilla+" and fr.upazilaid="+upazila+" and  ";
            create_sql += "(end_date::date>='"+start_date+"' or end_date is null) and " +
                    "(case when is_active=2 and end_date::date>='"+start_date+"' and end_date::date<='"+end_date+"' then report_date between '"+start_date+"' and end_date::date " +
                    "when is_active=1 and start_date::date>='"+start_date+"' then report_date between start_date::date and '"+end_date+"' " +
                    "else report_date between '"+start_date+"' and '"+end_date+"' end) ";
            if(sql_view_type.equals("1"))
                create_sql += "group by 1";
            else if (sql_view_type.equals("2"))
                create_sql += "group by 1,2";
            else if (sql_view_type.equals("3"))
                create_sql += "group by 1,2,3";
            else if(sql_view_type.equals("4"))
                create_sql += "group by 1,2,3,4";//,5,6,7";
//            if (sql_view_type.equals("3") && form_type.equals("3"))
//                create_sql += ",4";
            create_sql += ") as ss_data on ss_data.zillaid=fr_data.zillaid ";
            if (sql_view_type.equals("2") || sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "and ss_data.upazilaid=fr_data.upazilaid ";
            if (sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "and ss_data.unionid=fr_data.unionid ";
            if (sql_view_type.equals("4"))
                create_sql += "and ss_data.facilityid=fr_data.facilityid ";

        }
        catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(create_sql);
        return create_sql;
    }


    public String getSql_update(String sql_view_type, String sql_method_type, String form_type, String zilla, String upazila, String start_date, String end_date){
        String create_sql = "";
        String select_column = "";
        String start_part = " \"Year\", \"month\",";
        String geo_part ="";
        String end_part = "";
        try {

            create_sql = "select \"Year\", \"Month\", \"month\", fr_data.*, ";
            if(sql_method_type.equals("1")) //MNC Data
                select_column += "\"ANC1\",\"ANC2\",\"ANC3\",\"ANC4\",\"Delivery\",\"PNC1\",\"PNC2\",\"PNC3\",\"PNC4\",\"PNC1-N\",\"PNC2-N\",\"PNC3-N\",\"PNC4-N\" ";
            else if (sql_method_type.equals("2")) //FP Data
                select_column += "\"Pill\",\"Condom\",\"IUD\",\"IUD Followup\",\"Implant\",\"Impalnt Followup\",\"Injectable\",\"Permanet Method\",\"Permanet Method Followup\" ";
            else if (sql_method_type.equals("3")) //GP Data
                select_column += "\"Male\",\"Female\" ";
            else if (sql_method_type.equals("4")) //QED
                select_column += "\"Maternal Deaths\",\"Maternal Deaths-Haemorrhage\",\"Maternal Deaths-Preeclampsia/Eclampsia\",\"Maternal Deaths-Sepsis\",\"Maternal Deaths-Other\",\"Neonatal Deaths\",\"Neonatal Deaths-Prematurity\",\"Neonatal Deaths-Birth Asphyxia\",\"Neonatal Deaths-Sepsis\",\"Neonatal Deaths-Other\",\"Total Newborn\",\"Stillbirth\",\"Stillbirth-Fresh\",\"Stillbirth-Macerated\",\"Breastfed\",\"Oxytocin\",\"Birthweight\",\"Weighing≤2,000g\",\"Total Delivery\" ";
            else if (sql_method_type.equals("5")) //Flu Like Symptom
                select_column += "\"Fever_60+\",\"Fever_0-59\",\"Fever_PW\",\"Cold_60+\",\"Cold_0-59\",\"Cough_60+\",\"Cough_0-59\",\"Shortness_Breath_60+\",\"Shortness_Breath_0-59\",\"Shortness_Breath_PW\",\"Headache_60+\",\"Headache_0-59\",\"Sore_Throat_60+\",\"Sore_Throat_0-59\" ";
            create_sql += select_column +"from(select ";
            if(sql_view_type.equals("1")) { //All District
                create_sql += "fr.zillaid ";
                geo_part += "zillaid";
            }
            else if (sql_view_type.equals("2")) {//District All Upazila
                create_sql += "fr.zillaid,fr.upazilaid ";
                geo_part += "zillaid,upazilaid ";
            }
            else if (sql_view_type.equals("3")) { //Upazila All Union
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid ";
                geo_part += "zillaid,upazilaid,unionid,unionnameeng ";
            }
            else if(sql_view_type.equals("4")) {//Upazila All Facility
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid,fr.facilityid,up.upazilanameeng,un.unionnameeng,fr.facility_name ";
                geo_part += "zillaid,upazilaid,unionid,facilityid,upazilanameeng,unionnameeng,facility_name ";
            }
            if (sql_view_type.equals("3") && form_type.equals("3"))
                create_sql += ", un.unionnameeng ";
            create_sql += "from emis_facility_all_facility_registry fr " +
                    "inner join emis_facility_all_facility_provider fp on fp.facility_id=fr.facilityid " +
                    "inner join zilla z on z.zillaid=fr.zillaid ";
            if (sql_view_type.equals("2") || sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "inner join upazila up on up.zillaid=fr.zillaid and up.upazilaid=fr.upazilaid ";
            if (sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "inner join unions un on un.zillaid=fr.zillaid and un.upazilaid=fr.upazilaid and un.unionid=fr.unionid ";
            create_sql += "where ";
            if (sql_view_type.equals("2") || (sql_view_type.equals("4") && upazila.equals("")))
                create_sql += "fr.zillaid="+zilla+" and  ";
            else if(sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "fr.zillaid="+zilla+" and fr.upazilaid="+upazila+" and  ";
            create_sql += "(end_date::date>='"+start_date+"' or end_date is null) ";
            if(sql_view_type.equals("1"))
                create_sql += "group by 1";
            else if (sql_view_type.equals("2"))
                create_sql += "group by 1,2";
            else if (sql_view_type.equals("3"))
                create_sql += "group by 1,2,3";
            else if(sql_view_type.equals("4"))
                create_sql += "group by 1,2,3,4,5,6,7";
            if (sql_view_type.equals("3") && form_type.equals("3"))
                create_sql += ",4";
            create_sql += ") fr_data " +
                    "left join (select ";
            String time_series_sql = "date_part('year' :: text, ad.report_date)                              AS \"Year\",\n" +
                    "date_part('month' :: text, ad.report_date)                             AS \"Month\",\n" +
                    "to_char((ad.report_date) :: timestamp with time zone, 'MONTH' :: text) AS month,";
            create_sql += time_series_sql;
            if(sql_view_type.equals("1"))
                create_sql += "fr.zillaid ";
            else if (sql_view_type.equals("2"))
                create_sql += "fr.zillaid,fr.upazilaid ";
            else if (sql_view_type.equals("3"))
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid ";
            else if(sql_view_type.equals("4"))
                create_sql += "fr.zillaid,fr.upazilaid,fr.unionid,fr.facilityid ";//,up.upazilanameeng,un.unionnameeng,fr.facility_name ";
//            if (sql_view_type.equals("3") && form_type.equals("3"))
//                create_sql += ", un.unionnameeng ";

            if(sql_method_type.equals("1")) {
                create_sql += ",sum((mnc_data->>'ANC1')::int) as \"ANC1\" " +
                        ",sum((mnc_data->>'ANC2')::int) as \"ANC2\" " +
                        ",sum((mnc_data->>'ANC3')::int) as \"ANC3\" " +
                        ",sum((mnc_data->>'ANC4')::int) as \"ANC4\" " +
                        ",sum((mnc_data->>'Delivery')::int) as \"Delivery\" " +
                        ",sum((mnc_data->>'PNC1')::int) as \"PNC1\" " +
                        ",sum((mnc_data->>'PNC2')::int) as \"PNC2\" " +
                        ",sum((mnc_data->>'PNC3')::int) as \"PNC3\" " +
                        ",sum((mnc_data->>'PNC4')::int) as \"PNC4\" " +
                        ",sum((mnc_data->>'PNC1-N')::int) as \"PNC1-N\" " +
                        ",sum((mnc_data->>'PNC-N')::int) as \"PNC2-N\" " +
                        ",sum((mnc_data->>'PNC3-N')::int) as \"PNC3-N\" " +
                        ",sum((mnc_data->>'PNC4-N')::int) as \"PNC4-N\" ";
            }
            else if (sql_method_type.equals("2")) {
                create_sql += ",sum((fp_data->>'Pill')::int) as \"Pill\" " +
                        ",sum((fp_data->>'Condom')::int) as \"Condom\" " +
                        ",sum((fp_data->>'IUD')::int) as \"IUD\" " +
                        ",sum((fp_data->>'IUD Followup')::int) as \"IUD Followup\" " +
                        ",sum((fp_data->>'Implant')::int) as \"Implant\" " +
                        ",sum((fp_data->>'Impalnt Followup')::int) as \"Impalnt Followup\" " +
                        ",sum((fp_data->>'Injectable')::int) as \"Injectable\" " +
                        ",sum((fp_data->>'Permanet Method')::int) as \"Permanet Method\" " +
                        ",sum((fp_data->>'Permanet Method Followup')::int) as \"Permanet Method Followup\" ";
            }
            else if(sql_method_type.equals("3")) {
                create_sql += ",sum((gp_data->>'Male')::int) as \"Male\" " +
                        ",sum((gp_data->>'Female')::int) as \"Female\" ";
            }
            else if(sql_method_type.equals("4")){
                create_sql += ",sum((death_data->>'Maternal Deaths')::int) as \"Maternal Deaths\" " +
                        ",sum((death_data->>'Haemorrhage Deaths')::int) as \"Maternal Deaths-Haemorrhage\" " +
                        ",sum((death_data->>'Preeclampsia Eclampsia Deaths')::int) as \"Maternal Deaths-Preeclampsia/Eclampsia\" " +
                        ",sum((death_data->>'Maternal Sepsis Deaths')::int) as \"Maternal Deaths-Sepsis\" " +
                        ",sum((death_data->>'Maternal Deaths Other')::int) as \"Maternal Deaths-Other\" " +
                        ",sum((death_data->>'Meonatal Deaths')::int) as \"Neonatal Deaths\" " +
                        ",sum((death_data->>'Prematurity Deaths')::int) as \"Neonatal Deaths-Prematurity\" " +
                        ",sum((death_data->>'Birth Asphyxia Deaths')::int) as \"Neonatal Deaths-Birth Asphyxia\" " +
                        ",sum((death_data->>'Neonatal Sepsis Deaths')::int) as \"Neonatal Deaths-Sepsis\" " +
                        ",sum((death_data->>'Neonatal Deaths Other')::int) as \"Neonatal Deaths-Other\" " +
                        ",sum((mnc_data->>'Total_Newborn')::int) as \"Total Newborn\" " +
                        ",sum((mnc_data->>'Stillbirth')::int) as \"Stillbirth\" " +
                        ",sum((mnc_data->>'Stillbirth_Fresh')::int) as \"Stillbirth-Fresh\" " +
                        ",sum((mnc_data->>'Stillbirth_Macerated')::int) as \"Stillbirth-Macerated\" " +
                        ",sum((mnc_data->>'Breastfed')::int) as \"Breastfed\" " +
                        ",sum((mnc_data->>'Oxytocin')::int) as \"Oxytocin\" " +
                        ",sum((mnc_data->>'Birthweight_Documented')::int) as \"Birthweight\" " +
                        ",sum((mnc_data->>'Weighing ≤ 2,000g')::int) as \"Weighing≤2,000g\" " +
                        ",sum((mnc_data->>'Delivery')::int) as \"Total Delivery\" ";
            }
            else if(sql_method_type.equals("5")){
                create_sql += ",sum((gp_data->>'Fever_60+')::int) as \"Fever_60+\" " +
                        ",sum((gp_data->>'Fever_0-59')::int) as \"Fever_0-59\" " +
                        ",sum((mnc_data->>'Fever_PW')::int) as \"Fever_PW\" " +
                        ",sum((gp_data->>'Cold_60+')::int) as \"Cold_60+\" " +
                        ",sum((gp_data->>'Cold_0-59')::int) as \"Cold_0-59\" " +
                        ",sum((gp_data->>'Cough_60+')::int) as \"Cough_60+\" " +
                        ",sum((gp_data->>'Cough_0-59')::int) as \"Cough_0-59\" " +
                        ",sum((gp_data->>'Shortness_Breath_60+')::int) as \"Shortness_Breath_60+\" " +
                        ",sum((gp_data->>'Shortness_Breath_0-59')::int) as \"Shortness_Breath_0-59\" " +
                        ",sum((mnc_data->>'Shortness_Breath_PW')::int) as \"Shortness_Breath_PW\" " +
                        ",sum((gp_data->>'Headache_60+')::int) as \"Headache_60+\" " +
                        ",sum((gp_data->>'Headache_0-59')::int) as \"Headache_0-59\" " +
                        ",sum((gp_data->>'Sore_Throat_60+')::int) as \"Sore_Throat_60+\" " +
                        ",sum((gp_data->>'Sore_Throat_0-59')::int) as \"Sore_Throat_0-59\" ";
            }
            create_sql += "from emis_facility_aggregate_data ad " +
                    "inner join emis_facility_all_facility_provider fp using(providerid) " +
                    "inner join emis_facility_all_facility_registry fr on fr.facilityid=fp.facility_id and fr.zillaid=ad.zillaid and fr.upazilaid=ad.upazilaid ";
//            if(sql_view_type.equals("4"))
//                create_sql += "inner join upazila up on up.zillaid=fr.zillaid and up.upazilaid=fr.upazilaid ";
//            if (sql_view_type.equals("3") || sql_view_type.equals("4"))
//                create_sql += "inner join unions un on un.zillaid=fr.zillaid and un.upazilaid=fr.upazilaid and un.unionid=fr.unionid ";
            create_sql += "where ";
            if (sql_view_type.equals("2") || (sql_view_type.equals("4") && upazila.equals("")))
                create_sql += "fr.zillaid="+zilla+" and  ";
            else if(sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "fr.zillaid="+zilla+" and fr.upazilaid="+upazila+" and  ";
            create_sql += "(end_date::date>='"+start_date+"' or end_date is null) and " +
                    "(case when is_active=2 and end_date::date>='"+start_date+"' and end_date::date<='"+end_date+"' then report_date between '"+start_date+"' and end_date::date " +
                    "when is_active=1 and start_date::date>='"+start_date+"' then report_date between start_date::date and '"+end_date+"' " +
                    "else report_date between '"+start_date+"' and '"+end_date+"' end) ";
            create_sql +="group by 1,2,3";
            if(sql_view_type.equals("1"))
                create_sql += ",4";
            else if (sql_view_type.equals("2"))
                create_sql += ",4,5";
            else if (sql_view_type.equals("3"))
                create_sql += ",4,5,6";
            else if(sql_view_type.equals("4"))
                create_sql += ",4,5,6,7";//,5,6,7";
//            if (sql_view_type.equals("3") && form_type.equals("3"))
//                create_sql += ",4";
            create_sql += "order by 1,3";
            create_sql += ") as ss_data on ss_data.zillaid=fr_data.zillaid ";
            if (sql_view_type.equals("2") || sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "and ss_data.upazilaid=fr_data.upazilaid ";
            if (sql_view_type.equals("3") || sql_view_type.equals("4"))
                create_sql += "and ss_data.unionid=fr_data.unionid ";
            if (sql_view_type.equals("4"))
                create_sql += "and ss_data.facilityid=fr_data.facilityid ";

            end_part += " order by \"Year\", \"Month\") total ";

            create_sql = "select "+ geo_part +","+ start_part +select_column + "from (" +create_sql+ end_part;




        }
        catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(create_sql);
        return create_sql;
    }
}
