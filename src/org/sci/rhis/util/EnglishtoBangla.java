package org.sci.rhis.util;


public class EnglishtoBangla {

	public static String bangla_month;
	public static String bangla_number;
	
	public static String ConvertMonth(String givenMonth) {
		
		try {
			if(givenMonth.equals("01") || givenMonth.equals("1"))
				bangla_month = "জানুয়ারী";
			else if(givenMonth.equals("02") || givenMonth.equals("2"))
				bangla_month = "ফেব্রুয়ারি";
			else if(givenMonth.equals("03") || givenMonth.equals("3"))
				bangla_month = "মার্চ";
			else if(givenMonth.equals("04") || givenMonth.equals("4"))
				bangla_month = "এপ্রিল";
			else if(givenMonth.equals("05") || givenMonth.equals("5"))
				bangla_month = "মে";
			else if(givenMonth.equals("06") || givenMonth.equals("6"))
				bangla_month = "জুন";
			else if(givenMonth.equals("07") || givenMonth.equals("7"))
				bangla_month = "জুলাই";
			else if(givenMonth.equals("08") || givenMonth.equals("8"))
				bangla_month = "অগাস্ট";
			else if(givenMonth.equals("09") || givenMonth.equals("9"))
				bangla_month = "সেপ্টেম্বর";
			else if(givenMonth.equals("10") || givenMonth.equals("10"))
				bangla_month = "অক্টোবর";
			else if(givenMonth.equals("11") || givenMonth.equals("11"))
				bangla_month = "নভেম্বর";
			else if(givenMonth.equals("12") || givenMonth.equals("12"))
				bangla_month = "ডিসেম্বর";
		} 
		catch(Exception e){
			e.printStackTrace();			
		}
		
		return bangla_month;
		
	}
	
	public static String ConvertNumberToBangla(String givenNumber) {

		char[] banglaMap = {'০','১','২','৩','৪','৫','৬','৭','৮','৯'};
        String banglaResponse = "";

        for(int i = 0; i< givenNumber.length(); i++) {
            try {
                banglaResponse += banglaMap[givenNumber.charAt(i) - 48];
            } catch (ArrayIndexOutOfBoundsException aiob) {
                if(givenNumber.charAt(i) > 57 || givenNumber.charAt(i) < 48) {
                       banglaResponse += givenNumber.charAt(i);
                } 
            }
        }

        return banglaResponse;
    }


    public static String ConvertNumberToEnglish(String givenNumber) {

        char[] numberMap = {'0','1','2','3','4','5','6','7','8','9'}; //first 40 byte is wastage but its acceptable

        String englishResponse = "";

        for(int i = 0; i< givenNumber.length(); i++) {
            try {
                englishResponse += numberMap[givenNumber.charAt(i) - 2534];
            } catch (ArrayIndexOutOfBoundsException aiob) {
                if(givenNumber.charAt(i) > 2543 || givenNumber.charAt(i) < 2534) {
                    englishResponse += givenNumber.charAt(i);
                } 
            }
        }

        return englishResponse;
    }
    public static String getEngToBanDate(String engDate){
		try {
			return ConvertNumberToBangla(engDate.substring(0,2))+" "
					+ConvertMonth(engDate.substring(3,5))+" "
					+ConvertNumberToBangla(engDate.substring(6,10));
		}catch (Exception e){
			return engDate;
		}
	}

}
