package org.sci.rhis.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author sabah.mugab
 * @created September, 2015
 */
public class CalendarDate {
		
	public static String getCurrentDate(){
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date1 = new SimpleDateFormat("yyyy-MM-dd");
        String date = date1.format(calendar.getTime());
        
		return date;
	}
	
	public static String getCurrentDateTime(){
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
	}
	
	public static Long getCurrentTimeInMill(){
		
		Calendar calendar = Calendar.getInstance();
		long timeInMill = calendar.getTimeInMillis();
        
		return timeInMill;
	}

	public static int getDaysCount(Date earlyDate){
		int days = 0;
		
		Calendar calendar = Calendar.getInstance();
		long milliSecondLatestDate = calendar.getTimeInMillis();

		calendar.setTime(earlyDate);
		long milliSecondEarlyDate= calendar.getTimeInMillis();
			
		long milliSecondDiff = milliSecondLatestDate - milliSecondEarlyDate;

		days = (int) Math.floor((((milliSecondDiff/1000)/60)/60)/24);
       
		return days;
	}
	
	public static int getDaysCount(Date earlyDate, Date latestDate){
		int days = 0;
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(earlyDate);
		long milliSecondEarlyDate= calendar.getTimeInMillis();
		
		calendar.setTime(latestDate);
		long milliSecondLatestDate= calendar.getTimeInMillis();
		
		long milliSecondDiff = milliSecondLatestDate - milliSecondEarlyDate;

		days = (int) Math.floor((((milliSecondDiff/1000)/60)/60)/24);
       
		return days;
	}
	
	public static int daysInMonth(int year, int month){
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR,year);
		c.set(Calendar.MONTH,month-1);
		c.set(Calendar.DATE,1);
		
		return c.getActualMaximum(Calendar.DATE);
	}

	public static int currentDayMonthYear(String type){
		int value = 0;
		Calendar now = Calendar.getInstance();
		if(type.equals("Year"))
			value = now.get(Calendar.YEAR);
		else if (type.equals("Month"))
			value = (now.get(Calendar.MONTH) + 1);
		else if (type.equals("Day"))
			value = now.get(Calendar.DATE);
		return value;
	}
}
