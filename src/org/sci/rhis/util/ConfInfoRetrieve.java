package org.sci.rhis.util;

import java.util.Properties;
import java.io.IOException;

/**
 * @author sabah.mugab
 * @created November, 2015
 */
public class ConfInfoRetrieve {

	public static Properties readingConf(String location){
				
		Properties properties = new Properties();
		try{
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(location));
		}
		catch(IOException e){
			e.printStackTrace();
		}
	    return properties;
	}
}
