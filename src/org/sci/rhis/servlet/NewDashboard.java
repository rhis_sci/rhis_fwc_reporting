package org.sci.rhis.servlet;

import org.sci.rhis.db.DBListProperties;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NewDashboard extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

		try {
			PrintWriter out = response.getWriter();
			System.out.println("List Properties");

			//count zilla/upazila from properties file
			DBListProperties setZillaList = new DBListProperties();
			Properties db_prop = setZillaList.setProperties();
			String[] zilla_list = db_prop.getProperty("zilla_all").split(",");
			int countAllZilla = db_prop.getProperty("zilla_all").split(",").length;

			int countAllUpazilla = 0;
			for (String zilla:zilla_list){
				int numOfUpazila = db_prop.getProperty(zilla).split(",").length;
				countAllUpazilla = countAllUpazilla+numOfUpazila;
			}
			request.setAttribute("countAllZilla",countAllZilla);
			request.setAttribute("countAllUpazilla",countAllUpazilla);

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/dashboard/new_dashboard.jsp");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setDateHeader("Expires", 0); // Proxies.
			requestDispatcher.forward(request, response);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}
}
