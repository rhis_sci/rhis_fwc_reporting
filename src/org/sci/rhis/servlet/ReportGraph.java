package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.ReportResultSearch;

public class ReportGraph extends HttpServlet {
	
	public JSONObject reportGraph;

	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {
			//System.out.println("Report1Servlet");
			PrintWriter out = response.getWriter();
			
			JSONObject reportCred;
			
			reportCred = new JSONObject(request.getParameter("reportGraph"));
			
			//report1Search = ReportResultSearch.getSearchResult(reportCred);
			reportGraph = ReportResultSearch.getGraphResult(reportCred);
			//System.out.println("Advance Search Servlet: " + reportGraph);
			
			out.print(reportGraph);
			
//			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/reportGraph.jsp");
//			request.setAttribute("Result", reportGraph);
//			requestDispatcher.forward(request, response);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
