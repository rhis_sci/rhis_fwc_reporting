package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {
			//System.out.println("ReportServlet");
			PrintWriter out = response.getWriter();
			
			
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/dashboard_map.jsp");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setDateHeader("Expires", 0); // Proxies.
			requestDispatcher.forward(request, response);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
