package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.db.DBListProperties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Properties;

public class DistUpzList extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try{
            DBListProperties setZillaList = new DBListProperties();
            Properties geo_prop = setZillaList.setProperties();
            JSONObject geoList = new JSONObject();
            String[] zillaList = geo_prop.getProperty("zilla_all").split(",");
            geoList.put("Zilla", geo_prop.getProperty("zilla_all"));
            for (int i=0; i<zillaList.length; i++)
                geoList.put(zillaList[i], geo_prop.getProperty(zillaList[i]));

            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(geoList.toString());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
