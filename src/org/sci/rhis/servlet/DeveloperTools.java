package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.report.GetResultSet;
import org.sci.rhis.report.SynchronizationIssues;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Map;

public class DeveloperTools extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) {


        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        try{
            JSONObject infoApproval;
            PrintWriter out = response.getWriter();

            infoApproval = new JSONObject(request.getParameter("info"));
            String type = infoApproval.getString("menuType");
            String sql = "";

            RequestDispatcher requestDispatcher = null;

            if(type.equals("1")){

                requestDispatcher = request.getRequestDispatcher("jsp/developertools.jsp");

                DBSchemaInfo table_schema = new DBSchemaInfo();
                FacilityDB dbFacility = new FacilityDB();
                DBOperation dbOp = new DBOperation();
                DBInfoHandler dbObject = dbFacility.centralMonitorngInfo();
                sql = "select server_name,server_status from server_status";
                dbObject = dbOp.dbCreateStatement(dbObject);
                dbOp.dbExecute(dbObject,sql);

                ResultSet rs = dbObject.getResultSet();
                request.setAttribute("resultstring",rs);
                requestDispatcher.forward(request, response);


            }
            if (type.equals("3")){
                db_download_info(out);


            }
//            requestDispatcher = request.getRequestDispatcher("jsp/developertools.jsp");



            out.close();

        }catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }



    }

    public static void db_download_info(PrintWriter out) {
        int i = 0;
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.facilityDBInfo();
        String sql = "select zillaid,upazilaid,zillaname,upazilaname from emis_facility_implemented_upazila" +
                     " order by zillaid";
        dbObject = dbOp.dbCreateStatement(dbObject);
        dbOp.dbExecute(dbObject,sql);

        ResultSet rs = dbObject.getResultSet();
        StringBuilder result = new StringBuilder("<table class=\"table table-bordered table-sm\" style=\"width:50%;\"><thead><tr><th>Zilla</th><th>Upazila</th><th>Total person for dbdownload</th><th>Person with No heartbeat</th></tr></thead><tbody>");

        try {
            while (rs.next()) {

                Map<String, String> providerMap;
                String dist = rs.getString(1);
                String upz = rs.getString(2);
                String zillaname = rs.getString(3);
                String upzName = rs.getString(4);

                System.out.println(zillaname + upz);

                //get providerlist that we need to download
                String prov_sql = SynchronizationIssues.getSql(dist);
                providerMap = GetResultSet.getFinalResult(prov_sql, dist, upz, "14", "true");
                int count = providerMap.size();
                //get no heartbeat count
                int heartbeatcount = 0;
                if(count>0) {
                    for (Map.Entry<String, String> provider : providerMap.entrySet()) {
                        String providerid = provider.getKey();
                        String provName = provider.getValue();
                        if(provName.split(",").length>1)
                            heartbeatcount++;


                    }
                    result.append("<tr><td>" + zillaname +"("+dist+")"+ "</td><td>" + upzName+"("+upz+")" + "</td><td>" + count + "</td><td>" + heartbeatcount + "</td></tr>");
                }
            }

            result.append("</tbody></table>");
            out.print(result);



        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
