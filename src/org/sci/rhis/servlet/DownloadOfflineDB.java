package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.DownloadOfflineSqliteDB;
import org.sci.rhis.report.SymChannelCreate;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DownloadOfflineDB extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {

            JSONObject downloadInfo;

            downloadInfo = new JSONObject(request.getParameter("downloadInfo"));
            System.out.println(downloadInfo);

            boolean download = DownloadOfflineSqliteDB.download(downloadInfo);

            if(download==true) {
                String filePath = "/home/anik/sqlite_db_creation/" + downloadInfo.getString("dbName") + ".zip";
                File downloadFile = new File(filePath);
                FileInputStream inStream = new FileInputStream(downloadFile);

                // if you want to use a relative path to context root:
                String relativePath = getServletContext().getRealPath("");
                System.out.println("relativePath = " + relativePath);

                // obtains ServletContext
                ServletContext context = getServletContext();

                // gets MIME type of the file
                String mimeType = context.getMimeType(filePath);
                if (mimeType == null) {
                    // set to binary type if MIME mapping not found
                    mimeType = "application/octet-stream";
                }
                System.out.println("MIME type: " + mimeType);

                // modifies response
                response.setContentType(mimeType);
                response.setContentLength((int) downloadFile.length());

                // forces download
                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
                response.setHeader(headerKey, headerValue);

                // obtains response's output stream
                OutputStream outStream = response.getOutputStream();

                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }

                inStream.close();
                outStream.close();
            }
            else{
                PrintWriter out = response.getWriter();
                out.print(download);
                out.close();
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
