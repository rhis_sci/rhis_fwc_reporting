package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.MIS3SubmitInfo;

public class MIS3Submission extends HttpServlet {
	
	public boolean successSubmission;

	public void doGet(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		
		
		try {
			
			PrintWriter out = response.getWriter();

			String url_parameter[] = request.getQueryString().split("&");
			byte[] valueDecoded = Base64.getDecoder().decode(url_parameter[0]);
			String getValue = new String(valueDecoded);//StringUtils.newStringUtf8(Base64.decodeBase64(request.getQueryString()));

			if(url_parameter.length>1)
				getValue += "&"+url_parameter[1];
			
			System.out.println(getValue);
			
			String[] parameter = getValue.split("&");
			
			JSONObject reportCred = new JSONObject();
			
			for(int i=0;i<parameter.length;i++)
			{
				
				String[] parameterValue = parameter[i].split("=");
				if(parameterValue.length>1)
					reportCred.put(parameterValue[0], parameterValue[1]);
				else
					reportCred.put(parameterValue[0], "");
			}
			
//			if(reportCred.has("submissionStatus"))
//				request.setAttribute("submissionStatus", "0");
			
			System.out.println(reportCred);
			
			successSubmission = MIS3SubmitInfo.submitMIS3(reportCred);
			out.print(successSubmission);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
