package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sci.rhis.report.ReportResultSearch;
import org.sci.rhis.report.SymChannelCreate;

public class SymmetricDSConfServlet extends HttpServlet {
	
	//public String dropdown;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {
			//System.out.println("ReportServlet");
			PrintWriter out = response.getWriter();
			
			//dropdown = SymChannelCreate.getDropdown();
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/SymmetricDSConf.jsp");
			//request.setAttribute("Dropdown", dropdown);
			requestDispatcher.forward(request, response);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
