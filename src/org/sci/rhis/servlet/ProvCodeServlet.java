package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.report.FacilityInfoEntry;
import org.sci.rhis.report.ProviderInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ProvCodeServlet")
public class ProvCodeServlet extends HttpServlet {


    public static String zillaid;
    public static String providerType;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            JSONObject forminfo;

            forminfo = new JSONObject(request.getParameter("forminfo"));

            JSONObject provider_code = new JSONObject();
            provider_code = ProviderInfo.getProviderCode(forminfo);

            out.print(provider_code);
            out.close();


        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }


}
