package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.model.GetResult;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

public class SynchronizationStatusServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        try {
            JSONObject requestInfo = new JSONObject(request.getParameter("reportInfo"));
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();


            RequestDispatcher requestDispatcher;
            request.setAttribute("type", requestInfo.getString("type"));
            if (requestInfo.has("status")) {
                requestDispatcher = request.getRequestDispatcher("jsp/SynchronizationStatus/viewsynchronizationstatus.jsp");
            } else {
                requestDispatcher = request.getRequestDispatcher("jsp/SynchronizationStatus/synchronizationstatus.jsp");
                //ResultSet rs = GetResult.getResultSet(requestInfo, "provider_sync_status");
                ResultSet rs = GetResult.getResultSet(requestInfo, "web_fn_provider_sync_status");
                request.setAttribute("SYNCStatus", rs);

                JSONObject rs1 = GetResult.getDataSet(requestInfo);
                request.setAttribute("SYNCStatus1", rs1);

            }

            requestDispatcher.forward(request, response);

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
