package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.ProviderInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "changeProviderInfo")
public class changeProviderInfo extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            JSONObject providerInfo = new JSONObject();

            //provValue = new JSONObject(request.getParameter("provfo"));

            JSONObject provValue = new JSONObject();
            provValue = new JSONObject(request.getParameter("provfo"));

            //int providercode =  Integer.parseInt(request.getParameter("providercode"));
            //int zillacode =  Integer.parseInt(request.getParameter("zillaid"));

            int providercode =  provValue.getInt("providercode");
            int zillacode =  provValue.getInt("zillaid");
            int providerType = provValue.getInt("providertype");

            System.out.println("Providercode=" + providercode);
            System.out.println("Zillacode=" + zillacode);
            System.out.println("ProviderType=" + providerType);

            providerInfo = ProviderInfo.getProviderEditDetails(providercode, zillacode, providerType);

            //request.setAttribute("providerInfo", providerInfo);

            System.out.println("Providerinfo = " + providerInfo);

            request.setAttribute("jsonString", providerInfo.toString());

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/edit_providerinfo.jsp");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setDateHeader("Expires", 0); // Proxies.
            requestDispatcher.forward(request, response);
            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
