package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Calendar;


public class MeetingMinutes extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response){
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //demo : http://mamoni.net:8080/rhis_fwc_monitoring/meetingminutes?providercode=934094&designation=FWV&category=1&division=30&district=93&upazila=95&union=47

        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();
//            demourl: http://localhost:8080/meetingminutes?

            //Base64.getDecoder().decode(request.getQueryString());
            String getValue = request.getQueryString();
            String[] parameter = getValue.split("&");

            JSONObject reportCred = new JSONObject();

            for(int i=0;i<parameter.length;i++)
            {

                String[] parameterValue = parameter[i].split("=");
                if(parameterValue.length>1)
                    reportCred.put(parameterValue[0], parameterValue[1]);
                else
                    reportCred.put(parameterValue[0], "");

            }

            int division = 0;
            int zilla = 0;
            int upazila = 0;
            int union = 0;
            String providerid = null;
            int category = 0;
            String designation = null;
            if (reportCred.has("division"))
                division = reportCred.getInt("division");
            if (reportCred.has("district"))
                 zilla = reportCred.getInt("district");
            if (reportCred.has("upazila"))
                upazila = reportCred.getInt("upazila");
            if (reportCred.has("union"))
                union = reportCred.getInt("union");
            if (reportCred.has("providercode"))
                providerid = reportCred.getString("providercode");
            if (reportCred.has("designation"))
                designation = reportCred.getString("designation");
            if (reportCred.has("category"))
                category = reportCred.getInt("category");

            //set year and month
            int month = 0;
            int year = 0;
            if(reportCred.has("year") && reportCred.has("month")){
                year = reportCred.getInt("year");
                month = reportCred.getInt("month");
            }else {

                Calendar c = Calendar.getInstance();
                month = c.get(Calendar.MONTH)+1;
                year = c.get(Calendar.YEAR);
            }

//            int month = 12;
//            int year = 2021;
            /**
             * DB Operation
             */
            FacilityDB dbFacility = new FacilityDB();
            DBOperation dbOp = new DBOperation();
            DBInfoHandler dbObject = dbFacility.facilityDBInfo(zilla);


            String query = "select p.itemdes,t.itemdes as meeting_type,meeting_no,meeting_date,meeting_time ,n.is_meeting_minutes_circulate,n.status from notice_master_new n\n" +
                    "left join notice_meeting_place p on p.itemcode = n.place \n" +
                    "left join notice_meeting_type t  using (itemcode) where zillaid= " +zilla+ " and upazilaid ="+ upazila+ "and unionid = "+union+" and n.is_meeting_notice_circulate =  "+1 +
                    "and meeting_year = "+year+" and meeting_month = "+month;

            dbOp.dbExecute(dbObject,query);
            ResultSet rs = dbObject.getResultSet();


            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/meetingminutes/meetingmintues.jsp");
            request.setAttribute("meetingInfo", rs);
            request.setAttribute("providercode", providerid);
            request.setAttribute("designation", designation);
            request.setAttribute("division", division);
            request.setAttribute("zilla", zilla);
            request.setAttribute("upazila", upazila);
            request.setAttribute("union", union);
            request.setAttribute("category", category);
            request.setAttribute("year", year);
            request.setAttribute("month", month);


            requestDispatcher.forward(request, response);


            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
