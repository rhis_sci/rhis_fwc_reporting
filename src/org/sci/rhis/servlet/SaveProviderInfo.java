package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.CommunityInfo;
import org.sci.rhis.report.ProviderInfo;

public class SaveProviderInfo extends HttpServlet {
    public boolean forminfo;
    public String type;
    JSONObject facilityInfo = new JSONObject();

    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        String status;

        try {
            //System.out.println("Report1Servlet");
            PrintWriter out = response.getWriter();

            JSONObject formValue = new JSONObject();

            formValue = new JSONObject(request.getParameter("forminfo"));

            //System.out.println(formValue);

            //report1Search = ReportResultSearch.getSearchResult(reportCred);

            type = formValue.getString("type");

            System.out.print(type);

            switch (type) {

                case "1":
                    forminfo = ProviderInfo.saveProvider(formValue);
                    break;

                case "2":
                    forminfo = ProviderInfo.assignHealthID(formValue);
                    break;

                case "3":
                    forminfo = ProviderInfo.assignAppVersion(formValue);
                    break;

                case "4":
                    forminfo = ProviderInfo.updateProviderRetiredDate(formValue);
                    break;

                case "7":
                    forminfo = ProviderInfo.providerTransfer(formValue);
                    break;

                case "9":
                    forminfo = ProviderInfo.downloadUploadStatus(formValue);
                    break;

                case "10":
                    forminfo = ProviderInfo.additionalSDP(formValue);
                    break;
                case "11":
                    forminfo = ProviderInfo.saveProvider(formValue);
                    break;
                case "12":
                    forminfo = ProviderInfo.setManageProviderInfo(formValue);
                    break;
            }

//			if(type.equals("6"))
//				out.print(facilityInfo);
//			else{
            if (forminfo == true)
                status = "1";
            else
                status = "0";

            out.print(status);
            //}

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
