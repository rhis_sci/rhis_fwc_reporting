package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.login.LoginDao;
import org.sci.rhis.util.AES;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

public class UnifiedLoginServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response){
        /**
         * url format should be encoded
         * real demo url:http://localhost:8081/unifiedLoginStop?uid=269122&upass=**
         * format url to send(encoded param):http://localhost:8081/unifiedLoginStop?UVF+Z70yu8K1G6d4M08JqkIVO2tvl3P6ptSviXxHJrQ=
         */
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        try {
            PrintWriter out = response.getWriter();

            String getValue = AES.decrypt(request.getQueryString(), "rhis_unified_monitoring");

            //todo: need to do this generic type to pass the "getValue" here
            JSONObject loginCred = new JSONObject("{\"uid\":269122,\"upass\":Shahed#@1}"); //test

//            String[] parameter = getValue.split("&");
//
//            for(int i=0;i<parameter.length;i++)
//            {
//
//                String[] parameterValue = parameter[i].split("=");
//                if(parameterValue.length>1)
//                    loginCred.put(parameterValue[0], parameterValue[1]);
//                else
//                    loginCred.put(parameterValue[0], "");
//
//                //System.out.println(parameterValue[1]);
//            }

            //loginCred = new JSONObject(request.getParameter("loginInfo"));

            if(LoginDao.validateUnified(loginCred)){
                request.setAttribute("uName", LoginDao.pName);
                String uID = LoginDao.providerID;
                request.setAttribute("uID", LoginDao.providerID);
                request.setAttribute("uType", LoginDao.pType);
                request.setAttribute("unifiedLogin", "1");
                request.setAttribute("mis3_approver", 0);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/unifiedLogin.jsp");
                requestDispatcher.forward(request, response);

                HttpSession session=request.getSession();
                session.setAttribute("uName",LoginDao.pName);
                session.setAttribute("uID",LoginDao.pID);
            }
            else{
                out.print("false");
            }
            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}