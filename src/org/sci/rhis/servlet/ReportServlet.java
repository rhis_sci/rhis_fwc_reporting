package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.FacilityInfoEntry;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class ReportServlet extends HttpServlet {

    public static String type;
    public static String getutype;
    public static int utype;

    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            JSONObject reportCred;

            reportCred = new JSONObject(request.getParameter("reportInfo"));

            type = reportCred.getString("type");
            if(reportCred.has("utype")) {
                getutype = reportCred.getString("utype");
                utype = Integer.parseInt(getutype);

            }

            RequestDispatcher requestDispatcher = null;
            //call specific jsp regarding each section
            requestDispatcher = callToSpecificJsp(request,requestDispatcher);

            request.setAttribute("type", type);
            request.setAttribute("uType", utype);
            if (type.equals("4") || type.equals("21")) {
                JSONObject facility_type = new JSONObject();
                facility_type = FacilityInfoEntry.getFacilityType();
                request.setAttribute("facilityType", facility_type);
            } else
                request.setAttribute("facilityType", null);
            requestDispatcher.forward(request, response);

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RequestDispatcher callToSpecificJsp(HttpServletRequest request,RequestDispatcher requestDispatcher){
        if (type.equals("1")) { // provider performance
            requestDispatcher = request.getRequestDispatcher("jsp/providerperformance/providerPerformance.jsp");
        } else if (type.equals("3")) { // service statistics
            requestDispatcher = request.getRequestDispatcher("jsp/servicestatistics/serviceStatistics.jsp");
        } else if (type.equals("4")) { // mis3Report
            requestDispatcher = request.getRequestDispatcher("jsp/mis3report/mis3Report.jsp");
        } else if (type.equals("26") || type.equals("27")) { // mis3Report submission
            requestDispatcher = request.getRequestDispatcher("jsp/mis3submissioninformation/mis3SubmissionInfo.jsp");
        } else if (type.equals("25")) { // pregnant women list
            requestDispatcher = request.getRequestDispatcher("jsp/pregnantwomenlist/pregnantWomenList.jsp");
        } else if(type.equals("5")){  //upazila performnce by month
            requestDispatcher = request.getRequestDispatcher("jsp/upazilaperformance/upazilaPerformanceByMonth.jsp");
        }else if(type.equals("9")){  //upazila performnce by indicator
            requestDispatcher = request.getRequestDispatcher("jsp/upazilaperformancebyselectedindicator/indicatorPerformance.jsp");
        }else if(type.equals("22")){  //disease monitoring
            requestDispatcher = request.getRequestDispatcher("jsp/diseasemonitoring/diseaseMonitoring.jsp");
        }else if(type.equals("28")){  //qed indicator
            requestDispatcher = request.getRequestDispatcher("jsp/QEDindicator/QEDIndicator.jsp");
        }else if(type.equals("29")){  //flu like symptom(report)
            requestDispatcher = request.getRequestDispatcher("jsp/QEDindicator/QEDIndicator.jsp");//same interface for this
        }else if(type.equals("30")){  //flu like symptom(report)
            requestDispatcher = request.getRequestDispatcher("jsp/flulikesymptom/fluLikeSymtomTrend.jsp");
        }else if(type.equals("31")){  //dashboard of client
            requestDispatcher = request.getRequestDispatcher("jsp/clientinfo/clientInfo.jsp");
        }else if(type.equals("newborn")){  //newborn report
            requestDispatcher = request.getRequestDispatcher("jsp/newborn/viewNewborn.jsp");
        }else{
            requestDispatcher = request.getRequestDispatcher("jsp/reportInfo.jsp");
        }

        return requestDispatcher;
    }
}
