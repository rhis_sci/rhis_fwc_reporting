package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.FacilityInfoEntry;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TransferProvider extends HttpServlet {


    public void doPost(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {
            PrintWriter out = response.getWriter();

            JSONObject facility_type = new JSONObject();
            facility_type = FacilityInfoEntry.getFacilityType();

            JSONObject typeInfo;

            typeInfo = new JSONObject(request.getParameter("typeInfo"));

            String type = typeInfo.getString("type");

            System.out.println(typeInfo);
            System.out.println(type);

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/providerTransferAdditionalSDP.jsp");
            request.setAttribute("facilityType", facility_type);
            request.setAttribute("type", type);
            requestDispatcher.forward(request, response);

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
