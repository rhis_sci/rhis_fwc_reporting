package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.report.ProviderInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AddCSBA extends HttpServlet {
    public boolean status;

    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        try {

            PrintWriter out = response.getWriter();
            JSONObject info;
            info = new JSONObject(request.getParameter("provInfo"));
            status = ProviderInfo.addCSV(info);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
