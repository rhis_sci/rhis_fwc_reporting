package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import org.json.JSONObject;
import org.sci.rhis.login.LoginDao;
import org.sci.rhis.util.AES;

/**
 * @author sabah.mugab
 * @created July, 2015
 */
@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
		
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		JSONObject loginCred;
		
		try {
			PrintWriter out = response.getWriter();
			
			loginCred = new JSONObject(request.getParameter("loginInfo"));
			
			if(LoginDao.validate(loginCred)){
				if(loginCred.getString("client").equals("1")){
					request.setAttribute("uName", LoginDao.pName);
					request.setAttribute("uID", LoginDao.pID);
					request.setAttribute("faciltyName", LoginDao.facilityName);
					request.setAttribute("uType", LoginDao.pType);
					request.setAttribute("unifiedLogin", "0");
					request.setAttribute("mis3_approver", LoginDao.mis3_approver);

					Cookie cookieInfo = null;
//					cookieInfo = new Cookie("username", "");
//					response.addCookie(cookieInfo);
//					cookieInfo = new Cookie("userpass", "");
//					response.addCookie(cookieInfo);
					Cookie[] cookies = request.getCookies();
					int c = 0;
					String username = "";
//					String userpass = "";

					if(cookies != null) {
						for (Cookie cookie : cookies) {
							username = cookie.getValue().trim();
//						userpass = cookie.getValue().trim();
							if (username.equals("username") && username.isEmpty()) {
								c = 1;
							}
						}
					}

					if(c!=1 && loginCred.getString("rememberme").equals("1")) {
						cookieInfo = new Cookie("username", loginCred.getString("uid"));
						response.addCookie(cookieInfo);
						cookieInfo = new Cookie("userpass", loginCred.getString("upass"));
						response.addCookie(cookieInfo);
					}

//					JSONObject community_bypass_info = new JSONObject();
//					community_bypass_info.put("uid", loginCred.getString("uid"));
//					community_bypass_info.put("upass", LoginDao.getCommunityUserPass(loginCred));
//					String get_bypass_url_info = AES.encrypt(community_bypass_info.toString(), "rhis_unified_monitoring");
//					get_bypass_url_info = get_bypass_url_info.replace("+", "%2B");
////					System.out.println(AES.decrypt(get_bypass_url_info, "rhis_unified_monitoring"));
//					request.setAttribute("bypass_url_info", get_bypass_url_info);

					RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/home.jsp");
				    requestDispatcher.forward(request, response);
				    
				    HttpSession session=request.getSession();  
			        session.setAttribute("uName",LoginDao.pName);
			        session.setAttribute("uID",LoginDao.pID);
				}
				else if(loginCred.getString("client").equals("2")){
					JSONObject providerInfo = new JSONObject();
					
					providerInfo.put("ProvName", LoginDao.pName);
					providerInfo.put("ProvCode", LoginDao.pID);
					providerInfo.put("FacilityName", LoginDao.facilityName);
					providerInfo.put("ProvType", LoginDao.pType);
					providerInfo.put("loginStatus", true);
					providerInfo.put("client", LoginDao.client);
					providerInfo.put("server", LoginDao.server);
					providerInfo.put("id", LoginDao.id);
					
					
					out.print(providerInfo);
				}				
			}
			else{
				if(loginCred.getString("client").equals("1")){
					out.print("false");
				}
				else if(loginCred.getString("client").equals("2")){
					JSONObject providerInfo = new JSONObject();
					
					providerInfo.put("ProvName", "");
					providerInfo.put("ProvCode", "");
					providerInfo.put("FacilityName", "");
					providerInfo.put("ProvType", "");
					providerInfo.put("loginStatus", false);
					
					out.print(providerInfo);
				}
			}
			out.close();
		} 
		catch (ParseException e) {
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}
}