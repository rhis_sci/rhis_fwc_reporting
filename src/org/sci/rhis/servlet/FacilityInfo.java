package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.GetFacilityInfo;

public class FacilityInfo extends HttpServlet{
	JSONObject facilityInfo = new JSONObject();

	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		try {
			PrintWriter out = response.getWriter();
			
			JSONObject formValue = new JSONObject();
			
			formValue = new JSONObject(request.getParameter("forminfo"));
			
			System.out.println("Formvalue="+formValue);
			
			facilityInfo = GetFacilityInfo.getFacilityInfo(formValue);
			out.print(facilityInfo);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
