package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.ReportResultSearch;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Report1Servlet extends HttpServlet{
	
	public String report1Search = "";
	public Integer DateOption = 0;

	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {
			//System.out.println("Report1Servlet");
			PrintWriter out = response.getWriter();

			
			JSONObject reportCred;

			String[] table_graph = null;

			reportCred = new JSONObject(request.getParameter("report1Info"));
			
			//report1Search = ReportResultSearch.getSearchResult(reportCred);
//			System.out.println("XXXXXXXXXXXXXXXXXX");
			//System.out.println("Shahed");
			System.out.println(reportCred);
			report1Search = ReportResultSearch.getSearchResult(reportCred);
			System.out.println("Advance Search Servlet: " + report1Search);
			RequestDispatcher requestDispatcher;

			String type = "";
			int reportYear = 0;
			String pickDate = "";
			int pickYear = 0;

			if(reportCred.has("type"))
				type = reportCred.getString("type");

			/*
			if(reportCred.has("report1_year"))
				//reportYear = reportCred.getString("report1_year");
				reportYear = Integer.parseInt(reportCred.getString("report1_year"));
            */

			/*
			if(reportCred.has("report1_month")) {
				pickDate = reportCred.getString("report1_month");

				if(!pickDate.isEmpty())
				{
					String[] dateStr = pickDate.split("-", 2);
					pickYear = Integer.parseInt(dateStr[0]);
					System.out.println(pickYear);
				}

			}
			*/
			String date_type = "";
			if(reportCred.has("report1_dateType")) {
				date_type = reportCred.getString("report1_dateType");

				if (date_type.equals("1")) {
					String monthSelect = reportCred.getString("report1_month");
					String[] date = monthSelect.split("-");

					String dhis2_month = date[1];
					String dhis2_year = date[0];
					DateOption = Integer.parseInt(dhis2_year);
				} else if (date_type.equals("3")) {
					String yearSelect = reportCred.getString("report1_year");
					String start_date = yearSelect + "-01-01";
					String end_date = yearSelect + "-12-31";

					String[] edate = end_date.split("-");
					System.out.println("End Date for Testing = " + edate[0]);
					DateOption = Integer.parseInt(edate[0]);
				} else {
					String start_date = reportCred.getString("report1_start_date");
					String end_date = reportCred.getString("report1_end_date");

					String[] date = end_date.split("-");

					//bangla_month = EnglishtoBangla.ConvertMonth(date[1]);
					//bangla_year = EnglishtoBangla.ConvertNumberToBangla(date[0]);

					DateOption = Integer.parseInt(date[0]);
				}
			}


			//System.out.println("Before Page loading PickYear = "+ pickYear);

			if(type.equals("18")){
				requestDispatcher = request.getRequestDispatcher("jsp/mnc_report.jsp");
			}else if(type.equals("16")){
				requestDispatcher = request.getRequestDispatcher("jsp/pill_condom_report.jsp");
			}else if(type.equals("17")){
				requestDispatcher = request.getRequestDispatcher("jsp/gp_report.jsp");
			}else if(type.equals("19")){
				requestDispatcher = request.getRequestDispatcher("jsp/injectable_register.jsp");
			}else if(type.equals("20")){
				requestDispatcher = request.getRequestDispatcher("jsp/iud_register.jsp");
			}else if(type.equals("21")){
				requestDispatcher = request.getRequestDispatcher("jsp/implant_register.jsp");
			}else if(type.equals("25")){
				request.setAttribute("zilla", reportCred.get("report1_zilla"));
				request.setAttribute("upazila", reportCred.get("report1_upazila"));
				requestDispatcher = request.getRequestDispatcher("jsp/pregnantwomenlist/pregwomenReport.jsp");
			}//pregwomenReport
			else if((type.equals("4")) && (DateOption > 2018))
			{
				System.out.println("After Condition PickYear = "+ pickYear);
				requestDispatcher = request.getRequestDispatcher("jsp/NewMIS3Report.jsp");
			}
			else if(type.equals("26") || type.equals("27")){
				requestDispatcher = request.getRequestDispatcher("jsp/report_submission_info.jsp");
			}
			else if(type.equals("28")){
				request.setAttribute("Result", report1Search);
				requestDispatcher = request.getRequestDispatcher("jsp/report1Table.jsp");
			}
			else {
				requestDispatcher = request.getRequestDispatcher("jsp/report1Table.jsp");
			}
			//RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/report1Table.jsp");
			if(reportCred.has("type")){
				if (reportCred.get("type").equals("3")){
					table_graph = report1Search.split(" table/graph/map ");
					request.setAttribute("Result", table_graph[0]);
					request.setAttribute("Graph", table_graph[1]);
					request.setAttribute("Map", table_graph[2]);
					request.setAttribute("Division", table_graph[3]); // @Author: Evana, @Date:03/07/2019
				}
				else if (reportCred.get("type").equals("1") || reportCred.get("type").equals("22") || reportCred.get("type").equals("11") || reportCred.get("type").equals("30")){
					table_graph = report1Search.split(" table/graph ");
					request.setAttribute("Result", table_graph[0]);
					request.setAttribute("Graph", table_graph[1]);
				}
				else
					request.setAttribute("Result", report1Search);
			}
			else
				request.setAttribute("Result", report1Search);

			if(reportCred.has("type"))
				request.setAttribute("type", reportCred.getString("type"));
			if(reportCred.has("methodType"))
				request.setAttribute("methodType", reportCred.getString("methodType"));
			else
				request.setAttribute("methodType", "");
			requestDispatcher.forward(request, response);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}


}
