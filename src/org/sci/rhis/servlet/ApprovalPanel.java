package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.MIS3SubmitInfo;
import org.sci.rhis.report.SatelitePlanningSubmitInfo;

public class ApprovalPanel extends HttpServlet{
	
	public static String type;
	public static String getutype;
	public static int utype;

	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {
			//System.out.println("ReportServlet");
			PrintWriter out = response.getWriter();
			
			JSONObject infoApproval;
			
			infoApproval = new JSONObject(request.getParameter("infoApproval"));

			type = infoApproval.getString("type");
			getutype = infoApproval.getString("utype");
			utype = Integer.parseInt(getutype);
			System.out.println(infoApproval.getString("supervisorId"));

			String approval_list = "";


			if(type.equals("1"))
				approval_list = MIS3SubmitInfo.getApprovalList(infoApproval);
			else if(type.equals("3")) {
                if(infoApproval.has("approve_status")) {
                   SatelitePlanningSubmitInfo.updateApprovalStatus(infoApproval);
                }else{
                    approval_list = SatelitePlanningSubmitInfo.getApprovalList(infoApproval);
                }
            }
			System.out.println(request.getAttribute("approve_status"));
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/approvalPanel.jsp");
			request.setAttribute("type", type);
			request.setAttribute("uType", utype);

			if(infoApproval.has("month"))
				request.setAttribute("month", infoApproval.getString("month"));
			else
				request.setAttribute("month", "");

			if(type.equals("1") || type.equals("3"))
				request.setAttribute("approvalList", approval_list);
			requestDispatcher.forward(request, response);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
