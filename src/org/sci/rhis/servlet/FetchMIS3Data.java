package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.FeverColdCoughReport;
import org.sci.rhis.report.MIS3Report;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

@WebServlet(name = "fetchMIS3data")
public class FetchMIS3Data extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response){
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
        //sampleurl : http://mamoni.net:8080/rhis_fwc_monitoring/flulikesymptom?emlsbGFpZD0zNiZyZXBvcnRfdHlwZTQ1a2Z3W1mbHVfbGlrZV9zeW1wdG9t

        //sample urlparam: zillaid=36&report_type=flu_like_symptom&startDate=2021-05-01&endDate=2021-06-01

        //http://localhost:8081/flulikesymptom?emlsbGFpZD0zNiZyZXBvcnRfdHlwZTQ1a2Z3W1mbHVfbGlrZV9zeW1wdG9tJnN0YXJ0RGF0ZT0yMDIwLTA1LTI4JmVuZERhdGU9MjAyMC0wNS0zMQ


        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            String getSubStringValue = request.getQueryString().substring(30);
            String getURLValue = request.getQueryString().substring(0, 30) + getSubStringValue.substring(7);

            String salt = getSubStringValue.substring(0, 7);

            if (salt.equals("Q1a2Z3W")) {

                byte[] valueDecoded = Base64.getDecoder().decode(getURLValue);
                String getValue = new String(valueDecoded);//StringUtils.newStringUtf8(Base64.decodeBase64(request.getQueryString()));

                System.out.println(getValue);

                String[] parameter = getValue.split("&");

                JSONObject reportCred = new JSONObject();

                for (int i = 0; i < parameter.length; i++) {
                    String[] parameterValue = parameter[i].split("=");
                    reportCred.put(parameterValue[0], parameterValue[1]);
                }

//                JSONObject result = new JSONObject();
                String result = "";

//                if (reportCred.getString("report_type").equals("flu_like_symptom"))
                    result =  MIS3Report.getResultSet(reportCred);
//                else
//                    result = null;

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                out.print(result.toString());

            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
