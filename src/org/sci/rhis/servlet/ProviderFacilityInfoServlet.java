package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.GetFacilityInfo;
import org.sci.rhis.report.ProviderInfo;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ProviderFacilityInfoServlet extends HttpServlet{
	
	//private JSONObject 
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		try {
			PrintWriter out = response.getWriter();

			JSONObject formValue = new JSONObject();

			formValue = new JSONObject(request.getParameter("forminfo"));

			System.out.println(formValue);

			JSONObject providerInfo = new JSONObject();
			if(formValue.has("providerListType")){
				if(formValue.getString("providerListType").equals("2"))
					providerInfo = ProviderInfo.getProviderInfoASDP(formValue);
				else
					providerInfo = ProviderInfo.getProviderInfo(formValue);
			}
			else
				providerInfo = ProviderInfo.getProviderInfo(formValue);

			out.print(providerInfo);

			out.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
