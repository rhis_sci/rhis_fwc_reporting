package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.login.LoginChange;
import org.sci.rhis.report.GetFacilityInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "FacilityUpdateSubmit")
public class FacilityUpdateSubmit extends HttpServlet {

    public boolean forminfo;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        String status;
        try {
            //System.out.println("Report1Servlet");
            PrintWriter out = response.getWriter();

            JSONObject formValue = new JSONObject();

            formValue = new JSONObject(request.getParameter("forminfo"));

            System.out.println("Formvalue="+formValue);

            forminfo =  GetFacilityInfo.updateFacilityInfo(formValue);

            if(forminfo == true)
                status = "1";
            else
                status = "0";

            out.print(status);
            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }


    }


}
