package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.MIS3SubmitInfo;
import org.sci.rhis.report.ProviderInfo;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MIS3DataEntry extends HttpServlet {
    public boolean entryStatus;

    public void doPost(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        String status;

        try {
            //System.out.println("Report1Servlet");
            PrintWriter out = response.getWriter();

            JSONObject entryInfo = new JSONObject();

            entryInfo = new JSONObject(request.getParameter("entryInfo"));

            System.out.println(entryInfo);

            entryStatus = MIS3SubmitInfo.mis3DataEntry(entryInfo);

            if(entryStatus == true)
                status = "1";
            else
                status = "0";

            out.print(status);
            //}

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
