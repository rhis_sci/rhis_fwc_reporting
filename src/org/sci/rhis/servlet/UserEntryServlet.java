package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.FacilityInfoEntry;


public class UserEntryServlet extends HttpServlet{
	
	public static String getutype;
	public static String gettype;
	public static int utype;
	public static int type;

	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

		try {
			//System.out.println("ReportServlet");
			PrintWriter out = response.getWriter();
			
			JSONObject typeInfo;
			
			typeInfo = new JSONObject(request.getParameter("typeInfo"));
			
			getutype = typeInfo.getString("utype");
			utype = Integer.parseInt(getutype);
			gettype = typeInfo.getString("type");

			if(gettype.equals("addcsba")){
				gettype = "13";
			}

			type = Integer.parseInt(gettype);
			JSONObject facility_type = new JSONObject();
			facility_type = FacilityInfoEntry.getFacilityType();
			
			//System.out.println(gettype);

			if((type==11)||(type == 26))
			{
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/generalUserEntry.jsp");
				request.setAttribute("utype", utype);
				request.setAttribute("type", type);
				request.setAttribute("facilityType", facility_type);
				requestDispatcher.forward(request, response);

			}if(type==13) {

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/csbainfo/viewcsbainfo.jsp");
				request.setAttribute("type", "addcsba");
				requestDispatcher.forward(request, response);

			} else{
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/userEntry.jsp");
				request.setAttribute("utype", utype);
				request.setAttribute("type", type);
				request.setAttribute("facilityType", facility_type);
				requestDispatcher.forward(request, response);
			}

			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}
}
