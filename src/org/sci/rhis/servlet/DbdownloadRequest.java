package org.sci.rhis.servlet;

import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBSchemaInfo;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.report.GetResultSet;
import org.sci.rhis.report.ProviderInfo;
import org.sci.rhis.report.SynchronizationIssues;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbdownloadRequest extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        /**demo: http://localhost:8081/dbdownload?zilla=36&upazila=68
         * you can check the url from git bash with:
         * curl -H "Authorization: Bearer pr0VidEr#985M@m0ni" 'http://localhost:8081/dbdownload?zilla=29&upazila=62&dbdownload=0'
         */

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
        try {
            if (request.getHeader("Authorization").equals("Bearer pr0VidEr#985M@m0ni")) {
                PrintWriter out = response.getWriter();
                String partofUrl = request.getQueryString();

                Pattern p = Pattern.compile("(\\d+)&\\w+=(\\d+)&\\w+=(\\d+)");
                Matcher match = p.matcher(partofUrl);
                String dist = "";
                String upz = "";
                String dbdownload_flag = "";
                if (match.find()) {
                    dist = match.group(1);
                    upz = match.group(2);
                    dbdownload_flag = match.group(3);
                }

                //get view sync status sql
                String sql = SynchronizationIssues.getSql(dist);

                //get providerlist that we need to download
                Map<String, String> providerMap;
                providerMap = GetResultSet.getFinalResult(sql, dist, upz, "14", "true");
                String result = "";
                if(dbdownload_flag.equals("0")){
                     result = "\n== Selected provider for db download == \n";

                }else {
                     result = "\n== DB Download has been set for the provider (" + LocalDate.now() + ") == \n";
                }

                if (providerMap.size() > 0) {
                    for (Map.Entry<String, String> provider : providerMap.entrySet()) {
                        String providerid = provider.getKey();
                        String provName = provider.getValue();
                        result += "Name:" + provName + ", Providerid:" + providerid + ", zilla:" + dist + ", upazila:" + upz + "\n";


                        if (dbdownload_flag.equals("1")) {
                            dbDownload_providerwise(dist, upz, providerid);
                            insertDataintoDBtable(dist, upz, providerid, provName,2);
                        }
                    }
                } else {
                    result = "\n==No provider found for Dbdownload (" + LocalDate.now() + ")==\n zilla" + dist + ",upazila: " + upz;
                }

                out.print(result);

                out.close();
            } else {
                System.out.println("Please send correct authorization token");
            }
        } catch (IOException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void dbDownload_providerwise(String zilla, String upazila, String providerCode) {
        DBSchemaInfo table_schema = new DBSchemaInfo(zilla);
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(zilla);

        //update node details table
        String update_set_sql = table_schema.getColumn("", "node_details_dbdownloadstatus", new String[]{"1"}, "=") + "," + table_schema.getColumn("", "node_details_dbsyncrequest", new String[]{"1"}, "=");
        String sql_change_db_status = "update " + table_schema.getTable("table_node_details") + " set " + update_set_sql + " where " + table_schema.getColumn("", "node_details_provcode") + "=" + providerCode;
        dbOp.dbExecuteUpdate(dbObject, sql_change_db_status);

        //delete all sym tables referemce
        ProviderInfo.deleteSymTables_providerwise(dbOp, zilla, upazila, providerCode);
    }

    public static void insertDataintoDBtable(String zilla, String upazila, String providerCode, String provName, int status) {

        /**
         * status = 1(manual dbdownload from monitoring tool)
         * status = 2 (automatic dbdownload)
         * status = 3 (resolve_sym_context)
         */
        FacilityDB dbFacility = new FacilityDB();
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = dbFacility.centralMonitorngInfo();

        String sql = "insert into sync_issue_resolving_history (providerid,provname,zilla,upazila,downloaddate,action)" +
                " values(" + providerCode + ",'" + provName + "'," + zilla + "," + upazila + "," + "'" + LocalDateTime.now() + "' ,"+status+" )";
//                     " on conflict("+providerCode+")";

        dbOp.dbStatementExecute(dbObject, sql);
    }
}
