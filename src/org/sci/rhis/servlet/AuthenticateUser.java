package org.sci.rhis.servlet;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.Key;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;

import static org.sci.rhis.util.Helper.readInput;

public class AuthenticateUser extends HttpServlet {

    String secret = "mamoni";


    public void doPost(HttpServletRequest request, HttpServletResponse response)  {

        /**demo: http://localhost:8081/pregwomeninfo?zilla=36&nid=5535642267
         * you can check the url from git bash with:
         * curl -H "Authorization: Bearer pr0VidEr#985M@m0ni" 'http://localhost:8081/pregwomeninfo?zilla=4&nid=5512452433
         */

        String uid = null;
        String pass = null;
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        //part for sending req from postman
        String body = null;
        try {
            PrintWriter out = response.getWriter();
            body = readInput(request);
            JSONObject input = new JSONObject(body);
            uid = input.getString("username");
            pass = input.getString("password");

            String sql =  "select * from providerdb where providerid="+uid+" and provpass='"+pass+"'";
            DBOperation dbOp = new DBOperation();
            DBInfoHandler dbObject = new FacilityDB().facilityDBInfo();

            ResultSet rs = dbOp.dbExecute(dbObject, sql).getResultSet();

            String token = null;
            if(rs.next()){
                 token =  createToken(uid,pass);


            }

            out.println(token);

            out.close();


        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public String createToken(String uid,String pass){

        Key hmacKey = new SecretKeySpec(Base64.getDecoder().decode(secret),
                SignatureAlgorithm.HS256.getJcaName());

        Instant now = Instant.now();
        String jwtToken = Jwts.builder()
//                .claim("uid", uid)
//                .claim("pass", pass)
                .setSubject("Authorized")
//                .setId(UUID.randomUUID().toString())
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(5l, ChronoUnit.MINUTES)))
//                .setExpiration(new Date(System.currentTimeMillis() + 5*60*60*1000))
                .signWith(SignatureAlgorithm.HS256,secret)
                .compact();

        return jwtToken;

    }



}
