package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.ReportResultSearch;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

public class DataForDHIS2 extends HttpServlet {

    public String report1Search;

    public void doGet(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");



        try {

            PrintWriter out = response.getWriter();

            byte[] valueDecoded = Base64.getDecoder().decode(request.getQueryString());
            String getValue = new String(valueDecoded);//StringUtils.newStringUtf8(Base64.decodeBase64(request.getQueryString()));

            System.out.println(getValue);

            String[] parameter = getValue.split("&");

            JSONObject reportCred = new JSONObject();

            for(int i=0;i<parameter.length;i++)
            {

                String[] parameterValue = parameter[i].split("=");
                if(parameterValue.length>1)
                    reportCred.put(parameterValue[0], parameterValue[1]);
                else
                    reportCred.put(parameterValue[0], "");

                //System.out.println(parameterValue[1]);
            }

            if(!reportCred.has("providerId"))
                reportCred.put("providerId", "");

            if(!reportCred.has("csba"))
                reportCred.put("csba", "2");

            if(!reportCred.has("mis3ViewType"))
                reportCred.put("mis3ViewType", "");

            if(!reportCred.has("mis3Submit"))
                reportCred.put("mis3Submit", "0");

            if(!reportCred.has("mis3ViewType"))
                reportCred.put("mis3ViewType", "2");

            if(!reportCred.has("jsonRequest"))
                reportCred.put("jsonRequest", "0");

            reportCred.put("type", "4");
            reportCred.put("report1_dateType", "2");

            //System.out.println(reportCred);

            System.out.println(reportCred);

            report1Search = ReportResultSearch.getSearchResult(reportCred);


            JSONObject reportResponse = new JSONObject(report1Search);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(reportResponse.toString());

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

}
