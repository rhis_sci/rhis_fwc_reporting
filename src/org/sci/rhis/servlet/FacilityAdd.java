package org.sci.rhis.servlet;

import java.io.*;
import java.net.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.sci.rhis.report.FacilityInfoEntry;

public class FacilityAdd extends HttpServlet {

    private static final int READ_TIMEOUT = 15000;
    private static final int CONNECTION_TIMEOUT = 15000;
    public static String getutype;
    public static String gettype;
    public static int utype;
    public static int type;

    public boolean forminfo;

    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        String status;

        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            if (request.getParameterMap().containsKey("typeInfo")) {
                JSONObject typeInfo = new JSONObject();

                typeInfo = new JSONObject(request.getParameter("typeInfo"));

                getutype = typeInfo.getString("utype");
                utype = Integer.parseInt(getutype);
                gettype = typeInfo.getString("type");
                type = Integer.parseInt(gettype);

                System.out.println("Type=" + type);

                JSONObject facility_type = new JSONObject();

                facility_type = FacilityInfoEntry.getFacilityType();

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/userEntry.jsp");
                request.setAttribute("utype", utype);
                request.setAttribute("type", type);
                request.setAttribute("facilityType", facility_type);
                requestDispatcher.forward(request, response);
            } else if (request.getParameterMap().containsKey("forminfo")) {

                JSONObject formValue = new JSONObject();

                formValue = new JSONObject(request.getParameter("forminfo"));


                forminfo = FacilityInfoEntry.saveFacilityInfo(formValue);

                if (forminfo == true) {
                    status = "1";
                    callAPIOfFacilityAssesment(formValue);
                } else
                    status = "0";

                out.print(status);
            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void callAPIOfFacilityAssesment(JSONObject formValue) {
        String tokenValue = "e040cb79944b0c6e6da7862ea2266243";
        httpUrlCall("http://monitoring.mamoni.info/api/add_facility", formValue.toString(), tokenValue);
    }

    private static void httpUrlCall(String uri, String jsonString, String tokenValue) {

        try {
            String data = jsonString;
            StringBuilder result = new StringBuilder();
            //result.append("&data="+data);
            String urlParameters = "token=" + tokenValue + "&data=" + jsonString;
            if (urlParameters.contains(" "))
                urlParameters = urlParameters.replace(" ", "%20");


            URL url = new URL(uri + "?" + urlParameters);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(5000);
            conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
            conn.addRequestProperty("User-Agent", "Mozilla");
            conn.addRequestProperty("Referer", "google.com");

            System.out.println("Request URL ... " + url);

            boolean redirect = false;

            // normally, 3xx is redirect
            int status = conn.getResponseCode();
            if (status != HttpURLConnection.HTTP_OK) {
                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                        || status == HttpURLConnection.HTTP_MOVED_PERM
                        || status == HttpURLConnection.HTTP_SEE_OTHER)
                    redirect = true;
            }

            System.out.println("Response Code ... " + status);

            if (redirect) {

                // get redirect url from "location" header field
                String newUrl = conn.getHeaderField("Location");

                // get the cookie if need, for login
                String cookies = conn.getHeaderField("Set-Cookie");

                // open the new connnection again
                conn = (HttpURLConnection) new URL(newUrl).openConnection();
                conn.setRequestProperty("Cookie", cookies);
                conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
                conn.addRequestProperty("User-Agent", "Mozilla");
                conn.addRequestProperty("Referer", "google.com");

                System.out.println("Redirect to URL : " + newUrl);

            }

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer html = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                html.append(inputLine);
            }
            in.close();

//            System.out.println("URL Content... \n" + html.toString());
//            System.out.println("Done");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
