package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.GetFacilityInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class FacilityImplementInfo extends HttpServlet {

    public static String type;
    public static String getutype;
    public static int utype;

    public void doPost(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            JSONObject facilityImplementInfo = new JSONObject();

            facilityImplementInfo = GetFacilityInfo.implementInfo();

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/facilityImplementationInfo.jsp");
            request.setAttribute("facilityInfo", facilityImplementInfo);
            requestDispatcher.forward(request, response);

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

}
