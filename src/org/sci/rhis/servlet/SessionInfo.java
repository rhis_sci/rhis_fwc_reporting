package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionInfo extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){
		try {
			PrintWriter out = response.getWriter();
			HttpSession session=request.getSession(false);
			System.out.println(session.getAttribute("uID"));
	        if(session.getAttribute("uID")!=null){
	        	//HttpSession sessionSet=request.getSession(); 
	        	session.setAttribute("uName",null);
	        	session.setAttribute("uID",null);
	        	out.print("true");
	        }
	        else{
	        	out.print("false");
	        }
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}

}
