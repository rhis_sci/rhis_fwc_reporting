package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.ReportResultSearch;
import org.sci.rhis.report.SatelitePlanningSubmitInfo;

public class SubmittedApprovalList extends HttpServlet{
	
	public String getReport;

	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {
			RequestDispatcher requestDispatcher = null;
			//System.out.println("Report1Servlet");
			PrintWriter out = response.getWriter();
			
			JSONObject reportCred;
			
			reportCred = new JSONObject(request.getParameter("approvalInfo"));

			String approvalType = reportCred.getString("approvalType");

			if(!reportCred.has("viewOnly"))
				reportCred.put("viewOnly", "0");

			if(approvalType.equals("1")) {
				reportCred.put("type", "4");
				reportCred.put("report1_dateType", "1");
				if(reportCred.getString("viewOnly").equals("1"))
					reportCred.put("mis3Submit", "0");
				else
					reportCred.put("mis3Submit", "1");
				reportCred.put("mis3ViewType", "1");
				System.out.println(reportCred);
				getReport = ReportResultSearch.getSearchResult(reportCred);
				System.out.println("Advance Search Servlet: " + getReport);

				System.out.println(reportCred);

				String date_type = "";
				Integer DateOption = 0;
				String monthSelect = reportCred.getString("report1_month");
				String[] date = monthSelect.split("-");

				String dhis2_month = date[1];
				String dhis2_year = date[0];
				DateOption = Integer.parseInt(dhis2_year);


				if(DateOption > 2018)
				{
					requestDispatcher = request.getRequestDispatcher("jsp/NewMIS3Report.jsp");
				}
				else {
					requestDispatcher = request.getRequestDispatcher("jsp/report1Table.jsp");
				}

//				RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/report1Table.jsp");
				request.setAttribute("Result", getReport);
				requestDispatcher.forward(request, response);
			}
			else if(approvalType.equals("3")) {
//				requestDispatcher = request.getRequestDispatcher("jsp/approvalPanel.jsp");
				requestDispatcher = request.getRequestDispatcher("jsp/satelitePlannnigDetails.jsp");
				JSONObject getReport = SatelitePlanningSubmitInfo.getPlanningInfo(reportCred);
				request.setAttribute("eventparam", getReport);
				request.setAttribute("type", approvalType);
				out.print(getReport);
				requestDispatcher.forward(request, response);
			}
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
