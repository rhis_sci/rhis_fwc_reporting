package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.GetMapInfo;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DashboardData extends HttpServlet {
    public JSONObject info = new JSONObject();

    public void doPost(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {
            PrintWriter out = response.getWriter();

            JSONObject geoInfo;

            geoInfo = new JSONObject(request.getParameter("geoInfo"));
            geoInfo.put("methodType", "1");
            info.put("MNH", GetMapInfo.getInfo(geoInfo));

            geoInfo.put("methodType", "2");
            info.put("FP", GetMapInfo.getInfo(geoInfo));

            geoInfo.put("methodType", "3");
            info.put("GP", GetMapInfo.getInfo(geoInfo));

//            info = GetMapInfo.getInfo(geoInfo);
            //System.out.println("Test");
            //System.out.println(info);

            out.print(info);

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
