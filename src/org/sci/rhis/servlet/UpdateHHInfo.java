package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.CommunityInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.LinkedHashMap;

public class UpdateHHInfo extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response){
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        try {
            PrintWriter out = response.getWriter();
            JSONObject hhInfo = new JSONObject();
            hhInfo = new JSONObject(request.getParameter("forminfo"));
            if(hhInfo.has("submit"))
            {
                String status;
                boolean hhwardinfo = CommunityInfo.updateHHWardInfo(hhInfo);
                if(hhwardinfo == true)
                    status = "1";
                else
                    status = "0";
                out.print(status);
            }
            else {
                LinkedHashMap<String, LinkedHashMap<String, String>> hhwardinfo = CommunityInfo.LoadVillage(hhInfo);
                System.out.println(hhwardinfo);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/HHVillage.jsp");
                request.setAttribute("HHInfo", hhwardinfo);
                requestDispatcher.forward(request, response);
            }
            out.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
