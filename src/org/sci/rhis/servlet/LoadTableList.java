package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.ProviderInfo;
import org.sci.rhis.report.SymChannelCreate;

public class LoadTableList extends HttpServlet {
	
public String dropdown;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {
			PrintWriter out = response.getWriter();
			
			JSONObject geoInfo;
			
			geoInfo = new JSONObject(request.getParameter("geoInfo"));
			
			//System.out.println(formValue.getJSONObject("facilityname"));
			
			//report1Search = ReportResultSearch.getSearchResult(reportCred);
			dropdown = SymChannelCreate.getDropdown(geoInfo);
			
			out.print(dropdown);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
