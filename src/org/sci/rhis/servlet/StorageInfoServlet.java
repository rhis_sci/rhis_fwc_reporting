package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;
import org.sci.rhis.model.GetResult;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

public class StorageInfoServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        try {
            JSONObject requestInfo = new JSONObject(request.getParameter("reportInfo"));
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();


            RequestDispatcher requestDispatcher;
            request.setAttribute("type", requestInfo.getString("type"));
            if (requestInfo.has("status")) {
                requestDispatcher = request.getRequestDispatcher("jsp/storageinfo/viewstorageinfo.jsp");
            } else {
                requestDispatcher = request.getRequestDispatcher("jsp/storageinfo/storageInfo.jsp");
                ResultSet rs = GetResult.getResultSet(requestInfo, "provider_tab_storage_info");
                request.setAttribute("STORAGEInfo", rs);

            }

            requestDispatcher.forward(request, response);

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
