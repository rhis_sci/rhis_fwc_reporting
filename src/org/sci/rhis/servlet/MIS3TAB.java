package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
//import org.apache.tomcat.util.codec.binary.Base64;
import java.util.Base64;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.tomcat.util.codec.binary.StringUtils;
import org.json.JSONObject;
import org.sci.rhis.report.ReportResultSearch;

public class MIS3TAB extends HttpServlet {
	
	public String report1Search;

	public void doGet(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		
		
		try {
			
			PrintWriter out = response.getWriter();

			byte[] valueDecoded = Base64.getDecoder().decode(request.getQueryString());
			String getValue = new String(valueDecoded);//StringUtils.newStringUtf8(Base64.decodeBase64(request.getQueryString()));
			
			System.out.println(getValue);
			
			String[] parameter = getValue.split("&");
			
			JSONObject reportCred = new JSONObject();
			
			for(int i=0;i<parameter.length;i++)
			{
				
				String[] parameterValue = parameter[i].split("=");
				if(parameterValue.length>1)
					reportCred.put(parameterValue[0], parameterValue[1]);
				else
					reportCred.put(parameterValue[0], "");
				
				//System.out.println(parameterValue[1]);
			}
			
			if(!reportCred.has("providerId"))
				reportCred.put("providerId", "");
			
			if(!reportCred.has("csba"))
				reportCred.put("csba", "2");
			
			if(!reportCred.has("mis3ViewType"))
				reportCred.put("mis3ViewType", "");
			
			if(!reportCred.has("mis3Submit"))
				reportCred.put("mis3Submit", "0");
			
			if(!reportCred.has("mis3ViewType"))
				reportCred.put("mis3ViewType", "2");

			if(!reportCred.has("jsonRequest"))
				reportCred.put("jsonRequest", "0");
			
			reportCred.put("type", "4");
			reportCred.put("report1_dateType", "2");
			
			//System.out.println(reportCred);
			
			System.out.println(reportCred);
			
			report1Search = ReportResultSearch.getSearchResult(reportCred);
			
			if(reportCred.get("csba").equals("1")){
			
				String[] responseParameter = report1Search.split(",");
				
				JSONObject reportResponse = new JSONObject();
				
				for(int i=0;i<responseParameter.length;i++)
				{
					
					String[] responseParameterValue = responseParameter[i].split("=");
					if(responseParameterValue.length>1)
						reportResponse.put(responseParameterValue[0], responseParameterValue[1]);
					else
						reportResponse.put(responseParameterValue[0], "");
					
					//System.out.println(parameterValue[1]);
				}
				
				response.setContentType("application/json");
			    response.setCharacterEncoding("UTF-8");
			    out.print(reportResponse.toString());
			}
			else if(reportCred.get("jsonRequest").equals("1")){
				JSONObject reportResponse = new JSONObject(report1Search);

				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				out.print(reportResponse.toString());
			}
			else{

				String start_date = reportCred.getString("report1_start_date");
				String end_date = reportCred.getString("report1_end_date");

				String[] date = end_date.split("-");

				//bangla_month = EnglishtoBangla.ConvertMonth(date[1]);
				//bangla_year = EnglishtoBangla.ConvertNumberToBangla(date[0]);
				System.out.println("Json:"+report1Search);
				int DateOption = Integer.parseInt(date[0]);
				RequestDispatcher requestDispatcher = null;
				System.out.println(report1Search.substring(0,4));

				//to view only webend not in tab format
				if(reportCred.has("mis3webView")){
					request.setAttribute("webview", "true");

				}else{
					request.setAttribute("webview", "false");
				}

					if (DateOption < 2019 || report1Search.substring(0, 4).equals("<div"))
						requestDispatcher = request.getRequestDispatcher("jsp/reportMIS3TAB.jsp");
					else
						requestDispatcher = request.getRequestDispatcher("jsp/reportMIS3NEWTAB.jsp");

				request.setAttribute("Result", report1Search);
				requestDispatcher.forward(request, response);
			}
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
