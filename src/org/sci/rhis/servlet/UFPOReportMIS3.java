package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.MIS3SubmitInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

public class UFPOReportMIS3 extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            String getSubStringValue = request.getQueryString().substring(40);
            String getURLValue = request.getQueryString().substring(0,40) + getSubStringValue.substring(7);

            String salt = getSubStringValue.substring(0, 7);

            if(salt.equals("Q1a2Z3W")) {

                byte[] valueDecoded = Base64.getDecoder().decode(getURLValue);
                String getValue = new String(valueDecoded);//StringUtils.newStringUtf8(Base64.decodeBase64(request.getQueryString()));

                System.out.println(getValue);

                String[] parameter = getValue.split("&");

                JSONObject reportCred = new JSONObject();

                for (int i = 0; i < parameter.length; i++) {
                    String[] parameterValue = parameter[i].split("=");
                    reportCred.put(parameterValue[0], parameterValue[1]);
                }

                if(reportCred.getString("providerType").equals("15")) {

                    reportCred.put("report1_zilla", reportCred.getString("zillaid"));
                    reportCred.put("report1_upazila", reportCred.getString("upazilaid"));
                    reportCred.put("report1_month", reportCred.getString("year") + "-" + String.format("%02d", Integer.parseInt(reportCred.getString("month"))));

                    System.out.println(reportCred);

                    String result = MIS3SubmitInfo.MIS3SubmisssionInfo(reportCred);

                    RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/ufpo_report.jsp");
                    request.setAttribute("Result", result);
                    requestDispatcher.forward(request, response);
                }

            }

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
