package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.PregnantWomenList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class PregWomenServiceInfo extends HttpServlet {

    List<JSONObject> resultString = new ArrayList<JSONObject>();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        try {
            PrintWriter out = response.getWriter();
            JSONObject reportCred;
            reportCred = new JSONObject(request.getParameter("reportInfo"));

            request.setAttribute("healthid", (reportCred.getString("healthid").equals("") ? reportCred.getString("nrcid") : reportCred.getString("healthid")));
            request.setAttribute("client", reportCred.getString("client"));
            request.setAttribute("age", reportCred.getString("age"));
            request.setAttribute("hus", reportCred.getString("hus"));
            request.setAttribute("lmp", reportCred.getString("lmp"));
            request.setAttribute("edd", reportCred.getString("edd"));
            request.setAttribute("para", reportCred.getString("para"));
            request.setAttribute("gravida", reportCred.getString("gravida"));
            /*request.setAttribute("refercentername", reportCred.getString("refercentername"));*/
            resultString = PregnantWomenList.getPregwomenServiceDetails(reportCred);

            //call specific jsp regarding each section
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/pregnantwomenlist/pregwomenModalView.jsp");

            request.setAttribute("result", resultString);
            requestDispatcher.forward(request, response);


            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


