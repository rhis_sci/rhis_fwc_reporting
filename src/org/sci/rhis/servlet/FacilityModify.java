package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.GetFacilityInfo;
import org.sci.rhis.report.ProviderInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "FacilityModify")
public class FacilityModify extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            JSONObject forminfo;

            forminfo = new JSONObject(request.getParameter("facilityinfo"));

            JSONObject facility_details = new JSONObject();
            //facility_details = ProviderInfo.getProviderCode(forminfo);
            facility_details = GetFacilityInfo.getFacilityEditInfo(forminfo);

            //out.print(facility_details);
            //out.close();

            System.out.println("Facilityinfo = " + facility_details);

            request.setAttribute("jsonString", facility_details.toString());

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/modify_facilityinfo.jsp");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setDateHeader("Expires", 0); // Proxies.
            requestDispatcher.forward(request, response);
            out.close();

        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }


}
