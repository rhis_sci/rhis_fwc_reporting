package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.DashboardInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DashboardServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            JSONObject dashboardInfo = new JSONObject(request.getParameter("dashboardInfo"));

            JSONObject getGraphInfo_MNCH = new JSONObject();

            getGraphInfo_MNCH = DashboardInfo.getServiceStatistics(dashboardInfo.getString("providerId"), "1");

            JSONObject getGraphInfo_FP = new JSONObject();

            getGraphInfo_FP = DashboardInfo.getServiceStatistics(dashboardInfo.getString("providerId"), "2");

            JSONObject getGraphInfo_GP = new JSONObject();

            getGraphInfo_GP = DashboardInfo.getServiceStatistics(dashboardInfo.getString("providerId"), "3");

            String zilla_id = DashboardInfo.getZillaId(dashboardInfo.getString("providerId"));

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/dashboard.jsp");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setDateHeader("Expires", 0); // Proxies.
            request.setAttribute("MNCH", getGraphInfo_MNCH);
            request.setAttribute("FP", getGraphInfo_FP);
            request.setAttribute("GP", getGraphInfo_GP);
            request.setAttribute("zilla_id", zilla_id);
            requestDispatcher.forward(request, response);

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
