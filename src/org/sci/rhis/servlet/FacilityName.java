package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.GetFacilityInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "FacilityName")
public class FacilityName extends HttpServlet {

    JSONObject facilityInfo = new JSONObject();
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        try {
            PrintWriter out = response.getWriter();

            JSONObject formValue = new JSONObject();

            formValue = new JSONObject(request.getParameter("forminfo"));

            System.out.println("Formvalue="+formValue);

            facilityInfo = GetFacilityInfo.getFacilityName(formValue);
            out.print(facilityInfo);

            out.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }


}
