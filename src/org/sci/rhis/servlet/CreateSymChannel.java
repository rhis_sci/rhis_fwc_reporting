package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.report.CreateNewDatabase;
import org.sci.rhis.report.SymChannelCreate;

public class CreateSymChannel extends HttpServlet {
	
	public boolean forminfo;

	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		String status;
		
		try {
			//System.out.println("Report1Servlet");
			PrintWriter out = response.getWriter();
			
			JSONObject formValue;
			
			formValue = new JSONObject(request.getParameter("forminfo"));
			
			//System.out.println(formValue.getJSONObject("facilityname"));
			
			//report1Search = ReportResultSearch.getSearchResult(reportCred);
			forminfo = SymChannelCreate.createChannel(formValue);
			
			if(forminfo == true)
				status = "1";
			else
				status = "0";
			
			out.print(status);
			
			out.close();
		} 
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}

}
