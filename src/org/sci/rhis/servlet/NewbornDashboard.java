package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.MIS3SubmitInfo;
import org.sci.rhis.report.NewbornReport;
import org.sci.rhis.report.SatelitePlanningSubmitInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NewbornDashboard extends HttpServlet {


    public static String type;
    public static String getutype;
    public static int utype;

    public void doPost(HttpServletRequest request, HttpServletResponse response){

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS


        try {
            //System.out.println("ReportServlet");
            PrintWriter out = response.getWriter();

            JSONObject info;
            info = new JSONObject(request.getParameter("reportInfo"));
            List<JSONObject> resultString = new ArrayList<JSONObject>();


            resultString = NewbornReport.getResultSet(info);



            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/newborn/newbornResult.jsp");
            request.setAttribute("zilla",info.getString("report1_zilla"));
            request.setAttribute("upazila",info.getString("report1_upazila"));
            request.setAttribute("union",info.getString("report1_union"));
            request.setAttribute("chart_data", resultString);

            requestDispatcher.forward(request,response);

//            out.print(resultString.toString());
        }
        catch(IOException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
