package org.sci.rhis.servlet;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Key;
import java.sql.ResultSet;
import java.util.Base64;

import static org.sci.rhis.util.Helper.readInput;

public class PregwomenDataAPI extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        /**demo: http://43.229.15.152:8080/rhis_fwc_monitoring/pregwomendata
         * you can check the url from git bash with:
         * curl -H "Authorization: Bearer pr0VidEr#985M@m0ni" 'http://localhost:8081/pregwomeninfo?zilla=4&nid=[5512452433,5080689267,5052066155]
         */

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        String body = null;
        String dist = "";
        String nid = "";
        JSONObject data = new JSONObject();
        JSONObject result = new JSONObject();
        int status = 103;
        String message = null;
        PrintWriter out = response.getWriter();
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        try {


            body = readInput(request);
            JSONObject input = new JSONObject(body);

            if (input.has("accesstoken") && validateToken(input.getString("accesstoken"))) {


                dist = input.getString("zillaid");
                JSONArray nidList = input.getJSONArray("nid");
                int arrayLen = nidList.length();

                for (int i = 0; i < arrayLen; i++) {
                    nid = nidList.getString(i);


                    //number of nboy and ngirl, visitdate,
//                    String sql = "select name, c.dob, c.husbandname, c.fathername, c.mothername,a.visitdate, gravida,p.lmp," +
//                            "count(case when serviceid=1 then 1 else null end) as \"ANC1\"," +
//                            "count(case when serviceid=2 then 1 else null end) as \"ANC2\"," +
//                            "count(case when serviceid=3 then 1 else null end) as \"ANC3\"," +
//                            "count(case when serviceid=4 then 1 else null end) as \"ANC4\"," +
//                            "round((EXTRACT(day FROM (now() - p.lmp))::numeric)/7,2) AS Pregnancy_week" +
//                            " from member m inner join clientmap c using (generatedid) inner join pregwomen p on c.generatedid=   p.healthid" +
//                            " inner join ancservice a on a.healthid = p.healthid and a.pregno=p.pregno" +
//                            " where nid =" + nid + " and m.zillaid =" + dist + " and Cast(((current_date-m.dob)/365.25) as int)>=20 and Cast(((current_date-m.dob)/365.25) as int) <= 35 group by 1,2,3,4,5,6,7,8";


                    String sql = "with t as (select name, c.dob, c.husbandname, c.fathername, c.mothername, gravida," +
                            "p.lmp, (e.son+e.dau) as nlastchild, round((EXTRACT(day FROM (now() - p.lmp))::numeric)/7) AS Pregnancy_week," +
                            "(case when serviceid = 1 then visitdate else null end )as \"ANC1\"," +
                            "(case when serviceid = 2 then visitdate else null end )as \"ANC2\"," +
                            "(case when serviceid = 3 then visitdate else null end )as \"ANC3\"," +
                            "(case when serviceid = 4 then visitdate else null end )as \"ANC4\"" +
                            " from member m inner join clientmap c using (generatedid)" +
                            " inner join pregwomen p on c.generatedid = p.healthid inner join ancservice a on a.healthid = p.healthid" +
                            " and a.pregno=p.pregno inner join elco e on e.healthid=p.healthid" +
                            " where nid =" + nid + " and m.zillaid =" + dist + " and " +
                            " Cast(((current_date-m.dob)/365.25) as int)>=20 and" +
                            " Cast(((current_date-m.dob)/365.25) as int) <= 35 )" +
                            " select name,dob,husbandname,fathername,mothername,gravida,lmp,nlastchild,pregnancy_week," +
                            " max(\"ANC1\") as \"ANC1\"," + "max(\"ANC2\") as \"ANC2\"," +
                            " max(\"ANC3\") as \"ANC3\"," +
                            " max(\"ANC4\") as \"ANC4\" from t group by 1,2,3,4,5,6,7,8,9";

                    DBOperation dbOp = new DBOperation();
                    DBInfoHandler dbObject = new FacilityDB().facilityDBInfo(dist);
                    ResultSet rs = dbOp.dbExecute(dbObject, sql).getResultSet();

                    rs.last();
                    rs.beforeFirst();

                    while (rs.next()) {

                        status = 101;
                        message = "data found";

                        data.put("Name", rs.getString("name"));
                        data.put("DOB", rs.getString("dob"));
                        data.put("HusbandName", rs.getString("husbandname"));
                        data.put("FatherName", new String(rs.getBytes("fathername"), "UTF-8"));
                        data.put("MotherName", rs.getString("mothername"));
                        data.put("Gravida", rs.getString("gravida"));
                        data.put("LMP", rs.getString("lmp"));
                        data.put("NumberOfChild", rs.getString("nlastchild"));
                        data.put("ANC1", rs.getString("ANC1"));
                        data.put("ANC2", rs.getString("ANC2"));
                        data.put("ANC3", rs.getString("ANC3"));
                        data.put("ANC4", rs.getString("ANC4"));
                        data.put("Pregnancy_week", rs.getString("Pregnancy_week"));


                    }
                    if(status!=101)
                        status = 102;
                    if (status == 102) {
                        message = "data is not found with this NID";
                    } else if (status == 101) {

                        break;
                    }

                }

            } else {
                status = 103;
                message = "Please send correct authorization token";

            }
        } catch (IOException e) {
            e.printStackTrace();
            message = e.getMessage();

        } catch (Exception e) {
            e.printStackTrace();
            message = e.getMessage();

        } finally {
            if(data==null) {
                result.put("result", "");
            }else
                result.put("result",data);
            result.put("status", status);
            result.put("message", message);
            out.println(result);
            out.close();


        }
    }


    public static boolean validateToken(String jwtString) {
        boolean isValidate = true;
        String secret = "mamoni";
        Key hmacKey = new SecretKeySpec(Base64.getDecoder().decode(secret),
                SignatureAlgorithm.HS256.getJcaName());

        Jws<Claims> jwt = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(jwtString);

        if (jwt.getBody().get("sub").equals("Authorized")) {

            isValidate = true;
        }

        return isValidate;
    }

}
