package org.sci.rhis.servlet;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.FacilityDB;

import java.io.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MapData extends HttpServlet{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){
		
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		try {

			FacilityDB dbFacility = new FacilityDB();
			DBOperation dbOp = new DBOperation();
			DBInfoHandler dbObject = dbFacility.GEODBInfo();
			dbObject = dbOp.dbCreateStatement(dbObject);

			int zilla = Integer.parseInt( request.getParameter("zilla"));
			int upazila = Integer.parseInt( request.getParameter("upazila"));
			int unions = Integer.parseInt( request.getParameter("union"));
			String query;
			
			//GENERATE QUERY
			if(zilla!=0 && upazila==0){
				query = "SELECT *, ST_AsGeoJSON(geom) as geomJ FROM upazila where zillaid = "+ zilla;
			}
			else if(zilla!=0 && upazila != 0 ){
				query = "SELECT *, ST_AsGeoJSON(geom) as geomJ FROM unions where zillaid = "+ zilla + " and upazilaid = "+upazila;
			}

			//else if(zilla!=0 && upazila != 0 && unions !=0){
				//query = "SELECT *, ST_AsGeoJSON(geom) as geomJ FROM unions where zillaid = "+ zilla + " and upazilaid = "+upazila + " and unionid = "+unions;
			//}

			else
				query = "SELECT *, ST_AsGeoJSON(geom) as geomJ FROM zilla ";
				//query = "SELECT *, geom FROM zilla ";
			
			
			//System.out.println("Query");
			//System.out.println(query);
            //System.out.println("Connected to PostgreSQL database!");
            //System.out.println("Reading Map records...");
			
			//Initialize statement and result
//			Statement statement = connection.createStatement();
			dbObject = dbOp.dbExecute(dbObject, query);
			ResultSet resultSet = dbObject.getResultSet();
//            ResultSet resultSet = statement.executeQuery(query);
            
            
            //Generating GeoJson
            String featureCollection = "{\"type\": \"FeatureCollection\",\"features\": [";
            //System.out.println(featureCollection);
            
            while (resultSet.next()) {
                
            	//Generating fill color 
                String color = (resultSet.getString("color")==null)?"#7a7a7a":resultSet.getString("color");
                
                //Generating GeoJson Feature
                String 	geoJsonFeature = "{";
                		geoJsonFeature += "\"type\": \"Feature\",";
                		geoJsonFeature +="\"properties\": {";
                		geoJsonFeature +="\"name\": \"" + resultSet.getString("namee") + "\",";
                		geoJsonFeature +="\"divid\":\"" + resultSet.getString("divid") + "\",";
                		geoJsonFeature +="\"zillaid\": "+ resultSet.getString("zillaid") +",";
                		geoJsonFeature +="\"upazilaid\": "+ resultSet.getString("upazilaid") +",";

                		if(zilla!=0 && upazila != 0 )
							geoJsonFeature +="\"unionid\": "+ resultSet.getString("unionid") +",";

                		geoJsonFeature +="\"color\": \""+color+"\",";
						geoJsonFeature +="\"marker_pos\": \""+resultSet.getString("marker_pos") +"\",";
                		geoJsonFeature +="\"is_mamoni\":\""+resultSet.getString("is_mamoni")+"\"";
                		geoJsonFeature +="},";
                		geoJsonFeature +="\"geometry\":" + resultSet.getString("geomJ");
                		geoJsonFeature +="}";  
                		
                //Separate each GeoJson feature by comma		
                if (resultSet.isLast()) {
                	featureCollection += geoJsonFeature;
                }
                else {
                	featureCollection += geoJsonFeature + ",";
                }
                		
                		
            }
            
            //Closing GeoJon Feature collectoin            
            featureCollection += "]}";
            
            
            //UNregister Driver
            //DriverManager.deregisterDriver(mySqlDriver);
            
            PrintWriter out = response.getWriter();

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/dashboard/map_ajax.jsp");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setDateHeader("Expires", 0); // Proxies.
			request.setAttribute("mapdata", featureCollection);
			requestDispatcher.forward(request, response);
			
			out.close();
            
 
        }
		catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}