package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.report.MhealthReport;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class Mhealth extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response){

        JSONObject info = new JSONObject();

        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        //response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS

        HttpSession session = request.getSession();

            try {

                if(request.getParameter("uid")!=null) {
                    session.setAttribute("userid", request.getParameter("uid").toString());
                }

                if(request.getParameter("forminfo")!=null) {
                    info = new JSONObject(request.getParameter("forminfo"));
                }

                PrintWriter out = response.getWriter();

//                String sms_waiting = MhealthReport.sms_waiting(info);
                String sms_group_count = MhealthReport.sms_type_count(info);
                String sms_location_wise = MhealthReport.sms_location_wise_distribution(info);
                String sms_service_wise = MhealthReport.sms_service_wise(info);


                System.out.println(sms_service_wise);

                RequestDispatcher requestDispatcher;

                if(info.has("ajax")){
                    requestDispatcher = request.getRequestDispatcher("jsp/mhealth/ajax_response.jsp");
                }else{
                    requestDispatcher = request.getRequestDispatcher("jsp/mhealth/mhealth_view.jsp");
                }

                response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
                response.setDateHeader("Expires", 0); // Proxies.

//                request.setAttribute("sms_waiting", sms_waiting);
                request.setAttribute("sms_type_count", sms_group_count);
                request.setAttribute("sms_location_wise", sms_location_wise);
                request.setAttribute("sms_service_wise", sms_service_wise);

                requestDispatcher.forward(request, response);

                out.close();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            catch(Exception e){
                e.printStackTrace();
            }


    }
}
